<?php
use ddcop\models\OperatorDesa;
?>

<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Operator Desa</h2>
            <div class="p-10">
                <table class="table">
					<tr>
						<th>Nama Operator</th>
						<th>Email</th>
						<th>Tipe</th>
						<th>Status</th>
						<th>Tanggal</th>
						<th>Aksi</th>
					</tr>
					<?php
					$mod = OperatorDesa::find()->where(['kd_desa' => Yii::$app->user->identity->id]);
					foreach ($mod->all() as $v) {
					?>
					<tr>
						<td><?= ($v->operatorDetail != null)?$v->operatorDetail->nama:'Belum isi biodata' ?></td>
						<td><?= $v->operator->email ?></td>
						<td><?= $v->operator->tipe_operator ?></td>
						<td><?= $v->showStatus() ?></td>
						<td><?= $v->propose_at ?></td>
						<td><a href="javascript:void(0)" class="btn btn-xs" onclick="goLoad({url: '/operatora/desa-akses-operator/konfirmasi-desa?iduser=<?=$v->id?>'});"><i class="fa fa-send"></i></a></td>
					</tr>
					<?php
					}
					?>
				</table>
            </div>
        <!-- Dynamic Chart -->
	    </div>
    </div>
    <div class="clearfix"></div>
</div>