<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use sp3d\models\User;
/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */
/* @var $form yii\widgets\ActiveForm */
$arr = ['-' => ''];
if($model->isNewRecord){
    $model->id = Yii::$app->user->identity->id;
    $model->status = 0;    
}
$listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');
$listArr = $arr + $listArr;
?>

<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Pilih Desa</h2>
            <div class="p-10" id="operator-area">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
                <div class="row">
                    <div class="col-md-4">

                    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'kd_kabupaten')->dropdownList($listArr)->label('kabupaten') ?>
                    <?= $form->field($model, 'kd_kecamatan')->dropdownList([])->label('Kecamatan') ?>
                    <?= $form->field($model, 'kd_desa')->dropdownList([])->label('Desa') ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                    </div>
                </div>
                <?php ActiveForm::end();  ?>
            </div>
        </div>
    </div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('#operatordesa-kd_kabupaten').change(function(){
  var kode = $(this).val();
  $('#operatordesa-kd_kecamatan, #operatordesa-kd_desa').html('<option value=""></option>');

  if(kode !== ''){
    goLoad({elm:'#operatordesa-kd_kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});


$('#operatordesa-kd_kecamatan').change(function(){
  var kode = $(this).val();
  $('#operatordesa-kd_desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#operatordesa-kd_desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            console.log('Succed');
            goLoad({url:'/operator/operator/pilih-desa'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);