<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
	'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */
/* @var $form yii\widgets\ActiveForm */
$sama = 0;
if($model->isNewRecord){
	// $model->id = $model->kd_desa = 
}
?>

<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Profile Operator</h2>
            <div class="p-10" id="operator-area">
            	<?php $form = ActiveForm::begin($arrFormConfig); ?>
			    <div class="row">
			        <div class="col-md-8">
			            <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true])->label(false) ?>
			            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
			            <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
			            <?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true]) ?>
			            <?= $form->field($model, 'gender')->dropdownList(['L' => 'Laki-Laki', 'P' => 'Perempuan']) ?>
			            
			            <?= $form->field($model, 'pendidikan')->dropdownList([
					        '0|SD' => 'Sekolah Dasar', 
                            '1|SMP' => 'Sekolah Menengah Pertama', 
                            '2|SMA' => 'Sekolah Menengah Atas', 
                            '3|D1' => 'Diploma I', 
                            '4|D2' => 'Diploma II', 
                            '5|D3' => 'Diploma III', 
                            '6|D4' => 'Diploma IV', 
                            '7|S1' => 'Sarjana', 
                            '8|S2' => 'Magister', 
                            '9|S3' => 'Doktoral', 
					    ]) ?>

					    <?= $form->field($model, 'no_hp')->textInput(['maxlength' => true]) ?>

			            <?php // $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

			            <div class="row">
							<div class="col-sm-3"></div>
							<div class="col-sm-7">
								<?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
							</div>
						</div>
			        </div>
			    </div>

			    <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php $script = <<<JS
$('#operatordetail-tanggal_lahir').datetimepicker({
    format:'Y-m-d',
    mask:true
});

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            console.log('Succed');
            $.alert('Berhasil mengupdate data pribadi');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);