<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Pesan</h2>
            <div class="p-10">
                <h4 class="page-title b-0">Pesan Dari Admin</h4>
                
                <div class="listview list-click">
                    <header class="listview-header media">                            
                        <ul class="list-inline pull-right m-t-5 m-b-0">
                            <li class="pagin-value hidden-xs">35-70</li>
                            <li>
                                <a href="messages.html" title="Previous" class="tooltips">
                                    <i class="sa-list-back"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" title="Next" class="tooltips">
                                    <i class="sa-list-forwad"></i>
                                </a>
                            </li>
                        </ul>
                        
                        <ul class="list-inline list-mass-actions pull-left">
                            <li class="m-r-10">
                                <a href="messages.html" title="Back to Inbox" class="tooltips">
                                    <i class="sa-list-back"></i>
                                </a>
                            </li>
                            
                            <li>
                                <a href="javascript:void(0)" title="Archive" class="tooltips">
                                    <i class="sa-list-archive"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" title="Spam" class="tooltips">
                                    <i class="sa-list-spam"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" title="Delete" class="tooltips">
                                    <i class="sa-list-delete"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" title="Move" class="tooltips">
                                    <i class="sa-list-move"></i>
                                </a>
                            </li>
                            
                        </ul>

                        <div class="clearfix"></div>
                    </header>
                                        
                    <h2 class="page-title">Siskeudes Harap Di isi dengan Benar</h2>

                    <div class="media message-header o-visible">
                        <img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">
                        <div class="pull-right dropdown m-t-10">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="p-5">Pilihan</a>
                            <ul class="dropdown-menu text-right">
                                <li><a href="javascript:void(0)">Balas</a></li>
                                <li><a href="javascript:void(0)">Teruskan</a></li>
                                <li><a href="javascript:void(0)">Tandai Sebagai Spam</a></li>
                            </ul>
                        </div>
                        <div class="media-body">
                            <span class="f-bold pull-left m-b-5">User Desa/Kecamatan</span>
                            <div class="clearfix"></div>
                            <span class="dropdown m-t-5">
                                To <a href="javascript:void(0)" class="underline">Me</a> on 12th February 2014
                            </span>
                        </div>
                    </div>
                    
                    <div class="p-15">
                        <p>Selamat Pagi Admin,</p>
                        <p>Sehubungan dengan adanya pembaharuan sistem terbaru, kami menghimbau untuk kembali melakukan sinkronasi</p>
                    </div>
                    
                    <hr class="whiter">
                    <div class="p-15">
                        <p>1 Sisipan - <a class="underline" href="javascript:void(0)">Download semua ZIP</a></p>
                        
                        <a class="message-attachement news tile" href="javascript:void(0)">
                            <img src="img/filemanager/icon/doc.png" alt="Meeting Agenda.doc">
                            <br/>
                            <small class="t-overflow">SuratTugas.docx</small>
                        </a>
                    </div>    
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>