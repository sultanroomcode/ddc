<?php
namespace ddcop\modules\operator\controllers;

use yii\web\Controller;
use ddcop\models\OperatorDetail;
use ddcop\models\Operator;
use ddcop\models\OperatorDesa;
use Yii;
/**
 * Default controller for the `operator` module
 */
class OperatorController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionDashboard()
    {
        $modelc = OperatorDetail::findOne(['id' => Yii::$app->user->identity->id]);

        if($modelc == null){
            //new
            echo '<script>goLoad({url:\'/operator/operator/isi-data-pribadi\'});</script>';
        } else {
            return $this->renderAjax('dashboard', [
                'model' => $modelc,
            ]);
        }
    }

    public function actionSetPass($pass)
    {
        $modelc = Operator::findOne(['id' => Yii::$app->user->identity->id]);
        $modelc->password_hash = Yii::$app->security->generatePasswordHash($pass);
        var_dump($modelc->save(false));
    }

    public function actionSetting()
    {
        $modelc = Operator::findOne(['id' => Yii::$app->user->identity->id]);

        if($modelc == null){
            //new
            echo '<script>goLoad({url:\'/operator/operator/isi-data-pribadi\'});</script>';
        } else {
            if ($modelc->load(Yii::$app->request->post())) {
                $data = Yii::$app->request->post();
                // $modelc->password_hash = Yii::$app->security->generatePasswordHash($data['Operator']['password_baru']);
                $modelc->setPassword($data['Operator']['password_baru']);
                if($modelc->save(false)) echo 1; else echo 0;
            } else {
                return $this->renderAjax('setting', [
                    'model' => $modelc,
                ]);
            }
        }
    }

    public function actionPesan()
    {
        return $this->renderAjax('pesan');
    }

    public function actionIsiDataPribadi()
    {
    	$modelc = OperatorDetail::findOne(['id' => Yii::$app->user->identity->id]);

    	if($modelc == null){
            //new
    		$model = new OperatorDetail();
            $model->id = Yii::$app->user->identity->id;
            
    		if ($model->load(Yii::$app->request->post())) {
	            if($model->save()) echo 1; else echo 0;
	        } else {
	            return $this->renderAjax('isi-data-pribadi', [
	                'model' => $model,
	            ]);
	        }
    	} else {
            //update
            if ($modelc->load(Yii::$app->request->post())) {
                if($modelc->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('isi-data-pribadi', [
                    'model' => $modelc,
                ]);
            }
        }
    	
    }

    public function actionDesa()
    {
        $modelc = OperatorDesa::findOne(['id' => Yii::$app->user->identity->id]);

        return $this->renderAjax('desa', [
            'model' => $modelc,
        ]);
    }

    public function actionPilihDesa()
    {
    	$modelc = OperatorDesa::findOne(['id' => Yii::$app->user->identity->id]);//0 belum dilihat, 5 ditolak, 10 disetujui
        // echo $modelc->createCommand()->getRawSql(); //<- hanya bisa untuk find() bukan findOne
        if($modelc !== null){
            //update
            return $this->renderAjax('desa-pilihan', [
                'model' => $modelc
            ]);
        } else {
            //new
            $model = new OperatorDesa();
            $model->id = Yii::$app->user->identity->id;
            
            if ($model->load(Yii::$app->request->post())) {
                if($model->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('pilih-desa', [
                    'model' => $model,
                ]);
            }
        }
    }
}
