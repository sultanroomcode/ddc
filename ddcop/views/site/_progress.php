<section class="bar background-pentagon">
    <div class="container">
        <div class="col-md-12">
            <div class="heading text-center">
                <h2>Progres Desa</h2>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="box-simple">
                        <div class="icon-box" id="counter-desa">{{desa}}</div>
                        <h3>APB Desa</h3>
                        <p>Jumlah desa yang telah meng-entri APB Desa<br>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'data-umum/desa'})" class="btn btn-sm btn-primary">Lihat <i class="fa fa-link"></i></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-simple">
                        <div class="icon-box" id="counter-akumulasi">
                            {{desa}}
                        </div>
                        <h3>Akumulasi</h3>
                        <p>Akumulasi Nilai APB Desa Seluruh Kabupaten Tuban<br>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'data-umum/akumulasi'})" class="btn btn-sm btn-primary">Lihat <i class="fa fa-link"></i></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-simple">
                        <div class="icon-box" id="counter-penyerapan">
                            5
                        </div>
                        <h3>Penyerapan Anggaran</h3>
                        <p>Laporan Penyerapan Anggaran Rinci<br>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'data-umum/penyerapan'})" class="btn btn-sm btn-primary">Lihat <i class="fa fa-link"></i>
                        </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-custom" style="display: none;">
            <div class="col-md-10" style="width: 100%; background: #fff; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
var base_url = $('body').attr('data-url');
function getReadyCounter(o){
    $.ajax({
        type: 'get',
        url: base_url+o.url,
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        },
        success: function (res) {
            console.log(res);
            $('#counter-desa').text(res.data.desa);
            $('#counter-akumulasi').text(res.data.akumulasi);
        }
    });
}

getReadyCounter({url:'data-umum/data-count'});
</script>

<?php
if(Yii::$app->params['offline']){
  $this->registerCssFile('//localhost/local-cdn/bower_components/custombox/dist/custombox.min.css');
  $this->registerJsFile('//localhost/local-cdn/bower_components/custombox/dist/custombox.min.js');
} else {
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/custombox/4.0.3/custombox.min.css');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/custombox/4.0.3/custombox.min.js');
}