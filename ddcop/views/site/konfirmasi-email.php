<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use ddcop\models\Operator;

$mo = Operator::findOne(['email' => $email]);
$check = ($mo != null)?true:false;

$this->title = 'konfirmasi Email '.$email.' '.($check)?'Berhasil':'Gagal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper animated bounceInDown">
    <div class="title"><h1>Konfirmasi Email <?= ($check)?'Berhasil':'Telah Di Aktifkan' ?><br><b>Data Desa Center</b></h1></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="top-header">
                    <img src="../imgf/logo.png">
                    <p>Dinas Pemberdayaan Masyarakat Dan Desa<br>Provinsi Jawa Timur</p>

                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-lg-12" id="box-thanks">
                           <?php 
                           if($check){
                              if($mo->status == 0 && md5($mo->id) == $kode){
                                $mo->konfirmasi_email = $mo->status = 10;
                                $mo->update(false);

                                echo "Registrasi ".$mo->email." Berhasil. Akun telah diaktifkan anda bisa login dan mengisi data";
                                echo '<a href="'.Url::to(['/site/login?p='.$mo->tipe_operator]).'" class="btn btn-block btn-success">Kembali Ke Halaman Login</a>';
                              } else {
                                echo "Registrasi ".$mo->email." telah diaktifkan anda bisa login dan mengisi data";
                                echo '<a href="'.Url::to(['/site/login?p='.$mo->tipe_operator]).'" class="btn btn-block btn-success">Kembali Ke Halaman Login</a>';
                              }
                           } else {
                              echo "Email ".$email." tidak ditemukan. Silahkan mendaftar sebagai operator untuk dapat menggunakan sistem";
                              echo '<a href="'.Url::to(['site/register?p=bumdes']).'" class="btn btn-block btn-success">Kembali Ke Halaman Login</a>';
                           }
                           ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$style =<<<CSS
#box-thanks {
    padding: 20px 27px;    
}

body{ 
  background: url(../imgf/surabaya.png) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.margino {
    margin: 1em 1em -2em 1em;
}
.wrapper{
  margin-left: auto;
  margin-right: auto;
  max-width:26em; 
  margin-top: 40px;
}
@media (max-width: 500px){
 .wrapper{
  margin-top: 10px;
  } 
}
.title h1{
  text-align: center;
  max-width:50em; 
  padding: 5px 0 15px 0;
  font-size: 36px;
  font-weight: 600;
  color: #766db4;
}
@media (max-width: 500px){
  .title h1{
    font-size: 25px;
  }
}
.box {
  background-color: #fff;
  margin-left: auto;
  margin-right: auto;
  max-width:26em; 
}
.box{
      -webkit-box-shadow: 0 10px 6px -6px #777;
         -moz-box-shadow: 0 10px 6px -6px #777;
              box-shadow: 0 10px 6px -6px #777;
    }
.top-header{
  max-width:26em;
  margin-left: auto;
  margin-right: auto; 
  background: url(../imgf/header-login.png )no-repeat right ;
  min-height: 100px;
}
.top-header img{
  padding-top: 10px;
  width: 50px;
  display: block;
  margin-left: auto;
  margin-right: auto;
}
.top-header p{
  margin: 5px 0;
  padding-bottom: 5px;
  text-align: center;
  color: #999;
  text-transform: uppercase;
  font-size: 12px;
  letter-spacing: 2px;
}
.box-content{
  padding: 0 15px;
}
.form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 1px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.btn {
  text-align: center;
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
      touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 0;
}
CSS;

$this->registerCss($style);