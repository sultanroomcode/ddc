<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\LoginForm;

$model = new LoginForm();
?>
<header>
        <!-- *** TOP ***
_________________________________________________________ -->
        <div id="top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-5 contact">
                        <p class="hidden-sm hidden-xs"></p>
                        <p class="hidden-md hidden-lg"><a href="#" data-animate-hover="pulse"><i class="fa fa-phone"></i></a>  <a href="#" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                        </p>
                    </div>
                    <div class="col-xs-7">
                        <div class="social">
                            <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                        </div>
                        <?php if(Yii::$app->user->isGuest): ?>
                        <div class="login">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#sp-sv-login"><i class="fa fa-sign-in"></i> <span class="hidden-xs text-uppercase">Masuk</span></a>
                        </div>
                        <?php else: ?>
                            <div class="login">
                                <a href="<?= Url::to(['/dashboard']) ?>"><i class="fa fa-dashboard"></i> <span class="hidden-xs text-uppercase">DASHBOARD</span></a>
                                <a href="<?= Url::to(['site/logout']) ?>"><i class="fa fa-sign-out"></i> <span class="hidden-xs text-uppercase">KELUAR</span></a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>

            <!-- *** LOGIN MODAL ***
_________________________________________________________ -->

            <div class="modal fade" id="sp-sv-login" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
                <div class="modal-dialog modal-sm">

                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="Login">Login SP3D</h4>
                        </div>
                        <div class="modal-body">
                            <?php $form = ActiveForm::begin(['action' => Url::to(['site/login']), 'id' => 'login-form']); ?>

                            <?= $form->field($model, 'username') ?>

                            <?= $form->field($model, 'password')->passwordInput() ?>
                            <span style="color: #111;">
                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                            </span>
                            <div class="form-group">
                                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>  
                        </div>
                    </div>
                </div>
            </div>

            <!-- *** LOGIN MODAL END *** -->
        </div>

        <!-- *** TOP END *** -->

        <!-- *** NAVBAR ***
_________________________________________________________ -->

        <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

            <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                <div class="container">
                    <div class="navbar-header">

                        <a class="navbar-brand home" href="<?= Url::to(['/']) ?>">
                            <img src="<?=Url::to(['img/logo.png'], true) ?>" alt="Bappemas logo" class="hidden-xs hidden-sm">
                            <img src="<?=Url::to(['img/logo-small.png'], true) ?>" alt="Bappemas logo" class="visible-xs visible-sm"><span class="sr-only">go to homepage</span>
                        </a>
                        <div class="navbar-buttons">
                            <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="fa fa-align-justify"></i>
                            </button>
                        </div>
                    </div>
                    <!--/.navbar-header -->

                    <div class="navbar-collapse collapse" id="navigation">

                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown active">
                                <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Beranda <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= Url::to(['/']) ?>">Beranda</a></li>
                                    <li><a href="<?= Url::to(['/']) ?>">Grafik Data Keuangan</a></li>
                                    <li><a href="index.html">Berita</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Struktur Organisasi <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="index.html">DPMDKB</a></li>
                                    <li><a href="index2.html">Pemerintah Desa</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Realisasi <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="index.html">SPP</a></li>
                                    <li><a href="index.html">Pencairan</a></li>
                                    <li><a href="index.html">SPJ Kegiatan</a></li>
                                </ul>
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->



                    <div class="collapse clearfix" id="search">

                        <form class="navbar-form" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="input-group-btn">

                <button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button>

            </span>
                            </div>
                        </form>

                    </div>
                    <!--/.nav-collapse -->

                </div>


            </div>
            <!-- /#navbar -->

        </div>

        <!-- *** NAVBAR END *** -->

    </header>

    <!-- *** LOGIN MODAL ***
_________________________________________________________ -->

    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
        <div class="modal-dialog modal-sm">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="Login">Login Pengguna</h4>
                </div>
                <div class="modal-body">
                    <form action="customer-orders.html" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="email_modal" placeholder="email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password_modal" placeholder="password">
                        </div>

                        <p class="text-center">
                            <button class="btn btn-template-main"><i class="fa fa-sign-in"></i> Masuk</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- *** LOGIN MODAL END *** -->