<?php
use frontend\models\referensi\RefKecamatan;
$data = RefKecamatan::find()->orderBy('Nama_Kecamatan')->all();
?>
<div class="row animated slideInLeft">
<?php foreach ($data as $v): ?>
	<?php $v->tahun = $tahun; $kc = $v->kecamatancount; ?>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan-page?id=<?=$v->Kd_Kec?>&tahun=<?=$tahun?>'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> <?= substr($v->Nama_Kecamatan, 9) ?></a></span>
				<span class="info-box-number"><?= ($kc != null)?$kc->nf($kc->dana_rab):'Belum Ada Data' ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>
	<!-- /.info-box -->
<?php endforeach; ?>
</div>
<!-- /.row -->