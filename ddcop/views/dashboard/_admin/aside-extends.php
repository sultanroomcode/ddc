<?php
use frontend\models\SideMenu;
use yii\helpers\Url;
$arr = [
    ['link' => 'referensi/ref-bank-desa', 'label' => 'Bank Desa'],
    ['link' => 'referensi/ref-belanja-operasional', 'label' => 'Belanja Operasional'],
    ['link' => 'referensi/ref-bidang', 'label' => 'Bidang'],
    ['link' => 'referensi/ref-bunga', 'label' => 'Bunga'],
    ['link' => 'referensi/ref-desa', 'label' => 'Desa'],
    ['link' => 'referensi/ref-kecamatan', 'label' => 'Desa'],
    ['link' => 'referensi/ref-kololari', 'label' => 'Kololari'],
    ['link' => 'referensi/ref-neraca-close', 'label' => 'Neraca Close'],
    ['link' => 'referensi/ref-perangkat', 'label' => 'Perangkat'],
    ['link' => 'referensi/ref-potongan', 'label' => 'Potongan'],
    ['link' => 'referensi/ref-sbu', 'label' => 'SBU'],
    ['link' => 'referensi/ref-sumber', 'label' => 'Sumber']
];

$arr2 = [
    ['link' => 'transaksi/ta-anggaran', 'label' => 'Anggaran'],
    ['link' => 'transaksi/ta-anggaran-log', 'label' => 'Anggaran Log'],
    ['link' => 'transaksi/ta-anggaran-rincian', 'label' => 'Anggaran Rincian'],    
    ['link' => 'transaksi/ta-bidang', 'label' => 'Bidang'],
    ['link' => 'transaksi/ta-desa', 'label' => 'Desa'],
    ['link' => 'transaksi/ta-jurnal-umum', 'label' => 'Jurnal Umum'],
    ['link' => 'transaksi/ta-jurnal-umum-rinci', 'label' => 'Jurnal Umum Rincian'],
    ['link' => 'transaksi/ta-kegiatan', 'label' => 'Kegiatan'],
    ['link' => 'transaksi/ta-mutasi', 'label' => 'Mutasi'],
    ['link' => 'transaksi/ta-pajak', 'label' => 'Pajak'],
    ['link' => 'transaksi/ta-pajak-rinci', 'label' => 'Pajak Rinci'],
    ['link' => 'transaksi/ta-pemda', 'label' => 'Pemda'],
    ['link' => 'transaksi/ta-pencairan', 'label' => 'Pencairan'],
    ['link' => 'transaksi/ta-perangkat', 'label' => 'Perangkat'],
    ['link' => 'transaksi/ta-rab', 'label' => 'RAB'],
    ['link' => 'transaksi/ta-rab-sub', 'label' => 'RAB Sub'],
    ['link' => 'transaksi/ta-rab-rincian', 'label' => 'RAB Rincian'],
    ['link' => 'transaksi/ta-rpjm-bidang', 'label' => 'RPJM Bidang'],
    ['link' => 'transaksi/ta-rpjm-kegiatan', 'label' => 'RPJM Kegiatan'],
    ['link' => 'transaksi/ta-rpjm-misi', 'label' => 'RPJM Misi'],
    ['link' => 'transaksi/ta-rpjm-pagu-indikatif', 'label' => 'RPJM Pagu Indikatif'],
    ['link' => 'transaksi/ta-rpjm-pagu-tahunan', 'label' => 'RPJM Pagu Tahunan'],
    ['link' => 'transaksi/ta-rpjm-sasaran', 'label' => 'RPJM Sasaran'],
    ['link' => 'transaksi/ta-rpjm-tujuan', 'label' => 'RPJM Tujuan'],
    ['link' => 'transaksi/ta-rpjm-visi', 'label' => 'RPJM Visi'],
    ['link' => 'transaksi/ta-spj', 'label' => 'SPJ'],
    ['link' => 'transaksi/ta-spj-bukti', 'label' => 'SPJ Bukti'],
    ['link' => 'transaksi/ta-spj-pot', 'label' => 'SPJ Potongan'],
    ['link' => 'transaksi/ta-spj-rinci', 'label' => 'SPJ Rincian'],
    ['link' => 'transaksi/ta-spj-sisa', 'label' => 'SPJ Sisa'],
    ['link' => 'transaksi/ta-spp', 'label' => 'SPP'],
    ['link' => 'transaksi/ta-spp-bukti', 'label' => 'SPP Bukti'],
    ['link' => 'transaksi/ta-spp-pot', 'label' => 'SPP Potongan'],
    ['link' => 'transaksi/ta-spp-rinci', 'label' => 'SPP Rincian'],
    ['link' => 'transaksi/ta-saldo-awal', 'label' => 'Saldo Awal'],
    ['link' => 'transaksi/ta-tbp', 'label' => 'TBP'],
    ['link' => 'transaksi/ta-tbp-rinci', 'label' => 'TBP Rincian'],
    ['link' => 'transaksi/ta-triwulan', 'label' => 'Triwulan'],
    ['link' => 'transaksi/ta-triwulan-arsip', 'label' => 'Triwulan Arsip'],
    ['link' => 'transaksi/ta-user-desa', 'label' => 'User Desa'],
    ['link' => 'transaksi/ta-user-id', 'label' => 'User ID'],
    ['link' => 'transaksi/ta-user-log', 'label' => 'User Log'],
    ['link' => 'transaksi/ta-user-menu', 'label' => 'User Menu']
];

$menu = new SideMenu();
$menu->setAllLinks($arr);
?>
<li class="header">NAVIGASI ADMIN</li>
<li class="active"><a href="#"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
<li class="header">REFERENSI</li>
<li class="treeview">
    <a href="javascript:void(0)">
        <i class="fa fa-table"></i> <span>Data Referensi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <small class="hidden-xs label pull-right bg-green">new</small>
        </span>
    </a>
    <ul class="treeview-menu">
        <?= implode(' ', $menu->generateAjaxLinks()); ?>
        <li><a href="javascript:void(0)" onclick="goLoad({url:'#'})"><i class="fa fa-circle-o text-red"></i><span>Sumber</span>
            <span class="pull-right-container">
                <small class="label pull-right bg-red">3</small>
                <small class="label pull-right bg-blue">17</small>
            </span>
        </a></li>
    </ul>
</li>
<li class="header">TRANSAKSI</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-table"></i> <span>Data Transaksi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <small class="label pull-right bg-green hidden-xs">new</small>
        </span>
    </a>
    <ul class="treeview-menu">
        <?php $menu->setAllLinks($arr2); ?>
        <?= implode(' ', $menu->generateAjaxLinks()); ?>
    </ul>
</li>
<li><a href="javascript:void(0)" onclick="goLoad({url:'/referensi/default/all'})" title="Daftar Referensi Sistem"><i class="fa fa-book"></i> <span>Data Referensi</span></a>
<li><a href="javascript:void(0)" onclick="goLoad({url:'/umum/user'})" title="Daftar Pengguna Sistem"><i class="fa fa-users"></i> <span>Data Kabupaten</span></a>
<li><a href="javascript:void(0)" onclick="goLoad({url:'/umum/user'})" title="Daftar Pengguna Sistem"><i class="fa fa-users"></i> <span>User</span></a>