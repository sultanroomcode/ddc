<?php
use yii\helpers\Url;
?>
<!-- Sidbar Widgets -->
<div class="side-widgets overflow">
    <!-- Profile Menu -->
    <div class="text-center s-widget m-b-25 dropdown" id="profile-menu">
        <a href="" data-toggle="dropdown">
            <img class="profile-pic animated" src="<?= $directoryIcon ?>img/profile-pic.png" alt="">
        </a>
        <ul class="dropdown-menu profile-menu">
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/operator/operator/dashboard'})">Profile</a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/operator/operator/pesan'})">Pesan</a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/operator/operator/setting'})">Setting</a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
            <li><a href="<?= Url::to(['/site/logout']) ?>">Keluar</a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
        </ul>
        <h4 class="m-0"><span class="show-pop" data-animation="pop" id="show-pop-deskripsi" data-content="<p>Desa : ...<br>Kecamatan : ...<br>Kabupaten : ...</p>"><span id="akses-nama-lengkap"></span></span></h4>
        ~<span id="akses-nama-email"></span>
    </div>

    <div>
      <!-- <a href="javascript:void(0)" onclick="refreshTree()" class="btn btn-sm btn-info"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-sitemap"></i></a> -->
      <div id="jstree_menu" style="overflow-x: auto; height: 450px;margin-left: 0px;"></div>
    </div>
</div>
<?php
//jsTree on super-index
$url_jstree = 'menu-utama';
$scriptJs = <<<JS
  $('#jstree_menu').jstree({
       "plugins" : [ "search" ],
       'core' : {
          'data' : {
              "url" : "{$url_jstree}",
              "data" : function (node) {
                  return { "id" : node.id };
              }
          }
      }
  });
JS;

$this->registerJs($scriptJs);
