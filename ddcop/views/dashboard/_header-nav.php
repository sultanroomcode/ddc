<?php 
use yii\helpers\Url;
use yii\helpers\Html;
?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?= Url::to(['/dashboard'])?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>::</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">DATA<b>DESA CENTER</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <?php // if(!Yii::$app->user->isGuest){ echo $this->render('@frontend/views/dashboard/_'.Yii::$app->user->identity->viewRole().'/header-extends'); } ?>
    </nav>
  </header>