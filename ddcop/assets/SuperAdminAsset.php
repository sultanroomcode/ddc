<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace ddcop\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */

class SuperAdminAsset extends AssetBundle
{
    // public $urlPath = '//localhost';
    // public $urlPath = '//ddcop.dpmd.jatimprov.go.id';
    public $urlPath = '//192.168.0.115';
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/adminlte/bootstrap/css/bootstrap.min.css',
        // 'css/site.css',
        // 'css/adminlte/dist/css/AdminLTE.min.css',
        // 'css/adminlte/dist/css/skins/_all-skins.min.css',
        // 'css/adminlte/plugins/iCheck/flat/blue.css',
        // 'css/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        // 'css/adminlte/plugins/morris/morris.css',
        // 'css/adminlte/plugins/daterangepicker/daterangepicker.css',
        // 'css/adminlte/plugins/iCheck/flat/blue.css',
        // 'css/vendor/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
        'js/datetimepicker/jquery.datetimepicker.css',
        // 'css/adminlte/plugins/iCheck/flat/blue.css',
        // 'js/magnific-popup.css',

        'css/SuperAdmin-1-0-3/Template/1-0-3/css/animate.min.css',
        'css/SuperAdmin-1-0-3/Template/1-0-3/css/bootstrap.min.css',
        'css/SuperAdmin-1-0-3/Template/1-0-3/css/calendar.css',
        'css/SuperAdmin-1-0-3/Template/1-0-3/css/font-awesome.min.css',
        'css/SuperAdmin-1-0-3/Template/1-0-3/css/form.css',
        'css/SuperAdmin-1-0-3/Template/1-0-3/css/generics.css',
        'css/SuperAdmin-1-0-3/Template/1-0-3/css/icons.css',
        'css/SuperAdmin-1-0-3/Template/1-0-3/css/style.css',
        '//localhost/local-cdn/bower_components/webui-popover/dist/jquery.webui-popover.min.css',
        '//localhost/local-cdn/bower_components/datatables/media/css/jquery.dataTables.min.css',
        '//localhost/local-cdn/bower_components/jquery-confirm2/css/jquery-confirm.css',
        '//localhost/local-cdn/bower_components/custombox/dist/custombox.min.css',
        '//localhost/local-cdn/vakata-jstree/dist/themes/default/style.min.css',
        '//localhost/local-cdn/font-awesome-animation.min.css',
        'css/DataTables/superadmin-dt.css',
        'css/override-style.css',
    ];
    
    public $js = [
        '//localhost/local-cdn/bower_components/datatables/media/js/jquery.dataTables.min.js',
        '//localhost/local-cdn/jquery.form.min.js',
        '//localhost/local-cdn/bower_components/jquery-confirm2/js/jquery-confirm.js',
        'css/SuperAdmin-1-0-3/Template/1-0-3/js/jquery.easing.1.3.js',
        'css/SuperAdmin-1-0-3/Template/1-0-3/js/jquery-ui.min.js',
        'css/SuperAdmin-1-0-3/Template/1-0-3/js/bootstrap.min.js',
        'css/SuperAdmin-1-0-3/Template/1-0-3/js/feeds.min.js',
        'css/SuperAdmin-1-0-3/Template/1-0-3/js/toggler.min.js',
        'css/SuperAdmin-1-0-3/Template/1-0-3/js/functions.js',
        'css/SuperAdmin-1-0-3/Template/1-0-3/js/scroll.min.js',
        '//localhost/local-cdn/bower_components/textillate/assets/jquery.lettering.js',
        '//localhost/local-cdn/bower_components/textillate/jquery.textillate.js',
        '//localhost/local-cdn/bower_components/webui-popover/dist/jquery.webui-popover.min.js',
        '//localhost/local-cdn/echarts.min.js',
        '//localhost/local-cdn/bower_components/bootstrap-growl/jquery.bootstrap-growl.min.js',
        '//localhost/local-cdn/vakata-jstree/dist/jstree.min.js',
        '//localhost/local-cdn/bower_components/custombox/dist/custombox.min.js',

        //'css/adminlte/bootstrap/js/bootstrap.min.js',
        // 'css/adminlte/plugins/morris/morris.min.js',
        // 'css/adminlte/plugins/jQueryUI/jquery-ui.min.js',
        // 'css/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        // 'css/adminlte/dist/js/app.js',
        // 'css/adminlte/dist/js/demo.js',
        // 'css/adminlte/dist/js/pages/dashboard.js',
        // 'js/jquery.magnific-popup.min.js',
        'js/datetimepicker/build/jquery.datetimepicker.full.min.js',
        // 'js/sts.js',
        //js/jFormValidator/form-validator/jquery.form-validator.min.js
        //js/mustache/mustache.min.js
        //js/siapatakut.js
        //'css/SuperAdmin-1-0-3/Template/1-0-3/js/jquery.min.js',
        
        // '//localhost/local-cdn/jquery.form.malshup.js',
        'js/sts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}