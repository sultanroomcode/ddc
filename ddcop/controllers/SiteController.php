<?php
namespace ddcop\controllers;

use Yii;
use ddcop\models\LoginForm;
use ddcop\models\Operator;
use ddcop\models\PasswordResetRequestForm;
use ddcop\models\ResetPasswordForm;
use ddcop\models\SignupForm;
use ddcop\models\AktivasiForm;
use ddcop\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'registrasi-thanks', 'registrasi'],
                'rules' => [
                    [
                        'actions' => ['signup', 'registrasi', 'registrasi-thanks'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'frontend';
        //return $this->render('index');
        return $this->render('index-uni-1');
    }

    public function actionIndexp()
    {
        return $this->render('index-provinsi');
    }

    //LAUNCHING ONLY
    public function actionPreLaunching()
    {
        return $this->render('pre-launching');
    }

    public function actionLaunching()
    {
        return $this->render('launching');
    }

    //FORCE LOGIN
    public function actionFl()
    {
        Yii::$app->user->login(User::findByUsername('35'), true? 3600 * 24 * 30 : 0);
        return $this->redirect(['/dashboard/i']);
    }
    //LAUNCHING ONLY

    public function actionPass($p)
    {
        echo Yii::$app->security->generatePasswordHash($p);
    }

    public function actionSetPass($email)
    {
        $modelc = Operator::findOne(['email' => $email]);

        if($modelc == null){
           return null;
        } else {
            $modelc->setPassword('default');
            if($modelc->save(false)) return 1; else return 0;
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin($p=null)
    {
    	if($p == null){
    		$p = 'posyandu';
    	}
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'frontend';
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                'page' => $p
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionMilestone()
    {
        return $this->render('milestone');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionRegistrasi($p)
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $this->redirect(['site/registrasi-thanks?p='.$p]);
            }
        }

        return $this->render('registrasi', [
            'model' => $model,
            'page' => $p
        ]);
    }

    public function actionPassKode($k, $p)
    {
        $m = Operator::findByEmail($k);
        $m->password_hash = Yii::$app->security->generatePasswordHash($p);
        $m->save();
    }

    public function actionRegistrasiThanks($p)
    {
        return $this->render('registrasi-thanks', [
            'page' => $p
        ]);
    }

    public function actionKonfirmasiEmail($kode=null, $email=null)
    {
        return $this->render('konfirmasi-email', [
            'email' => $email,
            'kode' => $kode
        ]);
    }

    public function actionReSendAktivasi($p)
    {
        $model = new AktivasiForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->resend()) {
                $this->redirect(['site/resend-page-back?p='.$p]);
            }
        }

        return $this->render('re-send-aktivasi', [
            'model' => $model,
            'page' => $p
        ]);
    }

    public function actionResendPageBack($p)
    {
        return $this->render('resend-page-back', [
            'page' => $p
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
