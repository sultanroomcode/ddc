<?php
namespace ddcop\controllers;

use Yii;

use sp3d\models\AESServer;
use sp3d\models\Log;
use sp3d\models\Credential;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use hscstudio\mimin\components\Mimin;

/**
 * Site controller
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => [''],
                'rules' => [
                    [
                        'actions' => ['index', 'i', 'akses-nama', 'menu', 'menu-utama'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init(){
        parent::init();
        Yii::$app->view->params['metatag'] =  [
            'sitename' => 'Siapatakut Server'
        ];
        $this->layout = 'admin';
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionExecuteLog($id)
    {
        $dekrip = new AESServer($text, 'enkripsi', 128);
        $text2 = $dekrip->decrypt();
        echo $text2;
    }

    public function actionIndex()
    {
        $data = Log::find()->all();
        
        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionAksesNama()
    {
        if(isset(Yii::$app->user->identity->detail->nama)){
            $r = [
                'id' =>  Yii::$app->user->identity->id,
                'nama' =>  Yii::$app->user->identity->detail->nama,
                'email' =>  Yii::$app->user->identity->email,
            ];
        } else {
            $r = [
                'id' =>  Yii::$app->user->identity->id,
                'nama' =>  'Belum Diisi',
                'email' =>  Yii::$app->user->identity->email,
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $r;
    }

    public function actionI()
    {
        $this->layout = 'superadmin';
        $data = Log::find()->all();
        
        return $this->render('super-index', [
            'data' => $data,
        ]);
    }

    public function actionMenu()
    {
        return $this->renderAjax('_'.Yii::$app->user->identity->viewRole().'/su-aside-extends');
    }

    public function actionProvinsi()
    {
        $data = Log::find()->all();
        $cred = Credential::findOne(['id' => 1]);
        
        //var_dump($aes->decrypt());
        //$res = Yii::$app->db->createCommand($aes->decrypt())->queryAll();
        
        return $this->render('provinsi', [
            'data' => $data,
        ]);
    }

    public function actionMenuUtama()
    {
        $url = '/';

        $foomix = $fooadd = [];
        if(Yii::$app->user->identity->tipe_operator == 'bumdes'){
            $fooadd = [
            	//Bumdes
            	['id' => 'm5', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/default/index?bumdes=mandiri\'})">Profile BUMDesa</a>'],

                /*['id' => 'm5.1', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/bumdes\'})">Bum Desa Mandiri</a>'],
                ['id' => 'm5.1.1', 'parent' => 'm5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/default/index?bumdes=mandiri\'})">Data Bum Desa Mandiri</a>'],
                ['id' => 'm5.2', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/bumdes\'})">Bum Desa Bersama</a>'],
                ['id' => 'm5.2.1', 'parent' => 'm5.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/default/index?bumdes=bersama\'})">Data Bum Desa Bersama</a>'],*/

             
                ['id' => 'm3', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/isi-data-pribadi\'})">Profile Operator</a>'],
                ['id' => 'm4', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/desa\'})">Lokasi BumDesa</a>'],

                ['id' => 'm7', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Klinik Bumdes</a>'],
                ['id' => 'm7.1', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=lihat-permintaan-kelas\'})">Daftar Kelas</a>'],
                ['id' => 'm7.2', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=kelas-ikuti\'})">Kelas Yang Diikuti</a>'],
                ['id' => 'm7.3', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=forum\'})">Forum Tanya Jawab Kelas</a>'],
                ['id' => 'm7.4', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=forum-umum\'})">Forum Tanya Jawab Umum</a>'],
                ['id' => 'm7.5', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=faq\'})">F.A.Q</a>'],

                ['id' => 'm8', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/notifikasi\'})">Pusat Notifikasi</a>'],
            ];
        }

        if(Yii::$app->user->identity->tipe_operator == 'bumdesma'){
            $fooadd = [
                //Bumdes
                ['id' => 'm5', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/default/index?bumdes=bersama\'})">Profile BUMDESMA</a>'],
             
                ['id' => 'm3', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/isi-data-pribadi\'})">Profile Operator</a>'],
                ['id' => 'm4', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/desa\'})">Lokasi BumDesMa</a>'],

                /*['id' => 'm7', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Klinik Bumdes</a>'],
                ['id' => 'm7.1', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=lihat-permintaan-kelas\'})">Daftar Kelas</a>'],
                ['id' => 'm7.2', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=kelas-ikuti\'})">Kelas Yang Diikuti</a>'],
                ['id' => 'm7.3', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=forum\'})">Forum Tanya Jawab Kelas</a>'],
                ['id' => 'm7.4', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=forum-umum\'})">Forum Tanya Jawab Umum</a>'],
                ['id' => 'm7.5', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/peserta/dashboard?page=faq\'})">F.A.Q</a>'],

                ['id' => 'm8', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/notifikasi\'})">Pusat Notifikasi</a>'],*/
            ];
        }

        if(Yii::$app->user->identity->tipe_operator == 'kpm'){
            $fooadd = [
                //KADER PEMBERDAYAAN MASYARAKAT
                ['id' => 'm1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/isi-data-pribadi\'})">Profile Operator</a>'],
                ['id' => 'm2', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/desa\'})">Lokasi Desa</a>'],
                ['id' => 'm6', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/pendamping-desa\'})">Kader Pemberdayaan Masyarakat</a>'],
                ['id' => 'm6.1', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kpm\'})">Data Isian Kader</a>'],
                ['id' => 'm7', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/notifikasi\'})">Pusat Notifikasi</a>'],
            ];
        }

        if(Yii::$app->user->identity->tipe_operator == 'posyandu'){
            $fooadd = [
                //Posyandu
                ['id' => 'm4.5', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu\'})">Profile Posyandu</a>'],
                ['id' => 'm4.5.1', 'parent' => 'm4.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/\'})">Master Posyandu</a>'],
                ['id' => 'm4.5.2', 'parent' => 'm4.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-isian\'})">Isian Posyandu</a>'],
                ['id' => 'm4.5.3', 'parent' => 'm4.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-kegiatan\'})">Kegiatan Posyandu</a>'],
                ['id' => 'm4.5.4', 'parent' => 'm4.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-data\'})">Data Posyandu</a>'],
                ['id' => 'm4.5.5', 'parent' => 'm4.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-sarana\'})">Sarana Posyandu</a>'],


                ['id' => 'm1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/isi-data-pribadi\'})">Profile Operator</a>'],
                ['id' => 'm2', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operator/operator/desa\'})">Lokasi Posyandu</a>'],
                ['id' => 'm5', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/notifikasi\'})">Pusat Notifikasi</a>'],
            ];
        }

        // $foor = array_merge($foo, $foomix);
        Yii::$app->response->format = Response::FORMAT_JSON;
        // return $mod->all();
        $foomix = array_merge($fooadd);
        sort($foomix);
        return $foomix;
    }
}
