<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

if($params['offline']) {
    $db_access = [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ddc',
            'username' => 'taroom',
            'password' => '1234',
            'charset' => 'utf8',
        ];
} else {
    $db_access = [
            // 'class' => 'yii\db\Connection',
            // 'dsn' => 'mysql:host=localhost;dbname=mediatub_sp3d',
            // 'username' => 'mediatub_cli',
            // 'password' => 'XUIJOy70sH7di5pvVG',
            // 'charset' => 'utf8',

            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ddc',
            'username' => 'taroom',
            'password' => '1234',
            'charset' => 'utf8',
        ];
}

return [
    'id' => 'ddcop',
    'language' => 'id-ID',
    'name' => 'ddcop',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'ddcop\controllers',
    'as access' => [
         'class' => '\hscstudio\mimin\components\AccessControl',
         'allowActions' => [
            // add wildcard allowed action here!, data yang bisa di akses umum
            '*',
            'mimin/*'
            /*'site/*',
            'data-umum-desa/*',
            'data-check/*',
            'data-umum/*',
            'labs/*',
            'debug/*',
            'gii/*',
            , */// only in dev mode
        ],
    ],
    'components' => [
        'db' => $db_access,
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'bundles' => [
                // 'yii\web\JqueryAsset' => [
                //     'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                // ],
            ],
        ],
        'user' => [
            'identityClass' => 'ddcop\models\Operator',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
    'modules' => [
        'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        'transaksi' => [
            'class' => 'sp3d\modules\transaksi\Transaksi',
        ],
        'referensi' => [
            'class' => 'sp3d\modules\referensi\Referensi',
        ],
        'umum' => [
            'class' => 'sp3d\modules\umum\Umum',
        ],
        'pendamping' => [
            'class' => 'sp3d\modules\pendamping\Pendamping',
        ],
        'sp3ddashboard' => [
            'class' => 'sp3d\modules\sp3ddashboard\Sp3dDashboard',
        ],
        'posyandu' => [
            'class' => 'sp3d\modules\posyandu\Posyandu',
        ],
        'bumdes' => [
            'class' => 'sp3d\modules\bumdes\Bumdes',
        ],
        'kpm' => [
            'class' => 'sp3d\modules\kpm\Kpm',
        ],
        'kelas' => [
            'class' => 'sp3d\modules\kelas\Kelas',
        ],
        'operator' => [
            'class' => 'ddcop\modules\operator\Operator',
        ],
    ],    
    'params' => $params,
];