<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use lomdeskel\assets\OnlineSuperAdminAsset;
use lomdeskel\assets\SuperAdminAsset;
use common\widgets\Alert;

if(Yii::$app->params['offline']){
  SuperAdminAsset::register($this);
} else {
  OnlineSuperAdminAsset::register($this);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
	<link rel="icon" href="../favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body id="skin-cloth" data-url="<?=Url::base(true)?>">
<div class="wrapper">
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</div>
</body>
</html>
<?php $this->endPage() ?>
