<?php 
use yii\helpers\Url;
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= Url::to('img/person-2.jpg') ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=Yii::$app->user->identity->username?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form 
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <?php if(Yii::$app->user->can('admin')){ echo $this->render('_admin/aside-extends'); } ?>
        <?php if(Yii::$app->user->can('desa')){ echo $this->render('_desa/aside-extends'); } ?>
        <?php if(Yii::$app->user->can('kecamatan')){ echo $this->render('_kecamatan/aside-extends'); } ?>
        <?php if(Yii::$app->user->can('dpmdkb')){ echo $this->render('_dpmdkb/aside-extends'); } ?>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/data-umum/patch'})"><i class="fa fa-clock-o"></i> <span>patch</span></a></li>
        <li><a href="<?= Url::to(['site/logout']) ?>"><i class="fa fa-sign-out"></i> <span>Keluar</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>