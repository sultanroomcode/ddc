<?php
/* @var $this yii\web\View */
$this->title = 'Lomdeskel | Dashboard';
$bgback = 'skin-polygon';
$directoryIcon = Yii::$app->params['directoryIcon'];
?>

<header id="header" class="media">
            <a href="" id="sp3d-indicator">
              <div id="sp3d-indicator-cube" class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
              </div>
            </a> <!--  goLoad({elm:'#su-aside-menu', url:'/dashboard/menu'}); -->
            <a class="logo pull-left" href="javascript:void(0)" onclick="refreshTree();">LOMDESKEL</a> 
            
            <div class="media-body">
                <div class="media" id="top-menu">
                    <!-- <div class="pull-left tm-icon">
                        <a data-drawer="messages" class="drawer-toggle" href="">
                            <i class="sa-top-message"></i>
                            <i class="n-count animated">1</i>
                            <span>Pesan</span>
                        </a>
                    </div>
                    <div class="pull-left tm-icon">
                        <a data-drawer="notifications" class="drawer-toggle" href="">
                            <i class="sa-top-updates"></i>
                            <i class="n-count animated">1</i>
                            <span>Info</span>
                        </a>
                    </div>   -->                  

                    <div id="time" class="pull-right">
                        <span id="hours"></span>
                        :
                        <span id="min"></span>
                        :
                        <span id="sec"></span>
                    </div>
                    
                    <div class="media-body">
                        <input type="text" class="main-search">
                    </div>
                </div>
            </div>
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">    
                <!-- Side Menu -->
                <?= $this->render('_super-aside2', [
                    'directoryIcon' => $directoryIcon,
                ]) ?>
                <?= $this->render('_super-aside', [
                    'directoryIcon' => $directoryIcon
                ]) ?>
            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
                <?= $this->render('_super-drawer', [
                    'directoryIcon' => $directoryIcon
                ]) ?>
                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li><a href="javascript:void(0)">Beranda</a></li>
                    <li class="active">PUSDADESA</li>
                </ol>
                
                <h4 class="page-title">DASHBOARD <span id="notifikasi-area"></span></h4>
               
                <div class="block-area" id="main-container"></div>
                
                <?php /* $this->render('_super-chat', [
                    'directoryIcon' => $directoryIcon
                ]) */ ?>
            </section>
        </section>

<div id="modal-custom" style="display: none;"><div id="form-engine"><!-- ddcop/superindex --></div></div>
<?php 
$user = Yii::$app->user->identity->id;
$usertype = Yii::$app->user->identity->id;
$userphoto = Yii::$app->user->identity->id.'/'.Yii::$app->user->identity->id.'.jpg';
$url_jstree = '/dashboard/menu-utama';
$scriptJs = <<<JS
    var user = '{$user}';
    var usertype = '{$usertype}';
    $('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
    $('body').attr('id', '{$bgback}');

    function myNotif(){
      goLoad({elm:'#notifikasi-area', url:'/umum/notifikasi/get-notifikasi'});
    }

    setInterval(myNotif,20000);

    // $.getJSON(base_url+'/operator-detail/profile', function(res) {
    //     if(res != ''){
    //         $('.profile-pic').attr('src', res.foto_profile);
    //     }
    // });

    $.getJSON(base_url+'/dashboard/akses-nama', function(res) {
        if(res != ''){
            $('#akses-nama-lengkap').text(res.nama);
            $('#akses-nama-email').text(res.email);
        }
    });
JS;

$this->registerJs($scriptJs);

if(Yii::$app->params['offline']){
  // $this->registerCssFile('//localhost/local-cdn/bower_components/sweetalert/dist/sweetalert.css');
  // $this->registerCssFile('//localhost/local-cdn/DataTables/datatables.min.css');
  // $this->registerCssFile('//localhost/local-cdn/bower_components/animate.css/animate.min.css');
  // $this->registerCssFile('//localhost/local-cdn/vakata-jstree/dist/themes/default/style.min.css');
  // $this->registerCssFile('//localhost/local-cdn/bower_components/custombox/dist/custombox.min.css');
  // $this->registerCssFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css');

  // $this->registerJsFile('//localhost/local-cdn/DataTables/datatables.min.js');
  // $this->registerJsFile('//localhost/local-cdn/bower_components/sweetalert/dist/sweetalert.min.js');
  // $this->registerJsFile('//localhost/local-cdn/jquery.bootstrap-growl.min.js');
  // $this->registerJsFile('//localhost/local-cdn/vakata-jstree/dist/jstree.min.js');
  // $this->registerJsFile('//localhost/local-cdn/bower_components/custombox/dist/custombox.min.js');
  // $this->registerJsFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js');
  // $this->registerJsFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/js/i18n/defaults-id_ID.min.js');
  // $this->registerJsFile('//localhost/local-cdn/echarts.min.js');
  // $this->registerJsFile('//localhost/local-cdn/jquery.form.min.js');  
  $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css');
  $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

} else {
  // $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css');
  // $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/dataTables.bootstrap.min.css');
  // $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css');
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.4/themes/default/style.min.css');
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/custombox/4.0.3/custombox.min.css');
  // $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css');

  // $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/jquery.dataTables.min.js');
  // $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js');
  // $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.4/jstree.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/custombox/4.0.3/custombox.min.js');
  $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css');
  $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
  // $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js');
  // $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-id_ID.min.js');
  // $this->registerJsFile('http://files.mediatuban.com/echarts.min.js');
  // $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js');
}
