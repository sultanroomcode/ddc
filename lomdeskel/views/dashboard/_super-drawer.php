<!-- Messages Drawer -->
<div id="messages" class="tile drawer animated">
    <div class="listview narrow">
        <div class="media">
            <a href="javascript:void(0)">Kirim Pesan Baru</a>
            <a href="javascript:void(0)" onclick="goLoad({elm:'#tempat-pesan', url:'/umum/user/pesan-list'})">Refresh Pesan</a>
            <span class="drawer-close">&times;</span>
            
        </div>
        <div class="overflow" id="tempat-pesan" style="height: 254px">
            <!-- akan terisi pesan -->
        </div>
        <div class="media text-center whiter l-100">
            <a href="javascript:void(0)" onclick="goLoad({url:'/umum/user/pesan'})"><small>VIEW ALL</small></a>
        </div>
    </div>
</div>

<!-- Notification Drawer -->
<div id="notifications" class="tile drawer animated">
    <div class="listview narrow">
        <div class="media">
            <a href="">Notification Settings</a>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="overflow" style="height: 254px">
            <div class="media">
                <div class="pull-left">
                    <img width="40" src="<?= $directoryIcon ?>img/profile-pics/1.jpg" alt="">
                </div>
                <div class="media-body">
                    <small class="text-muted">Admin - 2 Jam Lalu</small><br>
                    <a class="t-overflow" href="">Selamat Datang Akun Desa</a>
                </div>
            </div>
        </div>
        <div class="media text-center whiter l-100">
            <a href="javascript:void(0)" onclick="goLoad({url:'/umum/user/info'})"><small>VIEW ALL</small></a>
        </div>
    </div>
</div>