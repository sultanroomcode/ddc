<?php
use yii\helpers\Url;
$kd = Yii::$app->user->identity->chapter->siskeu_id;
?>
<li class="header">NAVIGASI DESA</li>
<li class="active"><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa-page?id=<?=$kd?>&tahun=<?=date('Y')?>'})"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
<li class="header">REFERENSI</li>
<!-- <li><a href="javascript:void(0)" onclick="goLoad({url: '/data-umum/import-file'})"><i class="fa fa-file"></i>Import File</a></li> -->
<li><a href="javascript:void(0)" onclick="goLoad({url: '/data-umum/laporan?kd_desa=<?=$kd?>'})"><i class="fa fa-file"></i> <span>Laporan &amp; Grafik</span></a></li>
<li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-list'})"><i class="fa fa-database"></i> <span>Import Siskeudes</span></a></li>
<li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/excell-list'})"><i class="fa fa-file-excel-o"></i> <span>Import Excel</span></a></li>
<li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/api-sync'})"><i class="fa fa-exchange"></i> <span>Sync API</span></a></li>