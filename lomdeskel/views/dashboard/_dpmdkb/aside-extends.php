<?php
use frontend\models\SideMenu;

$arr = [
    ['link' => 'referensi/ref-bank-desa', 'label' => 'Bank Desa'],
    ['link' => 'referensi/ref-belanja-operasional', 'label' => 'Belanja Operasional'],
    ['link' => 'referensi/ref-bidang', 'label' => 'Bidang'],
    ['link' => 'referensi/ref-bunga', 'label' => 'Bunga'],
    ['link' => 'referensi/ref-desa', 'label' => 'Desa'],
    ['link' => 'referensi/ref-kecamatan', 'label' => 'Desa'],
    ['link' => 'referensi/ref-kololari', 'label' => 'Kololari'],
    ['link' => 'referensi/ref-neraca-close', 'label' => 'Neraca Close'],
    ['link' => 'referensi/ref-perangkat', 'label' => 'Perangkat'],
    ['link' => 'referensi/ref-potongan', 'label' => 'Potongan'],
    ['link' => 'referensi/ref-sbu', 'label' => 'SBU'],
    ['link' => 'referensi/ref-sumber', 'label' => 'Sumber']
];

$menu = new SideMenu();
$menu->setAllLinks($arr);
?>
<li class="header">NAVIGASI DESA</li>
<li class="active"><a href="#"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
<li class="header">REFERENSI</li>
<li class="treeview">
    <a href="javascript:void(0)">
        <i class="fa fa-table"></i> <span>Data Referensi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <small class="hidden-xs label pull-right bg-green">new</small>
        </span>
    </a>
    <ul class="treeview-menu">
        <?= implode(' ', $menu->generateAjaxLinks()); ?>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Sumber</span>
            <span class="pull-right-container">
                <small class="label pull-right bg-red">3</small>
                <small class="label pull-right bg-blue">17</small>
            </span>
        </a></li>
    </ul>
</li>
<li class="header">TRANSAKSI</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-table"></i> <span>Data Transaksi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <small class="label pull-right bg-green hidden-xs">new</small>
        </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Anggaran
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/ta-anggaran/index'})"><i class="fa fa-circle-o"></i> Utama</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/ta-anggaran-log/index'})"><i class="fa fa-circle-o"></i> Log</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/ta-anggaran-rincian/index'})"><i class="fa fa-circle-o"></i> Rincian</a></li>
            </ul>
        </li>
        
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Bidang</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Desa</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Jurnal Umum</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Jurnal Umum Rinci</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Kegiatan</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Mutasi</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Pajak</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Pajak Rinci</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>Pemda</span></a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o text-red"></i><span>RAB</span></a></li>
        <li>
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> RAB
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/ta-rab/index'})"><i class="fa fa-circle-o"></i> Utama</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/ta-rab-sub/index'})"><i class="fa fa-circle-o"></i> Sub</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/ta-rab-rincian/index'})"><i class="fa fa-circle-o"></i> Rincian</a></li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> RPJM
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Bidang</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Kegiatan</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Misi</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Pagu Indikatif</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Sasaran</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Tujuan</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Visi</a></li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> SPJ
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Utama</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Bukti</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Potongan</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Rincian</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Sisa</a></li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> SPP
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Utama</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Bukti</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Potongan</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Rincian</a></li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> STS
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Utama</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Rincian</a></li>
            </ul>
        </li>

        <li>
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Test
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Bidang</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Visi</a></li>
                <li>
                <a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Level Two
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Level Three</a></li>
                <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
                </li>
            </ul>
        </li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> Saldo Awal</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> SPJ</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url:''})"><i class="fa fa-circle-o"></i> SPP</a></li>
    </ul>
</li>