<?php
use yii\helpers\Url;
?>
<section class="bar background-white no-mb">
    <div class="container">

        <div class="col-md-12">
            <div class="heading text-center">
                <h2>Berita DPMDKB</h2>
            </div>

            <p class="lead">Berbagai informasi terbaru terkait Dinas Pemberdayaan Masyarakat dan Desa dan Keluarga Berencana, Sistem Informasi Aplikasi Pengelolaan Keuangan Transparan, Akuntabel dan Partisipatif (SIAPATAKUT). <span class="accent">Lihat Berita!</span>
            </p>

            <!-- *** BLOG HOMEPAGE ***
_________________________________________________________ -->

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="box-image-text blog">
                        <div class="top">
                            <div class="image">
                                <img src="<?= Url::to(['/'], false) ?>img/3siapatakut.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="bg"></div>
                            <div class="text">
                                <p class="buttons">
                                    <a href="blog-post.html" target="_blank" class="btn btn-template-transparent-primary"><i class="fa fa-link"></i> Lebih Lanjut</a>
                                </p>
                            </div>
                        </div>
                        <div class="content">
                            <h4><a href="blog-post.html" target="_blank">SiapaTakut</a></h4>
                            <p class="author-category">By <a href="#">Admin</a> di <a href="blog.html">Informasi</a>
                            </p>
                            <p class="intro">Sarana Penyebarluasan informasi terkait pengelolaan keuangan desa, sumberdaya dan potensi desa serta hasil pembangunan yang berasal dari program yang masuk ke desa serta sebagai sarana pengawasan terkait program pembangunan desa oleh masyarakat</p>
                            <p class="read-more"><a href="blog-post.html" class="btn btn-template-main">Lanjutkan membaca..</a>
                            </p>
                        </div>
                    </div>
                    <!-- /.box-image-text -->

                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="box-image-text blog">
                        <div class="top">
                            <div class="image">
                                <img src="<?= Url::to(['/'], false) ?>img/1-prodeskel.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="bg"></div>
                            <div class="text">
                                <p class="buttons">
                                    <a href="http://prodeskel.binapemdes.kemendagri.go.id" target="_blank" class="btn btn-template-transparent-primary"><i class="fa fa-link"></i> Lebih Lanjut</a>
                                </p>
                            </div>
                        </div>
                        <div class="content">
                            <h4><a href="http://prodeskel.binapemdes.kemendagri.go.id">Prodeskel</a></h4>
                            <p class="author-category">By <a href="#">Admin</a> di <a href="blog.html">Informasi</a>
                            </p>
                            <p class="intro">merupakan sistem informasi (aplikasi) berbasis Web (Online) dimana pedoman penyusunan dan pendayagunaan datanya berlandaskan pada Peraturan Menteri Dalam Negeri Nomor 12 Tahun 2007 (Permendagri 12/2007).</p>
                            <p class="read-more"><a href="http://prodeskel.binapemdes.kemendagri.go.id" target="_blank" class="btn btn-template-main">Lanjutkan membaca..</a>
                            </p>
                        </div>
                    </div>
                    <!-- /.box-image-text -->

                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="box-image-text blog">
                        <div class="top">
                            <div class="image">
                                <img src="<?= Url::to(['/'], false) ?>img/2tubankab.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="bg"></div>
                            <div class="text">
                                <p class="buttons">
                                    <a href="blog-post.html" target="_blank" class="btn btn-template-transparent-primary"><i class="fa fa-link"></i> Lebih Lanjut</a>
                                </p>
                            </div>
                        </div>
                        <div class="content">
                            <h4><a href="blog-post.html">Tuban Kab</a></h4>
                            <p class="author-category">By <a href="#">Admin</a> di <a href="blog.html">Informasi</a>
                            </p>
                            <p class="intro">Situs resmi Pemerintahan Kabupaten Tuban.</p>
                            <p class="read-more"><a href="blog-post.html" class="btn btn-template-main">Lanjutkan membaca..</a>
                            </p>
                        </div>
                    </div>
                    <!-- /.box-image-text -->

                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box-image-text blog">
                        <div class="top">
                            <div class="image">
                                <img src="<?= Url::to(['/'], false) ?>img/4siskeudes.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="bg"></div>
                            <div class="text">
                                <p class="buttons">
                                    <a href="blog-post.html" target="_blank" class="btn btn-template-transparent-primary"><i class="fa fa-link"></i> Lebih Lanjut</a>
                                </p>
                            </div>
                        </div>
                        <div class="content">
                            <h4><a href="blog-post.html">Siskeudes</a></h4>
                            <p class="author-category">By <a href="#">Admin</a> di <a href="blog.html">Informasi</a>
                            </p>
                            <p class="intro">Aplikasi Sistem Keuangan Desa (SISKEUDES) merupakan aplikasi yang dikembangkan Badan Pengawasan Keuangan dan Pembangunan (BPKP) dalam rangka meningkatkan kualitas tata kelola keuangan desa.</p>
                            <p class="read-more"><a href="blog-post.html" target="_blank" class="btn btn-template-main">Lanjutkan membaca..</a>
                            </p>
                        </div>
                    </div>
                    <!-- /.box-image-text -->

                </div>

            </div>
            <!-- /.row -->

            <!-- *** BLOG HOMEPAGE END *** -->

        </div>

    </div>
    <!-- /.container -->
</section>
<!-- /.bar -->