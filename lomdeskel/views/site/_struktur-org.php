<section class="bar background-pentagon no-mb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading text-center">
                    <h2>STRUKTUR ORGANISASI<br>DINAS PEMBERDAYAAN MASYARAKAT DAN DESA DAN KB</h2>
                </div>

                <p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean
                    ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="team-member" data-animate="fadeInUp">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-1.jpg" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Imamudin</a></h3>
                    <p class="role">Kepala Bappemas</p>
                    <div class="social">
                        <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                    </div>
                    <div class="text">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    </div>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-3 col-sm-3" data-animate="fadeInUp">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-2.jpg" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Susiyati</a></h3>
                    <p class="role">Sekretaris</p>

                    <div class="social">
                        <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                    </div>
                    <div class="text">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    </div>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-3 col-sm-3" data-animate="fadeInUp">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-3.png" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Princess Leia</a></h3>
                    <p class="role">Team Leader</p>
                    <div class="social">
                        <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                    </div>
                    <div class="text">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    </div>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-3 col-sm-3" data-animate="fadeInUp">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-4.jpg" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Jabba Hut</a></h3>
                    <p class="role">Lead Developer</p>
                    <div class="social">
                        <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                    </div>
                    <div class="text">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    </div>
                </div>
                <!-- /.team-member -->
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-2 col-sm-3">
                <div class="team-member" data-animate="fadeInDown">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-1.jpg" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Han Solo</a></h3>
                    <p class="role">Founder</p>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-2 col-sm-3" data-animate="fadeInDown">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-2.jpg" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Luke Skywalker</a></h3>
                    <p class="role">CTO</p>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-2 col-sm-3" data-animate="fadeInDown">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-3.png" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Princess Leia</a></h3>
                    <p class="role">Team Leader</p>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-2 col-sm-3" data-animate="fadeInDown">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-4.jpg" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Jabba Hut</a></h3>
                    <p class="role">Lead Developer</p>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-2 col-sm-3" data-animate="fadeInDown">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-2.jpg" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Franz Kafka</a></h3>
                    <p class="role">CTO</p>
                </div>
                <!-- /.team-member -->
            </div>
            <div class="col-md-2 col-sm-3" data-animate="fadeInDown">
                <div class="team-member">
                    <div class="image">
                        <a href="team-member.html">
                            <img src="img/person-3.png" alt="" class="img-responsive img-circle">
                        </a>
                    </div>
                    <h3><a href="team-member.html">Gregor Samsa</a></h3>
                    <p class="role">CTO</p>
                </div>
                <!-- /.team-member -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>