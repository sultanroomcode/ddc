<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Terhubung lebih dekat</h4>
                    <address>
            <strong>Dispemas Jatim</strong><br>
             Alamat<br>
             Lengkap </address>
                    <p>
                        <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
                        <i class="icon-envelope-alt"></i> admin@dispemas.jatimprov.go.id
                    </p>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Informasi</h4>
                    <ul class="link-list">
                        <li><a href="#">Kabar Berita</a></li>
                        <li><a href="#">Ketentuan Penggunaan</a></li>
                        <li><a href="#">Kebijakan Privasi</a></li>
                        <li><a href="#">Karir</a></li>
                        <li><a href="#">Kontak Kami</a></li>
                    </ul>
                </div>

            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Halaman</h4>
                    <ul class="link-list">
                        <li><a href="#">Kabar Berita</a></li>
                        <li><a href="#">Ketentuan Penggunaan</a></li>
                        <li><a href="#">Kebijakan Privasi</a></li>
                        <li><a href="#">Karir</a></li>
                        <li><a href="#">Kontak Kami</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Surat Kabar</h4>
                    <p>Masukkan alamat surat elektronik (E-Mail) Anda disini untuk mendapatkan kabar terbaru</p>
                    <div class="form-group multiple-form-group input-group">
                        <input type="email" name="email" class="form-control">
                        <span class="input-group-btn">
                    <button type="button" class="btn btn-theme btn-add">Subscribe</button>
                </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            <!--
            All the links in the footer should remain intact. 
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Sailor
          -->
                            <a href="https://bootstrapmade.com/bootstrap-business-templates/">Bootstrap Business Templates</a> dari <a href="https://bootstrapmade.com/">BootstrapMade</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="social-network">
                        <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</footer>