<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Operator Data Desa Center | Provinsi Jawa Timur';
?>

<div id="demo">
    <div class="top-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo animated fadeInLeft">
                        <img class="img-responsive" src="imgf/SP3D.png">
                    </div>

                    <div class="triggbtn button animated fadeInRight">
                        
                        <?php if(Yii::$app->user->isGuest): ?>
                        <a href="<?= Url::to(['site/login?p=posyandu'], true) ?>" class="btn btn-register">L o g i n</a>
                        <?php else: ?>
                        <a href="<?= Url::to(['/dashboard/i'], true) ?>" class="btn btn-register">Dashboard</a>
                        <a href="<?= Url::to(['/site/logout'], true) ?>" class="btn btn-register">Keluar</a>
                        <?php endif; ?>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>

  <div class="banner-info">
    <div class="wrapper">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <?php if(!Yii::$app->user->isGuest): ?>
                  <div class="col-md-12 txtin">
                    <marquee direction="" onmouseover="this.stop();" onmouseout="this.start();">
                  <span class="txtwelcome" data-out-effect="fadeOut" data-out-shuffle="true" data-loop="true" data-in-effect="rollIn">Selamat Datang <?= Yii::$app->user->identity->email ?></span></marquee>
                  </div>

                  <?php endif; ?>
                  
                  <div class="text-center wow fadeIn delay-05s">
                    <img class="bnr-logo wow fadeInDown" src="imgf/logo.png" data-wow-delay="0.5s" class="img-responsive" alt="Logo Provinsi">
                    <h2 class="bnr-title wow bounceIn" data-wow-delay="0.9s">Selamat Datang</h2>
                    <h1 class="bnr-sub-title wow fadeInUp" data-wow-delay="1.6s">Operator Data Desa Center</h1>
                  </div>
                  
                </div>
              </div>
          </div>
      </div>
  </div>

  <div class="iconjatim">
      <img src="imgf/surabaya.png" class="icon">
    </div>
    
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <p class="bnr-para wow fadeInUp" data-wow-delay="1s">Data Desa Center adalah himpunan data Desa di Jawa Timur (7.724 Desa, 602 Kec, 29 Kab dan 1 Kota Batu), yang berisi folder data : (1) Data Perencanaan Desa/APBDes, (2) Data Potensi Desa, (3) Data Lembaga Desa, (4) Data Lembaga Kemasyarakatan Desa, (5) Data Posyandu, (6) Data Lembaga Adat Desa, (7) Data BUMDes, (8) Data KPM Desa, (9) Data Aset Desa, dan lainnya. Data Desa Center sebagai aplikasi penghumpunan data desa guna pengamanan data untuk transparansi dan akuntabilitas percepatan pembangunan dan pemberdayaan masyarakat di Jawa Timur.</p>             
        </div>
      </div>
    </div>

    <div class="select bg-white wow fadeInDown" data-wow-delay="0.5s">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <form id="data-provinsi-form">
                    <!-- tahun -->
                    <select name="tahun-anggaran" id="tahun-anggaran">
                      <option value="2018" selected="selected">2018</option>
                      <option value="2017">2017</option>
                      <option value="2016">2016</option>
                    </select>
                    <!-- kabupaten -->
                    <select name="kabupaten-input" id="kabupaten-input">
                      <option value="" selected="selected">Semua</option>
                    </select>
                    <!-- kecamatan -->
                    <select name="kecamatan-input" id="kecamatan-input">
                      <option value="" selected="selected">Semua</option>
                    </select>
                    <!-- desa -->
                    <select name="desa-input" id="desa-input">
                      <option value="" selected="selected">Semua</option>
                    </select>
                    <br>
                    <!-- <input name="change" class="button"  style="vertical-align:middle" value="Kirim" type="submit"> -->
                      <button class="button" style="vertical-align:middle"><span>Proses </span></button>
                  </form>
                </div>
            </div>
        </div>
    </div>

    <div class="informasi bg-gray">
      <div class="container">
        <div class="row">
         <!--  <div class="col-md-2 col-sm-4 offset-md-1">
            <div class="wow fadeInUp" data-wow-delay="0.1s">
              <div class="box center">
                <i class="fa fa-money fa-3x circled bg-green"></i>
                <h4 class="h-bold" id="count-dana-desa">0</h4>
                <p>
                  Dana Desa
                </p>
              </div>
            </div>
          </div> -->
          <div class="col-md-3 col-sm-4">
            <div class="wow fadeInUp" data-wow-delay="0.3s">
              <div class="box center">
                <i class="fa fa-list-alt fa-3x circled bg-green"></i>
                <h4 class="h-bold" id="count-pendapatan">0</h4>
                <p>
                  Perencanaan Pendapatan
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-4">
            <div class="wow fadeInUp" data-wow-delay="0.5s">
              <div class="box center">
                <i class="fa fa-shopping-cart fa-3x circled bg-green"></i>
                <h4 class="h-bold" id="count-belanja">0</h4>
                <p>
                  Perencanaan Pembelanjaan
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="wow fadeInUp" data-wow-delay="0.7s">
              <div class="box center">
                <i class="fa fa-calculator fa-3x circled bg-green"></i>
                <h4 class="h-bold" id="count-pembiayaan">0</h4>
                <p>
                  Perencanaan Pembiayaan
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="wow fadeInUp" data-wow-delay="1s">
              <div class="box center">
                <a href="javascript:void(0)" id="modal-view-desa"><i class="fa fa-home fa-3x circled bg-green"></i></a>
                <h4 class="h-bold" id="count-desa-input">0</h4>
                <p>
                  Desa Sudah Entri
                </p>
              </div>
            </div>
          </div>     
        </div>
      </div>
    </div>    

    <div class="content bg-white" id="data-area-container">
      <div class="container">
        <div class="row">
            <div class="col-md-12" id="data-area"></div>
        </div>
      </div>
    </div>

    <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <p class="wow fadeInUp" data-wow-delay="0.2s"><b>DINAS PEMBERDAYAAN MASYARAKAT DAN DESA PROVINSI JAWA TIMUR</b></p>
                    <p class="wow fadeInUp" data-wow-delay="0.4s">JL. AHMAD YANI NO. 152C Telp. (031) 8292591</p>
                    <p class="wow fadeInUp" data-wow-delay="0.6s">Surabaya Jawa Timur </p>  
                    <p class="wow fadeInUp" data-wow-delay="1s">Copyright &copy; 2018 Data Desa Center</p>         
                </div>
            </div>
        </div>
    </div>  
</div>
<?php
$tahuntahun = '';
$script =<<<JS
function getDataProvinsi(tahun)
{
    $.getJSON(base_url+'/data-umum/data-count-provinsi?tahun='+tahun, function(res) {
        if(res != ''){
            $('#count-dana-desa').html(res.dana_desa);
            $('#count-pendapatan').html(res.pendapatan);
            $('#count-belanja').html(res.belanja);
            $('#count-pembiayaan').html(res.pembiayaan);
            $('#count-desa-input').html(res.desa_input);
        }
    });   
}

$(window).scroll(function() {
    var wScroll = $(this).scrollTop();
    if (wScroll > 200){
    
        $('.triggbtn').css(
        {
            'display' : 'none'
        })

    } else {

        $('.triggbtn').css(
        {
            'display' : 'block'
        })
    }
});

new WOW().init();
$('.data').DataTable({
  "language": {
      "decimal": ",",
      "thousands": "."
  }
});

$('form#data-provinsi-form').submit(function(){
    var forminput = $(this).serialize();
    //$('div#form-area').html(forminput);
    $('#data-area-container').show(1000);
    $.ajax({url:base_url+'/data-umum/data-table-provinsi', data:forminput, success:function(res){
        $('#data-area').html(res);
    }});
    return false;
});

$('#tahun-anggaran').change(function(){
  var tahun = $(this).val();
  getDataProvinsi(tahun);
  $('#kabupaten-input, #kecamatan-input, #desa-input').html('<option value="">Semua</option>');
  goLoad({elm:'#kabupaten-input', url:'/data-umum/data-option?tahun='+tahun});
});

$('#kabupaten-input').change(function(){
  var kode = $(this).val();
  var tahun = $('#tahun-anggaran').val();
  $('#kecamatan-input, #desa-input').html('<option value="">Semua</option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan-input', url:'/data-umum/data-option?tahun='+tahun+'&type=kecamatan&kode='+kode});
  }
});

$('#kecamatan-input').change(function(){
  var kode = $(this).val();
  var tahun = $('#tahun-anggaran').val();
  $('#desa-input').html('<option value="">Semua</option>');
  if(kode !== ''){
    goLoad({elm:'#desa-input', url:'/data-umum/data-option?tahun='+tahun+'&type=desa&kode='+kode});
  }
});

getDataProvinsi($('#tahun-anggaran').val());

$('#modal-view-desa').click(function(e){
   $('#data-area-container').show(1000);
   var tahun = $('#tahun-anggaran').val();
    $.ajax({url:base_url+'/data-umum/data-user-import', data:{tahun: tahun}, success:function(res){
        $('#data-area').html(res);
    }});
    return false;
});
JS;

$this->registerCssFile('@web/css/site-extend.css');
$this->registerJs($script);
// $this->registerJsFile('css/DataTables/datatables.min.js');