<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Sistem Perencanaan dan Pengendalian Keuangan Desa';
?>
<div id="all">
    <?= $this->render('_header-login') ?>
    <?= $this->render('_carousel') ?>
    <?= $this->render('_progress') ?>
    <?php //$this->render('_struktur-org') ?>    

    <section class="bar background-image-fixed-2 no-mb color-white text-center">
        <div class="dark-mask"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if(Yii::$app->user->isGuest): ?>
                        <div class="icon icon-lg"><i class="fa fa-sign-in"></i>
                        </div>
                        <h3 class="text-uppercase">Butuh Informasi Lebih?</h3>
                        <p class="lead">Data lebih rinci telah disiapkan ketika anda login.</p>
                        <p class="text-center">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#sp-sv-login" class="btn btn-template-transparent-black btn-lg"><i class="fa fa-sign-in"></i> Login</a>
                        </p>
                    <?php else: ?>
                        <div class="icon icon-lg"><i class="fa fa-dashboard"></i>
                        </div>
                        <h3 class="text-uppercase">Anda telah Login !</h3>
                        <p class="lead">Data lebih rinci telah disiapkan ketika anda klik dashboard.</p>
                        <p class="text-center">                            
                            <a href="<?= Url::to(['/dashboard']) ?>" class="btn btn-template-transparent-black btn-lg"><i class="fa fa-dashboard"></i> <span class="hidden-xs text-uppercase">DASHBOARD</span></a>
                            
                        </p>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </section>

    <?= $this->render('_berita') ?>
    <?= $this->render('_foot-section') ?>    
</div>
<!-- /#all -->