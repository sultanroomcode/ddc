<?php
namespace lomdeskel\controllers;

use Yii;

use sp3d\models\AESServer;
use sp3d\models\Credential;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use hscstudio\mimin\components\Mimin;

/**
 * Site controller
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => [''],
                'rules' => [
                    [
                        'actions' => ['index', 'i', 'akses-nama', 'menu', 'menu-utama'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init(){
        parent::init();
        Yii::$app->view->params['metatag'] =  [
            'sitename' => 'Siapatakut Server'
        ];
        $this->layout = 'admin';
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionExecuteLog($id)
    {
        $dekrip = new AESServer($text, 'enkripsi', 128);
        $text2 = $dekrip->decrypt();
        echo $text2;
    }

    public function actionIndex()
    {
        $data = Log::find()->all();
        
        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionAksesNama()
    {
        if(isset(Yii::$app->user->identity->detail->nama)){
            $r = [
                'id' =>  Yii::$app->user->identity->id,
                'nama' =>  Yii::$app->user->identity->detail->nama,
                'email' =>  Yii::$app->user->identity->email,
            ];
        } else {
            $r = [
                'id' =>  Yii::$app->user->identity->id,
                'nama' =>  'Belum Diisi',
                'email' =>  Yii::$app->user->identity->email,
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $r;
    }

    public function actionI()
    {
        $this->layout = 'superadmin';
        $data = null;
        
        return $this->render('super-index', [
            'data' => $data,
        ]);
    }

    public function actionMenu()
    {
        return $this->renderAjax('_'.Yii::$app->user->identity->viewRole().'/su-aside-extends');
    }

    public function actionProvinsi()
    {
        $data = Log::find()->all();
        $cred = Credential::findOne(['id' => 1]);
        
        //var_dump($aes->decrypt());
        //$res = Yii::$app->db->createCommand($aes->decrypt())->queryAll();
        
        return $this->render('provinsi', [
            'data' => $data,
        ]);
    }

    public function actionMenuUtama()
    {
        $url = '/';

        $foomix = $fooadd = [];
        $foomix = [
            //Bumdes
            ['id' => 'm1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/userarea/user-profile/view\'})">Profile Pengguna</a>'],
        ];

        if(Yii::$app->user->identity->profile != null){
            $foomix[] = ['id' => 'm2', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/contest/contest/view\'})">Lomba Desa/Kelurahan</a>'];
            $foomix[] = ['id' => 'm3', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/contest/default/index\'})">Alur Penggunaan Aplikasi</a>'];
        }
        // $foor = array_merge($foo, $foomix);
        Yii::$app->response->format = Response::FORMAT_JSON;
        // return $mod->all();
        $foomix = array_merge($foomix);
        sort($foomix);
        return $foomix;
    }
}
