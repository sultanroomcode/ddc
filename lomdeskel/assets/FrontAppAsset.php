<?php
namespace lomdeskel\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        '//localhost/local-cdn/bower_components/textillate/assets/animate.css',
        '//localhost/local-cdn/bower_components/tether/dist/css/tether.min.css',
        'css/bootstrap/css/bootstrap.min.css',
        'css/font-awesome/css/font-awesome.min.css',
        '//localhost/local-cdn/bower_components/datatables/media/css/dataTables.bootstrap4.min.css',
        '//localhost/local-cdn/bower_components/datatables/media/css/jquery.dataTables.min.css',
        'css/animate.css',
        'css/style.css',
    ];
   
    public $js = [
        // 'css/DataTables/jQuery-3.2.1/jquery-3.2.1.min.js',
        '//localhost/local-cdn/bower_components/datatables/media/js/dataTables.bootstrap4.min.js',
        '//localhost/local-cdn/bower_components/textillate/assets/jquery.lettering.js',
        '//localhost/local-cdn/bower_components/textillate/jquery.textillate.js',
        '//localhost/local-cdn/bower_components/datatables/media/js/jquery.dataTables.min.js',
        '//localhost/local-cdn/bower_components/tether/dist/js/tether.min.js',
        'css/bootstrap/js/bootstrap.min.js',
        '//localhost/local-cdn/echarts.min.js',
        'js/wow.min.js',
        'js/sts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
