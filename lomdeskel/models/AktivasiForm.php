<?php
namespace lomdeskel\models;

use yii\base\Model;
use Yii;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
/**
 * Signup form
 */
class AktivasiForm extends Model
{
    public $email;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'exist', 'targetClass' => '\lomdeskel\models\Operator', 'message' => 'email tidak ditemukan.'],

            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function resend()
    {
        if ($this->validate()) {
            $user = Operator::findOne(['email' => $this->email]);

            // send email 
            $ch = curl_init();
            $url = "http://artechindonesia.co.id/mailjet/mailing.php?email=".$user->email."&kode=".$user->id;
            // 'site/konfirmasi-email?kode='.md5($user->id).'&email='.$this->email;
            curl_setopt($ch, CURLOPT_URL, $url);
            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // $output contains the output string
            $outputsql = curl_exec($ch);
            curl_close($ch);

            return true;
        } else {
            return false;
        }
    }
}

