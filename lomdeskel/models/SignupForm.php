<?php
namespace lomdeskel\models;

use yii\base\Model;
use Yii;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\lomdeskel\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $uuidgen = Uuid::uuid4();
            $user->id = $uuidgen->getHex();
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            // $user->status = $user->konfirmasi_email = $user->konfirmasi_desa = 0;
            $user->status = 10;
            $user->konfirmasi_email = 10;
            $user->konfirmasi_desa = 10;
            if ($user->save()) {
                //send email
                $ch = curl_init();
                $url = "http://artechindonesia.co.id/mailjet/mailing.php?email=".$user->email."&kode=".$user->id;
                // 'site/konfirmasi-email?kode='.md5($user->id).'&email='.$this->email;
                curl_setopt($ch, CURLOPT_URL, $url);
                //return the transfer as a string
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                // $output contains the output string
                $outputsql = curl_exec($ch);
                curl_close($ch);
                return $user;
            }
        }

        return null;
    }
}

