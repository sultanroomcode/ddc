<?php
namespace lomdeskel\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\modules\umum\models\DdcNotifikasi;
use sp3d\models\User;
/**
 * This is the model class for table "operator_desa".
 *
 * @property string $id
 * @property string $kd_desa
 * @property integer $status
 * @property string $propose_at
 * @property string $confirm_at
 */
class OperatorDesa extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $kd_kecamatan, $kd_kabupaten, $email;
    public static function tableName()
    {
        return 'operator_desa';
    }

    public function behaviors()
    {
        return ['timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['propose_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['confirm_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kd_desa'], 'required'],
            [['status'], 'integer'],
            [['id'], 'string', 'max' => 32],
            [['kd_desa'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_desa' => 'Kd Desa',
            'status' => 'Status',
            'propose_at' => 'Propose At',
            'confirm_at' => 'Confirm At',
        ];
    }
    //tools
    public function showStatus(){
        switch ($this->status) {
            case 5:
                $a = 'Permintaan Ditolak';
            break;
            case 10:
                $a = 'Permintaan Diterima';
            break;
            
            default:
                $a = 'Permintaan Belum Dibaca';
            break;
        }

        return $a;
    }
    //event
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            $this->status = 10;
            return true;
        } else {
            return false;
        }
    } 
    
    public function afterSave($ins, $old)
    {
        if($ins){
            $m = new DdcNotifikasi;
            $m->n_from = Yii::$app->user->identity->id;
            $m->n_to = $this->kd_desa;
            $m->message = 'Operator desa membutuhkan tindakan persetujuan Anda';
            $m->open_url = '/operatora/desa-akses-operator/konfirmasi-desa-index';
            $m->status_read = 'S';//send
            $m->save();
        }

        if(!$ins){
            $m = new DdcNotifikasi;
            $m->n_from = Yii::$app->user->identity->id;
            $m->n_to = $this->id;
            $m->message = 'Desa '.(($this->status == 5)?'Menolak':'Menyetujui').' Permintaan Anda';
            $m->open_url = '/operator/operator/desa';
            $m->status_read = 'S';//send
            $m->save();
        }     
    }

    //relation
    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa']);
    }

    public function getOperator()//for kecamatan up..
    {
        return $this->hasOne(Operator::className(), ['id' => 'id']);
    }

    public function getOperatorDetail()//for kecamatan up..
    {
        return $this->hasOne(OperatorDetail::className(), ['id' => 'id']);
    }
}
