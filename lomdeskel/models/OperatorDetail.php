<?php

namespace lomdeskel\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "operator_detail".
 *
 * @property string $id
 * @property string $kd_desa
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $gender
 * @property string $pendidikan
 * @property string $no_hp
 * @property string $keterangan
 * @property integer $created_at
 * @property integer $updated_at
 */
class OperatorDetail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operator_detail';
    }

    public function behaviors()
    {
        return ['timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 // 'value' => new \yii\db\Expression('NOW()'),
             ],];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            // [['created_at', 'updated_at'], 'integer'],
            [['no_hp'], 'string', 'max' => 20],
            [['id'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 150],
            [['tempat_lahir', 'tanggal_lahir', 'keterangan'], 'string', 'max' => 255],
            [['gender', 'pendidikan'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_desa' => 'Kd Desa',
            'nama' => 'Nama',
            'no_hp' => 'Nomor HP Aktif',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'gender' => 'Gender',
            'pendidikan' => 'Pendidikan',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    //tools
    public function genderext()
    {
        return $this->gender == 'L'?'Laki-Laki':'Perempuan';
    }

    public function getAkun()//for kecamatan up..
    {
        return $this->hasOne(Operator::className(), ['id' => 'id']);
    }
}
