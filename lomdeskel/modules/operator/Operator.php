<?php

namespace lomdeskel\modules\operator;

/**
 * operator module definition class
 */
class Operator extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'lomdeskel\modules\operator\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
