<?php
namespace lomdeskel\modules\operator\controllers;

use yii\web\Controller;
use lomdeskel\models\OperatorDetail;
use lomdeskel\models\Operator;
use lomdeskel\models\OperatorDesa;
use Yii;
/**
 * Halaman ini khusus desa saja
 */
class DesaAksesOperatorController extends Controller
{
    public function actionKonfirmasiDesaIndex()
    {
        $modelc = OperatorDesa::find()->where(['kd_desa' => Yii::$app->user->identity->id]);

        return $this->renderAjax('konfirmasi-desa-index', [
            'model' => $modelc,
        ]);
    }

    public function actionKonfirmasiDesa($iduser)
    {
    	$modelc = OperatorDesa::findOne(['kd_desa' => Yii::$app->user->identity->id, 'id' => $iduser]);

        if($modelc == null){
            //do what?
        } else {
            //update
            if ($modelc->load(Yii::$app->request->post())) {
                if($modelc->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('konfirmasi-desa', [
                    'model' => $modelc,
                ]);
            }
        }
        
    }
}
