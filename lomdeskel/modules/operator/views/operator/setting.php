<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">SETTING OPERATOR</h2>
            <div class="p-10">
                <div class="listview list-click">                                        
                    <h2 class="page-title">Setting Operator</h2>
                    
                    <div class="p-15">
                        <p>
                            Email : <?= $model->email ?><br>
                            Dibuat Tanggal : <?= date('d-F-Y H:i', $model->created_at) ?><br>
                            Diupdate Tanggal : <?= date('d-F-Y H:i', $model->updated_at) ?><br>

                            <?php $form = ActiveForm::begin($arrFormConfig); ?>
                            <div class="row">
                                <div class="col-md-8">
                                    <?= $form->field($model, 'password_baru')->passwordInput(['maxlength' => true]) ?>
                                    
                                    <div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-7">
                                            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </p>
                    </div>
                    
                    <hr class="whiter">
                    
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            console.log('Succed');
            $.alert('Berhasil mengupdate kata sandi');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);