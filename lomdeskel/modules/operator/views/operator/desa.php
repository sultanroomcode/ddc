<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Desa</h2>
            <div class="p-10" id="operator-area">
                <?php
                if($model != null){
                    if($model->status == 0){
                        echo "Menunggu Persetujuan ".$model->desa->description;
                    } else if($model->status == 10){
                        echo $model->desa->id.'<br>';
                        echo $model->desa->description;
                    }
                } else {
                    echo '<a href="javascript:void(0)" onclick="goLoad({url: \'/operator/operator/pilih-desa\'})" class="btn btn-default">Ganti Lokasi Desa</a>';
                }
                ?>
            </div>
        </div>
    </div>
</div>