<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">DASHBOARD</h2>
            <div class="p-10">
                <div class="listview list-click">                                        
                    <h2 class="page-title">Biodata Operator</h2>
                    
                    <div class="p-15">
                        <p>
                            Email : <?= $model->akun->email ?><br>
                            Nama : <?= $model->nama ?><br>
                            Tempat, Tanggal Lahir : <?= $model->tempat_lahir.', '.date('d-F-Y', strtotime($model->tanggal_lahir)) ?><br>
                            Jenis Kelamin : <?= $model->genderext() ?><br>
                            Pendidikan Terakhir : <?= substr($model->pendidikan, 2) ?><br>
                            Dibuat Tanggal : <?= date('d-F-Y H:i', $model->created_at) ?><br>
                            Diupdate Tanggal : <?= date('d-F-Y H:i', $model->updated_at) ?><br>
                        </p>
                    </div>
                    
                    <hr class="whiter">
                    
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>