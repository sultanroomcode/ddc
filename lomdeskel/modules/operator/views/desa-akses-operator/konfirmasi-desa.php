<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Konfirmasi Operator Desa</h2>
            <div class="p-10" id="operator-area">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
                <div class="row">
                    <div class="col-md-4">
                    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'kd_desa')->hiddenInput()->label(false) ?>
                    Nama : <?= ($model->operatorDetail != null)?$model->operatorDetail->nama:'Belum isi biodata' ?><br>
                    Email : <?= $model->operator->email ?><br>
                     
                    <?= $form->field($model, 'status')->dropdownList([5 => 'Tolak Permintaan', 10 => 'Setujui Permintaan'])->label('Konfirmasi') ?>
                    
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Konfirmasi', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                    </div>
                </div>
                <?php ActiveForm::end();  ?>
            </div>
        </div>
    </div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            console.log('Succed');
            goLoad({url:'/operatora/desa-akses-operator/konfirmasi-desa-index'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);