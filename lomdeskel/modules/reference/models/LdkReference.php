<?php

namespace lomdeskel\modules\reference\models;

use Yii;

/**
 * This is the model class for table "ldk_reference".
 *
 * @property string $r_id
 * @property int $tr_id
 * @property string $r_name
 * @property string $r_root
 * @property string $r_url
 * @property string $r_status
 * @property int $r_created_at
 * @property int $r_updated_at
 */
class LdkReference extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_reference';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_id', 'tr_id', 'r_name', 'r_root', 'r_url', 'r_status', 'r_created_at', 'r_updated_at'], 'required'],
            [['tr_id', 'r_created_at', 'r_updated_at'], 'integer'],
            [['r_id', 'r_root'], 'string', 'max' => 50],
            [['r_name', 'r_url'], 'string', 'max' => 150],
            [['r_status'], 'string', 'max' => 10],
            [['r_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'r_id' => 'R ID',
            'tr_id' => 'Tr ID',
            'r_name' => 'R Name',
            'r_root' => 'R Root',
            'r_url' => 'R Url',
            'r_status' => 'R Status',
            'r_created_at' => 'R Created At',
            'r_updated_at' => 'R Updated At',
        ];
    }
}
