<?php

namespace lomdeskel\modules\reference\models;

use Yii;

/**
 * This is the model class for table "ldk_reference_answer".
 *
 * @property int $ra_id
 * @property string $r_id
 * @property string $ra_answer
 * @property int $ra_point
 * @property string $ra_default_answer
 * @property string $ra_attachment
 * @property string $ra_extends_description
 * @property int $ra_created_at
 * @property int $ra_updated_at
 */
class LdkReferenceAnswer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_reference_answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ra_answer', 'ra_point', 'ra_default_answer', 'ra_attachment', 'ra_created_at', 'ra_updated_at'], 'required'],
            [['ra_point', 'ra_created_at', 'ra_updated_at'], 'integer'],
            [['ra_extends_description'], 'string'],
            [['r_id'], 'string', 'max' => 50],
            [['ra_answer'], 'string', 'max' => 150],
            [['ra_default_answer', 'ra_attachment'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ra_id' => 'Ra ID',
            'r_id' => 'R ID',
            'ra_answer' => 'Ra Answer',
            'ra_point' => 'Ra Point',
            'ra_default_answer' => 'Ra Default Answer',
            'ra_attachment' => 'Ra Attachment',
            'ra_extends_description' => 'Ra Extends Description',
            'ra_created_at' => 'Ra Created At',
            'ra_updated_at' => 'Ra Updated At',
        ];
    }
}
