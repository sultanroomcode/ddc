<?php

namespace lomdeskel\modules\reference\models;

use Yii;

/**
 * This is the model class for table "ldk_ref_type".
 *
 * @property int $tr_id
 * @property string $tr_name
 * @property string $tr_description
 * @property string $tr_status
 * @property int $tr_created_at
 * @property int $tr_updated_at
 */
class LdkRefType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_ref_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tr_name', 'tr_description', 'tr_status', 'tr_created_at', 'tr_updated_at'], 'required'],
            [['tr_created_at', 'tr_updated_at'], 'integer'],
            [['tr_name'], 'string', 'max' => 100],
            [['tr_description'], 'string', 'max' => 150],
            [['tr_status'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tr_id' => 'Tr ID',
            'tr_name' => 'Tr Name',
            'tr_description' => 'Tr Description',
            'tr_status' => 'Tr Status',
            'tr_created_at' => 'Tr Created At',
            'tr_updated_at' => 'Tr Updated At',
        ];
    }
}
