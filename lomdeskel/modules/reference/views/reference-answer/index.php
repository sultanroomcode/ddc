<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ldk Reference Answers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-reference-answer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ldk Reference Answer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ra_id',
            'r_id',
            'ra_answer',
            'ra_point',
            'ra_default_answer',
            //'ra_attachment',
            //'ra_extends_description:ntext',
            //'ra_created_at',
            //'ra_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
