<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReferenceAnswer */

$this->title = 'Create Ldk Reference Answer';
$this->params['breadcrumbs'][] = ['label' => 'Ldk Reference Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-reference-answer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
