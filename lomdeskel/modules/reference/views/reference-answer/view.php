<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReferenceAnswer */

$this->title = $model->ra_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Reference Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-reference-answer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ra_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ra_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ra_id',
            'r_id',
            'ra_answer',
            'ra_point',
            'ra_default_answer',
            'ra_attachment',
            'ra_extends_description:ntext',
            'ra_created_at',
            'ra_updated_at',
        ],
    ]) ?>

</div>
