<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReferenceAnswer */

$this->title = 'Update Ldk Reference Answer: ' . $model->ra_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Reference Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ra_id, 'url' => ['view', 'id' => $model->ra_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-reference-answer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
