<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReferenceAnswer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-reference-answer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ra_answer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ra_point')->textInput() ?>

    <?= $form->field($model, 'ra_default_answer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ra_attachment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ra_extends_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ra_created_at')->textInput() ?>

    <?= $form->field($model, 'ra_updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
