<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReference */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-reference-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tr_id')->textInput() ?>

    <?= $form->field($model, 'r_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'r_root')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'r_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'r_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'r_created_at')->textInput() ?>

    <?= $form->field($model, 'r_updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
