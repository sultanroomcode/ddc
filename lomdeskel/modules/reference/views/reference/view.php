<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReference */

$this->title = $model->r_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk References', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-reference-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->r_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->r_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'r_id',
            'tr_id',
            'r_name',
            'r_root',
            'r_url:url',
            'r_status',
            'r_created_at',
            'r_updated_at',
        ],
    ]) ?>

</div>
