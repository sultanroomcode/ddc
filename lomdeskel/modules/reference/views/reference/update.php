<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReference */

$this->title = 'Update Ldk Reference: ' . $model->r_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk References', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_id, 'url' => ['view', 'id' => $model->r_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-reference-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
