<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkReference */

$this->title = 'Create Ldk Reference';
$this->params['breadcrumbs'][] = ['label' => 'Ldk References', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-reference-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
