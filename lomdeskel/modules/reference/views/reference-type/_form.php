<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkRefType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-ref-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tr_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tr_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tr_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tr_created_at')->textInput() ?>

    <?= $form->field($model, 'tr_updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
