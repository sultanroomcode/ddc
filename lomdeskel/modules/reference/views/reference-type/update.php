<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkRefType */

$this->title = 'Update Ldk Ref Type: ' . $model->tr_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Ref Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tr_id, 'url' => ['view', 'id' => $model->tr_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-ref-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
