<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\reference\models\LdkRefType */

$this->title = $model->tr_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Ref Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-ref-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tr_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->tr_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tr_id',
            'tr_name',
            'tr_description',
            'tr_status',
            'tr_created_at',
            'tr_updated_at',
        ],
    ]) ?>

</div>
