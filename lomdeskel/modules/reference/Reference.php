<?php

namespace lomdeskel\modules\reference;

/**
 * reference module definition class
 */
class Reference extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'lomdeskel\modules\reference\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
