<?php

namespace lomdeskel\modules\userarea\models;

use Yii;

/**
 * This is the model class for table "ldk_user_ticket".
 *
 * @property string $ut_id
 * @property int $co_id
 * @property int $u_id
 * @property string $ut_as
 * @property string $kb_id
 * @property string $kc_id
 * @property string $ds_id
 * @property string $ut_status
 * @property int $ut_created_at
 * @property int $ut_verified_at
 * @property int $ut_verified_by
 */
class LdkUserTicket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_user_ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ut_id', 'co_id', 'u_id', 'ut_as', 'kb_id', 'kc_id', 'ds_id', 'ut_status', 'ut_created_at'], 'required'],
            [['co_id', 'u_id', 'ut_created_at', 'ut_verified_at', 'ut_verified_by'], 'integer'],
            [['ut_id'], 'string', 'max' => 32],
            [['ut_as'], 'string', 'max' => 20],
            [['kb_id'], 'string', 'max' => 4],
            [['kc_id'], 'string', 'max' => 7],
            [['ds_id'], 'string', 'max' => 10],
            [['ut_status'], 'string', 'max' => 15],
            [['ut_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ut_id' => 'Ut ID',
            'co_id' => 'Co ID',
            'u_id' => 'U ID',
            'ut_as' => 'Ut As',
            'kb_id' => 'Kb ID',
            'kc_id' => 'Kc ID',
            'ds_id' => 'Ds ID',
            'ut_status' => 'Ut Status',
            'ut_created_at' => 'Ut Created At',
            'ut_verified_at' => 'Ut Verified At',
            'ut_verified_by' => 'Ut Verified By',
        ];
    }
}
