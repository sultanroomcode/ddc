<?php
namespace lomdeskel\modules\userarea\models;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "user_profile".
 *
 * @property int $u_id
 * @property string $up_fullname
 * @property string $up_address
 * @property string $up_born_place
 * @property string $up_born_date
 * @property string $up_image
 * @property string $up_base_kb_id
 * @property string $up_base_kc_id
 * @property string $up_base_ds_id
 * @property int $up_created_at
 * @property int $up_updated_at
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    public function behaviors()
    {
         return [
            //$component->detachBehavior('blame'); jika digunakan di ddc
            /*'blame' => [ 
                 [
                     'class' => BlameableBehavior::className(),
                     'createdByAttribute' => 'created_by',
                     'updatedByAttribute' => 'updated_by',
                 ],
            ],*/
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['up_created_at', 'up_updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['up_updated_at'],
                ],
                // 'value' => new \yii\db\Expression('NOW()'),
            ],
         ];
    }

    public function rules()
    {
        return [
            [['u_id', 'up_base_kb_id', 'up_base_kc_id', 'up_base_ds_id'], 'required'],
            [['u_id', 'up_created_at', 'up_updated_at'], 'integer'],
            [['up_born_date'], 'safe'],
            [['up_fullname', 'up_address'], 'string', 'max' => 150],
            [['up_born_place', 'up_image'], 'string', 'max' => 100],
            [['up_base_kb_id'], 'string', 'max' => 4],
            [['up_base_kc_id'], 'string', 'max' => 7],
            [['up_base_ds_id'], 'string', 'max' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'u_id' => 'U ID',
            'up_fullname' => 'Nama Lengkap',
            'up_address' => 'Alamat',
            'up_born_place' => 'Tempat Lahir',
            'up_born_date' => 'Tanggal Lahir',
            'up_image' => 'Foto',
            'up_base_kb_id' => 'Kabupaten',
            'up_base_kc_id' => 'Kecamatan',
            'up_base_ds_id' => 'Desa/Kelurahan',
            'up_created_at' => 'Dibuat Pada',
            'up_updated_at' => 'Diupdate Pada',
        ];
    }

    public function dataKabupaten()
    {
        return ArrayHelper::map(Wilayah::find()->where(['type' => 'kabupaten'])->all(), 'id', 'description');
    }
}
