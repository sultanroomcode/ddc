<?php

namespace lomdeskel\modules\userarea\models;

use Yii;

/**
 * This is the model class for table "wilayah".
 *
 * @property string $id
 * @property string $type
 * @property string $username
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 */
class Wilayah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wilayah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'username', 'description', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['id', 'type'], 'string', 'max' => 15],
            [['username', 'description'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'username' => 'Username',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
