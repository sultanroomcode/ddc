<?php

namespace lomdeskel\modules\userarea;

/**
 * userarea module definition class
 */
class UserArea extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'lomdeskel\modules\userarea\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
