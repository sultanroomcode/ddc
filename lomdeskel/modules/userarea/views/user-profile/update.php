<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\userarea\models\UserProfile */

$this->title = 'Update User Profile: ' . $model->up_fullname;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->u_id, 'url' => ['view', 'id' => $model->u_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
