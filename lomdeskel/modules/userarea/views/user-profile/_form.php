<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];
/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\userarea\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */

$model->u_id =Yii::$app->user->identity->id;
$urlback = 'goLoad({url:"/userarea/user-profile/view"});';
?>

<div class="user-profile-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'u_id')->textInput() ?>

    <?= $form->field($model, 'up_fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'up_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'up_born_place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'up_born_date')->textInput() ?>

    <?= $form->field($model, 'up_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'up_base_kb_id')->dropdownList($model->dataKabupaten()) ?>

    <?= $form->field($model, 'up_base_kc_id')->dropdownList(['' => '']) ?>

    <?= $form->field($model, 'up_base_ds_id')->dropdownList(['' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
goLoad({elm:'#userprofile-up_base_kc_id', url:'/userarea/user-profile/data-option?type=kecamatan&kode='+ $('#userprofile-up_base_kb_id').val()});
$('#userprofile-up_base_kb_id').change(function(){
  var kode = $(this).val();
  $('#userprofile-up_base_kc_id, #userprofile-up_base_ds_id').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#userprofile-up_base_kc_id', url:'/userarea/user-profile/data-option?type=kecamatan&kode='+kode});
  }
});

$('#userprofile-up_base_kc_id').change(function(){
  var kode = $(this).val();
  $('#userprofile-up_base_ds_id').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#userprofile-up_base_ds_id', url:'/userarea/user-profile/data-option?type=desa&kode='+kode});
  }
});

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);