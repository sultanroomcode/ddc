<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\userarea\models\UserProfile */

$this->title = $model->up_fullname;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'/userarea/user-profile/update'})" class="btn btn-danger">Update</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'u_id',
            'up_fullname',
            'up_address',
            'up_born_place',
            'up_born_date',
            'up_image',
            'up_base_kb_id',
            'up_base_kc_id',
            'up_base_ds_id',
            'up_created_at',
            'up_updated_at',
        ],
    ]) ?>

</div>
