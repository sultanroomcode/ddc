<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ldk User Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-user-ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ldk User Ticket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ut_id',
            'co_id',
            'u_id',
            'ut_as',
            'kb_id',
            //'kc_id',
            //'ds_id',
            //'ut_status',
            //'ut_created_at',
            //'ut_verified_at',
            //'ut_verified_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
