<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\userarea\models\LdkUserTicket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-user-ticket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ut_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'co_id')->textInput() ?>

    <?= $form->field($model, 'u_id')->textInput() ?>

    <?= $form->field($model, 'ut_as')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kb_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kc_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ds_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ut_status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
