<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\userarea\models\LdkUserTicket */

$this->title = $model->ut_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk User Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-user-ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ut_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ut_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ut_id',
            'co_id',
            'u_id',
            'ut_as',
            'kb_id',
            'kc_id',
            'ds_id',
            'ut_status',
            'ut_created_at',
            'ut_verified_at',
            'ut_verified_by',
        ],
    ]) ?>

</div>
