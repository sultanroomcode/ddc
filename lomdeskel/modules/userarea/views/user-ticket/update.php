<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\userarea\models\LdkUserTicket */

$this->title = 'Update Ldk User Ticket: ' . $model->ut_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk User Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ut_id, 'url' => ['view', 'id' => $model->ut_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-user-ticket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
