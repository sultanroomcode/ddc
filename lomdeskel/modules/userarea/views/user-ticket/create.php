<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\userarea\models\LdkUserTicket */

$this->title = 'Create Ldk User Ticket';
$this->params['breadcrumbs'][] = ['label' => 'Ldk User Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-user-ticket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
