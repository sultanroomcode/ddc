<?php

namespace lomdeskel\modules\contest;

/**
 * contest module definition class
 */
class Contest extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'lomdeskel\modules\contest\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
