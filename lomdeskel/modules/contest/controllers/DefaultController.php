<?php
namespace lomdeskel\modules\contest\controllers;

use yii\web\Controller;

/**
 * Default controller for the `contest` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }
}
