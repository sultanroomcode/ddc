<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\contest\models\LdkContest */

$this->title = 'Create Ldk Contest';
$this->params['breadcrumbs'][] = ['label' => 'Ldk Contests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-contest-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
