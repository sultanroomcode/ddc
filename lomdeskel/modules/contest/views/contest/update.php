<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\contest\models\LdkContest */

$this->title = 'Update Ldk Contest: ' . $model->co_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Contests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->co_id, 'url' => ['view', 'id' => $model->co_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-contest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
