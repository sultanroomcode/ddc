<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\contest\models\LdkContest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-contest-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'co_year_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'co_year_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'co_expired_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'co_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'co_last_day')->textInput() ?>

    <?= $form->field($model, 'co_show_day')->textInput() ?>

    <?= $form->field($model, 'co_level')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'co_created_at')->textInput() ?>

    <?= $form->field($model, 'co_updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
