<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ldk Contests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-contest-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ldk Contest', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'co_id',
            'co_year_1',
            'co_year_2',
            'co_expired_status',
            'co_description:ntext',
            //'co_last_day',
            //'co_show_day',
            //'co_level',
            //'co_created_at',
            //'co_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
