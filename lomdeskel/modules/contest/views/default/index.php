<div class="contest-default-index">
    <h1>Alur Penggunaan Aplikasi</h1>
    <p>
        
    step :
    - panitia membuat kontes (adalah user kecamatan, kabupaten, provinsi di ddc) <br>
    - baik juri dan peserta (dihitung user) registrasi melalui email (email konfirmas akan dikirimkan ke email masing-masing) [fix]<br>
    - setelah terkonfirmasi maka user akan bisa login ke sistem [fix] <br>
    - setelah masuk ke sistem user mengupdate profil diri [fix] dan lokasi desa yang diampu [fix] (pemilihan ini hanya 1 kali) <br>
    -- juri juga memilih basis desa (namun hanya terbatas pada wilayah di jawa timur) (masih ?) <br>
    - user memilih lomba/kontes yang diinginkan (lomba yang tampil di desa tergantung pada scope lomba) <br>
    - user kontestan akan lanjut dengan mengisi pengajuan, pengajuan detail, attachment <br>
    - user juri akan menunggu pengajuan yang ada dalam lomba lalu menilai apakah yang dikirimkan benar/tidak juga memberikan nilai <br>
    - juri dan kontestan akan bisa melihat hasil akhir ketika hari pengumuman kontes berakhir <br>
    - panitia akan menutup kontes
    </p>

</div>
