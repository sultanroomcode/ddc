<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\contest\models\LdkPoint */

$this->title = $model->p_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-point-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->p_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->p_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'p_id',
            'sd_id',
            's_id',
            'co_id',
            'ut_id_contestant',
            'ut_id_judges',
            'u_id_judges',
            'p_value',
            'p_correct',
            'p_description:ntext',
            'p_created_at',
            'p_updated_at',
        ],
    ]) ?>

</div>
