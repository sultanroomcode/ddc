<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\contest\models\LdkPoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-point-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sd_id')->textInput() ?>

    <?= $form->field($model, 's_id')->textInput() ?>

    <?= $form->field($model, 'co_id')->textInput() ?>

    <?= $form->field($model, 'ut_id_contestant')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ut_id_judges')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_id_judges')->textInput() ?>

    <?= $form->field($model, 'p_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'p_correct')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'p_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'p_created_at')->textInput() ?>

    <?= $form->field($model, 'p_updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
