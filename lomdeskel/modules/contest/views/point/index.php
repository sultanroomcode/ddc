<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ldk Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-point-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ldk Point', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'p_id',
            'sd_id',
            's_id',
            'co_id',
            'ut_id_contestant',
            //'ut_id_judges',
            //'u_id_judges',
            //'p_value',
            //'p_correct',
            //'p_description:ntext',
            //'p_created_at',
            //'p_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
