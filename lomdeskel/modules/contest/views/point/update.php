<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\contest\models\LdkPoint */

$this->title = 'Update Ldk Point: ' . $model->p_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->p_id, 'url' => ['view', 'id' => $model->p_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-point-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
