<?php

namespace lomdeskel\modules\contest\models;

use Yii;

/**
 * This is the model class for table "ldk_point".
 *
 * @property int $p_id
 * @property int $sd_id
 * @property int $s_id
 * @property int $co_id
 * @property string $ut_id_contestant
 * @property string $ut_id_judges
 * @property int $u_id_judges
 * @property string $p_value
 * @property string $p_correct
 * @property string $p_description
 * @property int $p_created_at
 * @property int $p_updated_at
 */
class LdkPoint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_point';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sd_id', 's_id', 'co_id', 'ut_id_contestant', 'ut_id_judges', 'u_id_judges', 'p_created_at', 'p_updated_at'], 'required'],
            [['sd_id', 's_id', 'co_id', 'u_id_judges', 'p_created_at', 'p_updated_at'], 'integer'],
            [['p_value'], 'number'],
            [['p_description'], 'string'],
            [['ut_id_contestant', 'ut_id_judges'], 'string', 'max' => 32],
            [['p_correct'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'p_id' => 'P ID',
            'sd_id' => 'Sd ID',
            's_id' => 'S ID',
            'co_id' => 'Co ID',
            'ut_id_contestant' => 'Ut Id Contestant',
            'ut_id_judges' => 'Ut Id Judges',
            'u_id_judges' => 'U Id Judges',
            'p_value' => 'P Value',
            'p_correct' => 'P Correct',
            'p_description' => 'P Description',
            'p_created_at' => 'P Created At',
            'p_updated_at' => 'P Updated At',
        ];
    }
}
