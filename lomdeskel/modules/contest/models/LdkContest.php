<?php

namespace lomdeskel\modules\contest\models;

use Yii;

/**
 * This is the model class for table "ldk_contest".
 *
 * @property int $co_id
 * @property string $co_year_1
 * @property string $co_year_2
 * @property string $co_expired_status
 * @property string $co_description
 * @property string $co_last_day
 * @property string $co_show_day
 * @property string $co_level
 * @property int $co_created_at
 * @property int $co_updated_at
 */
class LdkContest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_contest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['co_description'], 'string'],
            [['co_last_day', 'co_show_day'], 'safe'],
            [['co_created_at', 'co_updated_at'], 'required'],
            [['co_created_at', 'co_updated_at'], 'integer'],
            [['co_year_1', 'co_year_2'], 'string', 'max' => 4],
            [['co_expired_status'], 'string', 'max' => 10],
            [['co_level'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'co_id' => 'Co ID',
            'co_year_1' => 'Co Year 1',
            'co_year_2' => 'Co Year 2',
            'co_expired_status' => 'Co Expired Status',
            'co_description' => 'Co Description',
            'co_last_day' => 'Co Last Day',
            'co_show_day' => 'Co Show Day',
            'co_level' => 'Co Level',
            'co_created_at' => 'Co Created At',
            'co_updated_at' => 'Co Updated At',
        ];
    }
}
