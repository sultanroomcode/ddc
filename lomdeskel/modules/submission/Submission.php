<?php

namespace lomdeskel\modules\submission;

/**
 * submission module definition class
 */
class Submission extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'lomdeskel\modules\submission\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
