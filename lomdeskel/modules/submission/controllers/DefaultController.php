<?php

namespace lomdeskel\modules\submission\controllers;

use yii\web\Controller;

/**
 * Default controller for the `submission` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }
}
