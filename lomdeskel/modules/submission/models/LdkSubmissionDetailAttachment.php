<?php

namespace lomdeskel\modules\submission\models;

use Yii;

/**
 * This is the model class for table "ldk_submission_detail_attachment".
 *
 * @property int $sdat_id
 * @property int $sd_id
 * @property string $ut_id
 * @property string $sdat_description
 * @property string $sdat_url
 * @property string $sdat_type_file
 * @property string $sdat_mime_file
 * @property int $sd_uploaded_at
 * @property int $sd_uploaded_by
 */
class LdkSubmissionDetailAttachment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_submission_detail_attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sd_id', 'sdat_url', 'sdat_type_file', 'sdat_mime_file', 'sd_uploaded_at', 'sd_uploaded_by'], 'required'],
            [['sd_id', 'sd_uploaded_at', 'sd_uploaded_by'], 'integer'],
            [['sdat_description'], 'string'],
            [['ut_id'], 'string', 'max' => 32],
            [['sdat_url'], 'string', 'max' => 150],
            [['sdat_type_file', 'sdat_mime_file'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sdat_id' => 'Sdat ID',
            'sd_id' => 'Sd ID',
            'ut_id' => 'Ut ID',
            'sdat_description' => 'Sdat Description',
            'sdat_url' => 'Sdat Url',
            'sdat_type_file' => 'Sdat Type File',
            'sdat_mime_file' => 'Sdat Mime File',
            'sd_uploaded_at' => 'Sd Uploaded At',
            'sd_uploaded_by' => 'Sd Uploaded By',
        ];
    }
}
