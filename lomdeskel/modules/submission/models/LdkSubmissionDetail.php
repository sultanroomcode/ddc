<?php

namespace lomdeskel\modules\submission\models;

use Yii;

/**
 * This is the model class for table "ldk_submission_detail".
 *
 * @property int $sd_id
 * @property int $s_id
 * @property string $r_id
 * @property int $ra_id
 * @property string $ut_id
 * @property int $sd_created_at
 * @property int $sd_updated_at
 * @property int $sd_created_by
 * @property int $sd_updated_by
 */
class LdkSubmissionDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_submission_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['s_id', 'ra_id', 'sd_created_at', 'sd_updated_at', 'sd_created_by', 'sd_updated_by'], 'required'],
            [['s_id', 'ra_id', 'sd_created_at', 'sd_updated_at', 'sd_created_by', 'sd_updated_by'], 'integer'],
            [['r_id'], 'string', 'max' => 50],
            [['ut_id'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sd_id' => 'Sd ID',
            's_id' => 'S ID',
            'r_id' => 'R ID',
            'ra_id' => 'Ra ID',
            'ut_id' => 'Ut ID',
            'sd_created_at' => 'Sd Created At',
            'sd_updated_at' => 'Sd Updated At',
            'sd_created_by' => 'Sd Created By',
            'sd_updated_by' => 'Sd Updated By',
        ];
    }
}
