<?php

namespace lomdeskel\modules\submission\models;

use Yii;

/**
 * This is the model class for table "ldk_submission".
 *
 * @property int $s_id
 * @property string $ut_id
 * @property int $u_id
 * @property int $co_id
 * @property string $s_description
 * @property string $s_total_point
 * @property string $s_status
 * @property int $s_created_at
 * @property int $s_updated_at
 * @property int $s_created_by
 * @property int $s_updated_by
 */
class LdkSubmission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldk_submission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['u_id', 'co_id', 's_description', 's_total_point', 's_status', 's_created_at', 's_updated_at', 's_created_by', 's_updated_by'], 'required'],
            [['u_id', 'co_id', 's_created_at', 's_updated_at', 's_created_by', 's_updated_by'], 'integer'],
            [['s_total_point'], 'number'],
            [['ut_id'], 'string', 'max' => 32],
            [['s_description'], 'string', 'max' => 150],
            [['s_status'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            's_id' => 'S ID',
            'ut_id' => 'Ut ID',
            'u_id' => 'U ID',
            'co_id' => 'Co ID',
            's_description' => 'S Description',
            's_total_point' => 'S Total Point',
            's_status' => 'S Status',
            's_created_at' => 'S Created At',
            's_updated_at' => 'S Updated At',
            's_created_by' => 'S Created By',
            's_updated_by' => 'S Updated By',
        ];
    }
}
