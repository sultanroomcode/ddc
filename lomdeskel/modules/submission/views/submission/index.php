<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ldk Submissions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-submission-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ldk Submission', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            's_id',
            'ut_id',
            'u_id',
            'co_id',
            's_description',
            //'s_total_point',
            //'s_status',
            //'s_created_at',
            //'s_updated_at',
            //'s_created_by',
            //'s_updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
