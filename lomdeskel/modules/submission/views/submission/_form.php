<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\submission\models\LdkSubmission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-submission-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ut_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_id')->textInput() ?>

    <?= $form->field($model, 'co_id')->textInput() ?>

    <?= $form->field($model, 's_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_total_point')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_created_at')->textInput() ?>

    <?= $form->field($model, 's_updated_at')->textInput() ?>

    <?= $form->field($model, 's_created_by')->textInput() ?>

    <?= $form->field($model, 's_updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
