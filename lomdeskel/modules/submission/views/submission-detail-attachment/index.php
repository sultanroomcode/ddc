<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ldk Submission Detail Attachments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-submission-detail-attachment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ldk Submission Detail Attachment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sdat_id',
            'sd_id',
            'ut_id',
            'sdat_description:ntext',
            'sdat_url:url',
            //'sdat_type_file',
            //'sdat_mime_file',
            //'sd_uploaded_at',
            //'sd_uploaded_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
