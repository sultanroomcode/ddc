<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\submission\models\LdkSubmissionDetailAttachment */

$this->title = $model->sdat_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Submission Detail Attachments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-submission-detail-attachment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sdat_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sdat_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sdat_id',
            'sd_id',
            'ut_id',
            'sdat_description:ntext',
            'sdat_url:url',
            'sdat_type_file',
            'sdat_mime_file',
            'sd_uploaded_at',
            'sd_uploaded_by',
        ],
    ]) ?>

</div>
