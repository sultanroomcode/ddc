<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\submission\models\LdkSubmissionDetailAttachment */

$this->title = 'Create Ldk Submission Detail Attachment';
$this->params['breadcrumbs'][] = ['label' => 'Ldk Submission Detail Attachments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-submission-detail-attachment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
