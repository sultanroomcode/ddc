<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\submission\models\LdkSubmissionDetailAttachment */

$this->title = 'Update Ldk Submission Detail Attachment: ' . $model->sdat_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Submission Detail Attachments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sdat_id, 'url' => ['view', 'id' => $model->sdat_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-submission-detail-attachment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
