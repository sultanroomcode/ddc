<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\submission\models\LdkSubmissionDetailAttachment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-submission-detail-attachment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sd_id')->textInput() ?>

    <?= $form->field($model, 'ut_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sdat_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sdat_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sdat_type_file')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sdat_mime_file')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sd_uploaded_at')->textInput() ?>

    <?= $form->field($model, 'sd_uploaded_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
