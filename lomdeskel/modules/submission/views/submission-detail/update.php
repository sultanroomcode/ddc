<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\submission\models\LdkSubmissionDetail */

$this->title = 'Update Ldk Submission Detail: ' . $model->sd_id;
$this->params['breadcrumbs'][] = ['label' => 'Ldk Submission Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sd_id, 'url' => ['view', 'id' => $model->sd_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ldk-submission-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
