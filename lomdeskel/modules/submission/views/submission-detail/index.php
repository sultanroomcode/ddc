<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ldk Submission Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ldk-submission-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ldk Submission Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sd_id',
            's_id',
            'r_id',
            'ra_id',
            'ut_id',
            //'sd_created_at',
            //'sd_updated_at',
            //'sd_created_by',
            //'sd_updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
