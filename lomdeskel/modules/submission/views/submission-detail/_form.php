<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model lomdeskel\modules\submission\models\LdkSubmissionDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ldk-submission-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 's_id')->textInput() ?>

    <?= $form->field($model, 'r_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ra_id')->textInput() ?>

    <?= $form->field($model, 'ut_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sd_created_at')->textInput() ?>

    <?= $form->field($model, 'sd_updated_at')->textInput() ?>

    <?= $form->field($model, 'sd_created_by')->textInput() ?>

    <?= $form->field($model, 'sd_updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
