<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

if($params['offline']) {
    $db_access = [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=lomdeskel',
            'username' => 'taroom',
            'password' => '1234',
            'charset' => 'utf8',
        ];
} else {
    $db_access = [
            // 'class' => 'yii\db\Connection',
            // 'dsn' => 'mysql:host=localhost;dbname=mediatub_sp3d',
            // 'username' => 'mediatub_cli',
            // 'password' => 'XUIJOy70sH7di5pvVG',
            // 'charset' => 'utf8',

            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=lomdeskel',
            'username' => 'taroom',
            'password' => '1234',
            'charset' => 'utf8',
        ];
}

return [
    'id' => 'lomdeskel',
    'language' => 'id-ID',
    'name' => 'Lomba Desa dan Kelurahan',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'lomdeskel\controllers',
    'as access' => [
         'class' => '\hscstudio\mimin\components\AccessControl',
         'allowActions' => [
            // add wildcard allowed action here!, data yang bisa di akses umum
            '*',
            'mimin/*'
            /*'site/*',
            'data-umum-desa/*',
            'data-check/*',
            'data-umum/*',
            'labs/*',
            'debug/*',
            'gii/*',
            , */// only in dev mode
        ],
    ],
    'components' => [
        'db' => $db_access,
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'bundles' => [
                // 'yii\web\JqueryAsset' => [
                //     'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                // ],
            ],
        ],
        'user' => [
            'identityClass' => 'lomdeskel\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
    'modules' => [
        'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        'reference' => [
            'class' => 'lomdeskel\modules\reference\Reference',
        ],
        'contest' => [
            'class' => 'lomdeskel\modules\contest\Contest',
        ],
        'submission' => [
            'class' => 'lomdeskel\modules\submission\Submission',
        ],
        'userarea' => [
            'class' => 'lomdeskel\modules\userarea\UserArea',
        ],
    ],    
    'params' => $params,
];