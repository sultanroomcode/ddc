<?php
return [
    'adminEmail' => 'siapatakut@gmail.com',
    'supportEmail' => 'adminsiapatakut@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
