<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('proman', dirname(dirname(__DIR__)) . '/proman');
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('sp3d', dirname(dirname(__DIR__)) . '/sp3d');
Yii::setAlias('sadmin', dirname(dirname(__DIR__)) . '/sadmin');
Yii::setAlias('ddcop', dirname(dirname(__DIR__)) . '/ddcop');
Yii::setAlias('siapatakut', dirname(dirname(__DIR__)) . '/siapatakut');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('kelas', dirname(dirname(__DIR__)) . '/kelas');
Yii::setAlias('klinikbumdes', dirname(dirname(__DIR__)) . '/klinikbumdes');
Yii::setAlias('lomdeskel', dirname(dirname(__DIR__)) . '/lomdeskel');
