<?php

namespace common\models;
use yii\db\ActiveQuery;
/**
 * This is the ActiveQuery class for [[Userpuskar]].
 *
 * @see Userpuskar
 */
class UserpuskarQuery extends ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Userpuskar[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Userpuskar|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}