var base_url = $('body').attr('data-url');
var main_el = $('div#main-container');
var main_indicator = $('#sp3d-indicator-cube');
//smootscrol 1 page

main_el.text('DDC Siap!');
main_indicator.hide();

function refreshTree(){
    $('#jstree_menu').jstree(true).refresh();
}

var modalxyf = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#00f'
  }
});

function goPopup(o){//f stand front
	if (o.hasOwnProperty('welm')) {
		$('#form-engine').width(o.welm);
		console.log(o.welm);
	} else {
		$('#form-engine').width('400px');
	}
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxyf.open();    
}

function openModalXyf(o){//f stand front
	if (o.hasOwnProperty('welm')) {
		$('#form-engine').width(o.welm);
		console.log(o.welm);
	} else {
		$('#form-engine').width('400px');
	}
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxyf.open();    
}

function reopenModalXyf(o){//f stand front
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
}

function setAvatar(o)
{
	$.getJSON(base_url+o.url, function(res) {
		if(res !== ''){
			$('.foto-avatar').attr('src', res.src);
		}
   	});
}

function goHiatus(o){
	var elm = $(o.elm);
	if(o.state == 'show'){
		elm.show();
		elm.load(base_url+o.url);
	} else {
		elm.html("&nbsp;");
		elm.hide();
	}
}

function getVal(o) {//forsortable
	// var dataList = $(o.elm).map(function() {
	//     return $(this).data("id-conn");
	// }).get();
	// console.log(dataList.join('|'));

	var dataList = $(o.elm).map(function() {
	    return $(this).data("id-conn");
	}).get();

	$(o.elmshow).val(dataList.join('|'));
	return false;
}

function refreshGrid(o) {
	$.pjax.reload({container:o.id});
}

function goLoad(o) {
	dataObject = null;
	if (o.hasOwnProperty('dataObject')) {
		dataObject = o.dataObject.stringify();
	}

	if (o.hasOwnProperty('elm')) {
		main_indicator.show();
		if(dataObject !== null){
			$(o.elm).load(base_url+o.url,{data:dataObject},responseCall);
		} else {
			$(o.elm).load(base_url+o.url,responseCall);
		}
		
	} else {
		if(dataObject !== null){
			main_el.load(base_url+o.url,{data:dataObject}, responseCall);
		} else {
			main_el.load(base_url+o.url,responseCall);
		}
	}
}

function responseCall(response, status, xhr) {
    console.log(xhr);
    main_indicator.hide(1000);
    if (status == "success") {
        // SUCCESSFUL request //
    } else if (status == "error" || status == "timeout") {
        // ERROR and TIMEOUT request //
        if(xhr.status == 403){
        	$.bootstrapGrowl('Anda tidak dapat masuk pada bagian ini', {
                type: 'danger'
            });
        }

        if(xhr.status == 400){
        	$.bootstrapGrowl('Gagal meminta halaman ini', {
                type: 'info'
            });
        }

        if(xhr.status == 500){
        	$.bootstrapGrowl('Server error! hubungi admin', {
                type: 'danger'
            });
        }
    } else if (status == "notmodified") {
        // NOT MODIFIED request (likely cached) //
    } else if (status == "abort") {
        // ABORTED request //
    } 
}

function goCall(o){
	/*
	elm
	url
	msg
	urlback

	msgTitle
	msgLong
	timeOut
	typeSend
	*/
	var closing = 'batal|'+(o.timeOut == null?10000:o.timeOut);
    $.confirm({
        title: o.msgTitle == null?'Hapus Data!':o.msgTitle,
        content: o.msgLong == null?'Tindakan ini akan menghapus data':o.msgLong,
        autoClose: closing,
        buttons: {
            deleteUser: {
                text: o.textBtn == null?'Hapus Data!':o.textBtn,
                action: function () {
                    goTransmit({typeSend: o.typeSend == null?'post':o.typeSend, urlSend:o.url, msg: o.msg, elmChange:o.elm, urlBack:o.urlback});
                }
            },
            batal: function () {
                $.alert((o.msgCancel == null?'Tindakan dibatalkan':o.msgCancel));
            }
        }
    });
}

function goTransmit(o){
	//msg, typeSend, urlSend, elmChange, urlBack
	$.ajax({
		type: o.typeSend,
		url: base_url+o.urlSend,
		//data: $('form').serialize(),
		error: function (xhr, ajaxOptions, thrownError) {
			return false;	
		},
		success: function (res) {
			console.log(res);
			$.bootstrapGrowl('Berhasil '+o.msg, {
                type: 'success'
            });
			if (o.hasOwnProperty('elmChange')) {//artinya tidak mengganti element utama
				goLoad({elm:o.elmChange, url: o.urlBack});	
			} else {
				if (o.hasOwnProperty('urlBack')) {
					goLoad({url:o.urlBack});
				} else {
					main_el.html('-');
				}
			}
		}
	});
}

function goModal(o) {
	$(o.header).html(o.headertxt);
	goLoad({target:o.target, url:o.url});
}

function tabsXget(o){
	/* url, elm, data = object*/
	$.post(base_url+o.url,o.data).done(function(res){
        data = JSON.parse(res);
        $(o.elm).html(data);
    });
}

function emptyElm(obj){
	$(obj).html(':::');
}

function hidElm(o){
	tout = (o.hasOwnProperty('timeout'))?o.timeout:0;
	if (o.hasOwnProperty('elmShow')) {
		$(o.elmShow).show(tout);
	}

	if (o.hasOwnProperty('elmHide')) {
		$(o.elmHide).hide(tout);
	}
}

function goSendTabX(o){
	/* msg, typeSend, urlSend, urlBack, elm, data = object*/
	if(confirm(o.msg)){
		$.ajax({
			type: o.typeSend,
			url: base_url+o.urlSend,
			data: o.data,
			error: function (xhr, ajaxOptions, thrownError) {
				return false;	
			},
			success: function (res) {
				tabsXget({url:o.urlBack, data:o.data, elm:o.elm});
			}
		});
	}
}

function goSendLoad(o){
	//msg, typeSend, urlSend, elmChange, urlBack

	var alerting = swal({
		title: "Anda Yakin?",
		text: o.msg,
		type: "warning",
		animation:'pop',
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Hapus!",
		closeOnConfirm: false
	},
	function(isConfirm){
		if(isConfirm){
			$.ajax({
				type: o.typeSend,
				url: base_url+o.urlSend,
				//data: $('form').serialize(),
				error: function (xhr, ajaxOptions, thrownError) {
					return false;	
				},
				beforeSend: function() {
	                main_indicator.show();
	            },
	            complete: function() {
	            	main_indicator.hide();    
	            },
				success: function (res) {
					$.bootstrapGrowl('Berhasil '+o.msg, {
		                type: 'success'
		            });
					if (o.hasOwnProperty('elmChange')) {//artinya tidak mengganti element utama
						goLoad({elm:o.elmChange, url: o.urlBack});	
					} else {
						
						if (o.hasOwnProperty('urlBack')) {
							goLoad({url:o.urlBack});
						} else {
							main_el.html('-');
						}
					}
				}
			});
			swal.close();
		}
	});
}



$(function(){
	//http://stackoverflow.com/questions/36341638/unrecognized-expression-ahref
	$('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	        if (target.length) {
	            $('html,body').animate({
	                scrollTop: target.offset().top
	            }, 1000);
	            return false;
	        }
	    }
	});

	// $('.photostream').magnificPopup({delegate:'a',type:'image', titleSrc: 'title'});
});