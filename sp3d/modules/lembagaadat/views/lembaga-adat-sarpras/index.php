<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lembaga Adat Sarpras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-sarpras-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lembaga Adat Sarpras', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'id_lembaga',
            'tahun',
            'kd_kecamatan',
            'kd_kabupaten',
            //'memiliki_sekretariat',
            //'status_sekretariat',
            //'alamat_sekretariat',
            //'nama_cp',
            //'no_cp',
            //'bentuk_fasilitas',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
