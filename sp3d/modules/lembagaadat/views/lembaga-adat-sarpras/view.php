<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatSarpras */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Sarpras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-sarpras-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kd_desa' => $model->kd_desa, 'id_lembaga' => $model->id_lembaga, 'tahun' => $model->tahun], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_desa' => $model->kd_desa, 'id_lembaga' => $model->id_lembaga, 'tahun' => $model->tahun], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'id_lembaga',
            'tahun',
            'kd_kecamatan',
            'kd_kabupaten',
            'memiliki_sekretariat',
            'status_sekretariat',
            'alamat_sekretariat',
            'nama_cp',
            'no_cp',
            'bentuk_fasilitas',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
