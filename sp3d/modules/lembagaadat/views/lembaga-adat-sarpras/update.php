<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatSarpras */

$this->title = 'Update Lembaga Adat Sarpras: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Sarpras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_lembaga' => $model->id_lembaga, 'tahun' => $model->tahun]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lembaga-adat-sarpras-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
