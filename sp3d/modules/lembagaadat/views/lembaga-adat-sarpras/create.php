<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatSarpras */

$this->title = 'Create Lembaga Adat Sarpras';
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Sarpras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-sarpras-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
