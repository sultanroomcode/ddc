<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-6',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
}

$urlback = 'goLoad({elm:\'#zona-lembaga-adat-sarpras\', url:\'/lembagaadat/lembaga-adat-sarpras/view-in-box?kd_desa='.$model->kd_desa.'&id_lembaga='.$model->id_lembaga.'\'});';

$arr = [
    '2016' => '2016',
    '2017' => '2017',
    '2018' => '2018',
    '2019' => '2019',
];
?>

<div class="lembaga-adat-sarpras-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_lembaga')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'tahun')->dropdownList($arr) ?>

    <?= $form->field($model, 'memiliki_sekretariat')->dropdownList(['ada' => 'Ada', 'tidak' => 'tidak']) ?>

    <?= $form->field($model, 'status_sekretariat')->dropdownList(['-' => '-','sewa' => 'Sewa', 'milik-sendiri' => 'Milik Sendiri', 'pemerintah-desa' => 'Pemerintah Desa']) ?>

    <?= $form->field($model, 'alamat_sekretariat')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_cp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_cp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bentuk_fasilitas')->dropdownList([
        '-' => '-',
        'pemerintah' => 'Pemerintah Pusat, Provinsi, Kabupaten',
        'swasta' => 'Swasta/CSR',
        'swadaya' => 'Swadaya Masyarakat',
    ]) ?>

    <div class="col-md-2"></div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-danger">Kembali</a>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);