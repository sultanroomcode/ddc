<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lembaga Adats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lembaga Adat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'nama',
            'jenis',
            //'ditetapkan_oleh',
            //'sk_pendirian',
            //'sk_no',
            //'kesekertariatan_status_ada',
            //'status_kesekertariatan',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
