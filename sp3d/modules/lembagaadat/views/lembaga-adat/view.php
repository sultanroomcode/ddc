<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdat */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-view">
    <div class="tile">
        <h2 class="tile-title">Lembaga Adat</h2>
        <div class="p-10">
            <div id="zona-lembaga-adat-data"></div>
            <div id="zona-lembaga-adat-pengurus"></div>
            <div id="zona-lembaga-adat-kegiatan"></div>
            <div id="zona-lembaga-adat-sarpras"></div>
            <div id="zona-lembaga-adat-dokumentasi"></div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>
<?php 
$kd_desa = $model->kd_desa;
$script = <<<JS
goLoad({elm:'#zona-lembaga-adat-data', url:'/lembagaadat/lembaga-adat/view-in-box?kd_desa={$kd_desa}&id_lembaga={$model->id_lembaga}'});
goLoad({elm:'#zona-lembaga-adat-pengurus', url:'/lembagaadat/lembaga-adat-pengurus/view-in-box?kd_desa={$kd_desa}&id_lembaga={$model->id_lembaga}'});
goLoad({elm:'#zona-lembaga-adat-kegiatan', url:'/lembagaadat/lembaga-adat-kegiatan/view-in-box?kd_desa={$kd_desa}&id_lembaga={$model->id_lembaga}'});
goLoad({elm:'#zona-lembaga-adat-sarpras', url:'/lembagaadat/lembaga-adat-sarpras/view-in-box?kd_desa={$kd_desa}&id_lembaga={$model->id_lembaga}'});
goLoad({elm:'#zona-lembaga-adat-dokumentasi', url:'/lembagaadat/lembaga-adat-kegiatan-dokumentasi/view-in-box?kd_desa={$kd_desa}&id_lembaga={$model->id_lembaga}'});
JS;
$this->registerJs($script);
?>