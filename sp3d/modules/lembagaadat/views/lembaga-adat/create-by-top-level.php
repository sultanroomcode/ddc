<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$id = Yii::$app->user->identity->id;
$tipeuser = Yii::$app->user->identity->type;
$arr = ['-' => ''];
$listArr = [];
if($tipeuser == 'provinsi'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
}

if($tipeuser == 'kabupaten'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
}

$listArr = $arr + $listArr;
?>

<div class="lembaga-adat-form">
    <div class="tile">
        <h2 class="tile-title">Lembaga Adat</h2>
        <div class="p-10">
            <?php $form = ActiveForm::begin($arrFormConfig); ?>

            <div class="row">
                <div class="col-md-5">
                	<?php if($tipeuser == 'provinsi'){ ?>
			            <?= $form->field($model, 'kd_kabupaten')->dropdownList($listArr)->label('Kabupaten') ?>
			            <?= $form->field($model, 'kd_kecamatan')->dropdownList([])->label('Kecamatan') ?>
	                	<?= $form->field($model, 'kd_desa')->dropdownList([])->label('Desa') ?>
                	<?php } ?>

                	<?php if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten'){ ?>
                        <?php if($tipeuser == 'kabupaten'){ $model->kd_kabupaten = substr($id, 0, 4); ?>
                        	<?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
				            <?= $form->field($model, 'kd_kecamatan')->dropdownList([])->label('Kecamatan') ?>
		                	<?= $form->field($model, 'kd_desa')->dropdownList([])->label('Desa') ?>
                        <?php } ?>
                    <?php } else { $model->kd_kabupaten = substr($id, 0, 4); $model->kd_kecamatan = $id; ?>
                        	<?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
                        	<?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
		                	<?= $form->field($model, 'kd_desa')->dropdownList([])->label('Desa') ?>
                    <?php } ?>		            
		            
		            <?= $form->field($model, 'id_lembaga')->textInput(['readonly' => true, 'maxlength' => true])->label('Kode Lembaga') ?>

                    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'jenis_adat')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'status_legalitas')->dropdownList(['ada' => 'Ada', 'tidak' => 'Tidak Ada']) ?>
                    
                    <?= $form->field($model, 'jenis_legalitas')->textInput(['maxlength' => true]) ?>
                    
                    <?= $form->field($model, 'no_legalitas')->textInput(['maxlength' => true]) ?>
                    
                    <?= $form->field($model, 'deskripsi')->textarea(['maxlength' => true]) ?>

                    <?= $form->field($model, 'kategori')->dropdownList([
                        'EMBRIO' => 'Embrio',
                        'BERKEMBANG' => 'Berkembang',
                        'MANDIRI' => 'Mandiri',
                    ]) ?>

                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php 
$naming = strtolower($model->formName());
$script = <<<JS
function firstSearch()
{
    var kode = $('#{$naming}-kd_kabupaten').val();
    goLoad({elm:'#{$naming}-kd_kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
}

firstSearch();

$('#{$naming}-kd_kabupaten').change(function(){
  var kode = $(this).val();
  $('#{$naming}-kd_kecamatan, #{$naming}-kd_desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#{$naming}-kd_kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});

$('#{$naming}-kd_kecamatan').change(function(){
  var kode = $(this).val();
  $('#{$naming}-kd_desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#{$naming}-kd_desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

$('#{$naming}-kd_desa').change(function(){
  var kode = $(this).val();
  $.get(base_url+'/lembagaadat/lembaga-adat/get-new-id?kd_desa='+kode, function(res){
	// console.log(res);
	resp = JSON.parse(res);
	$('#{$naming}-id_lembaga').val(resp.num);
  });
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({url:'/lembagaadat/default'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);