<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdat */

$this->title = 'Update Lembaga Adat: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'id' => $model->kd_desa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lembaga-adat-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
