<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdat */

$this->title = 'Create Lembaga Adat';
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
