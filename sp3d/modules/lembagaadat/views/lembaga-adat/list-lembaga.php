<?php
$this->title = 'Daftar Lembaga Adat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-index">
    <a href="javascript:void(0)" onclick="goLoad({url:'/lembagaadat/lembaga-adat/create-by-toplevel'});" class="btn btn-danger">Tambah</a>

    <table class="table">
        <thead>
            <tr>
                <th>Kabupaten</th>
                <th>Kecamatan</th>
                <th>Desa</th>
                <th>Nama</th>
                <th>Status Legalitas</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($model->all() as $lembaga) { ?>
            <tr>
                <td><?= $lembaga->kabupaten->description ?></td>
                <td><?= $lembaga->kecamatan->description ?></td>
                <td><?= $lembaga->desa->description ?></td>
                <td><?= $lembaga->nama ?></td>
                <td><?= $lembaga->status_legalitas ?></td>
                <td>
                    <a href="javascript:void(0)" onclick="goLoad({url:'/lembagaadat/lembaga-adat/update?kd_desa=<?=$lembaga->kd_desa?>&id_lembaga=<?=$lembaga->id_lembaga?>&provinsi=1'});" class="btn btn-danger">Update</a>
                    <a href="javascript:void(0)" onclick="goLoad({url:'/lembagaadat/lembaga-adat/view-lembaga?kd_desa=<?=$lembaga->kd_desa?>&id_lembaga=<?=$lembaga->id_lembaga?>&provinsi=1'});" class="btn btn-danger">Lihat</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
