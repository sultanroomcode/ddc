<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatPengurus */

$this->title = 'Isi Pengurus Lembaga Adat';
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Penguruses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-pengurus-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
