<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatPengurus */

$this->title = 'Update Pengurus Lembaga Adat : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Penguruses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_pengurus' => $model->id_pengurus]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lembaga-adat-pengurus-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
