<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-lembaga-adat-pengurus', url:'/lembagaadat/lembaga-adat-pengurus/create?kd_desa=<?=$model->kd_desa?>&id_lembaga=<?=$model->id_lembaga?>'})" class="btn btn-danger">Tambah Pengurus</a>

<?php 
echo '<table class="table table-striped"><tr><th>Nama</th><th>Jabatan</th><th>Tanggal Lahir</th><th>Pendidikan</th><th>Aksi</th></tr>';
foreach($model->pengurus as $v){
    echo '<tr><th>'.$v->nama.'</th><th>'.$v->jabatan.'</th><th>'.$v->tanggal_lahir.'</th><th>'.substr($v->pendidikan, 2).'</th><th>Aksi</th></tr>';
}
echo '</table>';	
?>