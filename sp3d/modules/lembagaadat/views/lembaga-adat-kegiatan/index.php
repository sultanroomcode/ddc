<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lembaga Adat Kegiatans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-kegiatan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lembaga Adat Kegiatan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_kegiatan',
            'nama',
            //'jenis',
            //'tanggal_mulai',
            //'tanggal_selesai',
            //'sumber_dana',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
