<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatKegiatan */

$this->title = 'Update Kegiatan Lembaga Adat : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_kegiatan' => $model->id_kegiatan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lembaga-adat-kegiatan-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
