<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-lembaga-adat-kegiatan', url:'/lembagaadat/lembaga-adat-kegiatan/create?kd_desa=<?=$model->kd_desa?>&id_lembaga=<?=$model->id_lembaga?>'})" class="btn btn-danger">Tambah Kegiatan</a>

<?php 
echo '<table class="table table-striped"><tr><th>Nama</th><th>Jenis</th><th>Tanggal Mulai/Tanggal Selesai</th><th>Sumber Dana</th><th>Aksi</th></tr>';
foreach($model->kegiatan as $v){
    echo '<tr><th>'.$v->nama.'</th><th>'.$v->jenis.'</th><th>'.$v->tanggal_mulai.'/'.$v->tanggal_selesai.'</th><th>'.$v->sumber_dana.'</th><th>Aksi</th></tr>';
}
echo '</table>';	
?>