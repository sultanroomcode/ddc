<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatKegiatan */

$this->title = 'Isi Kegiatan Lembaga Adat';
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-kegiatan-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
