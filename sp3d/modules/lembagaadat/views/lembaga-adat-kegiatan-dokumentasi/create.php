<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatKegiatanDokumentasi */

$this->title = 'Create Lembaga Adat Kegiatan Dokumentasi';
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Kegiatan Dokumentasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-kegiatan-dokumentasi-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
