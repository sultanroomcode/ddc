<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatKegiatanDokumentasi */

$this->title = $model->id_dokumentasi;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Kegiatan Dokumentasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-kegiatan-dokumentasi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_dokumentasi], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_dokumentasi], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_dokumentasi',
            'kd_desa',
            'id_lembaga',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_kegiatan',
            'nama_file',
            'jenis',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
