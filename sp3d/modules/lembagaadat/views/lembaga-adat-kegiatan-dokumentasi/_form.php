<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use sp3d\modules\lembagaadat\models\LembagaAdatKegiatan;

$arrFormConfig = [
'id' => $model->formName(), 
'options' => ['enctype' => 'multipart/form-data'],
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-6',
        'error' => '',
        'hint' => '',
    ],
]];

$arrKegiatan = ArrayHelper::map(LembagaAdatKegiatan::find()->where(['kd_desa' => $model->kd_desa, 'id_lembaga' => $model->id_lembaga])->all(),'id_kegiatan', 'nama');

if($model->isNewRecord){
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
}

$urlback = "goLoad({elm:'#zona-lembaga-adat-dokumentasi', url:'/lembagaadat/lembaga-adat-kegiatan-dokumentasi/view-in-box?kd_desa={$model->kd_desa}&id_lembaga={$model->id_lembaga}'});";

?>

<div class="lembaga-adat-kegiatan-dokumentasi-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_lembaga')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_kegiatan')->dropdownList($arrKegiatan) ?>

    <?= $form->field($model, 'nama_file_box')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis')->dropdownList(['foto' => 'Foto', 'video' => 'Video']) ?>

    <div class="col-md-2"></div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-danger">Kembali</a>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$scripts =<<<JS
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#{$model->formName()}')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                {$urlback}
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($scripts);