<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lembaga Adat Kegiatan Dokumentasis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembaga-adat-kegiatan-dokumentasi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lembaga Adat Kegiatan Dokumentasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_dokumentasi',
            'kd_desa',
            'id_lembaga',
            'kd_kecamatan',
            'kd_kabupaten',
            //'id_kegiatan',
            //'nama_file',
            //'jenis',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
