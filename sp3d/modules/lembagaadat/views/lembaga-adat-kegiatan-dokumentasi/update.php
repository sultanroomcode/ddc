<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lembagaadat\models\LembagaAdatKegiatanDokumentasi */

$this->title = 'Update Lembaga Adat Kegiatan Dokumentasi: ' . $model->id_dokumentasi;
$this->params['breadcrumbs'][] = ['label' => 'Lembaga Adat Kegiatan Dokumentasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_dokumentasi, 'url' => ['view', 'id' => $model->id_dokumentasi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lembaga-adat-kegiatan-dokumentasi-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
