<div class="lembaga-adat-view">
    <div class="tile">
        <h2 class="tile-title">Daftar Lembaga Adat</h2>
        <div class="p-10">
            <div id="zona-lembaga-adat"></div>
        </div>
    </div>
</div>

<?php 
$script = <<<JS
goLoad({elm:'#zona-lembaga-adat', url:'/lembagaadat/lembaga-adat/list-lembaga'});
JS;
$this->registerJs($script);
?>