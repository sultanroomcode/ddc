<?php

namespace sp3d\modules\lembagaadat\controllers;

use Yii;
use sp3d\modules\lembagaadat\models\LembagaAdatSarpras;
use sp3d\modules\lembagaadat\models\LembagaAdat;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LembagaAdatSarprasController implements the CRUD actions for LembagaAdatSarpras model.
 */
class LembagaAdatSarprasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LembagaAdatSarpras models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LembagaAdatSarpras::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LembagaAdatSarpras model.
     * @param string $kd_desa
     * @param integer $id_lembaga
     * @param string $tahun
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_lembaga, $tahun)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_lembaga, $tahun),
        ]);
    }

    public function actionViewInBox($kd_desa, $id_lembaga)
    {
        $model = LembagaAdat::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga]);
        return $this->renderAjax('view-in-box', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new LembagaAdatSarpras model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id_lembaga)
    {
        $model = new LembagaAdatSarpras();
        $model->kd_desa = $kd_desa;
        $model->id_lembaga = $id_lembaga;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LembagaAdatSarpras model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_lembaga
     * @param string $tahun
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_lembaga, $tahun)
    {
        $model = $this->findModel($kd_desa, $id_lembaga, $tahun);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LembagaAdatSarpras model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_lembaga
     * @param string $tahun
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_lembaga, $tahun)
    {
        $this->findModel($kd_desa, $id_lembaga, $tahun)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LembagaAdatSarpras model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_lembaga
     * @param string $tahun
     * @return LembagaAdatSarpras the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_lembaga, $tahun)
    {
        if (($model = LembagaAdatSarpras::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga, 'tahun' => $tahun])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
