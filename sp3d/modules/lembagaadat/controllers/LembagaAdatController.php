<?php

namespace sp3d\modules\lembagaadat\controllers;

use Yii;
use sp3d\modules\lembagaadat\models\LembagaAdat;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LembagaAdatController implements the CRUD actions for LembagaAdat model.
 */
class LembagaAdatController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LembagaAdat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LembagaAdat::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListLembaga()//provinsi dan kabupaten
    {
        $tipeuser = Yii::$app->user->identity->type;
        $model = LembagaAdat::find();
        if($tipeuser == 'kabupaten'){
            $model->where(['kd_kabupaten' => Yii::$app->user->identity->id]);
        }

        return $this->renderAjax('list-lembaga', [
            'model' => $model,
        ]);
    }

    public function actionViewLembaga($kd_desa, $id_lembaga)
    {
        $model = LembagaAdat::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/lembagaadat/lembaga-adat/create\'});</script>';
        } else {
            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        }  
    }

    /**
     * Displays a single LembagaAdat model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $kd_desa = Yii::$app->user->identity->id;
        $model = LembagaAdat::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/lembagaadat/lembaga-adat/create\'});</script>';
        } else {
            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        }  
    }

    public function actionViewInBox($kd_desa)
    {
        $model = LembagaAdat::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/lembagaadat/lembaga-adat/create\'});</script>';
        } else {
            return $this->renderAjax('view-in-box', [
                'model' => $model,
            ]);
        }        
    }

    /**
     * Creates a new LembagaAdat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateByToplevel()
    {
        $model = new LembagaAdat();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create-by-top-level', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetNewId($kd_desa)
    {
        $model = LembagaAdat::find()->where(['kd_desa' => $kd_desa])->max('id_lembaga');
        if($model == null){
            $newnumber = 1;
        } else {
            $newnumber = $model +1;
        }


        $data = ['num' => $newnumber];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return json_encode($data);
    }

    public function actionCreate()
    {
        $model = new LembagaAdat();
        $model->kd_desa = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LembagaAdat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_lembaga)
    {
        $model = $this->findModel($kd_desa, $id_lembaga);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LembagaAdat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the LembagaAdat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return LembagaAdat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_lembaga)
    {
        if (($model = LembagaAdat::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
