<?php

namespace sp3d\modules\lembagaadat\controllers;

use Yii;
use sp3d\modules\lembagaadat\models\LembagaAdatKegiatan;
use sp3d\modules\lembagaadat\models\LembagaAdat;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LembagaAdatKegiatanController implements the CRUD actions for LembagaAdatKegiatan model.
 */
class LembagaAdatKegiatanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LembagaAdatKegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LembagaAdatKegiatan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LembagaAdatKegiatan model.
     * @param string $kd_desa
     * @param integer $id_kegiatan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_kegiatan)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_kegiatan),
        ]);
    }

    public function actionViewInBox($kd_desa, $id_lembaga)
    {
        $model = LembagaAdat::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga]);
        return $this->renderAjax('view-in-box', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new LembagaAdatKegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id_lembaga)
    {
        $model = new LembagaAdatKegiatan();
        $model->kd_desa = $kd_desa;
        $model->id_lembaga = $id_lembaga;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LembagaAdatKegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_kegiatan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_lembaga, $id_kegiatan)
    {
        $model = $this->findModel($kd_desa, $id_lembaga, $id_kegiatan);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LembagaAdatKegiatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_kegiatan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_lembaga, $id_kegiatan)
    {
        $this->findModel($kd_desa, $id_kegiatan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LembagaAdatKegiatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_kegiatan
     * @return LembagaAdatKegiatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_lembaga, $id_kegiatan)
    {
        if (($model = LembagaAdatKegiatan::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga, 'id_kegiatan' => $id_kegiatan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
