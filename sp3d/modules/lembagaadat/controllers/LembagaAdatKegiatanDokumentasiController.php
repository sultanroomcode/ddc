<?php

namespace sp3d\modules\lembagaadat\controllers;

use Yii;
use sp3d\modules\lembagaadat\models\LembagaAdatKegiatanDokumentasi;
use sp3d\modules\lembagaadat\models\LembagaAdat;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LembagaAdatKegiatanDokumentasiController implements the CRUD actions for LembagaAdatKegiatanDokumentasi model.
 */
class LembagaAdatKegiatanDokumentasiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LembagaAdatKegiatanDokumentasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LembagaAdatKegiatanDokumentasi::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LembagaAdatKegiatanDokumentasi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewInBox($kd_desa, $id_lembaga)
    {
        $model = LembagaAdatKegiatanDokumentasi::find()->where(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga]);
        $model2 = LembagaAdat::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga]);
        return $this->renderAjax('view-in-box', [
            'model' => $model,
            'model2' => $model2,
        ]);
    }

    /**
     * Creates a new LembagaAdatKegiatanDokumentasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id_lembaga)
    {
        $model = new LembagaAdatKegiatanDokumentasi();
        $model->kd_desa = $kd_desa;
        $model->id_lembaga = $id_lembaga;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LembagaAdatKegiatanDokumentasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LembagaAdatKegiatanDokumentasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LembagaAdatKegiatanDokumentasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LembagaAdatKegiatanDokumentasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LembagaAdatKegiatanDokumentasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
