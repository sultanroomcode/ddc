<?php

namespace sp3d\modules\lembagaadat\controllers;

use Yii;
use sp3d\modules\lembagaadat\models\LembagaAdatPengurus;
use sp3d\modules\lembagaadat\models\LembagaAdat;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LembagaAdatPengurusController implements the CRUD actions for LembagaAdatPengurus model.
 */
class LembagaAdatPengurusController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LembagaAdatPengurus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LembagaAdatPengurus::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LembagaAdatPengurus model.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_pengurus)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_pengurus),
        ]);
    }

    public function actionViewInBox($kd_desa, $id_lembaga)
    {
        $model = LembagaAdat::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga]);
        return $this->renderAjax('view-in-box', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new LembagaAdatPengurus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id_lembaga)
    {
        $model = new LembagaAdatPengurus();
        $model->kd_desa = $kd_desa;
        $model->id_lembaga = $id_lembaga;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LembagaAdatPengurus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_lembaga, $id_pengurus)
    {
        $model = $this->findModel($kd_desa,$id_lembaga , $id_pengurus);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LembagaAdatPengurus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_lembaga, $id_pengurus)
    {
        $this->findModel($kd_desa , $id_lembaga, $id_pengurus)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LembagaAdatPengurus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return LembagaAdatPengurus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_lembaga, $id_pengurus)
    {
        if (($model = LembagaAdatPengurus::findOne(['kd_desa' => $kd_desa, 'id_lembaga' => $id_lembaga, 'id_pengurus' => $id_pengurus])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
