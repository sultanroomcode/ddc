<?php
namespace sp3d\modules\lembagaadat\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "lembaga_adat_sarpras".
 *
 * @property string $kd_desa
 * @property int $id_lembaga
 * @property string $tahun
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property string $memiliki_sekretariat
 * @property string $status_sekretariat
 * @property string $alamat_sekretariat
 * @property string $nama_cp
 * @property string $no_cp
 * @property string $bentuk_fasilitas
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class LembagaAdatSarpras extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lembaga_adat_sarpras';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_lembaga', 'tahun', 'status_sekretariat', 'alamat_sekretariat', 'nama_cp', 'no_cp'], 'required'],
            [['id_lembaga'], 'integer'],
            [['tahun'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['memiliki_sekretariat'], 'string', 'max' => 10],
            [['status_sekretariat', 'no_cp'], 'string', 'max' => 20],
            [['alamat_sekretariat'], 'string', 'max' => 255],
            [['nama_cp'], 'string', 'max' => 100],
            [['bentuk_fasilitas'], 'string', 'max' => 50],
            [['kd_desa', 'id_lembaga', 'tahun'], 'unique', 'targetAttribute' => ['kd_desa', 'id_lembaga', 'tahun']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'id_lembaga' => 'Id Lembaga',
            'tahun' => 'Tahun',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'memiliki_sekretariat' => 'Memiliki Sekretariat',
            'status_sekretariat' => 'Status Sekretariat',
            'alamat_sekretariat' => 'Alamat Sekretariat',
            'nama_cp' => 'Nama Kontak Person',
            'no_cp' => 'No Kontak Person',
            'bentuk_fasilitas' => 'Bentuk Fasilitas',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
