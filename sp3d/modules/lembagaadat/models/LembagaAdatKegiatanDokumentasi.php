<?php
namespace sp3d\modules\lembagaadat\models;

use sp3d\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "lembaga_adat_kegiatan_dokumentasi".
 *
 * @property int $id_dokumentasi
 * @property string $kd_desa
 * @property int $id_lembaga
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_kegiatan
 * @property string $nama_file
 * @property string $jenis
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property LembagaAdatKegiatan $kdDesa
 */
class LembagaAdatKegiatanDokumentasi extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $nama_file_box, $dummy_var;
    public static function tableName()
    {
        return 'lembaga_adat_kegiatan_dokumentasi';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_lembaga', 'id_kegiatan'], 'required'],
            [['id_lembaga', 'id_kegiatan'], 'integer'],
            [['kd_desa'], 'string', 'max' => 10],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['nama_file'], 'string', 'max' => 255],
            [['nama_file_box'], 'file', 'extensions' => 'mp4, jpg'],
            [['jenis'], 'string', 'max' => 20],
            [['kd_desa', 'id_lembaga', 'id_kegiatan'], 'exist', 'skipOnError' => true, 'targetClass' => LembagaAdatKegiatan::className(), 'targetAttribute' => ['kd_desa' => 'kd_desa', 'id_lembaga' => 'id_lembaga', 'id_kegiatan' => 'id_kegiatan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_dokumentasi' => 'Id Dokumentasi',
            'kd_desa' => 'Kd Desa',
            'id_lembaga' => 'Id Lembaga',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_kegiatan' => 'Id Kegiatan',
            'nama_file' => 'Nama File',
            'jenis' => 'Jenis',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatan()
    {
        return $this->hasOne(LembagaAdatKegiatan::className(), ['kd_desa' => 'kd_desa', 'id_lembaga' => 'id_lembaga', 'id_kegiatan' => 'id_kegiatan']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // if($insert){
            $this->upload();
            // }
            
            return true;
        } else {
            return false;
        }
    }

    public function upload()
    {
        $this->validate();
        $this->nama_file_box = UploadedFile::getInstance($this, 'nama_file_box');
        if (isset($this->nama_file_box) && $this->nama_file_box->size != 0){
            $url = 'userfile/'.$this->kd_desa.'/';
            $this->makeDir($url);
            $newname = 'dok-adat-desa-'.time().'-'.$this->kd_desa.'.'.$this->nama_file_box->extension;
            if($this->isNewRecord){
                $callback = $this->nama_file_box->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->nama_file) && !is_dir($url.$this->nama_file)){
                    unlink($url.$this->nama_file);
                }
                $callback = $this->nama_file_box->saveAs($url.$newname);           
            }
            $this->nama_file = $newname;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
