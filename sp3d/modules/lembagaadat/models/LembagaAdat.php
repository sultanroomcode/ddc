<?php
namespace sp3d\modules\lembagaadat\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "lembaga_adat".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property string $nama
 * @property string $jenis
 * @property string $ditetapkan_oleh
 * @property string $sk_pendirian
 * @property string $sk_no
 * @property string $kesekertariatan_status_ada
 * @property string $status_kesekertariatan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class LembagaAdat extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lembaga_adat';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'nama'], 'required'],
            [['id_lembaga'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 7],
            [['kd_kabupaten'], 'string', 'max' => 4],
            [['nama'], 'string', 'max' => 150],
            [['jenis_adat', 'deskripsi'], 'string', 'max' => 100],
            [['status_legalitas'], 'string', 'max' => 10],
            [['jenis_legalitas'], 'string', 'max' => 50],
            [['no_legalitas'], 'string', 'max' => 30],
            [['kategori'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Desa',
            'kd_kecamatan' => 'Kecamatan',
            'kd_kabupaten' => 'Kabupaten',
            'nama' => 'Nama Lembaga Adat',
            'jenis_adat' => 'Jenis Adat Istiadat',
            
            'status_legalitas' => 'Legalitas Lembaga Adat',
            'jenis_legalitas' => 'Jenis Legalitas',
            'no_legalitas' => 'Nomor Legalitas',
            'deskripsi' => 'Deskripsi',
            'kategori' => 'Kategori',

            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diubah Oleh',
            'created_at' => 'Dibuat pada tanggal',
            'updated_at' => 'Diubah pada tanggal',
        ];
    }

    public function getPengurus()
    {
        return $this->hasMany(LembagaAdatPengurus::className(), ['kd_desa' => 'kd_desa', 'id_lembaga' => 'id_lembaga']);
    }

    public function getKegiatan()
    {
        return $this->hasMany(LembagaAdatKegiatan::className(), ['kd_desa' => 'kd_desa', 'id_lembaga' => 'id_lembaga']);
    }

    public function getSarpras()
    {
        return $this->hasMany(LembagaAdatSarpras::className(), ['kd_desa' => 'kd_desa', 'id_lembaga' => 'id_lembaga']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id_lembaga');
        if($mx == null){
            $this->id_lembaga = 1;
        } else {
            $this->id_lembaga = $mx +1;
        }
    }
}
