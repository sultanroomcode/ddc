<?php
namespace sp3d\modules\lembagaadat\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "lembaga_adat_pengurus".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_pengurus
 * @property string $jabatan
 * @property string $nama
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $sk_jabatan
 * @property string $pendidikan
 * @property string $status_aktif
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class LembagaAdatPengurus extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lembaga_adat_pengurus';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa', 'id_pengurus'], 'required'],
            [['id_pengurus'], 'integer'],
            [['tanggal_lahir'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['jabatan'], 'string', 'max' => 20],
            [['nama', 'tempat_lahir', 'sk_jabatan'], 'string', 'max' => 100],
            [['alamat'], 'string', 'max' => 150],
            [['pendidikan'], 'string', 'max' => 50],
            [['status_aktif'], 'string', 'max' => 10],
            [['kd_desa', 'id_pengurus'], 'unique', 'targetAttribute' => ['kd_desa', 'id_pengurus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_pengurus' => 'Id Pengurus',
            'jabatan' => 'Jabatan',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'sk_jabatan' => 'Sk Jabatan',
            'pendidikan' => 'Pendidikan',
            'status_aktif' => 'Status Aktif',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id_pengurus');
        if($mx == null){
            $this->id_pengurus = 1;
        } else {
            $this->id_pengurus = $mx +1;
        }
    }
}
