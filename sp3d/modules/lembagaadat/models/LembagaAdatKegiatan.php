<?php
namespace sp3d\modules\lembagaadat\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "lembaga_adat_kegiatan".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_kegiatan
 * @property string $nama
 * @property string $jenis
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $sumber_dana
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class LembagaAdatKegiatan extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lembaga_adat_kegiatan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa', 'id_kegiatan'], 'required'],
            [['id_kegiatan'], 'integer'],
            [['tanggal_mulai', 'tanggal_selesai'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['nama'], 'string', 'max' => 120],
            [['jenis'], 'string', 'max' => 100],
            [['sumber_dana'], 'string', 'max' => 20],
            [['kd_desa', 'id_kegiatan'], 'unique', 'targetAttribute' => ['kd_desa', 'id_kegiatan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_kegiatan' => 'Id Kegiatan',
            'nama' => 'Nama',
            'jenis' => 'Jenis',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'sumber_dana' => 'Sumber Dana',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_lembaga' => $this->id_lembaga])->max('id_kegiatan');
        if($mx == null){
            $this->id_kegiatan = 1;
        } else {
            $this->id_kegiatan = $mx +1;
        }
    }

    public function getDokumentasi()
    {
        return $this->hasMany(LembagaAdatKegiatanDokumentasi::className(), ['kd_desa' => 'kd_desa', 'id_lembaga' => 'id_lembaga', 'id_kegiatan' => 'id_kegiatan']);
    }
}
