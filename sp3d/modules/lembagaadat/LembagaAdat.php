<?php

namespace sp3d\modules\lembagaadat;

/**
 * lembagaadat module definition class
 */
class LembagaAdat extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\lembagaadat\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
