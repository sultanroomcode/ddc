<?php
namespace sp3d\modules\lkmd\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use yii\behaviors\BlameableBehavior;
/**

/**
 * This is the model class for table "lkmd".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property string $ditetapkan_oleh
 * @property string $sk_lembaga
 * @property string $pembina_status_ada
 * @property string $pembina_ditetapkan_oleh
 * @property string $pembina_sk_penetapan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Lkmd extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lkmd';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa'], 'required'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['ditetapkan_oleh', 'pembina_ditetapkan_oleh'], 'string', 'max' => 100],
            [['sk_lembaga', 'pembina_sk_penetapan'], 'string', 'max' => 50],
            [['pembina_status_ada'], 'string', 'max' => 10],
            [['kd_desa'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'ditetapkan_oleh' => 'Ditetapkan Oleh',
            'sk_lembaga' => 'Sk Lembaga',
            'pembina_status_ada' => 'Pembina Status Ada',
            'pembina_ditetapkan_oleh' => 'Pembina Ditetapkan Oleh',
            'pembina_sk_penetapan' => 'Pembina Sk Penetapan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diubah Oleh',
            'created_at' => 'Dibuat pada tanggal',
            'updated_at' => 'Diubah pada tanggal',
        ];
    }

    public function getPengurus()
    {
        return $this->hasMany(LkmdPengurus::className(), ['kd_desa' => 'kd_desa']);
    }

    public function getPembina()
    {
        return $this->hasMany(LkmdAnggotaPembina::className(), ['kd_desa' => 'kd_desa']);
    }

    public function getKegiatan()
    {
        return $this->hasMany(LkmdKegiatan::className(), ['kd_desa' => 'kd_desa']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }
}
