<?php
namespace sp3d\modules\lkmd\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "lkmd_anggota_pembina".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_anggota
 * @property string $jabatan
 * @property string $nama
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $pendidikan
 * @property string $status_aktif
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class LkmdAnggotaPembina extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lkmd_anggota_pembina';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa', 'id_anggota'], 'required'],
            [['id_anggota'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['jabatan', 'nama', 'alamat', 'tempat_lahir', 'tanggal_lahir', 'pendidikan', 'status_aktif'], 'string', 'max' => 100],
            [['kd_desa', 'id_anggota'], 'unique', 'targetAttribute' => ['kd_desa', 'id_anggota']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_anggota' => 'Id Anggota',
            'jabatan' => 'Jabatan',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'pendidikan' => 'Pendidikan',
            'status_aktif' => 'Status Aktif',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diubah Oleh',
            'created_at' => 'Dibuat pada tanggal',
            'updated_at' => 'Diubah pada tanggal',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id_anggota');
        if($mx == null){
            $this->id_anggota = 1;
        } else {
            $this->id_anggota = $mx +1;
        }
    }
}
