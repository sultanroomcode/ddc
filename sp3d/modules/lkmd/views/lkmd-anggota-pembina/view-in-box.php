<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
if($model->pembina_status_ada == 'ada'){
	?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-lkmd-pembina', url:'/lkmd/lkmd-anggota-pembina/create?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger">Tambah Pembina</a>
	<?php
	echo '<table class="table table-striped"><tr><th>Nama</th><th>Jabatan</th><th>Tanggal Lahir</th><th>Pendidikan</th><th>Aksi</th></tr>';
	foreach($model->pembina as $v){
	    echo '<tr><th>'.$v->nama.'</th><th>'.$v->jabatan.'</th><th>'.$v->tanggal_lahir.'</th><th>'.substr($v->pendidikan, 2).'</th><th>Aksi</th></tr>';
	}
	echo '</table>';	
} else {
	echo "Lembaga Pemberdayaan Masyarakat Desa/Kelurahan belum memiliki pembina";
}
?>