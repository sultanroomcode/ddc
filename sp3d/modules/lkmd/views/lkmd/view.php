<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lkmd\models\Lkmd */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lkmds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lkmd-view">
    <div class="tile">
        <h2 class="tile-title">LPMD/K</h2>
        <div class="p-10">
            <div id="zona-lkmd-data"></div>
            <div id="zona-lkmd-pengurus"></div>
            <div id="zona-lkmd-pembina"></div>
            <div id="zona-lkmd-kegiatan"></div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>
<?php 
$kd_desa = $model->kd_desa;
$script = <<<JS
goLoad({elm:'#zona-lkmd-data', url:'/lkmd/lkmd/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-lkmd-pengurus', url:'/lkmd/lkmd-pengurus/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-lkmd-pembina', url:'/lkmd/lkmd-anggota-pembina/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-lkmd-kegiatan', url:'/lkmd/lkmd-kegiatan/view-in-box?kd_desa={$kd_desa}'});
JS;
$this->registerJs($script);
?>