<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<a href="javascript:void(0)" onclick="goLoad({url:'/lkmd/lkmd/update?kd_desa=<?=$model->kd_desa?>'});" class="btn btn-danger">Update</a>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'label' => 'Desa',
            'value' => function($m){
                return $m->desa->description;
            }
        ],
        [
            'label' => 'Kecamatan',
            'value' => function($m){
                return $m->kecamatan->description;
            }
        ],
        [
            'label' => 'Kabupaten',
            'value' => function($m){
                return $m->kabupaten->description;
            }
        ],
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ],
]) ?>