<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lkmd\models\LkmdKegiatan */

$this->title = 'Daftar Kegiatan Lembaga Pemberdayaan Masyarakat Desa/Kelurahan';
$this->params['breadcrumbs'][] = ['label' => 'Lkmd Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lkmd-kegiatan-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
