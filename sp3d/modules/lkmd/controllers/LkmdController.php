<?php

namespace sp3d\modules\lkmd\controllers;

use Yii;
use sp3d\modules\lkmd\models\Lkmd;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LkmdController implements the CRUD actions for Lkmd model.
 */
class LkmdController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lkmd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Lkmd::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lkmd model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $kd_desa = Yii::$app->user->identity->id;
        $model = Lkmd::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/lkmd/lkmd/create\'});</script>';
        } else {
            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        }        
    }

    public function actionViewInBox($kd_desa)
    {
        $model = Lkmd::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/lkmd/lkmd/create\'});</script>';
        } else {
            return $this->renderAjax('view-in-box', [
                'model' => $model,
            ]);
        }        
    }

    /**
     * Creates a new Lkmd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Lkmd();
        $model->kd_desa = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Lkmd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa)
    {
        $model = $this->findModel($kd_desa);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Lkmd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lkmd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Lkmd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lkmd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
