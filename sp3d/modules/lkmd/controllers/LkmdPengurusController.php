<?php

namespace sp3d\modules\lkmd\controllers;

use Yii;
use sp3d\modules\lkmd\models\LkmdPengurus;
use sp3d\modules\lkmd\models\Lkmd;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LkmdPengurusController implements the CRUD actions for LkmdPengurus model.
 */
class LkmdPengurusController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LkmdPengurus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LkmdPengurus::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LkmdPengurus model.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionViewInBox($kd_desa)
    {
        $model = Lkmd::findOne(['kd_desa' => $kd_desa]);
        return $this->renderAjax('view-in-box', [
            'model' => $model,
        ]);
    }

    public function actionView($kd_desa, $id_pengurus)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_pengurus),
        ]);
    }

    /**
     * Creates a new LkmdPengurus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa)
    {
        $model = new LkmdPengurus();
        $model->kd_desa = $kd_desa;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LkmdPengurus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_pengurus)
    {
        $model = $this->findModel($kd_desa, $id_pengurus);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'id_pengurus' => $model->id_pengurus]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LkmdPengurus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_pengurus)
    {
        $this->findModel($kd_desa, $id_pengurus)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LkmdPengurus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_pengurus
     * @return LkmdPengurus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_pengurus)
    {
        if (($model = LkmdPengurus::findOne(['kd_desa' => $kd_desa, 'id_pengurus' => $id_pengurus])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
