<?php

namespace sp3d\modules\lkmd;

/**
 * lkmd module definition class
 */
class Lkmd extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\lkmd\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
