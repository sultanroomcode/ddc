<?php
namespace sp3d\modules\api\controllers;

use yii\web\Controller;
use ddcop\models\Operator;
use yii\rest\ActiveController;
/**
 * Default controller for the `api` module
 */
class OperatorController extends ActiveController
{
    public $modelClass = 'ddcop\models\Operator';
    
}
