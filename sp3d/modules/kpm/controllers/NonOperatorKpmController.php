<?php

namespace sp3d\modules\kpm\controllers;

use Yii;
use sp3d\modules\kpm\models\Kpm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KpmController implements the CRUD actions for Kpm model.
 */
class NonOperatorKpmController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kpm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = Kpm::find();

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kpm model.
     * @param string $kd_desa
     * @param integer $id
     * @return mixed
     */
    public function actionView($kd_desa, $id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id),
        ]);
    }

    /**
     * Creates a new Kpm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kpm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kpm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id)
    {
        $model = $this->findModel($kd_desa, $id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kpm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($kd_desa, $id)
    {
        $model = $this->findModel($kd_desa, $id)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the Kpm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @return Kpm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id)
    {
        if (($model = Kpm::findOne(['kd_desa' => $kd_desa, 'id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
