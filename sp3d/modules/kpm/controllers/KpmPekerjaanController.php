<?php

namespace sp3d\modules\kpm\controllers;

use Yii;
use sp3d\modules\kpm\models\KpmPekerjaan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KpmPekerjaanController implements the CRUD actions for KpmPekerjaan model.
 */
class KpmPekerjaanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KpmPekerjaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => KpmPekerjaan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KpmPekerjaan model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpk
     * @return mixed
     */
    public function actionView($kd_desa, $id, $idpk)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id, $idpk),
        ]);
    }

    /**
     * Creates a new KpmPekerjaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id)
    {
        $model = new KpmPekerjaan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id];

            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing KpmPekerjaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpk
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $idpk)
    {
        $model = $this->findModel($kd_desa, $id, $idpk);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id, 'idpk' => $idpk];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing KpmPekerjaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpk
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $idpk)
    {
        $model = $this->findModel($kd_desa, $id, $idpk)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the KpmPekerjaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpk
     * @return KpmPekerjaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $idpk)
    {
        if (($model = KpmPekerjaan::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'idpk' => $idpk])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
