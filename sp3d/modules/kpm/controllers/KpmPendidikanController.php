<?php

namespace sp3d\modules\kpm\controllers;

use Yii;
use sp3d\modules\kpm\models\KpmPendidikan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KpmPendidikanController implements the CRUD actions for KpmPendidikan model.
 */
class KpmPendidikanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KpmPendidikan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => KpmPendidikan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KpmPendidikan model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpd
     * @return mixed
     */
    public function actionView($kd_desa, $id, $idpd)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id, $idpd),
        ]);
    }

    /**
     * Creates a new KpmPendidikan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id)
    {
        $model = new KpmPendidikan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing KpmPendidikan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpd
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $idpd)
    {
        $model = $this->findModel($kd_desa, $id, $idpd);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id, 'idpd' => $idpd];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing KpmPendidikan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpd
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $idpd)
    {
        $model = $this->findModel($kd_desa, $id, $idpd)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the KpmPendidikan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idpd
     * @return KpmPendidikan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $idpd)
    {
        if (($model = KpmPendidikan::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'idpd' => $idpd])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
