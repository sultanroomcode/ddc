<?php

namespace sp3d\modules\kpm\controllers;

use Yii;
use sp3d\modules\kpm\models\KpmPelatihan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KpmPelatihanController implements the CRUD actions for KpmPelatihan model.
 */
class KpmPelatihanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KpmPelatihan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => KpmPelatihan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KpmPelatihan model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionView($kd_desa, $id, $idp)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id, $idp),
        ]);
    }

    /**
     * Creates a new KpmPelatihan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id)
    {
        $model = new KpmPelatihan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing KpmPelatihan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing KpmPelatihan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the KpmPelatihan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return KpmPelatihan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $idp)
    {
        if (($model = KpmPelatihan::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
