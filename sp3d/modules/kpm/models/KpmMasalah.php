<?php
namespace sp3d\modules\kpm\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kpm_masalah".
 *
 * @property int $id_masalah
 * @property int $kpm_id
 * @property string $kd_desa
 * @property string $bidang
 * @property string $masalah
 * @property string $sebab
 * @property string $akibat
 * @property string $lokasi
 * @property string $pelaksana
 * @property string $tanggal
 * @property string $keterangan
 * @property string $dibuat_oleh
 * @property string $dibuat_tanggal
 */
class KpmMasalah extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kpm_masalah';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'dibuat_oleh',
                 'updatedByAttribute' => null,
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kpm_id'], 'integer'],
            [['tanggal'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['bidang', 'masalah', 'sebab', 'akibat', 'lokasi', 'pelaksana', 'keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_masalah' => 'Id Masalah',
            'kpm_id' => 'Kpm ID',
            'kd_desa' => 'Kd Desa',
            'bidang' => 'Bidang',
            'masalah' => 'Masalah',
            'sebab' => 'Sebab',
            'akibat' => 'Akibat',
            'lokasi' => 'Lokasi',
            'pelaksana' => 'Pelaksana',
            'tanggal' => 'Tanggal',
            'keterangan' => 'Keterangan',
            'dibuat_oleh' => 'Dibuat Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
        ];
    }
}
