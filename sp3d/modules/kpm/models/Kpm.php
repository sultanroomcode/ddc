<?php
namespace sp3d\modules\kpm\models;

use sp3d\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kpm".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $id
 * @property integer $nik
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $gender
 * @property string $pendidikan
 * @property string $keterangan
 * @property string $status_aktif
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Kpm extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpm';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id', 'nik'], 'required'],
            [['id'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['status_aktif'], 'string', 'max' => 10],
            [['kd_kecamatan', 'gender', 'pendidikan'], 'string', 'max' => 8],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama'], 'string', 'max' => 150],
            [['tempat_lahir', 'tanggal_lahir'], 'string', 'max' => 255],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_desa' => 'Kd Desa',
            'id' => 'ID',
            'nik' => 'NIK',
            'nama' => 'Nama',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'gender' => 'Gender',
            'pendidikan' => 'Pendidikan',
            'keterangan' => 'Keterangan',
            'status_aktif' => 'Status Aktif',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    public function getPekerjaan()
    {
        return $this->hasMany(KpmPekerjaan::className(), ['kd_desa' => 'kd_desa', 'id' => 'id']);
    }

    public function getPelatihan()
    {
        return $this->hasMany(KpmPelatihan::className(), ['kd_desa' => 'kd_desa', 'id' => 'id']);
    }

    public function getPendidikann()
    {
        return $this->hasMany(KpmPendidikan::className(), ['kd_desa' => 'kd_desa', 'id' => 'id']);
    }

    public function getJadwal()
    {
        return $this->hasMany(KpmJadwal::className(), ['kd_desa' => 'kd_desa', 'kpm_id' => 'id']);
    }

    public function getMasalah()
    {
        return $this->hasMany(KpmMasalah::className(), ['kd_desa' => 'kd_desa', 'kpm_id' => 'id']);
    }

    public function getPotensi()
    {
        return $this->hasMany(KpmPotensiSumberDaya::className(), ['kd_desa' => 'kd_desa', 'kpm_id' => 'id']);
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id');
        if($mx == null){
            $this->id = 1;
        } else {
            $this->id = $mx +1;
        }
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }
}
