<?php
namespace sp3d\modules\kpm\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kpm_potensi_sumber_daya".
 *
 * @property int $id_psd
 * @property int $kpm_id
 * @property string $kd_desa
 * @property string $jenis
 * @property string $jumlah
 * @property string $sb_swa_lokasi
 * @property string $sb_pemerintah
 * @property string $sb_lain
 * @property string $keterangan
 * @property string $dibuat_oleh
 * @property string $dibuat_tanggal
 */
class KpmPotensiSumberDaya extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kpm_potensi_sumber_daya';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'dibuat_oleh',
                 'updatedByAttribute' => null,
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kpm_id'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['jenis', 'jumlah', 'sb_swa_lokasi', 'sb_pemerintah', 'sb_lain', 'keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_psd' => 'Id Psd',
            'kpm_id' => 'Kpm ID',
            'kd_desa' => 'Kd Desa',
            'jenis' => 'Jenis',
            'jumlah' => 'Jumlah/Volume',
            'sb_swa_lokasi' => 'Sumber Swadaya/Lokasi',
            'sb_pemerintah' => 'Sumber Pemerintah (Desa/Kel, Kab/Kota, Prov, Pusat)',
            'sb_lain' => 'Sumber Lain',
            'keterangan' => 'Keterangan',
            'dibuat_oleh' => 'Dibuat Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
        ];
    }
}
