<?php
namespace sp3d\modules\kpm\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kpm_jadwal".
 *
 * @property int $id_jadwal
 * @property int $kpm_id
 * @property string $kd_desa
 * @property string $nama_kegiatan
 * @property string $lokasi
 * @property string $pelaksana
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $keterangan
 * @property string $dibuat_oleh
 * @property string $dibuat_tanggal
 */
class KpmJadwal extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kpm_jadwal';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'dibuat_oleh',
                 'updatedByAttribute' => null,
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kpm_id'], 'integer'],
            [['tanggal_mulai', 'tanggal_selesai'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama_kegiatan', 'lokasi', 'pelaksana', 'keterangan'], 'string', 'max' => 255],
            [['dibuat_oleh'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jadwal' => 'Id Jadwal',
            'kpm_id' => 'Kpm ID',
            'kd_desa' => 'Kd Desa',
            'nama_kegiatan' => 'Nama Kegiatan',
            'lokasi' => 'Lokasi',
            'pelaksana' => 'Pelaksana',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'keterangan' => 'Keterangan',
            'dibuat_oleh' => 'Dibuat Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
        ];
    }
}
