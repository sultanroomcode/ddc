<?php
namespace sp3d\modules\kpm\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kpm_pendidikan".
 *
 * @property string $kd_desa
 * @property integer $id
 * @property integer $idpd
 * @property string $nama_institusi
 * @property string $tahun
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class KpmPendidikan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpm_pendidikan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id', 'idpd'], 'required'],
            [['id', 'idpd'], 'integer'],            
            [['kd_desa'], 'string', 'max' => 12],
            [['nama_institusi'], 'string', 'max' => 150],
            [['tahun'], 'string', 'max' => 9],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'id' => 'ID',
            'idpd' => 'Idpd',
            'nama_institusi' => 'Nama Institusi',
            'tahun' => 'Tahun',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id' => $this->id])->max('idpd');
        if($mx == null){
            $this->idpd = 1;
        } else {
            $this->idpd = $mx +1;
        }
    }
}
