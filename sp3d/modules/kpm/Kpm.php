<?php

namespace sp3d\modules\kpm;

/**
 * kpm module definition class
 */
class Kpm extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'sp3d\modules\kpm\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
