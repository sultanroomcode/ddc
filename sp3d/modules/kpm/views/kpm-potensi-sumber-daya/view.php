<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPotensiSumberDaya */

$this->title = $model->jenis;
$this->params['breadcrumbs'][] = ['label' => 'Kpm Potensi Sumber Dayas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-potensi-sumber-daya-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jenis',
            'jumlah',
            'sb_swa_lokasi',
            'sb_pemerintah',
            'sb_lain',
            'keterangan',
            'dibuat_tanggal',
        ],
    ]) ?>

</div>
