<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPotensiSumberDaya */

$this->title = 'Daftar Potensi dan Sumber Daya';
$this->params['breadcrumbs'][] = ['label' => 'Kpm Potensi Sumber Dayas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-potensi-sumber-daya-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
