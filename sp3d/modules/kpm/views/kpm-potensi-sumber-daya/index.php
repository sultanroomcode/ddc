<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kpm Potensi Sumber Dayas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-potensi-sumber-daya-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kpm Potensi Sumber Daya', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_psd',
            'kpm_id',
            'kd_desa',
            'jenis',
            'jumlah',
            //'sb_swa_lokasi',
            //'sb_pemerintah',
            //'sb_lain',
            //'keterangan',
            //'dibuat_oleh',
            //'dibuat_tanggal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
