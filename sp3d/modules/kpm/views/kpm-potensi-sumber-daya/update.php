<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPotensiSumberDaya */

$this->title = 'Update Daftar Potensi dan Sumber Daya';
$this->params['breadcrumbs'][] = ['label' => 'Kpm Potensi Sumber Dayas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_psd, 'url' => ['view', 'id' => $model->id_psd]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpm-potensi-sumber-daya-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
