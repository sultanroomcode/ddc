<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPekerjaan */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Kpm Pekerjaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-pekerjaan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'idpk' => $model->idpk], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'idpk' => $model->idpk], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'id',
            'idpk',
            'nama_institusi',
            'tahun',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
