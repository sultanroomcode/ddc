<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPekerjaan */

$this->title = 'Update Riwayat Pekerjaan: ' . $model->nama_institusi;
$this->params['breadcrumbs'][] = ['label' => 'Kpm Pekerjaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'idpk' => $model->idpk]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpm-pekerjaan-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
