<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPekerjaan */

$this->title = 'Isi Riwayat Pekerjaan';
$this->params['breadcrumbs'][] = ['label' => 'Kpm Pekerjaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-pekerjaan-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
