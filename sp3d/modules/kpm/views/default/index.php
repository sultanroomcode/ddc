<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">KPM</h2>
            <div class="p-10" id="kpm-area">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$kpm_url = '/kpm/kpm';
$scripts = <<<JS
goLoad({elm:'#kpm-area', url:'{$kpm_url}'});

var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#2A00FF'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}
JS;

$this->registerJs($scripts);