<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\Kpm */

$this->title = 'Update Kpm: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'KPM', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpm-update">    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
