<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\Kpm */

$this->title = 'Create Kpm';
$this->params['breadcrumbs'][] = ['label' => 'Kpms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
