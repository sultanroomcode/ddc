<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\Kpm */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Kpms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#kpm-area\', url:\'/kpm/kpm/update?kd_desa='.$model->kd_desa.'&id='.$model->id.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kabupaten.description',
            'kecamatan.description',
            'desa.description',
            'id',
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'gender',
            'pendidikan',
            'keterangan',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/kpm/kpm-pendidikan/create?kd_desa='.$model->kd_desa.'&id='.$model->id.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Pendidikan</a>' ?>
            <table class="table">
            <tr>
                <th>Nama Institusi</th>
                <th>Tahun</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pendidikann as $v): ?>
                <tr>
                    <td><?= $v->nama_institusi ?></td>
                    <td><?= $v->tahun ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-pendidikan/view?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idpd='.$v->idpd ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-pendidikan/update?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idpd='.$v->idpd ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/kpm/kpm-pendidikan/delete?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idpd='.$v->idpd ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/kpm/kpm-pekerjaan/create?kd_desa='.$model->kd_desa.'&id='.$model->id.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Pekerjaan</a>' ?>
            <table class="table">
            <tr>
                <th>Nama Institusi</th>
                <th>Tahun</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pekerjaan as $v): ?>
                <tr>
                    <td><?= $v->nama_institusi ?></td>
                    <td><?= $v->tahun ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-pekerjaan/view?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idpk='.$v->idpk ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-pekerjaan/update?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idpk='.$v->idpk ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/kpm/kpm-pekerjaan/delete?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idpk='.$v->idpk ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/kpm/kpm-pelatihan/create?kd_desa='.$model->kd_desa.'&id='.$model->id.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Pelatihan</a>' ?>
            <table class="table">
            <tr>
                <th>Nama Institusi</th>
                <th>Tahun</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pelatihan as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->tahun ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-pelatihan/view?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idp='.$v->idp ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-pelatihan/update?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idp='.$v->idp ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/kpm/kpm-pelatihan/delete?<?='kd_desa='.$v->kd_desa.'&id='.$v->id.'&idp='.$v->idp ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/kpm/kpm-jadwal/create?kd_desa='.$model->kd_desa.'&id='.$model->id.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Jadwal</a>' ?>
            <table class="table">
            <tr>
                <th>Nama Kegiatan</th>
                <th>Lokasi</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->jadwal as $v): ?>
                <tr>
                    <td><?= $v->nama_kegiatan ?></td>
                    <td><?= $v->lokasi ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-jadwal/view?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_jadwal='.$v->id_jadwal ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-jadwal/update?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_jadwal='.$v->id_jadwal ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/kpm/kpm-jadwal/delete?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_jadwal='.$v->id_jadwal ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/kpm/kpm-masalah/create?kd_desa='.$model->kd_desa.'&id='.$model->id.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Masalah</a>' ?>
            <table class="table">
            <tr>
                <th>Masalah</th>
                <th>Sebab</th>
                <th>Akibat</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->masalah as $v): ?>
                <tr>
                    <td><?= $v->masalah ?></td>
                    <td><?= $v->sebab ?></td>
                    <td><?= $v->akibat ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-masalah/view?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_masalah='.$v->id_masalah ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-masalah/update?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_masalah='.$v->id_masalah ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/kpm/kpm-masalah/delete?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_masalah='.$v->id_masalah ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/kpm/kpm-potensi-sumber-daya/create?kd_desa='.$model->kd_desa.'&id='.$model->id.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Potensi Sumber Daya</a>' ?>
            <table class="table">
            <tr>
                <th>Jenis</th>
                <th>Jumlah</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->potensi as $v): ?>
                <tr>
                    <td><?= $v->jenis ?></td>
                    <td><?= $v->jumlah ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-potensi-sumber-daya/view?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_psd='.$v->id_psd ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/kpm/kpm-potensi-sumber-daya/update?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_psd='.$v->id_psd ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/kpm/kpm-potensi-sumber-daya/delete?<?='kd_desa='.$v->kd_desa.'&id='.$v->kpm_id.'&id_psd='.$v->id_psd ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>
    .
</div>
<?php
$urlBack = '/kpm/kpm/view?kd_desa='.$model->kd_desa.'&id='.$model->id;
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#kpm-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);