<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KPM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-index">
    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kpm-area', url:'/kpm/kpm/create'})" class="btn btn-success"><i class="fa fa-plus"></i> KPM</a>
    </p>

    <table class="table">
        <tr>
            <th>NIK</th>
            <th>Nama</th>
            <th>TTL</th>
            <th>Gender</th>
            <th>Pendidikan</th>
            <th>Aksi</th>
        </tr>
    <?php foreach ($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->nik ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->tempat_lahir.', '.$v->tanggal_lahir ?></td>
            <td><?= $v->gender ?></td>
            <td><?= substr($v->pendidikan, 2) ?></td>
            <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#kpm-area\', url:\'/kpm/kpm/update?kd_desa='.$v->kd_desa.'&id='.$v->id.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#kpm-area\', url:\'/kpm/kpm/view?kd_desa='.$v->kd_desa.'&id='.$v->id.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
