<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\Kpm */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = Yii::$app->user->identity->desa->kd_desa;
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
}

$model->genNum();
?>

<div class="kpm-form">
    <div class="row">
        <div class="col-md-5">
            <?php $form = ActiveForm::begin($arrFormConfig); ?>

            <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'nik')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'gender')->dropdownList([
                'L' => 'Laki-Laki', 
                'P' => 'Perempuan'
            ]) ?>

            <?= $form->field($model, 'pendidikan')->dropdownList([
                '0|SD' => 'Sekolah Dasar', 
                '1|SMP' => 'Sekolah Menengah Pertama', 
                '2|SMA' => 'Sekolah Menengah Atas', 
                '3|D1' => 'Diploma I', 
                '4|D2' => 'Diploma II', 
                '5|D3' => 'Diploma III', 
                '6|D4' => 'Diploma IV', 
                '7|S1' => 'Sarjana', 
                '8|S2' => 'Magister', 
                '9|S3' => 'Doktoral', 
            ]) ?>

            <?= $form->field($model, 'status_aktif')->dropdownList([
                'aktif' => 'Aktif', 
                'non-aktif' => 'Non-Aktif'
            ]) ?>

            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#kpm-area', url:'/kpm/kpm/index'})" class="btn btn-success"><i class="fa fa-backward"></i></a>
                </div>
            </div>

            <?php ActiveForm::end(); ?>  
        </div>
    </div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('#kpm-tanggal_lahir').datetimepicker({
    format:'Y-m-d',
    mask:true
});

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#kpm-area', url : '/kpm/kpm/index'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);