<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmMasalah */

$this->title = 'Update Daftar Masalah';
$this->params['breadcrumbs'][] = ['label' => 'Kpm Masalahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_masalah, 'url' => ['view', 'id' => $model->id_masalah]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpm-masalah-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
