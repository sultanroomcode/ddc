<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kpm Masalahs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-masalah-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kpm Masalah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_masalah',
            'kpm_id',
            'kd_desa',
            'bidang',
            'masalah',
            //'sebab',
            //'akibat',
            //'lokasi',
            //'pelaksana',
            //'tanggal',
            //'keterangan',
            //'dibuat_oleh',
            //'dibuat_tanggal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
