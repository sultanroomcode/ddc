<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-8',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_desa = $data['kd_desa'];
    $model->kpm_id = $data['id'];
}
$urlback = '/kpm/kpm/view?kd_desa='.$data['kd_desa'].'&id='.$data['id'];
?>

<div class="kpm-masalah-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kpm_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'bidang')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'masalah')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'sebab')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'akibat')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'lokasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pelaksana')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
$('#kpmmasalah-tanggal').datetimepicker({
    format:'Y-m-d',
    mask:true
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#kpm-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);