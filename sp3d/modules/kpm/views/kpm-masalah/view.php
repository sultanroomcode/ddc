<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmMasalah */

$this->title = $model->masalah;
$this->params['breadcrumbs'][] = ['label' => 'Kpm Masalahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-masalah-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bidang',
            'masalah',
            'sebab',
            'akibat',
            'lokasi',
            'pelaksana',
            'tanggal',
            'keterangan',
            'dibuat_tanggal',
        ],
    ]) ?>

</div>
