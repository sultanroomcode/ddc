<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmJadwal */

$this->title = $model->nama_kegiatan;
$this->params['breadcrumbs'][] = ['label' => 'Kpm Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-jadwal-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_jadwal',
            'nama_kegiatan',
            'lokasi',
            'pelaksana',
            'tanggal_mulai',
            'tanggal_selesai',
            'keterangan',
            'dibuat_tanggal',
        ],
    ]) ?>

</div>
