<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kpm Jadwals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpm-jadwal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kpm Jadwal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_jadwal',
            'kpm_id',
            'kd_desa',
            'nama_kegiatan',
            'lokasi',
            //'pelaksana',
            //'tanggal_mulai',
            //'tanggal_selesai',
            //'keterangan',
            //'dibuat_oleh',
            //'dibuat_tanggal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
