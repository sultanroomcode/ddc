<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPendidikan */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = $data['kd_desa'];
    $model->id = $data['id'];
    $model->genNum();//generate number
}

$urlback = '/kpm/kpm/view?kd_desa='.$data['kd_desa'].'&id='.$data['id'];
?>

<div class="kpm-pendidikan-form">
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin($arrFormConfig); ?>

            <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'idpd')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'nama_institusi')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#kpm-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);