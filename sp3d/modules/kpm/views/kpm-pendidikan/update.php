<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kpm\models\KpmPendidikan */

$this->title = 'Update Riwayat Pendidikan: ' . $model->nama_institusi;
$this->params['breadcrumbs'][] = ['label' => 'Kpm Pendidikans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'idpd' => $model->idpd]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpm-pendidikan-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
