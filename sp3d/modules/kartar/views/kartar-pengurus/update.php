<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lkmd\models\LkmdPengurus */

$this->title = 'Update Pengurus Karang Taruna';
?>
<div class="lkmd-pengurus-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
