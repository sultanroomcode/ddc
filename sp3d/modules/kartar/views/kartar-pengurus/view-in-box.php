<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-kartar-pengurus', url:'/kartar/kartar-pengurus/create?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger">Tambah Pengurus</a>

<?php 
echo '<table class="table table-striped"><tr><th>Nama</th><th>Jabatan</th><th>Tanggal Lahir</th><th>Pendidikan</th><th>Pelatihan</th><th>Aksi</th></tr>';
foreach($model->pengurus as $v){
    echo '<tr><td>'.$v->nama.'</td><td>'.$v->jabatandetail->nama_jabatan.'</td><td>'.$v->tanggal_lahir.'</td><td>'.substr($v->pendidikan, 2).'</td><td>'.count($v->pelatihan).'</td><td><a href="javascript:void(0)" onclick="openModalXyf({welm:\'600px\',url:\'/kartar/kartar-pengurus/view?id_pengurus='.$v->id_pengurus.'&kd_desa='.$v->kd_desa.'\'})" class="btn btn-danger">View</a></td></tr>';
}
echo '</table>';	
?>