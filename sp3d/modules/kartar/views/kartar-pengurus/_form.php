<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use sp3d\models\transaksi\Sp3dMasterJabatan;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-6',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
    $model->genNum();
}

$jabatan = ArrayHelper::map(Sp3dMasterJabatan::find()->where(['tipe' => 'kartar', 'status' => 10])->andWhere(['like', 'id', 'pg-kartar-%', false])->orderBy('urut')->all(),'id','nama_jabatan');

$urlback = 'goLoad({elm:\'#zona-kartar-pengurus\', url:\'/kartar/kartar-pengurus/view-in-box?kd_desa='.$model->kd_desa.'\'});';
?>

<div class="kartar-pengurus-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_pengurus')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'jabatan')->dropdownList($jabatan) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sk_jabatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pendidikan')->dropdownList([
        '0|SD' => 'Sekolah Dasar', 
        '1|SMP' => 'Sekolah Menengah Pertama', 
        '2|SMA' => 'Sekolah Menengah Atas', 
        '3|D1' => 'Diploma I', 
        '4|D2' => 'Diploma II', 
        '5|D3' => 'Diploma III', 
        '6|D4' => 'Diploma IV', 
        '7|S1' => 'Sarjana', 
        '8|S2' => 'Magister', 
        '9|S3' => 'Doktoral', 
    ]) ?>

    <?= $form->field($model, 'status_aktif')->dropdownList(['aktif' => 'Aktif', 'tidak' => 'Tidak']) ?>
    <div class="col-md-2"></div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="goLoad({elm:'#zona-kartar-pengurus', url:'/kartar/kartar-pengurus/view-in-box?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger">Kembali</a>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
<?php 
$script = <<<JS
$('#kartarpengurus-tanggal_lahir').datetimepicker({
    format:'Y-m-d',
    mask:true
});

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);