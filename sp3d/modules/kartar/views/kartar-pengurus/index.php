<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lkmd Penguruses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lkmd-pengurus-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lkmd Pengurus', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_pengurus',
            'jabatan',
            //'nama',
            //'alamat',
            //'tempat_lahir',
            //'tanggal_lahir',
            //'sk_jabatan',
            //'pendidikan',
            //'status_aktif',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
