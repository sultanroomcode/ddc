<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kartar\models\Lkmd */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lkmds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kartar-view">
    <div class="tile">
        <h2 class="tile-title">Karang Taruna</h2>
        <div class="p-10">
            <div id="zona-kartar-data"></div>
            <div id="zona-kartar-pembina"></div>
            <div id="zona-kartar-pengurus"></div>
            <div id="zona-kartar-sumberdana"></div>
            <div id="zona-kartar-kegiatan"></div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>
<?php 
$kd_desa = $model->kd_desa;
$script = <<<JS
goLoad({elm:'#zona-kartar-data', url:'/kartar/kartar/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-kartar-pengurus', url:'/kartar/kartar-pengurus/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-kartar-pembina', url:'/kartar/kartar-anggota-pembina/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-kartar-sumberdana', url:'/kartar/kartar-sumber-dana/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-kartar-kegiatan', url:'/kartar/kartar-kegiatan/view-in-box?kd_desa={$kd_desa}'});
JS;
$this->registerJs($script);
?>