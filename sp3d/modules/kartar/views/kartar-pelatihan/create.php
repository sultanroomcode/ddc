<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kartar\models\KartarPelatihan */

$this->title = 'Riwayat Pelatihan/Workshop/Seminar/Bimtek';
$this->params['breadcrumbs'][] = ['label' => 'Kartar Pelatihans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kartar-pelatihan-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
