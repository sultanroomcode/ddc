<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['breadcrumbs'][] = ['label' => 'Kartar Pelatihan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#list-catatan-pelatihan', url:'/kartar/kartar-pelatihan/create?id=<?=$data['id']?>&kd_desa=<?=$data['kd_desa']?>&tipe=<?=$data['tipe']?>'})" class="btn btn-danger">Tambah Pelatihan</a>

<table class="table">
    <thead>
        <tr>
            <th>Tahun</th>
            <th>Nama Pelatihan</th>
            <th>Penyelenggara</th>
            <th>Aksi</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($model as $v) { ?>
        <tr>
            <td><?= $v->tahun ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->lembaga ?></td>
            <td>Aksi</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
