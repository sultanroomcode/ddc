<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kartar\models\LkmdAnggotaPembina */

$this->title = 'Pembina Karang Taruna';
$this->params['breadcrumbs'][] = ['label' => 'Lkmd Anggota Pembinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kartar-anggota-pembina-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
