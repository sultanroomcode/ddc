<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kartar\models\LkmdAnggotaPembina */

$this->title = 'Update Pembina Karang Taruna : ';
?>
<div class="kartar-anggota-pembina-update">
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
