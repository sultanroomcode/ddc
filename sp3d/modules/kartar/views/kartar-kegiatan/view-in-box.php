<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-kartar-kegiatan', url:'/kartar/kartar-kegiatan/create?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger">Tambah Kegiatan</a>

<?php 
echo '<table class="table table-striped"><tr><th>Nama</th><th>Jenis</th><th>Tanggal Mulai</th><th>Sumber Dana</th><th>Aksi</th></tr>';
foreach($model->kegiatan as $v){
    echo '<tr><td>'.$v->nama.'</td><td>'.$v->jenis.'</td><td>'.date('Y-m', strtotime($v->tanggal_mulai)).'</td><td>'.$v->sumber_dana.'</td><td>Aksi</td></tr>';
}
echo '</table>';	
?>