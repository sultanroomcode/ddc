<?php
namespace sp3d\modules\kartar\controllers;

use Yii;
use sp3d\modules\kartar\models\Kartar;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KartarController implements the CRUD actions for Kartar model.
 */
class KartarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kartar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Kartar::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kartar model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $kd_desa = Yii::$app->user->identity->id;
        $model = Kartar::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/kartar/kartar/create\'});</script>';
        } else {
            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        }        
    }

    public function actionViewInBox($kd_desa)
    {
        $model = Kartar::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/kartar/kartar/create\'});</script>';
        } else {
            return $this->renderAjax('view-in-box', [
                'model' => $model,
            ]);
        }        
    }

    /**
     * Creates a new Kartar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kartar();
        $model->kd_desa = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kartar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa)
    {
        $model = $this->findModel($kd_desa);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kartar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kartar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Kartar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kartar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
