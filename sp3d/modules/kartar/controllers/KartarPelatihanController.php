<?php
namespace sp3d\modules\kartar\controllers;

use Yii;
use sp3d\modules\kartar\models\KartarPelatihan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KartarPelatihanController implements the CRUD actions for KartarPelatihan model.
 */
class KartarPelatihanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KartarPelatihan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => KartarPelatihan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KartarPelatihan model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionView($kd_desa, $id, $tipe)
    {
        return $this->renderAjax('view', [
            'data' => ['kd_desa' => $kd_desa, 'id' => $id, 'tipe' => $tipe],
            'model' => KartarPelatihan::findAll(['kd_desa' => $kd_desa, 'id' => $id, 'tipe' => $tipe]),
        ]);
    }

    /**
     * Creates a new KartarPelatihan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id, $tipe)
    {
        $model = new KartarPelatihan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id, 'tipe' => $tipe];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing KartarPelatihan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing KartarPelatihan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the KartarPelatihan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return KartarPelatihan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $idp)
    {
        if (($model = KartarPelatihan::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
