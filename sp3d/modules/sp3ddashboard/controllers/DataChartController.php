<?php
namespace sp3d\modules\sp3ddashboard\controllers;
use yii\web\Controller;
use Yii;

use sp3d\models\User;
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\models\Pasardesa;
use sp3d\models\DataLaporan;
use sp3d\models\transaksi\Sp3dPerangkatDesa;
use sp3d\models\DesaCount;
use sp3d\models\KecamatanCount;
use sp3d\models\KabupatenCount;
use sp3d\models\ProvinsiCount;
class DataChartController extends Controller
{
    public function actionDataPemerintahDesa()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['needle']));//subjekval -> needle
        $model = new DataLaporan();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();
        return $this->renderAjax('pemerintah-desa/data-'.$data['modul'], [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionDataBumdes()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['needle']));//subjekval -> needle
        $model = new DataLaporan();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();    
        return $this->renderAjax('bumdes/data-'.$data['modul'], [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionDataPasarDesa()
    {
        $data = Yii::$app->request->get();        
        return $this->renderAjax('pasar-desa/data-'.$data['modul'], [
            'data' => $data
        ]);
    }

    /*public function actionDataKades()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        
        $model = Sp3dPerangkatDesa::find();
        $model->orderBy('user'); 

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-kades-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataPerangkat()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Sp3dPerangkatDesa::find();
        $model->joinWith('jabatanproperti');        
        $model->orderBy('sp3d_perangkat_desa.user, sp3d_master_jabatan.urut');        

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'desa']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'desa']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'desa']);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-perangkat-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataBpd()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Sp3dPerangkatDesa::find();
        $model->joinWith('jabatanproperti');        
        $model->orderBy('sp3d_perangkat_desa.user, sp3d_master_jabatan.urut');

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->where('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'bpd']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->where('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'bpd']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model->where('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'bpd']);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-bpd-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataBumdes()
    {
        $data = Yii::$app->request->get();        
        return $this->renderAjax('dc-bumdes-'.$data['modul'], [
            'data' => $data
        ]);
    }

    public function actionDataPasardesa()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Pasardesa::find();

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-pasardesa-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataTableProvinsi()
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_desa' => $data['desa-input']]);
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kecamatan' => $data['kecamatan-input']]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $model = KecamatanCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kabupaten' => $data['kabupaten-input']]); 
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = KabupatenCount::find()->where(['tahun' => $data['tahun-anggaran']]);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-table-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataOption($type='kabupaten', $kode=null)
    {
        switch ($type) {
            // case 'kabupaten':
            //     $model = User::find()->where(['type' => $tahun]);
            // break;
            case 'kecamatan':
                $model = User::find()->where(['type' => 'kecamatan']);
            break;
            case 'desa':
                $model = User::find()->where(['type' => 'desa']);
            break;
        }

        $model->andWhere('id LIKE :query')->addParams([':query'=> $kode.'%']);

        return $this->renderPartial('option', [
            'model' => $model,
            'type' => $type,
            'kode' => $kode,
        ]);
    } */
}
