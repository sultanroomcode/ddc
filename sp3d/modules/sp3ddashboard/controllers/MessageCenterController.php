<?php

namespace sp3d\modules\sp3ddashboard\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dMessageThread;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\web\Response;
/**
 * MessageCenterController implements the CRUD actions for Sp3dMessageThread model.
 */
class MessageCenterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sp3dMessageThread models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sp3dMessageThread::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sp3dMessageThread model.
     * @param string $id
     * @param string $from
     * @param string $to
     * @return mixed
     */
    public function actionView($id, $from, $to)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id, $from, $to),
        ]);
    }

    public function actionViewMessageAll()
    {
        return $this->renderAjax('view-message-all');
    }

    /**
     * Creates a new Sp3dMessageThread model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sp3dMessageThread();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Sp3dMessageThread model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $from
     * @param string $to
     * @return mixed
     */
    public function actionUpdate($id, $from, $to)
    {
        $model = $this->findModel($id, $from, $to);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sp3dMessageThread model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $from
     * @param string $to
     * @return mixed
     */
    public function actionDelete($id, $from, $to)
    {
        $this->findModel($id, $from, $to)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sp3dMessageThread model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $from
     * @param string $to
     * @return Sp3dMessageThread the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $from, $to)
    {
        if (($model = Sp3dMessageThread::findOne(['id' => $id, 'from' => $from, 'to' => $to])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMessageData()
    {
        //khusus selain desa
        $dttable = Yii::$app->request->get();

        // var_dump($dttable);
        // print_r($dttable);
        $data = Sp3dMessageThread::find()->select(['from', 'to', 'type', 'body_content']);
        $datap = $data->offset($dttable['start'])->limit($dttable['length']);//data paging

        if($dttable['kabupaten'] != '-' && $dttable['kecamatan'] == ''){
            $datap->where(['LIKE', 'from', $dttable['kabupaten'].'%', false]);
            $datap->orWhere(['LIKE', 'to', $dttable['kabupaten'].'%', false]);
        }

        if($dttable['kecamatan'] != ''){
            $datap->where(['LIKE', 'from', $dttable['kecamatan'].'%', false]);
            $datap->orWhere(['LIKE', 'to', $dttable['kecamatan'].'%', false]);
        }

        if($dttable['search']['value'] != ''){
            $datap->andWhere(['LIKE', 'body_content', $dttable['search']['value']]);
        }

        $dataserve = ArrayHelper::toArray($datap->all(), ['sp3d\models\transaksi\Sp3dMessageThread' => 
            [
            'from',
            'to',
            'body_content',
            'type',
            'action' => function($p){
                return '<a href="javascript:void(0)" onclick="openModalXy({url:\'/umum/user/detail-pengguna?id='.$p->from.'\'})" class="btn btn-sm">Balas</a>';
            }]
        ]);

        $foor = [
            'draw' => (int) $dttable['draw'],
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $datap->count(),
            'data' => $dataserve
        ];
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }
}
