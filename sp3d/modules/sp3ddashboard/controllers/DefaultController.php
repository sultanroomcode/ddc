<?php
namespace sp3d\modules\sp3ddashboard\controllers;

use yii\web\Controller;

/**
 * Default controller for the `sp3ddashboard` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');//sp3d
    }

    public function actionSdm()
    {
        return $this->renderAjax('sdm');
    }

    public function actionSda()
    {
        return $this->renderAjax('sda');
    }

    public function actionPotensiDesa()
    {
        return $this->renderAjax('potensi-desa');
    }

    public function actionKelembagaan()
    {
        return $this->renderAjax('kelembagaan');
    }

    public function actionAsetDesa()
    {
        return $this->renderAjax('aset-desa');
    }

    public function actionBumdes()
    {
        return $this->renderAjax('bumdes');
    }

    public function actionPendampingDesa()
    {
        return $this->renderAjax('pendamping-desa');
    }

    public function actionRpjmDesa()
    {
        return $this->renderAjax('rpjm-desa');
    }

    public function actionGrafik()
    {
        return $this->renderAjax('grafik');
    }

    public function actionGrafikPerangkat()
    {
        return $this->renderAjax('grafik-perangkat');
    }

    public function actionDownload()
    {
        return $this->renderAjax('download');
    }

    public function actionDokumen()
    {
        return $this->renderAjax('dokumen');
    }

    public function actionApbdesa()//khusus provinsi
    {
        return $this->renderAjax('apbdesa');
    }

    public function actionApbdesaSorting()//khusus provinsi
    {
        return $this->renderAjax('apbdesa-sorting');
    }

    public function actionApbdesDesa()//khusus provinsi
    {
        return $this->renderAjax('apbdes-desa');
    }

    public function actionGrafikDesa()
    {
        return $this->renderAjax('grafik-desa');
    }

    public function actionDevelopingState()
    {
        return $this->renderAjax('developing-state');
    }
}
