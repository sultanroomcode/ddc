<?php
namespace sp3d\modules\sp3ddashboard\controllers;
use yii\web\Controller;
use Yii;

use sp3d\modules\bumdes\models\Bumdes;

use sp3d\models\User;
use sp3d\models\DataLaporan;
use sp3d\models\Pasardesa;
use sp3d\models\transaksi\Sp3dPerangkatDesa;
use sp3d\models\DesaCount;
use sp3d\models\KecamatanCount;
use sp3d\models\KabupatenCount;
use sp3d\models\ProvinsiCount;

use sp3d\modules\bumdes\models\BumdesModal;
use sp3d\modules\bumdes\models\BumdesUnitUsaha;

use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;

class DataExportController extends Controller
{

    public function actionDataTableOutDesa()
    {
        // data-table-out-desa
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model = DesaCount::find();
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model = DesaCount::find();
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = DesaCount::find();
        }

        $model->where(['tahun' => $data['tahun-anggaran']]);
        $model->orderBy('dana_pendapatan DESC');

        $data['type'] = $type;
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'kabupatenLabel.description',],
                ['attribute' => 'kecamatanLabel.description',],
                ['attribute' => 'desaLabel.description',],
                ['attribute' => 'dana_pendapatan',],
                ['attribute' => 'dana_pendapatan','value' => function($model){ return number_format($model->dana_pendapatan,2,",","."); }],
                ['attribute' => 'dana_rab',],
                ['attribute' => 'dana_rab','value' => function($model){ return number_format($model->dana_rab,2,",","."); }],
            ],
        ]);
        $timeId = time();
        $as = $exporter->send('file-rekapitulasi-'.$timeId.'.xls');        
        // echo "<fieldset><legend>Export</legend>
        // File berhasil digenerate<br>
        // <a href='../file-perangkat-".$timeId.".xls' class='btn btn-default'>Download File</a></fieldset>";
    }

    public function actionDataKades()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
        }

        $model->select(['user' , 'nik', 'nama', 'tempat_lahir', 'tanggal_lahir', 'pendidikan']);

        $data['type'] = $type;
        
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'user',],
                ['attribute' => 'nik',],
                ['attribute' => 'nama',],
                ['attribute' => 'tempat_lahir',],
                ['attribute' => 'tanggal_lahir', 'value' => function($model){ return date('d-F-Y', strtotime($model->tanggal_lahir)); }],
                ['attribute' => 'pendidikan',],
            ],
        ]);
        $timeId = time();
        $exporter->send('file-kades-'.$timeId.'.xls');
        // echo "<a href='../file-kades-".$timeId.".xls' class='btn btn-default'>Download File</a>";
    }

    public function actionDataPerangkat()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model = Sp3dPerangkatDesa::find();
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('tipe = :query1')->addParams([':query1'=> 'desa']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model = Sp3dPerangkatDesa::find();
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('tipe = :query1')->addParams([':query1'=> 'desa']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = Sp3dPerangkatDesa::find();
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('tipe = :query1')->addParams([':query1'=> 'desa']);
        }

        $model->select(['user' , 'nik', 'nama', 'jabatan', 'tempat_lahir', 'tanggal_lahir', 'pendidikan']);
        $data['type'] = $type;
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'user',],
                ['attribute' => 'nik',],
                ['attribute' => 'nama',],
                ['attribute' => 'jabatan',],
                ['attribute' => 'tempat_lahir',],
                ['attribute' => 'tanggal_lahir', 'value' => function($model){ return date('d-F-Y', strtotime($model->tanggal_lahir)); }],
                ['attribute' => 'pendidikan',],
            ],
        ]);
        $timeId = time();
        $as = $exporter->send('file-perangkat-'.$timeId.'.xls');        
        // echo "<fieldset><legend>Export</legend>
        // File berhasil digenerate<br>
        // <a href='../file-perangkat-".$timeId.".xls' class='btn btn-default'>Download File</a></fieldset>";
    }

    public function actionDataBpd()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model = Sp3dPerangkatDesa::find();
            $model->where('tipe = :query1')->addParams([':query1'=> 'bpd']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model = Sp3dPerangkatDesa::find();
            $model->where('tipe = :query1')->addParams([':query1'=> 'bpd']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = Sp3dPerangkatDesa::find();
            $model->where('tipe = :query1')->addParams([':query1'=> 'bpd']);
        }

        $model->select(['user' , 'nik', 'nama', 'jabatan', 'tempat_lahir', 'tanggal_lahir', 'pendidikan']);
        $data['type'] = $type;
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'user',],
                ['attribute' => 'nik',],
                ['attribute' => 'nama',],
                ['attribute' => 'jabatan',],
                ['attribute' => 'tempat_lahir',],
                ['attribute' => 'tanggal_lahir', 'value' => function($model){ return date('d-F-Y', strtotime($model->tanggal_lahir)); }],
                ['attribute' => 'pendidikan',],
            ],
        ]);
        $timeId = time();
        $exporter->send('file-bpd-'.$timeId.'.xls');
        // echo "<a href='../file-bpd-".$timeId.".xls' class='btn btn-default'>Download File</a>";
    }

    public function actionDataBumdesChart()
    {
        $data = Yii::$app->request->get();
        switch ($data['tipedata']) {
            case 'keuntungan':
            case 'modal':
            case 'omset':
                $pool = [
                    '0-1jt' => ['min' => 0, 'max' => 1000000],
                    '1-5jt' => ['min' => 1000001, 'max' => 5000000],
                    '5-10jt' => ['min' => 5000001, 'max' => 10000000],
                    '10-50jt' => ['min' => 10000001, 'max' => 50000000],
                    '50jt-keatas' => ['min' => 5000001, 'max' => null]
                ];

                $model = BumdesModal::find();
                if($data['needle'] == '50jt-keatas'){
                    $model->where(['>', $data['tipedata'], $pool[$data['needle']]['min'] ]);
                } else {
                    $model->where(['>=', $data['tipedata'], $pool[$data['needle']]['min']]);
                    $model->andWhere(['<=', $data['tipedata'], $pool[$data['needle']]['max']]);
                }

                $attributes = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'bumdes.nama_bumdes',],
                    ['attribute' => 'bumdes.tahun_berdiri',],
                    ['attribute' => $data['tipedata']],
                ];
            break;
            case 'tahun':
                $data['needle'] = trim(urldecode($data['needle']));
                $model = Bumdes::find();
                $model->where(['tahun_berdiri' => $data['needle']]);

                $attributes = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'nama_bumdes',],
                    ['attribute' => 'tahun_berdiri',],
                ];
            break;
            case 'region':
                $data['needle'] = trim(urldecode($data['needle']));
                $model = Bumdes::find();
                $model->join('LEFT JOIN','user','bumdes.kd_kabupaten = user.id COLLATE utf8_general_ci');
                $model->where(['user.id' => $data['needle']]);
                $model->orderBy('kd_desa');

                $attributes = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'nama_bumdes',],
                    ['attribute' => 'tahun_berdiri',],
                ];
            break;
            case 'unit-usaha':
                $data['needle'] = trim(urldecode($data['needle']));
                $model = BumdesUnitUsaha::find();
                $model->where(['unit_usaha' => $data['needle']]);

                $attributes = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'bumdes.nama_bumdes',],
                    ['attribute' => 'bumdes.tahun_berdiri',],
                    ['attribute' => 'unit_usaha'],
                ];
            break;            
            default:
            break;
        }

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 200, // export batch size
                ],
            ]),
            'columns' => $attributes
        ]);
        $timeId = time();
        $exporter->send('file-bumdes-'.$data['tipedata'].$timeId.'.xls');
    }

    public function actionDataPemerintahDesaChart()
    {
        $data = Yii::$app->request->get();

        $model = new DataLaporan();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->result(),
                'pagination' => [
                    'pageSize' => 200, // export batch size
                ],
            ]),
            'columns' => $model->attributes()
        ]);
        $timeId = time();
        $exporter->send('file-desa-'.$data['subjek'].$timeId.'.xls');
    }

    public function actionDataBumdes()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Bumdes::find();
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $model->select(['kd_desa' , 'nama_bumdes', 'tahun_berdiri', 'alamat', 'legal_perdes_no', 'legal_skdes_bumdes_no']);
        $data['type'] = $type;
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'kd_desa',],
                ['attribute' => 'nama_bumdes',],
                ['attribute' => 'tahun_berdiri',],
                ['attribute' => 'alamat',],
                ['attribute' => 'legal_perdes_no',],
                ['attribute' => 'legal_skdes_bumdes_no',],
            ],
        ]);
        $timeId = time();
        $exporter->send('file-bumdes-'.$timeId.'.xls');
    }

    public function actionDataPasardesa()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Pasardesa::find();
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $model->select(['kd_desa' , 'nama', 'kepemilikan_tanah', 'legalitas', 'no_legalitas', 'tahun']);
        $data['type'] = $type;
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'kd_desa',],
                ['attribute' => 'nama',],
                ['attribute' => 'tahun',],
                ['attribute' => 'kepemilikan_tanah',],
                ['attribute' => 'legalitas',],
                ['attribute' => 'no_legalitas',],
            ],
        ]);
        $timeId = time();
        $exporter->send('file-pasardesa-'.$timeId.'.xls');
    }

    public function actionDataTableProvinsi()
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_desa' => $data['desa-input']]);
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kecamatan' => $data['kecamatan-input']]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $model = KecamatanCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kabupaten' => $data['kabupaten-input']]); 
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = KabupatenCount::find()->where(['tahun' => $data['tahun-anggaran']]);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-table-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataOption($type='kabupaten', $kode=null)
    {
        switch ($type) {
            // case 'kabupaten':
            //     $model = User::find()->where(['type' => $tahun]);
            // break;
            case 'kecamatan':
                $model = User::find()->where(['type' => 'kecamatan']);
            break;
            case 'desa':
                $model = User::find()->where(['type' => 'desa']);
            break;
        }

        $model->andWhere('id LIKE :query')->addParams([':query'=> $kode.'%']);

        return $this->renderPartial('option', [
            'model' => $model,
            'type' => $type,
            'kode' => $kode,
        ]);
    }
}
