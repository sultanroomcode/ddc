<?php
namespace sp3d\modules\sp3ddashboard\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use sp3d\models\transaksi\Sp3dFileIndikator;
/**
 * Default controller for the `sp3ddashboard` module
 */
class IndikatorDanaDesaController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');//sp3d
    }

    public function actionListVerifikasi()//khusus kabupaten
    {
        $model = Sp3dFileIndikator::find()->where(['kd_kabupaten' => Yii::$app->user->identity->id]);
        return $this->renderAjax('list-verifikasi', [
            'model' => $model
        ]);//sp3d
    }

        public function actionRapbdes()
    {
        return $this->renderAjax('rapbdes');//sp3d
    }

    public function actionPenggunaanDanaDesa()
    {
        return $this->renderAjax('penggunaan-dana-desa');//sp3d
    }

    public function actionPertanggungjawabanDanaDesa()
    {
        return $this->renderAjax('pertanggungjawaban-dana-desa');//sp3d
    }
    //tools
    public function actionEmbedPdf($id)
    {
        $model = Sp3dFileIndikator::findOne(['id' => $id]);
        return $this->renderAjax('embed-pdf', ['model' => $model]);//sp3d
    }

    public function actionEmbedAndVerificationForm($id)//kabupaten
    {
        $model = Sp3dFileIndikator::findOne(['id' => $id, 'kd_kabupaten' => Yii::$app->user->identity->id]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('embed-pdf-verifikasi', ['model' => $model]);//sp3d
        }
    }

    public function actionListData($tipe)//khuusus desa
    {
        $model = Sp3dFileIndikator::find()->where(['tipe_indikator' => $tipe, 'kd_desa' => Yii::$app->user->identity->id]);
        return $this->renderAjax('list-data', [
            'tipe_indikator' => $tipe,
            'model' => $model
        ]);//sp3d
    }

    public function actionData()
    {
        $dp = Sp3dFileIndikator::find();
        $foo = $dp->all();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $foo;
    }

    public function actionFormData($tipe)
    {
        $model = new Sp3dFileIndikator();
        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('form-data', [
                'model' => $model,
                'tipe_indikator' => $tipe
            ]);//sp3d
        }
    }
}
