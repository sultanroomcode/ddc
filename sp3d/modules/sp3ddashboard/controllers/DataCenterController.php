<?php
namespace sp3d\modules\sp3ddashboard\controllers;
use yii\web\Controller;
use Yii;

use sp3d\models\User;
use sp3d\models\DataLaporan;
use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\bumdes\models\BumdesModal;
use sp3d\models\Pasardesa;
use sp3d\models\transaksi\Sp3dPerangkatDesa;
use sp3d\models\transaksi\Sp3dPerangkatPelatihan;
use sp3d\models\DesaCount;
use sp3d\models\KecamatanCount;
use sp3d\models\KabupatenCount;
use sp3d\models\ProvinsiCount;
class DataCenterController extends Controller
{
    public function actionDataLaporanBaru()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new DataLaporan();
        $model->dataSet($data);
        $model->dataSelect();
        $model->dataRegion();

        return $this->renderAjax('laporan-baru/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionDataLaporan()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));

        $model = Sp3dPerangkatDesa::find();
        $subjekext = '';
        $objekparam = (isset($data['jabatan']) || $data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        switch($data['objek']){
            case 'kepala-desa':
                $regionWhere = 'sp3d_perangkat_desa';
                $model->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa', 'status_aktif' => 'aktif']);
                $objek = 'kades';
                $tipe = 'desa';
                if($data['subjek'] == 'pelatihan'){
                    $subjekext = '-'.$data['subjek'];
                    //reseting model
                    $model = null;
                    $model = Sp3dPerangkatPelatihan::find()->joinWith(['kategoripelatihan', 'perangkat']);//overide
                }
            break;
            case 'perangkat-desa':
                $regionWhere = 'sp3d_perangkat_desa';
                $model->where("tipe = 'desa' AND jabatan != :query2 AND status_aktif = 'aktif'")->addParams([':query2'=> 'kepala-desa']);
                if($data['jabatan'] != null){
                    $model->andWhere('jabatan = :query3')->addParams([':query3'=> $data['jabatan']]);
                }
                $objek = 'perangkat';
                $tipe = 'desa';
                if($data['subjek'] == 'pelatihan'){
                    $subjekext = '-'.$data['subjek'];
                    //reseting model
                    $model = null;
                    $model = Sp3dPerangkatPelatihan::find()->joinWith(['kategoripelatihan', 'perangkat']);//overide
                }
            break;
            case 'bpd':
                $regionWhere = 'sp3d_perangkat_desa';
                $model->where("tipe = :query1 AND status_aktif = 'aktif'")->addParams([':query1'=> 'bpd']);
                $objek = 'bpd';
                $tipe = 'bpd';
                if($data['jabatan'] != null){
                    $model->andWhere('jabatan = :query3')->addParams([':query3'=> $data['jabatan']]);
                }
                if($data['subjek'] == 'pelatihan'){
                    $subjekext = '-'.$data['subjek'];
                    //reseting model
                    $model = null;
                    $model = Sp3dPerangkatPelatihan::find()->joinWith(['kategoripelatihan', 'perangkat']);//overide
                }
            break;
            case 'bumdes':
                $regionWhere = 'bumdes';
                $model = Bumdes::find();//overide
                $objek = 'bumdes';
                if($data['subjek'] == 'unit-usaha'){
                    $model = BumdesUnitUsaha::find()->joinWith('bumdes');//overide
                    $subjekext = '-'.$data['subjek'];
                }

                if($data['subjek'] == 'keuntungan' || $data['subjek'] == 'omset' || $data['subjek'] == 'modal'){
                    $model = BumdesModal::find()->joinWith('bumdes');//overide
                    $subjekext = '-'.$data['subjek'];
                }
            break;
        }        

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->andWhere($regionWhere.'.kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->andWhere($regionWhere.'.kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }

        if($data['subjekval'] != '-' && $data['subjekval'] != null){
            switch($data['subjek']){
                case 'pelatihan':
                    $model->andWhere(['sp3d_perangkat_pelatihan.tipe' => $tipe, 'sp3d_perangkat_desa.jabatan' => $objekparam,'sp3d_master_jenis_pelatihan.id' => $data['subjekval']]);
                break;
                case 'lama-jabatan':
                    $model->select(['*', "(DATE_FORMAT(NOW(), '%Y') - tahun_awal) AS subjekval"]);
                    $model->andFilterHaving(['subjekval' => $data['subjekval']]);
                break;
                case 'jenis-kelamin':
                    $model->andWhere(['jenis_kelamin' => $data['subjekval']]);
                break;
                case 'pendidikan':
                    $model->andWhere(['pendidikan' => $data['subjekval']]);
                break;
                case 'umur':
                    $model->select(['*', "CASE WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 0 THEN '0'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 20 THEN '1-20'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 30 THEN '20-30'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 50 THEN '30-50'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 70 THEN '50-70'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) > 70 THEN '70-up' END AS subjekval"]);
                    $model->andFilterHaving(['subjekval' => $data['subjekval']]);
                break;
                //BUMDESA
                case 'tahun':
                    // $model::model()->findAllBySQL('SELECT bumdes.*,kabupaten.description, kecamatan.description, desa.description FROM bumdes LEFT JOIN user kabupaten ON kabupaten.id = bumdes.kd_kabupaten COLLATE utf8_unicode_ci LEFT JOIN user kecamatan ON kecamatan.id = bumdes.kd_kecamatan COLLATE utf8_unicode_ci LEFT JOIN user desa ON desa.id = bumdes.kd_desa COLLATE utf8_unicode_ci WHERE tahun_berdiri=:tahun',array(':tahun'=> $data['subjekval']));
                    $model->andWhere(['tahun_berdiri' => $data['subjekval']]);
                break;
                case 'keuntungan':
                case 'modal':
                case 'omset':
                    $model->select(['*', "bumdes.nama_bumdes,CASE WHEN (bumdes_modal.".$data['subjek'].") <= 1000000 THEN '0-1jt'
                     WHEN (bumdes_modal.".$data['subjek'].") <= 5000000 THEN '1-5jt'
                     WHEN (bumdes_modal.".$data['subjek'].") <= 10000000 THEN '5-10jt'
                     WHEN (bumdes_modal.".$data['subjek'].") <= 50000000 THEN '10-50jt'
                     WHEN (bumdes_modal.".$data['subjek'].") > 50000000 THEN '50jt-keatas' END AS subjekval"]);
                    $model->andFilterHaving(['subjekval' => $data['subjekval']]);
                break;
                case 'region':
                break;
                case 'unit-usaha':
                    // SELECT count(*) AS hit, unit_usaha AS subjekval FROM bumdes_unit_usaha GROUP BY subjekval ORDER BY hit DESC LIMIT 20
                    $model->andWhere(['unit_usaha' => $data['subjekval']]);
                break;
            }
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }

        return $this->renderAjax('data-'.$objek.$subjekext, [
            'model' => $model,
            'kode' => $kode,
            'data' => $data
        ]);
    }

    public function actionDataFormCompleteBumdes()
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        // $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        switch ($data['subjek']) {
            case 'status':
                $sql ="SELECT status AS subjekval FROM bumdes GROUP BY subjekval";
            break;    
            case 'keuntungan':
                $sql ="SELECT 
                CASE WHEN (keuntungan) <= 1000000 THEN '0-1jt'
                     WHEN (keuntungan) <= 5000000 THEN '1-5jt'
                     WHEN (keuntungan) <= 10000000 THEN '5-10jt'
                     WHEN (keuntungan) <= 50000000 THEN '10-50jt'
                     WHEN (keuntungan) > 50000000 THEN '50jt-keatas' END AS subjekval
                FROM bumdes_modal GROUP BY subjekval";
            break;
            case 'modal':
                $sql ="SELECT 
                CASE WHEN (modal) <= 1000000 THEN '0-1jt'
                     WHEN (modal) <= 5000000 THEN '1-5jt'
                     WHEN (modal) <= 10000000 THEN '5-10jt'
                     WHEN (modal) <= 50000000 THEN '10-50jt'
                     WHEN (modal) > 50000000 THEN '50jt-keatas' END AS subjekval
                FROM bumdes_modal GROUP BY subjekval";
            break;
            case 'omset':
                $sql ="SELECT 
                CASE WHEN (omset) <= 1000000 THEN '0-1jt'
                     WHEN (omset) <= 5000000 THEN '1-5jt'
                     WHEN (omset) <= 10000000 THEN '5-10jt'
                     WHEN (omset) <= 50000000 THEN '10-50jt'
                     WHEN (omset) > 50000000 THEN '50jt-keatas' END AS subjekval
                FROM bumdes_modal GROUP BY subjekval";
            break;
            case 'tahun':
                $sql ="SELECT tahun_berdiri AS subjekval FROM `bumdes` GROUP BY subjekval;";
            break;
            case 'region':
                $sql ="SELECT description AS subjekval FROM bumdes LEFT JOIN user ON user.id = bumdes.kd_kabupaten COLLATE utf8_unicode_ci GROUP BY kd_kabupaten";
            break;
            case 'unit-usaha':
                $sql ="SELECT count(*) AS hit, unit_usaha AS subjekval FROM bumdes_unit_usaha GROUP BY subjekval ORDER BY hit DESC LIMIT 20";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        if($data['subjek'] != 'region'){
            foreach($resultan as $v){
                $key = $val = $v['subjekval'];
                $responseval .= '<option value="'.$key.'">'.$val.'</td></tr>';
            }    
        }
        
        return $responseval;
    }

    public function actionDataFormComplete()//khusus desa (kepala-desa, perangkat-desa, bpd)
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        //mendapatkan jenis
        if($data['jabatan'] != '-'){
            $m = Sp3dPerangkatDesa::findOne(['jabatan' => $data['jabatan']]);
            $tipe = $m->tipe;
        } else {
            $m = Sp3dPerangkatDesa::findOne(['jabatan' => $data['objek']]);
            $tipe = $m->tipe;
        }

        switch ($data['subjek']) {
            case 'pelatihan':
                $sql = "SELECT id AS subjekval FROM sp3d_master_jenis_pelatihan WHERE tipe = '$tipe' GROUP BY subjekval";
            break;
            case 'akhir-jabatan':
                $sql = "SELECT (tahun_akhir - DATE_FORMAT(NOW(), '%Y')) AS subjekval FROM sp3d_perangkat_desa WHERE jabatan = '$objek' GROUP BY subjekval";
            break;
            case 'lama-jabatan':
                $sql = "SELECT (DATE_FORMAT(NOW(), '%Y') - tahun_awal) AS subjekval FROM sp3d_perangkat_desa WHERE jabatan = '$objek' GROUP BY subjekval";
            break;
            case 'pendidikan':
                $sql = "SELECT pendidikan AS subjekval FROM sp3d_perangkat_desa WHERE jabatan = '$objek' GROUP BY subjekval";
            break;
            case 'umur':
                $sql = "SELECT 
                CASE WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 20 THEN '0-20'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 30 THEN '21-30'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 50 THEN '31-50'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 70 THEN '51-70'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) > 70 THEN '70-100' END AS subjekval
                FROM sp3d_perangkat_desa WHERE status_aktif='aktif' AND jabatan = '$objek' GROUP BY subjekval";
            break;
            case 'jenis-kelamin':
            default:

                $sql = "SELECT jenis_kelamin AS subjekval FROM sp3d_perangkat_desa WHERE jabatan = '$objek' GROUP BY subjekval";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        foreach($resultan as $v){
            if($data['subjek'] == 'pendidikan'){
                $val = substr($v['subjekval'], 2);
                $key = $v['subjekval'];
            } else {
                $key = $val = $v['subjekval'];
            }
            $responseval .= '<option value="'.$key.'">'.$val.'</td></tr>';
        }
        return $responseval;
    }
    /*public function actionDataKades()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        
        $model = Sp3dPerangkatDesa::find();
        $model->orderBy('user'); 

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'tipe' => 'desa']);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-kades-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataPerangkat()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Sp3dPerangkatDesa::find();
        $model->joinWith('jabatanproperti');        
        $model->orderBy('sp3d_perangkat_desa.user, sp3d_master_jabatan.urut');        

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'desa']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'desa']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            
            $model->where('jabatan != :query2')->addParams([':query2'=> 'kepala-desa']);
            $model->andWhere('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'desa']);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-perangkat-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataBpd()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Sp3dPerangkatDesa::find();
        $model->joinWith('jabatanproperti');        
        $model->orderBy('sp3d_perangkat_desa.user, sp3d_master_jabatan.urut');

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->where('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'bpd']);
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->where('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'bpd']);
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model->where('sp3d_perangkat_desa.tipe = :query1')->addParams([':query1'=> 'bpd']);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-bpd-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }*/

    public function actionDataBumdes()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Bumdes::find();

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-bumdes-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataPasardesa()
    {
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        $model = Pasardesa::find();

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
            $model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
            $model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-pasardesa-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataTableProvinsi()
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_desa' => $data['desa-input']]);
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kecamatan' => $data['kecamatan-input']]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $model = KecamatanCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kabupaten' => $data['kabupaten-input']]); 
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = KabupatenCount::find()->where(['tahun' => $data['tahun-anggaran']]);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-table-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataTableProvinsiOutDesa()
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kecamatan' => $data['kecamatan-input']]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kabupaten' => $data['kabupaten-input']]); 
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran']]);
            $model->limit(600);
        }

        $model->orderBy('dana_pendapatan DESC');

        $data['type'] = $type;
        
        return $this->renderAjax('data-table-out-desa', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataTableApbdesaSorting()
    {
        $data = Yii::$app->request->get();
        $model = DesaCount::find()->where(['tahun' => $data['tahun']]);
        
        if($data['order'] == 'avg'){

        }

        if($data['order'] == 'maxmin'){
            $model->orderBy($data['apbdes'].' DESC');
        } else {
            $model->orderBy($data['apbdes'].' ASC');
        }

        return $this->renderAjax('data-apbdesa-sorting', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataOption($type='kabupaten', $kode=null)
    {
        switch ($type) {
            // case 'kabupaten':
            //     $model = User::find()->where(['type' => $tahun]);
            // break;
            case 'kecamatan':
                $model = User::find()->where(['type' => 'kecamatan']);
            break;
            case 'desa':
                $model = User::find()->where(['type' => 'desa']);
            break;
        }

        $model->andWhere('id LIKE :query')->addParams([':query'=> $kode.'%']);

        return $this->renderPartial('option', [
            'model' => $model,
            'type' => $type,
            'kode' => $kode,
        ]);
    }
}
