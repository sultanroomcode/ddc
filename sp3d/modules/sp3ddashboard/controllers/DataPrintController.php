<?php
namespace sp3d\modules\sp3ddashboard\controllers;

use Yii;
use common\models\LoginForm;
use sp3d\models\PasswordResetRequestForm;
use sp3d\models\ResetPasswordForm;
use sp3d\models\AESServer;
use sp3d\models\transaksi;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

/**
 * Site controller
 */
class DataPrintController extends Controller
{
    public function actionPrepare()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('prepare', [
            'data' => $data
        ]);
    }

    public function actionTest($kd_desa=null)
    {
        $data = Yii::$app->request->get();
        $data['kd_desa'] = ($kd_desa == null)?Yii::$app->user->identity->id:$kd_desa;
        return $this->renderAjax('pdf-page-'.$data['chapter'], [
            'data' => $data
        ]);
    }

    public function actionPdf()
    {
        $data = Yii::$app->request->get();
        $data['kd_desa'] = Yii::$app->user->identity->id;
        try {
            ob_start();
            echo $this->renderPartial('pdf-page-'.$data['chapter'], [
                'data' => $data
            ]);
            
            $content = ob_get_clean();
            $html2pdf = new Html2Pdf('P', 'A4', 'fr');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content);
            $html2pdf->output(time().'.pdf');
        } catch (Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }
}
