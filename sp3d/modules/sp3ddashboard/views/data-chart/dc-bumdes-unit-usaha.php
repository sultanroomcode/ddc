<?php 
use sp3d\modules\bumdes\models\BumdesUnitUsaha;
?>
<h1>Bumdes Unit Usaha</h1>
Unit Usaha <?= $data['needle'] ?><br>
<a href="javascript:void(0)" onclick="goExport()" class="btn btn-danger">Export</a><br>
<?php 
$data['needle'] = trim(urldecode($data['needle']));
$model = BumdesUnitUsaha::find();
$model->where(['unit_usaha' => $data['needle']]);

$model->orderBy('kd_kabupaten');

echo '<table class="table table-striped"><tr><th>No.</th><th>Kabupaten</th><th>Kecamatan</th><th>Desa</th><th>Nama Bumdes</th><th>Modal</th></tr>';
$i =1;
foreach($model->all() as $v){
	echo '<tr><td>'.$i.'</td><td>'.$v->kabupaten->description.'</td><td>'.$v->kecamatan->description.'</td><td>'.$v->desa->description.'</td><td>'.$v->bumdes->nama_bumdes.'</td><td align="right">'.$v->unit_usaha.'</td></tr>';
	$i++;
}
echo '</table>';
?>
<script type="text/javascript">
needle = encodeURI('<?=$data['needle']?>');
function goExport(){
    window.open(base_url+'/sp3ddashboard/data-export/data-bumdes-chart?tipedata=unit-usaha&needle='+needle, '_blank');
}
</script>