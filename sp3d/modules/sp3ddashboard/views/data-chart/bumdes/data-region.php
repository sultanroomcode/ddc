<h2 align="center">Daftar Bumdes di Provinsi Jawa Timur</h2>
<a href="javascript:void(0)" onclick="goExport()" class="btn btn-danger">Export</a><br>
<table class="table compact" id="data-bumdes">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>            
            <th>Nama</th>
            <th>Tahun</th>
            <th>Alamat</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>         
            <th>Nama</th>
            <th>Tahun</th>
            <th>Alamat</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <th><?= $v->kabupaten->description ?></th>
            <th><?= $v->kecamatan->description ?></th>
            <th><?= $v->desa->description ?></th>         
            <td><?= $v->nama_bumdes ?></td>
            <td><?= $v->tahun_berdiri ?></td>
            <td><?= $v->alamat ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-bumdes').DataTable();
function goExport(){
    window.open(base_url+'/sp3ddashboard/data-export/data-bumdes-chart?tipedata=region&needle={$data['needle']}', '_blank');
}
JS;

$this->registerJs($script);