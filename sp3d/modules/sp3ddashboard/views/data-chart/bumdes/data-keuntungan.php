<h2 align="center">Daftar Bumdes di Provinsi Jawa Timur</h2>
<a href="javascript:void(0)" onclick="goExport()" class="btn btn-danger">Export</a><br>
<table class="table compact" id="data-bumdes">
    <thead>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Keuntungan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Keuntungan</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr> 
            <th><?= $v->bumdes->kabupaten->description ?></th>
            <th><?= $v->bumdes->kecamatan->description ?></th>
            <th><?= $v->bumdes->desa->description ?></th>
            <td><?= $v->bumdes->nama_bumdes ?></td>
            <td><?= $v->nf($v->keuntungan) ?></td>
            <th>Aksi</th>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-bumdes').DataTable();
function goExport(){
    window.open(base_url+'/sp3ddashboard/data-export/data-bumdes-chart?tipedata=keuntungan&needle={$data['needle']}', '_blank');
}
JS;

$this->registerJs($script);