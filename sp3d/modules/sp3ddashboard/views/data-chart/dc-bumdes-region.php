<?php 
use sp3d\modules\bumdes\models\Bumdes;
$data['needle'] = trim(urldecode($data['needle']));
?>
<h1>Bumdes</h1>
Bumdes <?= $data['needle'] ?><br>
<a href="javascript:void(0)" onclick="goExport()" class="btn btn-danger">Export</a><br>
<?php 
$model = Bumdes::find()->join('LEFT JOIN','user','bumdes.kd_kabupaten = user.id COLLATE utf8_general_ci');
$model->where(['user.description' => $data['needle']]);
$model->orderBy('kd_desa');

echo '<table class="table table-striped"><tr><th>No.</th><th>Kabupaten</th><th>Kecamatan</th><th>Desa</th><th>Nama Bumdes</th><th>Tahun Berdiri</th></tr>';
$i =1;
foreach($model->all() as $v){
	echo '<tr><td>'.$i.'</td><td>'.$v->kabupaten->description.'</td><td>'.$v->kecamatan->description.'</td><td>'.$v->desa->description.'</td><td>'.$v->nama_bumdes.'</td><td align="right">'.$v->tahun_berdiri.'</td></tr>';
	$i++;
}
echo '</table>';
?>
<script type="text/javascript">
needle = encodeURI('<?=$data['needle']?>');
function goExport(){
    window.open(base_url+'/sp3ddashboard/data-export/data-bumdes-chart?tipedata=region&needle='+needle, '_blank');
}
</script>