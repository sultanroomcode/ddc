<?php 
use sp3d\modules\bumdes\models\BumdesModal;
?>
<h1>Bumdes Keuntungan</h1>
<?php
$pool = [
	'0-1jt' => ['min' => 0, 'max' => 1000000],
	'1-5jt' => ['min' => 1000001, 'max' => 5000000],
	'5-10jt' => ['min' => 5000001, 'max' => 10000000],
	'10-50jt' => ['min' => 10000001, 'max' => 50000000],
	'50jt-keatas' => ['min' => 5000001, 'max' => null]
];
?>
Bumdes Keuntungan <?= $data['needle'] ?><br>
<a href="javascript:void(0)" onclick="goExport()" class="btn btn-danger">Export</a><br>
<?php 
$model = BumdesModal::find();
if($data['needle'] == '50jt-keatas'){
	$model->where(['>', 'keuntungan', 50000000]);
} else {
	$model->where(['>=', 'keuntungan', $pool[$data['needle']]['min']]);
	$model->andWhere(['<=', 'keuntungan', $pool[$data['needle']]['max']]);
}

$model->orderBy('kd_kabupaten');

echo '<table class="table table-striped"><tr><th>Kabupaten</th><th>Kecamatan</th><th>Desa</th><th>Nama Bumdes</th><th>Keuntungan</th></tr>';
foreach($model->all() as $v){
	echo '<tr><td>'.$v->kabupaten->description.'</td><td>'.$v->kecamatan->description.'</td><td>'.$v->desa->description.'</td><td>'.$v->bumdes->nama_bumdes.'</td><td>'.$v->nf($v->keuntungan).'</td></tr>';
}
echo '</table>';
?>

<script type="text/javascript">
function goExport(){
    window.open(base_url+'/sp3ddashboard/data-export/data-bumdes-chart?tipedata=keuntungan&needle=<?=$data['needle']?>', '_blank');
}
</script>