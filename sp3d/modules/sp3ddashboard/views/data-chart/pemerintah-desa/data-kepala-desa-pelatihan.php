<h2 align="center">Daftar Kades di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kades">
    <thead>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Pelatihan</th>
            <th>Kategori</th>
            <th>Tahun</th>
            <th>Penyelenggara</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Pelatihan</th>
            <th>Kategori</th>
            <th>Tahun</th>
            <th>Penyelenggara</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>                
            <td><?= $v->kd_desa ?></td>
            <td><?= $v->nik ?></td>
            <td><?= $v->perangkat->nama ?></td>
            <td><?= $v->perangkat->jabatan ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->kategoripelatihan->nama_pelatihan ?></td>
            <td><?= $v->tahun ?></td>
            <td><?= $v->penyelenggara ?></td>
            <td>-</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kades').DataTable();
JS;

$this->registerJs($script);