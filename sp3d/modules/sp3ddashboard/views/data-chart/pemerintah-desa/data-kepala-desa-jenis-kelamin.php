<h2 align="center">Daftar Kades di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kades">
    <thead>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>TTL</th>
            <th>Pendidikan</th>
            <th>Jenis Kelamin</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>TTL</th>
            <th>Pendidikan</th>
            <th>Jenis Kelamin</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>                
            <td><?= $v->user ?></td>
            <td><?= $v->nik ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->jabatan ?></td>
            <td><?= $v->tempat_lahir.', '.date('d-F-Y', strtotime($v->tanggal_lahir)) ?></td>
            <td><?= substr($v->pendidikan, 2) ?></td>
            <td><?= $v->gender() ?></td>
            <td>-</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kades').DataTable();
JS;

$this->registerJs($script);