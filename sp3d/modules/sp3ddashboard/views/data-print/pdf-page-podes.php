<?php
use sp3d\modules\ddc\models\DdcPodes;
$model = DdcPodes::find()->where(['kd_desa' => $data['kd_desa'], 'jenis' => $data['modul']]);
$moduser = Yii::$app->user->identity->profile;
?>
<p style="text-align: center;">
<h2>PEMERINTAH KABUPATEN <?= $moduser->kabupaten->description ?> <br>
KECAMATAN <?=$moduser->kecamatan->description ?> <br>
DESA <?=$moduser->desa->description ?><br></h2>
</p>
<hr>
<p style="text-align: center;">
LAPORAN <?= strtoupper(str_replace('-', ' ', $data['modul']))?>
</p>
<style>
	th, td {
		padding: 5px;
	}
</style>
<p>
<?php
echo '<table style="border-collapse:collapse;" border="1"><tr><th>Komoditas</th><th>Luas Lahan</th><th>Hasil Produksi</th></tr>';
foreach($model->all() as $v){
	echo '<tr><td style="width:40%">'.$v->komoditasp->nama_komoditas.'</td><td style="width:20%">'.$v->luas_lahan_produksi.'</td><td style="width:20%">'.$v->hasil_produksi.'</td></tr>';
}
echo '</table>';
?>
</p>