<div class="prepare-view">
    <div class="tile">
        <h2 class="tile-title">Download PDF</h2>
        <a href="javascript:void()" class="btn btn-danger" onclick="getPDF()">Download <?= strtoupper($data['chapter']) ?></a>
        <div style="clear: both;"></div>
    </div>
</div>

<?php
$chapter = $data['chapter'];
$scripts =<<<JS
function getPDF(){
    window.open(base_url+'/sp3ddashboard/data-print/pdf?chapter={$chapter}', '_blank');
}
JS;

$this->registerJs($scripts);