<?php
use sp3d\modules\lpmd\models\Lpmd;
$model = Lpmd::findOne($data['kd_desa']);
// var_dump($data); var_dump($model);
?>
<p style="text-align: center;">
<h2>PEMERINTAH KABUPATEN <?=$model->kabupaten->description ?> <br>
KECAMATAN <?=$model->kecamatan->description ?> <br>
DESA <?=$model->desa->description ?><br></h2>
</p>
<hr>
<p>
Daftar Pengurus Lembaga Pemberdayaan Masyarakat Desa/Kelurahan <b><?=$model->desa->description ?></b> yang ditetapkan oleh <b><?=$model->ditetapkan_oleh ?></b> Nomer SK <b><?=$model->sk_lembaga ?></b> <br><br>
<?php 
echo '<table>';
foreach($model->pengurus as $v){
	echo '<tr><td style="width:40%">'.$v->jabatan.'</td><td style="width:60%">'.$v->nama.'</td></tr>';
}
echo '</table>';
?>

<br>
<?php if($model->pembina_status_ada == 'ada'){ ?>
Daftar Pembina Lembaga Pemberdayaan Masyarakat Desa/Kelurahan <b><?=$model->desa->description ?></b> yang ditetapkan oleh <b><?=$model->pembina_ditetapkan_oleh ?></b>  Nomer Sk <b><?=$model->pembina_sk_penetapan ?></b> <br><br>
<br>
<?php
echo '<table>'; 
foreach($model->pembina as $v){
	echo '<tr><td style="width:40%">'.$v->jabatan.'</td><td style="width:60%">'.$v->nama.'</td></tr>';
}
echo '</table>';
?>
<?php } ?>
</p>
<hr>
<h3>Daftar Kegiatan</h3>
<?php
echo '<table>'; 
foreach($model->kegiatan as $v){
	echo '<tr><td style="width:40%">'.$v->nama.'</td><td style="width:60%">'.$v->tanggal_mulai.'</td></tr>';
}
echo '</table>';
?>