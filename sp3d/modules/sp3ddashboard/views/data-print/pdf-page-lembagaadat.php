<?php
use sp3d\modules\lembagaadat\models\LembagaAdat;
$model = LembagaAdat::findOne($data['kd_desa']);
// var_dump($data); var_dump($model);
?>
<p style="text-align: center;">
<h2>PEMERINTAH KABUPATEN <?=$model->kabupaten->description ?> <br>
KECAMATAN <?=$model->kecamatan->description ?> <br>
DESA <?=$model->desa->description ?><br></h2>
</p>
<hr>
<p>
Daftar Pengurus Lembaga Adat <b><?=$model->desa->description ?></b> yang ditetapkan oleh <b><?=$model->ditetapkan_oleh ?></b> melalui <b><?=$model->sk_pendirian ?></b> dengan No. <?= $model->sk_no ?> <br><br>
<?php
if($model->kesekertariatan_status_ada == 'ada'){
?>
Status Kesekertariatan : <?= $model->status_kesekertariatan ?><br><br>
<?php 
}
echo '<table>';
foreach($model->pengurus as $v){
	echo '<tr><td style="width:40%">'.$v->jabatan.'</td><td style="width:60%">'.$v->nama.'</td></tr>';
}
echo '</table>';
?>
</p>
<hr>
<h3>Daftar Kegiatan</h3>
<?php
echo '<table>'; 
foreach($model->kegiatan as $v){
	echo '<tr><td style="width:40%">'.$v->nama.'</td><td style="width:60%">'.$v->tanggal_mulai.'/'.$v->tanggal_selesai.'</td></tr>';
}
echo '</table>';
?>