<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dMessageThread */

$this->title = 'Update Sp3d Message Thread: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Message Threads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'from' => $model->from, 'to' => $model->to]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sp3d-message-thread-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
