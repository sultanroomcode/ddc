<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$arr = ['-' => ''];
$kabArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');

$kabArr = $arr + $kabArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Pesan Pengguna</h2>
            <div class="p-10">
                <div class="row">
                    <div class="col-md-5">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($kabArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-md-5">
                        <select id="kecamatan" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button class="btn btn-block btn-danger" onclick="filteringAdd()">Filter</button>
                    </div>
                </div>
                <br>

                <table id="management-message" class="tile table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Dari</th>
                            <th>Kepada</th>
                            <th>Type</th>
                            <th>Pesan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div id="modal-custom" style="display: none;">
    <div style="width: 900px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
    </div>
</div>
<?php
$scripts =<<<JS
$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/pendamping/data/data-option?type=kecamatan&kode='+kode});
  }
});

$('#management-message').DataTable({
    "columns": [
        { "data": "from"},
        { "data": "to"},
        { "data": "type", 'searchable':false },
        { "data": "body_content", 'searchable':true },
        { "data": "action", 'searchable' : false, 'orderable': false }
    ],
    searchDelay: 1500,
    ajax: {
        url: base_url+'/sp3ddashboard/message-center/message-data',
        data : function(d){
            //https://medium.com/code-kings/datatables-js-how-to-update-your-data-object-for-ajax-json-data-retrieval-c1ac832d7aa5
            d.kabupaten = $('#kabupaten').val();
            d.kecamatan = $('#kecamatan').val();
        }
    },
    processing:true,
    serverSide:true
});

function filteringAdd(){
    $('#management-message').DataTable().ajax.reload();
    return false;
}
JS;

$this->registerJs($scripts);