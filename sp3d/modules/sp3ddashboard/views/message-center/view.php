<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dMessageThread */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Message Threads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp3d-message-thread-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'from' => $model->from, 'to' => $model->to], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'from' => $model->from, 'to' => $model->to], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_header',
            'body_content:ntext',
            'type',
            'from',
            'to',
            'status',
            'created_at',
            'last_inserted_at',
        ],
    ]) ?>

</div>
