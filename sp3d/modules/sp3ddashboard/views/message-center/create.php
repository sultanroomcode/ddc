<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dMessageThread */

$this->title = 'Tulis Saran/Kritik Untuk Pengembang PusDaDesa dan Sp3d';
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Message Threads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row animated slideInRight sp3d-message-thread-create">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title"><?= Html::encode($this->title) ?></h2>
            <div class="p-10">
			    <?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>
			</div>
		</div>
	</div>
</div>
