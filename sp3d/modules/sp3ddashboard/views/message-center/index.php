<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sp3d Message Threads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp3d-message-thread-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sp3d Message Thread', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title_header',
            'body_content:ntext',
            'type',
            'from',
            // 'to',
            // 'status',
            // 'created_at',
            // 'last_inserted_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
