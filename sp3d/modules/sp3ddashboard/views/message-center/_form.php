<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dMessageThread */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->id = time();
    $model->type = 'kritik';
    $model->from = Yii::$app->user->identity->id;
    $model->to = '35';
    $model->title_header = 'kritik '.$model->id. ' - '.$model->from;
    $model->status = '0';
}
?>

<div class="sp3d-message-thread-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'title_header')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'body_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'type')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'from')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'to')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Kirim' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$user = Yii::$app->user->identity->id;
$script = <<<JS
//regularly-ajax
$('#sp3dmessagethread-body_content').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    // ['fontsize', ['fontsize']],
    // ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    //['height', ['height']]
  ],
  height: 300,
});

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil mengirimkan saran/kritik');
            goLoad({url : '/umum/default/dashboard'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);