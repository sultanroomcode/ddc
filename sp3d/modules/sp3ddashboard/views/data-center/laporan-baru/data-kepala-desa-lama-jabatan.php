<h2 align="center">Daftar Kepala Desa di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kades">
    <thead>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>TTL</th>
            <th>Pendidikan</th>
            <th>TMJ</th>
            <th>Lama Jabatan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>TTL</th>
            <th>Pendidikan</th>
            <th>TMJ</th>
            <th>Lama Jabatan</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>                
            <td><?= $v->user ?></td>
            <td><?= $v->nik ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->jabatan ?></td>
            <td><?= $v->tempat_lahir.', '.date('d-F-Y', strtotime($v->tanggal_lahir)) ?></td>
            <td><?= substr($v->pendidikan, 2) ?></td>
            <td><?= $v->tahun_akhir ?></td>
            <td><?= $v->dummy_var ?> Tahun</td>
            <td><a href="javascript:void(0)" onclick="openModalXyf({url:'/transaksi/ta-perangkat-desa/view-perangkat-desa?nik=<?=$v->nik?>&user=<?=$v->user?>'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kades').DataTable();
JS;

$this->registerJs($script);