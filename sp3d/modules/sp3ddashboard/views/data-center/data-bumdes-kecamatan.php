<h2 align="center">Daftar Bumdes di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-bumdes">
    <thead>
        <tr>            
            <th>Nama</th>
            <th>Tahun</th>
            <th>Alamat</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Nama</th>
            <th>Tahun</th>
            <th>Alamat</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>                
            <td><?= $v->nama_bumdes ?></td>
            <td><?= $v->tahun_berdiri ?></td>
            <td><?= $v->alamat ?></td>
            <td><a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/view-perangkat-desa?id=<?=$v->id_bumdes?>&kd_desa=<?=$v->kd_desa?>'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-bumdes').DataTable();
JS;

$this->registerJs($script);