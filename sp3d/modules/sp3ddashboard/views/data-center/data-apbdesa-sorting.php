<h2 align="center">Daftar APBDESA di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-apbdesa-sorting">
    <thead>
        <tr>            
            <th>Kd Kabupaten</th>
            <th>Kd Kecamatan</th>
            <th>Kd Desa</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Kd Kabupaten</th>
            <th>Kd Kecamatan</th>
            <th>Kd Desa</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>                
            <td><?= $v->kabupatenLabel->description ?></td>
            <td><?= $v->kecamatanLabel->description ?></td>
            <td><?= $v->desaLabel->description ?></td>
            <td><?= $v->nf($v->dana_pendapatan) ?></td>
            <td><?= $v->nf($v->dana_rab) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-apbdesa-sorting').DataTable({
    "order": [[ 3, "desc" ]]
});
JS;

$this->registerJs($script);