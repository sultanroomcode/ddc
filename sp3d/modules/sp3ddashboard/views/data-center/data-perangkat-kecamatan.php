<h2 align="center">Daftar Perangkat di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kades">
    <thead>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Jabatan</th>
            <th>Nama</th>
            <th>TTL</th>
            <th>Pendidikan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kode</th>
            <th>NIK</th>
            <th>Jabatan</th>
            <th>Nama</th>
            <th>TTL</th>
            <th>Pendidikan</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>                
            <td><?= $v->user ?></td>
            <td><?= $v->nik ?></td>
            <td><?= $v->jabatan ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->tempat_lahir.', '.date('d-F-Y', strtotime($v->tanggal_lahir)) ?></td>
            <td><?= substr($v->pendidikan, 2) ?></td>
            <td><a href="javascript:void(0)" onclick="openModalXyf({url:'/transaksi/ta-perangkat-desa/view-perangkat-desa?nik=<?=$v->nik?>&user=<?=$v->user?>'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kades').DataTable();
JS;

$this->registerJs($script);