<h2 align="center">Daftar Pasar Desa di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-pasardesa">
    <thead>
        <tr>            
            <th>Nama</th>
            <th>Tahun</th>
            <th>Kepemilikan Tanah</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Nama</th>
            <th>Tahun</th>
            <th>Kepemilikan Tanah</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>                
            <td><?= $v->nama ?></td>
            <td><?= $v->tahun ?></td>
            <td><?= $v->kepemilikan_tanah ?></td>
            <td><a href="javascript:void(0)" onclick="openModalXyf({url:'/pasar/view-perangkat-desa?id=<?=$v->id_pasar?>&kd_desa=<?=$v->kd_desa?>'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-pasardesa').DataTable();
JS;

$this->registerJs($script);