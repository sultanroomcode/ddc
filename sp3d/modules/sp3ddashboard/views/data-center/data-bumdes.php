<h2 align="center">Daftar Bumdes di Provinsi Jawa Timur</h2>
<table class="table table-bordered compact" id="data-bumdes">
    <thead>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Tahun Berdiri</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Tahun Berdiri</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr> 
            <th><?= $v->kabupaten->description ?></th>
            <th><?= $v->kecamatan->description ?></th>
            <th><?= $v->desa->description ?></th>
            <td><?= $v->nama_bumdes ?></td>
            <td><?= $v->tahun_berdiri ?></td>
            <td><a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes/view-detail-only-provinsi?id_bumdes=<?=$v->id_bumdes?>&kd_desa=<?=$v->kd_desa?>',welm:'800px'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-bumdes').DataTable();
JS;

$this->registerJs($script);