<h2 align="center">Daftar Kades di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kades">
    <thead>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Lembaga</th>
            <th>Nama Pelatihan</th>
            <th>Tahun</th>
            <th>Lembaga <br>Penyelenggara</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Lembaga</th>
            <th>Nama Pelatihan</th>
            <th>Tahun</th>
            <th>Lembaga <br>Penyelenggara</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>                
            <td><?= $v->perangkat->kabupaten->description ?></td>
            <td><?= $v->perangkat->kecamatan->description ?></td>
            <td><?= $v->perangkat->desa->description ?></td>
            <td><?= $v->nik ?></td>
            <td><?= $v->perangkat->nama ?></td>
            <td><?= $v->tipe ?></td>
            <td><?= $v->kategoripelatihan->nama_pelatihan ?></td>
            <td><?= $v->tahun ?></td>
            <td><?= $v->penyelenggara ?></td>
            <td><a href="javascript:void(0)" onclick="openModalXyf({url:'/transaksi/ta-perangkat-desa/view-perangkat-desa?nik=<?=$v->nik?>&user=<?=$v->kd_desa?>'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kades').DataTable();
JS;

$this->registerJs($script);