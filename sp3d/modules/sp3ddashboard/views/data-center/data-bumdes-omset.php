<h2 align="center">Daftar Bumdes di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-bumdes">
    <thead>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Omset</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Omset</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr> 
            <th><?= $v->kabupaten->description ?></th>
            <th><?= $v->kecamatan->description ?></th>
            <th><?= $v->desa->description ?></th>
            <td><?= ($v->bumdes == null)?'belum-isi':$v->bumdes->nama_bumdes ?></td>
            <td><?= $v->nf($v->omset) ?></td>
            <th>Aksi</th>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-bumdes').DataTable();
JS;

$this->registerJs($script);