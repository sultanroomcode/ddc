<h2 align="center">APBDesa Provinsi Jawa Timur Tahun <?= $data['tahun-anggaran'] ?></h2>
<table class="table compact" id="data-table-provinsi">
    <thead>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>                
            <td><?= $v->kabupatenLabel->description ?></td>
            <td><?= $v->kecamatanLabel->description ?></td>
            <td><?= $v->desaLabel->description ?></td>
            <td align="right"><?= $v->nf($v->dana_pendapatan) ?></td>
            <td align="right"><?= $v->nf($v->dana_rab) ?></td>
            <td align="right"><a href="javascript:void(0)" onclick="goLoad({elm:'#apbdes-zona-penganggaran', url:'/transaksi/default/penganggaran-desa?id=<?=$v->kd_desa?>&tahun=<?=$v->tahun?>'})" class="btn btn-xs btn-danger">Lihat RAB</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div id="apbdes-zona-penganggaran"></div>
<?php
$script =<<<JS
$('#data-table-provinsi').DataTable({
  "language": {
      "decimal": ",",
      "thousands": "."
  }
});
JS;

$this->registerJs($script);