<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if($model->isNewRecord){
    $model->id = Yii::$app->user->identity->id.'-'.time();
    $model->kd_desa = Yii::$app->user->identity->id;
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
    $model->tipe_indikator = $tipe_indikator;
    $model->verifikasi_kab = 0;
    $model->komen_desa = $model->komen_kab = '-';
}
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Form Data</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'tipe_indikator')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'verifikasi_kab')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'komen_kab')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'komen_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'file_indikator_box')->fileInput(['maxlength' => true]) ?>
                    </div>
                </div>          

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    if($("#sp3dfileindikator-file_indikator_box").val() == ''){
        $.alert('Mohon Pilih File PDF!');
        return false;
    }
    var \$form = $(this);
    var \$data = new FormData($('form#{$model->formName()}')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                goLoad({elm:'#main-page-indikator', url:'/sp3ddashboard/indikator-dana-desa/list-data?tipe={$tipe_indikator}'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($scripts);