<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Verifikasi Indikator Dana Desa</h2>
            <div class="p-10">
            	<div class="block-area" id="tableStriped">
				    <h3 class="block-title">Aktifitas Upload</h3>
				    <div class="table-responsive overflow">
				    	<table id="ta-mdb-list" class="tile table table-bordered table-striped" cellspacing="0">
						    <thead>
						        <tr>
						            <th>ID</th>
						            <th>File</th>
						            <th>Tanggal Upload</th>
						            <th>Status Verifikasi</th>
						            <th>Action</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($model->all() as $v): ?>
						        <tr>
						            <td><?= $v->id ?></td>
						            <td><?= $v->file_indikator ?></td>
						            <td><?= $v->tanggal_upload ?></td>
						            <td><?= $v->verifikasi_kab ?></td>
						            <td>
					            		<a href="javascript:void(0)" onclick="openModalXyf({url:'/sp3ddashboard/indikator-dana-desa/embed-and-verification-form?id=<?=$v->id?>'})" title="Lihat PDF" class="show-pop label label-danger" data-animation="pop" data-content="<p>Klik untuk melihat PDF</p>"><i class="fa fa-eye"></i></a>
						            </td>
						        </tr>
						        <?php endforeach; ?>
						    </tbody>
						</table>
				    </div>
				</div><br>.
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
	$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
	$('#ta-mdb-list').DataTable();
JS;

$this->registerJs($scripts);