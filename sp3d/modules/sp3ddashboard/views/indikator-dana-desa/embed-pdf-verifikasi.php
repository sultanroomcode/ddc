<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//$model->file_indikator
?>
<iframe width="100%" height="450" src="<?= Url::base(true).'/userfile/'.$model->kd_desa.'/fi/'.$model->file_indikator ?>"></iframe>

<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Form Data</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'verifikasi_kab')->dropdownList([50 => 'Belum Valid', 100 => 'Valid']) ?>
                        <?= $form->field($model, 'komen_kab')->textarea(['maxlength' => true]) ?>
                    </div>
                </div>          

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);