<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Indikator Dana Desa > Publikasi Dana Desa > Penggunaan Dana Desa</h2>
            <div class="p-10"><a href="javascript:void(0)" onclick="goLoad({elm:'#main-page-form', url:'/sp3ddashboard/indikator-dana-desa/form-data?tipe=penggunaan-dana-desa'})" class="btn btn-success">Tambah</a>

            <div id="main-page-form"></div>
            <div id="main-page-indikator"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
    goLoad({elm:'#main-page-indikator', url:'/sp3ddashboard/indikator-dana-desa/list-data?tipe=penggunaan-dana-desa'});
JS;

$this->registerJs($script);