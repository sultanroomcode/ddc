<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;
$id = Yii::$app->user->identity->id;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">GRAFIK APBDES</h2>
            <div class="p-10">
                <div class="row">
                    <div class="col-md-2">
                        <select id="tahun" class="form-control">
                            <?php
                            for($i=2016; $i <= date('Y'); $i++){
                            ?>
                            <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#apbdes-zona', url: '/transaksi/default/penganggaran-desa?id=<?=$id?>&tahun=<?=$i?>&effect=slideInRight'})" class="btn btn-danger"><?= $i ?></a> -->
                            <option value="<?= $i ?>"> <?= $i ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-block btn-danger" onclick="filteringGrafik()">Filter</button>
                    </div>
                </div>
                <hr>
                <div id="apbdes-zona"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
function filteringGrafik(){
    var tahun = $('#tahun').val();
    var kode = '{$id}';
    goLoad({elm:'#apbdes-zona', url:'/umum/statistik/index?tahun='+tahun+'&id='+kode+'&type=desa'});
}
JS;

$this->registerJs($scripts);