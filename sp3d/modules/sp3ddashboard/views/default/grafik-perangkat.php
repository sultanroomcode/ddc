<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;
$id = Yii::$app->user->identity->id;
$tipeuser = Yii::$app->user->identity->type;
$arr = ['-' => ''];
if($tipeuser == 'provinsi'){
    $listKabArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
    $listKabArr = $arr + $listKabArr;
}

if($tipeuser == 'kabupaten'){
    $listKecArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
    $listKecArr = $arr + $listKecArr;
}

if($tipeuser == 'kecamatan'){
    $listDesArr = ArrayHelper::map(User::find()->where(['type' => 'desa'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
    $listDesArr = $arr + $listDesArr;
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">GRAFIK PERANGKAT DDC</h2>
            <div class="p-10">
                <div class="row">
                    <div class="col-md-2">
                        <select id="tipe" class="form-control">
                            <option value="bpd">BPD</option>
                            <option value="perangkat-desa">Perangkat Desa</option>
                            <option value="kepala-desa">Kepala Desa</option>
                        </select>
                    </div>
                    <?php if($tipeuser == 'provinsi'){ ?>
                    <div class="col-md-3">
                        <select id="kabupaten" class="form-control">
                            <?php
                            if(isset($listKabArr)){
                                foreach($listKabArr as $k => $v): ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php endforeach; 
                            }
                            ?>
                        </select>
                    </div>
                    <?php } else { echo '<input type="hidden" id="kabupaten" value="'.$id.'">'; }
                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten'){ ?>
                    <div class="col-md-3">
                        <select id="kecamatan" class="form-control">
                            <?php
                            if(isset($listKecArr)){
                                foreach($listKecArr as $k => $v): ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php endforeach; 
                            }
                            ?>
                        </select>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    }
                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten' || $tipeuser == 'kecamatan'){ ?>
                    <div class="col-md-3">
                        <select id="desa" class="form-control">
                            <?php
                            if(isset($listDesArr)){
                                foreach($listDesArr as $k => $v): ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php endforeach; 
                            }
                            ?>
                        </select>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    } ?>

                    <div class="col-md-1">
                        <button class="btn btn-block btn-danger" onclick="filteringAdd()">Filter</button>
                    </div>
                </div>
                <hr>
                <div id="apbdes-zona"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div id="modal-custom" style="display: none;"><div id="form-engine"><!-- sp3ddashboard/grafik --></div></div>

<?php
$scripts =<<<JS
$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});


$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

function filteringAdd(){
    var tahun = $('#tahun').val();
    var desa = $('#desa').val();
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    console.log('kab '+ kabupaten+' kec '+ kecamatan+' des ' + desa);
    var tipe = '';
    var kode = '';

    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    if(desa == '-' || desa == null){
        desa = '';
    }


    if(kabupaten == '' && kecamatan == '' && desa == ''){
        tipe = 'provinsi';
        kode = '35';
    }

    if(kabupaten != '' && kecamatan == '' && desa == ''){
        tipe = 'kabupaten';
        kode = kabupaten;
    }

    if(kabupaten != '' && kecamatan != '' && desa == ''){
        tipe = 'kecamatan';
        kode = kecamatan;
    }

    if(kabupaten != '' && kecamatan != '' && desa != ''){
        tipe = 'desa';
        kode = desa;
    }


    goLoad({elm:'#apbdes-zona', url:'/umum/statistik/index?tahun='+tahun+'&id='+kode+'&type='+tipe});
}
JS;

$this->registerJs($scripts);