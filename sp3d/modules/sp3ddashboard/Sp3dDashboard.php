<?php

namespace sp3d\modules\sp3ddashboard;

/**
 * sp3ddashboard module definition class
 */
class Sp3dDashboard extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'sp3d\modules\sp3ddashboard\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
