<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\LpmdPengurus */

$this->title = 'Isi Pengurus Lembaga Pemberdayaan Masyarakat Desa/Kelurahan';
?>
<div class="lpmd-pengurus-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
