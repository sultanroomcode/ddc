<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\LpmdPengurus */

$this->title = 'Update Pengurus Lembaga Pemberdayaan Masyarakat Desa/Kelurahan : ' . $model->nama;
?>
<div class="lpmd-pengurus-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
