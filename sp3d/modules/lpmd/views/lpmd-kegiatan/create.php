<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\LpmdKegiatan */

$this->title = 'Kegiatan Lembaga Pemberdayaan Masyarakat Desa/Kelurahan';
$this->params['breadcrumbs'][] = ['label' => 'Lpmd Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpmd-kegiatan-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
