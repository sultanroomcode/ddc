<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-6',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
    $model->genNum();
}

$urlback = 'goLoad({elm:\'#zona-lpmd-kegiatan\', url:\'/lpmd/lpmd-kegiatan/view-in-box?kd_desa='.$model->kd_desa.'\'});';
?>

<div class="lpmd-kegiatan-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_kegiatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'jenis')->dropdownList([
        'bimtek' => 'Bimtek',
        'pelatihan' => 'Pelatihan',
        'workshop' => 'Workshop',
        'seminar' => 'Seminar'
    ]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_mulai')->textInput() ?>

    <?= $form->field($model, 'sumber_dana')->dropdownList([
        'swadaya' => 'Swadaya Masyarakat',
        'apbd-prov' => 'APBD Provinsi',
        'apbd-kab' => 'APBD Kabupaten/Kota',
        'apbdes' => 'APBDesa',
        'bantuan-pemerintah' => 'Bantuan Pemerintah',
        'bantuan-pemprov' => 'Bantuan Pemerintah Provinsi',
        'bantuan-pemkab' => 'Bantuan Pemerintah Kabupaten/Kota',
        'bantuan-non-ikat' => 'Bantuan lain yang sah dan tidak mengikat',
    ]) ?>

    <div class="col-md-2"></div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="goLoad({elm:'#zona-lpmd-kegiatan', url:'/lpmd/lpmd-kegiatan/view-in-box?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger">Kembali</a>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
<?php 
$script = <<<JS
$('#lpmdkegiatan-tanggal_mulai').datetimepicker({
    format:'Y-m-d',
    mask:true
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);