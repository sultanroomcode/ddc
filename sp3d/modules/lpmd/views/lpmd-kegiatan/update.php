<?php
use yii\helpers\Html;

$this->title = 'Update Kegiatan Lembaga Pemberdayaan Masyarakat Desa/Kelurahan';
$this->params['breadcrumbs'][] = ['label' => 'Lpmd Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_kegiatan' => $model->id_kegiatan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lpmd-kegiatan-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
