<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-lpmd-sumberdana', url:'/lpmd/lpmd-sumber-dana/create?kd_desa=<?=$kd_desa?>'})" class="btn btn-danger">Tambah Sumber Dana</a>

<?php 
echo '<table class="table table-striped"><tr><th>Tahun</th><th>Jenis</th><th>Nilai</th><th>Aksi</th></tr>';
foreach($model->sumberdana as $v){
    echo '<tr><td>'.$v->tahun.'</td><td>'.$v->jenis.'</td><td>'.$v->nf($v->nilai).'</td><td>Aksi</td></tr>';
}
echo '</table>';	
?>