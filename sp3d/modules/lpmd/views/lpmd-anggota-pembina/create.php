<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\LpmdAnggotaPembina */

$this->title = 'Pembina Lembaga Pemberdayaan Masyarakat Desa/Kelurahan';
$this->params['breadcrumbs'][] = ['label' => 'Lpmd Anggota Pembinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpmd-anggota-pembina-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
