<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\LpmdAnggotaPembina */

$this->title = 'Update Pembina Lembaga Pemberdayaan Masyarakat Desa/Kelurahan : ' . $model->nama;
?>
<div class="lpmd-anggota-pembina-update">
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
