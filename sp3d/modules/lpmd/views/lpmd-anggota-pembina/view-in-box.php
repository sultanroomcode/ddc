<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
if($model->pembina_status_ada == 'ada'){
	?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-lpmd-pembina', url:'/lpmd/lpmd-anggota-pembina/create?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger"><?=(count($model->pembina) == 0)?'Isi':'Tambah'?> Pembina</a>
	<?php
	echo '<table class="table table-striped"><tr><th>Nama</th><th>Jabatan</th><th>Tanggal Lahir</th><th>Pendidikan</th><th>Pelatihan</th><th>Aksi</th></tr>';
	foreach($model->pembina as $v){
	    echo '<tr><td>'.$v->nama.'</td><td>'.$v->jabatandetail->nama_jabatan.'</td><td>'.$v->tanggal_lahir.'</td><td>'.substr($v->pendidikan, 2).'</td><td>'.count($v->pelatihan).'</td><td><a href="javascript:void(0)" onclick="openModalXyf({welm:\'600px\',url:\'/lpmd/lpmd-anggota-pembina/view?id_anggota='.$v->id_anggota.'&kd_desa='.$v->kd_desa.'\'})" class="btn btn-danger">View</a></td></tr>';
	}
	echo '</table>';	
} else {
	echo "Lembaga Pemberdayaan Masyarakat Desa/Kelurahan belum memiliki pembina";
}
?>