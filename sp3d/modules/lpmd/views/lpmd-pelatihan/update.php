<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kartar\models\KartarPelatihan */

$this->title = 'Update Riwayat Pelatihan/Workshop/Seminar/Bimtek';
$this->params['breadcrumbs'][] = ['label' => 'Kpm Pelatihans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'idp' => $model->idp]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kartar-pelatihan-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
