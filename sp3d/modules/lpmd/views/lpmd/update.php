<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\Lpmd */

$this->title = 'Update LPMD/K : ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lpmds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'id' => $model->kd_desa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lpmd-update">

    <h3><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
