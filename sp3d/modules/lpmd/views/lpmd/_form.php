<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
}

?>

<div class="lpmd-form">
    <div class="tile">
        <h2 class="tile-title">LPMD/K</h2>
        <div class="p-10">
            <?php $form = ActiveForm::begin($arrFormConfig); ?>

            <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
            <div class="row">
                <div class="col-md-5">
                <?= $form->field($model, 'ditetapkan_oleh')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'sk_lembaga')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'pembina_status_ada')->dropdownList(['ada' => 'Ada', 'tidak' => 'Tidak']) ?>

                <?= $form->field($model, 'pembina_ditetapkan_oleh')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'pembina_sk_penetapan')->textInput(['maxlength' => true]) ?>
                
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({url:'/lpmd/lpmd/view'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);