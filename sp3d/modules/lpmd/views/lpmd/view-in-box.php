<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<a href="javascript:void(0)" onclick="goLoad({url:'/lpmd/lpmd/update?kd_desa=<?=$model->kd_desa?>'});" class="btn btn-danger">Update</a>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'label' => 'Kabupaten',
            'value' => function($m){
                return substr($m->kabupaten->description, 10);
            }
        ],
        [
            'label' => 'Kecamatan',
            'value' => function($m){
                return substr($m->kecamatan->description, 10);
            }
        ],
        [
            'label' => 'Desa',
            'value' => function($m){
                return substr($m->desa->description, 5);
            }
        ],
        
        'sk_lembaga',
        'ditetapkan_oleh',
        'pembina_status_ada',
        'pembina_sk_penetapan',
        'pembina_ditetapkan_oleh',
    ],
]) ?>