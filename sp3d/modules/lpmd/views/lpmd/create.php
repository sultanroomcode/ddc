<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\Lpmd */

$this->title = 'Isi Data LPMD/K';
$this->params['breadcrumbs'][] = ['label' => 'Lkmds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpmd-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
