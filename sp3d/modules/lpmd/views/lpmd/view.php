<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lpmd\models\Lpmd */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lpmds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpmd-view">
    <div class="tile">
        <h2 class="tile-title">LPMD/K</h2>
        <div class="p-10">
            <div id="zona-lpmd-data"></div>
            <div id="zona-lpmd-pembina"></div>
            <div id="zona-lpmd-pengurus"></div>
            <div id="zona-lpmd-sumberdana"></div>
            <div id="zona-lpmd-kegiatan"></div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>
<?php 
$kd_desa = $model->kd_desa;
$script = <<<JS
goLoad({elm:'#zona-lpmd-data', url:'/lpmd/lpmd/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-lpmd-pengurus', url:'/lpmd/lpmd-pengurus/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-lpmd-pembina', url:'/lpmd/lpmd-anggota-pembina/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-lpmd-kegiatan', url:'/lpmd/lpmd-kegiatan/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-lpmd-sumberdana', url:'/lpmd/lpmd-sumber-dana/view-in-box?kd_desa={$kd_desa}'});
JS;
$this->registerJs($script);
?>