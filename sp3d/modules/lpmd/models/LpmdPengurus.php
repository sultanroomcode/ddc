<?php
namespace sp3d\modules\lpmd\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use sp3d\models\transaksi\Sp3dMasterJabatan;

/**
 * This is the model class for table "lkmd_pengurus".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_pengurus
 * @property string $jabatan
 * @property string $nama
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $sk_jabatan
 * @property string $pendidikan
 * @property string $status_aktif
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class LpmdPengurus extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lpmd_pengurus';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa', 'id_pengurus','nama', 'alamat', 'tempat_lahir', 'tanggal_lahir'], 'required'],
            [['id_pengurus'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['jabatan'], 'string', 'max' => 50],
            [['nama', 'tempat_lahir', 'tanggal_lahir', 'sk_jabatan', 'pendidikan', 'status_aktif'], 'string', 'max' => 100],
            [['alamat'], 'string', 'max' => 150],
            [['kd_desa', 'id_pengurus'], 'unique', 'targetAttribute' => ['kd_desa', 'id_pengurus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_pengurus' => 'Id Pengurus',
            'jabatan' => 'Jabatan',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'sk_jabatan' => 'Sk Jabatan',
            'pendidikan' => 'Jenjang Pendidikan Terakhir',
            'status_aktif' => 'Status Aktif',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diubah Oleh',
            'created_at' => 'Dibuat pada tanggal',
            'updated_at' => 'Diubah pada tanggal',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id_pengurus');
        if($mx == null){
            $this->id_pengurus = 1;
        } else {
            $this->id_pengurus = $mx +1;
        }
    }

    public function getJabatandetail()
    {
        return $this->hasOne(Sp3dMasterJabatan::className(), ['id' => 'jabatan']);
    }

    public function getPelatihan()
    {
        return $this->hasMany(LpmdPelatihan::className(), ['id' => 'id_pengurus', 'kd_desa' => 'kd_desa'])->onCondition(['tipe' => 'pengurus']);
    }
}
