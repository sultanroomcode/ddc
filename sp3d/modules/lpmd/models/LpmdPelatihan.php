<?php
namespace sp3d\modules\lpmd\models;
use sp3d\models\transaksi\Sp3dMasterJenisPelatihan;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kpm_pelatihan".
 *
 * @property string $kd_desa
 * @property integer $id
 * @property integer $idp
 * @property string $nama
 * @property string $tahun
 * @property string $penyelenggara
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class LpmdPelatihan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lpmd_pelatihan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id', 'idp', 'tipe'], 'required'],
            [['id', 'idp'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama', 'lembaga'], 'string', 'max' => 150],
            [['tahun'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'id' => 'ID',
            'idp' => 'Idp',
            'nama' => 'Nama Pelatihan/Workshop/Seminar/Bimtek',
            'tahun' => 'Tahun',
            'tipe' => 'Tipe',
            'lembaga' => 'Penyelenggara',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id' => $this->id])->max('idp');
        if($mx == null){
            $this->idp = 1;
        } else {
            $this->idp = $mx +1;
        }
    }
}
