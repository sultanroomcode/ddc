<?php

namespace sp3d\modules\lkmd\controllers;

use yii\web\Controller;

/**
 * Default controller for the `lkmd` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
