<?php
namespace sp3d\modules\lpmd\controllers;

use Yii;
use sp3d\modules\lpmd\models\LpmdSumberDana;
use sp3d\modules\lpmd\models\Lpmd;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LpmdPelatihanController implements the CRUD actions for LpmdPelatihan model.
 */
class LpmdSumberDanaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LpmdSumberDana models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LpmdSumberDana::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LpmdSumberDana model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionView($kd_desa, $id, $tipe)
    {
        return $this->renderAjax('view', [
            'data' => ['kd_desa' => $kd_desa, 'id' => $id, 'tipe' => $tipe],
            'model' => LpmdSumberDana::findAll(['kd_desa' => $kd_desa, 'id' => $id, 'tipe' => $tipe]),
        ]);
    }

    public function actionViewInBox($kd_desa)
    {
        $model = Lpmd::findOne(['kd_desa' => $kd_desa]);
        return $this->renderAjax('view-in-box', [
            'model' => $model,
            'kd_desa' => $kd_desa
        ]);
    }

    /**
     * Creates a new LpmdSumberDana model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa)
    {
        $model = new LpmdSumberDana();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing LpmdSumberDana model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing LpmdSumberDana model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the LpmdSumberDana model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return LpmdSumberDana the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $idp)
    {
        if (($model = LpmdSumberDana::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
