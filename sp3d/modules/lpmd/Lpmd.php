<?php
namespace sp3d\modules\lpmd;

/**
 * lkmd module definition class
 */
class Lpmd extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\lpmd\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
