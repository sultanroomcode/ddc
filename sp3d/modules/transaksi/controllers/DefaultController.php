<?php
namespace sp3d\modules\transaksi\controllers;
use Yii;
use yii\web\Controller;
use hscstudio\mimin\components\Mimin;
use yii\web\Response;
use sp3d\models\DesaCount;

class DefaultController extends Controller
{
	public $message = 'Transaksi Default';
	protected function check($char)
    {
        //return (Mimin::checkRoute($this->context->id.'/'.$char) || Mimin::checkRoute($this->context->id.'/*'));
        return (Mimin::checkRoute('transaksi/default/'.$char) || Mimin::checkRoute('transaksi/default/*'));
    }

    public function actionIndex()
    {
    	if($this->check('index')){
    		return $this->render('index');
    	} else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Index '.$this->message
            ]);
        }
    }
 
    public function actionDataCountingCenter($tahun, $kd_kecamatan, $kd_desa)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $mod = DesaCount::findOne(['tahun' => $tahun, 'kd_kecamatan' => $kd_kecamatan , 'kd_desa' => $kd_desa]);
        return ['data' => [
            'dana_anggaran' => 'Rp. '.number_format(round($mod->dana_anggaran), 0, ',','.'),
            'dana_pencairan' => 'Rp. '.number_format(round($mod->dana_pencairan), 0, ',','.'),
            'dana_kegiatan' => 'Rp. '.number_format(round($mod->dana_kegiatan), 0, ',','.'),
            'dana_mutasi' => 'Rp. '.number_format(round($mod->dana_mutasi), 0, ',','.'),
            'dana_pajak' => 'Rp. '.number_format(round($mod->dana_pajak), 0, ',','.'),
            'dana_rab' => 'Rp. '.number_format(round($mod->dana_rab), 0, ',','.'),
            'dana_spp' => 'Rp. '.number_format(round($mod->dana_spp), 0, ',','.'),
            'dana_spj' => 'Rp. '.number_format(round($mod->dana_spj), 0, ',','.'),
            'dana_sts' => 'Rp. '.number_format(round($mod->dana_sts), 0, ',','.'),
            'dana_tbp' => 'Rp. '.number_format(round($mod->dana_tbp), 0, ',','.'),
            
            'tabel_anggaran' => number_format(round($mod->data_anggaran), 0, ',','.').' Data Anggaran',
            'tabel_pencairan' => number_format(round($mod->data_pencairan), 0, ',','.').' Data Pencairan',
            'tabel_bidang' => number_format(round($mod->data_bidang), 0, ',','.').' Bidang',
            'tabel_kegiatan' => number_format(round($mod->data_kegiatan), 0, ',','.').' Data Kegiatan',
            'tabel_perangkat_desa' => number_format(round(3), 0, ',','.').' Perangkat',
            'tabel_jurnal_umum' => number_format(round(3), 0, ',','.').' Jurnal',
            'tabel_triwulan' => number_format(round(3), 0, ',','.').' Data',
            'tabel_mutasi' => number_format(round($mod->data_mutasi), 0, ',','.').' Data Mutasi',
            'tabel_pajak' => number_format(round($mod->data_pajak), 0, ',','.').' Data Pajak',
            'tabel_tbp' => number_format(round($mod->data_tbp), 0, ',','.').' Data TBP',
            'tabel_rab' => number_format(round($mod->data_rab), 0, ',','.').' RAB',
            'tabel_rpjm' => number_format(round(3), 0, ',','.').' Data',
            'tabel_spj' => number_format(round($mod->data_spj), 0, ',','.').' Data SPJ',
            'tabel_spp' => number_format(round($mod->data_spp), 0, ',','.').' Data SPP',
            'tabel_sts' => number_format(round($mod->data_sts), 0, ',','.').' Data STS'
        ]];
    }

    public function actionTahun($tahun)
    {
    	if($this->check('tahun')){
	        return $this->renderAjax('tahun', [
	            'tahun' => $tahun,
	        ]);
	    } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Tahun '.$this->message
            ]);
        }
    }

    public function actionKecamatan($id, $tahun)//diakses oleh admin
    {
        if($this->check('kecamatan')){
            return $this->renderAjax('kecamatan', [
                'id' => $id,
                'tahun' => $tahun
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Kecamatan '.$this->message
            ]);
        }
    }

    public function actionDesa($id, $tahun)
    {
        if($this->check('desa')){
            return $this->renderAjax('desa', [
                'id' => $id,
                'tahun' => $tahun
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Desa '.$this->message
            ]);
        }
    }

    //halaman untuk desa
    public function actionPerencanaanDesa($id,$tahun)
    {
        return $this->renderAjax('perencanaan-desa', [
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionPenganggaranDesa($id,$tahun)
    {
        return $this->renderAjax('penganggaran-desa', [
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionPenataUsahaanDesa($id,$tahun)
    {
        return $this->renderAjax('penata-usahaan-desa', [
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionPembukuanDesa($id,$tahun)
    {
        return $this->renderAjax('pembukuan-desa', [
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaPage($tahun)
    {
        return $this->renderAjax('desa-page', [
            'tahun' => $tahun
        ]);
    }

    //halaman untuk kecamatan
    public function actionKecamatanPage($id=null,$tahun)
    {
        if($this->check('kecamatan-page')){
            return $this->renderAjax('kecamatan-page', [
                'tahun' => $tahun,
                'id' => $id
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Kecamatan Page '.$this->message
            ]);
        }
    }

    //halaman untuk kabupaten
    public function actionKabupatenPage($id=null,$tahun)
    {
        if($this->check('kabupaten-page')){
            return $this->renderAjax('kabupaten-page', [
                'tahun' => $tahun,
                'id' => $id
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Kabupaten Page '.$this->message
            ]);
        }
    }

    //halaman untuk provinsi
    public function actionProvinsiPage($id=null,$tahun)
    {
        if($this->check('provinsi-page')){
            return $this->renderAjax('provinsi-page', [
                'tahun' => $tahun,
                'id' => $id
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Provinsi Page '.$this->message
            ]);
        }
    }

    //halaman untuk dpmdkb
    public function actionDpmdkbPage($tahun)
    {
        if($this->check('dpmdkb-page')){
            return $this->renderAjax('dpmdkb-page', [
                'tahun' => $tahun
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Desa Page '.$this->message
            ]);
        }
    }
}
