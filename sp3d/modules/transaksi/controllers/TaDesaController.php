<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaDesaController implements the CRUD actions for TaDesa model.
 */
class TaDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaDesa::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $m = TaDesa::findOne(['Tahun' => $tahun, 'Kd_Desa' => $id]);
        if($m == null){
            $model = new TaDesa();

            if ($model->load(Yii::$app->request->post())){
                if($model->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('desa-form', [
                    'model' => $model,
                    'id' => $id,
                    'tahun' => $tahun
                ]);
            }    
        } else {
            return $this->renderAjax('desa-view', [
                'model' => $m,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
        
    }

    public function actionDesaUpdate($id, $tahun)
    {
        $model = $this->findModel($tahun, $id);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun)
    {
        return $this->renderAjax('desa-view', [
            'model' => $this->findModel($tahun, $id),
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun)
    {
        if($this->findModel($tahun,$id)->delete()) echo 1; else echo 0;
    }


    

    /**
     * Lists all TaDesa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaDesa::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaDesa model.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Desa)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Tahun, $Kd_Desa),
        ]);
    }

    /**
     * Creates a new TaDesa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaDesa();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaDesa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Desa)
    {
        $model = $this->findModel($Tahun, $Kd_Desa);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaDesa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Desa)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaDesa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return TaDesa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Desa)
    {
        if (($model = TaDesa::findOne(['Tahun' => $Tahun, 'Kd_Desa' => $Kd_Desa])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
