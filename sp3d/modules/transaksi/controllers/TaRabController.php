<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaRAB;
use sp3d\models\transaksi\TaKegiatan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabController implements the CRUD actions for TaRAB model.
 */
class TaRabController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //RAB
    public function actionDaftarBelanja($tahun, $kdrincian)
    {
        // $dataProvider = TaRAB::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun, 'kegiatan' => 'belanja']);
        $model = TaRAB::find()->where(['user_id' => Yii::$app->user->identity->id,'Tahun' => $tahun, 'Kd_Keg' => $kdrincian, 'f_verified' => 1]);
        //$model = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('daftar-belanja', [
            'model' => $model,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaMainForm($id, $tahun,$automate=false, $kode=null)
    {
        $model = new TaRAB();
        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-main-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun,
                'kode' => $kode,
                'automate' => $automate
            ]);
        }
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        // $dataProvider = TaRAB::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun, 'kegiatan' => 'belanja']);
        $dataProvider = TaRAB::find()->where(['user_id' => $id,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '5.%', false]);
        //$dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun, $automate=false, $kode=null)
    {
        $model = new TaRAB();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun,
                'kode' => $kode,
                'automate' => $automate
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }
    
    public function actionDesaView($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun,'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaVisual($id, $tahun, $effect)
    {
        return $this->renderAjax('desa-visual-sp3d', [
            'id' => $id,
            'tahun' => $tahun,
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);
        
        if($model->delete()) echo 1; else echo 0;
    }
    //RAP ... Pendapatan
    public function actionPendapatanDesaIndex($id, $tahun, $effect)
    {
        return $this->renderAjax('pendapatan-desa-index', [
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPendapatanDesaList($id, $tahun, $effect)
    {
        // $dataProvider = TaRAB::find()->where(['user_id' => $id,'Tahun' => $tahun, 'f_verified' => 0])->join('LEFT JOIN','Ref_Rek4', 'Ref_Rek4.Obyek = Ta_RAB.Kd_Rincian COLLATE utf8_general_ci');
        $dataProvider = TaRAB::find()->select(['Ta_RAB.*', 'Ref_Rek4.Nama_Obyek AS nama_rincian', 'Ref_Rek4.kd_kabupaten'])->join('LEFT JOIN','Ref_Rek4', 'Ref_Rek4.Obyek = Ta_RAB.Kd_Rincian COLLATE utf8_general_ci')->where(['user_id' => $id,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '4.%', false]);
        // $dataProvider = TaRAB::find()->select(['Ta_RAB.*', 'Ref_Rek4.Nama_Obyek AS nama_rincian', 'Ref_Rek4.kd_kabupaten'])->join('LEFT JOIN','Ref_Rek4', 'Ref_Rek4.Obyek = Ta_RAB.Kd_Rincian COLLATE utf8_general_ci')->where(['Ref_Rek4.kd_kabupaten' => substr($id, 0,4), 'user_id' => $id,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '4.%', false]);

        return $this->renderAjax('pendapatan-desa-list', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPendapatanDesaVisual($id, $tahun, $effect)
    {
        return $this->renderAjax('pendapatan-desa-visual', [
            'id' => $id,
            'tahun' => $tahun,
        ]);
    }

    public function actionPendapatanDesaMainForm($id, $tahun,$automate=false, $kode=null)
    {
        $model = new TaRAB();
        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('pendapatan-desa-main-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun,
                'kode' => $kode,
                'automate' => $automate
            ]);
        }
    }

    public function actionPendapatanDesaCreate($id, $tahun,$automate=false, $kode=null)
    {
        $model = new TaRAB();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('pendapatan-desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun,
                'kode' => $kode,
                'automate' => $automate
            ]);
        }
    }

    public function actionPendapatanDesaUpdate($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('pendapatan-desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionPendapatanDesaView($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['user_id' => $id,'Tahun' => $tahun,'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        return $this->renderAjax('pendapatan-desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionPendapatanDesaDelete($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);
        
        if($model->delete()) echo 1; else echo 0;
    }




    //RAP ... Penerimaan
    public function actionPenerimaanDesaIndex($id, $tahun, $effect)
    {
        return $this->renderAjax('penerimaan-desa-index', [
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPenerimaanDesaList($id, $tahun, $effect)
    {
        $dataProvider = TaRAB::find()->where(['user_id' => $id,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '6.1.%', false]);

        return $this->renderAjax('penerimaan-desa-list', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPenerimaanDesaMainForm($id, $tahun, $effect)
    {
        return $this->renderAjax('penerimaan-desa-main-form', [
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPenerimaanDesaCreate($id, $tahun,$automate=false, $kode=null)
    {
        $model = new TaRAB();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('penerimaan-desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun,
                'kode' => $kode,
                'automate' => $automate
            ]);
        }
    }

    public function actionPenerimaanDesaUpdate($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('penerimaan-desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionPenerimaanDesaView($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun,'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        return $this->renderAjax('penerimaan-desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionPenerimaanDesaDelete($id, $tahun, $kd, $kd2)
    {
        if(TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2])->delete()) echo 1; else echo 0;
    }


    //RAP ... Pengeluaran
    public function actionPengeluaranDesaIndex($id, $tahun, $effect)
    {
        return $this->renderAjax('pengeluaran-desa-index', [
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPengeluaranDesaList($id, $tahun, $effect)
    {
        $dataProvider = TaRAB::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '6.2.%', false]);

        return $this->renderAjax('pengeluaran-desa-list', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPengeluaranDesaMainForm($id, $tahun, $effect)
    {
        return $this->renderAjax('pengeluaran-desa-main-form', [
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionPengeluaranDesaCreate($id, $tahun,$automate=false, $kode=null)
    {
        $model = new TaRAB();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('pengeluaran-desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun,
                'kode' => $kode,
                'automate' => $automate
            ]);
        }
    }

    public function actionPengeluaranDesaUpdate($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('pengeluaran-desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionPengeluaranDesaView($id, $tahun, $kd, $kd2)
    {
        $model = TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun,'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2]);

        return $this->renderAjax('pengeluaran-desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionPengeluaranDesaDelete($id, $tahun, $kd, $kd2)
    {
        if(TaRAB::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd, 'Kd_Rincian' => $kd2])->delete()) echo 1; else echo 0;
    }


    

    

    /**
     * Finds the TaRAB model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @return TaRAB the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian)
    {
        if (($model = TaRAB::findOne(['Tahun' => $Tahun, 'Kd_Desa' => $Kd_Desa, 'Kd_Keg' => $Kd_Keg, 'Kd_Rincian' => $Kd_Rincian])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
