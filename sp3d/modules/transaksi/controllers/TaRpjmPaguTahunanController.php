<?php

namespace frontend\modules\transaksi\controllers;

use Yii;
use frontend\models\transaksi\TaRPJMPaguTahunan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRpjmPaguTahunanController implements the CRUD actions for TaRPJMPaguTahunan model.
 */
class TaRpjmPaguTahunanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaRPJMPaguTahunan::find()->where(['Kd_Desa' => $id]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaRPJMPaguTahunan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd, $kd2, $kd3)
    {
        $model = TaRPJMPaguTahunan::findOne(['Kd_Desa' => $id, 'Kd_Keg' => $kd, 'Kd_Tahun' => $kd2, 'Kd_Sumber' => $kd3]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd, $kd2, $kd3)
    {
        $model = TaRPJMPaguTahunan::findOne(['Kd_Desa' => $id,'Kd_Keg' => $kd, 'Kd_Tahun' => $kd2, 'Kd_Sumber' => $kd3]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }


    public function actionDesaDelete($id, $tahun, $kd, $kd2, $kd3)
    {
        if(TaRPJMPaguTahunan::findOne(['Kd_Desa' => $id,'Kd_Keg' => $kd, 'Kd_Tahun' => $kd2, 'Kd_Sumber' => $kd3])->delete()) echo 1; else echo 0;
    }


    

    /**
     * Lists all TaRPJMPaguTahunan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaRPJMPaguTahunan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaRPJMPaguTahunan model.
     * @param string $Kd_Keg
     * @param string $Kd_Tahun
     * @param string $Kd_Sumber
     * @return mixed
     */
    public function actionView($Kd_Keg, $Kd_Tahun, $Kd_Sumber)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Kd_Keg, $Kd_Tahun, $Kd_Sumber),
        ]);
    }

    /**
     * Creates a new TaRPJMPaguTahunan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaRPJMPaguTahunan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaRPJMPaguTahunan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Kd_Keg
     * @param string $Kd_Tahun
     * @param string $Kd_Sumber
     * @return mixed
     */
    public function actionUpdate($Kd_Keg, $Kd_Tahun, $Kd_Sumber)
    {
        $model = $this->findModel($Kd_Keg, $Kd_Tahun, $Kd_Sumber);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaRPJMPaguTahunan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Kd_Keg
     * @param string $Kd_Tahun
     * @param string $Kd_Sumber
     * @return mixed
     */
    public function actionDelete($Kd_Keg, $Kd_Tahun, $Kd_Sumber)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaRPJMPaguTahunan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Kd_Keg
     * @param string $Kd_Tahun
     * @param string $Kd_Sumber
     * @return TaRPJMPaguTahunan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Kd_Keg, $Kd_Tahun, $Kd_Sumber)
    {
        if (($model = TaRPJMPaguTahunan::findOne(['Kd_Keg' => $Kd_Keg, 'Kd_Tahun' => $Kd_Tahun, 'Kd_Sumber' => $Kd_Sumber])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
