<?php

namespace frontend\modules\transaksi\controllers;

use Yii;
use frontend\models\transaksi\TaPajakRinci;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaPajakRinciController implements the CRUD actions for TaPajakRinci model.
 */
class TaPajakRinciController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun)
    {
        $dataProvider = TaRPJMBidang::find();

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaDelete($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }


    

    /**
     * Lists all TaPajakRinci models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaPajakRinci::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaPajakRinci model.
     * @param string $No_SSP
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionView($No_SSP, $No_Bukti, $Kd_Rincian)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($No_SSP, $No_Bukti, $Kd_Rincian),
        ]);
    }

    /**
     * Creates a new TaPajakRinci model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaPajakRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaPajakRinci model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $No_SSP
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionUpdate($No_SSP, $No_Bukti, $Kd_Rincian)
    {
        $model = $this->findModel($No_SSP, $No_Bukti, $Kd_Rincian);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaPajakRinci model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $No_SSP
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionDelete($No_SSP, $No_Bukti, $Kd_Rincian)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaPajakRinci model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $No_SSP
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return TaPajakRinci the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($No_SSP, $No_Bukti, $Kd_Rincian)
    {
        if (($model = TaPajakRinci::findOne(['No_SSP' => $No_SSP, 'No_Bukti' => $No_Bukti, 'Kd_Rincian' => $Kd_Rincian])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
