<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dPendampingDesa;
use sp3d\models\transaksi\Sp3dPendampingPilihDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabRinciController implements the CRUD actions for TaRABRinci model.
 */
class Sp3dPendampingDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionData($id, $tahun)
    {
        $dataProvider = Sp3dPendampingDesa::find()->where(['user' => Yii::$app->user->identity->id]);

        return $this->renderAjax('list-pendamping', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionListPendamping() 
    {
        $dataProvider = Sp3dPendampingPilihDesa::find()->where(['kd_desa' => Yii::$app->user->identity->id]);

        return $this->renderAjax('list-pendamping', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionViewPendamping($nik) 
    {
        $dataProvider = Sp3dPendampingDesa::findOne(['nik' => $nik]);

        return $this->renderAjax('view-pendamping', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionFormPendamping() 
    {
        $model = new Sp3dPendampingDesa();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-pendamping', [
                'model' => $model
            ]);
        }
    }

    public function actionUpdatePendamping($nik, $user) 
    {
        $model = Sp3dPendampingDesa::findOne(['nik' => $nik, 'user' => $user]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-pendamping', [
                'model' => $model
            ]);
        }
    }

    public function actionUpdateVerifikasiPendamping($nik, $user) 
    {
        $model = Sp3dPendampingPilihDesa::findOne(['nik' => $nik, 'kd_desa' => $user]);
        $model->scenario = 'verifikasi';
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-verifikasi-pendamping', [
                'model' => $model
            ]);
        }
    }

    public function actionDeletePendamping($nik, $user) 
    {
        $dataProvider = Sp3dPendampingDesa::findOne(['nik' => $nik, 'user' => $user]);
        if($dataProvider->delete()){
            echo "<script>$.alert('Data {$nik} sudah dihapus');goLoad({url: '/transaksi/sp3d-pendamping-desa/list-pendamping'});</script>";
        } else {
            echo "<script>$.alert('Gagal hapus data {$nik}');goLoad({url: '/transaksi/sp3d-pendamping-desa/list-pendamping'});</script>";
        }
    }
}
