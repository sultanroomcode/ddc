<?php

namespace frontend\modules\transaksi\controllers;

use Yii;
use frontend\models\transaksi\TaPencairan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaPencairanController implements the CRUD actions for TaPencairan model.
 */
class TaPencairanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaPencairan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaPencairan();

        if ($model->load(Yii::$app->request->post())){
            $data['tahun'] = $tahun;
            $data['kd_kabupaten'] = Yii::$app->user->identity->chapter->desa->kd_kabupaten; 
            $data['kd_kecamatan'] = Yii::$app->user->identity->chapter->desa->Kd_Kec;
            $data['kd_desa'] = Yii::$app->user->identity->chapter->Kd_Desa;
            $data['balance'] = 'tambah';

            $model->updatingPencairanDesaCount($data);

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $no, $no2)
    {
        $model = TaPencairan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'No_Cek' => $no, 'No_SPP' => $no2]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $no, $no2)
    {
        $model = TaPencairan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun,'No_Cek' => $no, 'No_SPP' => $no2]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $no, $no2)
    {
        $model = TaPencairan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'No_Cek' => $no, 'No_SPP' => $no2]);
        $data['tahun'] = $tahun;
        $data['kd_desa'] = Yii::$app->user->identity->chapter->Kd_Desa;
        $data['balance'] = 'kurang';

        $model->updatingPencairanDesaCount($data);
        if($model->delete()) echo 1; else echo 0;
    }



    

    /**
     * Lists all TaPencairan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaPencairan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaPencairan model.
     * @param string $No_Cek
     * @param string $No_SPP
     * @return mixed
     */
    public function actionView($No_Cek, $No_SPP)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($No_Cek, $No_SPP),
        ]);
    }

    /**
     * Creates a new TaPencairan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaPencairan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaPencairan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $No_Cek
     * @param string $No_SPP
     * @return mixed
     */
    public function actionUpdate($No_Cek, $No_SPP)
    {
        $model = $this->findModel($No_Cek, $No_SPP);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaPencairan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $No_Cek
     * @param string $No_SPP
     * @return mixed
     */
    public function actionDelete($No_Cek, $No_SPP)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaPencairan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $No_Cek
     * @param string $No_SPP
     * @return TaPencairan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($No_Cek, $No_SPP)
    {
        if (($model = TaPencairan::findOne(['No_Cek' => $No_Cek, 'No_SPP' => $No_SPP])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
