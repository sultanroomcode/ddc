<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaUserMenu;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaUserMenuController implements the CRUD actions for TaUserMenu model.
 */
class TaUserMenuController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        if(TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd])->delete()) echo 1; else echo 0;
    }


    



    /**
     * Lists all TaUserMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaUserMenu::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaUserMenu model.
     * @param string $UserID
     * @param string $IDMenu
     * @return mixed
     */
    public function actionView($UserID, $IDMenu)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($UserID, $IDMenu),
        ]);
    }

    /**
     * Creates a new TaUserMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaUserMenu();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaUserMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $UserID
     * @param string $IDMenu
     * @return mixed
     */
    public function actionUpdate($UserID, $IDMenu)
    {
        $model = $this->findModel($UserID, $IDMenu);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaUserMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $UserID
     * @param string $IDMenu
     * @return mixed
     */
    public function actionDelete($UserID, $IDMenu)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaUserMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $UserID
     * @param string $IDMenu
     * @return TaUserMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($UserID, $IDMenu)
    {
        if (($model = TaUserMenu::findOne(['UserID' => $UserID, 'IDMenu' => $IDMenu])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
