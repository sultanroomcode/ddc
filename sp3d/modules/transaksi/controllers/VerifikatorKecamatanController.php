<?php

namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dVerifikatorKec;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VerifikatorKecamatanController implements the CRUD actions for Sp3dVerifikatorKec model.
 */
class VerifikatorKecamatanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sp3dVerifikatorKec models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sp3dVerifikatorKec::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sp3dVerifikatorKec model.
     * @param string $nik
     * @param string $user
     * @return mixed
     */
    public function actionView($nik, $user)
    {
        return $this->render('view', [
            'model' => $this->findModel($nik, $user),
        ]);
    }

    /**
     * Creates a new Sp3dVerifikatorKec model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sp3dVerifikatorKec();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nik' => $model->nik, 'user' => $model->user]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Sp3dVerifikatorKec model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nik
     * @param string $user
     * @return mixed
     */
    public function actionUpdate($nik, $user)
    {
        $model = $this->findModel($nik, $user);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nik' => $model->nik, 'user' => $model->user]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sp3dVerifikatorKec model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nik
     * @param string $user
     * @return mixed
     */
    public function actionDelete($nik, $user)
    {
        $this->findModel($nik, $user)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sp3dVerifikatorKec model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nik
     * @param string $user
     * @return Sp3dVerifikatorKec the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nik, $user)
    {
        if (($model = Sp3dVerifikatorKec::findOne(['nik' => $nik, 'user' => $user])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
