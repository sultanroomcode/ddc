<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaTriwulanArsip;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaTriwulanArsipController implements the CRUD actions for TaTriwulanArsip model.
 */
class TaTriwulanArsipController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        if(TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd])->delete()) echo 1; else echo 0;
    }


    



    /**
     * Lists all TaTriwulanArsip models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaTriwulanArsip::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaTriwulanArsip model.
     * @param string $KdPosting
     * @param string $KURincianSD
     * @param string $Tahun
     * @return mixed
     */
    public function actionView($KdPosting, $KURincianSD, $Tahun)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($KdPosting, $KURincianSD, $Tahun),
        ]);
    }

    /**
     * Creates a new TaTriwulanArsip model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaTriwulanArsip();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaTriwulanArsip model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $KdPosting
     * @param string $KURincianSD
     * @param string $Tahun
     * @return mixed
     */
    public function actionUpdate($KdPosting, $KURincianSD, $Tahun)
    {
        $model = $this->findModel($KdPosting, $KURincianSD, $Tahun);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaTriwulanArsip model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $KdPosting
     * @param string $KURincianSD
     * @param string $Tahun
     * @return mixed
     */
    public function actionDelete($KdPosting, $KURincianSD, $Tahun)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaTriwulanArsip model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $KdPosting
     * @param string $KURincianSD
     * @param string $Tahun
     * @return TaTriwulanArsip the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($KdPosting, $KURincianSD, $Tahun)
    {
        if (($model = TaTriwulanArsip::findOne(['KdPosting' => $KdPosting, 'KURincianSD' => $KURincianSD, 'Tahun' => $Tahun])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
