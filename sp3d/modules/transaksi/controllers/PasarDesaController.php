<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\Pasardesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PasarDesaController implements the CRUD actions for Pasardesa model.
 */
class PasarDesaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionListReportPasarDesa()
    {
        return $this->renderAjax('list-report-pasar-desa');
    }

    /**
     * Lists all Pasardesa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = Pasardesa::find()->where(['kd_desa' => Yii::$app->user->identity->id]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pasardesa model.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_pasar)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_pasar),
        ]);
    }

    /**
     * Creates a new Pasardesa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pasardesa();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pasardesa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_pasar)
    {
        $model = $this->findModel($kd_desa, $id_pasar);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pasardesa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_pasar)
    {
        $model = $this->findModel($kd_desa, $id_pasar)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the Pasardesa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @return Pasardesa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_pasar)
    {
        if (($model = Pasardesa::findOne(['kd_desa' => $kd_desa, 'id_pasar' => $id_pasar])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
