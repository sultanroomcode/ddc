<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaKegiatan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaKegiatanController implements the CRUD actions for TaKegiatan model.
 */
class TaKegiatanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaIsiKegiatan($id, $tahun, $kd_bid, $kd_prog=null)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            //mengisi rab belanja di beforeSave
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-isi-kegiatan-form', [
                'model' => $model,
                'id' => $id,
                'kd_bid' => $kd_bid,
                'kd_prog' => $kd_prog,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['user_id' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);
        $data['tahun'] = $tahun;
        $data['kd_kabupaten'] = Yii::$app->user->identity->chapter->desa->kd_kabupaten; 
        $data['kd_kecamatan'] = Yii::$app->user->identity->chapter->desa->Kd_Kec;
        $data['kd_desa'] = Yii::$app->user->identity->chapter->Kd_Desa;
        $data['balance'] = 'kurang';

        $model->updatingKegiatanDesaCount($data);

        if($model->delete()) echo 1; else echo 0;
    }


    

    /**
     * Lists all TaKegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaKegiatan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaKegiatan model.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Keg)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Tahun, $Kd_Keg),
        ]);
    }

    /**
     * Creates a new TaKegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaKegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Keg)
    {
        $model = $this->findModel($Tahun, $Kd_Keg);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaKegiatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Keg)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaKegiatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @return TaKegiatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Keg)
    {
        if (($model = TaKegiatan::findOne(['Tahun' => $Tahun, 'Kd_Keg' => $Kd_Keg])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
