<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaSPPRinci;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaSppRinciController implements the CRUD actions for TaSPPRinci model.
 */
class TaSppRinciController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        if(TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd])->delete()) echo 1; else echo 0;
    }


    



    /**
     * Lists all TaSPPRinci models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaSPPRinci::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaSPPRinci model.
     * @param string $No_SPP
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Sumberdana
     * @return mixed
     */
    public function actionView($No_SPP, $Kd_Keg, $Kd_Rincian, $Sumberdana)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($No_SPP, $Kd_Keg, $Kd_Rincian, $Sumberdana),
        ]);
    }

    /**
     * Creates a new TaSPPRinci model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaSPPRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaSPPRinci model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $No_SPP
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Sumberdana
     * @return mixed
     */
    public function actionUpdate($No_SPP, $Kd_Keg, $Kd_Rincian, $Sumberdana)
    {
        $model = $this->findModel($No_SPP, $Kd_Keg, $Kd_Rincian, $Sumberdana);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaSPPRinci model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $No_SPP
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Sumberdana
     * @return mixed
     */
    public function actionDelete($No_SPP, $Kd_Keg, $Kd_Rincian, $Sumberdana)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaSPPRinci model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $No_SPP
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Sumberdana
     * @return TaSPPRinci the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($No_SPP, $Kd_Keg, $Kd_Rincian, $Sumberdana)
    {
        if (($model = TaSPPRinci::findOne(['No_SPP' => $No_SPP, 'Kd_Keg' => $Kd_Keg, 'Kd_Rincian' => $Kd_Rincian, 'Sumberdana' => $Sumberdana])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
