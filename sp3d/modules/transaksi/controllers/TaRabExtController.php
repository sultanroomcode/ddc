<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaRABExt;
use sp3d\models\transaksi\TaKegiatan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabController implements the CRUD actions for TaRAB model.
 */
class TaRabExtController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionCheckParentPendapatan($id_child, $kd_desa)
    {
        $exp = explode('.', $id_child);
        $vb = '';
        $vr = [];
        foreach ($exp as $v) {
            $vb .= $v.'.';
            $vr[] = rtrim($vb,'. ');// for in https://stackoverflow.com/questions/5592994/remove-the-last-character-from-string
        }
        unset($vr[0], $vr[count($exp)-1]);
        $data = TaRABExt::find()->select(['Kd_Rincian'])->where(['in', 'Kd_Rincian', $vr])->andWhere(['Kd_Desa' => $kd_desa]);

        if($data->count() > 0){
            $txt = [];
            foreach ($data->all() as $v) {
                $txt[] = $v->Kd_Rincian;
            }

            $arr = [
                'count' => $data->count(),
                'createvar' => implode('::',array_diff($vr,$txt)),//paremeter pertama sebagai needle, dan kedua sebagai pool
                'existvar' => implode('::', $txt)
            ];
        } else {
            $arr = [
                'count' => 0,
                'createvar' => implode('::', $vr),
                'existvar' => ''
            ];
        }

        echo json_encode($arr);
    }
}
