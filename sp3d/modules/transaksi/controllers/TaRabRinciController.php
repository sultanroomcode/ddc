<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaKegiatanSub;
use sp3d\models\transaksi\TaKegiatan;
use sp3d\models\transaksi\TaRABRinci;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabRinciController implements the CRUD actions for TaRABRinci model.
 */
class TaRabRinciController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun)
    {
        $dataProvider = TaRPJMBidang::find();

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionLihatBelanja($id, $tahun, $kd, $nokeg, $effect=null) //user : ta-Kegiatan-sub/kegiatan-desa
    {
        $dataProvider = TaKegiatan::findOne(['Tahun' => $tahun, 'Kd_Desa' => $id, 'Kd_Keg' => $kd]);

        return $this->renderAjax('lihat-belanja', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'kd' => $kd,
            'nokeg' => $nokeg,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionUpdateBelanjaRincian($id, $tahun, $kd, $nokeg, $nourut, $effect=null) //user : ta-Kegiatan-sub/kegiatan-desa
    {
        $model = TaRABRinci::findOne(['Tahun' => $tahun, 'Kd_Desa' => $id, 'No_Urut' => $nourut, 'Kd_Keg' => $kd, 'No_Keg' => $nokeg]);
        
        if ($model->load(Yii::$app->request->post())){
            
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'kd' => $kd,
                'nokeg' => $nokeg,
                'tahun' => $tahun,
                'effect' => $effect
            ]);
        }
    }

    public function actionLihatBelanjaRincian($id, $tahun, $kd, $nokeg, $nourut, $effect=null) //user : ta-Kegiatan-sub/kegiatan-desa
    {
        $model = TaRABRinci::findOne(['Tahun' => $tahun, 'Kd_Desa' => $id, 'No_Urut' => $nourut, 'Kd_Keg' => $kd, 'No_Keg' => $nokeg]);

        return $this->renderAjax('lihat-belanja-rincian', [
            'model' => $model,
            'id' => $id,
            'kd' => $kd,
            'nokeg' => $nokeg,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionHapusBelanjaRincian($id, $tahun, $kd, $nokeg, $nourut, $effect=null) //user : ta-Kegiatan-sub/kegiatan-desa
    {
        $model = TaRABRinci::findOne(['Tahun' => $tahun, 'Kd_Desa' => $id, 'No_Urut' => $nourut, 'Kd_Keg' => $kd, 'No_Keg' => $nokeg]);
        if($model->delete()) echo 1; else echo 0;
    }

    public function actionDesaCreate($id, $tahun, $kd, $kd2, $nokeg)//nokeg hanya untuk belanja
    {
        $model = new TaRABRinci();

        if ($model->load(Yii::$app->request->post())){
            
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'kd' => $kd,
                'kd2' => $kd2,
                'nokeg' => $nokeg,
                'tahun' => $tahun
            ]);
        }
    }
    
    //$id, $tahun, $kd, $kd2
    public function actionIsiRincianPendapatan($id, $tahun, $kd, $kd2)
    {
        $model = new TaRABRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('pendapatan-desa-form', [
                'model' => $model,
                'id' => $id,
                'kd' => $kd,
                'kd2' => $kd2,
                'tahun' => $tahun
            ]);
        }
    }

    //$id, $tahun, $kd, $kd2
    public function actionIsiRincianPenerimaan($id, $tahun, $kd, $kd2)
    {
        $model = new TaRABRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('penerimaan-desa-form', [
                'model' => $model,
                'id' => $id,
                'kd' => $kd,
                'kd2' => $kd2,
                'tahun' => $tahun
            ]);
        }
    }

    //$id, $tahun, $kd, $kd2
    public function actionIsiRincianPengeluaran($id, $tahun, $kd, $kd2)
    {
        $model = new TaRABRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('pengeluaran-desa-form', [
                'model' => $model,
                'id' => $id,
                'kd' => $kd,
                'kd2' => $kd2,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd, $kd2, $kd3, $no)
    {
        $model = $this->findModel($tahun, $id, $kd, $kd2, $kd3, $no);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'kd' => $kd2,//diputar hanya untuk update
                'kd2' => $kd,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd, $kd2, $kd3, $no)
    {
        return $this->renderAjax('desa-view', [
            'model' => $this->findModel($tahun, $id, $kd, $kd2, $kd3, $no),
        ]);
    }
    //$Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut
    public function actionDesaDelete($id, $tahun, $kd, $kd2, $kd3, $no)
    {
        if($this->findModel($tahun, $id, $kd, $kd2, $kd3, $no)->delete()) echo 1; else echo 0;
    }



    

    /**
     * Lists all TaRABRinci models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaRABRinci::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaRABRinci model.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut),
        ]);
    }

    /**
     * Creates a new TaRABRinci model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaRABRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaRABRinci model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        $model = $this->findModel($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaRABRinci model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaRABRinci model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return TaRABRinci the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        if (($model = TaRABRinci::findOne(['Tahun' => $Tahun, 'Kd_Desa' => $Kd_Desa, 'Kd_Keg' => $Kd_Keg, 'Kd_Rincian' => $Kd_Rincian, 'Kd_SubRinci' => $Kd_SubRinci, 'No_Urut' => $No_Urut])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
