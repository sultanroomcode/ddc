<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaAnggaran;
use hscstudio\mimin\components\Mimin;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaAnggaranController implements the CRUD actions for TaAnggaran model.
 * Dev By : AugStr agussutarom@gmail.com
 * Dev 4 : ARTech Software Developer and Integration
 */
class TaAnggaranController extends Controller
{
    /**
     * @inheritdoc
     */
    public $message = 'Transaksi Anggaran';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    protected function check($char)
    {
        //return (Mimin::checkRoute($this->context->id.'/'.$char) || Mimin::checkRoute($this->context->id.'/*'));
        return (Mimin::checkRoute('transaksi/ta-anggaran/'.$char) || Mimin::checkRoute('transaksi/ta-anggaran/*'));
    }

    /**
     * Lists all TaAnggaran models.
     * @return mixed
     */
    public function actionDesaIndex($id, $tahun)
    {
        if($this->check('desa-index')){
            $dataProvider = TaAnggaran::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

            return $this->renderAjax('desa-index', [
                'dataProvider' => $dataProvider,
                'id' => $id,
                'tahun' => $tahun
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Index '.$this->message
            ]);
        }
    }

    public function actionDesaCreate($id, $tahun)
    {
        if($this->check('desa-create')){
            $model = new TaAnggaran();

            if ($model->load(Yii::$app->request->post())){
                $data['tahun'] = $tahun;
                $data['kd_kabupaten'] = Yii::$app->user->identity->chapter->desa->kd_kabupaten; 
                $data['kd_kecamatan'] = Yii::$app->user->identity->chapter->desa->Kd_Kec;
                $data['kd_desa'] = Yii::$app->user->identity->chapter->Kd_Desa;
                $data['balance'] = 'tambah';

                $model->updatingDesaCount($data);
                if($model->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('desa-form', [
                    'model' => $model,
                    'id' => $id,
                    'tahun' => $tahun
                ]);
            }
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Tambah '.$this->message
            ]);
        }
    }

    public function actionDesaUpdate($id, $kd, $tahun, $kur)
    {
        if($this->check('desa-update')){
            $model = $this->findModel($kd, $tahun, $kur);

            if ($model->load(Yii::$app->request->post())){
                if($model->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('desa-form', [//desa from can use for update too
                    'model' => $model,
                    'id' => $id,
                    'tahun' => $tahun
                ]);
            }
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Update '.$this->message
            ]);
        }
    }

    public function actionDesaDelete($id, $kd, $tahun, $kur)
    {
        if($this->check('desa-delete')){
            $model = $this->findModel($kd, $tahun, $kur);
            $data['tahun'] = $tahun;
            $data['kd_desa'] = Yii::$app->user->identity->chapter->Kd_Desa;
            $data['balance'] = 'kurang';

            $model->updatingDesaCount($data);

            if($model->delete()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Hapus '.$this->message
            ]);
        }
    }

    public function actionDesaView($id, $kd, $tahun, $kur)
    {
        if($this->check('desa-view')){
            return $this->renderAjax('desa-view', [
                'model' => $this->findModel($kd, $tahun, $kur),
                'id' => $id,
                'tahun' => $tahun
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Lihat Detail '.$this->message
            ]);
        }
    }




    public function actionIndex()
    {
        if($this->check('index')){
            $dataProvider = new ActiveDataProvider([
                'query' => TaAnggaran::find(),
            ]);

            return $this->renderAjax('index', [
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Index '.$this->message
            ]);
        }
    }

    /**
     * Displays a single TaAnggaran model.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $KURincianSD
     * @return mixed
     */
    public function actionView($KdPosting, $Tahun, $KURincianSD)
    {
        if($this->check('view')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($KdPosting, $Tahun, $KURincianSD),
            ]);
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Lihat Detail '.$this->message
            ]);
        }
    }

    /**
     * Creates a new TaAnggaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if($this->check('create')){
            $model = new TaAnggaran();

            if ($model->load(Yii::$app->request->post())){
                if($model->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Tambah '.$this->message
            ]);
        }
    }

    /**
     * Updates an existing TaAnggaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $KURincianSD
     * @return mixed
     */
    public function actionUpdate($KdPosting, $Tahun, $KURincianSD)
    {
        if($this->check('update')){
            $model = $this->findModel($KdPosting, $Tahun, $KURincianSD);

            if ($model->load(Yii::$app->request->post())){
                if($model->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Update '.$this->message
            ]);
        }
    }

    /**
     * Deletes an existing TaAnggaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $KURincianSD
     * @return mixed
     */
    public function actionDelete($KdPosting, $Tahun, $KURincianSD)
    {
        if($this->check('delete')){
            if($this->findModel($id)->delete()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('@sp3d/modules/referensi/views/default/prohibited',[
                'message' => 'Hapus '.$this->message
            ]);
        }
    }

    /**
     * Finds the TaAnggaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $KURincianSD
     * @return TaAnggaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($KdPosting, $Tahun, $KURincianSD)
    {
        if (($model = TaAnggaran::findOne(['KdPosting' => $KdPosting, 'Tahun' => $Tahun, 'KURincianSD' => $KURincianSD])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
