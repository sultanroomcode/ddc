<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaRABSub;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabSubController implements the CRUD actions for TaRABSub model.
 */
class TaRabSubController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun)
    {
        $dataProvider = TaRPJMBidang::find();

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaDelete($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }


    

    /**
     * Lists all TaRABSub models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaRABSub::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaRABSub model.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci),
        ]);
    }

    /**
     * Creates a new TaRABSub model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaRABSub();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaRABSub model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci)
    {
        $model = $this->findModel($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaRABSub model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaRABSub model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @return TaRABSub the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci)
    {
        if (($model = TaRABSub::findOne(['Tahun' => $Tahun, 'Kd_Desa' => $Kd_Desa, 'Kd_Keg' => $Kd_Keg, 'Kd_Rincian' => $Kd_Rincian, 'Kd_SubRinci' => $Kd_SubRinci])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
