<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaAnggaranLog;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaAnggaranLogController implements the CRUD actions for TaAnggaranLog model.
 */
class TaAnggaranLogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun)
    {
        $dataProvider = TaAnggaranLog::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaAnggaranLog();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun)
    {
        $model = new TaAnggaranLog();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun)
    {
        $model = new TaAnggaranLog();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaDelete($id, $tahun)
    {
        $model = new TaAnggaranLog();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }


    

    /**
     * Lists all TaAnggaranLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaAnggaranLog::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaAnggaranLog model.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return mixed
     */
    public function actionView($KdPosting, $Tahun, $Kd_Desa)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($KdPosting, $Tahun, $Kd_Desa),
        ]);
    }

    /**
     * Creates a new TaAnggaranLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaAnggaranLog();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaAnggaranLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return mixed
     */
    public function actionUpdate($KdPosting, $Tahun, $Kd_Desa)
    {
        $model = $this->findModel($KdPosting, $Tahun, $Kd_Desa);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaAnggaranLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return mixed
     */
    public function actionDelete($KdPosting, $Tahun, $Kd_Desa)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaAnggaranLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @return TaAnggaranLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($KdPosting, $Tahun, $Kd_Desa)
    {
        if (($model = TaAnggaranLog::findOne(['KdPosting' => $KdPosting, 'Tahun' => $Tahun, 'Kd_Desa' => $Kd_Desa])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
