<?php

namespace frontend\modules\transaksi\controllers;

use Yii;
use frontend\models\transaksi\TaDanaDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaDanaDesaController implements the CRUD actions for TaDanaDesa model.
 */
class TaDanaDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaDanaDesa::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'effect' => $effect,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaCreate($id, $tahun, $effect)
    {
        $model = new TaDanaDesa();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'effect' => $effect,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $effect)
    {
        $model = TaDanaDesa::findOne(['Kd_Desa' => $id, 'Tahun' => $tahun, 'Kd_Bid' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'effect' => $effect,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaDanaDesa::findOne(['Kd_Desa' => $id, 'Tahun' => $tahun, 'Kd_Bid' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'effect' => $effect,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        $model = TaDanaDesa::findOne(['Kd_Desa' => $id, 'Tahun' => $tahun, 'Kd_Bid' => $kd]);

        if($model->delete()) echo 1; else echo 0;
    }

    /**
     * Lists all TaDanaDesa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaDanaDesa::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaDanaDesa model.
     * @param string $kd_desa
     * @param string $kd_rincian
     * @return mixed
     */
    public function actionView($kd_desa, $kd_rincian)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_desa, $kd_rincian),
        ]);
    }

    /**
     * Creates a new TaDanaDesa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaDanaDesa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'kd_rincian' => $model->kd_rincian]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaDanaDesa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param string $kd_rincian
     * @return mixed
     */
    public function actionUpdate($kd_desa, $kd_rincian)
    {
        $model = $this->findModel($kd_desa, $kd_rincian);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'kd_rincian' => $model->kd_rincian]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaDanaDesa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param string $kd_rincian
     * @return mixed
     */
    public function actionDelete($kd_desa, $kd_rincian)
    {
        $this->findModel($kd_desa, $kd_rincian)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaDanaDesa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param string $kd_rincian
     * @return TaDanaDesa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $kd_rincian)
    {
        if (($model = TaDanaDesa::findOne(['kd_desa' => $kd_desa, 'kd_rincian' => $kd_rincian])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
