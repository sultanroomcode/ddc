<?php

namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\PasardesaData;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PasarDesaDataController implements the CRUD actions for PasardesaData model.
 */
class PasarDesaDataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PasardesaData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = PasardesaData::find();

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PasardesaData model.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @param integer $kd_isian
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_pasar, $kd_isian)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_pasar, $kd_isian),
        ]);
    }

    /**
     * Creates a new PasardesaData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id_pasar)
    {
        $model = new PasardesaData();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
	        $data = ['kd_desa' => $kd_desa, 'id_pasar' => $id_pasar];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
	    }
    }

    /**
     * Updates an existing PasardesaData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @param integer $kd_isian
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_pasar, $kd_isian)
    {
        $model = $this->findModel($kd_desa, $id_pasar, $kd_isian);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_pasar' => $id_pasar, 'kd_isian' => $kd_isian];
	        return $this->renderAjax('update', [
	            'model' => $model,
                'data' => $data
	        ]);
	    }
    }

    /**
     * Deletes an existing PasardesaData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @param integer $kd_isian
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_pasar, $kd_isian)
    {
        $model = $this->findModel($kd_desa, $id_pasar, $kd_isian)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the PasardesaData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_pasar
     * @param integer $kd_isian
     * @return PasardesaData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_pasar, $kd_isian)
    {
        if (($model = PasardesaData::findOne(['kd_desa' => $kd_desa, 'id_pasar' => $id_pasar, 'kd_isian' => $kd_isian])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
