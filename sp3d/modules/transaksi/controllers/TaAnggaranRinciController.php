<?php

namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaAnggaranRinci;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaAnggaranRinciController implements the CRUD actions for TaAnggaranRinci model.
 */
class TaAnggaranRinciController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun)
    {
        $dataProvider = TaRPJMBidang::find();

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaDelete($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }


    

    /**
     * Lists all TaAnggaranRinci models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaAnggaranRinci::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaAnggaranRinci model.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return mixed
     */
    public function actionView($KdPosting, $Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($KdPosting, $Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut),
        ]);
    }

    /**
     * Creates a new TaAnggaranRinci model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaAnggaranRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaAnggaranRinci model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return mixed
     */
    public function actionUpdate($KdPosting, $Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        $model = $this->findModel($KdPosting, $Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaAnggaranRinci model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return mixed
     */
    public function actionDelete($KdPosting, $Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaAnggaranRinci model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $KdPosting
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $Kd_SubRinci
     * @param string $No_Urut
     * @return TaAnggaranRinci the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($KdPosting, $Tahun, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $Kd_SubRinci, $No_Urut)
    {
        if (($model = TaAnggaranRinci::findOne(['KdPosting' => $KdPosting, 'Tahun' => $Tahun, 'Kd_Desa' => $Kd_Desa, 'Kd_Keg' => $Kd_Keg, 'Kd_Rincian' => $Kd_Rincian, 'Kd_SubRinci' => $Kd_SubRinci, 'No_Urut' => $No_Urut])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
