<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dPerangkatDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabRinciController implements the CRUD actions for TaRABRinci model.
 */
class TaBpdController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionListReportBpd()
    {
        return $this->renderAjax('list-report-bpd');
    }

    public function actionDataBpd() 
    {
        $dataProvider = Sp3dPerangkatDesa::find()->where(['tipe' => 'bpd', 'user' => Yii::$app->user->identity->id])->andWhere(['!=', 'jabatan', 'kepala-desa']);

        return $this->renderAjax('data-bpd', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionViewBpd($nik, $user)//termasuk kepala desa
    {
        $dataProvider = Sp3dPerangkatDesa::findOne(['nik' => $nik, 'user' => $user]);

        return $this->renderAjax('view-bpd', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionFormBpd() 
    {
        $model = new Sp3dPerangkatDesa();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-bpd', [
                'model' => $model,
                'show_jabatan_list' => true,
            ]);
        }
    }

    public function actionUpdateBpd($nik, $user) 
    {
        $model = Sp3dPerangkatDesa::findOne(['nik' => $nik, 'user' => $user]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-bpd', [
                'model' => $model,
                'show_jabatan_list' => true,
            ]);
        }
    }

    public function actionDeleteBpd($nik, $user, $jabatan='perangkat') 
    {
        $dataProvider = Sp3dPerangkatDesa::findOne(['nik' => $nik, 'user' => $user]);
        if($dataProvider->delete()){
            echo "<script>$.alert('Data {$nik} sudah dihapus');goLoad({url: '/transaksi/ta-perangkat-desa/data-".$jabatan."-desa'});</script>";
        } else {
            echo "<script>$.alert('Gagal hapus data {$nik}');goLoad({url: '/transaksi/ta-perangkat-desa/data-".$jabatan."-desa'});</script>";
        }
    }
}
