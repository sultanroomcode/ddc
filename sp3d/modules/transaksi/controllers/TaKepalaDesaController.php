<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dKepalaDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabRinciController implements the CRUD actions for TaRABRinci model.
 */
class TaKepalaDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionData($id, $tahun)
    {
        $dataProvider = Sp3dKepalaDesa::find()->where(['user' => Yii::$app->user->identity->id]);

        return $this->renderAjax('list-kepala-desa', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionListKepalaDesa() 
    {
        $dataProvider = Sp3dKepalaDesa::find()->where(['user' => Yii::$app->user->identity->id]);

        return $this->renderAjax('list-kepala-desa', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionViewKepalaDesa($nik, $user) 
    {
        $dataProvider = Sp3dKepalaDesa::findOne(['nik' => $nik, 'user' => $user]);

        return $this->renderAjax('view-kepala-desa', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionFormKepalaDesa() 
    {
        $model = new Sp3dKepalaDesa();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-kepala-desa', [
                'model' => $model
            ]);
        }
    }

    public function actionUpdateKepalaDesa($nik, $user) 
    {
        $model = Sp3dKepalaDesa::findOne(['nik' => $nik, 'user' => $user]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-kepala-desa', [
                'model' => $model
            ]);
        }
    }

    public function actionDeleteKepalaDesa($nik, $user) 
    {
        $dataProvider = Sp3dKepalaDesa::findOne(['nik' => $nik, 'user' => $user]);
        if($dataProvider->delete()){
            echo "<script>$.alert('Data {$nik} sudah dihapus');goLoad({url: '/transaksi/ta-kepala-desa/list-kepala-desa'});</script>";
        } else {
            echo "<script>$.alert('Gagal hapus data {$nik}');goLoad({url: '/transaksi/ta-kepala-desa/list-kepala-desa'});</script>";
        }
    }
}
