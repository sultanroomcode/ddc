<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaSTS;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaStsController implements the CRUD actions for TaSTS model.
 */
class TaStsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaSTS::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaSTS();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaSTS::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'No_Bukti' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaSTS::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'No_Bukti' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        if(TaSTS::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'No_Bukti' => $kd])->delete()) echo 1; else echo 0;
    }


    



    /**
     * Lists all TaSTS models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaSTS::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaSTS model.
     * @param string $Tahun
     * @param string $No_Bukti
     * @return mixed
     */
    public function actionView($Tahun, $No_Bukti)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Tahun, $No_Bukti),
        ]);
    }

    /**
     * Creates a new TaSTS model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaSTS();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaSTS model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $No_Bukti
     * @return mixed
     */
    public function actionUpdate($Tahun, $No_Bukti)
    {
        $model = $this->findModel($Tahun, $No_Bukti);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaSTS model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $No_Bukti
     * @return mixed
     */
    public function actionDelete($Tahun, $No_Bukti)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaSTS model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $No_Bukti
     * @return TaSTS the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $No_Bukti)
    {
        if (($model = TaSTS::findOne(['Tahun' => $Tahun, 'No_Bukti' => $No_Bukti])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
