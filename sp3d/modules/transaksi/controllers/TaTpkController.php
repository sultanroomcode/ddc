<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaTpk;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaTpkController implements the CRUD actions for TaTpk model.
 */
class TaTpkController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaTpk::find()->where(['Kd_Desa' => $id]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaTpk();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $no_urut, $tahun)
    {
        $model = $this->findModel($id, $no_urut);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id,$no_urut, $tahun)
    {
        $model = TaTpk::findOne(['kd_desa' => $id,'no_urut' => $no_urut]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $no_urut)
    {
        if(TaTpk::findOne(['kd_desa' => $id, 'no_urut' => $no_urut])->delete()) echo 1; else echo 0;
    }

    /**
     * Lists all TaTpk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaTpk::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaTpk model.
     * @param string $kd_desa
     * @param string $no_urut
     * @return mixed
     */
    public function actionView($kd_desa, $no_urut)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_desa, $no_urut),
        ]);
    }

    /**
     * Creates a new TaTpk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaTpk();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'no_urut' => $model->no_urut]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaTpk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param string $no_urut
     * @return mixed
     */
    public function actionUpdate($kd_desa, $no_urut)
    {
        $model = $this->findModel($kd_desa, $no_urut);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'no_urut' => $model->no_urut]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaTpk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param string $no_urut
     * @return mixed
     */
    public function actionDelete($kd_desa, $no_urut)
    {
        $this->findModel($kd_desa, $no_urut)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaTpk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param string $no_urut
     * @return TaTpk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $no_urut)
    {
        if (($model = TaTpk::findOne(['kd_desa' => $kd_desa, 'no_urut' => $no_urut])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
