<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaTBPRinci;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaTbpRinciController implements the CRUD actions for TaTBPRinci model.
 */
class TaTbpRinciController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        if(TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd])->delete()) echo 1; else echo 0;
    }


    



    /**
     * Lists all TaTBPRinci models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaTBPRinci::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaTBPRinci model.
     * @param string $No_Bukti
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $RincianSD
     * @return mixed
     */
    public function actionView($No_Bukti, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $RincianSD)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($No_Bukti, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $RincianSD),
        ]);
    }

    /**
     * Creates a new TaTBPRinci model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaTBPRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaTBPRinci model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $No_Bukti
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $RincianSD
     * @return mixed
     */
    public function actionUpdate($No_Bukti, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $RincianSD)
    {
        $model = $this->findModel($No_Bukti, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $RincianSD);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaTBPRinci model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $No_Bukti
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $RincianSD
     * @return mixed
     */
    public function actionDelete($No_Bukti, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $RincianSD)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaTBPRinci model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $No_Bukti
     * @param string $Kd_Desa
     * @param string $Kd_Keg
     * @param string $Kd_Rincian
     * @param string $RincianSD
     * @return TaTBPRinci the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($No_Bukti, $Kd_Desa, $Kd_Keg, $Kd_Rincian, $RincianSD)
    {
        if (($model = TaTBPRinci::findOne(['No_Bukti' => $No_Bukti, 'Kd_Desa' => $Kd_Desa, 'Kd_Keg' => $Kd_Keg, 'Kd_Rincian' => $Kd_Rincian, 'RincianSD' => $RincianSD])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
