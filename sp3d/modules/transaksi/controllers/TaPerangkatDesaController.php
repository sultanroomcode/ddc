<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dPerangkatDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaRabRinciController implements the CRUD actions for TaRABRinci model.
 */
class TaPerangkatDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    //digunakan untuk selain desa
    public function actionListReportKepalaDesa()
    {
        return $this->renderAjax('list-report-kepala-desa');
    }

    public function actionListReport()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('list-report', [
            'data' => $data
        ]);
    }

    public function actionChartReport()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('chart-report', [
            'data' => $data
        ]);
    }

    public function actionDataKepalaDesa()
    {
        $dataProvider = Sp3dPerangkatDesa::find()->where(['jabatan' => 'kepala-desa', 'user' => Yii::$app->user->identity->id]);

        return $this->renderAjax('data-kepala-desa', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionListReportPerangkatDesa()
    {
        return $this->renderAjax('list-report-perangkat-desa');
    }

    public function actionDataPerangkatDesaBaru()
    {
        $dataProvider = Sp3dPerangkatDesa::find();
        $dataProvider->where(['jabatan', '!=', 'kepala-desa']);
        $dataProvider->andWhere(['user' => Yii::$app->user->identity->id]);

        return $this->renderAjax('data-kepala-desa', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDataPerangkatDesa() 
    {
        $dataProvider = Sp3dPerangkatDesa::find()->where(['tipe' => 'desa', 'user' => Yii::$app->user->identity->id])->andWhere(['!=', 'jabatan', 'kepala-desa']);

        return $this->renderAjax('data-perangkat-desa', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionViewPerangkatDesa($nik, $user)//termasuk kepala desa
    {
        $dataProvider = Sp3dPerangkatDesa::findOne(['nik' => $nik, 'user' => $user]);

        return $this->renderAjax('view-perangkat-desa', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionFormKepalaDesa() 
    {
        $model = new Sp3dPerangkatDesa();
        if ($model->load(Yii::$app->request->post())) {
            $model->jabatan = 'kepala-desa';
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-perangkat-desa', [
                'model' => $model,
                'show_jabatan_list' => false,
            ]);
        }
    }

    public function actionFormPerangkatDesa() 
    {
        $model = new Sp3dPerangkatDesa();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-perangkat-desa', [
                'model' => $model,
                'show_jabatan_list' => true,
            ]);
        }
    }

    public function actionUpdateKepalaDesa($nik, $user) 
    {
        $model = Sp3dPerangkatDesa::findOne(['nik' => $nik, 'user' => $user]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-perangkat-desa', [
                'model' => $model,
                'show_jabatan_list' => false,
            ]);
        }
    }

    public function actionUpdatePerangkatDesa($nik, $user) 
    {
        $model = Sp3dPerangkatDesa::findOne(['nik' => $nik, 'user' => $user]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('form-perangkat-desa', [
                'model' => $model,
                'show_jabatan_list' => true,
            ]);
        }
    }

    public function actionDeletePerangkatDesa($nik, $user, $jabatan='perangkat') 
    {
        $dataProvider = Sp3dPerangkatDesa::findOne(['nik' => $nik, 'user' => $user]);
        if($dataProvider->delete()){
            echo "<script>$.alert('Data {$nik} sudah dihapus');goLoad({url: '/transaksi/ta-perangkat-desa/data-".$jabatan."-desa'});</script>";
        } else {
            echo "<script>$.alert('Gagal hapus data {$nik}');goLoad({url: '/transaksi/ta-perangkat-desa/data-".$jabatan."-desa'});</script>";
        }
    }
}
