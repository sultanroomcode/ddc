<?php
namespace frontend\modules\transaksi\controllers;

use Yii;
use frontend\models\transaksi\TaKegiatanSub;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaKegiatanSubController implements the CRUD actions for TaKegiatanSub model.
 */
class TaKegiatanSubController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionKegiatanDesa($id, $tahun, $kd, $effect=null)
    {
        $dataProvider = TaKegiatanSub::find()->where(['Kd_Desa' => $id, 'Kd_Keg' => $kd, 'Tahun' => $tahun]);

        return $this->renderAjax('kegiatan-desa', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'kd' => $kd,
            'effect' => $effect
        ]);
    }

    public function actionDesaIndex($id, $tahun, $effect=null)
    {
        $dataProvider = TaKegiatanSub::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun, $kd)
    {
        $model = new TaKegiatanSub();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun,
                'kd' => $kd
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd, $no)
    {
        $model =  TaKegiatanSub::findOne(['Tahun' => $tahun, 'Kd_Keg' => $kd, 'No_Keg' => $no]);
       
        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd, $no)
    {
        $model = TaKegiatanSub::findOne(['Tahun' => $tahun, 'Kd_Keg' => $kd, 'No_Keg' => $no]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($no, $tahun, $kd)
    {
        $model = TaKegiatanSub::findOne(['No_Keg' => $no,'Tahun' => $tahun, 'Kd_Keg' => $kd]);
        if($model->delete()) echo 1; else echo 0;
    }

    /**
     * Lists all TaKegiatanSub models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaKegiatanSub::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaKegiatanSub model.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @param string $No_Keg
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Keg, $No_Keg)
    {
        return $this->render('view', [
            'model' => $this->findModel($Tahun, $Kd_Keg, $No_Keg),
        ]);
    }

    /**
     * Creates a new TaKegiatanSub model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaKegiatanSub();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg, 'No_Keg' => $model->No_Keg]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaKegiatanSub model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @param string $No_Keg
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Keg, $No_Keg)
    {
        $model = $this->findModel($Tahun, $Kd_Keg, $No_Keg);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg, 'No_Keg' => $model->No_Keg]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaKegiatanSub model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @param string $No_Keg
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Keg, $No_Keg)
    {
        $this->findModel($Tahun, $Kd_Keg, $No_Keg)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaKegiatanSub model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Keg
     * @param string $No_Keg
     * @return TaKegiatanSub the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Keg, $No_Keg)
    {
        if (($model = TaKegiatanSub::findOne(['Tahun' => $Tahun, 'Kd_Keg' => $Kd_Keg, 'No_Keg' => $No_Keg])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
