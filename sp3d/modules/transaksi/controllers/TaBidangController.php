<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaBidang;
use sp3d\models\transaksi\TaKegiatan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaBidangController implements the CRUD actions for TaBidang model.
 */
class TaBidangController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $runback=false, $url='')//runback untuk meload kembali 
    {
        $dataProvider = TaBidang::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'runback' => $runback,
            'url' => $url,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaBidang();

        if ($model->load(Yii::$app->request->post())){
            $model->Kd_Bid = $model->Kd_Desa.$model->Kd_Bid;
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaBidang::findOne(['Kd_Desa' => $id, 'Tahun' => $tahun, 'Kd_Bid' => $id.$kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaViewAll($id, $tahun)
    {
        $model = TaKegiatan::find()->where(['user_id' => $id, 'Tahun' => $tahun, 'f_verified' => 1]);

        return $this->renderAjax('desa-view-all', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaViewAuto($id, $tahun, $kd)
    {
        $model = TaBidang::findOne(['Kd_Desa' => $id, 'Tahun' => $tahun, 'Kd_Bid' => $id.$kd]);

        return $this->renderAjax('desa-view-auto', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        $model = TaBidang::findOne(['Kd_Desa' => $id, 'Tahun' => $tahun, 'Kd_Bid' => $kd]);

        if($model->delete()) echo 1; else echo 0;
    }


    /**
     * Lists all TaBidang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaBidang::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaBidang model.
     * @param string $Tahun
     * @param string $Kd_Bid
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Bid)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Tahun, $Kd_Bid),
        ]);
    }

    /**
     * Creates a new TaBidang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaBidang();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaBidang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $Kd_Bid
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Bid)
    {
        $model = $this->findModel($Tahun, $Kd_Bid);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaBidang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $Kd_Bid
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Bid)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaBidang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Bid
     * @return TaBidang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Bid)
    {
        if (($model = TaBidang::findOne(['Tahun' => $Tahun, 'Kd_Bid' => $Kd_Bid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
