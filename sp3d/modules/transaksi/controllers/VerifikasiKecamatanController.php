<?php

namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dVerifikasiKec;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VerifikasiKecamatanController implements the CRUD actions for Sp3dVerifikasiKec model.
 */
class VerifikasiKecamatanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sp3dVerifikasiKec models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sp3dVerifikasiKec::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sp3dVerifikasiKec model.
     * @param string $id
     * @param string $nik
     * @param string $user
     * @return mixed
     */
    public function actionView($id, $nik, $user)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $nik, $user),
        ]);
    }

    /**
     * Creates a new Sp3dVerifikasiKec model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sp3dVerifikasiKec();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'nik' => $model->nik, 'user' => $model->user]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Sp3dVerifikasiKec model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $nik
     * @param string $user
     * @return mixed
     */
    public function actionUpdate($id, $nik, $user)
    {
        $model = $this->findModel($id, $nik, $user);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'nik' => $model->nik, 'user' => $model->user]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sp3dVerifikasiKec model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $nik
     * @param string $user
     * @return mixed
     */
    public function actionDelete($id, $nik, $user)
    {
        $this->findModel($id, $nik, $user)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sp3dVerifikasiKec model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $nik
     * @param string $user
     * @return Sp3dVerifikasiKec the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $nik, $user)
    {
        if (($model = Sp3dVerifikasiKec::findOne(['id' => $id, 'nik' => $nik, 'user' => $user])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
