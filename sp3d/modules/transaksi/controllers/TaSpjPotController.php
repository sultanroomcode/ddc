<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaSPJPot;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaSpjPotiController implements the CRUD actions for TaSPJPot model.
 */
class TaSpjPotController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        if(TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd])->delete()) echo 1; else echo 0;
    }


    



    /**
     * Lists all TaSPJPot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaSPJPot::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaSPJPot model.
     * @param string $No_SPJ
     * @param string $Kd_Keg
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionView($No_SPJ, $Kd_Keg, $No_Bukti, $Kd_Rincian)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($No_SPJ, $Kd_Keg, $No_Bukti, $Kd_Rincian),
        ]);
    }

    /**
     * Creates a new TaSPJPot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaSPJPot();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaSPJPot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $No_SPJ
     * @param string $Kd_Keg
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionUpdate($No_SPJ, $Kd_Keg, $No_Bukti, $Kd_Rincian)
    {
        $model = $this->findModel($No_SPJ, $Kd_Keg, $No_Bukti, $Kd_Rincian);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaSPJPot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $No_SPJ
     * @param string $Kd_Keg
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionDelete($No_SPJ, $Kd_Keg, $No_Bukti, $Kd_Rincian)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaSPJPot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $No_SPJ
     * @param string $Kd_Keg
     * @param string $No_Bukti
     * @param string $Kd_Rincian
     * @return TaSPJPot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($No_SPJ, $Kd_Keg, $No_Bukti, $Kd_Rincian)
    {
        if (($model = TaSPJPot::findOne(['No_SPJ' => $No_SPJ, 'Kd_Keg' => $Kd_Keg, 'No_Bukti' => $No_Bukti, 'Kd_Rincian' => $Kd_Rincian])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
