<?php

namespace frontend\modules\transaksi\controllers;

use Yii;
use frontend\models\transaksi\TaJurnalUmumRinci;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaJurnalUmumRinciController implements the CRUD actions for TaJurnalUmumRinci model.
 */
class TaJurnalUmumRinciController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun)
    {
        $dataProvider = TaRPJMBidang::find();

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaDelete($id, $tahun)
    {
        $model = new TaAnggaran();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }


    

    /**
     * Lists all TaJurnalUmumRinci models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaJurnalUmumRinci::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaJurnalUmumRinci model.
     * @param string $NoBukti
     * @param string $Kd_Keg
     * @param string $RincianSD
     * @param string $NoID
     * @return mixed
     */
    public function actionView($NoBukti, $Kd_Keg, $RincianSD, $NoID)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($NoBukti, $Kd_Keg, $RincianSD, $NoID),
        ]);
    }

    /**
     * Creates a new TaJurnalUmumRinci model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaJurnalUmumRinci();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaJurnalUmumRinci model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $NoBukti
     * @param string $Kd_Keg
     * @param string $RincianSD
     * @param string $NoID
     * @return mixed
     */
    public function actionUpdate($NoBukti, $Kd_Keg, $RincianSD, $NoID)
    {
        $model = $this->findModel($NoBukti, $Kd_Keg, $RincianSD, $NoID);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaJurnalUmumRinci model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $NoBukti
     * @param string $Kd_Keg
     * @param string $RincianSD
     * @param string $NoID
     * @return mixed
     */
    public function actionDelete($NoBukti, $Kd_Keg, $RincianSD, $NoID)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaJurnalUmumRinci model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $NoBukti
     * @param string $Kd_Keg
     * @param string $RincianSD
     * @param string $NoID
     * @return TaJurnalUmumRinci the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($NoBukti, $Kd_Keg, $RincianSD, $NoID)
    {
        if (($model = TaJurnalUmumRinci::findOne(['NoBukti' => $NoBukti, 'Kd_Keg' => $Kd_Keg, 'RincianSD' => $RincianSD, 'NoID' => $NoID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
