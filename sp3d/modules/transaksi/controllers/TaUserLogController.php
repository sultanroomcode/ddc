<?php
namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\TaUserLog;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaUserLogController implements the CRUD actions for TaUserLog model.
 */
class TaUserLogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDesaIndex($id, $tahun, $effect)
    {
        $dataProvider = TaKegiatan::find()->where(['Kd_Desa' => $id,'Tahun' => $tahun]);

        return $this->renderAjax('desa-index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'tahun' => $tahun,
            'effect' => $effect
        ]);
    }

    public function actionDesaCreate($id, $tahun)
    {
        $model = new TaKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaUpdate($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('desa-form', [
                'model' => $model,
                'id' => $id,
                'tahun' => $tahun
            ]);
        }
    }

    public function actionDesaView($id, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd]);

        return $this->renderAjax('desa-view', [
            'model' => $model,
            'id' => $id,
            'tahun' => $tahun
        ]);
    }

    public function actionDesaDelete($id, $tahun, $kd)
    {
        if(TaKegiatan::findOne(['Kd_Desa' => $id,'Tahun' => $tahun, 'Kd_Keg' => $kd])->delete()) echo 1; else echo 0;
    }


    



    /**
     * Lists all TaUserLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TaUserLog::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaUserLog model.
     * @param string $TglLogin
     * @param string $Komputer
     * @param string $UserID
     * @return mixed
     */
    public function actionView($TglLogin, $Komputer, $UserID)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($TglLogin, $Komputer, $UserID),
        ]);
    }

    /**
     * Creates a new TaUserLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaUserLog();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaUserLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $TglLogin
     * @param string $Komputer
     * @param string $UserID
     * @return mixed
     */
    public function actionUpdate($TglLogin, $Komputer, $UserID)
    {
        $model = $this->findModel($TglLogin, $Komputer, $UserID);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaUserLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $TglLogin
     * @param string $Komputer
     * @param string $UserID
     * @return mixed
     */
    public function actionDelete($TglLogin, $Komputer, $UserID)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the TaUserLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $TglLogin
     * @param string $Komputer
     * @param string $UserID
     * @return TaUserLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($TglLogin, $Komputer, $UserID)
    {
        if (($model = TaUserLog::findOne(['TglLogin' => $TglLogin, 'Komputer' => $Komputer, 'UserID' => $UserID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
