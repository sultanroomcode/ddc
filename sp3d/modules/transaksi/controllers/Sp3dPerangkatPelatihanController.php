<?php

namespace sp3d\modules\transaksi\controllers;

use Yii;
use sp3d\models\transaksi\Sp3dPerangkatPelatihan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Sp3dPerangkatPelatihanController implements the CRUD actions for Sp3dPerangkatPelatihan model.
 */
class Sp3dPerangkatPelatihanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sp3dPerangkatPelatihan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sp3dPerangkatPelatihan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewInBox()
    {
        $data = Yii::$app->request->get();
        $dataProvider = Sp3dPerangkatPelatihan::find()->where(['kd_desa' => $data['kd_desa'], 'nik' => $data['nik']]);
        $dataProvider->orderBy('tahun DESC');

        return $this->renderAjax('view-in-box', [
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single Sp3dPerangkatPelatihan model.
     * @param string $kd_desa
     * @param string $nik
     * @param integer $idp
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $nik, $idp)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $nik, $idp),
        ]);
    }

    /**
     * Creates a new Sp3dPerangkatPelatihan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateInBox()
    {
        $model = new Sp3dPerangkatPelatihan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'inbox' => ['status' => true, 'data' => Yii::$app->request->get()]
            ]);
        }
    }

    public function actionUpdateInBox($kd_desa, $nik, $idp)
    {
        $model = $this->findModel($kd_desa, $nik, $idp);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'inbox' => ['status' => true, 'data' => Yii::$app->request->get()]
            ]);
        }
    }

    public function actionCreate()
    {
        $model = new Sp3dPerangkatPelatihan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'inbox' => false
            ]);
        }
    }

    public function actionUpdate($kd_desa, $nik, $idp)
    {
        $model = $this->findModel($kd_desa, $nik, $idp);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'inbox' => false
            ]);
        }
    }

    /**
     * Deletes an existing Sp3dPerangkatPelatihan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param string $nik
     * @param integer $idp
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $nik, $idp)
    {
        $this->findModel($kd_desa, $nik, $idp)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sp3dPerangkatPelatihan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param string $nik
     * @param integer $idp
     * @return Sp3dPerangkatPelatihan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $nik, $idp)
    {
        if (($model = Sp3dPerangkatPelatihan::findOne(['kd_desa' => $kd_desa, 'nik' => $nik, 'idp' => $idp])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
