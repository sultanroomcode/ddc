<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPPBukti */

$this->title = $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Sppbuktis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-sppbukti-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_Bukti], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_Bukti], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-sppbukti/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Sppbukti</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-sppbukti/delete?id=<?= $model->id?>', urlBack:'ta-sppbukti/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Sppbukti</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'Kd_Keg',
            'Kd_Rincian',
            'Sumberdana',
            'No_Bukti',
            'Tgl_Bukti',
            'Nm_Penerima',
            'Alamat',
            'Rek_Bank',
            'Nm_Bank',
            'NPWP',
            'Keterangan',
            'Nilai',
        ],
    ]) ?>

</div>
