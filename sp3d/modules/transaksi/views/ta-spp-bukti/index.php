<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Sppbuktis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-sppbukti-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-sppbukti/create'})" class="btn btn-warning">Buat Ta Sppbukti</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'Kd_Keg',
            // 'Kd_Rincian',
            // 'Sumberdana',
            // 'No_Bukti',
            // 'Tgl_Bukti',
            // 'Nm_Penerima',
            // 'Alamat',
            // 'Rek_Bank',
            // 'Nm_Bank',
            // 'NPWP',
            // 'Keterangan',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
