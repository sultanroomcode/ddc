<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Pendamping Desa</h2>
            <div class="p-10">
            	<!-- <a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/sp3d-pendamping-desa/form-pendamping'})" class="show-pop btn btn-danger" title="Tambah Data Pendamping Desa" data-animation="pop" data-content="<p>Menambahkan Data Pendamping Desa.</p>"><i class="fa fa-user"></i></a> -->

				<div class="block-area" id="tableStriped">
				    <div class="table-responsive overflow">
				    	<table id="sp3d-pendamping" class="tile table table-bordered table-striped" cellspacing="0">
						    <thead>
						        <tr>
						            <th>NIK</th>
						            <th>Nama</th>
						            <th>Status Verifikasi</th>
						            <th>Aksi</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($dataProvider->all() as $v): ?>
						    	<tr>
						            <th><?= $v->nik ?></th>
						            <th><?= $v->pendamping->nama ?></th>
						            <th><?= ($v->verified_status == 0)?'belum terverifikasi':'terverifikasi<br>'.$v->verified_at ?></th>
						            <th>
						            	<a href="javascript:void(0)" onclick="openModalXyf({url:'/transaksi/sp3d-pendamping-desa/view-pendamping?nik=<?=$v->nik?>'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
						            	<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/sp3d-pendamping-desa/update-verifikasi-pendamping?nik=<?=$v->nik?>&user=<?=$v->kd_desa?>'})" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>
						            	<a href="javascript:void(0)" onclick="confirmationDelete('<?=$v->nik?>','<?=$v->kd_desa?>')" class="btn btn-sm btn-default"><i class="fa fa-trash"></i></a>
						           	</th>
						        </tr>
						    	<?php endforeach; ?>
						    </tbody>
						</table>
				    </div>
				</div><br>
				<b>Nb</b> : -
			</div>
		</div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
	$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
	$('#sp3d-pendamping').DataTable();

	function confirmationDelete(nik, user){
		$.confirm({
		    title: 'Hapus Kepala desa?',
		    content: 'Tindakan ini akan menghapus data pendamping desa dengan NIK '+nik,
		    autoClose: 'batal|10000',
		    buttons: {
		        deleteUser: {
		            text: 'hapus',
		            action: function () {
		                goLoad({url: '/transaksi/sp3d-pendamping-desa/delete-pendamping?nik='+nik+'&user='+user});
		            }
		        },
		        batal: function () {
		            $.alert('Tindakan dibatalkan');
		        }
		    }
		});
	}
JS;

$this->registerJs($scripts);