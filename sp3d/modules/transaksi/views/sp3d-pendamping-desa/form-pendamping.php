<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Form - Pendamping Desa</h2>
            <div class="p-10">
            	<?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'nik')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999999999999999'
                        ]) ?>
                        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'foto_profile_box')->fileInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'jenis_kelamin')->dropdownList(['L' => 'Laki-Laki', 'P' => 'Perempuan']) ?>
                        <?= $form->field($model, 'alamat')->textarea() ?>
                    </div>
                </div>		    

			    <div class="form-group">
			        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/sp3d-pendamping-desa/list-pendamping'})" class="show-pop btn btn-danger" title="Tambah Data Kepala Desa">Kembali</a>
			    </div>

			    <?php ActiveForm::end(); ?>
            </div>
		</div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
$('#sp3dpendampingdesa-tanggal_lahir, #sp3dpendampingdesa').datetimepicker({
    format:'Y-m-d',
    mask:true
});
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#{$model->formName()}')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : '/transaksi/sp3d-pendamping-desa/list-pendamping'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($scripts);