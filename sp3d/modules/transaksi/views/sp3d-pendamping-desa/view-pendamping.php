<?php
$data = $dataProvider;
?>
<div class="row">
	<div class="col-md-3"><img src="<?= $data->show_foto() ?>"></div>
	<div class="col-md-9">
		<table class="table">
			<tr>
				<td>Nama</td>
				<td><?= $data->nama ?></td>
			</tr>

			<tr>
				<td>Tempat dan Tanggal Lahir</td>
				<td><?= $data->tempat_lahir.', '. $data->tanggal_lahir_format() ?></td>
			</tr>

			<tr>
				<td>Jenis Kelamin</td>
				<td><?= ($data->jenis_kelamin == 'L')?'Laki-Laki':'Perempuan' ?></td>
			</tr>

			<tr>
				<td valign="top">Alamat</td>
				<td><?= $data->alamat ?></td>
			</tr>
		</table>
	</div>
</div>
