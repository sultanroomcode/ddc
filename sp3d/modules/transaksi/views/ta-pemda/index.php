<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Pemdas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-pemda-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-pemda/create'})" class="btn btn-warning">Buat Ta Pemda</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Prov',
            'Kd_Kab',
            'Nama_Pemda',
            'Nama_Provinsi',
            // 'Ibukota',
            // 'Alamat',
            // 'Nm_Bupati',
            // 'Jbt_Bupati',
            // 'Logo',
            // 'C_Kode',
            // 'C_Pemda',
            // 'C_Data',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
