<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPemda */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaPemda */
/* @var $generator string helper base name with camel to id Inflector ta-pemda */
/* @var $generator string helper base name with camel to word Inflector Ta Pemda */
?>

<div class="ta-pemda-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-pemda/index'})" class="btn btn-warning">List Ta Pemda</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Prov')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Kab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama_Pemda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama_Provinsi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ibukota')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nm_Bupati')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jbt_Bupati')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Logo')->textInput() ?>

    <?= $form->field($model, 'C_Kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'C_Pemda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'C_Data')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-pemda/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta Pemda')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-pemda/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);