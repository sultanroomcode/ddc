<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPemda */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pemdas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-pemda-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Tahun], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Tahun], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-pemda/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Pemda</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-pemda/delete?id=<?= $model->id?>', urlBack:'ta-pemda/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Pemda</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Prov',
            'Kd_Kab',
            'Nama_Pemda',
            'Nama_Provinsi',
            'Ibukota',
            'Alamat',
            'Nm_Bupati',
            'Jbt_Bupati',
            'Logo',
            'C_Kode',
            'C_Pemda',
            'C_Data',
        ],
    ]) ?>

</div>
