<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPemda */

$this->title = 'Update Ta Pemda: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pemdas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'id' => $model->Tahun]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-pemda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
