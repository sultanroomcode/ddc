<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta User Desas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-desa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-desa/create'})" class="btn btn-warning">Buat Ta User Desa</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'Kd_Kec',
            'Kd_Desa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
