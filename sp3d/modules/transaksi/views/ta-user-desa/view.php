<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserDesa */

$this->title = $model->Id;
$this->params['breadcrumbs'][] = ['label' => 'Ta User Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-desa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-desa/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta User Desa</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-user-desa/delete?id=<?= $model->id?>', urlBack:'ta-user-desa/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta User Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'Kd_Kec',
            'Kd_Desa',
        ],
    ]) ?>

</div>
