<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Perangkat</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-perangkat/perangkat-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-perangkat/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-desa/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Perangkat Desa</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-perangkat-table" class="display table" width="100%">
	    <thead>
	        <tr>
	            <th>Kades</th>
	            <th>Sekdes</th>
	            <th>Bendahara</th>
	            <th>Alamat</th>
	            <th>Status</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kades</th>
	            <th>Sekdes</th>
	            <th>Bendahara</th>
	            <th>Alamat</th>
	            <th>Status</th>
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <th><?= $v->Nm_Kades ?></th>
	            <th><?= $v->Nm_Sekdes ?></th>
	            <th><?= $v->Nm_Bendahara ?></th>
	            <th><?= $v->Alamat ?></th>
	            <th><?= $v->Status ?></th>
	            <th>
	            	<?php if(Mimin::checkRoute('transaksi/ta-desa/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Perangkat Desa?', urlSend:'/transaksi/ta-desa/desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>', urlBack:'/transaksi/ta-desa/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-desa/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-desa/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </th>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
/*'Tahun',
            'Kd_Desa',
            'Nm_Kades',
            'Jbt_Kades',
            'Nm_Sekdes',*/
$scripts =<<<JS
	$('#ta-perangkat-table').DataTable();
JS;

$this->registerJs($scripts);