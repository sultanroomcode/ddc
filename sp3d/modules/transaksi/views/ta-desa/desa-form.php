<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-bidang */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Nm_Kades = $model->Nm_Sekdes = $model->NIP_Sekdes = $model->Nm_Bendahara = $model->Nm_Kaur_Keu = '-';
    $model->Jbt_Kades = 'Kepala Desa';
    $model->Jbt_Sekdes = 'Sekretaris Desa';
    $model->Jbt_Bendahara = 'Bendahara Desa';
    $model->Jbt_Kaur_Keu = 'Kaur Keuangan Desa';
    $model->Kd_Desa = $id;
    $model->Status = 1;
    $idkec = substr($id, 0, 7);
    $model->kd_kecamatan = $idkec[0];
}
?>

<div class="ta-bidang-form animated slideInLeft">
    <?php if(!$model->isNewRecord){ ?>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#penganggaran-datadesa', url:'/transaksi/ta-desa/desa-view?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">Kembali Ke Data Umum Desa</a>
    <?php } ?>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Status')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'Nm_Kades')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Jbt_Kades')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Nm_Sekdes')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'NIP_Sekdes')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Jbt_Sekdes')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Nm_Bendahara')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Jbt_Bendahara')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Nm_Kaur_Keu')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Jbt_Kaur_Keu')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'No_Perdes')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Tgl_Perdes')->textInput() ?>
            <?= $form->field($model, 'No_Perdes_PB')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Tgl_Perdes_PB')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'No_Perdes_PJ')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Tgl_Perdes_PJ')->textInput() ?>
            <?= $form->field($model, 'Alamat')->textarea(['maxlength' => true]) ?>
            <?= $form->field($model, 'Ibukota')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'NPWP')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Isi' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
$('#tadesa-tgl_perdes, #tadesa-tgl_perdes_pb, #tadesa-tgl_perdes_pj').datetimepicker({
    format:'Y-m-d',
    mask:true
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#penganggaran-area',url : '/transaksi/ta-desa/desa-view?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);