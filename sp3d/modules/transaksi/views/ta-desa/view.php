<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaDesa */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-desa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-desa/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Desa</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-desa/delete?id=<?= $model->id?>', urlBack:'ta-desa/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'Nm_Kades',
            'Jbt_Kades',
            'Nm_Sekdes',
            'NIP_Sekdes',
            'Jbt_Sekdes',
            'Nm_Kaur_Keu',
            'Jbt_Kaur_Keu',
            'Nm_Bendahara',
            'Jbt_Bendahara',
            'No_Perdes',
            'Tgl_Perdes',
            'No_Perdes_PB',
            'Tgl_Perdes_PB',
            'No_Perdes_PJ',
            'Tgl_Perdes_PJ',
            'Alamat',
            'Ibukota',
            'Status',
            'NPWP',
        ],
    ]) ?>

</div>
