<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Desas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-desa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-desa/create'})" class="btn btn-warning">Buat Ta Desa</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'Nm_Kades',
            'Jbt_Kades',
            'Nm_Sekdes',
            // 'NIP_Sekdes',
            // 'Jbt_Sekdes',
            // 'Nm_Kaur_Keu',
            // 'Jbt_Kaur_Keu',
            // 'Nm_Bendahara',
            // 'Jbt_Bendahara',
            // 'No_Perdes',
            // 'Tgl_Perdes',
            // 'No_Perdes_PB',
            // 'Tgl_Perdes_PB',
            // 'No_Perdes_PJ',
            // 'Tgl_Perdes_PJ',
            // 'Alamat',
            // 'Ibukota',
            // 'Status',
            // 'NPWP',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
