<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaDesa */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaDesa */
/* @var $generator string helper base name with camel to id Inflector ta-desa */
/* @var $generator string helper base name with camel to word Inflector Ta Desa */
?>

<div class="ta-desa-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-desa/index'})" class="btn btn-warning">List Ta Desa</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nm_Kades')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jbt_Kades')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nm_Sekdes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NIP_Sekdes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jbt_Sekdes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nm_Kaur_Keu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jbt_Kaur_Keu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nm_Bendahara')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jbt_Bendahara')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_Perdes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tgl_Perdes')->textInput() ?>

    <?= $form->field($model, 'No_Perdes_PB')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tgl_Perdes_PB')->textInput() ?>

    <?= $form->field($model, 'No_Perdes_PJ')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tgl_Perdes_PJ')->textInput() ?>

    <?= $form->field($model, 'Alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ibukota')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NPWP')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-desa/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta Desa')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-desa/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);