<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-perangkat-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>            
        <?php if(is_null($model->Tahun)){ ?>

            <?php if(Mimin::checkRoute('transaksi/ta-perangkat/desa-create')): ?>
            <a href="javascript:void(0)" onclick="goLoad({elm:'#vtab-data-umum-desa', url:'/transaksi/ta-desa/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Data Umum Desa</a>
            <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#vtab-data-umum-desa', url:'/transaksi/ta-perangkat/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Perangkat</a>
         <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#vtab-data-umum-desa',msg:'Hapus Perangkat Desa?', urlSend:'/transaksi/ta-perangkat/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Desa?>&no=<?=$model->Kd_Desa?>', urlBack:'/transaksi/ta-perangkat/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus Perangkat</a> -->
            <?php endif; ?>
        <?php } else { ?>
            <a href="javascript:void(0)" onclick="goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-desa/desa-update?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Update Data</a>
            <a href="<?= Url::to(['/data-umum/pdf?bagian=data-desa&kd_desa='.$model->Kd_Desa], true) ?>" target="_blank" class="btn btn-sm btn-info">PDF</a>
        <?php } ?>
    </p>

    <div class="row">
        <div class="col-md-6 col-lg-4">
            <b>Tahun :</b><br><?= $model->Tahun ?><br>
            <b>Kode Desa :</b><br><?= $model->Kd_Desa ?><br>
            <b>Nama :</b><br><?= $model->Nm_Kades ?><br>
            <b>Jabatan :</b><br><?= $model->Jbt_Kades ?><br>
        </div>
        <div class="col-md-6 col-lg-4">
            <b>Nama :</b><br><?= $model->Nm_Sekdes ?><br>
            <b>NIP :</b><br><?= $model->NIP_Sekdes ?><br>
            <b>Jabatan :</b><br><?= $model->Jbt_Sekdes ?><br>
            
            <b>Nama :</b><br><?= $model->Nm_Kaur_Keu ?><br>
            <b>Jabatan :</b><br><?= $model->Jbt_Kaur_Keu ?><br>

            <b>Nama :</b><br><?= $model->Nm_Bendahara ?><br>
            <b>Bendahara :</b><br><?= $model->Jbt_Bendahara ?><br>
        </div>
        <div class="col-md-6 col-lg-4">
            <b>No. Perdes :</b><br><?= $model->No_Perdes ?><br>
            <b>Tanggal Perdes :</b><br><?= $model->Tgl_Perdes ?><br>

            <!-- <b>Tahun :</b><br><?= $model->No_Perdes_PB ?><br>
            <b>Tahun :</b><br><?= $model->Tgl_Perdes_PB ?><br>
            <b>Tahun :</b><br><?= $model->No_Perdes_PJ ?><br>
            <b>Tahun :</b><br><?= $model->Tgl_Perdes_PJ ?><br> -->

            <b>Alamat :</b><br><?= $model->Alamat ?><br>
            <b>Ibukota :</b><br><?= $model->Ibukota ?><br>
            <b>NPWP :</b><br><?= $model->NPWP ?><br>
        </div>
    </div>    
</div>
