<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaDesa */

$this->title = 'Update Ta Desa: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-desa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
