<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMMisi */

$this->title = 'Create Ta Rpjmmisi';
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmmisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmmisi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
