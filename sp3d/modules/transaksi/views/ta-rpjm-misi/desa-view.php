<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rpjm-misi-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-misi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RPJM Kegiatan Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RPJM Kegiatan Desa?', urlSend:'/transaksi/ta-rpjm-misi/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->ID_Misi?>', urlBack:'/transaksi/ta-rpjm-misi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RPJM Kegiatan Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Misi',
            'Kd_Desa',
            'ID_Visi',
            'No_Misi',
            'Uraian_Misi',
        ],
    ]) ?>

</div>
