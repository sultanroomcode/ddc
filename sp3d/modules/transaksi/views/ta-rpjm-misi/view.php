<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMMisi */

$this->title = $model->ID_Misi;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmmisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmmisi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_Misi], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_Misi], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmmisi/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmmisi</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmmisi/delete?id=<?= $model->id?>', urlBack:'ta-rpjmmisi/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmmisi</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Misi',
            'Kd_Desa',
            'ID_Visi',
            'No_Misi',
            'Uraian_Misi',
        ],
    ]) ?>

</div>
