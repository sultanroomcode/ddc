<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMMisi */

$this->title = 'Update Ta Rpjmmisi: ' . $model->ID_Misi;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmmisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_Misi, 'url' => ['view', 'id' => $model->ID_Misi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmmisi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
