<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmmisis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmmisi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmmisi/create'})" class="btn btn-warning">Buat Ta Rpjmmisi</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_Misi',
            'Kd_Desa',
            'ID_Visi',
            'No_Misi',
            'Uraian_Misi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
