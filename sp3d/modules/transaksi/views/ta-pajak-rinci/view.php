<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPajakRinci */

$this->title = $model->No_SSP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pajak Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-pajak-rinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_SSP' => $model->No_SSP, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_SSP' => $model->No_SSP, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-pajak-rinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Pajak Rinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-pajak-rinci/delete?id=<?= $model->id?>', urlBack:'ta-pajak-rinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Pajak Rinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'No_SSP',
            'No_Bukti',
            'Kd_Rincian',
            'Nilai',
        ],
    ]) ?>

</div>
