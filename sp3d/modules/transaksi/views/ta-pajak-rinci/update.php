<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPajakRinci */

$this->title = 'Update Ta Pajak Rinci: ' . $model->No_SSP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pajak Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SSP, 'url' => ['view', 'No_SSP' => $model->No_SSP, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-pajak-rinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
