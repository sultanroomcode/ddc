<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmpagu Tahunans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmpagu-tahunan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmpagu-tahunan/create'})" class="btn btn-warning">Buat Ta Rpjmpagu Tahunan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Desa',
            'Kd_Keg',
            'Kd_Tahun',
            'Kd_Sumber',
            'Biaya',
            // 'Volume',
            // 'Satuan',
            // 'Lokasi_Spesifik',
            // 'Jml_Sas_Pria',
            // 'Jml_Sas_Wanita',
            // 'Jml_Sas_ARTM',
            // 'Waktu',
            // 'Mulai',
            // 'Selesai',
            // 'Pola_Kegiatan',
            // 'Pelaksana',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
