<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rpjm-pagu-tahunan-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-pagu-tahunan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RPJM Pagu Tahunan Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RPJM Pagu Tahunan Desa?', urlSend:'/transaksi/ta-rpjm-pagu-tahunan/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Tahun?>&kd3=<?=$v->Kd_Sumber?>', urlBack:'/transaksi/ta-rpjm-pagu-tahunan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RPJM Pagu Tahunan Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Tahun',
            'Kd_Sumber',
            'Biaya',
            'Volume',
            'Satuan',
            'Lokasi_Spesifik',
            'Jml_Sas_Pria',
            'Jml_Sas_Wanita',
            'Jml_Sas_ARTM',
            'Waktu',
            'Mulai',
            'Selesai',
            'Pola_Kegiatan',
            'Pelaksana',
        ],
    ]) ?>

</div>
