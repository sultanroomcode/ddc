<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMPaguTahunan */

$this->title = 'Update Ta Rpjmpagu Tahunan: ' . $model->Kd_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmpagu Tahunans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Keg, 'url' => ['view', 'Kd_Keg' => $model->Kd_Keg, 'Kd_Tahun' => $model->Kd_Tahun, 'Kd_Sumber' => $model->Kd_Sumber]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmpagu-tahunan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
