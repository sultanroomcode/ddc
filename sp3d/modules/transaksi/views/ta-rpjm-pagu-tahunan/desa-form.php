<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
}
?>

<div class="ta-rpjm-pagu-tahunan-form animated slideInUp">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-pagu-tahunan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Pagu Tahunan RPJM</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Sumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Biaya')->textInput() ?>

    <?= $form->field($model, 'Volume')->textInput() ?>

    <?= $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lokasi_Spesifik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jml_Sas_Pria')->textInput() ?>

    <?= $form->field($model, 'Jml_Sas_Wanita')->textInput() ?>

    <?= $form->field($model, 'Jml_Sas_ARTM')->textInput() ?>

    <?= $form->field($model, 'Waktu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Mulai')->textInput() ?>

    <?= $form->field($model, 'Selesai')->textInput() ?>

    <?= $form->field($model, 'Pola_Kegiatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pelaksana')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-rpjm-pagu-tahunan/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);