<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMPaguTahunan */

$this->title = $model->Kd_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmpagu Tahunans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmpagu-tahunan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Kd_Keg' => $model->Kd_Keg, 'Kd_Tahun' => $model->Kd_Tahun, 'Kd_Sumber' => $model->Kd_Sumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Kd_Keg' => $model->Kd_Keg, 'Kd_Tahun' => $model->Kd_Tahun, 'Kd_Sumber' => $model->Kd_Sumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmpagu-tahunan/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmpagu Tahunan</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmpagu-tahunan/delete?id=<?= $model->id?>', urlBack:'ta-rpjmpagu-tahunan/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmpagu Tahunan</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Tahun',
            'Kd_Sumber',
            'Biaya',
            'Volume',
            'Satuan',
            'Lokasi_Spesifik',
            'Jml_Sas_Pria',
            'Jml_Sas_Wanita',
            'Jml_Sas_ARTM',
            'Waktu',
            'Mulai',
            'Selesai',
            'Pola_Kegiatan',
            'Pelaksana',
        ],
    ]) ?>

</div>
