<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMPaguTahunan */

$this->title = 'Create Ta Rpjmpagu Tahunan';
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmpagu Tahunans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmpagu-tahunan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
