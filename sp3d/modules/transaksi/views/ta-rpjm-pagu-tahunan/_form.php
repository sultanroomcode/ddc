<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMPaguTahunan */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaRPJMPaguTahunan */
/* @var $generator string helper base name with camel to id Inflector ta-rpjmpagu-tahunan */
/* @var $generator string helper base name with camel to word Inflector Ta Rpjmpagu Tahunan */
?>

<div class="ta-rpjmpagu-tahunan-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmpagu-tahunan/index'})" class="btn btn-warning">List Ta Rpjmpagu Tahunan</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Sumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Biaya')->textInput() ?>

    <?= $form->field($model, 'Volume')->textInput() ?>

    <?= $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lokasi_Spesifik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jml_Sas_Pria')->textInput() ?>

    <?= $form->field($model, 'Jml_Sas_Wanita')->textInput() ?>

    <?= $form->field($model, 'Jml_Sas_ARTM')->textInput() ?>

    <?= $form->field($model, 'Waktu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Mulai')->textInput() ?>

    <?= $form->field($model, 'Selesai')->textInput() ?>

    <?= $form->field($model, 'Pola_Kegiatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pelaksana')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-rpjmpagu-tahunan/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta Rpjmpagu Tahunan')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-rpjmpagu-tahunan/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);