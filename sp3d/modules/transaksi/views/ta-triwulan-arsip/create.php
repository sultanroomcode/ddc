<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTriwulanArsip */

$this->title = 'Create Ta Triwulan Arsip';
$this->params['breadcrumbs'][] = ['label' => 'Ta Triwulan Arsips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-triwulan-arsip-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
