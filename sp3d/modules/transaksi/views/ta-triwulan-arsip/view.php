<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTriwulanArsip */

$this->title = $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Triwulan Arsips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-triwulan-arsip-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'KdPosting' => $model->KdPosting, 'KURincianSD' => $model->KURincianSD, 'Tahun' => $model->Tahun], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'KdPosting' => $model->KdPosting, 'KURincianSD' => $model->KURincianSD, 'Tahun' => $model->Tahun], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-triwulan-arsip/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Triwulan Arsip</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-triwulan-arsip/delete?id=<?= $model->id?>', urlBack:'ta-triwulan-arsip/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Triwulan Arsip</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KdPosting',
            'KURincianSD',
            'Tahun',
            'Sifat',
            'SumberDana',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Keg',
            'Kd_Rincian',
            'Anggaran',
            'AnggaranPAK',
            'Tw1Rinci',
            'Tw2Rinci',
            'Tw3Rinci',
            'Tw4Rinci',
            'KunciData',
        ],
    ]) ?>

</div>
