<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTriwulanArsip */

$this->title = 'Update Ta Triwulan Arsip: ' . $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Triwulan Arsips', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->KdPosting, 'url' => ['view', 'KdPosting' => $model->KdPosting, 'KURincianSD' => $model->KURincianSD, 'Tahun' => $model->Tahun]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-triwulan-arsip-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
