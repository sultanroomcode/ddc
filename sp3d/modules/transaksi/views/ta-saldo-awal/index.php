<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Saldo Awals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-saldo-awal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-saldo-awal/create'})" class="btn btn-warning">Buat Ta Saldo Awal</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Rincian',
            'Jenis',
            // 'Anggaran',
            // 'Debet',
            // 'Kredit',
            // 'Tgl_Bukti',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
