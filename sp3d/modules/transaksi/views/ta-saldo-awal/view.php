<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSaldoAwal */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Saldo Awals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-saldo-awal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Rincian' => $model->Kd_Rincian], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Rincian' => $model->Kd_Rincian], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-saldo-awal/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Saldo Awal</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-saldo-awal/delete?id=<?= $model->id?>', urlBack:'ta-saldo-awal/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Saldo Awal</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Rincian',
            'Jenis',
            'Anggaran',
            'Debet',
            'Kredit',
            'Tgl_Bukti',
        ],
    ]) ?>

</div>
