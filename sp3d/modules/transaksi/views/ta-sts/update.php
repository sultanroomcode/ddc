<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSTS */

$this->title = 'Update Ta Sts: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Sts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'No_Bukti' => $model->No_Bukti]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-sts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
