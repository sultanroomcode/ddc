<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSTS */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Sts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-sts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tahun' => $model->Tahun, 'No_Bukti' => $model->No_Bukti], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tahun' => $model->Tahun, 'No_Bukti' => $model->No_Bukti], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-sts/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Sts</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-sts/delete?id=<?= $model->id?>', urlBack:'ta-sts/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Sts</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_Bukti',
            'Tgl_Bukti',
            'Kd_Desa',
            'kd_kecamatan',
            'Uraian',
            'NoRek_Bank',
            'Nama_Bank',
            'Jumlah',
            'Nm_Bendahara',
            'Jbt_Bendahara',
        ],
    ]) ?>

</div>
