<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Sts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-sts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-sts/create'})" class="btn btn-warning">Buat Ta Sts</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'No_Bukti',
            'Tgl_Bukti',
            'Kd_Desa',
            'kd_kecamatan',
            // 'Uraian',
            // 'NoRek_Bank',
            // 'Nama_Bank',
            // 'Jumlah',
            // 'Nm_Bendahara',
            // 'Jbt_Bendahara',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
