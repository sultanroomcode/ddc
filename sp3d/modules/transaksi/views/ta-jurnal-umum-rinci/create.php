<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaJurnalUmumRinci */

$this->title = 'Create Ta Jurnal Umum Rinci';
$this->params['breadcrumbs'][] = ['label' => 'Ta Jurnal Umum Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-jurnal-umum-rinci-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
