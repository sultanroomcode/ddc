<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaJurnalUmumRinci */

$this->title = $model->NoBukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Jurnal Umum Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-jurnal-umum-rinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'NoBukti' => $model->NoBukti, 'Kd_Keg' => $model->Kd_Keg, 'RincianSD' => $model->RincianSD, 'NoID' => $model->NoID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'NoBukti' => $model->NoBukti, 'Kd_Keg' => $model->Kd_Keg, 'RincianSD' => $model->RincianSD, 'NoID' => $model->NoID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-jurnal-umum-rinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Jurnal Umum Rinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-jurnal-umum-rinci/delete?id=<?= $model->id?>', urlBack:'ta-jurnal-umum-rinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Jurnal Umum Rinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'NoBukti',
            'Kd_Keg',
            'RincianSD',
            'NoID',
            'Kd_Desa',
            'Akun',
            'Kd_Rincian',
            'Sumberdana',
            'DK',
            'Debet',
            'Kredit',
        ],
    ]) ?>

</div>
