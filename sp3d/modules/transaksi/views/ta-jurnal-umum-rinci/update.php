<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaJurnalUmumRinci */

$this->title = 'Update Ta Jurnal Umum Rinci: ' . $model->NoBukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Jurnal Umum Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NoBukti, 'url' => ['view', 'NoBukti' => $model->NoBukti, 'Kd_Keg' => $model->Kd_Keg, 'RincianSD' => $model->RincianSD, 'NoID' => $model->NoID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-jurnal-umum-rinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
