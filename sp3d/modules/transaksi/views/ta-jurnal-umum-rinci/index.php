<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Jurnal Umum Rincis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-jurnal-umum-rinci-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-jurnal-umum-rinci/create'})" class="btn btn-warning">Buat Ta Jurnal Umum Rinci</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'NoBukti',
            'Kd_Keg',
            'RincianSD',
            'NoID',
            // 'Kd_Desa',
            // 'Akun',
            // 'Kd_Rincian',
            // 'Sumberdana',
            // 'DK',
            // 'Debet',
            // 'Kredit',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
