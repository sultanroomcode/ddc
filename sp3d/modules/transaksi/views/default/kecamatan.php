<?php
use frontend\models\referensi\RefKecamatan;
use frontend\models\referensi\RefDesa;
$data = RefKecamatan::find()->where(['Kd_Kec' => $id])->one();//id dari controller
$data->tahun = $tahun;
$dc = $data->kecamatancount;
?>

<div class="row animated slideInRight">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="info-box bg-green">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Pendapatan</a></span>
				<span class="info-box-number"><?= $dc->nf($dc->dana_pendapatan) ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-orange">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Belanja</a></span>
				<span class="info-box-number"><?= $dc->nf($dc->dana_rab) ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-red">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Penerimaan / Pengeluaran</a></span>
				<span class="info-box-number"><?= $dc->nf($dc->dana_penerimaan).' / '.$dc->nf($dc->dana_pengeluaran) ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>
	<!-- /.info-box -->
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Bar Chart</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="chart">
					<canvas id="barChart" style="height:230px"></canvas>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- /.row -->

<div class="row animated slideInLeft">
<?php foreach($data->desa as $v): ?>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="info-box bg-green">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/desa?id=<?=$v->Kd_Desa?>&tahun=<?=$tahun?>'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> <?=substr($v->Nama_Desa, 11)?><br></a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
					<?php 
					$v->tahun = 2017; 
					$vrel = $v->desacount; 
					if($vrel !== null){
						echo 'RAB : Rp. '.$vrel->nf($vrel->dana_rab);
					} ?>
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>

<?php endforeach; ?>
</div>