<div class="row animated slideInRight">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="info-box bg-red">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Anggaran <?= $tahun ?></a></span>
				<span class="info-box-number" id="counter-tabel-anggaran-atas"></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description" id="progressbar-counter-anggaran-atas">
					70% Increase in 30 Days 
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-red">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Pencairan <?= $tahun ?></a></span>
				<span class="info-box-number" id="counter-tabel-pencairan-atas"></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description" id="progressbar-counter-pencairan-atas">
					70% Increase in 30 Days
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>
	<!-- /.info-box -->
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Bar Chart</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="chart">
					<canvas id="barChart" style="height:230px"></canvas>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- /.row -->

<div id="desa-container" class="animated slideInLeft">
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="linkto"><span class="info-box-icon"><i class="fa fa-calculator"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-danger btn-xs"><i class="fa fa-calculator"></i> Anggaran <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-anggaran"><?= number_format(round($ds_count->dana_anggaran), 0, ',','.') ?></span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-anggaran">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
 
			<div class="info-box bg-red">
				<span class="info-box-icon"><i class="fa fa-building-o"></i></span>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container',url:'/transaksi/ta-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-danger btn-xs"><i class="fa fa-building-o"></i> Bidang <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-bidang">{{jml_bidang}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-bidang">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-perangkat/perangkat-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="linkto"><span class="info-box-icon"><i class="fa fa-users"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-perangkat/perangkat-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-danger btn-xs"><i class="fa fa-users"></i> Perangkat Desa <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-perangkat-desa">{{jml_perangkat_desa}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-perangkat-desa">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-jurnal-umum/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-table"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-jurnal-umum/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-table"></i> Jurnal Umum <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-jurnal-umum">{{jurnal_umum}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-jurnal-umum">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-triwulan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-address-card-o"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-triwulan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-address-card-o"></i> Triwulan <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-triwulan">{{data_triwulan}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-triwulan">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>



		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-thumb-tack"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-thumb-tack"></i> Kegiatan <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-kegiatan"><?= number_format(round($ds_count->dana_kegiatan), 0, ',','.') ?></span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-kegiatan">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-exchange"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-exchange"></i> Mutasi <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-mutasi">{{data_mutasi}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-mutasi">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-scissors"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-scissors"></i> Pajak <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-pajak">{{data_pajak}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-pajak">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pencairan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-tint"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pencairan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-tint"></i> Pencairan <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-pencairan"></span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-pencairan">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-address-card-o"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-address-card-o"></i> TBP <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-tbp">{{data_tbp}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-tbp">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>



		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-location-arrow"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-location-arrow"></i> RAB <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-rab">{{data_rab}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-rab">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/rpjm-main-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="linkto"><span class="info-box-icon"><i class="fa fa-address-card-o"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/rpjm-main-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-danger btn-xs"><i class="fa fa-address-card-o"></i> RPJM <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-rpjm">{{data_rpjm}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-rpjm">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/rpjm-main-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="linkto"><span class="info-box-icon"><i class="fa fa-paper"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-address-card-o"></i> SPJ <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-spj">{{data_spj}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-spj">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-address-card-o"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-address-card-o"></i> SPP <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-spp">{{data_spp}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-spp">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-sts/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-address-card-o"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-sts/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-address-card-o"></i> STS <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-sts">{{data_sts}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-sts">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var base_url = $('body').attr('data-url');
function getReadyCounter(o){
	$.ajax({
        type: 'get',
        url: base_url+o.url,
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        },
        success: function (res) {

            $('#progressbar-counter-anggaran-atas, #progressbar-counter-anggaran').text(res.data.tabel_anggaran);
            $('#progressbar-counter-pencairan-atas, #progressbar-counter-pencairan').text(res.data.tabel_pencairan);
            $('#progressbar-counter-bidang').text(res.data.tabel_bidang);
            $('#progressbar-counter-pencairan').text(res.data.tabel_pencairan);
            $('#progressbar-counter-mutasi').text(res.data.tabel_mutasi);
            $('#progressbar-counter-pajak').text(res.data.tabel_pajak);
            $('#progressbar-counter-rab').text(res.data.tabel_rab);
            $('#progressbar-counter-rpjm').text(res.data.tabel_bidang);
            $('#progressbar-counter-kegiatan').text(res.data.tabel_kegiatan);
            $('#progressbar-counter-sts').text(res.data.tabel_sts);
            $('#progressbar-counter-spp').text(res.data.tabel_spp);
            $('#progressbar-counter-spj').text(res.data.tabel_spj);
            $('#progressbar-counter-tbp').text(res.data.tabel_tbp);
            $('#progressbar-counter-triwulan').text(res.data.tabel_bidang);
            $('#progressbar-counter-jurnal-umum').text(res.data.tabel_bidang);
            $('#progressbar-counter-perangkat-desa').text(res.data.tabel_bidang);

            $('#counter-tabel-anggaran, #counter-tabel-anggaran-atas').text(res.data.dana_anggaran);
            $('#counter-tabel-pencairan, #counter-tabel-pencairan-atas').text(res.data.dana_pencairan);
            $('#counter-tabel-kegiatan').text(res.data.dana_kegiatan);
            $('#counter-tabel-bidang').text(res.data.tabel_bidang);
            $('#counter-tabel-perangkat-desa').text(res.data.tabel_perangkat_desa);
            $('#counter-tabel-jurnal-umum').text(res.data.tabel_jurnal_umum);
            $('#counter-tabel-triwulan').text(res.data.tabel_triwulan);
            $('#counter-tabel-mutasi').text(res.data.dana_mutasi);
            $('#counter-tabel-pajak').text(res.data.dana_pajak);
            $('#counter-tabel-tbp').text(res.data.dana_tbp);
            $('#counter-tabel-rab').text(res.data.dana_rab);
            $('#counter-tabel-rpjm').text(res.data.tabel_rpjm);
            $('#counter-tabel-spj').text(res.data.dana_spj);
            $('#counter-tabel-spp').text(res.data.dana_spp);
            $('#counter-tabel-sts').text(res.data.dana_sts);
        }
    });
}

getReadyCounter({url:'/transaksi/default/data-counting-center?tahun=<?=$tahun?>&kd_kecamatan=<?=$ds_count->kd_kecamatan?>&kd_desa=<?=$ds_count->kd_desa?>'});
</script>