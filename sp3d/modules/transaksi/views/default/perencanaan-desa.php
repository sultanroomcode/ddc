<div id="penganggaran-desa-container" class="animated slideInLeft">
	<div class="row">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="linkto"><span class="info-box-icon"><i class="fa fa-calculator"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/rpjm-main-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="btn btn-danger btn-xs"><i class="fa fa-calculator"></i> RPJM Desa <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-data-umum-desa">&nbsp;</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-data-umum-desa">
						&nbsp;
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>

		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="info-box bg-red">
				<span class="info-box-icon"><i class="fa fa-building-o"></i></span>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container',url:'/transaksi/ta-perangkat/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="btn btn-danger btn-xs"><i class="fa fa-building-o"></i> Perangkat Desa <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-bidang">&nbsp;</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-bidang">
						&nbsp;
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>

		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="info-box bg-red">
				<span class="info-box-icon"><i class="fa fa-building-o"></i></span>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container',url:'/transaksi/ta-tpk/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="btn btn-danger btn-xs"><i class="fa fa-building-o"></i> TPK</a></span>
					<span class="info-box-number" id="counter-tabel-bidang">&nbsp;</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-bidang">
						&nbsp;
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var base_url = $('body').attr('data-url');
function getReadyCounter(o){
	$.ajax({
        type: 'get',
        url: base_url+o.url,
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        },
        success: function (res) {
            $('#counter-tabel-data-umum-desa').text(res.data.tabel_desa);
            $('#counter-tabel-bidang').text(res.data.dana_kegiatan);
            
            // $('#progressbar-counter-bidang').text(res.data.tabel_bidang_kegiatan);
            // $('#progressbar-counter-pendapatan').text(res.data.tabel_pendapatan);
        }
    });
}

<?php
$ds = explode('.', $id);
?>

//getReadyCounter({url:'/data-umum/data-counting-display?tahun=<?=$tahun?>&kd_kecamatan=<?=$ds[0]?>&kd_desa=<?=$id?>'});
</script>