<?php
use yii\helpers\Url;
?>

<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Penganggaran <?= $tahun ?></h2>

            <div class="tile-config dropdown">
                <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu eyes-menu" title="" data-original-title="Options"></a>
                <ul class="dropdown-menu pull-right text-right"> 
                    <!-- <li><a href="javascript:void(0)" onclick="resetView(); goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-desa/desa-create?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'});return false;">Data Desa</a></li> -->
                    <li><a href="javascript:void(0)" onclick="resetView(); goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-rab/pendapatan-desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'});return false;">Pendapatan</a></li>
                    <li><a href="javascript:void(0)" onclick="resetView(); goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'});return false;">Belanja</a></li>
                    <li><a href="javascript:void(0)" onclick="resetView(); goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-rab/penerimaan-desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'});return false;">Penerimaan</a></li>
                    <li><a href="javascript:void(0)" onclick="resetView(); goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-rab/pengeluaran-desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'});return false;">Pengeluaran</a></li>
                </ul>
            </div>


            <div class="p-10">
                <div class="nav-tabs-custom" id="penganggaran-area">
                    
                </div>
                <!-- nav-tabs-custom -->
            </div>

        <div class="clearfix"></div>
        <!-- Dynamic Chart -->
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<?php
$scripts =<<<JS
goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-rab/pendapatan-desa-list?id={$id}&tahun={$tahun}&effect=slideInRight'});

var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#2A00FF'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}

function resetView()
{
    $('.tab-pane').html(' ');
}
JS;
$this->registerJs($scripts);