<?php
use frontend\models\DesaCount;
use yii\helpers\Url;
use frontend\models\referensi\RefKecamatan;
use frontend\models\referensi\RefDesa;
$id = Yii::$app->user->identity->id;
$data = Yii::$app->user->identity->chapter->getAllRoot();
/*
frequently used : 
id=<?=$id?>&tahun=<?=$tahun?>
id={$id}&tahun={$tahun}
*/

$ds_count = DesaCount::findOne(['tahun' => $tahun, 'kd_desa' => $id]);
?>
<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa-page?tahun=<?=$tahun?>'})" class="btn btn-sm btn-primary animated slideInDown"><i class="fa fa-refresh fa-spin"></i> Refresh</a>

<h2 class="animated slideInLeft"><?= Yii::$app->user->identity->description ?> | <?= $tahun ?></h2>


<div id="desa-container" class="animated slideInRight">
	<div class="row">
		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Perencanaan</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
			        <a href="javascript:void(0)">
			        <!-- <a href="javascript:void(0)"  onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/perencanaan-desa?id=<?=$id?>&tahun=<?=$tahun?>'})"> -->
			            <div class="media">
		                	<i class="icon pull-left">&#61744</i>
		                	<div class="media-body">Data</div>
			            </div>
		            </a>

		            <!-- <div class="media">
		                <i class="icon pull-left">&#61753</i>
		                <div class="media-body">Grafik</div>
		            </div> -->
		        </div>
	        </div>
		</div>

		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Penganggaran</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
		            <a href="javascript:void(0)"  onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/penganggaran-desa?id=<?=$id?>&tahun=<?=$tahun?>'})">
		            	<div class="media">
		                <i class="icon pull-left">&#61744</i>
		                <div class="media-body">Data</div>
		            	</div>
		            </a>
		            
		            <!-- <div class="media">
		                <i class="icon pull-left">&#61753</i>
		                <div class="media-body">Grafik</div>
		            </div> -->
		        </div>
	        </div>
		</div>

		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Penata Usahaan</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
		            <a href="javascript:void(0)">
		            <!-- <a href="javascript:void(0)"  onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/penata-usahaan-desa?id=<?=$id?>&tahun=<?=$tahun?>'})"> -->
		            	<div class="media">
			                <i class="icon pull-left">&#61744</i>
			                <div class="media-body">Data</div>
			            </div>
		            </a>
		            
		            <!-- <div class="media">
		                <i class="icon pull-left">&#61753</i>
		                <div class="media-body">Grafik</div>
		            </div> -->
		        </div>
	        </div>
		</div>

		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Pembukuan</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
		            <a href="javascript:void(0)">
		            <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/pembukuan-desa?id=<?=$id?>&tahun=<?=$tahun?>'})"> -->
		            	<div class="media">
			                <i class="icon pull-left">&#61744</i>
			                <div class="media-body">Data</div>
			            </div>
			        </a>
		            
		            <!-- <div class="media">
		                <i class="icon pull-left">&#61753</i>
		                <div class="media-body">Grafik</div>
		            </div> -->
		        </div>
	        </div>
		</div>
	</div>
</div>
