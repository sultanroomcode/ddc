<?php
use frontend\models\referensi\RefKecamatan;
use frontend\models\KecamatanCount;
use frontend\models\DesaCount;
$id = ($id)?$id:Yii::$app->user->identity->id;
$dc = KecamatanCount::find()->where(['kd_kecamatan' => $id, 'tahun' => $tahun])->one();
?>

<div id="desa-container" class="animated slideInRight">
	<div class="row">
		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Pendapatan</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
		            <div class="media">
	                	<i class="icon pull-left">&#61744</i>
	                	<div class="media-body"><?= ($dc == null)?'Belum terisi':$dc->nf($dc->dana_pendapatan) ?></div>
		            </div>
		        </div>
	        </div>
		</div>

		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Pengeluaran</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
		            	<div class="media">
		                <i class="icon pull-left">&#61744</i>
		                <div class="media-body"><?= ($dc == null)?'Belum terisi':$dc->nf($dc->dana_rab) ?></div>
		            	</div>
		            </a>
		        </div>
	        </div>
		</div>

		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Penerimaan</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
	            	<div class="media">
		                <i class="icon pull-left">&#61744</i>
		                <div class="media-body"><?=($dc == null)?'Belum terisi':$dc->nf($dc->dana_penerimaan) ?></div>
		            </div>
		        </div>
	        </div>
		</div>

		<div class="col-md-3">
			<div class="tile">
		        <h2 class="tile-title">Pengeluaran</h2>
		        <div class="tile-config dropdown">
		            <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
		            <ul class="dropdown-menu pull-right text-right"> 
		                <li><a href="javascript:void(0)">Edit</a></li>
		                <li><a href="javascript:void(0)">Delete</a></li>
		            </ul>
		        </div>
		        
		        <div class="listview icon-list">
	            	<div class="media">
		                <i class="icon pull-left">&#61744</i>
		                <div class="media-body"><?= ($dc == null)?'Belum terisi':$dc->nf($dc->dana_pengeluaran) ?></div>
		            </div>
		        </div>
	        </div>
		</div>
	</div>

	<div class="row">
	    <div class="col-md-12">
	        <!-- Main Chart -->
	        <div class="tile">
	            <h2 class="tile-title">Desa <?= $tahun ?></h2>
	            <div class="p-10">
	                <?php
	                $listdesa = DesaCount::find()->where(['kd_kecamatan' => Yii::$app->user->identity->id, 'tahun' => $tahun]);
	                ?>
	                <table id="desa-count-list" class="compact row-border" width="100%">
				        <thead>
				            <tr>
				                <th>Kode Desa</th>
				                <th>Nama Desa</th>
				                <th>Pendapatan</th>
				                <th>Belanja</th>
				                <th>Penerimaan</th>
				                <th>Pengeluaran</th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				                <th>Kode Desa</th>
				                <th>Nama Desa</th>
				                <th>Pendapatan</th>
				                <th>Belanja</th>
				                <th>Penerimaan</th>
				                <th>Pengeluaran</th>
				            </tr>
				        </tfoot>
				        <tbody>
				            <?php foreach ($listdesa->all() as $v): ?>
				            <tr>
				                <th><?= $v->kd_desa ?></th>
				                <th>Nama Desa</th>
				                <th><?= $v->nf($v->dana_pendapatan) ?></th>
				                <th><?= $v->nf($v->dana_rab) ?></th>
				                <th><?= $v->nf($v->dana_penerimaan) ?></th>
				                <th><?= $v->nf($v->dana_pengeluaran) ?></th>
				            </tr>
				            <?php endforeach; ?>
				        </tbody>
				    </table>

	            </div>  
	        </div>
	    </div>
	    <div class="clearfix"></div>
	</div>
</div>

<div id="modal-custom" style="display: none;">
    <div style="width: 900px; background: #fff; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
    </div>
</div>
<?php
$scripts =<<<JS
$('#desa-count-list').DataTable();
var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#0f0'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}
JS;
$this->registerJs($scripts);