<?php
use frontend\models\referensi\RefKecamatan;
use frontend\models\referensi\RefDesa;
$data = RefDesa::findOne(['Kd_Desa' => $id]);
$data->tahun = $tahun;
$dc = $data->desacount;
?>
<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/kecamatan?id=<?=$data->kecamatan->Kd_Kec?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-primary">Kembali</a>
<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-primary"><i class="fa fa-refresh fa-spin"></i> Refresh</a>
<h2><?= substr($data->kecamatan->Nama_Kecamatan,10) ?> - <?= substr($data->Nama_Desa, 11) ?> | <?= $tahun ?></h2>
<?php if($dc != null){ ?>
<div class="row animated slideInRight">
	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="info-box bg-green">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Pendapatan</a></span>
				<span class="info-box-number"><?= $dc->nf($dc->dana_pendapatan) ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-orange">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Belanja</a></span>
				<span class="info-box-number"><?= $dc->nf($dc->dana_rab) ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
		

		<div class="info-box bg-red">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Penerimaan</a></span>
				<span class="info-box-number"><?= $dc->nf($dc->dana_penerimaan) ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-red">
			<a href="javascript:void(0)" style="color: #fff;" onclick="goLoad({url:'/transaksi/default/kecamatan'})"><span class="info-box-icon"><i class="fa fa-map-marker"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Pengeluaran</a></span>
				<span class="info-box-number"><?= $dc->nf($dc->dana_pengeluaran) ?></span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp;
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>
	<!-- /.info-box -->
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Bar Chart</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/pendapatan-desa-visual?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-primary"><i class="fa fa-pie-chart"></i></a>
				<a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/desa-visual?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-success"><i class="fa fa-bar-chart"></i></a>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<div id="modal-custom" style="display: none;">
    <div style="width: 900px; background: #fff; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
    </div>
</div>
<?php
$scripts =<<<JS
goLoad({elm:'#penganggaran-datadesa', url:'/transaksi/ta-desa/desa-create?id={$id}&tahun={$tahun}&effect=slideInRight'});

var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#0f0'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}
JS;
$this->registerJs($scripts);
} else {
	echo "Belum ada data desa yang masuk";
} //endif dc ?>