<div id="penganggaran-desa-container" class="animated slideInLeft">
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="linkto"><span class="info-box-icon"><i class="fa fa-calculator"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="btn btn-danger btn-xs"><i class="fa fa-calculator"></i> Pajak Desa <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-pajak">{{jml_data}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-pajak">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-jurnal-umum/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="linkto"><span class="info-box-icon"><i class="fa fa-building-o"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container',url:'/transaksi/ta-jurnal-umum/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="btn btn-danger btn-xs"><i class="fa fa-building-o"></i> Jurnal Umum <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-jurnal-umum">{{jml_bidang}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-jurnal-umum">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>



		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-thumb-tack"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-thumb-tack"></i> Mutasi Desa <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-mutasi">{{jml_pendapatan}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-mutasi">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-exchange"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-exchange"></i> TBP <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-tbp">{{jml_belanja}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-tbp">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-sts/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-exchange"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-sts/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-exchange"></i> STS <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-sts">{{jml_belanja}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-sts">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>



		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-location-arrow"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-location-arrow"></i> SPJ <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-spj">{{data_rab}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-spj">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>

			<div class="info-box bg-red">
				<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="linkto"><span class="info-box-icon"><i class="fa fa-address-card-o"></i></span></a>

				<div class="info-box-content">
					<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=slideInRight'})" class="btn btn-danger btn-xs"><i class="fa fa-address-card-o"></i> SPP <?= $tahun ?></a></span>
					<span class="info-box-number" id="counter-tabel-spp">{{data_rpjm}}</span>

					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
					</div>
					<span class="progress-description" id="progressbar-counter-spp">
						70% Increase in 30 Days
					</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var base_url = $('body').attr('data-url');
function getReadyCounter(o){
	$.ajax({
        type: 'get',
        url: base_url+o.url,
        error: function (xhr, ajaxOptions, thrownError) {
            return false;
        },
        success: function (res) {
            $('#counter-tabel-pajak').text(res.data.dana_pajak);
            $('#counter-tabel-jurnal-umum').text(res.data.tabel_jurnal_umum);
            $('#counter-tabel-mutasi').text(res.data.dana_mutasi);
            $('#counter-tabel-tbp').text(res.data.dana_tbp);
            $('#counter-tabel-sts').text(res.data.dana_sts);
            $('#counter-tabel-spj').text(res.data.dana_spj);
            $('#counter-tabel-spp').text(res.data.dana_spp);

            $('#progressbar-counter-pajak').text(res.data.tabel_pajak);
            $('#progressbar-counter-jurnal-umum').text(res.data.tabel_jurnal_umum);
            $('#progressbar-counter-mutasi').text(res.data.tabel_mutasi);
            $('#progressbar-counter-tbp').text(res.data.tabel_tbp);
            $('#progressbar-counter-sts').text(res.data.tabel_sts);
            $('#progressbar-counter-spj').text(res.data.tabel_spj);
            $('#progressbar-counter-spp').text(res.data.tabel_spp);
        }
    });
}

<?php
/*
counter-tabel-data-umum-desa
progressbar-counter-data-umum-desa
counter-tabel-bidang
progressbar-counter-bidang
counter-tabel-pendapatan
progressbar-counter-pendapatan
counter-tabel-belanja
progressbar-counter-belanja
counter-tabel-pembiayaan-in
progressbar-counter-pembiayaan-in
counter-tabel-pembiayaan-out
progressbar-counter-pembiayaan-out
*/
$ds = explode('.', $id);
?>

getReadyCounter({url:'/data-umum/data-counting-display?tahun=<?=$tahun?>&kd_kecamatan=<?=$ds[0]?>&kd_desa=<?=$id?>'});
</script>