<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pasar Desa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title"><?= Html::encode($this->title) ?></h2>
            <div class="p-10" id="pasar-desa-area">
                <p>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#pasar-desa-area', url:'/transaksi/pasar-desa/create'})" class="btn btn-warning">Tambah Pasar</a>
                </p>
                <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Kepemilikan Tanah</th>
                    <th>Legalitas</th>
                    <th>No Legalitas</th>
                    <th>Tahun Berdiri</th>
                    <th>Aksi</th>
                </tr>
    
                <?php foreach ($dataProvider->all() as $v){ ?>
                <tr>
                    <td><?= $v->id_pasar?></td>
                    <td><?= $v->nama?></td>
                    <td><?= $v->kepemilikan_tanah?></td>
                    <td><?= $v->legalitas?></td>
                    <td><?= $v->no_legalitas?></td>
                    <td><?= $v->tahun?></td>
                    <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#pasar-desa-area\', url:\'/transaksi/pasar-desa/update?kd_desa='.$v->kd_desa.'&id_pasar='.$v->id_pasar.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> 
                    <a href="javascript:void(0)" onclick="goLoad({elm:\'#pasar-desa-area\', url:\'/transaksi/pasar-desa/view?kd_desa='.$v->kd_desa.'&id_pasar='.$v->id_pasar.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?>
                        <a href="javascript:void(0)" onclick="deleteData('/transaksi/pasar-desa/delete?<?='kd_desa='.$v->kd_desa.'&id_pasar='.$v->id_pasar ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a></td>
                </tr>
                <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
$urlBack = '/transaksi/pasar-desa/index';
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);