<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\models\Pasardesa */

$this->title = 'Create Pasardesa';
$this->params['breadcrumbs'][] = ['label' => 'Pasardesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasardesa-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
