<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\models\Pasardesa */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pasardesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasardesa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'desa.description',
            'kecamatan.description',
            'kabupaten.description',
            'kd_provinsi',
            'id_pasar',
            'nama',
            'kepemilikan_tanah',
            'legalitas',
            'no_legalitas',
            'tahun',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <a href="javascript:void(0)" onclick="openModalXyf({url:'/transaksi/pasar-desa-data/create?kd_desa=<?=$model->kd_desa?>&id_pasar=<?=$model->id_pasar?>'})" class="btn btn-warning">Tambah Data Pasar</a>

    <table class="table">
    <tr>
        <th>ID</th>
        <th>Jumlah Pedagang</th>
        <th>Jumlah Jenis Barang</th>
        <th>Ruko</th>
        <th>Kios</th>
        <th>Los</th>
        <th>Lapak</th>
        <th>Lesehan</th>
        <th>Kantor</th>
        <th>Prasarana</th>
        <th>Aksi</th>
    </tr>
    <?php foreach ($model->data as $v) { ?>
    <tr>
        <td><?=$v->kd_isian?></td>
        <td><?=$v->jml_pedagang?></td>
        <td><?=$v->jml_jenis_barang?></td>
        <td><?=$v->pras_ruko?></td>
        <td><?=$v->pras_kios?></td>
        <td><?=$v->pras_los?></td>
        <td><?=$v->pras_lapak?></td>
        <td><?=$v->pras_lesehan?></td>
        <td><?=$v->pras_kantor?></td>
        <td><?=$v->pras_aset?></td>
        <td>
            <a href="javascript:void(0)" onclick="openModalXyf({url:'/transaksi/pasar-desa-data/view?<?='kd_desa='.$v->kd_desa.'&id_pasar='.$v->id_pasar.'&kd_isian='.$v->kd_isian ?>'})" class="btn btn-success">Lihat</a>
            <a href="javascript:void(0)" onclick="openModalXyf({url:'/transaksi/pasar-desa-data/update?<?='kd_desa='.$v->kd_desa.'&id_pasar='.$v->id_pasar.'&kd_isian='.$v->kd_isian ?>'})" class="btn btn-success">Edit</a>
            <a href="javascript:void(0)" onclick="deleteData('/transaksi/pasar-desa-data/delete?<?='kd_desa='.$v->kd_desa.'&id_pasar='.$v->id_pasar.'&kd_isian='.$v->kd_isian ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
        </td>
    </tr>
    <?php } ?>
    </table>
</div>
<?php
$urlBack = '/transaksi/pasar-desa/view?kd_desa='.$model->kd_desa.'&id_pasar='.$model->id_pasar;
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#pasar-desa-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);