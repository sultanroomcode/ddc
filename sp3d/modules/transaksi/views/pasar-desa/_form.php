<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-8',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\models\Pasardesa */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = Yii::$app->user->identity->id;
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
    $model->kd_provinsi = substr($model->kd_kecamatan, 0, 2);
    $model->genNum();
    $model->no_legalitas = '-';
}
?>

<div class="pasardesa-form">
    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_provinsi')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_pasar')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kepemilikan_tanah')->dropdownList($model->arrLahan()) ?>

    <?= $form->field($model, 'legalitas')->dropdownList(['ada' => 'Ada', 'tidak-ada' => 'Tidak Ada']) ?>

    <?= $form->field($model, 'no_legalitas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/pasar-desa/index\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({url : '/transaksi/pasar-desa/index'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);