<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\models\Pasardesa */

$this->title = 'Update Pasardesa: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Pasardesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_pasar' => $model->id_pasar]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pasardesa-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
