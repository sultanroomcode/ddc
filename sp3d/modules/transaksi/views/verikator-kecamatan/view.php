<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dVerifikatorKec */

$this->title = $model->nik;
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Verifikator Kecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp3d-verifikator-kec-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'nik' => $model->nik, 'user' => $model->user], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'nik' => $model->nik, 'user' => $model->user], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nik',
            'nama',
            'user',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
