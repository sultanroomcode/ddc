<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefRek4;
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-pajak */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
$kodeList = ArrayHelper::map(RefRek4::findAll(['Jenis' => '7.1.1.']),'Obyek',function($m){ return $m->Nama_Obyek.' :: Kode Map : '.$m->Peraturan; });

if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
}
?>

<div class="ta-pajak-form animated slideInUp">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Pajak</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'No_SSP')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Tgl_SSP')->textInput() ?>

            <?= $form->field($model, 'Keterangan')->textarea(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'NPWP')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Nama_WP')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'Alamat_WP')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Nm_Penyetor')->textInput(['maxlength' => true]) ?>           
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Jn_Transaksi')->dropdownList(['Setor' => 'Setor', 'Terima' => 'Terima']) ?>

            <?= $form->field($model, 'Kd_Rincian')->dropdownList($kodeList) ?>

            <?= $form->field($model, 'Kd_MAP')->textInput(['maxlength' => true, 'placeholder' => 'Lihat Kode Rincian']) ?>
            
            <?= $form->field($model, 'Jumlah')->textInput() ?>

            <?= $form->field($model, 'KdBayar')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS

$('#tapajak-tgl_ssp').datetimepicker({
    format:'Y-m-d H:i',
    mask:true
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-pajak/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);