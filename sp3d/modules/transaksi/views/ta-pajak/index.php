<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Pajaks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-pajak-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-pajak/create'})" class="btn btn-warning">Buat Ta Pajak</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'No_SSP',
            'Tgl_SSP',
            'Keterangan',
            // 'Nama_WP',
            // 'Alamat_WP',
            // 'NPWP',
            // 'Kd_MAP',
            // 'Nm_Penyetor',
            // 'Jn_Transaksi',
            // 'Kd_Rincian',
            // 'Jumlah',
            // 'KdBayar',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
