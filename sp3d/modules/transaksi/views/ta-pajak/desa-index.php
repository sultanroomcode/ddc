<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Pajak</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/penata-usahaan-desa?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-pajak/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Pajak</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-pajak-table" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>No</th>
	            <th>Tanggal</th>
	            <th>Nama WP</th>
	            <th>NPWP</th>
	            <th>Penyetor</th>
	            <th>Jumlah</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>No</th>
	            <th>Tanggal</th>
	            <th>Nama WP</th>
	            <th>NPWP</th>
	            <th>Penyetor</th>
	            <th>Jumlah</th>

	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->No_SSP ?></td>
	            <td><?= $v->Tgl_SSP ?></td>
	            <td><?= $v->Nama_WP ?></td>
	            <td><?= $v->NPWP ?></td>
	            <td><?= $v->Nm_Penyetor ?></td>
	            <td align="right"><?= number_format(round($v->Jumlah), 0, ',','.') ?> </td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-pajak/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_SSP?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-pajak/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Pajak Desa?', urlSend:'/transaksi/ta-pajak/desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_SSP?>', urlBack:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>	            	

	            	<?php if(Mimin::checkRoute('transaksi/ta-pajak/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_SSP?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-pajak-table').DataTable();
JS;

$this->registerJs($scripts);