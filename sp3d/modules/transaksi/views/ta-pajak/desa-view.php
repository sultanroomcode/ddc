<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-pajak-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Pajak Desa</a>
        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Pajak Desa?', urlSend:'/transaksi/ta-pajak/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&no=<?=$model->No_SSP?>', urlBack:'/transaksi/ta-pajak/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus Pajak</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'No_SSP',
            'Tgl_SSP',
            'Keterangan',
            'Nama_WP',
            'Alamat_WP',
            'NPWP',
            'Kd_MAP',
            'Nm_Penyetor',
            'Jn_Transaksi',
            'Kd_Rincian',
            'Jumlah',
            'KdBayar',
        ],
    ]) ?>

</div>
