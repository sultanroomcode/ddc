<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPajak */

$this->title = $model->No_SSP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pajaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-pajak-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_SSP], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_SSP], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-pajak/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Pajak</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-pajak/delete?id=<?= $model->id?>', urlBack:'ta-pajak/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Pajak</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'No_SSP',
            'Tgl_SSP',
            'Keterangan',
            'Nama_WP',
            'Alamat_WP',
            'NPWP',
            'Kd_MAP',
            'Nm_Penyetor',
            'Jn_Transaksi',
            'Kd_Rincian',
            'Jumlah',
            'KdBayar',
        ],
    ]) ?>

</div>
