<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPajak */

$this->title = 'Update Ta Pajak: ' . $model->No_SSP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pajaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SSP, 'url' => ['view', 'id' => $model->No_SSP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-pajak-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
