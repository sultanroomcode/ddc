<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dVerifikasiKec */

$this->title = 'Create Sp3d Verifikasi Kec';
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Verifikasi Kecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp3d-verifikasi-kec-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
