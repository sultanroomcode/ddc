<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dVerifikasiKec */

$this->title = 'Update Sp3d Verifikasi Kec: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Verifikasi Kecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'nik' => $model->nik, 'user' => $model->user]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sp3d-verifikasi-kec-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
