<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaranRinci */

$this->title = $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggaran Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-rinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_SubRinci' => $model->Kd_SubRinci, 'No_Urut' => $model->No_Urut], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_SubRinci' => $model->Kd_SubRinci, 'No_Urut' => $model->No_Urut], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran-rinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Anggaran Rinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-anggaran-rinci/delete?id=<?= $model->id?>', urlBack:'ta-anggaran-rinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Anggaran Rinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KdPosting',
            'Tahun',
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Rincian',
            'Kd_SubRinci',
            'No_Urut',
            'Uraian',
            'SumberDana',
            'JmlSatuan',
            'HrgSatuan',
            'Satuan',
            'Anggaran',
            'JmlSatuanPAK',
            'HrgSatuanPAK',
            'AnggaranStlhPAK',
            'AnggaranPAK',
        ],
    ]) ?>

</div>
