<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaranRinci */

$this->title = 'Create Ta Anggaran Rinci';
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggaran Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-rinci-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
