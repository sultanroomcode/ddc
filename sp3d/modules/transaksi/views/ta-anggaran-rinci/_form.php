<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaranRinci */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaAnggaranRinci */
/* @var $generator string helper base name with camel to id Inflector ta-anggaran-rinci */
/* @var $generator string helper base name with camel to word Inflector Ta Anggaran Rinci */
?>

<div class="ta-anggaran-rinci-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran-rinci/index'})" class="btn btn-warning">List Ta Anggaran Rinci</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'KdPosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Rincian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_SubRinci')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_Urut')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Uraian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SumberDana')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JmlSatuan')->textInput() ?>

    <?= $form->field($model, 'HrgSatuan')->textInput() ?>

    <?= $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Anggaran')->textInput() ?>

    <?= $form->field($model, 'JmlSatuanPAK')->textInput() ?>

    <?= $form->field($model, 'HrgSatuanPAK')->textInput() ?>

    <?= $form->field($model, 'AnggaranStlhPAK')->textInput() ?>

    <?= $form->field($model, 'AnggaranPAK')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-anggaran-rinci/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta Anggaran Rinci')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-anggaran-rinci/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);