<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaranRinci */

$this->title = 'Update Ta Anggaran Rinci: ' . $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggaran Rincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->KdPosting, 'url' => ['view', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_SubRinci' => $model->Kd_SubRinci, 'No_Urut' => $model->No_Urut]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-anggaran-rinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
