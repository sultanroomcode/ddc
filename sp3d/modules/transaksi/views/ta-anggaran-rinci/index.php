<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Anggaran Rincis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-rinci-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran-rinci/create'})" class="btn btn-warning">Buat Ta Anggaran Rinci</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'KdPosting',
            'Tahun',
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Rincian',
            // 'Kd_SubRinci',
            // 'No_Urut',
            // 'Uraian',
            // 'SumberDana',
            // 'JmlSatuan',
            // 'HrgSatuan',
            // 'Satuan',
            // 'Anggaran',
            // 'JmlSatuanPAK',
            // 'HrgSatuanPAK',
            // 'AnggaranStlhPAK',
            // 'AnggaranPAK',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
