<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPPPot */

$this->title = 'Update Ta Spppot: ' . $model->No_SPP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spppots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SPP, 'url' => ['view', 'No_SPP' => $model->No_SPP, 'Kd_Keg' => $model->Kd_Keg, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spppot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
