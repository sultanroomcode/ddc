<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Spppots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spppot-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spppot/create'})" class="btn btn-warning">Buat Ta Spppot</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'Kd_Keg',
            // 'No_Bukti',
            // 'Kd_Rincian',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
