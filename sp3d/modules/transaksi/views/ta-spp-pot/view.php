<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPPPot */

$this->title = $model->No_SPP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spppots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spppot-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_SPP' => $model->No_SPP, 'Kd_Keg' => $model->Kd_Keg, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_SPP' => $model->No_SPP, 'Kd_Keg' => $model->Kd_Keg, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spppot/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spppot</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spppot/delete?id=<?= $model->id?>', urlBack:'ta-spppot/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spppot</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'Kd_Keg',
            'No_Bukti',
            'Kd_Rincian',
            'Nilai',
        ],
    ]) ?>

</div>
