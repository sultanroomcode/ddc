<?php
use yii\helpers\Url;
?>
<!-- Box Comment -->
<div class="box box-widget">
<div class="box-body">
<a href="javascript:void(0)" onclick="refreshTree()" class="btn btn-sm btn-info"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-sitemap"></i></a>
<div class="row">
    <div class="col-md-6">
        <div id="jstree_pengeluaran" style="overflow-x: auto; height: 400px;margin-left: 20px;"></div>
    </div>
    <div class="col-md-6">
        <div id="ta-pengeluaran-main-form"></div>
    </div>
</div>

</div>
</div>
<?php
$url_jstree = Url::to(['/data-umum/menu-utama', 'referensi' => 'pembiayaan', 'type' => 2, 'kd_desa' => $id, 'tahun' => $tahun], true);
$script = <<<JS
$('#jstree_pengeluaran').jstree({
     "plugins" : [ "search" ],
     'core' : {
        'data' : {
            "url" : "{$url_jstree}",
            "data" : function (node) {
                return { "id" : node.id };
            }
        }
    }
});

    
function refreshTree(){
    $('#jstree_pengeluaran').jstree(true).refresh();
}
JS;

$this->registerJs($script);