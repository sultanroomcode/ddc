<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use frontend\models\DesaCount;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
	<div class="box-body">
	  	<!-- post text -->
	  	<a href="javascript:void(0)" onclick="goLoad({elm:'#penerimaan-index-dash', url:'/transaksi/ta-rab/penerimaan-desa-list?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-warning"><i class="fa fa-spin fa-refresh"></i></a>
	  	<!-- <a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/penerimaan-desa-main-form?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i></a> -->
	  	<a href="<?= Url::to(['/data-umum/pdf?bagian=data-rab-penerimaan&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-file-pdf-o"></i></a>
	  	<div class="row">
			<div class="col-md-12">
				<div id="penerimaan-index-dash"></div>
			</div>
		</div>
	</div>
</div>
<?php
$scripts =<<<JS
	goLoad({elm:'#penerimaan-index-dash', url:'/transaksi/ta-rab/penerimaan-desa-list?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
	$('#ta-rab-table').DataTable();
JS;

$this->registerJs($scripts);