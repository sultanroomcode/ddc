<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rab-view animated slideInLeft">
<!-- Box Comment -->
<div class="box box-widget">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel RAB - Belanja</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
    <!-- post text -->
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">Daftar Belanja</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RAB Desa?', urlSend:'/transaksi/ta-rab/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>&kd2=<?=$model->Kd_Rincian?>', urlBack:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RAB</a>

        <a href="<?= Url::to(['/data-umum/pdf?bagian=laporan-belanja-rinci&kode='.$model->Kd_Keg.'&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-info">Laporan Belanja Rinci<i class="fa fa-file-pdf-o"></i></a><br>
    </p>

    <div class="well">
        <div class="row">
            <div class="col-md-4">
                <b>Deksripsi Kegiatan</b><br>
                <b>Tahun :</b><br><?= $model->detailkegiatan->Tahun ?><br>
                <b>Rekening Kegiatan :</b><br><?= $model->detailkegiatan->ID_Keg ?><br>
                <b>Nama Kegiatan :</b><br><?= $model->detailkegiatan->Nama_Kegiatan ?><br>
                <b>Pagu :</b><br><?= $model->nf($model->detailkegiatan->Pagu) ?><br>
                <b>Pagu (PAK) :</b><br><?= $model->nf($model->detailkegiatan->Pagu_PAK) ?><br>
            </div>
            <div class="col-md-4">
                <br>
                <b>Nama PPTK :</b><br><?= $model->detailkegiatan->Nm_PPTKD ?><br>
                <b>NIP :</b><br><?= $model->detailkegiatan->NIP_PPTKD ?><br>
                <b>Lokasi :</b><br><?= $model->detailkegiatan->Lokasi ?><br>
                <b>Waktu :</b><br><?= $model->detailkegiatan->Waktu ?><br>
                <b>Keluaran:</b><br><?= $model->detailkegiatan->Keluaran ?><br>
                <b>Sumber Dana:</b><br><?= $model->detailkegiatan->Sumberdana ?><br>
            </div>

            <div class="col-md-4">
                <b>Deksripsi Belanja</b><br>
                <?php
                echo '<b>Anggaran :</b><br>'.$model->nf($model->Anggaran).'<br>';
                echo '<b>Anggaran PAK :</b><br>'.$model->nf($model->AnggaranPAK).'<br>';
                echo '<b>Anggaran Setelah PAK :</b><br>'.$model->nf($model->AnggaranStlhPAK).'<br>';
                ?>
            </div>
        </div>
    </div>

    <div id="desa-view-in-detail"></div>

    <h2>Rincian</h2>
    <?php if($model->rincian != null){  ?>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/desa-create?id=<?=$model->Kd_Desa?>&tahun=<?=$model->Tahun?>&kd2=<?=$model->Kd_Keg?>&kd=<?=$model->Kd_Rincian?>'})" class="btn btn-info btn-sm">Buat Rincian Belanja Baru</a><br>

    <table id="ta-rincian-in-rab-table" class="display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Uraian</th>
                <th>Volume</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Anggaran</th>
                
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Uraian</th>
                <th>Volume</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Anggaran</th>
                
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($model->rincian2 as $vk): ?>
            <tr>
                <td><?= $vk->No_Urut ?></td>
                <td><?= $vk->Uraian ?></td>
                <td align="right"><?= $model->nf($vk->JmlSatuan) ?></td>
                <td><?= $vk->Satuan ?></td>
                <td align="right"><?= $model->nf($vk->HrgSatuan) ?></td>
                <td align="right"><?= $model->nf($vk->Anggaran) ?></td>
                
                <td>
                    <?php // if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-update')): ?>
                    <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/desa-update?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>&kd2=<?=$vk->Kd_Rincian?>&kd3=<?=$vk->Kd_SubRinci?>&no=<?=$vk->No_Urut?>'})" class="label label-danger">Update</a> -->
                    <?php // endif; ?>

                    <?php if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-view')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-view-in-detail', url:'/transaksi/ta-rab-rinci/desa-view?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>&kd2=<?=$vk->Kd_Rincian?>&kd3=<?=$vk->Kd_SubRinci?>&no=<?=$vk->No_Urut?>'})" class="label label-warning">View</a>
                    <?php endif; ?>
                    
                    <?php if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data rab-rinci Desa?', urlSend:'/transaksi/ta-rab-rinci/desa-delete?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>&kd2=<?=$vk->Kd_Rincian?>&kd3=<?=$vk->Kd_SubRinci?>&no=<?=$vk->No_Urut?>', urlBack:'/transaksi/ta-rab/desa-view?kd=<?=$vk->Kd_Keg?>&kd2=<?=$vk->Kd_Rincian?>&id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
                    <?php endif; ?>   
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php
$scripts =<<<JS
    $('#ta-rincian-in-rab-table').DataTable();
JS;
$this->registerJs($scripts);

    } else { ?>
    Rincian di dalam Pendapatan ini masih kosong<br><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/desa-create?id=<?=$model->Kd_Desa?>&tahun=<?=$model->Tahun?>&kd2=<?=$model->Kd_Keg?>&kd=<?=$model->Kd_Rincian?>'})" class="btn btn-info btn-sm">Klik Untuk Membuat Rincian Baru</a>
    <?php } ?>
</div>
</div>
</div>