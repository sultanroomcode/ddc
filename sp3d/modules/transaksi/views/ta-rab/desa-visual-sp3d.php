<?php
use hscstudio\mimin\components\Mimin;
use sp3d\models\transaksi\TaRAB;
use sp3d\models\DesaCount;
use yii\helpers\Url;
$formatt = new DesaCount();
$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT SUBSTR(tk.`ID_Keg`, 1, 2) as Keg, rb.nama, COUNT(tk.ID_Keg) as c_keg FROM Ta_Kegiatan tk LEFT JOIN ref_bidang rb ON SUBSTR(tk.`ID_Keg`, 1, 2) = rb.`kode` WHERE tk.`user_id` ='".$id."' AND tk.`Tahun` ='$tahun' GROUP BY Keg;");
// $model = $conn->createCommand("SELECT SUBSTR(tk.`ID_Keg`, 1, 1) as Keg, rb.nama, COUNT(tk.ID_Keg) as c_keg FROM Ta_Kegiatan tk LEFT JOIN ref_bidang rb ON SUBSTR(tk.`ID_Keg`, 1, 1) = SUBSTR(rb.`kode`, 2, 1)  WHERE tk.`user_id` ='".$id."' AND tk.`Tahun` ='2017' GROUP BY Keg;");
$model2 = $conn->createCommand("SELECT SUM( a.`Pagu` ) AS Ang, SUBSTR( a.`Kd_Keg` , 7, 2 ) AS Kode_as, b.kode, b.nama
FROM `Ta_Kegiatan` a
INNER JOIN `ref_bidang` b ON b.kode = SUBSTR( a.`ID_Keg` , 1, 2)
COLLATE utf8_unicode_ci
WHERE a.`user_id` ='".$id."' AND a.`Tahun` = '$tahun'
GROUP BY Kode_as");
/*$model2 = $conn->createCommand("SELECT SUM( a.`Anggaran` ) AS Ang, SUBSTR( a.`Kd_Keg` , 7, 1 ) AS Kode_as, b.kode, b.nama
FROM `Ta_RABRinci` a
INNER JOIN `ref_bidang` b ON b.kode_normal = SUBSTR( a.`Kd_Keg` , 7, 1 )
COLLATE utf8_unicode_ci
WHERE a.`user_id` ='".$id."' AND a.`Kd_Rincian` LIKE '5.%'
GROUP BY Kode");*/

$resultcount = $model->queryAll();
$resultang = $model2->queryAll();

$destdana = $valdana = [];
$nilaidana = $jmldata =0;
$tabledana = '<table class="table">';
$tabledana .= '<tr><td>Kode</td><td>Uraian</td><td>Anggaran</td><td>Keg</td></tr>';
foreach($resultang as $k => $v){
    $destdana[] = "'".$v['kode']."'";
    $valdana[] = $v['Ang'];
    $nilaidana += $v['Ang'];
    $jmldata += $resultcount[$k]['c_keg'];
    
    $tabledana .= '<tr><td>'.$v['kode'].'</td><td>'.$v['nama'].'</td><td>'.$formatt->nf($v['Ang']).'</td><td>'.$formatt->nfo($resultcount[$k]['c_keg']).'</td></tr>';    
}
$tabledana .= '<tr><td></td><td></td><td>'.$formatt->nf($nilaidana).'</td><td>'.$formatt->nfo($jmldata).'</td></tr>';
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-6"><div id="chart-belanja" style="width: 100%;height:300px;">aa</div></div>
            <div class="col-md-6"><h2>Rencana Belanja Per Bidang</h2><?= $tabledana ?>
            </div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-belanja'));
    option = {
        backgroundColor:'#fff',
        color: ['#F00', '#0FF', '#0F0', '#FF0'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : [<?=implode(',', $destdana)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Rencana',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $valdana)?>]
            }
        ]
    };
    
    myChart.setOption(option);
</script>