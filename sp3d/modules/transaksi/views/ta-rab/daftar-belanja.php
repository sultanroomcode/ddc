<table id="ta-rab-daftar-belanja" class="compact row-border" width="100%">
    <thead>
        <tr>
            <th>Kode</th>               
            <th>Nama</th>
            <th>Pagu</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Kode</th>
            <th>Nama</th>
            <th>Pagu</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($model->all() as $vk): ?>
        <tr>
            <td><?= $vk->Kd_Rincian ?></td>
            <td><?php if($vk->uraian['Nama_Obyek'] != null){ echo $vk->uraian->Nama_Obyek; } else { echo "--";} ?></td>
            <td align="right"><?= $vk->nf($vk->Anggaran) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$scripts =<<<JS
    $('#ta-rab-daftar-belanja').DataTable();
JS;

$this->registerJs($scripts);