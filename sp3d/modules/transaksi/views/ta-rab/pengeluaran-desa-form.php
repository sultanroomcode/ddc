<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefRekeningPembiayaan;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaKegiatan;
$sdanaList = ArrayHelper::map(RefSumber::find()->all(),'Kode','Nama_Sumber');
$kegiatanList = ArrayHelper::map(TaKegiatan::find()->where(['Kd_Desa' => $id])->all(),'Kd_Keg','Nama_Kegiatan');
$kegList = ArrayHelper::map(RefRekeningPembiayaan::find()->where(['tipe' => '1'])->andWhere('kode LIKE :query')->addParams([':query'=> '3.2%'])->all(),'kode',function($m){ return $m->kode.' | '.$m->uraian; });
//how to add item on first order in array?
$kegList = array_merge(['0' => '---'],$kegList);
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    $model->kegiatan = 'pengeluaran';
    $model->sumberdana = '-';
    $model->Kd_Keg = $id.'00.00.';
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Anggaran = $model->AnggaranPAK = $model->AnggaranStlhPAK = 0;
} else {
    $model->pendapatan_x = $model->pendapatan_xmask = $model->Anggaran;
}
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInRight">
<div class="box-body">
<div class="ta-rab-form">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/pengeluaran-desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">Daftar Pengeluaran</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'kegiatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-12">
            <?php if($model->isNewRecord && $automate){
                $model->Kd_Rincian = $kode;
                echo '<b>Kode :</b><br> '.$kode;//tampilkan juga uraiannya
            } else {             
            ?>
            <?php }
            echo $form->field($model, 'Kd_Rincian')->hiddenInput(['maxlength' => true])->label(false);
            echo $form->field($model, 'sumberdana')->dropdownList($sdanaList)->label('Sumber Dana');
            echo $form->field($model, 'pendapatan_x')->hiddenInput(['maxlength' => true])->label(false);
            echo $form->field($model, 'pendapatan_xmask')->widget(\yii\widgets\MaskedInput::className(), [
                'options' => [
                    'onchange' => 'updateCount()',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ]);
             ?>              
        
            <?= $form->field($model, 'Anggaran')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>

            <?= $form->field($model, 'AnggaranPAK')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>

            <?= $form->field($model, 'AnggaranStlhPAK')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>

            <?= $form->field($model, 'rab_ext')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            <?= $form->field($model, 'rab_ext_ada')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
        </div>
    </div>   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Isi Pengeluaran' : 'Update Pengeluaran', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
</div>
<?php
$newRecord = ($model->isNewRecord)?'baru':'update';
$script = <<<JS
var newRecord = '{$newRecord}';
function checkParent()
{
    var id = $('#tarab-kd_rincian').val();
    var id_desa = $('#tarab-kd_desa').val();
    $.get(base_url+'/transaksi/ta-rab-ext/check-parent-pendapatan?id_child='+id+'&kd_desa='+id_desa).done(function(res){
        var parsing = JSON.parse(res);
        $('#tarab-rab_ext').val(parsing.createvar);
        $('#tarab-rab_ext_ada').val(parsing.existvar);
    });
}

checkParent();

function updateCount(){
    var s1 = $('#tarab-pendapatan_xmask').val().replace(/\./g, "").replace(/\,/g, ".");

    $('#tarab-pendapatan_x').val(s1);
}

function cariProgram(){
    var urlid = $('#tarab-kode_bidang').val();
    $.post(base_url+'/data-umum/referensi?jenis=pembiayaan&kode='+urlid+'&form=1').done(function(res){
        $('#tarab-kode_program').html(res);
        $('#tarab-kd_rincian, #tarab-kode_rincian').html("<option value=''>---</option>");
    });
}

function cariKegiatan(){
    var urlid = $('#tarab-kode_program').val();
    if(urlid !== ''){
        $.post(base_url+'/data-umum/referensi?jenis=pembiayaan&type=2&kode='+urlid+'&form=1').done(function(res){
            $('#tarab-kd_rincian').html(res);
        });
    } else {
        $('#tarab-kd_rincian').html("<option value=''>---</option>");
    }
}
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            if(newRecord == 'baru'){
                $(\$form).trigger('reset');
                goHiatus({elm:'#ta-pengeluaran-main-form', state:'hide'});
                goLoad({elm:'#pengeluaran-index-dash',url : '/transaksi/ta-rab/pengeluaran-desa-list?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
            } else {
                goLoad({elm:'#pengeluaran-index-dash',url : '/transaksi/ta-rab/pengeluaran-desa-list?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
                Custombox.modal.close();
            }  
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);