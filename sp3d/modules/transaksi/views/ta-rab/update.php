<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRAB */

$this->title = 'Update Ta Rab: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rabs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rab-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
