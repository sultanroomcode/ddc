<?php
use hscstudio\mimin\components\Mimin;
use frontend\models\DesaCount;
?>
<div class="box box-widget animated slideInLeft">
	<table id="pendapatan-table" class="compact row-border" width="100%">
    <thead>
        <tr>
            <th>Kode Rincian</th>
            <th>Uraian</th>
            <th>Anggaran</th>
            <th>Anggaran PAK</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Kode Rincian</th>
            <th>Uraian</th>
            <th>Anggaran</th>
            <th>Anggaran PAK</th>
        </tr>
    </tfoot>
    <tbody>
    	<?php foreach ($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->Kd_Rincian ?></td>
            <td><?= (!isset($v->nama_rincian))?'Kode Rekening Lokal Kabupaten':$v->nama_rincian ?></td>
            <td align="right"><?= $v->nf(round($v->Anggaran)) ?> </td>
            <td align="right"><?= $v->nf(round($v->AnggaranPAK)) ?> </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<?php
$scripts =<<<JS
	$('#pendapatan-table').DataTable();
JS;
$this->registerJs($scripts);