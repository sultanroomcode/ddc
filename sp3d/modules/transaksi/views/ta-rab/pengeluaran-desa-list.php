<?php
use hscstudio\mimin\components\Mimin;
use frontend\models\DesaCount;
use yii\helpers\Url;
?>
<div class="animated slideInLeft">
	<br><br>	
  	<table id="ta-rab-table" class="compact row-border" width="100%">
	    <thead>
	        <tr>
	            <th>Kode Rincian</th>
	            <th>Uraian</th>
	            <th>Anggaran</th>
	            <th>Anggaran PAK</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kode Rincian</th>
	            <th>Uraian</th>
	            <th>Anggaran</th>
	            <th>Anggaran PAK</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->Kd_Rincian ?></td>
	            <td><?= $v->uraian->Nama_Obyek ?></td>
	            <td align="right"><?= $v->nf($v->Anggaran) ?> </td>
	            <td align="right"><?= $v->nf($v->AnggaranPAK) ?> </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
<?php
$scripts =<<<JS
	$('#ta-rab-table').DataTable();
JS;

$this->registerJs($scripts);