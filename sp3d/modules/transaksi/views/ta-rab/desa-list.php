<?php
use hscstudio\mimin\components\Mimin;
use frontend\models\DesaCount;
use yii\helpers\Url;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Belanja</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">	
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/penganggaran-desa?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if($dataProvider->count() > 0){ ?>
	<a href="<?= Url::to(['/data-umum/pdf?bagian=data-rab-belanja&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-sm btn-info">PDF</a>
	<?php } else { ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-warning">Isi Bidang dan Kegiatan</a>
	<?php } ?>
	<hr>

	<div class="row">
		<div class="col-md-12" id="belanja-index-dash">
			<?php
			if($dataProvider->count() > 0){
			$dsc = DesaCount::find()->select('dana_rab')->where(['kd_desa' => $id, 'tahun' => $tahun])->one();
			echo "<b>Jumlah : ". $dsc->nf($dsc->dana_rab).'</b><br>';
			?>
		  	
		  	<table id="ta-rab-table" class="compact row-border" width="100%">
			    <thead>
			        <tr>
			            <th>Kode Rincian</th>
			            <th>Kode Kegiatan</th>
			            <th>Anggaran</th>
			            <th>Anggaran PAK</th>
			            <th>Action</th>
			        </tr>
			    </thead>
			    <tfoot>
			        <tr>
			            <th>Kode Rincian</th>
			            <th>Kode Kegiatan</th>
			            <th>Anggaran</th>
			            <th>Anggaran PAK</th>
			            <th>Action</th>
			        </tr>
			    </tfoot> 
			    <tbody>
			    	<?php foreach ($dataProvider->all() as $v): ?>
			        <tr>
			            <td><?= $v->Kd_Rincian ?></td>
			            <td><?= $v->rincianKegiatan->uraian ?></td>
			            <td align="right"><?= $v->nf($v->Anggaran) ?> </td>
			            <td align="right"><?= $v->nf($v->AnggaranPAK) ?> </td>
			            <td>
			            	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-delete')): ?>
			            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RAB Desa?', urlSend:'/transaksi/ta-rab/desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>', urlBack:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
			            	<?php endif; ?>	            	

			            	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-view')): ?>
			            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>'})" class="label label-warning">Rincian</a>
			            	<?php endif; ?>
			            </td>
			        </tr>
			        <?php endforeach; ?>
			    </tbody>
			</table>
			<?php } else { ?>
				<h3>Harap untuk mengisi bidang dan kegiatan dulu</h3>
			<?php } ?>
		</div>
	</div>
</div>
</div>
<?php
$scripts =<<<JS
	$('#ta-rab-table').DataTable();
JS;

$this->registerJs($scripts);