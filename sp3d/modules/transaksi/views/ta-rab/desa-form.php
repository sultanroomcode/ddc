<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefRekeningKegiatan;
use frontend\models\transaksi\TaBidang;
use frontend\models\transaksi\TaKegiatan;
$bidangList = ArrayHelper::map(TaBidang::find()->where(['Kd_Desa' => $id])->all(),'Kd_Bid',function($m){ return $m->Kd_Bid.' | '. $m->Nama_Bidang; });
$kegiatanList = ArrayHelper::map(TaKegiatan::find()->where(['Kd_Desa' => $id])->all(),'Kd_Keg','Nama_Kegiatan');
$kegList = ArrayHelper::map(RefRekeningKegiatan::find()->where(['tipe' => '1'])->all(),'kode',function($m){ return $m->kode.' | '. $m->uraian; });
//how to add item on first order in array?
$bidangList = array_merge(['0' => '---'],$bidangList);
$kegList = array_merge(['0' => '---'],$kegList);
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    $model->sumberdana = '-';
    $model->kegiatan = 'belanja';
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Anggaran = $model->AnggaranPAK = $model->AnggaranStlhPAK = 0;
}
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInRight">
<div class="box-body">
<div class="ta-rab-form">
    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    
    <?= $form->field($model, 'sumberdana')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kegiatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'kode_bidang_kegiatan')->dropdownList($bidangList, ['onchange' => 'cariBidangKegiatan()']) ?>
            <?= $form->field($model, 'Kd_Keg')->dropdownList(['' => '---']) ?>
            <?php if($model->isNewRecord && $automate){
                $model->Kd_Rincian = $kode;
                echo $form->field($model, 'Kd_Rincian')->hiddenInput(['maxlength' => true])->label(false);
            } else { ?>
            <?= $form->field($model, 'kode_bidang')->dropdownList($kegList, ['onchange' => 'cariProgram()']) ?>

            <?= $form->field($model, 'kode_program')->dropdownList(['' => '---'], ['onchange' => 'cariKegiatan()']) ?>

            <?= $form->field($model, 'Kd_Rincian')->dropdownList(['' => '---']) ?> 
            <?php } ?>             
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'Anggaran')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'AnggaranPAK')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'AnggaranStlhPAK')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        </div>
    </div>   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Isi Belanja' : 'Update Belanja', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
</div>
</div>
<?php $script = <<<JS
function cariBidangKegiatan(){
    var urlid = $('#tarab-kode_bidang_kegiatan').val();
    if(urlid !== ''){
        $.post(base_url+'/data-umum/ambil-transaksi-kegiatan?bid='+urlid+'&form=1').done(function(res){
            $('#tarab-kd_keg').html(res);
        });
    } else {
        $('#tarab-kd_keg').html("<option value=''>---</option>");
    }
}

function cariProgram(){
    var urlid = $('#tarab-kode_bidang').val();
    $.post(base_url+'/data-umum/referensi?jenis=kegiatan&kode='+urlid+'&form=1').done(function(res){
        $('#tarab-kode_program').html(res);
        $('#tarab-kd_rincian').html("<option value=''>---</option>");
    });
}

function cariKegiatan(){
    var urlid = $('#tarab-kode_program').val();
    if(urlid !== ''){
        $.post(base_url+'/data-umum/referensi?jenis=kegiatan&type=2&kode='+urlid+'&form=1').done(function(res){
            $('#tarab-kd_rincian').html(res);
        });
    } else {
        $('#tarab-kd_rincian').html("<option value=''>---</option>");
    }
}
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-rab/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);