<?php
use yii\helpers\Url;
?>
<!-- Box Comment -->
<div class="box box-widget">
<div class="box-body">
<a href="javascript:void(0)" onclick="refreshTree()" class="btn btn-sm btn-info"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-sitemap"></i></a>
<div class="row">
    <div class="col-md-6">
        <div id="jstree_pendapatan" style="overflow-x: auto; height: 300px;margin-left: 20px;"></div>
    </div>
    <div class="col-md-6">
        <div id="ta-pendapatan-main-form"></div>
    </div>
</div>

</div>
</div>
<?php
$url_jstree = Url::to(['/data-umum/menu-utama', 'referensi' => 'pendapatan', 'type' => 1, 'kd_desa' => $id, 'tahun' => $tahun], true);

$script = <<<JS
$('#jstree_pendapatan').jstree({
     "plugins" : [ "search" ],
     'core' : {
        'data' : {
            "url" : "{$url_jstree}",
            "data" : function (node) {
                return { "id" : node.id };
            }
        }
    }
});

    
function refreshTree(){
    $('#jstree_pendapatan').jstree(true).refresh();
}
JS;

$this->registerJs($script);