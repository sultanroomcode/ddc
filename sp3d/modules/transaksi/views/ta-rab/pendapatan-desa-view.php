<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rab-view animated slideInLeft">
<!-- Box Comment -->
<div class="box box-widget">
<!-- /.box-header -->
<div class="box-body">
    <!-- post text -->
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RAB Desa?', urlSend:'/transaksi/ta-rab/pendapatan-desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>&kd2=<?=$model->Kd_Rincian?>', urlBack:'/transaksi/ta-rab/pendapatan-desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-sm btn-danger">Hapus Pendapatan</a>

        <a href="<?= Url::to(['/data-umum/pdf?bagian=laporan-pendapatan-rinci&kode='.$model->Kd_Rincian.'&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-sm btn-info">PDF</a>
    </p>

    <div class="row">
        <div class="col-md-6">
            <?php
            echo '<b>Kode Rekening :</b><br>'.$model->Kd_Rincian.'<br>';
            // echo '<b>Uraian :</b><br>'.$model->rincianPendapatan->uraian.'<br>';
            echo '<b>Sumber Dana :</b><br>'.$model->sumberdana.'<br>';
            ?>
        </div>
        <div class="col-md-6">
            <?php
            echo '<b>Anggaran :</b><br>'.$model->nf(round($model->Anggaran)).'<br>';
            echo '<b>Anggaran PAK :</b><br>'.$model->nf(round($model->AnggaranPAK)).'<br>';
            echo '<b>Anggaran Setelah PAK :</b><br>'.$model->nf(round($model->AnggaranStlhPAK)).'<br>';
            ?>
        </div>
    </div>

    <div id="desa-view-in-detail"></div>

    <h2>List Rincian Pendapatan</h2>
    <?php if($model->rincian2 != null){  ?>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/isi-rincian-pendapatan?id=<?=$model->Kd_Desa?>&tahun=<?=$model->Tahun?>&kd2=<?=$model->Kd_Keg?>&kd=<?=$model->Kd_Rincian?>'})" class="btn btn-info btn-sm">Buat Rincian Baru</a><br>

    <table id="ta-rincian-in-rab-table" class="display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Uraian</th>
                <th>Jumlah</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Anggaran</th>
                
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Uraian</th>
                <th>Jumlah</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Anggaran</th>
                
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($model->rincian2 as $vk): ?>
            <tr>
                <td><?= $vk->No_Urut ?></td>
                <td><?= $vk->Uraian ?></td>
                <td align="right"><?= $model->nf(round($vk->JmlSatuan)) ?></td>
                <td><?= $vk->Satuan ?></td>
                <td align="right"><?= $model->nf(round($vk->HrgSatuan)) ?></td>
                <td align="right"><?= $model->nf(round($vk->Anggaran)) ?></td>
                
                <td>
                    <?php // if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-update')): ?>
                    <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/desa-update?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>&kd2=<?=$vk->Kd_Rincian?>&kd3=<?=$vk->Kd_SubRinci?>&no=<?=$vk->No_Urut?>'})" class="label label-danger">Update</a> -->
                    <?php // endif; ?>

                    <?php if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-view')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-view-in-detail', url:'/transaksi/ta-rab-rinci/desa-view?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>&kd2=<?=$vk->Kd_Rincian?>&kd3=<?=$vk->Kd_SubRinci?>&no=<?=$vk->No_Urut?>'})" class="label label-warning">View</a>
                    <?php endif; ?>
                    
                    <?php if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data rab-rinci Desa?', urlSend:'/transaksi/ta-rab-rinci/desa-delete?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>&kd2=<?=$vk->Kd_Rincian?>&kd3=<?=$vk->Kd_SubRinci?>&no=<?=$vk->No_Urut?>', urlBack:'/transaksi/ta-rab/pendapatan-desa-view?kd=<?=$model->Kd_Keg?>&id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
                    <?php endif; ?>   
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php
$scripts =<<<JS
    $('#ta-rincian-in-rab-table').DataTable();
JS;

$this->registerJs($scripts);

    } else { ?>
    Rincian di dalam Pendapatan ini masih kosong<br><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/isi-rincian-pendapatan?id=<?=$model->Kd_Desa?>&tahun=<?=$model->Tahun?>&kd2=<?=$model->Kd_Keg?>&kd=<?=$model->Kd_Rincian?>'})" class="btn btn-info btn-sm">Klik Untuk Membuat Rincian Baru</a>
    <?php } ?>
</div>
</div>
</div>