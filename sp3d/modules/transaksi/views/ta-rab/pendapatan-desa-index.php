<?php
use yii\helpers\Url;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
	<div class="box-body">
		<a href="javascript:void(0)" onclick="goLoad({elm:'#penganggaran-area', url:'/transaksi/ta-rab/pendapatan-desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-warning"><i class="fa fa-spin fa-refresh"></i></a>
		<!-- <a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/pendapatan-desa-main-form?id=<?=$id?>&tahun=<?=$tahun?>&automate=true'})" class="btn btn-sm btn-primary"><i class="fa fa-plus-square"></i></a> 
		<a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/pendapatan-desa-visual?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-success"><i class="fa fa-pie-chart"></i></a>-->
		<a href="<?= Url::to(['/data-umum/pdf?bagian=data-rab-pendapatan&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-file-pdf-o"></i></a>
		<br>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div id="pendapatan-index-dash"></div>
			</div>
		</div>		
	</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
goLoad({elm : '#pendapatan-index-dash', url:'/transaksi/ta-rab/pendapatan-desa-list?id={$id}&tahun={$tahun}&effect=slideInRight'});
JS;
$this->registerJs($scripts);
