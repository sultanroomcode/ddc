<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use frontend\models\DesaCount;
?>
<div class="animated slideInRight">
    <br><br>
	<table id="ta-rab-table" class="compact row-border" width="100%">
    <thead>
        <tr>
            <th>Kode Rincian</th>
            <th>Uraian</th>
            <th>Anggaran</th>
            <th>Anggaran PAK</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Kode Rincian</th>
            <th>Uraian</th>
            <th>Anggaran</th>
            <th>Anggaran PAK</th>
        </tr>
    </tfoot>
    <tbody>
    	<?php foreach ($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->Kd_Rincian ?></td>
            <td><?= $v->uraian->Nama_Obyek ?></td>
            <td align="right"><?= $v->nf($v->Anggaran) ?> </td>
            <td align="right"><?= $v->nf($v->AnggaranPAK) ?> </td>
            <!-- <td>
            	<?php if(Mimin::checkRoute('transaksi/ta-rab/penerimaan-desa-update')): ?>
            	<a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/penerimaan-desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>'})" class="label label-warning"><i class="fa fa-pencil"></i></a>
            	<?php endif; ?>

            	<?php if(Mimin::checkRoute('transaksi/ta-rab/penerimaan-desa-view')): ?>
            	<a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/penerimaan-desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>'})" class="label label-warning"><i class="fa fa-eye"></i></a>
            	<?php endif; ?>

            	<?php if(Mimin::checkRoute('transaksi/ta-rab/penerimaan-desa-delete')): ?>
            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#penerimaan-index-dash',msg:'Hapus Data RAB Desa?', urlSend:'/transaksi/ta-rab/penerimaan-desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>', urlBack:'/transaksi/ta-rab/penerimaan-desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger"><i class="fa fa-trash"></i></a>
            	<?php endif; ?>	    
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-rab-table').DataTable();
JS;

$this->registerJs($scripts);