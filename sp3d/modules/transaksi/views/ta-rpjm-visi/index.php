<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmvisis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmvisi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmvisi/create'})" class="btn btn-warning">Buat Ta Rpjmvisi</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_Visi',
            'Kd_Desa',
            'kd_kecamatan',
            'No_Visi',
            'Uraian_Visi',
            // 'TahunA',
            // 'TahunN',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
