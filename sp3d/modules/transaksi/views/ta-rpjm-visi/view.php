<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMVisi */

$this->title = $model->ID_Visi;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmvisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmvisi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_Visi], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_Visi], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmvisi/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmvisi</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmvisi/delete?id=<?= $model->id?>', urlBack:'ta-rpjmvisi/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmvisi</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Visi',
            'Kd_Desa',
            'kd_kecamatan',
            'No_Visi',
            'Uraian_Visi',
            'TahunA',
            'TahunN',
        ],
    ]) ?>

</div>
