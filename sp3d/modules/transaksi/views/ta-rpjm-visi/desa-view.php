<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rpjm-visi-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-visi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RPJM Visi Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RPJM Visi Desa?', urlSend:'/transaksi/ta-rpjm-visi/desa-delete?id=<?=$id?>&kd=<?=$model->No_Visi?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-rpjm-visi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RPJM Visi Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Visi',
            'Kd_Desa',
            'kd_kecamatan',
            'No_Visi',
            'Uraian_Visi',
            'TahunA',
            'TahunN',
        ],
    ]) ?>

</div>
