<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMVisi */

$this->title = 'Update Ta Rpjmvisi: ' . $model->ID_Visi;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmvisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_Visi, 'url' => ['view', 'id' => $model->ID_Visi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmvisi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
