<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMPaguIndikatif */

$this->title = $model->Kd_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmpagu Indikatifs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmpagu-indikatif-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Kd_Keg' => $model->Kd_Keg, 'Kd_Sumber' => $model->Kd_Sumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Kd_Keg' => $model->Kd_Keg, 'Kd_Sumber' => $model->Kd_Sumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmpagu-indikatif/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmpagu Indikatif</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmpagu-indikatif/delete?id=<?= $model->id?>', urlBack:'ta-rpjmpagu-indikatif/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmpagu Indikatif</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Sumber',
            'Tahun1',
            'Tahun2',
            'Tahun3',
            'Tahun4',
            'Tahun5',
            'Tahun6',
            'Pola',
        ],
    ]) ?>

</div>
