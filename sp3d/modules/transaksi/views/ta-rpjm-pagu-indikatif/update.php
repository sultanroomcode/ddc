<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMPaguIndikatif */

$this->title = 'Update Ta Rpjmpagu Indikatif: ' . $model->Kd_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmpagu Indikatifs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Keg, 'url' => ['view', 'Kd_Keg' => $model->Kd_Keg, 'Kd_Sumber' => $model->Kd_Sumber]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmpagu-indikatif-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
