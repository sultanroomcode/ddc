<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMPaguIndikatif */

$this->title = 'Create Ta Rpjmpagu Indikatif';
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmpagu Indikatifs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmpagu-indikatif-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
