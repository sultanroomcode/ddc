<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmpagu Indikatifs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmpagu-indikatif-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmpagu-indikatif/create'})" class="btn btn-warning">Buat Ta Rpjmpagu Indikatif</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Desa',
            'Kd_Keg',
            'Kd_Sumber',
            'Tahun1',
            'Tahun2',
            // 'Tahun3',
            // 'Tahun4',
            // 'Tahun5',
            // 'Tahun6',
            // 'Pola',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
