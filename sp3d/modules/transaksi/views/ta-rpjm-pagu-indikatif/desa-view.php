<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rpjm-pagu-indikatif-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-pagu-indikatif/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RPJM Pagu Indikatif Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RPJM Pagu Indikatif Desa?', urlSend:'/transaksi/ta-rpjm-pagu-indikatif/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>&kd2=<?=$model->Kd_Sumber?>', urlBack:'/transaksi/ta-rpjm-pagu-indikatif/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RPJM Pagu Indikatif Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Sumber',
            'Tahun1',
            'Tahun2',
            'Tahun3',
            'Tahun4',
            'Tahun5',
            'Tahun6',
            'Pola',
        ],
    ]) ?>

</div>
