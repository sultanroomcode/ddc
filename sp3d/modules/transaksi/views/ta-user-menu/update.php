<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserMenu */

$this->title = 'Update Ta User Menu: ' . $model->UserID;
$this->params['breadcrumbs'][] = ['label' => 'Ta User Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->UserID, 'url' => ['view', 'UserID' => $model->UserID, 'IDMenu' => $model->IDMenu]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-user-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
