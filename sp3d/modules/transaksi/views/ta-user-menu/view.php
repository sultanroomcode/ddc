<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserMenu */

$this->title = $model->UserID;
$this->params['breadcrumbs'][] = ['label' => 'Ta User Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'UserID' => $model->UserID, 'IDMenu' => $model->IDMenu], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'UserID' => $model->UserID, 'IDMenu' => $model->IDMenu], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-menu/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta User Menu</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-user-menu/delete?id=<?= $model->id?>', urlBack:'ta-user-menu/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta User Menu</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'UserID',
            'IDMenu',
            'Otoritas',
        ],
    ]) ?>

</div>
