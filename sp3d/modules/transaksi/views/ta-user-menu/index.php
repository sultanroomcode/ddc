<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta User Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-menu/create'})" class="btn btn-warning">Buat Ta User Menu</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'UserID',
            'IDMenu',
            'Otoritas',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
