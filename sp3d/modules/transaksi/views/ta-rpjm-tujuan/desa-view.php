<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rpjm-tujuan-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-tujuan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RPJM Tujuan Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RPJM Tujuan Desa?', urlSend:'/transaksi/ta-rpjm-tujuan/desa-delete?id=<?=$id?>&kd=<?=$model->No_Tujuan?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-rpjm-tujuan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RPJM Tujuan Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Tujuan',
            'Kd_Desa',
            'kd_kecamatan',
            'ID_Misi',
            'No_Tujuan',
            'Uraian_Tujuan',
        ],
    ]) ?>

</div>
