<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMTujuan */

$this->title = $model->ID_Tujuan;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmtujuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmtujuan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_Tujuan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_Tujuan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmtujuan/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmtujuan</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmtujuan/delete?id=<?= $model->id?>', urlBack:'ta-rpjmtujuan/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmtujuan</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Tujuan',
            'Kd_Desa',
            'kd_kecamatan',
            'ID_Misi',
            'No_Tujuan',
            'Uraian_Tujuan',
        ],
    ]) ?>

</div>
