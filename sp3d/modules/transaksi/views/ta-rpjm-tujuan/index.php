<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmtujuans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmtujuan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmtujuan/create'})" class="btn btn-warning">Buat Ta Rpjmtujuan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_Tujuan',
            'Kd_Desa',
            'kd_kecamatan',
            'ID_Misi',
            'No_Tujuan',
            // 'Uraian_Tujuan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
