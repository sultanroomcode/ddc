<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMTujuan */

$this->title = 'Update Ta Rpjmtujuan: ' . $model->ID_Tujuan;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmtujuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_Tujuan, 'url' => ['view', 'id' => $model->ID_Tujuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmtujuan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
