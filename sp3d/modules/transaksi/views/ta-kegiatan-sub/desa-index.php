<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Kegiatan</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/desa-page?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Kegiatan</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-kegiatan-table" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>Kode</th>	            
	            <th>Nama</th>
	            <th>Lokasi</th>
	            <th>Waktu</th>
	            <th>Pagu</th>
	            
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kode</th>
	            <th>Nama</th>
	            <th>Lokasi</th>
	            <th>Waktu</th>
	            <th>Pagu</th>

	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->Kd_Keg ?></td>
	            <td><?= $v->Nama_Kegiatan ?></td>
	            <td><?= $v->Lokasi ?></td>
	            <td><?= $v->Waktu ?></td>
	            <td align="right"><?= number_format(round($v->Pagu), 0, ',','.') ?></td>
	            
	            <td>
	            	<?php //sementara di hidden dulu if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-update')): ?>
	            	<!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>'})" class="label label-warning">Update</a> -->
	            	<?php //endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Kegiatan Desa?', urlSend:'/transaksi/ta-kegiatan/desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>', urlBack:'/transaksi/ta-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>	  
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-kegiatan-table').DataTable();
JS;

$this->registerJs($scripts);