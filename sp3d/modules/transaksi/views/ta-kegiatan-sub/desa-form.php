<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaBidang;
use frontend\models\transaksi\TaKegiatan;
use frontend\models\transaksi\TaTpk;
$bidList = ArrayHelper::map(TaBidang::find()->where(['Kd_Desa' => $id])->all(),'Kd_Bid','Nama_Bidang');
$tpkList = ArrayHelper::map(TaTpk::find()->where(['Kd_Desa' => $id])->all(),'no_urut','nama_tpk');
$smbrList = ArrayHelper::map(RefSumber::find()->all(),'Kode','Nama_Sumber');
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-bidang */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $mroot = TaKegiatan::findOne(['Tahun' => $tahun, 'Kd_Desa' => $id , 'Kd_Keg' => $kd]);
    $model->Tahun = $mroot->Tahun;
    $model->Pagu = $model->Pagu_PAK = 0;
    $model->Kd_Bid = $mroot->Kd_Bid;
    $model->Kd_Desa = $mroot->Kd_Desa;
    $model->ID_Keg = $mroot->ID_Keg;
    $model->Kd_Keg = $mroot->Kd_Keg;
    $model->Nama_Kegiatan = $mroot->Nama_Kegiatan;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $mroot->kd_kecamatan;
    $model->setNewNumber();
}
?>

<div class="ta-bidang-form">
    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Bid')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'ID_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Pagu')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Pagu_PAK')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'No_Keg')->textInput(['maxlength' => true, 'readonly' => true]) ?>
            <?= $form->field($model, 'Nama_Kegiatan')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'Nm_PPK')->dropdownList($tpkList, ['class' => 'form-control selectpickera']) ?>
            <?= $form->field($model, 'Lokasi')->textInput(['maxlength' => true, 'placeholder' => 'Isi lokasi kegiatan dilaksanakan']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'Waktu')->textInput(['maxlength' => true, 'placeholder' => 'Ex : Januari s/d Desember']) ?>

            <?= $form->field($model, 'Keluaran')->textarea(['maxlength' => true , 'placeholder' => 'Masukkan tujuan/hasil yang ingin dicapai']) ?>

            <?= $form->field($model, 'Sumberdana')->dropdownList($smbrList, ['class' => 'form-control selectpickera']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('.selectpicker').selectpicker({
  liveSearch:true
});

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
        } else {
            console.log('Fail but not error');
        }
        Custombox.modal.close();
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);