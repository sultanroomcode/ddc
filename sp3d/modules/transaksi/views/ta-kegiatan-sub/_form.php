<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaKegiatanSub */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ta-kegiatan-sub-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_kecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Bid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama_Kegiatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pagu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pagu_PAK')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nm_PPK')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lokasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Waktu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Keluaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Sumberdana')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'tgl_submit')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
