<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget <?=($effect!=null)?'animated '.$effect:''?>">
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="openModalX({url:'/transaksi/ta-kegiatan-sub/desa-create?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$kd?>', opened:'sub', arr:{content:{effect:'slide', positionY: 'bottom', positionX: 'center'}}})" class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> Sub Kegiatan</a>
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#ta-kegiatan-sub-list', url:'/transaksi/ta-kegiatan-sub/kegiatan-desa?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$kd ?>&effect=slideInRight'})" class="btn btn-sm btn-info"><i class="fa fa-spin fa-refresh"></i> Sub Kegiatan</a><hr>
	
  	<table id="ta-kegiatan-sub-table" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>Kode</th>	            
	            <th>Nama</th>
	            <th>Lokasi</th>
	            <th>Waktu</th>
	            <th>Pagu</th>
	            
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kode</th>
	            <th>Nama</th>
	            <th>Lokasi</th>
	            <th>Waktu</th>
	            <th>Pagu</th>

	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->No_Keg ?></td>
	            <td><?= $v->Nama_Kegiatan ?></td>
	            <td><?= $v->Lokasi ?></td>
	            <td><?= $v->Waktu ?></td>
	            <td align="right"><?= $v->nf($v->Pagu) ?></td>
	            
	            <td>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#ta-kegiatan-belanja-index', url:'/transaksi/ta-rab-rinci/lihat-belanja?id=<?=$v->Kd_Desa?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&nokeg=<?=$v->No_Keg?>&effect=slideInLeft'})" class="label label-success"><i class="fa fa-shopping-cart"></i></a>

	            	<a href="javascript:void(0)" onclick="openModal({url:'/transaksi/ta-kegiatan-sub/desa-update?id=<?=$v->Kd_Desa?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Keg?>&kd=<?=$v->Kd_Keg?>', nokeg:'<?=$v->No_Keg?>', opened:'sub'})" class="label label-info"><i class="fa fa-pencil"></i></a>

	            	<a href="javascript:void(0)" onclick="openModal({url:'/transaksi/ta-kegiatan-sub/desa-view?id=<?=$v->Kd_Desa?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Keg?>&kd=<?=$v->Kd_Keg?>', nokeg:'<?=$v->No_Keg?>', opened:'sub'})" class="label label-warning"><i class="fa fa-eye"></i></a>

	            	<?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="hideZone({zone:'#ta-kegiatan-belanja-index'});goSendLoad({elmChange:'#ta-kegiatan-sub-list',msg:'Hapus Data Sub Kegiatan Desa?', urlSend:'/transaksi/ta-kegiatan-sub/desa-delete?no=<?=$v->No_Keg?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>', urlBack:'/transaksi/ta-kegiatan-sub/kegiatan-desa?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$v->Kd_Keg?>&effect=rubberBand', typeSend:'get'})" class="label label-danger"><i class="fa fa-trash"></i></a>
	            	<?php endif; ?>	  
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-kegiatan-sub-table').DataTable();
JS;

$this->registerJs($scripts);