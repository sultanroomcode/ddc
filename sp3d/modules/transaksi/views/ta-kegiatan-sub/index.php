<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Kegiatan Subs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-kegiatan-sub-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ta Kegiatan Sub', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Bid',
            'Kd_Keg',
            // 'No_Keg',
            // 'ID_Keg',
            // 'Nama_Kegiatan',
            // 'Pagu',
            // 'Pagu_PAK',
            // 'Nm_PPK',
            // 'Lokasi',
            // 'Waktu',
            // 'Keluaran',
            // 'Sumberdana',
            // 'user_id',
            // 'tgl_submit',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
