<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaKegiatanSub */

$this->title = 'Update Ta Kegiatan Sub: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Kegiatan Subs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg, 'No_Keg' => $model->No_Keg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-kegiatan-sub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
