<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaKegiatanSub */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Kegiatan Subs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-kegiatan-sub-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg, 'No_Keg' => $model->No_Keg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg, 'No_Keg' => $model->No_Keg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Bid',
            'Kd_Keg',
            'No_Keg',
            'ID_Keg',
            'Nama_Kegiatan',
            'Pagu',
            'Pagu_PAK',
            'Nm_PPK',
            'Lokasi',
            'Waktu',
            'Keluaran',
            'Sumberdana',
            'user_id',
            'tgl_submit',
        ],
    ]) ?>

</div>
