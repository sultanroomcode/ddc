<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<!-- Box Comment -->
<div class="box box-widget">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Sub Kegiatan</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <h4>Deskripsi Kegiatan</h4>
        <div class="col-md-6">
            <b>Tahun :</b><br><?= $model->Tahun ?><br>
            <b>Rekening Kegiatan :</b><br><?= $model->ID_Keg ?><br>
            <b>Nama Kegiatan :</b><br><?= $model->Nama_Kegiatan ?><br>
            <b>Pagu :</b><br><?= $model->nf($model->Pagu) ?><br>
            <b>Pagu (PAK) :</b><br><?= $model->nf($model->Pagu_PAK) ?><br>
        </div>
        <div class="col-md-6">
            <b>Lokasi :</b><br><?= $model->Lokasi ?><br>
            <b>Waktu :</b><br><?= $model->Waktu ?><br>
            <b>Keluaran:</b><br><?= $model->Keluaran ?><br>
            <b>Sumber Dana:</b><br><?= $model->Sumberdana ?><br>
        </div>
    </div>   
</div>
</div>