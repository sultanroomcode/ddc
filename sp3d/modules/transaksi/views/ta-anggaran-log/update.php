<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaranLog */

$this->title = 'Update Ta Anggaran Log: ' . $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggaran Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->KdPosting, 'url' => ['view', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-anggaran-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
