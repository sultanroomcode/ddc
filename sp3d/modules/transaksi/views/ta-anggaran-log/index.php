<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Anggaran Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran-log/create'})" class="btn btn-warning">Buat Ta Anggaran Log</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'KdPosting',
            'Tahun',
            'Kd_Desa',
            'No_Perdes',
            'TglPosting',
            // 'UserID',
            // 'Kunci',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
