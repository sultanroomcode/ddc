<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaranLog */

$this->title = $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggaran Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran-log/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Anggaran Log</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-anggaran-log/delete?id=<?= $model->id?>', urlBack:'ta-anggaran-log/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Anggaran Log</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KdPosting',
            'Tahun',
            'Kd_Desa',
            'No_Perdes',
            'TglPosting',
            'UserID',
            'Kunci',
        ],
    ]) ?>

</div>
