<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-tpk-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tpk/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List TPK</a>
        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data TPK Desa?', urlSend:'/transaksi/ta-tpk/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&no_urut=<?=$model->no_urut?>', urlBack:'/transaksi/ta-tpk/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus TPK</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kecamatan',
            'kd_desa',
            'no_urut',
            'nama_tpk',
            'alamat_tpk',
            'no_hp_tpk',
            'tipe_tpk',
        ],
    ]) ?>

</div>
