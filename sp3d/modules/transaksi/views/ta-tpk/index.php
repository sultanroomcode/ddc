<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Tpks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-tpk-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ta Tpk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_kecamatan',
            'kd_desa',
            'no_urut',
            'nama_tpk',
            'alamat_tpk',
            // 'no_hp_tpk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
