<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTpk */

$this->title = 'Update Ta Tpk: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Ta Tpks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'no_urut' => $model->no_urut]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-tpk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
