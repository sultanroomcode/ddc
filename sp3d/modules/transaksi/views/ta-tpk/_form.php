<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTpk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ta-tpk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_kecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_urut')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_tpk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_tpk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_hp_tpk')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
