<?php
use yii\helpers\Url;
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel TPK</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/perencanaan-desa?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-tpk/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tpk/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah TPK</a>
	<?php endif; ?>
	<hr>
  	
  	<table id="ta-tpk-table" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>No</th>
	            <th>Nama</th>
	            <th>Alamat</th>
	            <th>No. HP</th>
	            <th>Tipe</th>

	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>No</th>
	            <th>Nama</th>
	            <th>Alamat</th>
	            <th>No. HP</th>
	            <th>Tipe</th>
	            
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->no_urut ?></td>
	            <td><?= $v->nama_tpk ?></td>
	            <td><?= $v->alamat_tpk ?></td>
	            <td><?= $v->no_hp_tpk ?></td>
	            <td><?= $v->tipe_tpk ?></td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-tpk/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tpk/desa-update?id=<?=$id?>&no_urut=<?=$v->no_urut?>&tahun=<?=$tahun?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-tpk/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data TPK Desa?', urlSend:'/transaksi/ta-tpk/desa-delete?id=<?=$id?>&no_urut=<?=$v->no_urut?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-tpk/desa-index?id=<?=$id?>&no_urut=<?=$v->no_urut?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>	            	

	            	<?php if(Mimin::checkRoute('transaksi/ta-tpk/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tpk/desa-view?id=<?=$id?>&no_urut=<?=$v->no_urut?>&tahun=<?=$tahun?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-tpk-table').DataTable();
JS;

$this->registerJs($scripts);