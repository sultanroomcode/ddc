<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-pajak */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */

if($model->isNewRecord){
    $model->kd_desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->setNewNumber();
}
?>

<div class="ta-pajak-form animated slideInUp">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tpk/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">Daftar TPK</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'no_urut')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'nama_tpk')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'no_hp_tpk')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'alamat_tpk')->textarea(['maxlength' => true]) ?>

            <?= $form->field($model, 'tipe_tpk')->dropdownList(['operasional' => 'Operasional', 'kegiatan' => 'Kegiatan'], ['class' => 'selectpicker form-control']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
//regularly-ajax
$('.selectpicker').selectpicker({
  liveSearch:true
});

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-tpk/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);