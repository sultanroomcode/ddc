<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRABSub */

$this->title = 'Create Ta Rabsub';
$this->params['breadcrumbs'][] = ['label' => 'Ta Rabsubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rabsub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
