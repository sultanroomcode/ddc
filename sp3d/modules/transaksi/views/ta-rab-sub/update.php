<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRABSub */

$this->title = 'Update Ta Rabsub: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rabsubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_SubRinci' => $model->Kd_SubRinci]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rabsub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
