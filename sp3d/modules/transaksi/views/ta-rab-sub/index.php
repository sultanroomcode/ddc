<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rabsubs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rabsub-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rabsub/create'})" class="btn btn-warning">Buat Ta Rabsub</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Rincian',
            'Kd_SubRinci',
            // 'Nama_SubRinci',
            // 'Anggaran',
            // 'AnggaranPAK',
            // 'AnggaranStlhPAK',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
