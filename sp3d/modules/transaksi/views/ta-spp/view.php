<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPP */

$this->title = $model->No_SPP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_SPP], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_SPP], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spp/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spp</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spp/delete?id=<?= $model->id?>', urlBack:'ta-spp/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spp</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_SPP',
            'Tgl_SPP',
            'Jn_SPP',
            'Kd_Desa',
            'kd_kecamatan',
            'Keterangan',
            'Jumlah',
            'Potongan',
            'Status',
        ],
    ]) ?>

</div>
