<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPP */

$this->title = 'Update Ta Spp: ' . $model->No_SPP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SPP, 'url' => ['view', 'id' => $model->No_SPP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
