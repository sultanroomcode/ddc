<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-spp-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List SPP Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data SPP Desa?', urlSend:'/transaksi/ta-spp/desa-delete?id=<?=$id?>&kd=<?=$model->No_SPP?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-spp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus SPP Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_SPP',
            'Tgl_SPP',
            'Jn_SPP',
            'Kd_Desa',
            'kd_kecamatan',
            'Keterangan',
            'Jumlah',
            'Potongan',
            'Status',
        ],
    ]) ?>

</div>
