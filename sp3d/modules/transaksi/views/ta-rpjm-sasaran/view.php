<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMSasaran */

$this->title = $model->ID_Sasaran;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmsasarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmsasaran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_Sasaran], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_Sasaran], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmsasaran/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmsasaran</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmsasaran/delete?id=<?= $model->id?>', urlBack:'ta-rpjmsasaran/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmsasaran</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Sasaran',
            'Kd_Desa',
            'kd_kecamatan',
            'ID_Tujuan',
            'No_Sasaran',
            'Uraian_Sasaran',
        ],
    ]) ?>

</div>
