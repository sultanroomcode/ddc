<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rpjm-sasaran-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-sasaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RPJM Pagu Tahunan Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RPJM Pagu Tahunan Desa?', urlSend:'/transaksi/ta-rpjm-sasaran/desa-delete?id=<?=$id?>&kd=<?=$model->No_Sasaran?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-rpjm-sasaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RPJM Pagu Tahunan Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Sasaran',
            'Kd_Desa',
            'kd_kecamatan',
            'ID_Tujuan',
            'No_Sasaran',
            'Uraian_Sasaran',
        ],
    ]) ?>

</div>
