<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmsasarans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmsasaran-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmsasaran/create'})" class="btn btn-warning">Buat Ta Rpjmsasaran</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_Sasaran',
            'Kd_Desa',
            'kd_kecamatan',
            'ID_Tujuan',
            'No_Sasaran',
            // 'Uraian_Sasaran',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
