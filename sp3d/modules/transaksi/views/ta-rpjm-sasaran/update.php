<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMSasaran */

$this->title = 'Update Ta Rpjmsasaran: ' . $model->ID_Sasaran;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmsasarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_Sasaran, 'url' => ['view', 'id' => $model->ID_Sasaran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmsasaran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
