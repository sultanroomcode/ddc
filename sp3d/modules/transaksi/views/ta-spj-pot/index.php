<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Spjpots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjpot-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjpot/create'})" class="btn btn-warning">Buat Ta Spjpot</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPJ',
            'Kd_Keg',
            // 'No_Bukti',
            // 'Kd_Rincian',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
