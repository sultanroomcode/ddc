<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJPot */

$this->title = $model->No_SPJ;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjpots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjpot-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_SPJ' => $model->No_SPJ, 'Kd_Keg' => $model->Kd_Keg, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_SPJ' => $model->No_SPJ, 'Kd_Keg' => $model->Kd_Keg, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjpot/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spjpot</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spjpot/delete?id=<?= $model->id?>', urlBack:'ta-spjpot/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spjpot</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPJ',
            'Kd_Keg',
            'No_Bukti',
            'Kd_Rincian',
            'Nilai',
        ],
    ]) ?>

</div>
