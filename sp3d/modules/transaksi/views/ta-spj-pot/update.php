<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJPot */

$this->title = 'Update Ta Spjpot: ' . $model->No_SPJ;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjpots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SPJ, 'url' => ['view', 'No_SPJ' => $model->No_SPJ, 'Kd_Keg' => $model->Kd_Keg, 'No_Bukti' => $model->No_Bukti, 'Kd_Rincian' => $model->Kd_Rincian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spjpot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
