<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang 

<a href="javascript:void(0)" onclick="goHiatus({elm:'#bidang-index-form', url:'/transaksi/ta-kegiatan/desa-isi-kegiatan?id=<?=$id?>&tahun=<?=$tahun?>&kd_bid=<?=$model->Kd_Bid?>', state:'show'})" class="btn btn-info btn-sm">Buat Kegiatan Baru</a><br>

*/
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInRight">
<div class="box-body">
    <?php if($model->count() != 0){  ?><!-- 
    <a href="<?= Url::to(['/data-umum/pdf?bagian=spm-kolektif-desa&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-file-pdf-o"></i> SPM Kolektif</a> -->
    <br><br>
    <table id="ta-kegiatan-in-bidang-table" class="compact row-border" width="100%">
        <thead>
            <tr>
                <th>Kode</th>               
                <th>Nama</th>
                <th>Pagu</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th>Pagu</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($model->all() as $vk): ?>
            <tr>
                <td><?= $vk->ID_Keg ?></td>
                <td><a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/daftar-belanja?tahun=<?=$vk->Tahun?>&kdrincian=<?=$vk->Kd_Keg?>'})"><?= $vk->Nama_Kegiatan ?></a></td>
                <td align="right"><?= $vk->nf($vk->Pagu) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
</div>
<?php
$scripts =<<<JS
    $('#ta-kegiatan-in-bidang-table').DataTable();
JS;

$this->registerJs($scripts);

    } else { ?>
    Kegiatan di dalam bidang ini masih kosong<br><!-- <a href="javascript:void(0)" onclick="goHiatus({elm:'#bidang-index-form', url:'/transaksi/ta-kegiatan/desa-isi-kegiatan?id=<?=$id?>&tahun=<?=$tahun?>&kd_bid=<?=$id?>01', state:'show'})" class="btn btn-info btn-sm">Klik Untuk Membuat Kegiatan Baru</a> -->
    <?php } ?>