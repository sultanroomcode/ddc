<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang 

<a href="javascript:void(0)" onclick="goHiatus({elm:'#bidang-index-form', url:'/transaksi/ta-kegiatan/desa-isi-kegiatan?id=<?=$id?>&tahun=<?=$tahun?>&kd_bid=<?=$model->Kd_Bid?>', state:'show'})" class="btn btn-info btn-sm">Buat Kegiatan Baru</a><br>

*/
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInRight">
<!-- /.box-header -->
<div class="box-body">
    <h3><?= substr($model->Kd_Bid, 6). ' - '.$model->Nama_Bidang ?></h3>
    <?php if($model->kegiatan != null){  ?>
    <a href="<?= Url::to(['/data-umum/pdf?bagian=spm-kolektif-desa&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-sm btn-info">SPM Kolektif</a>
    <table id="ta-kegiatan-in-bidang-table" class="display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>Kode</th>               
                <th>Nama</th>
                <th>Pagu</th>
                
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th>Pagu</th>

                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($model->kegiatan as $vk): ?>
            <tr>
                <td><?= $vk->ID_Keg ?></td>
                <td><?= $vk->Nama_Kegiatan ?></td>
                <td align="right"><?= $vk->nf($vk->Pagu) ?></td>
                
                <td>
                    <?php //sementara di hidden dulu if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-update')): ?>
                    <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-update?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>'})" class="label label-warning">Update</a> -->
                    <?php //endif; ?>

                    <a href="<?= Url::to(['/data-umum/pdf?bagian=spm-desa&kd_desa='.$id.'&kode='.$vk->Kd_Keg], true) ?>" target="_blank" class="label label-info">SPM</a>

                    <?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-view')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-view?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>'})" class="label label-warning"><i class="fa fa-eye"></i> View</a>
                    <?php endif; ?>

                    <?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#bidang-index-dash',msg:'Hapus Data Kegiatan Desa?', urlSend:'/transaksi/ta-kegiatan/desa-delete?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>', urlBack:'/transaksi/ta-bidang/desa-view-auto?kd=<?=substr($model->Kd_Bid, 6)?>&id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'}); goHiatus({elm:'#bidang-index-form', state:'hide'})" class="label label-danger"><i class="fa fa-trash"></i> Hapus</a>
                    <?php endif; ?>   
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
</div>
<?php
$scripts =<<<JS
    $('#ta-kegiatan-in-bidang-table').DataTable();
JS;

$this->registerJs($scripts);

    } else { ?>
    Kegiatan di dalam bidang ini masih kosong<br><a href="javascript:void(0)" onclick="goHiatus({elm:'#bidang-index-form', url:'/transaksi/ta-kegiatan/desa-isi-kegiatan?id=<?=$id?>&tahun=<?=$tahun?>&kd_bid=<?=$model->Kd_Bid?>', state:'show'})" class="btn btn-info btn-sm">Klik Untuk Membuat Kegiatan Baru</a>
    <?php } ?>