<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Bidangs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-bidang-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-bidang/create'})" class="btn btn-warning">Buat Ta Bidang</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'Kd_Bid',
            'Nama_Bidang',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
