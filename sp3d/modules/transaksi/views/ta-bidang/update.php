<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */

$this->title = 'Update Ta Bidang: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Bidangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Bid' => $model->Kd_Bid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-bidang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
