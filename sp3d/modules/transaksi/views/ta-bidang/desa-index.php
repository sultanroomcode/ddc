<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInRight">
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#bidang-index-dash', url:'/transaksi/ta-bidang/desa-view-all?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning"><i class="fa fa-spin fa-rotate-right"></i> <i class="fa fa-list"></i></a>
  	<!-- <a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/desa-main-form?id=<?=$id?>&tahun=<?=$tahun?>&automate=true'})" class="btn btn-sm btn-primary"><i class="fa fa-plus-square"></i></a> 
  	<a href="javascript:void(0)" onclick="openModalXy({url:'/transaksi/ta-rab/desa-visual?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-success"><i class="fa fa-pie-chart"></i></a>-->
	<!-- <a href="<?= Url::to(['/data-umum/pdf?bagian=data-bidang&kd_desa='.$id], true) ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-file-pdf-o"></i></a> -->

	<div class="row">
		<div class="col-md-12">
			<div id="bidang-index-dash"></div>
		</div>
	</div>
</div>
</div>
<!-- /.box -->

<?php
$runbackview = ($runback == true)?'running':'stop';
$scripts =<<<JS
	goLoad({elm:'#bidang-index-dash', url:'/transaksi/ta-bidang/desa-view-all?id={$id}&tahun={$tahun}'})
JS;

$this->registerJs($scripts);