<?php
use frontend\models\referensi\RefBidang;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bidangList = ArrayHelper::map(RefBidang::find()->all(),'kode','nama');
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-bidang */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Nama_Bidang = 'Bidang Penyelenggaraan Pemerintah Desa';
} else {
	$model->Kd_Bid = substr($model->Kd_Bid, -2,2);
}
?>

<div class="ta-bidang-form animated slideInLeft">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-warning">List Bidang</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'Kd_Bid')->dropdownList($bidangList) ?>

    <?= $form->field($model, 'Nama_Bidang')->hiddenInput(['maxlength' => true])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
//regularly-ajax
$('#tabidang-kd_bid').change(function(){
	var vl = $('#tabidang-kd_bid option:selected').text();
	$('#tabidang-nama_bidang').val(vl);
});
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-bidang/desa-index?id={$id}&tahun={$tahun}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

JS;

$this->registerJs($script);