<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="box box-widget animated slideInRight">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description"><?= $model->Nama_Bidang ?></span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-warning">List Bidang</a>

        <?php if(Mimin::checkRoute('transaksi/ta-bidang/desa-delete')): ?>
        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus ? ini akan menghapus seluruh data terkait dengan Bidang', urlSend:'/transaksi/ta-bidang/desa-delete?id=<?=$id?>&kd=<?=$model->Kd_Bid?>&tahun=<?=$model->Tahun?>', urlBack:'/transaksi/ta-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>', typeSend:'get'})" class="btn btn-danger">Hapus</a>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Kd_Bid',
                'value' => function($model){
                    return substr($model->Kd_Bid, 6);
                }
            ],
            'Nama_Bidang',
        ],
    ]) ?>

    <h3>Daftar Kegiatan</h3>
    <?php if($model->kegiatan != null){  ?>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-isi-kegiatan?id=<?=$id?>&tahun=<?=$tahun?>&kd_bid=<?=$model->Kd_Bid?>'})" class="btn btn-info btn-sm">Buat Kegiatan Baru</a><br>

    <table id="ta-kegiatan-in-bidang-table" class="display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>Kode</th>               
                <th>Nama</th>
                <th>Lokasi</th>
                <th>Waktu</th>
                <th>Pagu</th>
                
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th>Lokasi</th>
                <th>Waktu</th>
                <th>Pagu</th>

                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($model->kegiatan as $vk): ?>
            <tr>
                <td><?= $vk->ID_Keg ?></td>
                <td><?= $vk->Nama_Kegiatan ?></td>
                <td><?= $vk->Lokasi ?></td>
                <td><?= $vk->Waktu ?></td>
                <td align="right"><?= number_format(round($vk->Pagu), 0, ',','.') ?></td>
                
                <td>
                    <?php //sementara di hidden dulu if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-update')): ?>
                    <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-update?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>'})" class="label label-warning">Update</a> -->
                    <?php //endif; ?>

                    <?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-view')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-view?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>'})" class="label label-warning">View</a>
                    <?php endif; ?>

                    <?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Kegiatan Desa?', urlSend:'/transaksi/ta-kegiatan/desa-delete?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>', urlBack:'/transaksi/ta-bidang/desa-view?kd=<?=$model->Kd_Bid?>&id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
                    <?php endif; ?>   
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php
$scripts =<<<JS
    $('#ta-kegiatan-in-bidang-table').DataTable();
JS;

$this->registerJs($scripts);

    } else { ?>
    Kegiatan di dalam bidang ini masih kosong<br><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-isi-kegiatan?id=<?=$id?>&tahun=<?=$tahun?>&kd_bid=<?=$model->Kd_Bid?>'})" class="btn btn-info btn-sm">Klik Untuk Membuat Kegiatan Baru</a>
    <?php } ?>
    </div>
</div>
