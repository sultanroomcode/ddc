<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Kegiatans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-kegiatan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-kegiatan/create'})" class="btn btn-warning">Buat Ta Kegiatan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'Kd_Bid',
            'Kd_Keg',
            'ID_Keg',
            // 'Nama_Kegiatan',
            // 'Pagu',
            // 'Pagu_PAK',
            // 'Nm_PPTKD',
            // 'NIP_PPTKD',
            // 'Lokasi',
            // 'Waktu',
            // 'Keluaran',
            // 'Sumberdana',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
