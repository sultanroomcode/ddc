<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaBidang;
$bidList = ArrayHelper::map(TaBidang::find()->where(['Kd_Desa' => $id])->all(),'Kd_Bid','Nama_Bidang');
$smbrList = ArrayHelper::map(RefSumber::find()->all(),'Kode','Nama_Sumber');
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-bidang */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    //Kd_Keg = kd_bid + 
    //ID_Keg
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
}
?>

<div class="ta-bidang-form animated slideInUp">
    <?php if(count($bidList) > 0){ ?>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Kegiatan</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'Kd_Bid')->dropdownList($bidList, ['onchange' => 'cariKegiatan()']) ?>

            <?= $form->field($model, 'ID_Keg')->dropdownList(['' => '---'], ['onchange' => 'updateKodeKegiatan()']) ?>

            <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true, 'readonly' => true]) ?>
            <span id="info-status"></span>
            <?= $form->field($model, 'Nama_Kegiatan')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php if($model->isNewRecord){ ?>
            <?= $form->field($model, 'Pagu')->textInput() ?>
            <?= $form->field($model, 'Pagu_PAK')->textInput() ?>
            <?php } ?>

            <?= $form->field($model, 'Nm_PPTKD')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'NIP_PPTKD')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Lokasi')->textInput(['maxlength' => true, 'placeholder' => 'Isi lokasi kegiatan dilaksanakan']) ?>

            <?= $form->field($model, 'Waktu')->textInput(['maxlength' => true, 'placeholder' => 'Ex : Januari s/d Desember']) ?>

            <?= $form->field($model, 'Keluaran')->textarea(['maxlength' => true , 'placeholder' => 'Masukkan tujuan/hasil yang ingin dicapai']) ?>

            <?= $form->field($model, 'Sumberdana')->dropdownList($smbrList) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php
    //KEGIATAN CHECK
    } else {
        ?>
        <h2>Mohon isi Bidang dulu</h2>
        Bidang dibutuhkan untuk mengisi data Kegiatan. Untuk membuat Bidang <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-bidang/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-primary btn-sm">klik disini</a>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-warning">Kembali Ke Halaman Kegiatan</a>
        <?php
    }
    ?>
    <br>
    <br>
    <br>

</div>
<?php $script = <<<JS
function cekKegiatan(){
    var urlid = $('#takegiatan-kd_keg').val();
    $.post(base_url+'/data-umum/transaksi-kegiatan?kid='+urlid+'&form=1&check=1').done(function(res){
        $('#info-status').html(res);
    });
}

function cariKegiatan(){
    var urlid = $('#takegiatan-kd_bid').val();
    $.post(base_url+'/data-umum/referensi-kegiatan?bid='+urlid+'&form=1&parsing=1').done(function(res){
        $('#takegiatan-id_keg').html(res);
    });
}

function updateKodeKegiatan(){
    var kd1 = $('#takegiatan-kd_desa').val();
    var kd2 = $('#takegiatan-id_keg').val();
    var kd3 = $('#takegiatan-id_keg option:selected').text();
    $('#takegiatan-kd_keg').val(kd1+kd2);
    $('#takegiatan-nama_kegiatan').val(kd3);
    cekKegiatan();
}

cariKegiatan();
updateKodeKegiatan();

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-kegiatan/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);