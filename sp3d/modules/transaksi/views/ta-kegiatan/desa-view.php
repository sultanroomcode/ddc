<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>

<!-- Box Comment -->
<div class="box box-widget animated slideInRight">
<div class="box-body">
    <!-- post text -->
     <a href="javascript:void(0)" onclick="goLoad({elm:'#bidang-index-dash', url:'/transaksi/ta-kegiatan/desa-view?id=<?=$id?>&tahun=<?=$model->Tahun?>&kd=<?=$model->Kd_Keg?>'})" class="btn btn-sm btn-info"><i class="fa fa-spin fa-refresh"></i> Kegiatan</a>

    <div class="row">
        
        <div class="col-md-6">
            <h4>Deskripsi Kegiatan</h4>
            <b>Tahun :</b><br><?= $model->Tahun ?><br>
            <b>Rekening Kegiatan :</b><br><?= $model->ID_Keg ?><br>
            <b>Nama Kegiatan :</b><br><?= $model->Nama_Kegiatan ?><br>
            <b>Pagu :</b><br><?= $model->nf($model->Pagu) ?><br>
            <b>Pagu (PAK) :</b><br><?= $model->nf($model->Pagu_PAK) ?>
        </div>
        <div class="col-md-6">
            <div class="well">
                <?php if($model->rab != null){ ?>
                <h4>Deskripsi Belanja</h4>                
                        <b>Anggaran :</b><br><?= $model->nf($model->rab->Anggaran) ?><br>
                        <b>Anggaran (PAK) :</b><br><?= $model->nf($model->rab->AnggaranPAK) ?><br>
                        <b>Anggaran + PAK :</b><br><?= $model->nf($model->rab->AnggaranStlhPAK) ?>
                <?php } else { ?>
                
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#bidang-index-form', url:'/data-umum/create-object?kd_desa=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>'})" class="btn btn-sm btn-warning">Buat Objek Anggaran Belanja</a>
                
                <?php } ?>
            </div>
        </div>
        <div class="col-md-12">
            <div id="ta-kegiatan-sub-list"></div>
            <div id="ta-kegiatan-belanja-index"></div>
        </div>
    </div>   
</div>
</div>

<div id="modal-custom" style="display: none;">
    <div style="width: 700px; background: #fff; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine-x">
    </div>
</div>

<?php 
$script =<<<JS
var opened;
var nokeg;
var modalx = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-x',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        switch(opened){
            case 'sub':
                goLoad({elm:'#ta-kegiatan-sub-list', url:'/transaksi/ta-kegiatan-sub/kegiatan-desa?id={$model->Kd_Desa}&tahun={$model->Tahun}&kd={$model->Kd_Keg}'});
            break;
            case 'belanja':
                goLoad({elm:'#ta-kegiatan-belanja-index', url:'/transaksi/ta-rab-rinci/lihat-belanja?id={$model->Kd_Desa}&tahun={$model->Tahun}&kd={$model->Kd_Keg}&nokeg='+nokeg})
            break;
        }
    }
  },
  overlay:{
    color:'#0f0'
  }
});

goLoad({elm:'#ta-kegiatan-belanja-index', url:'/transaksi/ta-rab-rinci/lihat-belanja?id={$model->Kd_Desa}&tahun={$model->Tahun}&kd={$model->Kd_Keg}&nokeg='+nokeg});

function hideZone(o)
{
    $(o.zone).html('::');
}
// Open
function openModalX(o){
    opened = o.opened;
    
    nokeg = o.nokeg;
    $('#form-engine-x').load(base_url+o.url);

    modalx.open(o.arr);    
}

JS;

$this->registerJs($script);

        