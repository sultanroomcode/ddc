<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaBidang;
$bidList = ArrayHelper::map(TaBidang::find()->where(['Kd_Desa' => $id])->all(),'Kd_Bid','Nama_Bidang');
$kode_bidang_back = substr($kd_bid, 6);

$formatCurrency = [
                'options' => [
                    'onchange' => 'updateVal()',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'alias' => 'integer',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ];
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-bidang */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    $model->Kd_Bid = $kd_bid;
    $model->Pagu = $model->Pagu_PAK = 0;
    //Kd_Keg = kd_bid + 
    //ID_Keg
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
}
?>

<div class="ta-bidang-form animated slideInLeft">
    <?php if(count($bidList) > 0){ ?>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-12">
            <!-- <b>Bidang :</b><br>
            <?= substr($model->Kd_Bid, 6). ' - '.$model->bidang->Nama_Bidang ?><br> -->

            <?= $form->field($model, 'Kd_Bid')->hiddenInput(['onload' => 'updateKodeKegiatan()'])->label(false) ?>
            <?= $form->field($model, 'ID_Keg')->dropdownList(['' => '---'], ['onchange' => 'updateKodeKegiatan()','class' => 'selectpicker form-control']) ?>

            <?= $form->field($model, 'Kd_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
            <span id="info-status"></span>
            <?= $form->field($model, 'Nama_Kegiatan')->hiddenInput(['maxlength' => true])->label(false) ?>
        
            <?php if($model->isNewRecord){ ?>
            <?= $form->field($model, 'Pagu')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'Pagu_PAK')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a href="javascript:void(0)" onclick="goHiatus({elm:'#ta-belanja-main-form', state:'hide'})" class="btn btn-warning">Batal</a>
    </div>

    <?php ActiveForm::end(); ?>
    <?php
    //KEGIATAN CHECK
    } else {
        ?>
        <h2>Mohon isi Bidang dulu</h2>
        Bidang dibutuhkan untuk mengisi data Kegiatan. Untuk membuat Bidang <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-bidang/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-primary btn-sm">klik disini</a>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-warning">Kembali Ke Halaman Kegiatan</a>
        <?php
    }
    ?>
    <br>
    <br>
    <br>

</div>
<?php 
$prog = '';
if($kd_prog != null){
    $prog = '&prog='.$kd_prog;
}

$script = <<<JS
function updateVal(){
    var s1 = $('#takegiatan-pagumask').val().replace(/\./g, "");
    var s2 = $('#takegiatan-pagu_pakmask').val().replace(/\./g, "");

    $('#takegiatan-pagu').val(s1);    
    $('#takegiatan-pagu_pak').val(s2);
}

function cekKegiatan(){
    var urlid = $('#takegiatan-id_keg').val();
    var kd_desa = $('#takegiatan-kd_desa').val();
    $.post(base_url+'/data-umum/transaksi-kegiatan?kid='+urlid+'&kd_desa='+kd_desa+'&form=1&check=1').done(function(res){
        $('#info-status').html(res);
    });
}

function cariKegiatan(){
    var urlid = $('#takegiatan-kd_bid').val();
    $.post(base_url+'/data-umum/referensi-kegiatan?bid='+urlid+'&form=1&parsing=1{$prog}').done(function(res){
        $('#takegiatan-id_keg').html(res);
        $('.selectpicker').selectpicker({
          liveSearch:true
        });
        updateKodeKegiatan();
    });
}

function updateKodeKegiatan(){
    var kd1 = $('#takegiatan-kd_desa').val();
    var kd2 = $('#takegiatan-id_keg').val();
    var kd3 = $('#takegiatan-id_keg option:selected').text();
    $('#takegiatan-kd_keg').val(kd1+kd2);
    $('#takegiatan-nama_kegiatan').val(kd3);
    cekKegiatan();
}

cariKegiatan();
//updateKodeKegiatan();

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            //var kd = $('#takegiatan-kd_keg').val();
            goLoad({elm:'#bidang-index-dash', url:'/transaksi/ta-bidang/desa-view-all?id={$id}&tahun={$tahun}'})
            //goLoad({elm:'#desa-container',url : '/transaksi/ta-kegiatan/desa-view?id={$id}&tahun={$tahun}&kd='+kd+'&effect=lightSpeedIn'});
            // goLoad({elm:'#bidang-index-dash',url : '/transaksi/ta-bidang/desa-view-auto?id={$id}&tahun={$tahun}&kd={$kode_bidang_back}&effect=lightSpeedIn'});
            goHiatus({elm:'#ta-belanja-main-form', state:'hide'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);