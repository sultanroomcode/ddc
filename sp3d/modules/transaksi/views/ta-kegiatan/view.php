<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaKegiatan */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-kegiatan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-kegiatan/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Kegiatan</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-kegiatan/delete?id=<?= $model->id?>', urlBack:'ta-kegiatan/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Kegiatan</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'Kd_Bid',
            'Kd_Keg',
            'ID_Keg',
            'Nama_Kegiatan',
            'Pagu',
            'Pagu_PAK',
            'Nm_PPTKD',
            'NIP_PPTKD',
            'Lokasi',
            'Waktu',
            'Keluaran',
            'Sumberdana',
        ],
    ]) ?>

</div>
