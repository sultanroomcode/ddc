<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-mutasi-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Mutasi Desa</a>
        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Mutasi Desa?', urlSend:'/transaksi/ta-mutasi/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&no=<?=$model->No_Bukti?>', urlBack:'/transaksi/ta-mutasi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus Mutasi</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'No_Bukti',
            'Tgl_Bukti',
            'Keterangan',
            'Kd_Bank',
            'Kd_Rincian',
            'Kd_Keg',
            'Sumberdana',
            'Kd_Mutasi',
            'Nilai',
        ],
    ]) ?>

</div>
