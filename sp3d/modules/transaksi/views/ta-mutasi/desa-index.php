<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Mutasi</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/penata-usahaan-desa?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-mutasi/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Mutasi</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-mutasi-table" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>No</th>
	            <th>Tanggal</th>
	            <th>Sumber</th>
	            <th>Kode</th>
	            <th>Nilai</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>No</th>
	            <th>Tanggal</th>
	            <th>Sumber</th>
	            <th>Kode</th>
	            <th>Nilai</th>
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->No_Bukti ?></td>
	            <td><?= $v->Tgl_Bukti ?></td>
	            <td><?= $v->Sumberdana ?></td>
	            <td><?= $v->Kd_Mutasi ?></td>
	            <td align="right"><?= number_format(round($v->Nilai), 0, ',','.') ?> </td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-mutasi/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Bukti?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-mutasi/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Mutasi Desa?', urlSend:'/transaksi/ta-mutasi/desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Bukti?>', urlBack:'/transaksi/ta-mutasi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>	            	

	            	<?php if(Mimin::checkRoute('transaksi/ta-mutasi/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-mutasi/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Bukti?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-mutasi-table').DataTable();
JS;

$this->registerJs($scripts);