<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaMutasi */

$this->title = 'Update Ta Mutasi: ' . $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Mutasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_Bukti, 'url' => ['view', 'id' => $model->No_Bukti]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-mutasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
