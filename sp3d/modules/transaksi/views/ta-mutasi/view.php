<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaMutasi */

$this->title = $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Mutasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-mutasi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_Bukti], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_Bukti], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-mutasi/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Mutasi</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-mutasi/delete?id=<?= $model->id?>', urlBack:'ta-mutasi/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Mutasi</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'No_Bukti',
            'Tgl_Bukti',
            'Keterangan',
            'Kd_Bank',
            'Kd_Rincian',
            'Kd_Keg',
            'Sumberdana',
            'Kd_Mutasi',
            'Nilai',
        ],
    ]) ?>

</div>
