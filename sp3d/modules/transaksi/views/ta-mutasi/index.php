<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Mutasis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-mutasi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-mutasi/create'})" class="btn btn-warning">Buat Ta Mutasi</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'No_Bukti',
            'Tgl_Bukti',
            'Keterangan',
            // 'Kd_Bank',
            // 'Kd_Rincian',
            // 'Kd_Keg',
            // 'Sumberdana',
            // 'Kd_Mutasi',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
