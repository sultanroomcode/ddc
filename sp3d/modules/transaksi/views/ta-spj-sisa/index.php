<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Spjsisas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjsisa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjsisa/create'})" class="btn btn-warning">Buat Ta Spjsisa</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_Bukti',
            'Tgl_Bukti',
            // 'No_SPJ',
            // 'Tgl_SPJ',
            // 'No_SPP',
            // 'Tgl_SPP',
            // 'Kd_Keg',
            // 'Keterangan',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
