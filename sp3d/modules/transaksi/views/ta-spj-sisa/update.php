<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJSisa */

$this->title = 'Update Ta Spjsisa: ' . $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjsisas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_Bukti, 'url' => ['view', 'id' => $model->No_Bukti]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spjsisa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
