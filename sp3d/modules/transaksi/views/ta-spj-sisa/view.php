<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJSisa */

$this->title = $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjsisas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjsisa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_Bukti], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_Bukti], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjsisa/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spjsisa</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spjsisa/delete?id=<?= $model->id?>', urlBack:'ta-spjsisa/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spjsisa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_Bukti',
            'Tgl_Bukti',
            'No_SPJ',
            'Tgl_SPJ',
            'No_SPP',
            'Tgl_SPP',
            'Kd_Keg',
            'Keterangan',
            'Nilai',
        ],
    ]) ?>

</div>
