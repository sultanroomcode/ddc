<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMKegiatan */

$this->title = 'Update Ta Rpjmkegiatan: ' . $model->Kd_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmkegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Keg, 'url' => ['view', 'id' => $model->Kd_Keg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmkegiatan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
