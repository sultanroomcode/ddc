<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rpjm-kegiatan-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RPJM Kegiatan Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RPJM Kegiatan Desa?', urlSend:'/transaksi/ta-rpjm-kegiatan/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>', urlBack:'/transaksi/ta-rpjm-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RPJM Kegiatan Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Desa',
            'Kd_Bid',
            'Kd_Keg',
            'ID_Keg',
            'Nama_Kegiatan',
            'Lokasi',
            'Keluaran',
            'Kd_Sas',
            'Sasaran',
            'Tahun1',
            'Tahun2',
            'Tahun3',
            'Tahun4',
            'Tahun5',
            'Tahun6',
            'Swakelola',
            'Kerjasama',
            'Pihak_Ketiga',
            'Sumberdana',
        ],
    ]) ?>

</div>
