<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmkegiatans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmkegiatan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmkegiatan/create'})" class="btn btn-warning">Buat Ta Rpjmkegiatan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Desa',
            'Kd_Bid',
            'Kd_Keg',
            'ID_Keg',
            'Nama_Kegiatan',
            // 'Lokasi',
            // 'Keluaran',
            // 'Kd_Sas',
            // 'Sasaran',
            // 'Tahun1',
            // 'Tahun2',
            // 'Tahun3',
            // 'Tahun4',
            // 'Tahun5',
            // 'Tahun6',
            // 'Swakelola',
            // 'Kerjasama',
            // 'Pihak_Ketiga',
            // 'Sumberdana',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
