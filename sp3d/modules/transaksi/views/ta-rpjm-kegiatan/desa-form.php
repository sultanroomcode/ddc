<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\transaksi\TaRPJMBidang;
use frontend\models\referensi\RefBidang;
use frontend\models\referensi\RefSumber;
use yii\helpers\ArrayHelper;
$bidangList = ArrayHelper::map(TaRPJMBidang::find()->where(['Kd_Desa' => $id])->all(),'Kd_Bid','Nama_Bidang');
$sumberList = ArrayHelper::map(RefSumber::find()->all(),'Kode','Nama_Sumber');
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
} else {
    $model->Kd_Bid = substr($model->Kd_Bid, -2,2);
}
?>

<div class="ta-rpjm-kegiatan-form animated slideInUp">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Kegiatan RPJM</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'Kd_Bid')->dropdownList($bidangList) ?>

            <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ID_Keg')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Nama_Kegiatan')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Lokasi')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Keluaran')->textarea(['maxlength' => true, 'placeholder' => 'Masukkan Hasil yang diharapkan dari kegiatan sebanyak 50 karakter']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Kd_Sas')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Sasaran')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Swakelola')->textInput() ?>

            <?= $form->field($model, 'Kerjasama')->textInput() ?>

            <?= $form->field($model, 'Pihak_Ketiga')->textInput() ?>

            <?= $form->field($model, 'Sumberdana')->dropdownList($sumberList) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Tahun1')->textInput(['maxlength' => true, 'placeholder' => 'Ex: 2016']) ?>

            <?= $form->field($model, 'Tahun2')->textInput(['maxlength' => true, 'placeholder' => 'Ex: 2017']) ?>

            <?= $form->field($model, 'Tahun3')->textInput(['maxlength' => true, 'placeholder' => 'Ex: 2018']) ?>

            <?= $form->field($model, 'Tahun4')->textInput(['maxlength' => true, 'placeholder' => 'Ex: 2019']) ?>

            <?= $form->field($model, 'Tahun5')->textInput(['maxlength' => true, 'placeholder' => 'Ex: 2020']) ?>

            <?= $form->field($model, 'Tahun6')->textInput(['maxlength' => true, 'placeholder' => 'Ex: 2021']) ?>
        </div>
    </div>
    

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-rpjm-kegiatan/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);