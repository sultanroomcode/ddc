<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMKegiatan */

$this->title = $model->Kd_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmkegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmkegiatan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kd_Keg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kd_Keg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmkegiatan/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmkegiatan</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmkegiatan/delete?id=<?= $model->id?>', urlBack:'ta-rpjmkegiatan/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmkegiatan</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Desa',
            'Kd_Bid',
            'Kd_Keg',
            'ID_Keg',
            'Nama_Kegiatan',
            'Lokasi',
            'Keluaran',
            'Kd_Sas',
            'Sasaran',
            'Tahun1',
            'Tahun2',
            'Tahun3',
            'Tahun4',
            'Tahun5',
            'Tahun6',
            'Swakelola',
            'Kerjasama',
            'Pihak_Ketiga',
            'Sumberdana',
        ],
    ]) ?>

</div>
