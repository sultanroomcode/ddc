<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMKegiatan */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaRPJMKegiatan */
/* @var $generator string helper base name with camel to id Inflector ta-rpjmkegiatan */
/* @var $generator string helper base name with camel to word Inflector Ta Rpjmkegiatan */
?>

<div class="ta-rpjmkegiatan-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmkegiatan/index'})" class="btn btn-warning">List Ta Rpjmkegiatan</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Bid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama_Kegiatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lokasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Keluaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Sas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Sasaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tahun1')->textInput() ?>

    <?= $form->field($model, 'Tahun2')->textInput() ?>

    <?= $form->field($model, 'Tahun3')->textInput() ?>

    <?= $form->field($model, 'Tahun4')->textInput() ?>

    <?= $form->field($model, 'Tahun5')->textInput() ?>

    <?= $form->field($model, 'Tahun6')->textInput() ?>

    <?= $form->field($model, 'Swakelola')->textInput() ?>

    <?= $form->field($model, 'Kerjasama')->textInput() ?>

    <?= $form->field($model, 'Pihak_Ketiga')->textInput() ?>

    <?= $form->field($model, 'Sumberdana')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-rpjmkegiatan/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta Rpjmkegiatan')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-rpjmkegiatan/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);