<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel RPJM - Kegiatan</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/rpjm-main-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-kegiatan/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Kegiatan RPJM</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-rpjm-kegiatan-table" class="display table" cellspacing="0" width="100%">
	    <thead>
	        <tr>
	            <th>Kode Bidang</th>
	            <th>Kode Kegiatan</th>
	            <th>Nama Kegiatan</th>
	            <th>Lokasi</th>
	            <th>Sasaran</th>
	            <th>Sumber</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kode Bidang</th>
	            <th>Kode Kegiatan</th>
	            <th>Nama Kegiatan</th>
	            <th>Lokasi</th>
	            <th>Sasaran</th>
	            <th>Sumber</th>
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->Kd_Bid ?></td>
	            <td><?= $v->Kd_Keg ?></td>
	            <td><?= $v->Nama_Kegiatan ?></td>
	            <td><?= $v->Lokasi ?></td>
	            <td><?= $v->Sasaran ?></td>
	            <td><?= $v->Sumberdana ?></td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-rpjm-kegiatan/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data?', urlSend:'/transaksi/ta-rpjm-kegiatan/desa-delete?id=<?=$id?>&kd=<?=$v->Kd_Keg?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-rpjm-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-rpjm-kegiatan/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-kegiatan/desa-update?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$v->Kd_Keg?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-rpjm-kegiatan/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-kegiatan/desa-view?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$v->Kd_Keg?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-rpjm-kegiatan-table').DataTable();
JS;

$this->registerJs($scripts);