<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dPerangkatPelatihan */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Perangkat Pelatihans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp3d-perangkat-pelatihan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kd_desa' => $model->kd_desa, 'nik' => $model->nik, 'idp' => $model->idp], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_desa' => $model->kd_desa, 'nik' => $model->nik, 'idp' => $model->idp], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'nik',
            'idp',
            'nama',
            'tahun',
            'penyelenggara',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
