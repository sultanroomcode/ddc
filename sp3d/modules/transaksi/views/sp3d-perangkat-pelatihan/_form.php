<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-8',
        'error' => '',
        'hint' => '',
    ],
]];

$model->tipe = $inbox['data']['tipe'];

if($model->isNewRecord){
    if($inbox != false){
        $model->kd_desa = $inbox['data']['kd_desa'];
        $model->nik = $inbox['data']['nik'];
    }
    $model->genNum();
}
?>

<div class="sp3d-perangkat-pelatihan-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>
    
    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'nik')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'idp')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'tipe')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kategori')->dropdownList($model->arrKategori()) ?>

    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'penyelenggara')->textInput(['maxlength' => true]) ?>
    
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="goLoad({elm:'#list-catatan-pelatihan', url:'/transaksi/sp3d-perangkat-pelatihan/view-in-box?tipe=<?=$model->tipe?>&kd_desa=<?=$model->kd_desa?>&nik=<?=$model->nik?>'})" class="btn btn-success"><i class="fa fa-backward"></i></a>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#list-catatan-pelatihan', url:'/transaksi/sp3d-perangkat-pelatihan/view-in-box?tipe={$model->tipe}&kd_desa={$model->kd_desa}&nik={$model->nik}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);