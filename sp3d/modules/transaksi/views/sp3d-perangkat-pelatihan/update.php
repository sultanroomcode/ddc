<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dPerangkatPelatihan */

$this->title = 'Update Pelatihan: ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Perangkat Pelatihans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'nik' => $model->nik, 'idp' => $model->idp]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sp3d-perangkat-pelatihan-update">
    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'inbox' => $inbox
    ]) ?>

</div>
