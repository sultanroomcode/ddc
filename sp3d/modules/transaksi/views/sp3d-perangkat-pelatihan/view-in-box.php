<?php if(Yii::$app->user->identity->type == 'desa'){ ?>
<a href="javascript:void(0)" onclick="goLoad({elm:'#list-catatan-pelatihan', url:'/transaksi/sp3d-perangkat-pelatihan/create-in-box?tipe=<?=$data['tipe']?>&kd_desa=<?=$data['kd_desa']?>&nik=<?=$data['nik']?>'})" class="btn btn-danger">Tambah Pelatihan</a>
<?php } ?>

<?php if($dataProvider->count() > 0){ ?>
<table class="table table-striped">
    <tr>
    	<th>Tahun</th>
	    <th>Nama Pelatihan</th>
	    <th>Penyelenggara</th>
	    <th>Aksi</th>
	</tr>

    <?php 
    foreach($dataProvider->all() as $v){
    ?>
    <tr>
    	<td><?= $v->tahun ?></td>
    	<td><?= $v->nama ?></td>
    	<td><?= $v->penyelenggara ?></td>
    	<td>
    		<?php if(Yii::$app->user->identity->type == 'desa'){ ?>
    		<a href="javascript:void(0)" onclick="goLoad({elm:'#list-catatan-pelatihan', url:'/transaksi/sp3d-perangkat-pelatihan/update-in-box?tipe=<?=$data['tipe']?>&kd_desa=<?=$data['kd_desa']?>&nik=<?=$data['nik']?>&idp=<?=$v->idp?>' })" class="btn btn-danger">Update</a><a href="javascript:void(0)" onclick="goLoad({elm:'#', url:'/transaksi/sp3d-perangkat-pelatihan/create-in-box?kd_desa=<?=$data['kd_desa']?>&nik=<?=$data['nik']?>' })" class="btn btn-danger">Hapus</a>
	    	<?php } ?>
    	</td>
    </tr>
    <?php
    }
    ?>
</table>
<?php } else { ?>
Data Pelatihan Belum Ada
<?php } ?>