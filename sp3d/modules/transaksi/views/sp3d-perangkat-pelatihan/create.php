<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dPerangkatPelatihan */

$this->title = 'Isi Pelatihan';
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Perangkat Pelatihans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp3d-perangkat-pelatihan-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'inbox' => $inbox
    ]) ?>

</div>
