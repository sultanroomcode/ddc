<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use sp3d\models\transaksi\Sp3dMasterJabatan;
use yii\widgets\ActiveForm;
if($show_jabatan_list == true){ 
    $jabatanList = ArrayHelper::map(Sp3dMasterJabatan::find()->where(['tipe' => 'bpd', 'status' => '10'])->orderBy('urut')->all(), 'id', 'nama_jabatan');
    $urlBack = '/transaksi/ta-bpd/data-bpd';
    $label_nama = 'Nama Perangkat BPD';
} else {
    $urlBack = '/transaksi/ta-perangkat-desa/data-kepala-desa';
    $label_nama = 'Nama Kepala Desa';
}

$model->tipe = 'bpd';
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Data <?= ($show_jabatan_list == false)?'Kepala':'BPD' ?></h2>
            <div class="p-10">
            	<?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'nik')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999999999999999'
                        ]) ?>
                        <?= $form->field($model, 'nama')->textInput(['maxlength' => true])->label($label_nama) ?>
                        <?php if($show_jabatan_list == true){ 
                            unset($jabatanList['kepala-desa']);
                            echo $form->field($model, 'jabatan')->dropdownList($jabatanList);
                        } ?>
                        <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'tipe')->hiddenInput(['maxlength' => true])->label(false) ?>
                        <?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'foto_profile_box')->fileInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'jenis_kelamin')->dropdownList(['L' => 'Laki-Laki', 'P' => 'Perempuan']) ?>
                        <?= $form->field($model, 'pendidikan')->dropdownList([
                            '0|SD' => 'Sekolah Dasar', 
                            '1|SMP' => 'Sekolah Menengah Pertama', 
                            '2|SMA' => 'Sekolah Menengah Atas', 
                            '3|D1' => 'Diploma I', 
                            '4|D2' => 'Diploma II', 
                            '5|D3' => 'Diploma III', 
                            '6|D4' => 'Diploma IV', 
                            '7|S1' => 'Sarjana', 
                            '8|S2' => 'Magister', 
                            '9|S3' => 'Doktoral', 
                        ]) ?>
                        <?= $form->field($model, 'tgl_sk_pengangkatan')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'no_sk_pengangkatan')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'tahun_awal')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999'
                        ]) ?>
                        <?= $form->field($model, 'tahun_akhir')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999'
                        ]) ?>
                        <?= $form->field($model, 'no_kontak')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999999999999'
                        ]) ?>

                        <?= $form->field($model, 'status_aktif')->dropdownList([
                            'aktif' => 'Aktif', 
                            'non-aktif' => 'Non-Aktif'
                        ]) ?>
                    </div>
                </div>		    

			    <div class="form-group">
			        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <a href="javascript:void(0)" onclick="goLoad({url: '<?=$urlBack?>'})" class="show-pop btn btn-danger" title="Tambah Data Kepala Desa">Kembali</a>
			    </div>

			    <?php ActiveForm::end(); ?>
            </div>
		</div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
$('#sp3dperangkatdesa-tanggal_lahir, #sp3dperangkatdesa-tgl_sk_pengangkatan').datetimepicker({
    format:'Y-m-d',
    mask:true
});
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#{$model->formName()}')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : '{$urlBack}'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($scripts);