<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaJurnalUmum */

$this->title = 'Update Ta Jurnal Umum: ' . $model->NoBukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Jurnal Umums', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NoBukti, 'url' => ['view', 'id' => $model->NoBukti]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-jurnal-umum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
