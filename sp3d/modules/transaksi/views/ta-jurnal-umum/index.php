<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Jurnal Umums';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-jurnal-umum-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-jurnal-umum/create'})" class="btn btn-warning">Buat Ta Jurnal Umum</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'KdBuku',
            'Kd_Desa',
            'Tanggal',
            'JnsBukti',
            // 'NoBukti',
            // 'Keterangan',
            // 'DK',
            // 'Debet',
            // 'Kredit',
            // 'Jenis',
            // 'Posted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
