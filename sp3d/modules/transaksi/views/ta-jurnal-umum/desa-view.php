<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-jurnal-umum-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-jurnal-umum/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Jurnal</a>
        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Jurnal Umum?', urlSend:'/transaksi/ta-jurnal-umum/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&no=<?=$model->NoBukti?>', urlBack:'/transaksi/ta-jurnal-umum/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus Jurnal</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'KdBuku',
            'Kd_Desa',
            'Tanggal',
            'JnsBukti',
            'NoBukti',
            'Keterangan',
            'DK',
            'Debet',
            'Kredit',
            'Jenis',
            'Posted',
        ],
    ]) ?>

</div>
