<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaJurnalUmum */

$this->title = $model->NoBukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Jurnal Umums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-jurnal-umum-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->NoBukti], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->NoBukti], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-jurnal-umum/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Jurnal Umum</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-jurnal-umum/delete?id=<?= $model->id?>', urlBack:'ta-jurnal-umum/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Jurnal Umum</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'KdBuku',
            'Kd_Desa',
            'Tanggal',
            'JnsBukti',
            'NoBukti',
            'Keterangan',
            'DK',
            'Debet',
            'Kredit',
            'Jenis',
            'Posted',
        ],
    ]) ?>

</div>
