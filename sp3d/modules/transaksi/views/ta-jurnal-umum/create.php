<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaJurnalUmum */

$this->title = 'Create Ta Jurnal Umum';
$this->params['breadcrumbs'][] = ['label' => 'Ta Jurnal Umums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-jurnal-umum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
