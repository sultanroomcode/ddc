<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTriwulan */

$this->title = 'Update Ta Triwulan: ' . $model->KURincianSD;
$this->params['breadcrumbs'][] = ['label' => 'Ta Triwulans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->KURincianSD, 'url' => ['view', 'KURincianSD' => $model->KURincianSD, 'Tahun' => $model->Tahun]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-triwulan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
