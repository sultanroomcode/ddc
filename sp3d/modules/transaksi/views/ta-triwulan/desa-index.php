<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Triwulan</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa-page?tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-primary animated slideInLeft"><i class="fa fa-refresh"></i> Kembali</a>

  	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-triwulan/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Triwulan</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-triwulan-table" class="display table" cellspacing="0" width="100%">
	    <thead>
	        <tr>
	            <th>No Triwulan</th>
	            <th>Sumber Dana</th>

	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>No Triwulan</th>
	            <th>Sumber Dana</th>

	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <th><?= $v->KURincianSD ?></th>
	            <th><?= $v->SumberDana ?></th>
	            
	            <th>
	            	<?php if(Mimin::checkRoute('transaksi/ta-triwulan/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data?', urlSend:'/transaksi/ta-triwulan/desa-delete?id=<?=$id?>&kd=<?=$v->KURincianSD?>&tahun=<?=$v->Tahun?>', urlBack:'/transaksi/ta-triwulan/desa-index?id=<?=$id?>&tahun=<?=$v->Tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-triwulan/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-triwulan/desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->KURincianSD?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-triwulan/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-triwulan/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->KURincianSD?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </th>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-triwulan-table').DataTable();
JS;

$this->registerJs($scripts);