<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-triwulan-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-triwulan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Triwulan Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Triwulan Desa?', urlSend:'/transaksi/ta-triwulan/desa-delete?id=<?=$id?>&kd=<?=$model->KURincianSD?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-triwulan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus Triwulan Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KURincianSD',
            'Tahun',
            'Sifat',
            'SumberDana',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Keg',
            'Kd_Rincian',
            'Anggaran',
            'AnggaranPAK',
            'Tw1Rinci',
            'Tw2Rinci',
            'Tw3Rinci',
            'Tw4Rinci',
            'KunciData',
        ],
    ]) ?>

</div>
