<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Triwulans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-triwulan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-triwulan/create'})" class="btn btn-warning">Buat Ta Triwulan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'KURincianSD',
            'Tahun',
            'Sifat',
            'SumberDana',
            'Kd_Desa',
            // 'kd_kecamatan',
            // 'Kd_Keg',
            // 'Kd_Rincian',
            // 'Anggaran',
            // 'AnggaranPAK',
            // 'Tw1Rinci',
            // 'Tw2Rinci',
            // 'Tw3Rinci',
            // 'Tw4Rinci',
            // 'KunciData',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
