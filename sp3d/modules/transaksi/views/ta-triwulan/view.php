<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTriwulan */

$this->title = $model->KURincianSD;
$this->params['breadcrumbs'][] = ['label' => 'Ta Triwulans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-triwulan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'KURincianSD' => $model->KURincianSD, 'Tahun' => $model->Tahun], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'KURincianSD' => $model->KURincianSD, 'Tahun' => $model->Tahun], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-triwulan/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Triwulan</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-triwulan/delete?id=<?= $model->id?>', urlBack:'ta-triwulan/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Triwulan</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KURincianSD',
            'Tahun',
            'Sifat',
            'SumberDana',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Keg',
            'Kd_Rincian',
            'Anggaran',
            'AnggaranPAK',
            'Tw1Rinci',
            'Tw2Rinci',
            'Tw3Rinci',
            'Tw4Rinci',
            'KunciData',
        ],
    ]) ?>

</div>
