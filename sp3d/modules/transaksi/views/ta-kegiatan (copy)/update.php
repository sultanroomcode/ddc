<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaKegiatan */

$this->title = 'Update Ta Kegiatan: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Keg' => $model->Kd_Keg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-kegiatan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
