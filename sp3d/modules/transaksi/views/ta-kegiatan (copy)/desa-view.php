<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-kegiatan-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Kegiatan</a> -->
        <a href="javascript:void(0)" onclick="goHiatus({elm:'#bidang-index-form', state:'hidden'})" class="btn btn-sm btn-warning">Kembali</a>
    </p>

    <div class="row">
        <h4>Deskripsi Kegiatan</h4>
        <div class="col-md-6">
            <b>Tahun :</b><br><?= $model->Tahun ?><br>
            <b>Rekening Kegiatan :</b><br><?= $model->ID_Keg ?><br>
            <b>Nama Kegiatan :</b><br><?= $model->Nama_Kegiatan ?><br>
            <b>Pagu :</b><br><?= $model->nf($model->Pagu) ?><br>
            <b>Pagu (PAK) :</b><br><?= $model->nf($model->Pagu_PAK) ?><br>
        </div>
        <div class="col-md-6">
            
            <b>Nama PPTK :</b><br><?= $model->Nm_PPTKD ?><br>
            <b>NIP :</b><br><?= $model->NIP_PPTKD ?><br>
            <b>Lokasi :</b><br><?= $model->Lokasi ?><br>
            <b>Waktu :</b><br><?= $model->Waktu ?><br>
            <b>Keluaran:</b><br><?= $model->Keluaran ?><br>
            <b>Sumber Dana:</b><br><?= $model->Sumberdana ?><br>
        </div>
    </div>   

    <div class="row">
        <h4>Deskripsi Belanja</h4>
        <?php if($model->rab != null){ ?>
        
        <div class="col-md-4">
            <b>Anggaran :</b><br><?= $model->nf($model->rab->Anggaran) ?>
        </div>
        <div class="col-md-4">
            <b>Anggaran (PAK) :</b><br><?= $model->nf($model->rab->AnggaranPAK) ?>
        </div>
        <div class="col-md-4">
           <b>Anggaran + PAK :</b><br><?= $model->nf($model->rab->AnggaranStlhPAK) ?>
        </div>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-view?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>&kd2=<?=$model->ID_Keg?>'})" class="btn btn-sm btn-warning">Isi Rincian Belanja</a>

        <?php } else { ?>
            <a href="javascript:void(0)" onclick="goLoad({elm:'#bidang-index-form', url:'/data-umum/create-object?kd_desa=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>'})" class="btn btn-sm btn-warning">Buat Objek Anggaran Belanja</a>
        <?php } ?>
    </div>   
</div>
