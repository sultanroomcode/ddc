<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaKegiatan */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaKegiatan */
/* @var $generator string helper base name with camel to id Inflector ta-kegiatan */
/* @var $generator string helper base name with camel to word Inflector Ta Kegiatan */
?>

<div class="ta-kegiatan-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-kegiatan/index'})" class="btn btn-warning">List Ta Kegiatan</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Bid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama_Kegiatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pagu')->textInput() ?>

    <?= $form->field($model, 'Pagu_PAK')->textInput() ?>

    <?= $form->field($model, 'Nm_PPTKD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NIP_PPTKD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lokasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Waktu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Keluaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Sumberdana')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-kegiatan/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta Kegiatan')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-kegiatan/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);