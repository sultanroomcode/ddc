<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJRinci */

$this->title = $model->No_SPJ;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjrincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjrinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_SPJ' => $model->No_SPJ, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Sumberdana' => $model->Sumberdana], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_SPJ' => $model->No_SPJ, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Sumberdana' => $model->Sumberdana], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjrinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spjrinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spjrinci/delete?id=<?= $model->id?>', urlBack:'ta-spjrinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spjrinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_SPJ',
            'Kd_Keg',
            'Kd_Rincian',
            'Sumberdana',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'JmlCair',
            'Nilai',
            'Sisa',
        ],
    ]) ?>

</div>
