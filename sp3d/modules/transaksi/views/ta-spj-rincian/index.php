<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Spjrincis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjrinci-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjrinci/create'})" class="btn btn-warning">Buat Ta Spjrinci</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'No_SPJ',
            'Kd_Keg',
            'Kd_Rincian',
            'Sumberdana',
            // 'Kd_Desa',
            // 'kd_kecamatan',
            // 'No_SPP',
            // 'JmlCair',
            // 'Nilai',
            // 'Sisa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
