<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJRinci */

$this->title = 'Update Ta Spjrinci: ' . $model->No_SPJ;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjrincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SPJ, 'url' => ['view', 'No_SPJ' => $model->No_SPJ, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Sumberdana' => $model->Sumberdana]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spjrinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
