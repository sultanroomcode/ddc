<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMBidang */

$this->title = 'Create Ta Rpjmbidang';
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmbidangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmbidang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
