<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\referensi\RefBidang;
use yii\helpers\ArrayHelper;
$bidangList = ArrayHelper::map(RefBidang::find()->all(),'kode','nama');
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Nama_Bidang = 'Bidang Penyelenggaraan Pemerintah Desa';
} else {
    $model->Kd_Bid = substr($model->Kd_Bid, -2,2);
}
?>

<div class="ta-rab-form animated slideInUp">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Bidang RPJM</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'Kd_Bid')->dropdownList($bidangList) ?>
    <?= $form->field($model, 'Nama_Bidang')->hiddenInput(['maxlength' => true])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
$('#tarpjmbidang-kd_bid').change(function(){
    var vl = $('#tarpjmbidang-kd_bid option:selected').text();
    $('#tarpjmbidang-nama_bidang').val(vl);
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-rpjm-bidang/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);