<?php 
use yii\helpers\Url;
?>
<h1 class="animated slideInDown"><i class="fa fa-address-card-o"></i> RPJM</h1>

<div class="row animated slideInUp">
	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-visi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-eye"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-visi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> Visi</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-misi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-bullseye"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-misi/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-bullseye"></i> Misi</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-tujuan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-calculator"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-tujuan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-calculator"></i> Tujuan</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
		
		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-sasaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-dot-circle-o"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-sasaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-dot-circle-o"></i> Sasaran</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-building-o"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-bidang/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-building-o"></i> Bidang</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-thumb-tack"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-kegiatan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-thumb-tack"></i> Kegiatan</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-pagu-indikatif/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-calculator"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-pagu-indikatif/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-calculator"></i> Pagu Indikatif</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-red">
			<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-pagu-tahunan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="linkto"><span class="info-box-icon"><i class="fa fa-calculator"></i></span></a>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rpjm-pagu-tahunan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-danger btn-xs"><i class="fa fa-calculator"></i> Pagu Tahunan</a></span>
				<span class="info-box-number">&nbsp;</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped animated slideInLeft" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					&nbsp; <!-- fix -->
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>
</div>