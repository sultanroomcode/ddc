<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMBidang */

$this->title = 'Update Ta Rpjmbidang: ' . $model->Kd_Bid;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmbidangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Bid, 'url' => ['view', 'id' => $model->Kd_Bid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rpjmbidang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
