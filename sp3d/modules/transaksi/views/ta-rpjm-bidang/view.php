<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRPJMBidang */

$this->title = $model->Kd_Bid;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rpjmbidangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmbidang-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kd_Bid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kd_Bid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmbidang/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rpjmbidang</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rpjmbidang/delete?id=<?= $model->id?>', urlBack:'ta-rpjmbidang/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rpjmbidang</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Desa',
            'Kd_Bid',
            'Nama_Bidang',
        ],
    ]) ?>

</div>
