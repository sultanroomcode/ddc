<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rpjmbidangs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rpjmbidang-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rpjmbidang/create'})" class="btn btn-warning">Buat Ta Rpjmbidang</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Desa',
            'Kd_Bid',
            'Nama_Bidang',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
