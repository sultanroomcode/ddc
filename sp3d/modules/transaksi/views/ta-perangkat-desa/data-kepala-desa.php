<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Kepala Desa</h2>
            <div class="p-10">
            	<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/ta-perangkat-desa/form-kepala-desa'})" class="show-pop btn btn-danger" title="Tambah Data Kepala Desa" data-animation="pop" data-content="<p>Menambahkan Data Kepala Desa.</p>">Data Baru</a>
				<!-- <a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-normalize-count'})" class="btn btn-danger" title="Normalisasi Hitungan"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-list-ol"></i></a> -->

				<div class="block-area" id="tableStriped">
				    <div class="table-responsive overflow">
				    	<table id="ta-kepala-desa" class="tile table table-bordered table-striped" cellspacing="0">
						    <thead>
						        <tr>
						            <th>NIK</th>
						            <th>Nama</th>
						            <th>Tahun Pemerintahan</th>
						            <th>SK Pengangkatan</th>
						            <th>Aksi</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($dataProvider->all() as $v): ?>
						    	<tr>
						            <td><?= $v->nik ?></td>
						            <td><?= $v->nama ?></td>
						            <td><?= $v->tahun_awal . ' - '. $v->tahun_akhir ?></td>
						            <td><?= $v->no_sk_pengangkatan ?><br><small><?= date('d-F-Y', strtotime($v->tgl_sk_pengangkatan)) ?></small></td>
						            <td>
						            	<a href="javascript:void(0)" onclick="openModalXyf({welm:'600px',url:'/transaksi/ta-perangkat-desa/view-perangkat-desa?nik=<?=$v->nik?>&user=<?=$v->user?>'})" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
						            	<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/ta-perangkat-desa/update-kepala-desa?nik=<?=$v->nik?>&user=<?=$v->user?>'})" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>
						            	<a href="javascript:void(0)" onclick="confirmationDelete('<?=$v->nik?>','<?=$v->user?>')" class="btn btn-sm btn-default"><i class="fa fa-trash"></i></a>
						           	</td>
						        </tr>
						    	<?php endforeach; ?>
						    </tbody>
						</table>
				    </div>
				</div><br>
				<b>Nb</b> : -
			</div>
		</div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
	$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
	$('#ta-kepala-desa').DataTable();

	function confirmationDelete(nik, user){
		$.confirm({
		    title: 'Hapus Kepala desa?',
		    content: 'Tindakan ini akan menghapus data kepala desa dengan NIK '+nik,
		    autoClose: 'batal|10000',
		    buttons: {
		        deleteUser: {
		            text: 'hapus',
		            action: function () {
		                goLoad({url: '/transaksi/ta-perangkat-desa/delete-perangkat-desa?nik='+nik+'&user='+user+'&jabatan=kepala'});
		            }
		        },
		        batal: function () {
		            $.alert('Tindakan dibatalkan');
		        }
		    }
		});
	}
JS;

$this->registerJs($scripts);