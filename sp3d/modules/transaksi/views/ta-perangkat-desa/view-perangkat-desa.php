<?php
$data = $dataProvider;
?>
<h4>Biodata</h4>
<div class="row">
	<div class="col-md-3">
		<img src="<?= $data->show_foto() ?>">
	</div>
	<div class="col-md-9">
		<table class="table table-striped">
			<tr>
				<td>Nama</td>
				<td><?= $data->nama ?></td>
			</tr>

			<tr>
				<td>Jabatan</td>
				<td><?= $data->jabatanproperti->nama_jabatan ?></td>
			</tr>

			<tr>
				<td>Tempat dan Tanggal Lahir</td>
				<td><?= $data->tempat_lahir.', '. $data->tanggal_lahir_format() ?></td>
			</tr>

			<tr>
				<td>Jenis Kelamin</td>
				<td><?= $data->jenis_kelamin ?></td>
			</tr>

			<tr>
				<td valign="top">SK Pengangkatan</td>
				<td>No : <?= $data->no_sk_pengangkatan ?><br>Tanggal : <?= $data->tanggal_sk_format() ?></td>
			</tr>
			<tr>
				<td>Tahun Pemerintahan</td>
				<td><?= $data->tahun_awal ?> - <?= $data->tahun_akhir ?></td>
			</tr>
		</table>
	</div>
</div>

<h4>Catatan Pelatihan</h4>
<div id="list-catatan-pelatihan"></div>
<?php 
$script = <<<JS
goLoad({elm:'#list-catatan-pelatihan', url:'/transaksi/sp3d-perangkat-pelatihan/view-in-box?tipe={$data->tipe}&kd_desa={$data->user}&nik={$data->nik}'});
JS;

$this->registerJs($script);