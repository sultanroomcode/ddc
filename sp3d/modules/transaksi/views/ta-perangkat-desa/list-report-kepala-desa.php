<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;
$id = Yii::$app->user->identity->id;
$tipeuser = Yii::$app->user->identity->type;
$arr = ['-' => ''];
$listArr = [];
if($tipeuser == 'provinsi'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
}

if($tipeuser == 'kabupaten'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
}

$listArr = $arr + $listArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">DAFTAR KEPALA DESA TERDAFTAR</h2>
            <div class="p-10">
                <div class="row">
                    <?php if($tipeuser == 'provinsi'){ ?>
                    <div class="col-md-4">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } 
                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten'){ ?>
                    <div class="col-md-4">
                    	<?php if($tipeuser == 'kabupaten'){ ?>
	                    	<input type="hidden" id="kabupaten" value="<?= substr($id, 0, 4) ?>">
	                    <?php } ?>
	                    
                        <select id="kecamatan" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    }
                    ?>
                    <div class="col-md-4">
                        <button class="btn btn-danger" onclick="filteringAdd()">Filter</button>
                        <button class="btn btn-danger" onclick="filteringGrafik()">Grafik</button>
                        <button class="btn btn-danger" onclick="filteringExport()">Export</button>
                    </div>
                </div>
            	<hr>
            	<div id="report-list-kades-zona"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div id="modal-custom" style="display: none;">
    <div style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
    </div>
</div>
<?php
$scripts =<<<JS
$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});

$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

function filteringGrafik(){
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    goLoad({elm:'#report-list-kades-zona', url:'/umum/statistik/other-stat?data=kepala-desa&kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan});
}

function filteringAdd(){
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    goLoad({elm:'#report-list-kades-zona', url:'/sp3ddashboard/data-center/data-kades?kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan});
}

function filteringExport(){
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    window.open(base_url+'/sp3ddashboard/data-export/data-kades?kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan, '_blank');
    // goLoad({elm:'#report-list-kades-zona', url:'/sp3ddashboard/data-export/data-kades?kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan});
}
JS;

$this->registerJs($scripts);