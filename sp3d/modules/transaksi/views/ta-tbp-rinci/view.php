<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTBPRinci */

$this->title = $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Tbprincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-tbprinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_Bukti' => $model->No_Bukti, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'RincianSD' => $model->RincianSD], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_Bukti' => $model->No_Bukti, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'RincianSD' => $model->RincianSD], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-tbprinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Tbprinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-tbprinci/delete?id=<?= $model->id?>', urlBack:'ta-tbprinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Tbprinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_Bukti',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Keg',
            'Kd_Rincian',
            'RincianSD',
            'SumberDana',
            'Nilai',
        ],
    ]) ?>

</div>
