<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Tbprincis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-tbprinci-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-tbprinci/create'})" class="btn btn-warning">Buat Ta Tbprinci</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'No_Bukti',
            'Kd_Desa',
            'kd_kecamatan',
            'Kd_Keg',
            // 'Kd_Rincian',
            // 'RincianSD',
            // 'SumberDana',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
