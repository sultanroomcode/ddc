<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTBPRinci */

$this->title = 'Update Ta Tbprinci: ' . $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Tbprincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_Bukti, 'url' => ['view', 'No_Bukti' => $model->No_Bukti, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'RincianSD' => $model->RincianSD]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-tbprinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
