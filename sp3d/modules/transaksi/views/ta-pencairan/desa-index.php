<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Pencairan</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/default/desa-page?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-pencairan/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pencairan/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Pencairan</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-pencairan-table" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>No</th>
	            <th>Tanggal</th>
	            <th>No SPP</th>
	            <th>Jumlah</th>
	            <th>Potongan</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>No</th>
	            <th>Tanggal</th>
	            <th>No SPP</th>
	            <th>Jumlah</th>
	            <th>Potongan</th>
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->No_Cek ?></td>
	            <td><?= $v->tanggal ?></td>
	            <td><?= $v->No_SPP ?></td>
	            <td align="right"><?= number_format(round($v->Jumlah), 0, ',','.') ?></td>
	            <td align="right"><?= number_format(round($v->Potongan), 0, ',','.') ?></td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-pencairan/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pencairan/desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Cek?>&no2=<?=$v->No_SPP?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-pencairan/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Pajak Desa?', urlSend:'/transaksi/ta-pencairan/desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Cek?>&no2=<?=$v->No_SPP?>', urlBack:'/transaksi/ta-pencairan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>	            	

	            	<?php if(Mimin::checkRoute('transaksi/ta-pencairan/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pencairan/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&no=<?=$v->No_Cek?>&no2=<?=$v->No_SPP?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-pencairan-table').DataTable();
JS;

$this->registerJs($scripts);