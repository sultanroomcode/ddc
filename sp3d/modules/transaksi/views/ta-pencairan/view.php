<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPencairan */

$this->title = $model->No_Cek;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pencairans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-pencairan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_Cek' => $model->No_Cek, 'No_SPP' => $model->No_SPP], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_Cek' => $model->No_Cek, 'No_SPP' => $model->No_SPP], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-pencairan/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Pencairan</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-pencairan/delete?id=<?= $model->id?>', urlBack:'ta-pencairan/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Pencairan</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_Cek',
            'No_SPP',
            'Tgl_Cek',
            'Kd_Desa',
            'Keterangan',
            'Jumlah',
            'Potongan',
            'KdBayar',
        ],
    ]) ?>

</div>
