<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaPencairan */

$this->title = 'Update Ta Pencairan: ' . $model->No_Cek;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pencairans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_Cek, 'url' => ['view', 'No_Cek' => $model->No_Cek, 'No_SPP' => $model->No_SPP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-pencairan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
