<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Pencairans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-pencairan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-pencairan/create'})" class="btn btn-warning">Buat Ta Pencairan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'No_Cek',
            'No_SPP',
            'Tgl_Cek',
            'Kd_Desa',
            // 'Keterangan',
            // 'Jumlah',
            // 'Potongan',
            // 'KdBayar',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
