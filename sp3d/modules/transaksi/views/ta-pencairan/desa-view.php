<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-pencairan-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pencairan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Pencairan Desa</a>
        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Pencairan Desa?', urlSend:'/transaksi/ta-pencairan/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&no=<?=$model->No_Cek?>&no2=<?=$model->No_SPP?>', urlBack:'/transaksi/ta-pencairan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus Pajak</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_Cek',
            'No_SPP',
            'Tgl_Cek',
            'Kd_Desa',
            'Keterangan',
            'Jumlah',
            'Potongan',
            'KdBayar',
        ],
    ]) ?>

</div>
