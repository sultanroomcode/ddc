<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-pencairan */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
}
?>

<div class="ta-pencairan-form animated slideInUp">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-pencairan/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List Pajak</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'No_Cek')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_SPP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tgl_Cek')->textInput() ?>

    <?= $form->field($model, 'Keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jumlah')->textInput() ?>

    <?= $form->field($model, 'Potongan')->textInput() ?>

    <?= $form->field($model, 'KdBayar')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
$('#tapencairan-tgl_cek').datetimepicker({
    format:'Y-m-d H:i',
    mask:true
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-pencairan/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);