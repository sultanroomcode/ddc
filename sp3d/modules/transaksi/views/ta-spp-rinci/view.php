<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPPRinci */

$this->title = $model->No_SPP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spprincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spprinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_SPP' => $model->No_SPP, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Sumberdana' => $model->Sumberdana], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_SPP' => $model->No_SPP, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Sumberdana' => $model->Sumberdana], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spprinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spprinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spprinci/delete?id=<?= $model->id?>', urlBack:'ta-spprinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spprinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'Kd_Keg',
            'Kd_Rincian',
            'Sumberdana',
            'Nilai',
        ],
    ]) ?>

</div>
