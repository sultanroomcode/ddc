<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPPRinci */

$this->title = 'Update Ta Spprinci: ' . $model->No_SPP;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spprincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SPP, 'url' => ['view', 'No_SPP' => $model->No_SPP, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Sumberdana' => $model->Sumberdana]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spprinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
