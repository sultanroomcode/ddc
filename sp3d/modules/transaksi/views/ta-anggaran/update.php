<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaran */

$this->title = 'Update Ta Anggaran: ' . $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->KdPosting, 'url' => ['view', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'KURincianSD' => $model->KURincianSD]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-anggaran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
