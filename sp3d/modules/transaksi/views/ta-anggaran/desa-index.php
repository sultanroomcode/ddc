<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Anggaran</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/desa-page?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah Anggaran</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-anggaran-table" class="display table" cellspacing="0" width="100%">
	    <thead>
	        <tr>
	            <th>Kode</th>
	            <th>KU Rincian</th>
	            <th>Kode Rinci</th>
	            <th>Tahun</th>
	            <th>Anggaran</th>
	            <th>Anggaran PAK</th>
	            <th>Anggaran Setelah PAK</th>
	            <th>Belanja</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kode</th>
	            <th>KU Rincian</th>
	            <th>Kode Rinci</th>
	            <th>Tahun</th>
	            <th>Anggaran</th>
	            <th>Anggaran PAK</th>
	            <th>Anggaran Setelah PAK</th>
	            <th>Belanja</th>
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->KdPosting ?></td>
	            <td><?= $v->KURincianSD ?></td>
	            <td><?= $v->Kd_Rincian ?></td>
	            <td><?= $v->Tahun ?></td>
	            <td><?= number_format(round($v->Anggaran), 0, ',','.') ?></td>
	            <td><?= number_format(round($v->AnggaranPAK), 0, ',','.') ?></td>
	            <td><?= number_format(round($v->AnggaranStlhPAK), 0, ',','.') ?></td>
	            <td><?= $v->Belanja ?></td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-update?id=<?=$id?>&kd=<?=$v->KdPosting?>&tahun=<?=$v->Tahun?>&kur=<?=$v->KURincianSD?>'})" class="label label-danger">Rinci</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-update?id=<?=$id?>&kd=<?=$v->KdPosting?>&tahun=<?=$v->Tahun?>&kur=<?=$v->KURincianSD?>'})" class="label label-warning">Edit</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data?', urlSend:'/transaksi/ta-anggaran/desa-delete?id=<?=$id?>&kd=<?=$v->KdPosting?>&tahun=<?=$v->Tahun?>&kur=<?=$v->KURincianSD?>', urlBack:'/transaksi/ta-anggaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-view?id=<?=$id?>&kd=<?=$v->KdPosting?>&tahun=<?=$v->Tahun?>&kur=<?=$v->KURincianSD?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-anggaran-table').DataTable();
JS;

$this->registerJs($scripts);