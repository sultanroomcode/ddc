<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaran */

$this->title = 'Create Ta Anggaran';
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
