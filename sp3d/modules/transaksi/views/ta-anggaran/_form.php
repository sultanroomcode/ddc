<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaran */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaAnggaran */
/* @var $generator string helper base name with camel to id Inflector ta-anggaran */
/* @var $generator string helper base name with camel to word Inflector Ta Anggaran */
?>

<div class="ta-anggaran-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran/index'})" class="btn btn-warning">List Ta Anggaran</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'KdPosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'KURincianSD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Rincian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'RincianSD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Anggaran')->textInput() ?>

    <?= $form->field($model, 'AnggaranPAK')->textInput() ?>

    <?= $form->field($model, 'AnggaranStlhPAK')->textInput() ?>

    <?= $form->field($model, 'Belanja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SumberDana')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TglPosting')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-anggaran/index-npm?id='});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);