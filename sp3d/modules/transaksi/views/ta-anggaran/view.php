<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaran */

$this->title = $model->KdPosting;
$this->params['breadcrumbs'][] = ['label' => 'Ta Anggarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'KURincianSD' => $model->KURincianSD], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'KdPosting' => $model->KdPosting, 'Tahun' => $model->Tahun, 'KURincianSD' => $model->KURincianSD], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Anggaran</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-anggaran/delete?id=<?= $model->id?>', urlBack:'ta-anggaran/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Anggaran</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KdPosting',
            'Tahun',
            'KURincianSD',
            'Kd_Rincian',
            'RincianSD',
            'Anggaran',
            'AnggaranPAK',
            'AnggaranStlhPAK',
            'Belanja',
            'Kd_Keg',
            'SumberDana',
            'Kd_Desa',
            'TglPosting',
        ],
    ]) ?>

</div>
