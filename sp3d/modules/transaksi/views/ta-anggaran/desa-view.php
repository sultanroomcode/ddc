<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="ta-anggaran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-anggaran/update?id=<?= $model->KdPosting?>'})" class="btn btn-warning">Update Ta Anggaran</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-anggaran/delete?id=<?= $model->KdPosting?>', urlBack:'ta-anggaran/index', typeSend:'post'})" class="btn btn-warning">Hapus Anggaran</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KdPosting',
            'Tahun',
            'KURincianSD',
            'Kd_Rincian',
            'RincianSD',
            'Anggaran',
            'AnggaranPAK',
            'AnggaranStlhPAK',
            'Belanja',
            'Kd_Keg',
            'SumberDana',
            'Kd_Desa',
            'TglPosting',
        ],
    ]) ?>

</div>
