<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\referensi\RefSBU;
use frontend\models\referensi\RefRekeningKegiatan;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaKegiatan;
use kartik\select2\Select2;

//$belanjaList = ArrayHelper::map(RefSBU::find()->select(['Nama_SBU'])->all(),'Nama_SBU','Nama_SBU');
$kodeList = ArrayHelper::map(RefRekeningKegiatan::find()->all(),'kode','uraian');
$smbrList = ArrayHelper::map(RefSumber::find()->all(),'Kode',function($m){ return $m->Kode.'::'.$m->Nama_Sumber;});
$kegiatanList = ArrayHelper::map(TaKegiatan::find()->select(['Kd_Keg', 'Nama_Kegiatan'])->where(['Kd_Desa' => $id])->all(),'Kd_Keg','Nama_Kegiatan');
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaAnggaran */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaAnggaran */
/* @var $generator string helper base name with camel to id Inflector ta-anggaran */
/* @var $generator string helper base name with camel to word Inflector Ta Anggaran */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->TglPosting = date('Y-m-d H:i:s');
    $model->Anggaran = $model->AnggaranStlhPAK = $model->AnggaranPAK = 0;
}
?>

<div class="ta-anggaran-form animated slideInUp">
    <?php if(count($kegiatanList) > 0){ ?>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-warning">List Data Anggaran</a>
    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'KdPosting')->dropdownList(['1' => 'Pendapatan', '2' => 'Kegiatan', '3' => 'Pembelanjaan']) ?>

            <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?php //KURincianSD = Kode Kegiatan + Kode Rincian + Kode SumberDana ?>
            <?= $form->field($model, 'KURincianSD')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Kd_Rincian')->dropdownList($kodeList) ?>
            <?php //RincianSD = Kode Rincian + Kode SumberDana ?>
            <?= $form->field($model, 'RincianSD')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Anggaran')->textInput(['readonly' => 'readonly']) ?>

            <?= $form->field($model, 'AnggaranPAK')->textInput(['readonly' => 'readonly']) ?>

            <?= $form->field($model, 'AnggaranStlhPAK')->textInput(['readonly' => 'readonly']) ?>

            <?= $form->field($model, 'Belanja')->textInput() ?>
            
            <?php /* $form->field($model, 'Belanja')->widget(Select2::classname(), [
                'data' => $belanjaList,
                'options' => ['placeholder' => 'Pilih Belanja'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])*/ ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Kd_Keg')->dropdownList($kegiatanList) ?>

            <?= $form->field($model, 'SumberDana')->dropdownList($smbrList) ?>

            <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'TglPosting')->hiddenInput()->label(false) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php
    //KEGIATAN CHECK
    } else {
        ?>
        <h2>Mohon isi kegiatan dulu</h2>
        Kegiatan dibutuhkan untuk mengisi data Anggaran. Untuk membuat kegiatan <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-primary btn-sm">klik disini</a>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-index?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-warning">Kembali Ke Halaman Anggaran</a>
        <?php
    }
    ?>
</div>
<?php $script = <<<JS
//regularly-ajax
var smbdn = $('#taanggaran-sumberdana').val();
var kdkeg = $('#taanggaran-kd_keg').val();
var kdrin = $('#taanggaran-kd_rincian').val();

$('#taanggaran-kurinciansd').val(kdkeg+kdrin+smbdn);
$('#taanggaran-rinciansd').val(kdrin+smbdn);

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : '/transaksi/ta-anggaran/desa-index?id={$id}&tahun={$tahun}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);