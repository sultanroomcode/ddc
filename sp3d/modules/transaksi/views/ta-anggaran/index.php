<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaksi Anggaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-anggaran-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/ta-anggaran/create'})" class="btn btn-warning">Buat Ta Anggaran</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'KdPosting',
            'Tahun',
            'KURincianSD',
            'Kd_Rincian',
            'RincianSD',
            // 'Anggaran',
            // 'AnggaranPAK',
            // 'AnggaranStlhPAK',
            // 'Belanja',
            // 'Kd_Keg',
            // 'SumberDana',
            // 'Kd_Desa',
            // 'TglPosting',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
