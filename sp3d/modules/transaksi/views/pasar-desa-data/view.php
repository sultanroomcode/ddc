<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\models\PasardesaData */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Pasardesa Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasardesa-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'id_pasar',
            'kd_isian',
            'jml_pedagang',
            'jml_jenis_barang',
            'pras_ruko',
            'pras_kios',
            'pras_los',
            'pras_lapak',
            'pras_lesehan',
            'pras_kantor',
            'pras_aset',
            'pras_pendukung',
            'pengelola',
            'keterangan',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
