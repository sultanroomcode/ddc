<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\models\PasardesaData */

$this->title = 'Isi Data Pasar Desa';
$this->params['breadcrumbs'][] = ['label' => 'Pasardesa Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasardesa-data-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
