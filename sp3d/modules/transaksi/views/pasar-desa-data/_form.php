<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
    'id' => $model->formName(), 
    'layout' => 'horizontal',
    'fieldConfig' => [
        'enableError' => true,
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            // 'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ]
];

$arrTemplate = ['item' => function($index, $label, $name, $checked, $value) {
    $return = '<label class="containerx">';
    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.(($checked)?'checked':'').' tabindex="3">';
    $return .= '<i></i>';
    $return .= '<span class="checkmark"></span>';
    $return .= '<span>' . ucwords($label) . '</span>';
    $return .= '</label>';

    return $return;
}];

if($model->isNewRecord){
    $model->kd_desa = $data['kd_desa'];
    $model->id_pasar = $data['id_pasar'];
    $model->kd_isian = time();

}

$urlback = '/transaksi/pasar-desa/view?kd_desa='.$data['kd_desa'].'&id_pasar='.$data['id_pasar'];
?>

<div class="pasardesa-data-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_pasar')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'kd_isian')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'jml_pedagang')->textInput() ?>

    <?= $form->field($model, 'jml_jenis_barang')->textInput() ?>

    <?= $form->field($model, 'pras_ruko')->textInput() ?>

    <?= $form->field($model, 'pras_kios')->textInput() ?>

    <?= $form->field($model, 'pras_los')->textInput() ?>

    <?= $form->field($model, 'pras_lapak')->textInput() ?>

    <?= $form->field($model, 'pras_lesehan')->textInput() ?>

    <?= $form->field($model, 'pras_kantor')->radioList(['ada' => 'Ada', 'tidak' => 'Tidak'],$arrTemplate)->label('Prasarana Kantor') ?>

    <?= $form->field($model, 'pras_aset')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pras_pendukung')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pengelola')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#pasar-desa-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);