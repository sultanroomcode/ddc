<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\models\PasardesaData */

$this->title = 'Update Data Pasar : ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Pasardesa Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_pasar' => $model->id_pasar, 'kd_isian' => $model->kd_isian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pasardesa-data-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
