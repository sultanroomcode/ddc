<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefRekeningPendapatan;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaKegiatan;

$sumberList = ArrayHelper::map(RefSumber::find()->all(),'Kode',function($m){ return $m->Kode. ' | ' .$m->Nama_Sumber; });
$kegiatanList = ArrayHelper::map(TaKegiatan::find()->where(['Kd_Desa' => $id])->all(),'Kd_Keg','Nama_Kegiatan');
$kegList = ArrayHelper::map(RefRekeningPendapatan::find()->where(['tipe' => '1'])->all(),'kode','uraian');
//how to add item on first order in array?
$kegList = array_merge(['0' => '---'],$kegList);
$formatCurrency = [
                'options' => [
                    'onchange' => 'updateCount()',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ];
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Keg = $kd2;
    $model->No_Keg = $nokeg;
    $model->Kd_Rincian = $kd;
    $model->Kd_SubRinci = '01';
    $model->AnggaranPAK = $model->AnggaranPAKMask = 0;
    $model->Satuan = 'Ls';
    $model->kegiatan = 'belanja';
    $model->Kd_Desa = $id;

    $model->Uraian = $model->rab->detailkegiatan->Nama_Kegiatan;

    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Anggaran = $model->AnggaranPAK = $model->AnggaranStlhPAK = $model->JmlSatuan = $model->HrgSatuan = $model->JmlSatuanPAK = $model->HrgSatuanPAK = $model->AnggaranStlhPAK = 0;
    $model->setNewNumber();
}
?>
<!-- Box Comment -->
<div class="box box-widget">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel RAB</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
<div class="ta-rab-form">
    <?php
     $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kegiatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_SubRinci')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'No_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Rincian')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'No_Urut')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'SumberDana')->dropdownList($sumberList) ?>

            <?= $form->field($model, 'Uraian')->textarea(['maxlength' => true]) ?>            
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'JmlSatuan')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'JmlSatuanMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>

            <?= $form->field($model, 'HrgSatuan')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'HrgSatuanMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>

            <?= $form->field($model, 'Anggaran')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'AnggaranMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'JmlSatuanPAK')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            <?= $form->field($model, 'JmlSatuanPAKMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>

            <?= $form->field($model, 'HrgSatuanPAK')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            <?= $form->field($model, 'HrgSatuanPAKMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>

            <?= $form->field($model, 'AnggaranPAK')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            <?= $form->field($model, 'AnggaranPAKMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>

            <?= $form->field($model, 'AnggaranStlhPAK')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>            
            <?= $form->field($model, 'AnggaranStlhPAKMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>            

            <?= $form->field($model, 'Kode_SBU')->textInput(['maxlength' => true]) ?>
        </div>
    </div>   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Isi Belanja' : 'Update Belanja', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
</div>

<?php $script = <<<JS
function updateCount(){
    var s1 = $('#tarabrinci-jmlsatuanmask').val().replace(/\./g, "").replace(/\,/g, ".");
    var s2 = $('#tarabrinci-hrgsatuanmask').val().replace(/\./g, "").replace(/\,/g, ".");

    $('#tarabrinci-jmlsatuan,#tarabrinci-jmlsatuanpak, #tarabrinci-jmlsatuanpakmask').val(s1);    
    $('#tarabrinci-hrgsatuan,#tarabrinci-hrgsatuanpak, #tarabrinci-hrgsatuanpakmask').val(s2);
    $('#tarabrinci-anggaran, #tarabrinci-anggaranstlhpak,#tarabrinci-anggaranmask, #tarabrinci-anggaranstlhpakmask').val(s1*s2);
}

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            //goLoad({elm:'#desa-container',url : '/transaksi/ta-rab/desa-view?id={$id}&tahun={$tahun}&kd={$kd2}&kd2={$kd}&effect=lightSpeedIn'});
            Custombox.modal.close();
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);