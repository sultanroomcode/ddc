<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget <?=($effect!=null)?'animated '.$effect:''?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Belanja</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<div class="col-md-6">
        <?php
        echo '<b>No. Urut :</b><br>'.$model->No_Urut.'<br>';
        echo '<b>Sumber Dana :</b><br>'.$model->SumberDana.'<br>';
        echo '<b>Uraian :</b><br>'.$model->Uraian.'<br>';
        echo '<b>Satuan :</b><br>'.$model->Satuan.'<br>';
        echo '<b>Jumlah :</b><br>'.$model->nf($model->JmlSatuan).'<br>';
        echo '<b>Harga :</b><br>'.$model->nf($model->HrgSatuan).'<br>';
        ?>
    </div>
    <div class="col-md-6">
        <?php
        echo '<b>Anggaran :</b><br>'.$model->nf($model->Anggaran).'<br>';
        echo '<b>Jumlah (PAK) :</b><br>'.$model->nf($model->JmlSatuanPAK).'<br>';
        echo '<b>Harga (PAK) :</b><br>'.$model->nf($model->HrgSatuanPAK).'<br>';
        echo '<b>Anggaran (PAK) :</b><br>'.$model->nf($model->AnggaranPAK).'<br>';
        echo '<b>Anggaran (Setelah PAK) :</b><br>'.$model->nf($model->AnggaranStlhPAK).'<br>';
        ?>
    </div>
</div>
</div>