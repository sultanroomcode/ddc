<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<hr>
<div class="row">
    <div class="col-md-6">
        <?php
        echo '<b>No. Urut :</b><br>'.$model->No_Urut.'<br>';
        echo '<b>Sumber Dana :</b><br>'.$model->SumberDana.'<br>';
        echo '<b>Uraian :</b><br>'.$model->Uraian.'<br>';
        echo '<b>Satuan :</b><br>'.$model->Satuan.'<br>';
        echo '<b>Jumlah :</b><br>'.$model->nf($model->JmlSatuan).'<br>';
        echo '<b>Harga :</b><br>'.$model->nf($model->HrgSatuan).'<br>';
        ?>
    </div>
    <div class="col-md-6">
        <?php
        echo '<b>Anggaran :</b><br>'.$model->nf($model->Anggaran).'<br>';
        echo '<b>Jumlah (PAK) :</b><br>'.$model->nf($model->JmlSatuanPAK).'<br>';
        echo '<b>Harga (PAK) :</b><br>'.$model->nf($model->HrgSatuanPAK).'<br>';
        echo '<b>Anggaran (PAK) :</b><br>'.$model->nf($model->AnggaranPAK).'<br>';
        echo '<b>Anggaran (Setelah PAK) :</b><br>'.$model->nf($model->AnggaranStlhPAK).'<br>';
        ?>
    </div>
</div>
