<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel RAB</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/desa-page?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah RAB</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-rab-table" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>Kode Rincian</th>
	            <th>Kode Kegiatan</th>
	            <th>Anggaran</th>
	            <th>Anggaran PAK</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kode Rincian</th>
	            <th>Kode Kegiatan</th>
	            <th>Anggaran</th>
	            <th>Anggaran PAK</th>
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->Kd_Rincian ?></td>
	            <td><?= $v->Kd_Keg ?></td>
	            <td align="right"><?= number_format(round($v->Anggaran), 0, ',','.') ?> </td>
	            <td align="right"><?= number_format(round($v->AnggaranPAK), 0, ',','.') ?> </td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RAB Desa?', urlSend:'/transaksi/ta-rab/desa-delete?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>', urlBack:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>	            	

	            	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&kd2=<?=$v->Kd_Rincian?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-rab-table').DataTable();
JS;

$this->registerJs($scripts);