<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefRekeningPendapatan;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaKegiatan;

$sumberList = ArrayHelper::map(RefSumber::find()->all(),'Kode',function($m){ return $m->Kode. ' | ' .$m->Nama_Sumber; });
$kegiatanList = ArrayHelper::map(TaKegiatan::find()->where(['Kd_Desa' => $id])->all(),'Kd_Keg','Nama_Kegiatan');
$kegList = ArrayHelper::map(RefRekeningPendapatan::find()->where(['tipe' => '1'])->all(),'kode','uraian');
//how to add item on first order in array?
$kegList = array_merge(['0' => '---'],$kegList);
$formatCurrency = [
                'options' => [
                    'onchange' => 'updateCount()',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ];
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Keg = $kd2;
    $model->Kd_Rincian = $kd;
    $model->Kd_SubRinci = '01';
    $model->AnggaranPAK = $model->AnggaranPAKMask = 0;
    $model->Satuan = 'Ls';
    $model->kegiatan = 'belanja';
    $model->Kd_Desa = $id;

    $model->Uraian = $model->rab->detailkegiatan->Nama_Kegiatan;

    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Anggaran = $model->AnggaranPAK = $model->AnggaranStlhPAK = $model->JmlSatuan = $model->HrgSatuan = $model->JmlSatuanPAK = $model->HrgSatuanPAK = $model->AnggaranStlhPAK = 0;
    $model->setNewNumber();
}
?>

<div class="ta-rabrinci-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-rabrinci/index'})" class="btn btn-warning">List Ta Rabrinci</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Rincian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_SubRinci')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_Urut')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SumberDana')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Uraian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JmlSatuan')->textInput() ?>

    <?= $form->field($model, 'HrgSatuan')->textInput() ?>

    <?= $form->field($model, 'Anggaran')->textInput() ?>

    <?= $form->field($model, 'JmlSatuanPAK')->textInput() ?>

    <?= $form->field($model, 'HrgSatuanPAK')->textInput() ?>

    <?= $form->field($model, 'AnggaranStlhPAK')->textInput() ?>

    <?= $form->field($model, 'AnggaranPAK')->textInput() ?>

    <?= $form->field($model, 'Kode_SBU')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-rab/desa-view?id={$id}&tahun={$tahun}&kd={$kd2}&kd2={$kd}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);