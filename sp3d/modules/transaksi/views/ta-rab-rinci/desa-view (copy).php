<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-rab-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List RAB Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data RAB Desa?', urlSend:'/transaksi/ta-rab/desa-delete?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$model->Kd_Keg?>&kd2=<?=$model->Kd_Rincian?>', urlBack:'/transaksi/ta-rab/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus RAB</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Rincian',
            'Anggaran',
            'AnggaranPAK',
            'AnggaranStlhPAK',
        ],
    ]) ?>


    <h2>Rincian</h2>
    <?php if($model->rincian != null){  ?>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/isi-rincian-pendapatan?id=<?=$model->Kd_Desa?>&tahun=<?=$model->Tahun?>&kd_rincian=<?=$model->Kd_Rincian?>'})" class="btn btn-info btn-sm">Buat Rincian Baru</a><br>

    <table id="ta-rincian-in-rab-table" class="display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>Kode</th>               
                <th>Nama</th>
                <th>Lokasi</th>
                <th>Waktu</th>
                <th>Pagu</th>
                
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th>Lokasi</th>
                <th>Waktu</th>
                <th>Pagu</th>

                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($model->kegiatan as $vk): ?>
            <tr>
                <td><?= $vk->Kd_Keg ?></td>
                <td><?= $vk->Nama_Kegiatan ?></td>
                <td><?= $vk->Lokasi ?></td>
                <td><?= $vk->Waktu ?></td>
                <td align="right"><?= number_format(round($vk->Pagu), 0, ',','.') ?></td>
                
                <td>
                    <?php //sementara di hidden dulu if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-update')): ?>
                    <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-kegiatan/desa-update?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>'})" class="label label-warning">Update</a> -->
                    <?php //endif; ?>

                    <?php if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-view')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/desa-view?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>'})" class="label label-warning">View</a>
                    <?php endif; ?>

                    <?php if(Mimin::checkRoute('transaksi/ta-rab-rinci/desa-delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Kegiatan Desa?', urlSend:'/transaksi/ta-rab-rinci/desa-delete?id=<?=$id?>&tahun=<?=$vk->Tahun?>&kd=<?=$vk->Kd_Keg?>', urlBack:'/transaksi/ta-rab/desa-view?kd=<?=$model->Kd_Bid?>&id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
                    <?php endif; ?>   
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php
$scripts =<<<JS
    $('#ta-rincian-in-rab-table').DataTable();
JS;

$this->registerJs($scripts);

    } else { ?>
    Rincian di dalam Pendapatan ini masih kosong<br><a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-rab-rinci/isi-rincian-pendapatan?id=<?=$model->Kd_Desa?>&tahun=<?=$model->Tahun?>&kd_rincian=<?=$model->Kd_Rincian?>'})" class="btn btn-info btn-sm">Klik Untuk Membuat Rincian Baru</a>
    <?php } ?>
</div>
