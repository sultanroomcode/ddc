<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRABRinci */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rabrincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rabrinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_SubRinci' => $model->Kd_SubRinci, 'No_Urut' => $model->No_Urut], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_SubRinci' => $model->Kd_SubRinci, 'No_Urut' => $model->No_Urut], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rabrinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Rabrinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-rabrinci/delete?id=<?= $model->id?>', urlBack:'ta-rabrinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Rabrinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Rincian',
            'Kd_SubRinci',
            'No_Urut',
            'SumberDana',
            'Uraian',
            'Satuan',
            'JmlSatuan',
            'HrgSatuan',
            'Anggaran',
            'JmlSatuanPAK',
            'HrgSatuanPAK',
            'AnggaranStlhPAK',
            'AnggaranPAK',
            'Kode_SBU',
        ],
    ]) ?>

</div>
