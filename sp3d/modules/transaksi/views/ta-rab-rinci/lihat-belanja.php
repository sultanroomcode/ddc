<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget <?=($effect!=null)?'animated '.$effect:''?>">
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="openModalX({url:'/transaksi/ta-rab-rinci/desa-create?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$dataProvider->ID_Keg?>&kd2=<?=$dataProvider->Kd_Keg?>', opened:'belanja', nokeg:''})" class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> <i class="fa fa-shopping-cart"></i></a>
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#ta-kegiatan-belanja-index', url:'/transaksi/ta-rab-rinci/lihat-belanja?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$kd ?>&effect=slideInRight'})" class="btn btn-sm btn-info"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-shopping-cart"></i> Rincian</a><hr>
	
  	<table id="ta-rab-lihat-belanja" class="display table table-bordered" width="100%">
	    <thead>
	        <tr>
	            <th>Kode</th>	            
	            <th>Uraian</th>
	            <th>Anggaran</th>
	            
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kode</th>	            
	            <th>Uraian</th>
	            <th>Anggaran</th>
	            

	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->belanjaRinci as $v): ?>
	        <tr>
	            <td><?= $v->No_Urut ?></td>
	            <td><?= $v->Uraian ?></td>
	            <td align="right"><?= $v->nf($v->Anggaran) ?></td>
	            
	            <td>
	            	<a href="javascript:void(0)" onclick="openModal({url:'/transaksi/ta-rab-rinci/update-belanja-rincian?id=<?=$v->Kd_Desa?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&nourut=<?=$v->No_Urut?>', nokeg:'', opened:'belanja'})" class="label label-warning"><i class="fa fa-pencil"></i></a>

	            	<a href="javascript:void(0)" onclick="openModal({url:'/transaksi/ta-rab-rinci/lihat-belanja-rincian?id=<?=$v->Kd_Desa?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&nourut=<?=$v->No_Urut?>', nokeg:'<?=$v->No_Keg?>', opened:'belanja'})" class="label label-info"><i class="fa fa-eye"></i></a>

	            	<?php if(Mimin::checkRoute('transaksi/ta-kegiatan/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#ta-kegiatan-belanja-index',msg:'Hapus Data Kegiatan Desa?', urlSend:'/transaksi/ta-rab-rinci/hapus-belanja-rincian?id=<?=$v->Kd_Desa?>&tahun=<?=$v->Tahun?>&kd=<?=$v->Kd_Keg?>&nourut=<?=$v->No_Urut?>', urlBack:'/transaksi/ta-rab-rinci/lihat-belanja?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$kd ?>&effect=rubberBand', typeSend:'get'})" class="label label-danger"><i class="fa fa-trash"></i></a>
	            	<?php endif; ?>	
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-rab-lihat-belanja').DataTable();
JS;

$this->registerJs($scripts);