<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\referensi\RefRekeningBelanja;
use frontend\models\referensi\RefRekeningPendapatan;
use frontend\models\referensi\RefSumber;
use frontend\models\transaksi\TaKegiatan;

$sumberList = ArrayHelper::map(RefSumber::find()->all(),'Kode',function($m){ return $m->Kode. ' | ' .$m->Nama_Sumber; });
$kegiatanList = ArrayHelper::map(TaKegiatan::find()->where(['Kd_Desa' => $id])->all(),'Kd_Keg','Nama_Kegiatan');
$jbelanjaList = ArrayHelper::map(RefRekeningBelanja::find()->where(['tipe' => 1])->all(),'kode','uraian'); 
$kegList = ArrayHelper::map(RefRekeningPendapatan::find()->where(['tipe' => '1'])->all(),'kode','uraian');
//how to add item on first order in array?
$kegList = array_merge(['0' => '---'],$kegList);
$formatCurrency = [
                'options' => [
                    'onchange' => 'updateCount()',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ];
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Tahun = $tahun;
    $model->Kd_Keg = $kd2;
    $model->No_Keg = $nokeg;
    $model->Kd_Rincian = $kd;
    $model->Kd_SubRinci = '01';
    $model->AnggaranPAK = $model->AnggaranPAKMask = 0;
    $model->Satuan = 'Ls';
    $model->kegiatan = 'belanja';
    $model->Kd_Desa = $id;

    $kd_jns = '';
    //$model->Uraian = $model->rab->detailkegiatan->Nama_Kegiatan;

    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Anggaran = $model->AnggaranPAK = $model->AnggaranStlhPAK = $model->JmlSatuan = $model->HrgSatuan = $model->JmlSatuanPAK = $model->HrgSatuanPAK = $model->AnggaranStlhPAK = 0;
    $model->setNewNumber();
} else {
    $model->jenis_belanja_xtd1 = $kd_jns = $model->jenis_belanja;
    $model->jenis_belanja = substr($model->jenis_belanja, 0, 2);
    $model->JmlSatuanMask = $model->JmlSatuan;
    $model->HrgSatuanMask = $model->HrgSatuan;
}
?>
<!-- Box Comment -->
<div class="box box-widget">
<div class="box-body">
<div class="ta-rab-form">
    <?php
     $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kegiatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_SubRinci')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'No_Keg')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Kd_Rincian')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Anggaran')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'AnggaranPAK')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'AnggaranStlhPAK')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'JmlSatuanPAK')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'HrgSatuanPAK')->hiddenInput(['maxlength' => true])->label(false) ?>
    
    <div class="row">
        <div class="col-md-6">
            <?php if($model->isNewRecord){ ?>
                <?= $form->field($model, 'No_Urut')->textInput(['maxlength' => true]) ?>
            <?php } else {
                echo 'Kode Jenis Belanja Lama : '. $model->jenis_belanja;
                } ?>
            <?= $form->field($model, 'jenis_belanja')->dropdownList($jbelanjaList, ['onchange' => 'getLevel2()']) ?>

            <?= $form->field($model, 'jenis_belanja_xtd1')->dropdownList(['' => '---'], ['class' => 'form-control selectpickear']) ?>

            <?= $form->field($model, 'SumberDana')->dropdownList($sumberList) ?>

                   
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'Uraian')->textarea(['maxlength' => true]) ?>
            <?= $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'JmlSatuan')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'JmlSatuanMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>

            <?= $form->field($model, 'HrgSatuan')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'HrgSatuanMask')->widget(\yii\widgets\MaskedInput::className(), $formatCurrency) ?>

            <?= $form->field($model, 'Kode_SBU')->hiddenInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Isi Belanja' : 'Update Belanja', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    Nb : <b>Esc</b> Untuk Keluar.
    <?php ActiveForm::end(); ?>
</div>
</div>
</div>

<?php
if($model->isNewRecord){
    $state = 'baru';
} else {
    $state = 'update';
}

$script = <<<JS
var state = '{$state}';
$('.selectpicker').selectpicker({
    liveSearch:true,
    showTick:true
});

function getLevel2(){
    var urlid = $('#tarabrinci-jenis_belanja').val();
    $.post(base_url+'/data-umum/referensi?jenis=belanja&kode='+urlid+'&form=1').done(function(res){
        $('#tarabrinci-jenis_belanja_xtd1').html(res);
        //selectpickerdestroy first
        $('.selectpicker').selectpicker('refresh');
        if(state == 'update'){
            $('#tarabrinci-jenis_belanja_xtd1').val('{$kd_jns}');
        }
    });
}

getLevel2();

function updateCount(){
    var s1 = $('#tarabrinci-jmlsatuanmask').val().replace(/\./g, "").replace(/\,/g, ".");
    var s2 = $('#tarabrinci-hrgsatuanmask').val().replace(/\./g, "").replace(/\,/g, ".");

    $('#tarabrinci-jmlsatuan,#tarabrinci-jmlsatuanpak, #tarabrinci-jmlsatuanpakmask').val(s1);    
    $('#tarabrinci-hrgsatuan,#tarabrinci-hrgsatuanpak, #tarabrinci-hrgsatuanpakmask').val(s2);
    $('#tarabrinci-anggaran, #tarabrinci-anggaranstlhpak,#tarabrinci-anggaranmask, #tarabrinci-anggaranstlhpakmask').val(s1*s2);
}

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            //Custombox.modal.close();
            goLoad({elm:'#form-engine-x', url:'/transaksi/ta-rab-rinci/desa-create?id={$model->Kd_Desa}&tahun={$model->Tahun}&kd={$model->Kd_Rincian}&nokeg={$model->No_Keg}&kd2={$model->Kd_Keg}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);