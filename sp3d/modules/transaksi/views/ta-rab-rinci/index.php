<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Rabrincis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-rabrinci-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-rabrinci/create'})" class="btn btn-warning">Buat Ta Rabrinci</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'Kd_Keg',
            'Kd_Rincian',
            'Kd_SubRinci',
            // 'No_Urut',
            // 'SumberDana',
            // 'Uraian',
            // 'Satuan',
            // 'JmlSatuan',
            // 'HrgSatuan',
            // 'Anggaran',
            // 'JmlSatuanPAK',
            // 'HrgSatuanPAK',
            // 'AnggaranStlhPAK',
            // 'AnggaranPAK',
            // 'Kode_SBU',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
