<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaRABRinci */

$this->title = 'Update Ta Rabrinci: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Rabrincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Keg' => $model->Kd_Keg, 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_SubRinci' => $model->Kd_SubRinci, 'No_Urut' => $model->No_Urut]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-rabrinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
