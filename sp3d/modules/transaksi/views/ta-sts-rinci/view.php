<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSTSRinci */

$this->title = $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Stsrincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-stsrinci-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'No_Bukti' => $model->No_Bukti, 'No_TBP' => $model->No_TBP], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'No_Bukti' => $model->No_Bukti, 'No_TBP' => $model->No_TBP], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-stsrinci/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Stsrinci</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-stsrinci/delete?id=<?= $model->id?>', urlBack:'ta-stsrinci/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Stsrinci</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_Bukti',
            'No_TBP',
            'Uraian',
            'Nilai',
        ],
    ]) ?>

</div>
