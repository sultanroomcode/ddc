<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Stsrincis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-stsrinci-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-stsrinci/create'})" class="btn btn-warning">Buat Ta Stsrinci</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'kd_kecamatan',
            'No_Bukti',
            'No_TBP',
            // 'Uraian',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
