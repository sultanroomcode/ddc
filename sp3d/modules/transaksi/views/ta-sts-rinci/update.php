<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSTSRinci */

$this->title = 'Update Ta Stsrinci: ' . $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Stsrincis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_Bukti, 'url' => ['view', 'No_Bukti' => $model->No_Bukti, 'No_TBP' => $model->No_TBP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-stsrinci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
