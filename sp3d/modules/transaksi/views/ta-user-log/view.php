<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserLog */

$this->title = $model->TglLogin;
$this->params['breadcrumbs'][] = ['label' => 'Ta User Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'TglLogin' => $model->TglLogin, 'Komputer' => $model->Komputer, 'UserID' => $model->UserID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'TglLogin' => $model->TglLogin, 'Komputer' => $model->Komputer, 'UserID' => $model->UserID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-log/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta User Log</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-user-log/delete?id=<?= $model->id?>', urlBack:'ta-user-log/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta User Log</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'TglLogin',
            'Komputer',
            'UserID',
            'Tahun',
            'AplVer',
            'DbVer',
        ],
    ]) ?>

</div>
