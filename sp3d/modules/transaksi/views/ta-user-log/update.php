<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserLog */

$this->title = 'Update Ta User Log: ' . $model->TglLogin;
$this->params['breadcrumbs'][] = ['label' => 'Ta User Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TglLogin, 'url' => ['view', 'TglLogin' => $model->TglLogin, 'Komputer' => $model->Komputer, 'UserID' => $model->UserID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-user-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
