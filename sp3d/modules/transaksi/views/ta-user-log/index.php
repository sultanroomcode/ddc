<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta User Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-log/create'})" class="btn btn-warning">Buat Ta User Log</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'TglLogin',
            'Komputer',
            'UserID',
            'Tahun',
            'AplVer',
            // 'DbVer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
