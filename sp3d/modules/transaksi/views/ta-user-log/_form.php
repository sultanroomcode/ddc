<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserLog */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaUserLog */
/* @var $generator string helper base name with camel to id Inflector ta-user-log */
/* @var $generator string helper base name with camel to word Inflector Ta User Log */
?>

<div class="ta-user-log-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-log/index'})" class="btn btn-warning">List Ta User Log</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'TglLogin')->textInput() ?>

    <?= $form->field($model, 'Komputer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'AplVer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DbVer')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-user-log/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta User Log')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-user-log/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);