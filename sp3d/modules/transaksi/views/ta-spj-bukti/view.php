<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJBukti */

$this->title = $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjbuktis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjbukti-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_Bukti], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_Bukti], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjbukti/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spjbukti</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spjbukti/delete?id=<?= $model->id?>', urlBack:'ta-spjbukti/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spjbukti</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_SPJ',
            'Kd_Keg',
            'Kd_Rincian',
            'No_Bukti',
            'Tgl_Bukti',
            'Sumberdana',
            'Kd_Desa',
            'kd_kecamatan',
            'Nm_Penerima',
            'Alamat',
            'Rek_Bank',
            'Nm_Bank',
            'NPWP',
            'Keterangan',
            'Nilai',
        ],
    ]) ?>

</div>
