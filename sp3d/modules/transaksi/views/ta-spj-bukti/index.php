<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Spjbuktis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spjbukti-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spjbukti/create'})" class="btn btn-warning">Buat Ta Spjbukti</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'No_SPJ',
            'Kd_Keg',
            'Kd_Rincian',
            'No_Bukti',
            // 'Tgl_Bukti',
            // 'Sumberdana',
            // 'Kd_Desa',
            // 'kd_kecamatan',
            // 'Nm_Penerima',
            // 'Alamat',
            // 'Rek_Bank',
            // 'Nm_Bank',
            // 'NPWP',
            // 'Keterangan',
            // 'Nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
