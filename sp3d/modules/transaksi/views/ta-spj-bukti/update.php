<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJBukti */

$this->title = 'Update Ta Spjbukti: ' . $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjbuktis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_Bukti, 'url' => ['view', 'id' => $model->No_Bukti]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spjbukti-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
