<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel TBP</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container',url: '/transaksi/default/penata-usahaan-desa?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-primary animated slideInLeft"><i class="fa fa-refresh"></i> Kembali</a>

  	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah TBP</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-tbp-table" class="display table" cellspacing="0" width="100%">
	    <thead>
	        <tr>
	            <th>No TBP</th>
	            <th>Keterangan</th>
	            <th>Jumlah</th>

	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>No TBP</th>
	            <th>Keterangan</th>
	            <th>Jumlah</th>

	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <th><?= $v->No_Bukti ?></th>
	            <th><?= $v->Uraian ?></th>
	            <td align="right"><?= number_format(round($v->Jumlah), 0, ',','.') ?> </td>
	            
	            <th>
	            	<?php if(Mimin::checkRoute('transaksi/ta-tbp/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data?', urlSend:'/transaksi/ta-tbp/desa-delete?id=<?=$id?>&kd=<?=$v->No_Bukti?>&tahun=<?=$v->Tahun?>', urlBack:'/transaksi/ta-tbp/desa-index?id=<?=$id?>&tahun=<?=$v->Tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-tbp/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-update?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->No_Bukti?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-tbp/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-view?id=<?=$id?>&tahun=<?=$v->Tahun?>&kd=<?=$v->No_Bukti?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>

	            	<a href="<?= Url::to(['/data-umum/pdf?bagian=data-tbp-satu&kode='.$v->No_Bukti.'&kd_desa='.$id], true) ?>" target="_blank" class="label label-info">PDF</a>
	            </th>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-tbp-table').DataTable();
JS;

$this->registerJs($scripts);