<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTBP */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaTBP */
/* @var $generator string helper base name with camel to id Inflector ta-tbp */
/* @var $generator string helper base name with camel to word Inflector Ta Tbp */
?>

<div class="ta-tbp-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ta-tbp/index'})" class="btn btn-warning">List Ta Tbp</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_Bukti')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tgl_Bukti')->textInput() ?>

    <?= $form->field($model, 'Kd_Desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_kecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Uraian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nm_Penyetor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Alamat_Penyetor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TTD_Penyetor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NoRek_Bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama_Bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jumlah')->textInput() ?>

    <?= $form->field($model, 'Nm_Bendahara')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jbt_Bendahara')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'KdBayar')->textInput() ?>

    <?= $form->field($model, 'Ref_Bayar')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ta-tbp/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ta Tbp')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ta-tbp/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);