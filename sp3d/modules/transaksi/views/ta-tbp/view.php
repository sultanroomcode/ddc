<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaTBP */

$this->title = $model->No_Bukti;
$this->params['breadcrumbs'][] = ['label' => 'Ta Tbps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-tbp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_Bukti], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_Bukti], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-tbp/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Tbp</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-tbp/delete?id=<?= $model->id?>', urlBack:'ta-tbp/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Tbp</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_Bukti',
            'Tgl_Bukti',
            'Kd_Desa',
            'kd_kecamatan',
            'Uraian',
            'Nm_Penyetor',
            'Alamat_Penyetor',
            'TTD_Penyetor',
            'NoRek_Bank',
            'Nama_Bank',
            'Jumlah',
            'Nm_Bendahara',
            'Jbt_Bendahara',
            'Status',
            'KdBayar',
            'Ref_Bayar',
        ],
    ]) ?>

</div>
