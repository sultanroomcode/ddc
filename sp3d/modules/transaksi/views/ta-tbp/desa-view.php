<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-tbp-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-tbp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List TBP Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data TBP Desa?', urlSend:'/transaksi/ta-tbp/desa-delete?id=<?=$id?>&kd=<?=$model->No_Bukti?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-tbp/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus TBP Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_Bukti',
            'Tgl_Bukti',
            'Kd_Desa',
            'kd_kecamatan',
            'Uraian',
            'Nm_Penyetor',
            'Alamat_Penyetor',
            'TTD_Penyetor',
            'NoRek_Bank',
            'Nama_Bank',
            'Jumlah',
            'Nm_Bendahara',
            'Jbt_Bendahara',
            'Status',
            'KdBayar',
            'Ref_Bayar',
        ],
    ]) ?>

</div>
