<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserID */

$this->title = 'Create Ta User Id';
$this->params['breadcrumbs'][] = ['label' => 'Ta User Ids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-id-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
