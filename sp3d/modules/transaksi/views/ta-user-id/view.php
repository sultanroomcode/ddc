<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaUserID */

$this->title = $model->UserID;
$this->params['breadcrumbs'][] = ['label' => 'Ta User Ids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-user-id-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->UserID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->UserID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-user-id/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta User Id</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-user-id/delete?id=<?= $model->id?>', urlBack:'ta-user-id/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta User Id</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'UserID',
            'Nama',
            'Password',
            'Level',
            'Keterangan',
        ],
    ]) ?>

</div>
