<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Spjs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spj-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spj/create'})" class="btn btn-warning">Buat Ta Spj</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'No_SPJ',
            'Tgl_SPJ',
            'Kd_Desa',
            'kd_kecamatan',
            // 'No_SPP',
            // 'Keterangan',
            // 'Jumlah',
            // 'Potongan',
            // 'Status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
