<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJ */

$this->title = 'Update Ta Spj: ' . $model->No_SPJ;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_SPJ, 'url' => ['view', 'id' => $model->No_SPJ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-spj-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
