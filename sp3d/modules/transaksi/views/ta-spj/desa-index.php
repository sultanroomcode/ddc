<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel SPJ</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/penata-usahaan-desa?tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-sm btn-primary animated slideInLeft"><i class="fa fa-refresh"></i> Kembali</a>

  	<?php if(Mimin::checkRoute('transaksi/ta-rab/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-create?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-warning">Tambah SPJ</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-spj-table" class="display table" cellspacing="0" width="100%">
	    <thead>
	        <tr>
	            <th>No SPJ</th>
	            <th>Keterangan</th>
	            <th>Jumlah</th>

	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>No SPJ</th>
	            <th>Keterangan</th>
	            <th>Jumlah</th>

	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <th><?= $v->No_SPJ ?></th>
	            <th><?= $v->Keterangan ?></th>
	            <td align="right"><?= number_format(round($v->Jumlah), 0, ',','.') ?> </td>

	            <th>
	            	<?php if(Mimin::checkRoute('transaksi/ta-spj/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data?', urlSend:'/transaksi/ta-spj/desa-delete?id=<?=$id?>&kd=<?=$v->No_SPJ?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-spj/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-spj/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-update?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$v->No_SPJ?>'})" class="label label-warning">Update</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-spj/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-view?id=<?=$id?>&tahun=<?=$tahun?>&kd=<?=$v->No_SPJ?>'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </th>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-spj-table').DataTable();
JS;

$this->registerJs($scripts);