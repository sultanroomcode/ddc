<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
?>
<div class="ta-spj-view animated slideInLeft">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List SPJ Desa</a>

        <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data SPJ Desa?', urlSend:'/transaksi/ta-spj/desa-delete?id=<?=$id?>&kd=<?=$model->No_SPJ?>&tahun=<?=$tahun?>', urlBack:'/transaksi/ta-spj/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn', typeSend:'get'})" class="btn btn-warning">Hapus SPJ Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_SPJ',
            'Tgl_SPJ',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'Keterangan',
            'Jumlah',
            'Potongan',
            'Status',
        ],
    ]) ?>

</div>
