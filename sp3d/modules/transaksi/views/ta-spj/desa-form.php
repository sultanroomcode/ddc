<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name TaBidang */
/* @var $generator string helper base name with camel to id Inflector ta-rab */
/* @var $generator string helper base name with camel to word Inflector Ta Bidang */
if($model->isNewRecord){
    $model->Kd_Desa = $id;
    $idkec = explode('.', $id);
    $model->kd_kecamatan = $idkec[0];
    $model->Tahun = $tahun;
}
?>

<div class="ta-spj-form animated slideInUp">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-spj/desa-index?id=<?=$id?>&tahun=<?=$tahun?>&effect=lightSpeedIn'})" class="btn btn-warning">List SPJ</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'Kd_Desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'Tahun')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'No_SPJ')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tgl_SPJ')->textInput() ?>

    <?= $form->field($model, 'No_SPP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Keterangan')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jumlah')->textInput() ?>

    <?= $form->field($model, 'Potongan')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <br>
</div>
<?php $script = <<<JS
$('#taspj-tgl_spj').datetimepicker({
    format:'Y-m-d H:i',
    mask:true
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#desa-container',url : '/transaksi/ta-spj/desa-index?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);