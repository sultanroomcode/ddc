<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\transaksi\TaSPJ */

$this->title = $model->No_SPJ;
$this->params['breadcrumbs'][] = ['label' => 'Ta Spjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-spj-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->No_SPJ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->No_SPJ], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ta-spj/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ta Spj</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ta-spj/delete?id=<?= $model->id?>', urlBack:'ta-spj/index', typeSend:'post'})" class="btn btn-warning">Hapus Ta Spj</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_SPJ',
            'Tgl_SPJ',
            'Kd_Desa',
            'kd_kecamatan',
            'No_SPP',
            'Keterangan',
            'Jumlah',
            'Potongan',
            'Status',
        ],
    ]) ?>

</div>
