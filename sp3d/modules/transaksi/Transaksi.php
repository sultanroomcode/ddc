<?php
namespace sp3d\modules\transaksi;

class Transaksi extends \yii\base\Module
{
    public $controllerNamespace = 'sp3d\modules\transaksi\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
