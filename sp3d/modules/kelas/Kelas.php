<?php

namespace sp3d\modules\kelas;

/**
 * kelas module definition class
 */
use Yii;
class Kelas extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\kelas\controllers';
}
