<?php
namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "gelombang".
 *
 * @property int $id_gelombang
 * @property int $id_kelas
 * @property string $nama_gelombang
 * @property string $status
 * @property string $dimulai_tanggal
 * @property string $berakhir_tanggal
 * @property int $max_ikut
 * @property string $dibuat_oleh
 * @property string $diupdate_oleh
 * @property string $dibuat_tanggal
 * @property string $diupdate_tanggal
 *
 * @property Kelas $kelas
 * @property Ikut[] $ikuts
 */
class Gelombang extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $getdata;
    public static function tableName()
    {
        return 'gelombang';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'dibuat_oleh',
                 'updatedByAttribute' => 'diupdate_oleh',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal', 'diupdate_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['diupdate_tanggal'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kelas', 'max_ikut'], 'integer'],
            [['dimulai_tanggal', 'berakhir_tanggal',], 'safe'],
            [['nama_gelombang'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 10],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_gelombang' => 'Id Gelombang',
            'id_kelas' => 'Id Kelas',
            'nama_gelombang' => 'Nama Gelombang',
            'status' => 'Status',
            'dimulai_tanggal' => 'Dimulai Tanggal',
            'berakhir_tanggal' => 'Berakhir Tanggal',
            'max_ikut' => 'Max Ikut',
            'dibuat_oleh' => 'Dibuat Oleh',
            'diupdate_oleh' => 'Diupdate Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
            'diupdate_tanggal' => 'Diupdate Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIkuts()
    {
        return $this->hasMany(Ikut::className(), ['id_gelombang' => 'id_gelombang']);
    }

    public function getThreads()
    {
        return $this->hasMany(KelasThread::className(), ['id_gelombang' => 'id_gelombang']);
    }
}
