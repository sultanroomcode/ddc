<?php

namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kelas".
 *
 * @property int $id_kelas
 * @property string $nama_kelas
 * @property string $deskripsi
 * @property string $status
 * @property string $dibuat_oleh
 * @property string $diupdate_oleh
 * @property string $dibuat_tanggal
 * @property string $diupdate_tanggal
 *
 * @property Gelombang[] $gelombangs
 * @property KelasTopik[] $kelasTopiks
 */
class Kelas extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $getdata;
    public static function tableName()
    {
        return 'kelas';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => ['dibuat_oleh', 'diupdate_oleh'],
                 'updatedByAttribute' => ['diupdate_oleh'],
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal', 'diupdate_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['diupdate_tanggal'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deskripsi'], 'string'],
            [['nama_kelas'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 10]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kelas' => 'Id Kelas',
            'nama_kelas' => 'Nama Kelas',
            'deskripsi' => 'Deskripsi',
            'status' => 'Status',
            'dibuat_oleh' => 'Dibuat Oleh',
            'diupdate_oleh' => 'Diupdate Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
            'diupdate_tanggal' => 'Diupdate Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGelombangs()
    {
        return $this->hasMany(Gelombang::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelasTopiks()
    {
        return $this->hasMany(KelasTopik::className(), ['id_kelas' => 'id_kelas']);
    }

    public function getForumumum()
    {
        return $this->hasMany(KelasThread::className(), ['id_kelas' => 'id_kelas'])->onCondition(['id_gelombang' => null]);
    }
}
