<?php
namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use ddcop\models\Operator;
/**
 * This is the model class for table "kelas_permintaan".
 *
 * @property int $id_permintaan
 * @property int $id_topik
 * @property string $user
 * @property string $dibuat_tanggal
 *
 * @property KelasTopik $topik
 */
class KelasPermintaan extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $c;
    public static function tableName()
    {
        return 'kelas_permintaan';
    }

    public function behaviors()
    {
        return ['timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kelas'], 'integer'],
            [['user', 'operator'], 'string', 'max' => 32],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
            
            [['status'], 'string', 'max' => 10]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_permintaan' => 'Id Permintaan',
            'id_kelas' => 'Id Topik',
            'user' => 'User',
            'dibuat_tanggal' => 'Dibuat Tanggal',
        ];
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            $this->kd_kecamatan = substr($this->user, 0, 7);
            $this->kd_kabupaten = substr($this->kd_kecamatan, 0, 4);
            return true;
        } else {
            return false;
        }
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }


    public function getIkut()
    {
        return $this->hasOne(Ikut::className(), ['id_permintaan' => 'id_permintaan']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'user'])->onCondition(['type' => 'desa']);
    }

    public function getOperatordesa()
    {
        return $this->hasOne(Operator::className(), ['id' => 'operator']);
    }
}
