<?php
namespace sp3d\modules\kelas\models;

use Yii;

use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kelas_topik_objek".
 *
 * @property int $id_objek
 * @property int $id_topik
 * @property string $konten
 * @property string $status
 * @property string $dibuat_oleh
 * @property string $diupdate_oleh
 * @property string $dibuat_tanggal
 * @property string $diupdate_tanggal
 *
 * @property KelasTopik $topik
 */
class KelasTopikObjek extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas_topik_objek';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'dibuat_oleh',
                 'updatedByAttribute' => 'diupdate_oleh',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal', 'diupdate_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['diupdate_tanggal'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_topik', 'urutan'], 'integer'],
            [['konten'], 'string'],
            [['status'], 'string', 'max' => 10],
            [['id_topik'], 'exist', 'skipOnError' => true, 'targetClass' => KelasTopik::className(), 'targetAttribute' => ['id_topik' => 'id_topik']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_objek' => 'Id Objek',
            'id_topik' => 'Id Topik',
            'konten' => 'Konten',
            'status' => 'Status',
            'urutan' => 'Urutan',
            'dibuat_oleh' => 'Dibuat Oleh',
            'diupdate_oleh' => 'Diupdate Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
            'diupdate_tanggal' => 'Diupdate Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopik()
    {
        return $this->hasOne(KelasTopik::className(), ['id_topik' => 'id_topik']);
    }
}
