<?php
namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kelas_topik".
 *
 * @property int $id_topik
 * @property int $id_kelas
 * @property string $deskripsi
 * @property string $status
 * @property string $dibuat_oleh
 * @property string $diupdate_oleh
 * @property string $dibuat_tanggal
 * @property string $diupdate_tanggal
 *
 * @property Kelas $kelas
 * @property KelasTopikObjek[] $kelasTopikObjeks
 */
class KelasTopik extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas_topik';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'dibuat_oleh',
                 'updatedByAttribute' => 'diupdate_oleh',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal', 'diupdate_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['diupdate_tanggal'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kelas', 'urutan'], 'integer'],
            [['deskripsi'], 'string'],
            [['judul'], 'string', 'min' => 5, 'max' => 255],
            [['status'], 'string', 'max' => 10],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_topik' => 'Id Topik',
            'id_kelas' => 'Id Kelas',
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'status' => 'Status',
            'dibuat_oleh' => 'Dibuat Oleh',
            'diupdate_oleh' => 'Diupdate Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
            'diupdate_tanggal' => 'Diupdate Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelasTopikObjeks()
    {
        return $this->hasMany(KelasTopikObjek::className(), ['id_topik' => 'id_topik']);
    }
}
