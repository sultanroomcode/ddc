<?php
namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kelas_thread".
 *
 * @property int $id_thread
 * @property int $id_gelombang
 * @property string $message
 * @property string $dibuat_oleh
 * @property string $dibuat_tanggal
 *
 * @property Gelombang $gelombang
 * @property KelasThreadReply[] $kelasThreadReplies
 */
class KelasThread extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $getdata;
    public static function tableName()
    {
        return 'kelas_thread';
    }

    public function behaviors()
    {
        return [
            /*[
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => ['dibuat_oleh'],
                 'updatedByAttribute' => null,
            ],*/
            'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_gelombang','id_kelas'], 'integer'],
            [['message'], 'string', 'max' => 255],
            // [['dibuat_oleh'], 'string', 'max' => 32],
            [['id_gelombang'], 'exist', 'skipOnError' => true, 'targetClass' => Gelombang::className(), 'targetAttribute' => ['id_gelombang' => 'id_gelombang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_thread' => 'Id Thread',
            'id_gelombang' => 'Id Gelombang',
            'message' => 'Message',
            'dibuat_oleh' => 'Dibuat Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGelombang()
    {
        return $this->hasOne(Gelombang::className(), ['id_gelombang' => 'id_gelombang']);
    }

    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReplies()
    {
        return $this->hasMany(KelasThreadReply::className(), ['id_thread' => 'id_thread']);
    }

    public function getPembuat()
    {
        return $this->hasOne(KelasUser::className(), ['id_user_kelas' => 'dibuat_oleh']);
    }
}
