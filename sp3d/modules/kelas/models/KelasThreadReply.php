<?php
namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "kelas_thread_reply".
 *
 * @property int $id_replay
 * @property int $id_thread
 * @property int $root
 * @property string $message
 * @property string $dibuat_oleh
 * @property string $dibuat_tanggal
 *
 * @property KelasThread $thread
 */
class KelasThreadReply extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas_thread_reply';
    }

    public function behaviors()
    {
        return [
            // [
            //      'class' => BlameableBehavior::className(),
            //      'createdByAttribute' => 'dibuat_oleh',
            //      'updatedByAttribute' => null,
            // ],
            'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_thread', 'root'], 'integer'],
            [['message'], 'string', 'max' => 255],
            // [['dibuat_oleh'], 'string', 'max' => 32],
            [['id_thread'], 'exist', 'skipOnError' => true, 'targetClass' => KelasThread::className(), 'targetAttribute' => ['id_thread' => 'id_thread']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_replay' => 'Id Replay',
            'id_thread' => 'Id Thread',
            'root' => 'Root',
            'message' => 'Message',
            'dibuat_oleh' => 'Dibuat Oleh',
            'dibuat_tanggal' => 'Dibuat Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThread()
    {
        return $this->hasOne(KelasThread::className(), ['id_thread' => 'id_thread']);
    }

    public function getPembuat()
    {
        return $this->hasOne(KelasUser::className(), ['id_user_kelas' => 'dibuat_oleh']);
    }
}
