<?php
namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
/**
 * This is the model class for table "ikut".
 *
 * @property int $id_ikut
 * @property int $id_gelombang
 * @property int $id_kelas
 * @property string $status
 * @property string $dibuat_tanggal
 * @property string $diupdate_tanggal
 *
 * @property Gelombang $gelombang
 */
class Ikut extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $getdata;
    public static function tableName()
    {
        return 'ikut';
    }

    public function behaviors()
    {
         return [
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal', 'diupdate_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['diupdate_tanggal'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kelas', 'id_permintaan'], 'required'],
            [['id_gelombang', 'id_kelas', 'id_permintaan'], 'integer'],
            [['status'], 'string', 'max' => 10],
            [['user'], 'string', 'max' => 32],
            // ['user', 'exist','targetAttribute' => ['id_kelas','id_gelombang', 'user']],
            [['id_gelombang'], 'exist', 'skipOnError' => true, 'targetClass' => Gelombang::className(), 'targetAttribute' => ['id_gelombang' => 'id_gelombang']],
        ];
    }

    // https://stackoverflow.com/questions/42627989/yii2-exists-validator-with-filter
    //perlu dipelajari

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ikut' => 'ID Ikut',
            'id_gelombang' => 'ID Gelombang',
            'id_kelas' => 'ID Kelas',
            'id_permintaan' => 'ID Permintaan',
            'user' => 'Pengguna',
            'status' => 'Status',
            'dibuat_tanggal' => 'Dibuat Tanggal',
            'diupdate_tanggal' => 'Diupdate Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGelombang()
    {
        return $this->hasOne(Gelombang::className(), ['id_gelombang' => 'id_gelombang']);
    }

    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    public function getPermintaan()
    {
        return $this->hasOne(KelasPermintaan::className(), ['id_permintaan' => 'id_permintaan']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'user'])->onCondition(['type' => 'desa']);
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            $m = KelasPermintaan::findOne(['id_permintaan' => $this->id_permintaan, 'id_kelas' => $this->id_kelas]);
            $m->status = 'ACTIVE';
            $this->user = $m->user;
            $this->kd_kecamatan = substr($this->user, 0, 7);
            $this->kd_kabupaten = substr($this->kd_kecamatan, 0, 4);
            $m->save();

            return true;
        } else {
            return false;
        }
    } 

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $m = KelasPermintaan::findOne(['id_permintaan' => $this->id_permintaan, 'id_kelas' => $this->id_kelas]);
        $m->status = 'REQUEST';
        $m->save();
        return true;
    }
}
