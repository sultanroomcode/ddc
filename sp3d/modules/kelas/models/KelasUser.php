<?php
namespace sp3d\modules\kelas\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "kelas_user".
 *
 * @property int $id_user_kelas
 * @property string $nama_display
 * @property string $linked_id
 * @property string $kd_desa
 * @property string $tipe_user
 * @property string $status
 * @property string $dibuat_tanggal
 * @property string $diupdate_tanggal
 */
class KelasUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas_user';
    }

    public function behaviors()
    {
         return [
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['dibuat_tanggal', 'diupdate_tanggal'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['diupdate_tanggal'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_display', 'linked_id'], 'required'],
            [['nama_display'], 'string', 'max' => 250],
            [['linked_id'], 'string', 'max' => 32],
            [['kd_desa', 'tipe_user', 'status'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user_kelas' => 'Id User Kelas',
            'nama_display' => 'Nama Display',
            'linked_id' => 'Linked ID',
            'kd_desa' => 'Kd Desa',
            'tipe_user' => 'Tipe User',
            'status' => 'Status',
            'dibuat_tanggal' => 'Dibuat Tanggal',
            'diupdate_tanggal' => 'Diupdate Tanggal',
        ];
    }
}
