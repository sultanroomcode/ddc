<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ikuts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ikut-index">
    <table class="table table-hover table-striped">
        <tr>
            <th>ID</th>
            <th>Nama Kelas</th>
            <th>Gelombang</th>
            <th>Status</th>
            <th>Tanggal Masuk</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($model->all() as $v): ?>
        <tr>
            <td><?= $v->id_ikut ?></td>
            <td><?= $v->kelas->nama_kelas ?></td>
            <td><?= $v->gelombang->nama_gelombang ?></td>
            <td><?= $v->status ?></td>
            <td><?= $v->dibuat_tanggal ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-kelas?id=<?= $v->id_ikut ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
