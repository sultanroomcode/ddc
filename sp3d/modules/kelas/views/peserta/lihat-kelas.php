<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Kelas */

$this->title = 'Kelas : '.$model->kelas->nama_kelas;
$this->params['breadcrumbs'][] = ['label' => 'Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <hr>
    <div id="topik-kelas-area"></div>
</div>
<?php
$scripts =<<<JS
    goLoad({elm:'#topik-kelas-area', url:'/kelas/peserta/lihat-topik?id={$model->id_ikut}'});
JS;

$this->registerJs($scripts);