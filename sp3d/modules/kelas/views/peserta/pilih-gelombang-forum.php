<div class="kelas-topik-objek-index">
    <div class="row">
        <h3>Pilih Gelombang Forum</h3>

        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/pilih-gelombang-forum?peserta=1'})" class="btn btn-success"><i class="fa fa-plus"></i> Forum Kelas</a>

    	<table class="table table-striped">
            <thead>
                <tr>
                    <th>Gelombang</th>
                    <th>Kelas</th>
                    <th>Status Gelombang</th>
                    <th>Jumlah Forum</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($model->all() as $v): ?>
                <tr>
        	        <td><?= $v->gelombang->nama_gelombang ?></td>
                    <td><?= $v->kelas->nama_kelas ?></td>
                    <td><?= $v->gelombang->status ?></td>
                    <td><?= count($v->gelombang->threads) ?></td>
                    <td>
                        <?php if($v->status == 'ACTIVE' && $v->gelombang->status == 'ACTIVE'){ ?>
                        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-daftar-forum?id=<?=$v->id_gelombang?>'})" class="btn btn-danger">Lihat Daftar Forum</a>
                        <?php } ?>
                    </td>
                </tr> 
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>