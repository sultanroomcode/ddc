<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Klinik Desa</h2>
            <div class="p-10" id="kelas-area">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
switch ($data['getdata']['page']) {
	case 'lihat-permintaan-kelas':
		$kli_url = '/kelas/peserta/lihat-permintaan-kelas';
	break;

	case 'kelas-ikuti':
		$kli_url = '/kelas/peserta/peserta-viewbox';
	break;

	case 'forum':
		$kli_url = '/kelas/peserta/pilih-gelombang-forum';
	break;

	case 'forum-umum':
		$kli_url = '/kelas/peserta/forum-umum';
	break;

	case 'faq':
		$kli_url = '/kelas/peserta/faq';
	break;
	
	default:
		$kli_url = '/kelas/peserta/ambil-kelas';
	break;
}
$scripts = <<<JS
goLoad({elm:'#kelas-area', url:'{$kli_url}'});
JS;

$this->registerJs($scripts);
//https://code.tutsplus.com/tutorials/how-to-create-a-phpmysql-powered-forum-from-scratch--net-10188