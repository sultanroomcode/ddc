<div class="kelas-topik-objek-index">
    <div class="row">
        <h3>Permintaan Kelas</h3>

        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/ambil-kelas'})" class="btn btn-danger">Tambah Permintaan Kelas</a><br>

    	<table class="table table-striped">
        <?php foreach($model->all() as $v): ?>
        <tr>
	        <td><?= $v->id_permintaan ?></td>
            <td><?= $v->kelas->nama_kelas ?></td>
            <td><?= $v->dibuat_tanggal ?></td>
            <td><?= $v->status ?></td>
            <td>
                <?php if($v->status == 'ACTIVE'){ ?>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-kelas?id=<?=$v->ikut->id_ikut?>'})" class="btn btn-danger">Lihat Kelas</a>
                <?php } ?>

                <?php if($v->status == 'REQUEST'){ ?>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/hapus-permintaan-kelas?id=<?=$v->id_permintaan?>'})" class="btn btn-danger">Hapus</a>
                <?php } ?>
            </td>
        </tr> 
        <?php endforeach; ?>
        </table>
    </div>
</div>