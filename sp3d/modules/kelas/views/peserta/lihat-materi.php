<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelas Topik Objeks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-objek-index">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-kelas?id=<?= $modelikut->id_ikut ?>'})" class="btn btn-success"><i class="fa fa-backward"></i></a>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-materi?id_topik=<?= $modeltopik->id_topik ?>&id_ikut=<?= $modelikut->id_ikut ?>'})" class="btn btn-success"><i class="fa fa-spin fa-refresh"></i></a>
    <div class="row">
        <?php foreach($model->all() as $v): ?>
        <div class="col-md-12"><?= $v->konten ?></div>
        <?php endforeach; ?>
    </div>
</div>
