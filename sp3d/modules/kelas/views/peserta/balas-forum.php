<?php 
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $modelf->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-1',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-11',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/kelas/peserta/lihat-balasan-forum?id_gelombang='.$model->id_gelombang.'&id_forum='.$modelf->id_thread;
$urlback = 'goLoad({elm:\'#place-replies-forum\', url:\''.$url.'\'});';

if(isset($umum) == 1){
    $url = '/kelas/peserta/lihat-balasan-forum-umum?id_kelas='.$model->id_kelas.'&id_forum='.$modelf->id_thread;
    $urlback = 'goLoad({elm:\'#place-replies-forum\', url:\''.$url.'\'});';
}
?>

<div class="gelombang-form row">
    <div class="col-md-12">
    <?php // var_dump($modelf->getdata) ?>
    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($modelf, 'id_thread')->hiddenInput()->label(false) ?>
    <?= $form->field($modelf, 'dibuat_oleh')->hiddenInput()->label(false) ?>

    <?= $form->field($modelf, 'root')->hiddenInput()->label(false) ?>

    <?= $form->field($modelf, 'message')->textarea(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-7">
            <?= Html::submitButton('<i class="fa fa-paper-plane-o faa-float"></i> Balas', ['class' => 'faa-parent animated-hover btn btn-success']) ?>
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$modelf->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(result){
        res = JSON.parse(result);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert(res.message);
            {$urlback}
            $('#form-replies-forum').html('.')
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);
?>