<div class="kelas-topik-objek-index">
    <div class="row">
        <h3>Daftar Forum</h3>

    	<table class="table table-striped">
            <thead>
                <tr>
                    <th>Topik</th>
                    <th>Dibuat Oleh</th>
                    <th>Jumlah Balasan</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($model->threads as $v): ?>
                <tr>
        	        <td><?= $v->message ?></td>
                    <td><?= $v->pembuat->nama_display ?><br><?= $v->dibuat_tanggal ?></td>
                    <td><?= count($v->replies) ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="goPopup({welm:'700px', url:'/kelas/peserta/lihat-forum?id_gelombang=<?=$v->id_gelombang?>&id_forum=<?=$v->id_thread?>'})" class="btn btn-success faa-parent animated-hover"><i class="fa fa-comments faa-bounce"></i></a>
                    </td>
                </tr> 
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>