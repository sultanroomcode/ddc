<?php 
if($data['insert']){
	echo "<h3 class='text-center'>Berhasil mendaftar pada kelas</h3>";
	$kli_url = '/kelas/peserta/lihat-permintaan-kelas';
}

if($data['max']){
	echo "<h3 class='text-center'>Anda sudah memilih 3 Materi</h3>";
	$kli_url = '/kelas/peserta/lihat-permintaan-kelas';
}

if($data['exist']){
	echo "<h3 class='text-center'>Sudah pernah ambil kelas</h3>";
	$kli_url = '/kelas/peserta/ambil-kelas';
}

$scripts = <<<JS
setTimeout(function(){ 
	goLoad({elm:'#kelas-area', url:'{$kli_url}'});
}, 3000);
JS;

$this->registerJs($scripts);