<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelas Topiks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-index">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-kelas?id=<?= $model->id_ikut ?>'})" class="btn btn-success"><i class="fa fa-spin fa-refresh"></i></a>

    <table class="table table-hover table-striped">
        <tr>
            <th>Judul</th>
            <th>Deskripsi</th>
            <th>Status</th>
            <th>Urutan</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($modelkelas->kelasTopiks as $v): ?>
        <tr>
            <td><?= $v->judul ?></td>
            <td><?= $v->deskripsi ?></td>
            <td><?= $v->status ?></td>
            <td><?= $v->urutan ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-materi?id_topik=<?= $v->id_topik ?>&id_ikut=<?=$model->id_ikut?>'})" class="btn btn-success"><i class="fa fa-eye"></i> Lihat Materi</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
