<div class="kelas-topik-objek-index">
    <div class="row">
    	<table class="table table-striped">
        <?php foreach($model->all() as $v): ?>
        <tr>
	        <td><?= $v->nama_kelas ?></td>
	        <td><a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/ambil-kelas-no?id=<?=$v->id_kelas?>'})" class="btn btn-danger">Gabung Kelas</a></td>
        </tr> 
        <?php endforeach; ?>
        </table>
    </div>
</div>