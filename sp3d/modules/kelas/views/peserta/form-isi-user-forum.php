<?php 
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-8',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/kelas/peserta/dashboard?page=lihat-permintaan-kelas';
$urlback = 'goLoad({url:\''.$url.'\'})';
?>
<div class="row">
    <div class="col-md-6">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Data User Forum</h2>
            <div class="p-10" id="kelas-area">
                <div class="col-md-12">
                <?php // var_dump($model->getdata) ?>
                <?php $form = ActiveForm::begin($arrFormConfig); ?>

                <?= $form->field($model, 'linked_id')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'tipe_user')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'status')->hiddenInput(['maxlength' => true])->label(false) ?>

                <?= $form->field($model, 'nama_display')->textInput(['maxlength' => true]) ?>

                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-8">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                        <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
                    </div>
                </div>
                
                <?php ActiveForm::end(); ?>
                </div>
                &nbsp;
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(result){
        res = JSON.parse(result);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert(res.message);
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($scripts);