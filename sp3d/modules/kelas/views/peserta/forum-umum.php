<div class="kelas-topik-objek-index">
    <div class="row">
        <h3>Daftar Forum</h3>

        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/pilih-kelas-forum?peserta=1'})" class="btn btn-success"><i class="fa fa-plus"></i> Forum Umum</a>

    	<table class="table table-striped">
            <thead>
                <tr>
                    <th>Nama Kelas</th>
                    <th>Jumlah Forum</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($model->all() as $v): ?>
                <tr>
        	        <td><?= $v->nama_kelas ?></td>
                    <td><?= count($v->forumumum) ?></td>
                    <td>
                        <?php if($v->status == 'ACTIVE'){ ?>
                        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/peserta/lihat-daftar-forum-umum?id=<?=$v->id_kelas?>'})" class="btn btn-danger">Lihat Daftar Forum</a>
                        <?php } ?>
                    </td>
                </tr> 
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>