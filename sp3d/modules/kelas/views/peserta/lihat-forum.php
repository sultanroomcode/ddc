<div id="kelas-forum-area-chat">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-forum-area-chat', url:'/kelas/peserta/lihat-forum?id_gelombang=<?= $model->id_gelombang ?>&id_forum=<?= $modelf->id_thread ?>'})" class="btn btn-success"><i class="fa fa-spin fa-refresh"></i></a>

    <div class="row">
    	<table class="table table-striped">
            <thead>
                <tr>
                    <th>Pesan</th>
                    <th>Dibuat</th>
                    <th>Aksi</th>
                </tr> 
            </thead>

            <tbody>
                <tr>
                    <td><?=$modelf->message?></td>
                    <td><?= $modelf->pembuat->nama_display ?><br><?= $modelf->dibuat_tanggal ?></td>
                    <td><a href="javascript:void(0)" title="Balas" onclick="goLoad({elm:'#form-replies-forum', url:'/kelas/peserta/balas-forum?id_gelombang=<?= $model->id_gelombang ?>&id_forum=<?= $modelf->id_thread ?>'})" class="btn btn-success faa-parent animated-hover"><i class="fa fa-commenting faa-bounce"></i></a></td>
                </tr>

                <tr>
                    <td colspan="2" id="form-replies-forum">                       
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="table table-striped" id="place-replies-forum">
        </table>
    </div>
</div>
<?php 
$url = '/kelas/peserta/lihat-balasan-forum?id_gelombang='.$model->id_gelombang.'&id_forum='.$modelf->id_thread;
$script = <<<JS
function reloadBalasan(){
    goLoad({elm:'#place-replies-forum', url:'{$url}'});
}

reloadBalasan();
JS;

$this->registerJs($script);
?>