<h3>Sesuatu yang anda cari tidak ada/sudah dihapus</h3>
<?php 
$kli_url = '/kelas/peserta/peserta-viewbox';
if(isset($data) && isset($data['urlback'])){
	$kli_url = $data['urlback'];
}
$scripts = <<<JS
setTimeout(function(){ 
    goLoad({elm:'#kelas-area', url:'{$kli_url}'});
}, 3000);
JS;

$this->registerJs($scripts);