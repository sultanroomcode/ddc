<?php 
$data = $model->delete();
?>
<div class="kelas-topik-objek-index">
    <div class="row">
    	<h3 class="text-center">Permintaan gabung kelas sudah dihapus</h3>
    </div>
</div>
<?php 
$kli_url = '/kelas/peserta/lihat-permintaan-kelas';
$scripts = <<<JS
setTimeout(function(){ 
    goLoad({elm:'#kelas-area', url:'{$kli_url}'});
}, 3000);
JS;

$this->registerJs($scripts);