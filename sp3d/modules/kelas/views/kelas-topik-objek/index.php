<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelas Topik Objeks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-objek-index">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-topik-objek-area', url:'/kelas/kelas-topik-objek/create?id=<?= $id->id_topik ?>'})" class="btn btn-success"><i class="fa fa-plus"></i> Materi</a>

    <table class="table table-hover table-striped">
        <tr>
            <th>Konten</th>
            <th>Status</th>
            <th>Urutan</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($model->all() as $v): ?>
        <tr>
            <td><?= $v->konten ?></td>
            <td><?= $v->status ?></td>
            <td><?= $v->urutan ?></td>
            <td>
                <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-topik-objek-area', url:'/kelas/kelas-topik-objek/view?id=<?= $v->id_topik ?>'})" class="btn btn-success"><i class="fa fa-search-plus"></i></a> -->
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-topik-objek-area', url:'/kelas/kelas-topik-objek/update?id=<?= $v->id_objek ?>&id_topik=<?=$v->id_topik?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0)" onclick="deleteData('#kelas-topik-objek-area', '/kelas/kelas-topik-objek/delete?id=<?=$v->id_objek ?>','/kelas/kelas-topik-objek/index?id=<?=$v->id_topik?>','Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
