<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\KelasTopikObjek */

$this->title = 'Create Kelas Topik Objek';
$this->params['breadcrumbs'][] = ['label' => 'Kelas Topik Objeks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-objek-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
