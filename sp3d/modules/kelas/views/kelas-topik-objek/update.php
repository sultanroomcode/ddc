<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\KelasTopikObjek */

$this->title = 'Update Kelas Topik Objek: ' . $model->id_objek;
$this->params['breadcrumbs'][] = ['label' => 'Kelas Topik Objeks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_objek, 'url' => ['view', 'id' => $model->id_objek]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kelas-topik-objek-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
