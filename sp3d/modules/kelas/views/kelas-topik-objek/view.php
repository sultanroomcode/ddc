<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\KelasTopikObjek */

$this->title = $model->id_objek;
$this->params['breadcrumbs'][] = ['label' => 'Kelas Topik Objeks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-objek-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_objek], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_objek], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_objek',
            'id_topik',
            'konten:ntext',
            'status',
            'dibuat_oleh',
            'diupdate_oleh',
            'dibuat_tanggal',
            'diupdate_tanggal',
        ],
    ]) ?>

</div>
