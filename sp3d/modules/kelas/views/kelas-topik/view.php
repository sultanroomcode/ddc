<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\KelasTopik */

$this->title = $model->id_topik;
$this->params['breadcrumbs'][] = ['label' => 'Kelas Topiks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-view">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#topik-kelas-area', url:'/kelas/kelas-topik/index?id=<?= $model->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-list"></i></a>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#topik-kelas-area', url:'/kelas/kelas-topik/update?id=<?= $model->id_topik ?>&id_kelas=<?= $model->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_topik',
            'id_kelas',
            'judul',
            'deskripsi:ntext',
            'status',
            'urutan',
            'dibuat_oleh',
            'diupdate_oleh',
            'dibuat_tanggal',
            'diupdate_tanggal',
        ],
    ]) ?>
    
    <div id="kelas-topik-objek-area"></div>
</div>
<?php
$scripts =<<<JS
    goLoad({elm:'#kelas-topik-objek-area', url:'/kelas/kelas-topik-objek/index?id={$model->id_topik}'});
JS;

$this->registerJs($scripts);