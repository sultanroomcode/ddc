<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelas Topiks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-index">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#topik-kelas-area', url:'/kelas/kelas-topik/create?id=<?= $id->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-plus"></i> Topik</a>

    <table class="table table-hover table-striped">
        <tr>
            <th>Judul</th>
            <th>Deskripsi</th>
            <th>Status</th>
            <th>Urutan</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($model->all() as $v): ?>
        <tr>
            <td><?= $v->judul ?></td>
            <td><?= $v->deskripsi ?></td>
            <td><?= $v->status ?></td>
            <td><?= $v->urutan ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#topik-kelas-area', url:'/kelas/kelas-topik/view?id=<?= $v->id_topik ?>'})" class="btn btn-success"><i class="fa fa-search-plus"></i></a>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#topik-kelas-area', url:'/kelas/kelas-topik/update?id=<?= $v->id_topik ?>&id_kelas=<?=$v->id_kelas?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0)" onclick="deleteData('#topik-kelas-area', '/kelas/kelas-topik/delete?id=<?= $v->id_topik ?>','/kelas/kelas-topik/index?id=<?=$v->id_kelas?>','Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
