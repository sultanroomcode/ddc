<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\KelasTopik */

$this->title = 'Update Kelas Topik: ' . $model->id_topik;
$this->params['breadcrumbs'][] = ['label' => 'Kelas Topiks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_topik, 'url' => ['view', 'id' => $model->id_topik]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kelas-topik-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
