<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\KelasTopik */

$this->title = 'Create Kelas Topik';
$this->params['breadcrumbs'][] = ['label' => 'Kelas Topiks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-topik-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
