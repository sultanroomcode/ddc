<?php 
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/kelas/gelombang/index?id='.$model->id_kelas;
$urlback = 'goLoad({elm:\'#gelombang-kelas-area\', url:\''.$url.'\'})';
if(isset($model->getdata['request']) && $model->getdata['request'] ==1){
    $url = '/kelas/kelas/list-request-detail?id='.$model->id_kelas;
    $urlback = 'goLoad({elm:\'#kelas-area-permintaan\', url:\''.$url.'\'})';
}

if(isset($model->getdata['back-url']) && !empty($model->getdata['back-url'])){
    $url = '/kelas/kelas/'.$model->getdata['back-url'];
    $urlback = 'goLoad({elm:\'#kelas-area\', url:\''.$url.'\'})';
}
?>

<div class="gelombang-form row">
    <div class="col-md-7">
    <?php // var_dump($model->getdata) ?>
    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'id_kelas')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'nama_gelombang')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropdownList(['ACTIVE' => 'Aktif', 'NONACTIVE' => 'Non-Aktif']) ?>

    <?= $form->field($model, 'dimulai_tanggal')->textInput() ?>

    <?= $form->field($model, 'berakhir_tanggal')->textInput() ?>

    <?= $form->field($model, 'max_ikut')->textInput() ?>

    <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-7">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php 
$script = <<<JS
$('#gelombang-dimulai_tanggal, #gelombang-berakhir_tanggal').datetimepicker({
    format:'Y-m-d H:i',
    mask:true
});
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(result){
        res = JSON.parse(result);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert(res.message);
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);
?>