<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Gelombang */

$this->title = 'Update Gelombang: ' . $model->id_gelombang;
$this->params['breadcrumbs'][] = ['label' => 'Gelombangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_gelombang, 'url' => ['view', 'id' => $model->id_gelombang]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gelombang-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
