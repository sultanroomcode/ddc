<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Gelobang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tab-container tile" id="gelombangtabs">
    <ul class="nav tab nav-tabs">
        <li class="active"><a href="#gelombang-peserta">Peserta</a></li>
        <li><a href="#permintaan-peserta">Permintaan</a></li>
    </ul>
      
    <div class="tab-content">
        <div class="tab-pane active" id="gelombang-peserta">
            
        </div>
        <div class="tab-pane" id="permintaan-peserta">
            aa
        </div>
    </div>
</div>
<?php
$scripts =<<<JS
    goLoad({elm:'#gelombang-peserta', url:'/kelas/ikut/index-only?id_gelombang={$dataProvider->id_gelombang}&id_kelas={$dataProvider->id_kelas}'});
    goLoad({elm:'#permintaan-peserta', url:'/kelas/kelas/list-request-detail-only?id_gelombang={$dataProvider->id_gelombang}&id={$dataProvider->id_kelas}'});
    $('#gelombangtabs').tabs();
JS;

$this->registerJs($scripts);