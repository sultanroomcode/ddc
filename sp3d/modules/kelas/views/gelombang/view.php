<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Gelombang */

$this->title = $model->id_gelombang;
$this->params['breadcrumbs'][] = ['label' => 'Gelombangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gelombang-view">
    <a href="javascript:void(0)" onclick="goLoad({elm:'#gelombang-kelas-area', url:'/kelas/gelombang/index?id=<?= $model->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-list"></i></a>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#gelombang-kelas-area', url:'/kelas/gelombang/update?id=<?= $model->id_gelombang ?>&id_kelas=<?= $model->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_gelombang',
            'id_kelas',
            'nama_gelombang',
            'status',
            'dimulai_tanggal',
            'berakhir_tanggal',
            'max_ikut',
            'dibuat_oleh',
            'diupdate_oleh',
            'dibuat_tanggal',
            'diupdate_tanggal',
        ],
    ]) ?>

    <div id="ikut-area"></div>
</div>
<?php
$scripts =<<<JS
    goLoad({elm:'#ikut-area', url:'/kelas/ikut/index?id_gelombang={$model->id_gelombang}&id_kelas={$model->id_kelas}'});
JS;

$this->registerJs($scripts);