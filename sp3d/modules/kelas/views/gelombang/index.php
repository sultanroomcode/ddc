<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gelombang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gelombang-index">
    <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#gelombang-kelas-area', url:'/kelas/gelombang/create?id=<?= $id->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-plus"></i> Gelombang</a> -->

    <table class="table table-hover table-striped">
        <tr>
            <th>ID</th>
            <th>Nama Gelombang</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Jumlah Max</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($model->all() as $v): ?>
        <tr>
            <td><?= $v->id_gelombang ?></td>
            <td><?= $v->nama_gelombang ?></td>
            <td><?= $v->dimulai_tanggal ?></td>
            <td><?= $v->berakhir_tanggal ?></td>
            <td><?= $v->max_ikut ?></td>
            <td><?= $v->status ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#gelombang-kelas-area', url:'/kelas/gelombang/view?id=<?= $v->id_gelombang ?>&id_kelas=<?=$v->id_kelas?>'})" class="btn btn-success"><i class="fa fa-search-plus"></i></a>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#gelombang-kelas-area', url:'/kelas/gelombang/update?id=<?= $v->id_gelombang ?>&id_kelas=<?=$v->id_kelas?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0)" onclick="deleteData('#gelombang-kelas-area', '/kelas/gelombang/delete?id=<?= $v->id_gelombang ?>','/kelas/gelombang/index?id=<?=$v->id_kelas?>','Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
