<?php 
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/kelas/kelas/list-forum';
$urlback = 'goLoad({elm:\'#kelas-area-forum\', url:\''.$url.'\'})';

if(isset($model->getdata['peserta']) && $model->getdata['peserta'] == 1){
    $url = '/kelas/peserta/pilih-gelombang-forum';
    if($model->id_gelombang == null){
        //umum
        $url = '/kelas/peserta/forum-umum';
    }
    $urlback = 'goLoad({elm:\'#kelas-area\', url:\''.$url.'\'})';
}
?>

<div class="gelombang-form row">
    <div class="col-md-7">
    <?php // var_dump($model->getdata) ?>
    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'id_gelombang')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'message')->textarea(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-7">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(result){
        res = JSON.parse(result);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert(res.message);
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);
?>