<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Gelombang */

$this->title = 'Create Gelombang';
$this->params['breadcrumbs'][] = ['label' => 'Gelombangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gelombang-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
