<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Ikut */

$this->title = 'Update Ikut: ' . $model->id_ikut;
$this->params['breadcrumbs'][] = ['label' => 'Ikuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_ikut, 'url' => ['view', 'id' => $model->id_ikut]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ikut-update">
	<?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
