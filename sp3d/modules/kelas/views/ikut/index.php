<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ikuts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ikut-index">
    <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#ikut-area', url:'/kelas/ikut/create?id_gelombang=<?=$id->id_gelombang?>&id_kelas=<?= $id->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Peserta</a> -->

    <table class="table table-hover table-striped">
        <tr>
            <th>ID</th>
            <th>Status</th>
            <th>ID Pengguna</th>
            <th>Nama Desa</th>
            <th>Tanggal</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($model->all() as $v): ?>
        <tr>
            <td><?= $v->id_ikut ?></td>
            <td><?= $v->status ?></td>
            <td><?= $v->user ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= $v->dibuat_tanggal ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#ikut-area', url:'/kelas/ikut/update?id=<?= $v->id_ikut ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0)" onclick="deleteData('#ikut-area', '/kelas/ikut/delete?id=<?= $v->id_ikut ?>','/kelas/ikut/index?id_gelombang=<?=$v->id_gelombang?>&id_kelas=<?=$v->id_kelas?>','Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
