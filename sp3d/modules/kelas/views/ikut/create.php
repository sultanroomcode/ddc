<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Ikut */

$this->title = 'Create Ikut';
$this->params['breadcrumbs'][] = ['label' => 'Ikuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ikut-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
