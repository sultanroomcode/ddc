<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ikuts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ikut-index">
    <div id="ikut-form-area">
    </div>
    <div id="ikut-table-area">
        <div>
            <input type="text" class="form-control" id="searchingdom">
        </div>
        <table id="ikut-table-dom" class="table table-hover table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Kabupaten</th>
                <th>Kecamatan</th>
                <th>Desa</th>
                <th>Status</th>
                <th>Tanggal Masuk</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
        <?php foreach($model->all() as $v): ?>
            <tr>
                <td><?= $v->id_ikut ?></td>
                <td><?= $v->kabupaten->description ?></td>
                <td><?= $v->kecamatan->description ?></td>
                <td><?= $v->desa->description ?></td>
                <td><?= $v->status ?></td>
                <td><?= $v->dibuat_tanggal ?></td>
                <td>
                    <a href="javascript:void(0)" onclick="changeStatDom(); goLoad({elm:'#ikut-form-area', url:'/kelas/ikut/update?id=<?= $v->id_ikut ?>&popup=1'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                    <a href="javascript:void(0)" onclick="goCall({elm:'#gelombang-peserta', url: '/kelas/ikut/delete?id=<?= $v->id_ikut ?>', urlback:'/kelas/ikut/index-only?id_gelombang=<?=$v->id_gelombang?>&id_kelas=<?=$v->id_kelas?>', msgTitle:'Hapus Peserta?', msg:'Hapus Peserta'})" class="btn btn-success"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php 
$script = <<<JS
$('#ikut-form-area').hide();
function changeStatDom(){
    $('#ikut-form-area').show();
    $('#ikut-table-area').hide();
}
$('#searchingdom').quicksearch('table#ikut-table-dom tbody tr');
JS;

$this->registerJs($script);
?>