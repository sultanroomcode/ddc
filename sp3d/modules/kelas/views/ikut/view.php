<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Ikut */

$this->title = $model->id_ikut;
$this->params['breadcrumbs'][] = ['label' => 'Ikuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ikut-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_ikut], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_ikut], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_ikut',
            'id_gelombang',
            'id_kelas',
            'status',
            'dibuat_tanggal',
            'diupdate_tanggal',
        ],
    ]) ?>

</div>
