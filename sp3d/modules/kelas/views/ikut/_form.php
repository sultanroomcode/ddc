<?php 
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use sp3d\modules\kelas\models\KelasPermintaan;
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$userArr = ArrayHelper::map(KelasPermintaan::find()->where(['status' => 'REQUEST', 'id_kelas' => $model->id_kelas])->orderBy('dibuat_tanggal ASC')->all(), 'id_permintaan', 'desa.description');

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/kelas/ikut/index?id_gelombang='.$model->id_gelombang.'&id_kelas='.$model->id_kelas;
$urlback = 'goLoad({elm:\'#ikut-area\', url:\''.$url.'\'})';
$popup = 0;
if(isset($model->getdata['popup']) && $model->getdata['popup'] == 1){
    $popup = 1;
}
?>

<div class="ikut-form row">
    <div class="col-md-7">
    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'id_gelombang')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'id_kelas')->hiddenInput()->label(false) ?>
    
    <?php if($model->isNewRecord){ ?>
        <?= $form->field($model, 'id_permintaan')->dropdownList($userArr) ?>
    <?php } ?>

    <?= $form->field($model, 'status')->dropdownList(['ACTIVE' => 'Aktif', 'NONACTIVE' => 'Non-Aktif']) ?>
    <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-7">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
            <?php if($popup == 1){ ?>
            <a href="javascript:void(0)" onclick="changeStatDomForm()" class="btn btn-success"><i class="fa fa-backward"></i></a>
            <?php } else { ?>
            <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
            <?php } ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php 
$script = <<<JS
//regularly-ajax
var popup = {$popup};
function changeStatDomForm(){
    $('#ikut-form-area').hide();
    $('#ikut-table-area').show();
}

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(result){
        res = JSON.parse(result);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert(res.message);
            if(popup == 1){
                changeStatDomForm();
            } else {
                {$urlback}
            }
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);
?>