<?php 
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$arr = ['-' => ''];
$kabArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');

$kabArr = $arr + $kabArr;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/kelas/ikut/index?id_gelombang='.$model->id_gelombang.'&id_kelas='.$model->id_kelas;
$urlback = 'goLoad({elm:\'#ikut-area\', url:\''.$url.'\'})';
?>

<div class="ikut-form row">
    <div class="col-md-7">
    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'id_gelombang')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'id_kelas')->hiddenInput()->label(false) ?>
    
    <?php if($model->isNewRecord){ ?>
    <div class="form-group field-ikut-user has-success">
        <label class="control-label col-sm-5" for="ikut-user">Kabupaten</label>
        <div class="col-sm-7">
            <select id="kabupaten" class="form-control">
                <?php foreach($kabArr as $k => $v): ?>
                <option value="<?= $k ?>"><?= $v ?></option>
                <?php endforeach; ?>
            </select>

            <p class="help-block help-block-error "></p>
        </div>
    </div>

    <div class="form-group field-ikut-user has-success">
        <label class="control-label col-sm-5" for="ikut-user">Kabupaten</label>
        <div class="col-sm-7">
            <select id="kecamatan" class="form-control">
                <option value="">-</option>
            </select>

            <p class="help-block help-block-error "></p>
        </div>
    </div>

    <?= $form->field($model, 'user')->dropdownList(['' => '-']) ?>
    <?php } ?>

    <?= $form->field($model, 'status')->dropdownList(['aktif' => 'Aktif', 'nonaktif' => 'Non-Aktif']) ?>

    <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-7">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
            <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php 
$script = <<<JS
$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #ikut-user').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/pendamping/data/data-option?type=kecamatan&kode='+kode});
  }
});

$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#ikut-user').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#ikut-user', url:'/pendamping/data/data-option?type=desa&kode='+kode});
  }
});

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(result){
        res = JSON.parse(result);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert(res.message);
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);
?>