<div class="row">
    <div class="col-md-6">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Kelas</h2>
            <div class="p-10" id="kelas-area">
            </div>  
        </div>
    </div>

    <div class="col-md-6">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Daftar Permintaan</h2>
            <div class="p-10" id="kelas-area-permintaan">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-6">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Daftar Pengguna</h2>
            <div class="p-10" id="kelas-area-pengguna">
            </div>  
        </div>
    </div>

    <div class="col-md-6">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Daftar Forum</h2>
            <div class="p-10" id="kelas-area-forum">
            </div>  
        </div>
    </div>
</div>
<?php
$kpm_url = '/kelas/kelas';
$scripts = <<<JS
goLoad({elm:'#kelas-area', url:'{$kpm_url}'});
goLoad({elm:'#kelas-area-pengguna', url:'/kelas/kelas/list-user'});
goLoad({elm:'#kelas-area-forum', url:'/kelas/kelas/list-forum'});
goLoad({elm:'#kelas-area-permintaan', url:'/kelas/kelas/list-request'});

var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#2A00FF'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}
JS;

$this->registerJs($scripts);