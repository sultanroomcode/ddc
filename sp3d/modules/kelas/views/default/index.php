<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Klinik Bumdes</h2>
            <div class="p-10" id="kelas-area">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$kpm_url = '/kelas/kelas';
$scripts = <<<JS
goLoad({elm:'#kelas-area', url:'{$kpm_url}'});

var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#2A00FF'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}
JS;

$this->registerJs($scripts);