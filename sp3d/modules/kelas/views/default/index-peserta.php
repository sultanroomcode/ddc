<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Kelas</h2>
            <div class="p-10" id="kelas-area">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$kpm_url = '/kelas/peserta/peserta-viewbox';
$scripts = <<<JS
goLoad({elm:'#kelas-area', url:'{$kpm_url}'});
JS;

$this->registerJs($scripts);