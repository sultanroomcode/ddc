<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Kelas */

$this->title = 'Create Kelas';
$this->params['breadcrumbs'][] = ['label' => 'Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
