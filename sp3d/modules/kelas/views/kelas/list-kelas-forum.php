<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pilih Kelas';
$this->params['breadcrumbs'][] = $this->title;
$urlx = '';
$elm = '#kelas-area-forum';
if(isset($getdata['peserta']) && $getdata['peserta'] == 1){
    $urlx = '&peserta=1';
    $elm = '#kelas-area';
}
?>
<div class="kelas-index">
    <h3><?= Html::encode($this->title) ?></h3>
    Forum akan ditampilkan pada kelas yang dipilih.
    <hr>
    <table class="table table-hover table-striped">
        <tr>
            <th>Kelas</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->nama_kelas ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'<?=$elm?>', url:'/kelas/gelombang/buat-forum-umum?id_kelas=<?= $v->id_kelas.$urlx ?>'})" class="btn btn-success" title="buat forum umum"><i class="fa fa-plus"></i> Forum Umum</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>