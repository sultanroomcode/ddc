<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Daftar Forum';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">
    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area-forum', url:'/kelas/kelas/pilih-gelombang-forum'})" class="btn btn-success"><i class="fa fa-plus"></i> Forum</a>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area-forum', url:'/kelas/kelas/pilih-kelas-forum'})" class="btn btn-success"><i class="fa fa-plus"></i> Forum Umum</a>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area-forum', url:'/kelas/kelas/list-forum'})" class="btn btn-success"><i class="fa fa-refresh fa-spin"></i></a>
    </p>
    
    <table class="table table-hover table-striped">
        <tr>
            <th>Nama Gelombang</th>
            <th>Kelas</th>
            <th>Judul Forum</th>
            <th>Jumlah Balasan</th>
            <th>Dibuat Tanggal</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= ((!isset($v->gelombang->nama_gelombang))?'-':$v->gelombang->nama_gelombang) ?></td>
            <td><?= $v->kelas->nama_kelas ?></td>
            <td><?= $v->message ?></td>
            <td><?= count($v->replies) ?></td>
            <td><?= $v->pembuat->nama_display ?><br><?= $v->dibuat_tanggal ?></td>
            <td>
                <?php if(!isset($v->gelombang->nama_gelombang)){ ?>
                <a href="javascript:void(0)" onclick="goPopup({welm:'700px', url:'/kelas/peserta/lihat-forum-umum?id_kelas=<?=$v->id_kelas?>&id_forum=<?=$v->id_thread?>'})" class="btn btn-success faa-parent animated-hover"><i class="fa fa-comments faa-bounce"></i></a>
                <?php } else { ?>
                <a href="javascript:void(0)" onclick="goPopup({welm:'700px', url:'/kelas/peserta/lihat-forum?id_gelombang=<?=$v->id_gelombang?>&id_forum=<?=$v->id_thread?>'})" class="btn btn-success faa-parent animated-hover"><i class="fa fa-comments faa-bounce"></i></a>
                <?php } ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>