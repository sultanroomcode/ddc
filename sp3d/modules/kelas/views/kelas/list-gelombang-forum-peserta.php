<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pilih Gelombang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">
    <h3><?= Html::encode($this->title) ?></h3>
    Forum akan ditampilkan pada gelombang kelas yang dipilih.
    <hr>
    <table class="table table-hover table-striped">
        <tr>
            <th>Nama Gelombang</th>
            <th>Kelas</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->gelombang->nama_gelombang ?></td>
            <td><?= $v->kelas->nama_kelas ?></td>
            <td><?= $v->status ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/gelombang/buat-forum?id_gelombang=<?= $v->id_gelombang ?>&id_kelas=<?=$v->id_kelas ?>&peserta=1'})" class="btn btn-success" title="buat forum"><i class="fa fa-plus"></i> Forum</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>