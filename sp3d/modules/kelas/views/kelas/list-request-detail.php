<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Permintaan Kelas : '.$model->nama_kelas;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area-permintaan', url:'/kelas/kelas/list-request'})" class="btn btn-success"><i class="fa fa-back"></i> Kembali</a>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area-permintaan', url:'/kelas/gelombang/create?id=<?= $model->id_kelas ?>&request=1'})" class="btn btn-success"><i class="fa fa-plus"></i> Buat Kelas/Gelombang</a>
    </p>
    
    <table class="table table-hover table-striped">
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Tanggal Permintaan</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->kabupaten->description ?></td>
            <td><?= $v->kecamatan->description ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= $v->dibuat_tanggal ?></td>
            <td>
                -
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
<?php
$scripts =<<<JS
    function deleteData(elm, url, urlback, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:elm, urlBack:urlback});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);