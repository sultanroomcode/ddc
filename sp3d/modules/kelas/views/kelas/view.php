<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\kelas\models\Kelas */

$this->title = $model->nama_kelas;
$this->params['breadcrumbs'][] = ['label' => 'Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas'})" class="btn btn-success"><i class="fa fa-backward"></i></a>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/view?id=<?= $model->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-refresh fa-spin"></i></a>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/update?id=<?= $model->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
    <a href="javascript:void(0)" onclick="deleteData('#kelas-area', '/kelas/kelas/delete?id=<?= $model->id_kelas ?>','/kelas/kelas','Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kelas',
            'nama_kelas',
            'deskripsi:ntext',
            'status',
            'dibuat_oleh',
            'diupdate_oleh',
            'dibuat_tanggal',
            'diupdate_tanggal',
        ],
    ]) ?>

    <div id="gelombang-kelas-area"></div>

    <div class="clearfix"></div>
    <hr>
    <div id="topik-kelas-area"></div>
</div>
<?php
$scripts =<<<JS
    goLoad({elm:'#gelombang-kelas-area', url:'/kelas/gelombang/index?id={$model->id_kelas}'});
    goLoad({elm:'#topik-kelas-area', url:'/kelas/kelas-topik/index?id={$model->id_kelas}'});
    function deleteData(elm, url, urlback, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:elm, urlBack:urlback});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);