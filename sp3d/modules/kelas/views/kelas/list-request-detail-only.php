<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Permintaan Kelas : '.$model->nama_kelas;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">
    <div>
        <input type="text" class="form-control" id="searchingdom2">
    </div>
    <table id="permintaan-table-dom" class="table table-hover table-striped">
        <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Operator</th>
            <th>Tanggal Permintaan</th>
            <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->kabupaten->description ?></td>
            <td><?= $v->kecamatan->description ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= (isset($v->operatordesa->detail->nama))?$v->operatordesa->detail->nama:$v->operatordesa->id ?></td>
            <td><?= $v->dibuat_tanggal ?></td>
            <td>
                <a href="javascript:void(0)" title="Tambahkan ke Peserta" onclick="goCall({elm:'#permintaan-peserta', url: '/kelas/ikut/create-from-popup?id_permintaan=<?=$v->id_permintaan?>&id_gelombang=<?=$model->getdata['id_gelombang']?>&id_kelas=<?=$v->id_kelas?>&id_user=<?= $v->user ?>', urlback:'/kelas/kelas/list-request-detail-only?id=<?=$v->id_kelas?>', msg:'Tambah Peserta', msgTitle:'Tambah Peserta?', textBtn:'Tambahkan', msgLong:'Tindakan ini akan menambahkan desa kedalam daftar peserta'})" class="btn btn-success"><i class="fa fa-plus"></i></a>
            </td>
        </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php 
$script = <<<JS
$('#searchingdom2').quicksearch('table#permintaan-table-dom tbody tr');
JS;

$this->registerJs($script);
?>