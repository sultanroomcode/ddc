<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Forum';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">    
    <table class="table table-hover table-striped">
        <tr>
            <th>Nama Gelombang</th>
            <th>Status</th>
            
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->nama_display ?></td>
            <td><?= $v->status ?></td>
            <td>
                
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>