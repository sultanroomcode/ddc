<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Permintaan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">
    <p>
        <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas'})" class="btn btn-success"><i class="fa fa-back"></i> Kembali</a> -->
    </p>
    
    <table class="table table-hover table-striped">
        <tr>
            <th>Nama Kurikulum</th>
            <th>Jumlah Permintaan</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->kelas->nama_kelas ?></td>
            <td><?= $v->c ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area-permintaan', url:'/kelas/kelas/list-request-detail?id=<?= $v->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-search-plus"></i> Lihat Detail</a>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area-permintaan', url:'/kelas/gelombang/create?id=<?= $v->id_kelas ?>&request=1'})" class="btn btn-success"><i class="fa fa-plus"></i> Buat Kelas</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
<?php
$scripts =<<<JS
    function deleteData(elm, url, urlback, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:elm, urlBack:urlback});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);