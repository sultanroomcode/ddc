<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kurikulum';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">
    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/create'})" class="btn btn-success"><i class="fa fa-plus"></i> Kurikulum</a>
        <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/list-request'})" class="btn btn-success"><i class="fa fa-search-plus"></i> Daftar Permintaan</a> -->
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/list-gelombang'})" class="btn btn-success"><i class="fa fa-search-plus"></i> Daftar Gelombang</a>
        <!-- <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/list-forum'})" class="btn btn-success"><i class="fa fa-search-plus"></i> Daftar Forum</a> -->
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/list-faq'})" class="btn btn-success"><i class="fa fa-search-plus"></i> Daftar F.A.Q</a>
       <!--  <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/list-user'})" class="btn btn-success"><i class="fa fa-search-plus"></i> Daftar Pengguna</a> -->
    </p>
    
    <table class="table table-hover table-striped">
        <tr>
            <th>Nama Kurikulum</th>
            <th>Deskripsi</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->nama_kelas ?></td>
            <td><?= $v->deskripsi ?></td>
            <td><?= $v->status ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/view?id=<?= $v->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-search-plus"></i></a>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/update?id=<?= $v->id_kelas ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0)" onclick="deleteData('#kelas-area', '/kelas/kelas/delete?id=<?= $v->id_kelas ?>','/kelas/kelas','Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
<?php
$scripts =<<<JS
    function deleteData(elm, url, urlback, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:elm, urlBack:urlback});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);