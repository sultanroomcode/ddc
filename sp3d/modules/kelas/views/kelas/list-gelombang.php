<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Gelombang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas'})" class="btn btn-success"><i class="fa fa-list"></i> Kembali Ke Kurikulum</a>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/kelas/list-gelombang'})" class="btn btn-success"><i class="fa fa-refresh fa-spin"></i></a>
    </p>
    
    <table class="table table-hover table-striped">
        <tr>
            <th>Nama Gelombang</th>
            <th>Kelas</th>
            <th>Maksimal Perserta</th>
            <th>Status</th>
            <th>Peserta</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->nama_gelombang ?></td>
            <td><?= $v->kelas->nama_kelas ?></td>
            <td><?= $v->max_ikut ?></td>
            <td><?= $v->status ?></td>
            <td><?= count($v->ikuts) ?> Desa</td>
            <td>
                <a href="javascript:void(0)" onclick="goPopup({welm:'700px', elm:'#kelas-area', url:'/kelas/gelombang/list-peserta?id=<?= $v->id_gelombang ?>&id_kelas=<?=$v->id_kelas?>&back-url=list-gelombang'})" class="btn btn-success"><i class="fa fa-users"></i></a>
                <a title="Edit Gelombang" href="javascript:void(0)" onclick="goLoad({elm:'#kelas-area', url:'/kelas/gelombang/update?id=<?= $v->id_gelombang ?>&id_kelas=<?=$v->id_kelas?>&back-url=list-gelombang'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                <a title="Hapus Gelombang" href="javascript:void(0)" onclick="goCall({elm:'#kelas-area', url:'/kelas/gelombang/delete?id=<?= $v->id_gelombang ?>',urlback:'/kelas/kelas/list-gelombang', msg:'Hapus Data'})" class="btn btn-success"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>