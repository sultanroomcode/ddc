<?php
namespace sp3d\modules\kelas\controllers;

use yii\web\Controller;
use sp3d\modules\kelas\models\KelasUser;
use Yii;
/**
 * Default controller for the `kelas` module
 */
class DefaultController extends Controller
{
	public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
            exit();
        }
        return parent::beforeAction($action);
    }
    
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionNewIndex()
    {
        if($this->findModel(Yii::$app->user->identity->id) == null){
            $model = new KelasUser();
            $model->linked_id = $model->kd_desa = Yii::$app->user->identity->id;
            $model->tipe_user = 'user';
            $model->status = 'ACTIVE';
            if ($model->load(Yii::$app->request->post())) {
                $data = $model->save();
                if($data){
                    $data = ['status' => 1, 'message' => 'Berhasil isi user forum'];
                } else 
                {
                    $data = ['status' => 0, 'message' => 'Gagal isi user forum'];
                }
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return json_encode($data);
            } else {
                return $this->renderAjax('form-isi-user-forum', [
                    'model' => $model,
                ]);
            }
        }

        return $this->renderAjax('new-index');
    }

    public function actionIndexPeserta()
    {
        if($this->findModel(Yii::$app->user->identity->id) == null){
            $model = new KelasUser();
            $model->linked_id = Yii::$app->user->identity->id;
            $model->kd_desa = Yii::$app->user->identity->desa->kd_desa;
            $model->tipe_user = 'operator';
            $model->status = 'ACTIVE';
            if ($model->load(Yii::$app->request->post())) {
                $data = $model->save();
                if($data){
                    $data = ['status' => 1, 'message' => 'Berhasil isi user forum'];
                } else 
                {
                    $data = ['status' => 0, 'message' => 'Gagal isi user forum'];
                }
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return json_encode($data);
            } else {
                return $this->renderAjax('form-isi-user-forum', [
                    'model' => $model,
                ]);
            }
        }

        return $this->renderAjax('index-peserta');
    }

    public function findModel($id)
    {
        if (($model = KelasUser::findOne(['linked_id' => $id])) !== null) {
            return $model;
        } else {
            return null;
        }

        // throw new NotFoundHttpException('The requested page does not exist.');
    }
}
