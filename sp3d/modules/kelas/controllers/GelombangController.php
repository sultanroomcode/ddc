<?php
namespace sp3d\modules\kelas\controllers;

use Yii;
use sp3d\modules\kelas\models\Kelas;
use sp3d\modules\kelas\models\KelasUser;
use sp3d\modules\kelas\models\KelasThread;
use sp3d\modules\kelas\models\Gelombang;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GelombangController implements the CRUD actions for Gelombang model.
 */
class GelombangController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
            exit();
        }
        return parent::beforeAction($action);
    }
    
    public function actionIndex($id)
    {
        $dataProvider = Gelombang::find()->where(['id_kelas' => $id]);
        $id = Kelas::findOne($id);

        return $this->renderAjax('index', [
            'model' => $dataProvider,
            'id' => $id,
        ]);
    }

    public function actionListPeserta($id)
    {
        /* load permintaan and ikut */
        $dataProvider = Gelombang::findOne($id);
        $dataProvider->getdata = Yii::$app->request->get(); 

        return $this->renderAjax('list-peserta', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gelombang model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $id_kelas)
    {
        return $this->renderAjax('view', [
            'model' => Gelombang::findOne(['id_gelombang' => $id, 'id_kelas' => $id_kelas])
        ]);
    }

    public function actionBuatForum()
    {
        $model = new KelasThread();
        $modeluser = KelasUser::findOne(['linked_id' => Yii::$app->user->identity->id]);
        $model->getdata = Yii::$app->request->get();
        $model->id_kelas = $model->getdata['id_kelas'];
        $model->dibuat_oleh = $modeluser->id_user_kelas;
        $model->id_gelombang = $model->getdata['id_gelombang'];
        $model->status = 'ACTIVE';
        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil menambah forum'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal menambah forum'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('create-forum', [
                'model' => $model,
            ]);
        }
    }

    public function actionBuatForumUmum()
    {
        $model = new KelasThread();       
        $modeluser = KelasUser::findOne(['linked_id' => Yii::$app->user->identity->id]);
        $model->getdata = Yii::$app->request->get();
        $model->id_gelombang = null;
        $model->status = 'ACTIVE';
        $model->dibuat_oleh = $modeluser->id_user_kelas;
        $model->id_kelas = $model->getdata['id_kelas'];
        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil menambah forum'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal menambah forum'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('create-forum', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate($id)
    {
        $model = new Gelombang();
        $model->id_kelas = $id;

        $model->getdata = Yii::$app->request->get();
        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil menambah gelombang'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal menambah gelombang'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Gelombang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $id_kelas)
    {
        $model = Gelombang::findOne(['id_gelombang' => $id, 'id_kelas' => $id_kelas]);
        $model->getdata = Yii::$app->request->get();
        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil mengubah gelombang'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal mengubah gelombang'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gelombang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = $this->findModel($id)->delete();

        if($data){
            $data = ['status' => 1, 'message' => 'Berhasil menghapus gelombang'];
        } else 
        {
            $data = ['status' => 0, 'message' => 'Gagal menghapus gelombang'];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return json_encode($data);
    }

    /**
     * Finds the Gelombang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gelombang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gelombang::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
