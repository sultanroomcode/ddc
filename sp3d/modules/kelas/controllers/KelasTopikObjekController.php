<?php

namespace sp3d\modules\kelas\controllers;

use Yii;
use sp3d\modules\kelas\models\KelasTopik;
use sp3d\modules\kelas\models\KelasTopikObjek;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KelasTopikObjekController implements the CRUD actions for KelasTopikObjek model.
 */
class KelasTopikObjekController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
            exit();
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all KelasTopikObjek models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $dataProvider = KelasTopikObjek::find()->where(['id_topik' => $id]);
        $id = KelasTopik::findOne($id);

        return $this->renderAjax('index', [
            'model' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Displays a single KelasTopikObjek model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KelasTopikObjek model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new KelasTopikObjek();
        $model->id_topik = $id;
        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil menambah objek topik kelas'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal menambah objek topik kelas'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KelasTopikObjek model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $id_topik)
    {
        $model = KelasTopikObjek::findOne(['id_objek' => $id, 'id_topik' => $id_topik]);

        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil mengubah objek topik kelas'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal mengubah objek topik kelas'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KelasTopikObjek model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = $this->findModel($id)->delete();

        if($data){
            $data = ['status' => 1, 'message' => 'Berhasil menghapus objek topik kelas'];
        } else 
        {
            $data = ['status' => 0, 'message' => 'Gagal menghapus objek topik kelas'];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return json_encode($data);
    }

    /**
     * Finds the KelasTopikObjek model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KelasTopikObjek the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KelasTopikObjek::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
