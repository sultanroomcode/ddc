<?php

namespace sp3d\modules\kelas\controllers;

use Yii;
use sp3d\modules\kelas\models\Ikut;
use sp3d\modules\kelas\models\Gelombang;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IkutController implements the CRUD actions for Ikut model.
 */
class IkutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
            exit();
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Ikut models.
     * @return mixed
     */
    public function actionIndex($id_gelombang, $id_kelas)
    {
        $dataProvider = Ikut::find()->where(['id_gelombang' => $id_gelombang, 'id_kelas' => $id_kelas]);
        $id = Gelombang::findOne(['id_gelombang' => $id_gelombang, 'id_kelas' => $id_kelas]);
        return $this->renderAjax('index', [
            'model' => $dataProvider,
            'id' => $id,
        ]);
    }

    public function actionIndexOnly($id_gelombang, $id_kelas)
    {
        $dataProvider = Ikut::find()->where(['id_gelombang' => $id_gelombang, 'id_kelas' => $id_kelas]);
        $id = Gelombang::findOne(['id_gelombang' => $id_gelombang, 'id_kelas' => $id_kelas]);
        return $this->renderAjax('index-only', [
            'model' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Displays a single Ikut model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ikut model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateFromPopup($id_permintaan, $id_gelombang, $id_kelas, $id_user)
    {
        $model = new Ikut();
        $model->id_gelombang = $id_gelombang;
        $model->id_kelas = $id_kelas;
        $model->id_permintaan = $id_permintaan;
        $model->status = 'ACTIVE';
        $model->user = $id_user;
        if($model->save()){
            $data = ['status' => 1, 'message' => 'Berhasil menambah peserta'];
        } else 
        {
            $data = ['status' => 0, 'message' => 'Gagal menambah peserta'];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return json_encode($data);
    }

    public function actionCreate($id_gelombang, $id_kelas)
    {
        $check = Gelombang::findOne(['id_gelombang' => $id_gelombang, 'id_kelas' => $id_kelas]);
        if(count($check->ikuts) < $check->max_ikut){
            $model = new Ikut();
            $model->id_gelombang = $id_gelombang;
            $model->id_kelas = $id_kelas;

            if ($model->load(Yii::$app->request->post())) {
                $data = $model->save();
                if($data){
                    $data = ['status' => 1, 'message' => 'Berhasil menambah peserta'];
                } else 
                {
                    $data = ['status' => 0, 'message' => 'Gagal menambah peserta'];
                }
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return json_encode($data);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->renderAjax('max', [
                'model' => $check,
            ]);
        }
    }

    /**
     * Updates an existing Ikut model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->getdata = Yii::$app->request->get();

        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil mengubah peserta'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal mengubah peserta'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ikut model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = $this->findModel($id)->delete();

        if($data){
            $data = ['status' => 1, 'message' => 'Berhasil menghapus peserta'];
        } else 
        {
            $data = ['status' => 0, 'message' => 'Gagal menghapus peserta'];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return json_encode($data);
    }

    /**
     * Finds the Ikut model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ikut the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ikut::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
