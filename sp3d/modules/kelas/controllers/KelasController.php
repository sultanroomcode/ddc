<?php

namespace sp3d\modules\kelas\controllers;

use Yii;
use sp3d\modules\kelas\models\Kelas;
use sp3d\modules\kelas\models\Gelombang;
use sp3d\modules\kelas\models\Ikut;
use sp3d\modules\kelas\models\KelasPermintaan;
use sp3d\modules\kelas\models\KelasThread;
use sp3d\modules\kelas\models\KelasUser;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KelasController implements the CRUD actions for Kelas model.
 */
class KelasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
            exit();
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Kelas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = Kelas::find();

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListUser()
    {
        $dataProvider = KelasUser::find();

        return $this->renderAjax('list-user', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListRequest()
    {
        $dataProvider = KelasPermintaan::find()->select(['COUNT(*) AS c', 'kelas_permintaan.*'])->where(['status' => 'REQUEST'])->groupBy(['id_kelas'])->orderBy('c');

        return $this->renderAjax('list-request', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListGelombang()
    {
        $dataProvider = Gelombang::find()->orderBy('dibuat_tanggal DESC');

        return $this->renderAjax('list-gelombang', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListForum()
    {
        $dataProvider = KelasThread::find()->orderBy('dibuat_tanggal DESC');

        return $this->renderAjax('list-forum', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPilihGelombangForum()
    {
        $getdata = Yii::$app->request->get();

        if(isset($getdata['peserta']) && $getdata['peserta'] == 1){
            $dataProvider = Ikut::find()->where(['user' => Yii::$app->user->identity->desa->kd_desa, 'status' => 'ACTIVE']);

            return $this->renderAjax('list-gelombang-forum-peserta', [
                'dataProvider' => $dataProvider,
                'getdata' => $getdata
            ]);
        } else {
            $dataProvider = Gelombang::find()->orderBy('dibuat_tanggal DESC');

            return $this->renderAjax('list-gelombang-forum', [
                'dataProvider' => $dataProvider,
                'getdata' => $getdata
            ]);
        }
    }

    public function actionPilihKelasForum()
    {
        $getdata = Yii::$app->request->get();
        $dataProvider = Kelas::find()->where(['status' => 'ACTIVE']);
        
        return $this->renderAjax('list-kelas-forum', [
            'dataProvider' => $dataProvider,
            'getdata' => $getdata
        ]);
    }

    public function actionListFaq()
    {
        $dataProvider = Gelombang::find()->orderBy('dibuat_tanggal DESC');

        return $this->renderAjax('list-gelombang', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListRequestDetail($id)
    {
        $dataProvider = KelasPermintaan::find()->where(['status' => 'REQUEST', 'id_kelas' => $id])->orderBy('dibuat_tanggal DESC');
        $model = Kelas::findOne($id);
        $model->getdata = Yii::$app->request->get();

        return $this->renderAjax('list-request-detail', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    public function actionListRequestDetailOnly($id)
    {
        $dataProvider = KelasPermintaan::find()->where(['status' => 'REQUEST', 'id_kelas' => $id])->orderBy('dibuat_tanggal DESC');
        $model = Kelas::findOne($id);
        $model->getdata = Yii::$app->request->get();

        return $this->renderAjax('list-request-detail-only', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single Kelas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kelas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kelas();

        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil menambah kelas'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal menambah kelas'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kelas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil mengubah kelas'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal mengubah kelas'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kelas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = $this->findModel($id)->delete();

        if($data){
            $data = ['status' => 1, 'message' => 'Berhasil menghapus kelas'];
        } else 
        {
            $data = ['status' => 0, 'message' => 'Gagal menghapus kelas'];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return json_encode($data);
    }

    /**
     * Finds the Kelas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kelas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kelas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
