<?php
namespace sp3d\modules\kelas\controllers;

use Yii;
use sp3d\modules\kelas\models\Kelas;
use sp3d\modules\kelas\models\KelasUser;
use sp3d\modules\kelas\models\KelasTopik;
use sp3d\modules\kelas\models\KelasPermintaan;
use sp3d\modules\kelas\models\KelasThread;
use sp3d\modules\kelas\models\KelasThreadReply;
use sp3d\modules\kelas\models\KelasTopikObjek;
use sp3d\modules\kelas\models\Ikut;
use sp3d\modules\kelas\models\Gelombang;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IkutController implements the CRUD actions for Ikut model.
 */
class PesertaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    private $max_select = 3;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login?p=bumdes']);
            exit();
        }
        return parent::beforeAction($action);
    }

    public function actionDashboard()//accessed
    {
        $id = Yii::$app->user->identity->id;
        $data = Yii::$app->request->get();

        if($this->findModel(Yii::$app->user->identity->id) == null){
            $model = new KelasUser();
            $model->linked_id = Yii::$app->user->identity->id;
            $model->kd_desa = Yii::$app->user->identity->desa->kd_desa;
            $model->tipe_user = 'operator';
            $model->status = 'ACTIVE';
            if ($model->load(Yii::$app->request->post())) {
                $data = $model->save();
                if($data){
                    $data = ['status' => 1, 'message' => 'Berhasil isi user forum'];
                } else 
                {
                    $data = ['status' => 0, 'message' => 'Gagal isi user forum'];
                }
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return json_encode($data);
            } else {
                return $this->renderAjax('form-isi-user-forum', [
                    'model' => $model,
                ]);
            }
        }

        return $this->renderAjax('dashboard', [
            'data' => [
                'id' => $id,
                'getdata' => $data
            ]
        ]);
    }
    //PERMINTAAN KELAS
    public function actionHapusPermintaanKelas($id)//accessed
    {
        $kd_desa = Yii::$app->user->identity->desa->kd_desa;
        $model = KelasPermintaan::findOne(['user' => $kd_desa, 'id_permintaan' => $id, 'status' => 'REQUEST']);

        return $this->renderAjax('hapus-permintaan-kelas', [
            'model' => $model
        ]);
    }

    public function actionLihatPermintaanKelas()//accessed
    {
        $id = Yii::$app->user->identity->desa->kd_desa;
        $model = KelasPermintaan::find()->where(['user' => $id]);

        return $this->renderAjax('lihat-permintaan-kelas', [
            'model' => $model
        ]);
    }

    public function actionAmbilKelas()//accessed
    {
        $id = Yii::$app->user->identity->desa->kd_desa;
        $model = KelasPermintaan::find()->where(['user' => $id, 'status' => ['REQUEST', 'ACTIVE']]);
        if($model->count() < $this->max_select){
            $model = Kelas::find()->where(['status' => 'ACTIVE'])->orderBy('id_kelas ASC');

            return $this->renderAjax('ambil-kelas', [
                'model' => $model
            ]);
        } else{
            return $this->renderAjax('ambil-kelas-full', [
                'model' => $model
            ]);
        }
    }

    public function actionAmbilKelasNo($id)//accessed
    {
        $userid = Yii::$app->user->identity->desa->kd_desa;
        $operatorid = Yii::$app->user->identity->id;
        $model = KelasPermintaan::find()->where(['user' => $userid, 'status' => ['REQUEST', 'ACTIVE']]);
        $insert = $max = $exist = false;
        if($model->count() < $this->max_select){

            $fmodel = KelasPermintaan::findOne(['user' => $userid, 'id_kelas' => $id]);
            if($fmodel == null){
                $nmodel = new KelasPermintaan();
                $nmodel->user = $userid;
                $nmodel->operator = $operatorid;
                $nmodel->id_kelas = $id;
                $nmodel->status = 'REQUEST';//REQUEST|ACTIVE|FINISH
                $nmodel->save();
                $insert = true;
            } else {
                $exist = true;
            }
        } else {
            $max = true;
        }
        
        return $this->renderAjax('ambil-kelas-no', [
            'model' => $model,
            'data' => ['insert' => $insert, 'max' => $max, 'exist' => $exist]
        ]);
    }
    //KELAS
    public function actionPesertaViewbox()
    {
        $id = Yii::$app->user->identity->desa->kd_desa;
        $model = Ikut::find()->where(['user' => $id, 'status' => 'ACTIVE']);

        return $this->renderAjax('peserta-viewbox', [
            'model' => $model
        ]);
    }

    public function actionLihatKelas($id)
    {
        //butuh shield untuk melindungi kelas
        $iduser = Yii::$app->user->identity->desa->kd_desa;
        $model = Ikut::findOne(['id_ikut' => $id, 'user' => $iduser, 'status' => 'ACTIVE']);

        if($model != null)
        {
            return $this->renderAjax('lihat-kelas', [
                'model' => $model
            ]);
        } else {
            return $this->renderAjax('kosong');
        }
    }

    public function actionLihatTopik($id)
    {
        //butuh shield untuk melindungi kelas
        $iduser = Yii::$app->user->identity->desa->kd_desa;
        $model = Ikut::findOne(['id_ikut' => $id, 'user' => $iduser, 'status' => 'ACTIVE']);

        if($model != null)
        {
            $modelkelas = Kelas::findOne($model->id_kelas);
            return $this->renderAjax('lihat-topik', [
                'modelkelas' => $modelkelas,
                'model' => $model
            ]);
        } else {
            return $this->renderAjax('kosong');
        }
    }

    public function actionLihatMateri($id_topik, $id_ikut)
    {
        //butuh shield untuk melindungi kelas
        $iduser = Yii::$app->user->identity->desa->kd_desa;
        $modelikut = Ikut::findOne(['id_ikut' => $id_ikut, 'user' => $iduser, 'status' => 'ACTIVE']);

        if($modelikut != null)
        {
            $model = KelasTopikObjek::find()->where(['id_topik' => $id_topik, 'status' => 'ACTIVE'])->orderBy('urutan ASC');
            $modeltopik = KelasTopik::findOne($id_topik);
            return $this->renderAjax('lihat-materi', [
                'model' => $model,
                'modelikut' => $modelikut,
                'modeltopik' => $modeltopik
            ]);
        } else {
            return $this->renderAjax('kosong');
        }        
    }
    //FORUM
    public function actionForumUmum()
    {
        //butuh shield untuk melindungi kelas
        $iduser = Yii::$app->user->identity->desa->kd_desa;
        $model = Kelas::find();
        return $this->renderAjax('forum-umum', [
            'model' => $model
        ]);
    }

    public function actionLihatDaftarForumUmum($id)
    {
        //butuh shield untuk melindungi kelas
        $model = KelasThread::find()->where(['id_kelas' => $id, 'id_gelombang' => null]);

        if($model->count() > 0)
        {
            return $this->renderAjax('daftar-forum-umum', [
                'model' => $model
            ]);
        } else {
            $urlback = '/kelas/peserta/forum-umum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback]
            ]);
        }
    }

    public function actionLihatForumUmum($id_kelas, $id_forum)
    {
        //butuh shield untuk melindungi kelas
        $model = KelasThread::findOne(['id_kelas' => $id_kelas, 'id_thread' => $id_forum, 'status' => 'ACTIVE']);

        if($model != null)
        {
            return $this->renderAjax('lihat-forum-umum', [
                'model' => $model,
            ]);
        } else {
            $urlback = '/kelas/peserta/forum-umum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback, 'popup' => 1]
            ]);
        }
    }

    public function actionLihatBalasanForumUmum($id_forum)
    {
        //butuh shield untuk melindungi kelas
        $model = KelasThread::findOne(['id_thread' => $id_forum, 'status' => 'ACTIVE']);

        if($model != null)
        {
            $modelf = KelasThreadReply::find()->where(['id_thread' => $id_forum])->orderBy('dibuat_tanggal DESC');

            return $this->renderAjax('lihat-balasan-forum', [
                'modelf' => $modelf,
                'model' => $model,
            ]);
        } else {
            $urlback = '/kelas/peserta/forum-umum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback, 'popup' => 1]
            ]);
        }
    }

    public function actionBalasForumUmum($id_kelas, $id_forum)
    {
        //butuh shield untuk melindungi kelas
        $model = KelasThread::findOne(['id_thread' => $id_forum, 'id_kelas' => $id_kelas, 'status' => 'ACTIVE']);
        $modeluser = KelasUser::findOne(['linked_id' => Yii::$app->user->identity->id]);
        if($model != null && $modeluser != null)
        {
            $modelf = new KelasThreadReply;
            $modelf->id_thread = $id_forum;
            $modelf->dibuat_oleh = $modeluser->id_user_kelas;

            if ($modelf->load(Yii::$app->request->post())) {
                if($data = $modelf->save()){

                    $data = ['status' => 1, 'message' => 'Berhasil menambah forum'];
                } else 
                {
                    $data = ['status' => 0, 'message' => 'Gagal menambah forum'];
                }
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return json_encode($data);
            } else {
                return $this->renderAjax('balas-forum', [
                    'modelf' => $modelf,
                    'model' => $model,
                    'umum' => 1
                ]);
            }
        } else {
            $urlback = '/kelas/peserta/pilih-gelombang-forum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback, 'popup' => 1]
            ]);
        }
    }

    //forum kelas

    public function actionPilihGelombangForum()
    {
        //butuh shield untuk melindungi kelas
        $iduser = Yii::$app->user->identity->desa->kd_desa;
        $model = Ikut::find()->where(['user' => $iduser, 'status' => 'ACTIVE']);

        if($model->count() > 0)
        {
            return $this->renderAjax('pilih-gelombang-forum', [
                'model' => $model
            ]);
        } else {
            $urlback = '/kelas/peserta/dashboard?page=kelas-ikuti';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback]
            ]);
        }
    }

    public function actionLihatDaftarForum($id)
    {
        //butuh shield untuk melindungi kelas
        $model = Gelombang::findOne(['id_gelombang' => $id, 'status' => 'ACTIVE']);

        if($model != null && count($model->threads) > 0)
        {
            return $this->renderAjax('daftar-forum', [
                'model' => $model
            ]);
        } else {
            $urlback = '/kelas/peserta/pilih-gelombang-forum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback]
            ]);
        }
    }

    public function actionLihatForum($id_gelombang, $id_forum)
    {
        //butuh shield untuk melindungi kelas
        $model = Gelombang::findOne(['id_gelombang' => $id_gelombang, 'status' => 'ACTIVE']);

        if($model != null)
        {
            $modelf = KelasThread::findOne($id_forum);
            return $this->renderAjax('lihat-forum', [
                'model' => $model,
                'modelf' => $modelf,
            ]);
        } else {
            $urlback = '/kelas/peserta/pilih-gelombang-forum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback, 'popup' => 1]
            ]);
        }
    }

    public function actionBalasForum($id_gelombang, $id_forum)
    {
        //butuh shield untuk melindungi kelas
        $model = Gelombang::findOne(['id_gelombang' => $id_gelombang, 'status' => 'ACTIVE']);
        $modeluser = KelasUser::findOne(['linked_id' => Yii::$app->user->identity->id]);
        if($model != null)
        {
            $modelf = new KelasThreadReply;
            $modelf->id_thread = $id_forum;
            $modelf->dibuat_oleh = $modeluser->id_user_kelas;

            if ($modelf->load(Yii::$app->request->post())) {
                /*$data = $modelf->save();
                if($data){*/
                    //skrip diatas akan menyebabkan double data saving ketika online
                if($data = $modelf->save()){
                    $data = ['status' => 1, 'message' => 'Berhasil menambah forum'];
                } else 
                {
                    $data = ['status' => 0, 'message' => 'Gagal menambah forum'];
                }
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return json_encode($data);
            } else {
                return $this->renderAjax('balas-forum', [
                    'modelf' => $modelf,
                    'model' => $model,
                ]);
            }
        } else {
            $urlback = '/kelas/peserta/pilih-gelombang-forum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback, 'popup' => 1]
            ]);
        }
    }

    public function actionLihatBalasanForum($id_gelombang, $id_forum)
    {
        //butuh shield untuk melindungi kelas
        $model = Gelombang::findOne(['id_gelombang' => $id_gelombang, 'status' => 'ACTIVE']);

        if($model != null)
        {
            $modelf = KelasThreadReply::find()->where(['id_thread' => $id_forum])->orderBy('dibuat_tanggal DESC');

            return $this->renderAjax('lihat-balasan-forum', [
                'modelf' => $modelf,
                'model' => $model,
            ]);
        } else {
            $urlback = '/kelas/peserta/pilih-gelombang-forum';
            return $this->renderAjax('kosong', [
                'data' => ['urlback' => $urlback, 'popup' => 1]
            ]);
        }
    }

    //tools

    public function findModel($id)
    {
        if (($model = KelasUser::findOne(['linked_id' => $id])) !== null) {
            return $model;
        } else {
            return null;
        }

        // throw new NotFoundHttpException('The requested page does not exist.');
    }
}
