<?php

namespace sp3d\modules\kelas\controllers;

use Yii;
use sp3d\modules\kelas\models\Kelas;
use sp3d\modules\kelas\models\KelasTopik;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KelasTopikController implements the CRUD actions for KelasTopik model.
 */
class KelasTopikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
            exit();
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all KelasTopik models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $dataProvider = KelasTopik::find()->where(['id_kelas' => $id]);
        $id = Kelas::findOne($id);

        return $this->renderAjax('index', [
            'model' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Displays a single KelasTopik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KelasTopik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new KelasTopik();
        $model->id_kelas = $id;
        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil menambah topik kelas'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal menambah topik kelas'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KelasTopik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $id_kelas)
    {
        $model = KelasTopik::findOne(['id_topik' => $id, 'id_kelas' => $id_kelas]);

        if ($model->load(Yii::$app->request->post())) {
            $data = $model->save();
            if($data){
                $data = ['status' => 1, 'message' => 'Berhasil mengubah topik kelas'];
            } else 
            {
                $data = ['status' => 0, 'message' => 'Gagal mengubah topik kelas'];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KelasTopik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = $this->findModel($id)->delete();

        if($data){
            $data = ['status' => 1, 'message' => 'Berhasil menghapus topik kelas'];
        } else 
        {
            $data = ['status' => 0, 'message' => 'Gagal menghapus topik kelas'];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return json_encode($data);
    }

    /**
     * Finds the KelasTopik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KelasTopik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KelasTopik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
