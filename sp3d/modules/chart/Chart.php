<?php

namespace sp3d\modules\chart;

/**
 * chart module definition class
 */
class Chart extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\chart\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
