<div class="row">
    <div class="col-md-12">
        <div id="in-statistik"></div>  
    </div>
</div>
<?php
$user = ($data['kode'] == null)?Yii::$app->user->identity->id:$data['kode'];
$tingkat = $data['tingkat'];
switch($data['objek']){
    case 'kepala-desa':
        $objek = 'kades';
    break;
    case 'perangkat-desa':
        $objek = 'perangkat';
    break;
    case 'bpd':
        $objek = 'bpd';
    break;
    case 'bumdes':
        $objek = 'bumdes';
    break;
    case 'pasar-desa':
        $objek = 'pasar-desa';
    break;
    case 'tkd':
        $objek = 'tkd';
    break;
    case 'pkk':
        $objek = 'pkk';
    break;
    case 'kartar':
        $objek = 'kartar';
    break;
    case 'lpmd':
        $objek = 'lpmd';
    break;
    case 'lembagaadat':
        $objek = 'lembagaadat';
    break;
    case 'data-tambahan':
        $objek = 'data-tambahan';
    break;
    case 'kpm':
        $objek = 'kpm';
    break;
    case 'podes-pertanian-pangan':
    case 'podes-pertanian-buah':
    case 'podes-pertanian-apotik':
    case 'podes-perkebunan':
    case 'podes-kehutanan':
    case 'podes-peternakan':
    case 'podes-perikanan':
        $objek = $data['objek'];
    break;
}      

switch ($data['subjek']) {
    case 'pelatihan':
        $type = 'pelatihan-'.$objek;
    break;
    case 'lama-jabatan':
        $type = 'long-order-'.$objek;
    break;
    case 'pendidikan':
        $type = 'pendidikan-'.$objek;
    break;
    case 'umur':
        $type = 'age-'.$objek;
    break;
    case 'jenis-kelamin':    
        $type = 'gender-'.$objek;
    break;
    //BUMDES
    case 'keuntungan':    
        $type = $objek.'-modal-keuntungan';
    break;
    case 'modal':
        $type = $objek.'-modal';
    break;
    case 'omset':
        $type = $objek.'-modal-omset';
    break;
    case 'unit-usaha':    
        $type = $objek.'-unit-usaha';
    break;
    case 'tahun'://termasuk objek pasar-desa
        $type = $objek.'-tahun';
    break;
    case 'region'://termasuk objek pasar-desa
        $type = $objek.'-region';
    break;
    //PASAR-DESA
    case 'jml-pedagang':
    case 'jml-los':
    case 'jml-lapak':
    case 'jml-kios':
    case 'jml-ruko':
    case 'jml-lesehan':
        $type = $objek.'-'.$data['subjek'];
    break;
    //tkd
    case 'lokasi':
    case 'luas-lahan':
    case 'jenis-lahan':
    case 'jenis-sertifikat':
        $type = $objek.'-'.$data['subjek'];
    break;
    //kelembagaan
    case 'pembina':    
        $type = $objek.'-pembina';
    break;
    case 'status':    
        $type = $objek.'-status';
    break;
    //podes
    case 'luas-lahan-produksi':
    case 'hasil-produksi':
    case 'lahan-milik':
    case 'satuan':
    case 'komoditas':
        $type = $objek.'-'.$data['subjek'];
    break;
    //data-tambahan
    case 'umum':
        $type = $objek.'-'.$data['subjek'];
    break;
    default:
    break;
}
$jabatan = $data['jabatan'];
$kabupaten = $data['kabupaten-input'];
$kecamatan = $data['kecamatan-input'];
$scriptJs = <<<JS
    goLoad({elm: '#in-statistik', url: '/umum/statistik/chart?tingkat={$tingkat}&tahun=&jabatan={$jabatan}&type={$type}&kd={$user}&kabupaten-input={$kabupaten}&kecamatan-input={$kecamatan}'});
JS;

$this->registerJs($scriptJs);