<?php
namespace sp3d\modules\chart\controllers;

use yii\web\Controller;
use Yii;
/**
 * Default controller for the `chart` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRequestChart()
    {
        $data = Yii::$app->request->get();
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $tingkat = 'kecamatan';
            $kode = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $tingkat = 'kabupaten';
            $kode = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $kode = null;
            $tingkat = 'provinsi';
        }
        $data['tingkat'] = $tingkat;
        $data['kode'] = $kode;

        return $this->renderAjax('request-chart', [
            'data' => $data
        ]);
    }
}
