<?php
namespace sp3d\modules\chart\controllers;

use yii\web\Controller;

/**
 * Default controller for the `chart` module
 */
class PasarDesaChartController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
