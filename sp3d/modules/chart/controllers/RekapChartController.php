<?php
namespace sp3d\modules\chart\controllers;

use yii\web\Controller;

/**
 * Default controller for the `chart` module
 */
class RekapChartController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionEntrian()
    {
        return $this->renderAjax('entrian');
    }
}
