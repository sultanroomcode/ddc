<?php

namespace sp3d\modules\posyandu\controllers;

use Yii;
use sp3d\modules\posyandu\models\Posyandu;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PosyanduController implements the CRUD actions for Posyandu model.
 */
class NonOperatorPosyanduController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Posyandu models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Posyandu::find(),
        // ]);
        $dataProvider = Posyandu::find();
        $dataProvider->where(['kd_desa' => Yii::$app->user->identity->id]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Posyandu model.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @return mixed
     */
    public function actionView($kd_desa, $no)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $no),
        ]);
    }

    /**
     * Creates a new Posyandu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Posyandu();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Posyandu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @return mixed
     */
    public function actionUpdate($kd_desa, $no)
    {
        $model = $this->findModel($kd_desa, $no);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Posyandu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @return mixed
     */
    public function actionDelete($kd_desa, $no)
    {
        $model = $this->findModel($kd_desa, $no);

        if($model->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the Posyandu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @return Posyandu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $no)
    {
        if (($model = Posyandu::findOne(['kd_desa' => $kd_desa, 'no' => $no])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
