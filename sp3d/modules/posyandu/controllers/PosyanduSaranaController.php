<?php
namespace sp3d\modules\posyandu\controllers;

use Yii;
use sp3d\modules\posyandu\models\PosyanduSarana;
use sp3d\modules\posyandu\models\PosyanduIsian;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PosyanduSaranaController implements the CRUD actions for PosyanduSarana model.
 */
class PosyanduSaranaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PosyanduSarana models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = PosyanduSarana::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa]);
        $dataPosyandu = PosyanduIsian::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
            'dataPosyandu' => $dataPosyandu,
        ]);
    }

    public function actionCreateBySelect()
    {
        $model = new PosyanduSarana();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('_form-create-by-select', [
                'model' => $model,                
            ]);
        }
    }

    /**
     * Displays a single PosyanduSarana model.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @param string $tahun
     * @return mixed
     */
    public function actionView($kd_desa, $no, $tahun)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $no, $tahun),
        ]);
    }

    /**
     * Creates a new PosyanduSarana model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $no, $tahun)
    {
        $model = new PosyanduSarana();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'tahun' => $tahun, 'no' => $no];

            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing PosyanduSarana model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @param string $tahun
     * @return mixed
     */
    public function actionUpdate($kd_desa, $no, $tahun)
    {
        $model = $this->findModel($kd_desa, $no, $tahun);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'tahun' => $tahun, 'no' => $no];
            
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing PosyanduSarana model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @param string $tahun
     * @return mixed
     */
    public function actionDelete($kd_desa, $no, $tahun)
    {
        $model = $this->findModel($kd_desa, $no, $tahun)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the PosyanduSarana model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $no_rw
     * @param integer $no
     * @param string $tahun
     * @return PosyanduSarana the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $no, $tahun)
    {
        if (($model = PosyanduSarana::findOne(['kd_desa' => $kd_desa, 'no' => $no, 'tahun' => $tahun])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
