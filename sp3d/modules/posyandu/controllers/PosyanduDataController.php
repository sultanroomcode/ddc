<?php

namespace sp3d\modules\posyandu\controllers;

use Yii;
use sp3d\modules\posyandu\models\PosyanduData;
use sp3d\modules\posyandu\models\PosyanduIsian;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PosyanduDataController implements the CRUD actions for PosyanduData model.
 */
class PosyanduDataController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PosyanduData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = PosyanduData::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa]);
        $dataPosyandu = PosyanduIsian::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
            'dataPosyandu' => $dataPosyandu,
        ]);
    }

    /**
     * Displays a single PosyanduData model.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return mixed
     */
    public function actionView($kd_desa, $tahun, $no, $bulan)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $tahun, $no, $bulan),
        ]);
    }

    /**
     * Creates a new PosyanduData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateBySelect()
    {
        $model = new PosyanduData();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('_form-create-by-select', [
                'model' => $model,                
            ]);
        }
    }

    public function actionCreate($kd_desa, $no, $tahun)//from posyandu section
    {
        $model = new PosyanduData();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'tahun' => $tahun , 'no' => $no];

            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing PosyanduData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return mixed
     */
    public function actionUpdate($kd_desa, $tahun, $no, $bulan)
    {
        $model = $this->findModel($kd_desa, $tahun, $no, $bulan);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
            // var_dump($model->errors);
        } else {
            $data = ['kd_desa' => $kd_desa, 'tahun' => $tahun, 'bulan' => $bulan, 'no' => $no];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing PosyanduData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return mixed
     */
    public function actionDelete($kd_desa, $tahun, $no, $bulan)
    {
        $model = $this->findModel($kd_desa, $tahun, $no, $bulan)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the PosyanduData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return PosyanduData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $tahun, $no, $bulan)
    {
        if (($model = PosyanduData::findOne(['kd_desa' => $kd_desa, 'tahun' => $tahun, 'no' => $no, 'bulan' => $bulan])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
