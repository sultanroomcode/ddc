<?php

namespace sp3d\modules\posyandu\controllers;

use Yii;
use sp3d\modules\posyandu\models\PosyanduKegiatan;
use sp3d\modules\posyandu\models\PosyanduIsian;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PosyanduKegiatanController implements the CRUD actions for PosyanduKegiatan model.
 */
class PosyanduKegiatanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PosyanduKegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = PosyanduKegiatan::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa]);
        $dataPosyandu = PosyanduIsian::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
            'dataPosyandu' => $dataPosyandu,
        ]);
    }

    public function actionCreateBySelect()
    {
        $model = new PosyanduKegiatan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('_form-create-by-select', [
                'model' => $model,                
            ]);
        }
    }

    /**
     * Displays a single PosyanduKegiatan model.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return mixed
     */
    public function actionView($kd_desa, $tahun, $no_rw, $no, $bulan)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $tahun, $no_rw, $no, $bulan),
        ]);
    }

    /**
     * Creates a new PosyanduKegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $tahun, $no)
    {
        $model = new PosyanduKegiatan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'tahun' => $tahun, 'no' => $no];

            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing PosyanduKegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return mixed
     */
    public function actionUpdate($kd_desa, $tahun, $no, $bulan)
    {
        $model = $this->findModel($kd_desa, $tahun, $no, $bulan);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'tahun' => $tahun, 'bulan' => $bulan, 'no' => $no];

            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing PosyanduKegiatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return mixed
     */
    public function actionDelete($kd_desa, $tahun, $no, $bulan)
    {
        $model = $this->findModel($kd_desa, $tahun, $no, $bulan)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the PosyanduKegiatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param string $tahun
     * @param integer $no_rw
     * @param integer $no
     * @param string $bulan
     * @return PosyanduKegiatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $tahun, $no, $bulan)
    {
        if (($model = PosyanduKegiatan::findOne(['kd_desa' => $kd_desa, 'tahun' => $tahun, 'no' => $no, 'bulan' => $bulan])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
