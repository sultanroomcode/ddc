<?php
namespace sp3d\modules\posyandu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "posyandu_kegiatan".
 *
 * @property string $kd_desa
 * @property string $tahun
 * @property integer $no
 * @property string $bulan
 * @property integer $ih_periksa
 * @property integer $ih_fe
 * @property integer $i_asi
 * @property integer $kd_kondom
 * @property integer $kd_pil
 * @property integer $kd_suntik
 * @property integer $pb_jbsp
 * @property integer $pb_kms
 * @property integer $pb_timbang
 * @property integer $pb_naik
 * @property integer $pb_bgm_l
 * @property integer $pb_bgm_p
 * @property integer $blt_vit_a
 * @property integer $blt_kms_out
 * @property integer $blt_fe_1
 * @property integer $blt_fe_2
 * @property integer $blt_pmt
 * @property integer $blt_imns_hepatitis0
 * @property integer $blt_imns_hepatitis1
 * @property integer $blt_imns_hepatitis2
 * @property integer $blt_imns_hepatitis3
 * @property integer $blt_imns_bcg
 * @property integer $blt_imns_dpthb1
 * @property integer $blt_imns_dpthb2
 * @property integer $blt_imns_dpthb3
 * @property integer $blt_imns_polio1
 * @property integer $blt_imns_polio2
 * @property integer $blt_imns_polio3
 * @property integer $blt_imns_polio4
 * @property integer $blt_imns_campak
 * @property integer $blt_imns_tt1
 * @property integer $blt_imns_tt2
 * @property integer $blt_diare
 * @property integer $blt_diare_oralit
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PosyanduKegiatan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posyandu_kegiatan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'tahun', 'no', 'bulan'], 'required'],
            [['no', 'ih_periksa', 'ih_fe', 'i_asi', 'kd_kondom', 'kd_pil', 'kd_suntik', 'pb_jbsp', 'pb_kms', 'pb_timbang', 'pb_naik', 'pb_bgm_l', 'pb_bgm_p', 'blt_vit_a', 'blt_kms_out', 'blt_fe_1', 'blt_fe_2', 'blt_pmt', 'blt_imns_hepatitis0', 'blt_imns_hepatitis1', 'blt_imns_hepatitis2', 'blt_imns_hepatitis3', 'blt_imns_bcg', 'blt_imns_dpthb1', 'blt_imns_dpthb2', 'blt_imns_dpthb3', 'blt_imns_polio1', 'blt_imns_polio2', 'blt_imns_polio3', 'blt_imns_polio4', 'blt_imns_campak', 'blt_imns_tt1', 'blt_imns_tt2', 'blt_diare', 'blt_diare_oralit'], 'integer'],

            [['ih_periksa', 'ih_fe', 'i_asi', 'kd_kondom', 'kd_pil', 'kd_suntik', 'pb_jbsp', 'pb_kms', 'pb_timbang', 'pb_naik', 'pb_bgm_l', 'pb_bgm_p', 'blt_vit_a', 'blt_kms_out', 'blt_fe_1', 'blt_fe_2', 'blt_pmt', 'blt_imns_hepatitis0', 'blt_imns_hepatitis1', 'blt_imns_hepatitis2', 'blt_imns_hepatitis3', 'blt_imns_bcg', 'blt_imns_dpthb1', 'blt_imns_dpthb2', 'blt_imns_dpthb3', 'blt_imns_polio1', 'blt_imns_polio2', 'blt_imns_polio3', 'blt_imns_polio4', 'blt_imns_campak', 'blt_imns_tt1', 'blt_imns_tt2', 'blt_diare', 'blt_diare_oralit'], 'default', 'value' => 0],

            [['kd_desa'], 'string', 'max' => 12],
            [['tahun'], 'string', 'max' => 4],
            [['bulan'], 'string', 'max' => 2],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'tahun' => 'Tahun',
            'no' => 'No',
            'ih_periksa' => 'Ibu Hamil Periksa',
            'ih_fe' => 'Ibu Hamil Fe',
            'i_asi' => 'Jumlah Ibu yang menyusui',
            
            'kd_kondom' => 'KB Kondom/Jiwa',
            'kd_pil' => 'KB Pil/Jiwa',
            'kd_suntik' => 'KB Suntik/Jiwa',
            
            'pb_jbsp' => 'Jumlah Balita Sasaran Posyandu',
            'pb_kms' => 'Jumlah Balita Punya KMS',
            'pb_timbang' => 'Jumlah Balita Ditimbang',
            'pb_naik' => 'Jumlah Balita Naik BB',
            'pb_bgm_l' => 'BGM Laki-Laki',
            'pb_bgm_p' => 'BGM Perempuan',

            'blt_vit_a' => 'Balita Mendapat Vitamin A',
            'blt_kms_out' => 'KMS yang keluar',
            'blt_fe_1' => 'Balita Dapat Fe 1',
            'blt_fe_2' => 'Balita Dapat Fe 2',
            'blt_pmt' => 'Balita Dapat PMT',

            'blt_imns_hepatitis0' => 'Hepatitis-0',
            'blt_imns_hepatitis1' => 'Hepatitis-1',
            'blt_imns_hepatitis2' => 'Hepatitis-2',
            'blt_imns_hepatitis3' => 'Hepatitis-3',
            'blt_imns_bcg' => 'BCG',
            'blt_imns_dpthb1' => 'DPT-HB-1',
            'blt_imns_dpthb2' => 'DPT-HB-2',
            'blt_imns_dpthb3' => 'DPT-HB-3',

            'blt_imns_polio1' => 'Polio-1',
            'blt_imns_polio2' => 'Polio-2',
            'blt_imns_polio3' => 'Polio-3',
            'blt_imns_polio4' => 'Polio-4',
            'blt_imns_campak' => 'Campak',
            'blt_imns_tt1' => 'TT-1',
            'blt_imns_tt2' => 'TT-2',

            'blt_diare' => 'Jumlah Balita Diare',
            'blt_diare_oralit' => 'Jumlah Balita Diare Dapat Oralit',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
}
