<?php
namespace sp3d\modules\posyandu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "posyandu_isian".
 *
 * @property string $kd_desa
 * @property integer $no
 * @property string $tahun
 * @property string $strata
 * @property string $pj_umum
 * @property string $pj_operasional
 * @property string $ka_pelaksana
 * @property string $sekretaris
 * @property integer $i_paud
 * @property integer $i_bkb
 * @property integer $i_dll
 * @property integer $jml_anggota_aktif
 * @property integer $jml_anggota_nonaktif
 * @property integer $jml_petugas_kb
 * @property integer $jml_medis_paramedis
 * @property integer $jml_bidan_desa
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PosyanduIsian extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posyandu_isian';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'no', 'tahun'], 'required'],
            [['no', 'i_paud', 'i_bkb', 'i_dll', 'jml_anggota_aktif', 'jml_anggota_nonaktif', 'jml_petugas_kb', 'jml_medis_paramedis', 'jml_bidan_desa'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['tahun'], 'string', 'max' => 4],
            [['strata'], 'string', 'max' => 20],
            [['pj_umum', 'pj_operasional', 'ka_pelaksana', 'sekretaris'], 'string', 'max' => 100],
            [['keterangan'], 'string', 'max' => 255],
            [['created_by', 'updated_by'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'no' => 'No',
            'tahun' => 'Tahun',
            'strata' => 'Strata',
            'pj_umum' => 'Penanggungjawab Umum',
            'pj_operasional' => 'Penanggungjawab Operasional',
            'ka_pelaksana' => 'Ketua Pelaksana',
            'sekretaris' => 'Sekretaris',
            'i_paud' => 'Integrasi PAUD',
            'i_bkb' => 'Integrasi BKB',
            'i_dll' => 'Integrasi Lain-Lain',
            'jml_anggota_aktif' => 'Jumlah Anggota Aktif',//Jumlah Kader Posyandu
            'jml_anggota_nonaktif' => 'Jumlah Anggota Nonaktif',
            'jml_petugas_kb' => 'Jumlah Petugas KB',
            'jml_medis_paramedis' => 'Jumlah Medis Paramedis',
            'jml_bidan_desa' => 'Jumlah Bidan Desa',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    public function getPosyandu()
    {
        return $this->hasOne(Posyandu::className(), ['kd_desa' => 'kd_desa', 'no' => 'no']);
    }

    public function getData()
    {
        return $this->hasMany(PosyanduData::className(), ['kd_desa' => 'kd_desa', 'no' => 'no', 'tahun' => 'tahun']);
    }

    public function getKegiatan()
    {
        return $this->hasMany(PosyanduKegiatan::className(), ['kd_desa' => 'kd_desa', 'no' => 'no', 'tahun' => 'tahun']);
    }

    public function getSarana()
    {
        return $this->hasOne(PosyanduSarana::className(), ['kd_desa' => 'kd_desa', 'no' => 'no', 'tahun' => 'tahun']);
    }

    //event
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        PosyanduData::deleteAll(['kd_desa' => $this->kd_desa, 'no' => $this->no, 'tahun' => $this->tahun]);
        PosyanduKegiatan::deleteAll(['kd_desa' => $this->kd_desa, 'no' => $this->no, 'tahun' => $this->tahun]);
        PosyanduSarana::deleteAll(['kd_desa' => $this->kd_desa, 'no' => $this->no, 'tahun' => $this->tahun]);

        return true;
    }
}
