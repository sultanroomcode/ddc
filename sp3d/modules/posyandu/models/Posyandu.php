<?php
namespace sp3d\modules\posyandu\models;

use Yii;
use sp3d\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "posyandu".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $no_rt
 * @property integer $no_rw
 * @property integer $dusun_lingkungan
 * @property integer $no
 * @property string $nama
 * @property string $alamat
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Posyandu extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posyandu';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'no'], 'required'],
            [['no_rw', 'no_rt', 'no'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['jenis'], 'string', 'max' => 20],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama', 'dusun_lingkungan'], 'string', 'max' => 100],
            [['alamat'], 'string', 'max' => 250],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_desa' => 'Kode Desa',
            'no_rw' => 'Rukun Warga',
            'no_rt' => 'Rukun Tetangga',
            'dusun_lingkungan' => 'Dusun/Lingkungan',
            'no' => 'No Posyandu', //Posyandu         
            'nama' => 'Nama Posyandu',
            'alamat' => 'Alamat Posyandu',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
    //relation
    public function getIsian()
    {
        return $this->hasMany(PosyanduIsian::className(), ['kd_desa' => 'kd_desa', 'no' => 'no']);
    }
    //event
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        foreach (PosyanduIsian::findAll(['kd_desa' => $this->kd_desa, 'no' => $this->no]) as $v) {
            PosyanduIsian::findOne([
                'kd_desa' => $v->kd_desa,
                'tahun' => $v->tahun,
                'no' => $v->no])->delete();
        }

        return true;
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('no');
        if($mx == null){
            $this->no = 1;
        } else {
            $this->no = $mx +1;
        }
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }
}
