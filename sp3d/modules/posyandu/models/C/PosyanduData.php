<?php

namespace sp3d\modules\posyandu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "posyandu_data".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property string $tahun
 * @property integer $no_rw
 * @property integer $no
 * @property string $bulan
 * @property integer $by_012
 * @property integer $blt_1324
 * @property integer $blt_2559
 * @property integer $wus
 * @property integer $ibu_pus
 * @property integer $ibu_hamil
 * @property integer $ibu_asi
 * @property integer $jml_bayi_lahir
 * @property integer $jml_bayi_wft
 * @property integer $jml_ibu_wft
 * @property integer $jml_kader_pkk
 * @property integer $jml_plkb
 * @property integer $jml_medis_paramedis
 * @property integer $ibu_nf_fe
 * @property integer $ibu_nf_vit_a
 * @property integer $ibu_hamil_kek
 * @property integer $ibu_hamil_anemia
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PosyanduData extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posyandu_data';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'tahun', 'no_rw', 'no', 'bulan'], 'required'],
            [['no_rw', 'no', 'by_012', 'blt_1324', 'blt_2559', 'wus', 'ibu_pus', 'ibu_hamil', 'ibu_asi', 'jml_bayi_lahir', 'jml_bayi_wft', 'jml_ibu_wft', 'jml_kader_pkk', 'jml_plkb', 'jml_medis_paramedis', 'ibu_nf_fe', 'ibu_nf_vit_a', 'ibu_hamil_kek', 'ibu_hamil_anemia'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_desa'], 'string', 'max' => 12],
            [['tahun'], 'string', 'max' => 4],
            [['bulan'], 'string', 'max' => 2],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_desa' => 'Kode Desa',
            'tahun' => 'Tahun',
            'no_rw' => 'No RW',
            'no' => 'No',
            'bulan' => 'Bulan',
            'by_012' => 'Bayi 0-12',
            'blt_1324' => 'Balita 13-24',
            'blt_2559' => 'Balita 25-59',
            'wus' => 'Wanita Usia Subur',
            'ibu_pus' => 'Pasangan Usia Subur',
            'ibu_hamil' => 'Ibu Hamil',
            'ibu_asi' => 'Ibu Menyusui',
            'jml_bayi_lahir' => 'Jumlah Bayi Lahir',
            'jml_bayi_wft' => 'Jumlah Bayi Meninggal',
            'jml_ibu_wft' => 'Jumlah Ibu Meninggal',
            'jml_kader_pkk' => 'Jumlah Kader PKK',
            'jml_plkb' => 'Jumlah PLKB',
            'jml_medis_paramedis' => 'Jumlah Medis Paramedis',
            'ibu_nf_fe' => 'Ibu Nifas Yang Mendapat Fe',
            'ibu_nf_vit_a' => 'Ibu Nifas Vitamin A',
            'ibu_hamil_kek' => 'Ibu Hamil KEK',
            'ibu_hamil_anemia' => 'Ibu Hamil Anemia',
            'keterangan' => 'Keterangan',
            
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
