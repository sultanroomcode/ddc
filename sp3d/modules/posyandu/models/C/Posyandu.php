<?php

namespace sp3d\modules\posyandu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "posyandu".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $no_rw
 * @property integer $no
 * @property string $strata
 * @property string $nama
 * @property string $alamat
 * @property string $pj_umum
 * @property string $pj_operasional
 * @property string $ka_pelaksana
 * @property string $sekretaris
 * @property integer $i_paud
 * @property integer $i_bkb
 * @property integer $i_dll
 * @property integer $jml_anggota_aktif
 * @property integer $jml_anggota_nonaktif
 * @property integer $jml_petugas_kb
 * @property integer $jml_medis_paramedis
 * @property integer $jml_bidan_desa
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Posyandu extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posyandu';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'no_rw', 'no'], 'required'],
            [['no_rw', 'no', 'i_paud', 'i_bkb', 'i_dll', 'jml_anggota_aktif', 'jml_anggota_nonaktif', 'jml_petugas_kb', 'jml_medis_paramedis', 'jml_bidan_desa'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_desa'], 'string', 'max' => 12],
            [['strata'], 'string', 'max' => 20],
            [['nama', 'pj_umum', 'pj_operasional', 'ka_pelaksana', 'sekretaris'], 'string', 'max' => 100],
            [['alamat'], 'string', 'max' => 250],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_desa' => 'Kode Desa',
            'no_rw' => 'No Rw',
            'no' => 'No',
            'strata' => 'Strata',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'pj_umum' => 'Penanggungjawab Umum',
            'pj_operasional' => 'Penanggungjawab Operasional',
            'ka_pelaksana' => 'Ketua Pelaksana',
            'sekretaris' => 'Sekretaris',
            'i_paud' => 'Integrasi PAUD',
            'i_bkb' => 'Integrasi BKB',
            'i_dll' => 'Integrasi Lain-Lain',
            'jml_anggota_aktif' => 'Jumlah Anggota Aktif',//Jumlah Kader Posyandu
            'jml_anggota_nonaktif' => 'Jumlah Anggota Nonaktif',
            'jml_petugas_kb' => 'Jumlah Petugas KB',
            'jml_medis_paramedis' => 'Jumlah Medis Paramedis',
            'jml_bidan_desa' => 'Jumlah Bidan Desa',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    public function getData()
    {
        return $this->hasMany(PosyanduData::className(), ['kd_desa' => 'kd_desa', 'no' => 'no', 'no_rw' => 'no_rw']);
    }

    public function getKegiatan()
    {
        return $this->hasMany(PosyanduKegiatan::className(), ['kd_desa' => 'kd_desa', 'no' => 'no', 'no_rw' => 'no_rw']);
    }

    public function getSarana()
    {
        return $this->hasMany(PosyanduSarana::className(), ['kd_desa' => 'kd_desa', 'no' => 'no', 'no_rw' => 'no_rw']);
    }
}
