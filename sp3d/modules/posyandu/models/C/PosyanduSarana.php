<?php

namespace sp3d\modules\posyandu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "posyandu_sarana".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $no_rw
 * @property integer $no
 * @property string $tahun
 * @property integer $tp_gedung_sendiri
 * @property integer $tp_menumpang
 * @property integer $tp_mebeleur
 * @property integer $tmbg_bayi
 * @property integer $tmbg_balita
 * @property integer $tmbg_ibu
 * @property integer $buku_kib
 * @property integer $form_sip
 * @property integer $blanko_skdn
 * @property integer $bk_ctt_keu
 * @property integer $alat_peraga
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PosyanduSarana extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posyandu_sarana';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'no_rw', 'no', 'tahun'], 'required'],
            [['no_rw', 'no', 'tp_gedung_sendiri', 'tp_menumpang', 'tp_mebeleur', 'tmbg_bayi', 'tmbg_balita', 'tmbg_ibu', 'buku_kib', 'form_sip', 'blanko_skdn', 'bk_ctt_keu', 'alat_peraga'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_desa'], 'string', 'max' => 12],
            [['tahun'], 'string', 'max' => 4],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_desa' => 'Kode Desa',
            'no_rw' => 'No RW',
            'no' => 'No',
            'tahun' => 'Tahun',
            'tp_gedung_sendiri' => 'Tempat Pelayanan - Gedung Sendiri',
            'tp_menumpang' => 'Tempat Pelayanan - Menumpang',
            'tp_mebeleur' => 'Tempat Pelayanan - Mebeleur',
            'tmbg_bayi' => 'Timbangan Bayi',
            'tmbg_balita' => 'Timbangan Balita',
            'tmbg_ibu' => 'Timbangan Ibu',
            'buku_kib' => 'Jumlah Buku KIB',
            'form_sip' => 'Jumlah Formulir SIP',
            'blanko_skdn' => 'Jumlah Blanko SKDN',
            'bk_ctt_keu' => 'Jumlah Buku Catatan Keuangan',
            'alat_peraga' => 'Jumlah Alat Peraga Penyuluhan',
            'keterangan' => 'Keterangan',
            
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
