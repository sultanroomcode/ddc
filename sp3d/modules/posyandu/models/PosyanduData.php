<?php
namespace sp3d\modules\posyandu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "posyandu_data".
 *
 * @property string $kd_desa
 * @property string $tahun
 * @property integer $no
 * @property string $bulan
 * @property integer $by_012
 * @property integer $blt_1324
 * @property integer $blt_2559
 * @property integer $wus
 * @property integer $ibu_pus
 * @property integer $ibu_hamil
 * @property integer $ibu_asi
 * @property integer $jml_bayi_lahir
 * @property integer $jml_bayi_wft
 * @property integer $jml_ibu_wft
 * @property integer $jml_kader_pkk
 * @property integer $jml_plkb
 * @property integer $jml_medis_paramedis
 * @property integer $ibu_nf_fe
 * @property integer $ibu_nf_vit_a
 * @property integer $ibu_hamil_kek
 * @property integer $ibu_hamil_anemia
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PosyanduData extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posyandu_data';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'tahun', 'no', 'bulan'], 'required'],
            [['no', 'by_012', 'blt_1324', 'blt_2559', 'wus', 'ibu_pus', 'ibu_hamil', 'ibu_asi', 'jml_bayi_lahir', 'jml_bayi_wft', 'jml_ibu_wft', 'jml_kader_pkk', 'jml_plkb', 'jml_medis_paramedis', 'ibu_nf_fe', 'ibu_nf_vit_a', 'ibu_hamil_kek', 'ibu_hamil_anemia'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['tahun'], 'string', 'max' => 4],
            [['bulan'], 'string', 'max' => 2],
            [['keterangan'], 'string', 'max' => 255],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'tahun' => 'Tahun',
            'no' => 'No',
            'bulan' => 'Bulan',
            'by_012' => 'Bayi 0-12 Bulan/Jiwa',
            'blt_1324' => 'Balita 13-24 Bulan/Jiwa',
            'blt_2559' => 'Balita 25-59 Bulan/Jiwa',
            'wus' => 'Wanita Usia Subur/Jiwa',
            'ibu_pus' => 'Ibu Usia Subur/Jiwa',
            'ibu_hamil' => 'Ibu Hamil/Jiwa',
            'ibu_asi' => 'Ibu Menyusui/Jiwa',
            'jml_bayi_lahir' => 'Jumlah Bayi Lahir/Jiwa',
            'jml_bayi_wft' => 'Jumlah Bayi Meninggal/Jiwa',
            'jml_ibu_wft' => 'Jumlah Kematian Ibu Hamil/Jiwa',
            'jml_kader_pkk' => 'Jumlah Kader PKK',
            'jml_plkb' => 'Jumlah PLKB',
            'jml_medis_paramedis' => 'Jumlah Medis Paramedis',
            'ibu_nf_fe' => 'Ibu Nifas Yang Mendapat Fe',
            'ibu_nf_vit_a' => 'Ibu Nifas Yang Mendapat Vitamin A',
            'ibu_hamil_kek' => 'Ibu Hamil Yang Kurang Ene',
            'ibu_hamil_anemia' => 'Ibu Hamil Anemia',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
}
