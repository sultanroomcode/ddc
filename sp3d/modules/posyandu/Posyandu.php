<?php

namespace sp3d\modules\posyandu;

/**
 * posyandu module definition class
 */
class Posyandu extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'sp3d\modules\posyandu\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
