<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posyandu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Posyandu</h2>
            <div class="p-10" id="posyandu-area">
				<div class="posyandu-index">
				    <table class="table table-striped">
				        <tr>
				            <th>No</th>
				            <th>RT/RW</th>
				            <th>Nama Posyandu</th>
				            <th>Alamat</th>
				            <th>Tanggal Isi</th>
				        </tr>
				    <?php foreach($dataProvider->all() as $v): ?>
				        <tr>
				            <td><?= $v->no ?></td>
				            <td><?= $v->no_rt .'/'. $v->no_rw ?></td>
				            <td><?= $v->nama ?></td>
				            <td><?= $v->alamat ?></td>
				            <td><?= $v->created_at ?></td>
				    <?php endforeach; ?>
				    </table>
				</div>

            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>