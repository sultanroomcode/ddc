<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduSarana */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Saranas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-sarana-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'no',
            'tahun',
            'tp_gedung_sendiri',
            'tp_menumpang',
            'tp_mebeleur',
            'tmbg_bayi',
            'tmbg_balita',
            'tmbg_ibu',
            'buku_kib',
            'form_sip',
            'blanko_skdn',
            'bk_ctt_keu',
            'alat_peraga',
            'keterangan',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
