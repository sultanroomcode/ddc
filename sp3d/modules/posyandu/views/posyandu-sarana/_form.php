<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduSarana */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = $data['kd_desa'];
    $model->no = $data['no'];
    $model->tahun = $data['tahun'];
}

$arrTahun = ['2016' => '2016', '2017' => '2017', '2018' => '2018'];
$urlback = '/posyandu/posyandu-isian/view?kd_desa='.$data['kd_desa'].'&tahun='.$data['tahun'].'&no='.$data['no'];

$arrTemplate = ['item' => function($index, $label, $name, $checked, $value) {
                    $return = '<label class="containerx">';
                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.(($checked)?'checked':'').' tabindex="3">';
                    $return .= '<i></i>';
                    $return .= '<span class="checkmark"></span>';
                    $return .= '<span>' . ucwords($label) . '</span>';
                    $return .= '</label>';

                    return $return;
                }];
?>

<div class="posyandu-sarana-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'no')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'tahun')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend>Tempat Pelayanan</legend>
                <?= $form->field($model, 'tp_gedung_sendiri')->radioList(['1' => 'Ada', '0' => 'Tidak Ada'],$arrTemplate)->label('Gedung Sendiri') ?>

                <?= $form->field($model, 'tp_menumpang')->radioList(['1' => 'Ada', '0' => 'Tidak Ada'],$arrTemplate)->label('Menumpang') ?>

                <?= $form->field($model, 'tp_mebeleur')->radioList(['1' => 'Ada', '0' => 'Tidak Ada'],$arrTemplate)->label('Mebeleur') ?>
            </fieldset>

            <fieldset>
                <legend>Jumlah Timbangan</legend>
                <?= $form->field($model, 'tmbg_bayi')->textInput() ?>

                <?= $form->field($model, 'tmbg_balita')->textInput() ?>

                <?= $form->field($model, 'tmbg_ibu')->textInput() ?>
            </fieldset>
        
            <?= $form->field($model, 'buku_kib')->textInput() ?>

            <?= $form->field($model, 'form_sip')->textInput() ?>

            <?= $form->field($model, 'blanko_skdn')->textInput() ?>

            <?= $form->field($model, 'bk_ctt_keu')->textInput() ?>

            <?= $form->field($model, 'alat_peraga')->textInput() ?>

            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#posyandu-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);