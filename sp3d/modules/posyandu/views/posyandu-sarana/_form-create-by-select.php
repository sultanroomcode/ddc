<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use sp3d\modules\posyandu\models\PosyanduIsian;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];
/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduData */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = Yii::$app->user->identity->desa->kd_desa; 
}
$arrPosyandu = ArrayHelper::map(PosyanduIsian::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa])->all(),'no','posyandu.nama');
$arrTahun = [];
$arrBulan = ['01' => '01 Januari', '02' => '02 Februari', '03' => '03 Maret', '04' => '04 April', '05' => '05 Mei', '06' => '06 Juni', '07' => '07 Juli', '08' => '08 Agustus', '09' => '09 September', '10' => '10 Oktober', '11' => '11 November', '12' => '12 Desember'];

$urlback = '/posyandu/posyandu-sarana';

$arrTemplate = ['item' => function($index, $label, $name, $checked, $value) {
                    $return = '<label class="containerx">';
                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.(($checked)?'checked':'').' tabindex="3">';
                    $return .= '<i></i>';
                    $return .= '<span class="checkmark"></span>';
                    $return .= '<span>' . ucwords($label) . '</span>';
                    $return .= '</label>';

                    return $return;
                }];
?>

<div class="posyandu-data-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'no')->dropdownList($arrPosyandu,['onchange'=>'selecta()'])->label('Pilih Posyandu') ?>

            <?= $form->field($model, 'tahun')->dropdownList($arrTahun) ?>

            <fieldset>
                <legend>Tempat Pelayanan</legend>
                <?= $form->field($model, 'tp_gedung_sendiri')->radioList(['1' => 'Ada', '0' => 'Tidak Ada'],$arrTemplate)->label('Gedung Sendiri') ?>

                <?= $form->field($model, 'tp_menumpang')->radioList(['1' => 'Ada', '0' => 'Tidak Ada'],$arrTemplate)->label('Menumpang') ?>

                <?= $form->field($model, 'tp_mebeleur')->radioList(['1' => 'Ada', '0' => 'Tidak Ada'],$arrTemplate)->label('Mebeleur') ?>
            </fieldset>

            <fieldset>
                <legend>Jumlah Timbangan</legend>
                <?= $form->field($model, 'tmbg_bayi')->textInput() ?>

                <?= $form->field($model, 'tmbg_balita')->textInput() ?>

                <?= $form->field($model, 'tmbg_ibu')->textInput() ?>
            </fieldset>
        
            <?= $form->field($model, 'buku_kib')->textInput() ?>

            <?= $form->field($model, 'form_sip')->textInput() ?>

            <?= $form->field($model, 'blanko_skdn')->textInput() ?>

            <?= $form->field($model, 'bk_ctt_keu')->textInput() ?>

            <?= $form->field($model, 'alat_peraga')->textInput() ?>

            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
function selecta(){
    var kd_desa = $('#posyandusarana-kd_desa').val();
    var no = $('#posyandusarana-no').val();
    goLoad({elm:'#posyandusarana-tahun', url:'/posyandu/posyandu-isian/select-tahun?kd_desa='+kd_desa+'&no='+no });
}

selecta();
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#posyandu-area', url : '{$urlback}'});
            // modalxy.close('form-xy');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);