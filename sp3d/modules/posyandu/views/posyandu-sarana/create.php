<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduSarana */

$this->title = 'Create Posyandu Sarana';
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Saranas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-sarana-create">

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
