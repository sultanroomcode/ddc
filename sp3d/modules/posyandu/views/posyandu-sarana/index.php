<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Sarana Posyandu</h2>
            <div class="p-10" id="posyandu-area">
                <a href="javascript:void(0)" onclick="goLoad({elm:'#posyandu-area', url:'/posyandu/posyandu-sarana/create-by-select'})" class="btn btn-success"><i class="fa fa-plus"></i></a>

                <table class="table">
                    <tr>
                        <th valign="bottom">Tahun</th>

                        <th class="vth"><div><span>Tempat Pelayanan - Gedung Sendiri</span></div></th>
                        <th class="vth"><div><span>Tempat Pelayanan - Menumpang</span></div></th>
                        <th class="vth"><div><span>Tempat Pelayanan - Mebeleur</span></div></th>
                        <th class="vth"><div><span>Timbangan Bayi</span></div></th>
                        <th class="vth"><div><span>Timbangan Balita</span></div></th>
                        <th class="vth"><div><span>Timbangan Ibu</span></div></th>
                        <th class="vth"><div><span>Jumlah Buku KIB</span></div></th>
                        <th class="vth"><div><span>Jumlah Formulir SIP</span></div></th>
                        <th class="vth"><div><span>Jumlah Blanko SKDN</span></div></th>
                        <th class="vth"><div><span>Jumlah Buku Catatan Keuangan</span></div></th>
                        <th class="vth"><div><span>Jumlah Alat Peraga Penyuluhan</span></div></th>

                        <th valign="middle">Tanggal Isi</th>
                        <th valign="middle">Aksi</th>
                    </tr>
                <?php foreach($dataProvider->all() as $v): ?>
                    <tr>
                        <td><?= $v->tahun ?></td>
                    
                        <td><?= $v->tp_gedung_sendiri ?></td>
                        <td><?= $v->tp_menumpang ?></td>
                        <td><?= $v->tp_mebeleur ?></td>
                        <td><?= $v->tmbg_bayi ?></td>
                        <td><?= $v->tmbg_balita ?></td>
                        <td><?= $v->tmbg_ibu ?></td>
                        <td><?= $v->buku_kib ?></td>
                        <td><?= $v->form_sip ?></td>
                        <td><?= $v->blanko_skdn ?></td>
                        <td><?= $v->bk_ctt_keu ?></td>
                        <td><?= $v->alat_peraga ?></td>

                        <td><?= $v->created_at ?></td>
                        <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu-data/update?kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu-data/view?kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
                <?php endforeach; ?>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$counting = $dataPosyandu->count();
$scripts = <<<JS
var checking = {$counting};

if(checking == 0){
    $.alert('Mohon isi dulu data isian posyandu');
    goLoad({url:'/posyandu/posyandu-isian'});
}
JS;

$this->registerJs($scripts);