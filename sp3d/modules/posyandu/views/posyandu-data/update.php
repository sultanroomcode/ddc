<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduData */

$this->title = 'Update Data Posyandu : ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'tahun' => $model->tahun, 'no' => $model->no, 'bulan' => $model->bulan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posyandu-data-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
