<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Data Posyandu</h2>
            <div class="p-10" id="posyandu-area">
                <a href="javascript:void(0)" onclick="goLoad({elm:'#posyandu-area', url:'/posyandu/posyandu-data/create-by-select'})" class="btn btn-success"><i class="fa fa-plus"></i></a>

                <table class="table">
                    <tr>
                        <th valign="bottom">Tahun</th>
                        <th valign="bottom">Bulan</th>
                        <th class="vth"><div><span>Bayi 0-12 Bulan/jiwa</span></div></th>
                        <th class="vth"><div><span>Balita 13-24 Bulan/jiwa</span></div></th>
                        <th class="vth"><div><span>Baduta 25-59 Bulan/jiwa</span></div></th>

                        <th class="vth"><div><span>Wanita Usia Subur/jiwa</span></div></th>
                        <th class="vth"><div><span>Pasangan Usia Subur/pasang</span></div></th>
                        <th class="vth"><div><span>Ibu Hamil/jiwa</span></div></th>
                        <th class="vth"><div><span>Ibu Menyusui/jiwa</span></div></th>
                        <th class="vth"><div><span>* Bayi Lahir/jiwa</span></div></th>
                        <th class="vth"><div><span>* Bayi Meninggal/jiwa</span></div></th>
                        <th class="vth"><div><span>* Ibu Meninggal/jiwa</span></div></th>
                        <th class="vth"><div><span>* Kader PKK/jiwa</span></div></th>
                        <th class="vth"><div><span>* PLKB/jiwa</span></div></th>
                        <th class="vth"><div><span>* Medis Paramedis/jiwa</span></div></th>
                        <th class="vth"><div><span>Ibu Nifas Yang Mendapat Fe/jiwa</span></div></th>
                        <th class="vth"><div><span>Ibu Nifas Vitamin A/jiwa</span></div></th>
                        <th class="vth"><div><span>Ibu Hamil KEK/jiwa</span></div></th>
                        <th class="vth"><div><span>Ibu Hamil Anemia/jiwa</span></div></th>

                        <th valign="middle">Tanggal Isi</th>
                        <th valign="middle">Aksi</th>
                    </tr>
                <?php foreach($dataProvider->all() as $v): ?>
                    <tr>
                        <td><?= $v->tahun ?></td>
                        <td><?= $v->bulan ?></td>
                        <td><?= $v->by_012 ?></td>
                        <td><?= $v->blt_1324 ?></td>
                        <td><?= $v->blt_2559 ?></td>
                        <td><?= $v->wus ?></td>
                        <td><?= $v->ibu_pus ?></td>
                        <td><?= $v->ibu_hamil ?></td>
                        <td><?= $v->ibu_asi ?></td>
                        <td><?= $v->jml_bayi_lahir ?></td>
                        <td><?= $v->jml_bayi_wft ?></td>
                        <td><?= $v->jml_ibu_wft ?></td>
                        <td><?= $v->jml_kader_pkk ?></td>
                        <td><?= $v->jml_plkb ?></td>
                        <td><?= $v->jml_medis_paramedis ?></td>
                        <td><?= $v->ibu_nf_fe ?></td>
                        <td><?= $v->ibu_nf_vit_a ?></td>
                        <td><?= $v->ibu_hamil_kek ?></td>
                        <td><?= $v->ibu_hamil_anemia ?></td>
                        <td><?= $v->created_at ?></td>
                        <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu-data/update?kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu-data/view?kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
                <?php endforeach; ?>
                </table>

                NB : <br>
                - PLKB : Petugas Lapangan KB <br>
                - KEK : Kurang Energi Kronis <br>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$counting = $dataPosyandu->count();
$scripts = <<<JS
var checking = {$counting};

if(checking == 0){
    $.alert('Mohon isi dulu data isian posyandu');
    goLoad({url:'/posyandu/posyandu-isian'});
}
JS;

$this->registerJs($scripts);