<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduData */

$this->title = 'Isi Data Posyandu';
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-data-create">
	<h3 class="text-center"><?= Html::encode($this->title) ?></h3>
	
    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
