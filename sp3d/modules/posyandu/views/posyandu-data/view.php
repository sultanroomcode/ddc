<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduData */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-data-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'kd_desa' => $model->kd_desa, 'tahun' => $model->tahun, 'no' => $model->no, 'bulan' => $model->bulan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_desa' => $model->kd_desa, 'tahun' => $model->tahun, 'no' => $model->no, 'bulan' => $model->bulan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'tahun',
            'no',
            'bulan',
            
            'by_012',
            'blt_1324',
            'blt_2559',
            'wus',
            'ibu_pus',
            'ibu_hamil',
            'ibu_asi',
            'jml_bayi_lahir',
            'jml_bayi_wft',
            'jml_ibu_wft',
            'jml_kader_pkk',
            'jml_plkb',
            'jml_medis_paramedis',
            'ibu_nf_fe',
            'ibu_nf_vit_a',
            'ibu_hamil_kek',
            'ibu_hamil_anemia',
            'keterangan',

            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
