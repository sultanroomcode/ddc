<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduData */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = $data['kd_desa'];
    $model->no = $data['no'];
    $model->tahun = $data['tahun'];
}

$arrTahun = ['2016' => '2016', '2017' => '2017', '2018' => '2018'];
$arrBulan = ['01' => '01 Januari', '02' => '02 Februari', '03' => '03 Maret', '04' => '04 April', '05' => '05 Mei', '06' => '06 Juni', '07' => '07 Juli', '08' => '08 Agustus', '09' => '09 September', '10' => '10 Oktober', '11' => '11 November', '12' => '12 Desember'];

$urlback = '/posyandu/posyandu-isian/view?kd_desa='.$data['kd_desa'].'&tahun='.$data['tahun'].'&no='.$data['no'];
?>

<div class="posyandu-data-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'no')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'tahun')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-12">
            <?php if($model->isNewRecord){ ?>
                <?= $form->field($model, 'bulan')->dropdownList($arrBulan) ?>
            <?php } else { ?>                

                <?= $form->field($model, 'bulan')->hiddenInput()->label(false) ?>
            <?php } ?>
            <fieldset>
                <legend>Jumlah Pengunjung</legend>
                <?= $form->field($model, 'by_012')->textInput() ?>

                <?= $form->field($model, 'blt_1324')->textInput() ?>

                <?= $form->field($model, 'blt_2559')->textInput() ?>

                <?= $form->field($model, 'wus')->textInput() ?>

                <?= $form->field($model, 'ibu_pus')->textInput() ?>

                <?= $form->field($model, 'ibu_hamil')->textInput() ?>

                <?= $form->field($model, 'ibu_asi')->textInput() ?>
            </fieldset>
        
            <fieldset>
                <legend>Jumlah Bayi</legend>
                <?= $form->field($model, 'jml_bayi_lahir')->textInput()->label('Lahir/Jiwa') ?>

                <?= $form->field($model, 'jml_bayi_wft')->textInput()->label('Meninggal/Jiwa') ?>
            </fieldset>

            <?= $form->field($model, 'jml_ibu_wft')->textInput()->label('Jumlah Kematian Ibu Hamil/Jiwa') ?>

            <fieldset>
                <legend>Jumlah Ibu Nifas</legend>
                <?= $form->field($model, 'ibu_nf_fe')->textInput()->label('Dapat FE/Jiwa') ?>

                <?= $form->field($model, 'ibu_nf_vit_a')->textInput()->label('Dapat Vitamin A/Jiwa') ?>
            </fieldset>

            <fieldset>
                <legend>Jumlah Ibu Hamil</legend>
                <?= $form->field($model, 'ibu_hamil_kek')->textInput()->label('Kurang Energi Kronis (KEK)/Jiwa') ?>

                <?= $form->field($model, 'ibu_hamil_anemia')->textInput()->label('Anemia/Jiwa') ?>
            </fieldset>

            <fieldset>
                <legend>Jumlah Petugas Hadir</legend>
                <?= $form->field($model, 'jml_kader_pkk')->textInput()->label('Kader PKK/Posyandu/Jiwa') ?>

                <?= $form->field($model, 'jml_plkb')->textInput()->label('PLKB/PKB/Jiwa') ?>
                
                <?= $form->field($model, 'jml_medis_paramedis')->textInput()->label('Medis dan Paramedis/Jiwa') ?>
            </fieldset>

            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>    

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#posyandu-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);