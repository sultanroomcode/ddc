<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduIsian */

$this->title = 'Isian Posyandu';
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Isians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-isian-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
