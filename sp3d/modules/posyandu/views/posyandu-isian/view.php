<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduIsian */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Isians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-isian-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/view?kd_desa='.$model->kd_desa.'&no='.$model->no.'\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
        <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu-isian/update?kd_desa='.$model->kd_desa.'&no='.$model->no.'&tahun='.$model->tahun.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'no',
            'tahun',
            'strata',
            'pj_umum',
            'pj_operasional',
            'ka_pelaksana',
            'sekretaris',
            'i_paud',
            'i_bkb',
            'i_dll',
            'jml_anggota_aktif',
            'jml_anggota_nonaktif',
            'jml_petugas_kb',
            'jml_medis_paramedis',
            'jml_bidan_desa',
            'keterangan',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/posyandu/posyandu-data/create?kd_desa='.$model->kd_desa.'&no='.$model->no.'&tahun='.$model->tahun.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Data</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Bulan</th>
                <th>Bayi 0-12</th>
                <th>Balita 13-24</th>
                <th>Balita 25-59</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->data as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->bulan ?></td>
                    <td><?= $v->by_012 ?></td>
                    <td><?= $v->blt_1324 ?></td>
                    <td><?= $v->blt_2559 ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/posyandu/posyandu-data/view?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/posyandu/posyandu-data/update?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/posyandu/posyandu-data/delete?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
            Catatan : Dalam Bulan
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/posyandu/posyandu-kegiatan/create?kd_desa='.$model->kd_desa.'&no='.$model->no.'&tahun='.$model->tahun.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Kegiatan</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Bulan</th>
                <th>IH Periksa</th>
                <th>IH Mendapat FE</th>
                <th>IH Menyusui</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->kegiatan as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->bulan ?></td>
                    <td><?= $v->ih_periksa ?></td>
                    <td><?= $v->ih_fe ?></td>
                    <td><?= $v->i_asi ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/posyandu/posyandu-kegiatan/view?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/posyandu/posyandu-kegiatan/update?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/posyandu/posyandu-kegiatan/delete?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
            Catatan : IH : Ibu Hamil
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/posyandu/posyandu-sarana/create?kd_desa='.$model->kd_desa.'&no='.$model->no.'&tahun='.$model->tahun.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Sarana</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Gedung Sendiri</th>
                <th>Gedung Menumpang</th>
                <th>Gedung Mebeluer</th>
                <th>Aksi</th>
            </tr>
            <?php if(isset($model->sarana) || $model->sarana != null){ 
                $v = $model->sarana;
                ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->tp_gedung_sendiri ?></td>
                    <td><?= $v->tp_menumpang ?></td>
                    <td><?= $v->tp_mebeleur ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/posyandu/posyandu-sarana/view?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/posyandu/posyandu-sarana/update?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/posyandu/posyandu-sarana/delete?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php } ?>
            </table>
            Catatan : 1 = Ya, 0 = Tidak
        </div>
    </div>
    .
</div>
<?php
$urlBack = '/posyandu/posyandu-isian/view?kd_desa='.$model->kd_desa.'&tahun='.$model->tahun.'&no='.$model->no;
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#posyandu-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);