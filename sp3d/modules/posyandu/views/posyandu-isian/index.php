<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Isian Posyandu</h2>
            <div class="p-10" id="posyandu-area">
                <a href="javascript:void(0)" onclick="goLoad({elm:'#posyandu-area', url:'/posyandu/posyandu-isian/create-by-select'})" class="btn btn-success"><i class="fa fa-plus"></i></a>

                <table class="table">
                <?php foreach($dataProvider->all() as $v): ?>
                    <tr>
                        <td><?= $v->tahun ?></td>
                        <td><?= $v->strata ?></td>
                        <td><?= $v->pj_umum ?></td>
                        <td><?= $v->pj_operasional ?></td>
                        <td><?= $v->ka_pelaksana ?></td>
                        <td><?= $v->created_at ?></td>
                        <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/update?kd_desa='.$v->kd_desa.'&no='.$v->no.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/view?kd_desa='.$v->kd_desa.'&no='.$v->no.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
                <?php endforeach; ?>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$counting = $dataPosyandu->count();
$scripts = <<<JS
var checking = {$counting};

if(checking == 0){
    $.alert('Mohon isi dulu data posyandu');
    goLoad({url:'/posyandu'});
}
JS;

$this->registerJs($scripts);