<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];
/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduIsian */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = $data['kd_desa'];
    $model->no = $data['no'];
}

$strataList = [
    1 => 'Pratama',
    2 => 'Madya',
    3 => 'Purnama',
    4 => 'Mandiri',
];

$arrTahun = array_combine(range(2016,date('Y')),range(2016,date('Y')));

$arrTemplate = ['item' => function($index, $label, $name, $checked, $value) {
                    $return = '<label class="containerx">';
                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.(($checked)?'checked':'').' tabindex="3">';
                    $return .= '<i></i>';
                    $return .= '<span class="checkmark"></span>';
                    $return .= '<span>' . ucwords($label) . '</span>';
                    $return .= '</label>';

                    return $return;
                }];
$urlback = '/posyandu/posyandu/view?kd_desa='.$data['kd_desa'].'&no='.$data['no'];
?>

<div class="posyandu-isian-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'no')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'tahun')->dropdownList($arrTahun) ?>
            <fieldset>
                <legend>Struktur Organisasi</legend>
                    <?= $form->field($model, 'pj_umum')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'pj_operasional')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'ka_pelaksana')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'sekretaris')->textInput(['maxlength' => true]) ?>
            </fieldset>
        
            <?= $form->field($model, 'strata')->dropdownList($strataList) ?>

            <fieldset>
                <legend>Jumlah Kader Posyandu</legend>
                <?= $form->field($model, 'jml_anggota_aktif')->textInput()->label('Anggota Aktif') ?>
                <?= $form->field($model, 'jml_anggota_nonaktif')->textInput()->label('Anggota Non-Aktif') ?>
            </fieldset>

            <fieldset>
                <legend>Jumlah Petugas</legend>
                <?= $form->field($model, 'jml_petugas_kb')->radioList(['1' => 'Ada', '0' => 'Tidak'],$arrTemplate)->label('Petugas KB') ?>
                <?= $form->field($model, 'jml_medis_paramedis')->radioList(['1' => 'Ada', '0' => 'Tidak'],$arrTemplate)->label('Petugas Medis/Paramedis') ?>
                <?= $form->field($model, 'jml_bidan_desa')->radioList(['1' => 'Ada', '0' => 'Tidak'],$arrTemplate)->label('Bidan Desa') ?>
            </fieldset>
        
            <fieldset>
                <legend>Terintegrasi Program</legend>
                <?= $form->field($model, 'i_paud')->radioList(['1' => 'Ada', '0' => 'Tidak'],$arrTemplate)->label('Dengan PAUD') ?>
                <?= $form->field($model, 'i_bkb')->radioList(['1' => 'Ada', '0' => 'Tidak'],$arrTemplate)->label('Dengan BKB') ?>
                <?= $form->field($model, 'i_dll')->radioList(['1' => 'Ada', '0' => 'Tidak'],$arrTemplate)->label('Dengan DLL') ?>
            </fieldset>  

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            // $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#posyandu-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);