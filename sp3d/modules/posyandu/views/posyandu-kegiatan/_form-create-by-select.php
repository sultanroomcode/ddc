<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use sp3d\modules\posyandu\models\PosyanduIsian;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];
/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduData */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = Yii::$app->user->identity->desa->kd_desa; 
}
$arrPosyandu = ArrayHelper::map(PosyanduIsian::find()->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa])->all(),'no','posyandu.nama');
$arrTahun = [];
$arrBulan = ['01' => '01 Januari', '02' => '02 Februari', '03' => '03 Maret', '04' => '04 April', '05' => '05 Mei', '06' => '06 Juni', '07' => '07 Juli', '08' => '08 Agustus', '09' => '09 September', '10' => '10 Oktober', '11' => '11 November', '12' => '12 Desember'];

$urlback = '/posyandu/posyandu-kegiatan';
?>

<div class="posyandu-data-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'no')->dropdownList($arrPosyandu,['onchange'=>'selecta()'])->label('Pilih Posyandu') ?>

            <?= $form->field($model, 'tahun')->dropdownList($arrTahun) ?>

            <?php if($model->isNewRecord){ ?>
                <?= $form->field($model, 'bulan')->dropdownList($arrBulan) ?>
            <?php } else { ?> 
                <?= $form->field($model, 'bulan')->hiddenInput()->label(false) ?>
            <?php } ?>
            <fieldset>
                <legend>JUMLAH IBU HAMIL</legend>
                <?= $form->field($model, 'ih_periksa')->textInput()->label('Ibu Hamil yang memeriksakan diri/Jiwa') ?>
                <?= $form->field($model, 'ih_fe')->textInput()->label('Ibu Hamil yang mendapat Fe/Jiwa') ?>
            </fieldset>

            <?= $form->field($model, 'i_asi')->textInput() ?>

            <fieldset>
                <legend>JUMLAH PESERTA KB YANG MENDAPAT PELAYANAN</legend>
                <?= $form->field($model, 'kd_kondom')->textInput() ?>
                <?= $form->field($model, 'kd_pil')->textInput() ?>
                <?= $form->field($model, 'kd_suntik')->textInput() ?>
            </fieldset>

            <fieldset>
                <legend>PENIMBANGAN BALITA (JUMLAH)</legend>
                <?= $form->field($model, 'pb_jbsp')->textInput() ?>
                <?= $form->field($model, 'pb_kms')->textInput() ?>
                <?= $form->field($model, 'pb_timbang')->textInput() ?>
                <?= $form->field($model, 'pb_naik')->textInput() ?>
                <?= $form->field($model, 'pb_bgm_l')->textInput() ?>
                <?= $form->field($model, 'pb_bgm_p')->textInput() ?>
            </fieldset>
        
            <fieldset>
                <legend>JUMLAH BALITA</legend>
                <?= $form->field($model, 'blt_vit_a')->textInput() ?>
                <?= $form->field($model, 'blt_kms_out')->textInput() ?>
                <?= $form->field($model, 'blt_fe_1')->textInput() ?>
                <?= $form->field($model, 'blt_fe_2')->textInput() ?>
                <?= $form->field($model, 'blt_pmt')->textInput() ?>
            </fieldset>

            <fieldset>
                <legend>JUMLAH BALITA YANG DIIMUNISASI</legend>
                <?= $form->field($model, 'blt_imns_hepatitis0')->textInput() ?>
                <?= $form->field($model, 'blt_imns_hepatitis1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_hepatitis2')->textInput() ?>
                <?= $form->field($model, 'blt_imns_hepatitis3')->textInput() ?>
                <?= $form->field($model, 'blt_imns_bcg')->textInput() ?>
            </fieldset>
        
            <fieldset>
                <legend>JUMLAH BALITA YANG DIIMUNISASI (2)</legend>
                <?= $form->field($model, 'blt_imns_dpthb1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_dpthb2')->textInput() ?>
                <?= $form->field($model, 'blt_imns_dpthb3')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio2')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio3')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio4')->textInput() ?>
                <?= $form->field($model, 'blt_imns_campak')->textInput() ?>
                <?= $form->field($model, 'blt_imns_tt1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_tt2')->textInput() ?>
            </fieldset>

            <fieldset>
                <legend>JUMLAH BALITA YANG MENDERITA DIARE</legend>
                <?= $form->field($model, 'blt_diare')->textInput() ?>
                <?= $form->field($model, 'blt_diare_oralit')->textInput() ?>
            </fieldset>
            
            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
function selecta(){
    var kd_desa = $('#posyandukegiatan-kd_desa').val();
    var no = $('#posyandukegiatan-no').val();
    goLoad({elm:'#posyandukegiatan-tahun', url:'/posyandu/posyandu-isian/select-tahun?kd_desa='+kd_desa+'&no='+no });
}

selecta();
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#posyandu-area', url : '{$urlback}'});
            // modalxy.close('form-xy');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);