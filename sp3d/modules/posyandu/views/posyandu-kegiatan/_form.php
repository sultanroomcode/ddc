<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduKegiatan */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $model->kd_desa = $data['kd_desa'];
    $model->tahun = $data['tahun'];
    $model->no = $data['no'];
}

$arrTahun = ['2016' => '2016', '2017' => '2017', '2018' => '2018'];
$arrBulan = ['01' => '01 Januari', '02' => '02 Februari', '03' => '03 Maret', '04' => '04 April', '05' => '05 Mei', '06' => '06 Juni', '07' => '07 Juli', '08' => '08 Agustus', '09' => '09 September', '10' => '10 Oktober', '11' => '11 November', '12' => '12 Desember'];

$urlback = '/posyandu/posyandu-isian/view?kd_desa='.$data['kd_desa'].'&tahun='.$data['tahun'].'&no='.$data['no'];
?>

<div class="posyandu-kegiatan-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'no')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'tahun')->hiddenInput()->label(false) ?>
                
    <div class="row">
        <div class="col-md-12">
            <?php if($model->isNewRecord){ ?>
                <?= $form->field($model, 'bulan')->dropdownList($arrBulan) ?>
            <?php } else { ?> 
                <?= $form->field($model, 'bulan')->hiddenInput()->label(false) ?>
            <?php } ?>
            <fieldset>
                <legend>Jumlah Ibu Hamil</legend>
                <?= $form->field($model, 'ih_periksa')->textInput()->label('Ibu Hamil yang memeriksakan diri/Jiwa') ?>
                <?= $form->field($model, 'ih_fe')->textInput()->label('Ibu Hamil yang mendapat Fe/Jiwa') ?>
            </fieldset>

            <?= $form->field($model, 'i_asi')->textInput() ?>

            <fieldset>
                <legend>Jumlah Peserta KB Yang mendapat pelayanan</legend>
                <?= $form->field($model, 'kd_kondom')->textInput() ?>
                <?= $form->field($model, 'kd_pil')->textInput() ?>
                <?= $form->field($model, 'kd_suntik')->textInput() ?>
            </fieldset>

            <fieldset>
                <legend>Penimbangan Balita</legend>
                <?= $form->field($model, 'pb_jbsp')->textInput() ?>
                <?= $form->field($model, 'pb_kms')->textInput() ?>
                <?= $form->field($model, 'pb_timbang')->textInput() ?>
                <?= $form->field($model, 'pb_naik')->textInput() ?>
                <?= $form->field($model, 'pb_bgm_l')->textInput() ?>
                <?= $form->field($model, 'pb_bgm_p')->textInput() ?>
            </fieldset>
       
            <fieldset>
                <legend>Jumlah Balita</legend>
                <?= $form->field($model, 'blt_vit_a')->textInput() ?>
                <?= $form->field($model, 'blt_kms_out')->textInput() ?>
                <?= $form->field($model, 'blt_fe_1')->textInput() ?>
                <?= $form->field($model, 'blt_fe_2')->textInput() ?>
                <?= $form->field($model, 'blt_pmt')->textInput() ?>
            </fieldset>

            <fieldset>
                <legend>Jumlah Balita Yang Di imunisasi</legend>
                <?= $form->field($model, 'blt_imns_hepatitis0')->textInput() ?>
                <?= $form->field($model, 'blt_imns_hepatitis1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_hepatitis2')->textInput() ?>
                <?= $form->field($model, 'blt_imns_hepatitis3')->textInput() ?>
                <?= $form->field($model, 'blt_imns_bcg')->textInput() ?>
            </fieldset>
       
            <fieldset>
                <legend>Jumlah Balita Yang Di imunisasi (2)</legend>
                <?= $form->field($model, 'blt_imns_dpthb1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_dpthb2')->textInput() ?>
                <?= $form->field($model, 'blt_imns_dpthb3')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio2')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio3')->textInput() ?>
                <?= $form->field($model, 'blt_imns_polio4')->textInput() ?>
                <?= $form->field($model, 'blt_imns_campak')->textInput() ?>
                <?= $form->field($model, 'blt_imns_tt1')->textInput() ?>
                <?= $form->field($model, 'blt_imns_tt2')->textInput() ?>
            </fieldset>

            <fieldset>
                <legend>Jumlah Balita Yang Menderita Diare</legend>
                <?= $form->field($model, 'blt_diare')->textInput() ?>
                <?= $form->field($model, 'blt_diare_oralit')->textInput() ?>
            </fieldset>
            
            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-7">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#posyandu-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);