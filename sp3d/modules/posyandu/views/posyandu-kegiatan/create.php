<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduKegiatan */

$this->title = 'Create Posyandu Kegiatan';
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-kegiatan-create">

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
