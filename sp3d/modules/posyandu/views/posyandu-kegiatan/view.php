<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduKegiatan */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-kegiatan-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'kd_desa' => $model->kd_desa, 'tahun' => $model->tahun, 'no_rw' => $model->no_rw, 'no' => $model->no, 'bulan' => $model->bulan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_desa' => $model->kd_desa, 'tahun' => $model->tahun, 'no_rw' => $model->no_rw, 'no' => $model->no, 'bulan' => $model->bulan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'kd_kecamatan',
            'kd_desa',
            'tahun',
            'no_rw',
            'no',
            'bulan',
            'ih_periksa',
            'ih_fe',
            'i_asi',
            'kd_kondom',
            'kd_pil',
            'kd_suntik',
            'pb_jbsp',
            'pb_kms',
            'pb_timbang',
            'pb_naik',
            'pb_bgm_l',
            'pb_bgm_p',
            'blt_vit_a',
            'blt_kms_out',
            'blt_fe_1',
            'blt_fe_2',
            'blt_pmt',
            'blt_imns_hepatitis0',
            'blt_imns_hepatitis1',
            'blt_imns_hepatitis2',
            'blt_imns_hepatitis3',
            'blt_imns_bcg',
            'blt_imns_dpthb1',
            'blt_imns_dpthb2',
            'blt_imns_dpthb3',
            'blt_imns_polio1',
            'blt_imns_polio2',
            'blt_imns_polio3',
            'blt_imns_polio4',
            'blt_imns_campak',
            'blt_imns_tt1',
            'blt_imns_tt2',
            'blt_diare',
            'blt_diare_oralit',
            'keterangan',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
