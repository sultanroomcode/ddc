<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Kegiatan Posyandu</h2>
            <div class="p-10" id="posyandu-area">
                <a href="javascript:void(0)" onclick="goLoad({elm:'#posyandu-area', url:'/posyandu/posyandu-kegiatan/create-by-select'})" class="btn btn-success"><i class="fa fa-plus"></i></a>

                <table class="table">
                    <tr>
                        <th valign="bottom">Tahun</th>
                        <th valign="bottom">Bulan</th>

                        <th class="vth"><div><span>Ibu Hamil Periksa</span></div></th>
                        <th class="vth"><div><span>Ibu Hamil Fe</span></div></th>
                        <th class="vth"><div><span>Jumlah Ibu yang menyusui</span></div></th>
                        <th class="vth"><div><span>KB Kondom</span></div></th>
                        <th class="vth"><div><span>KB Pil</span></div></th>
                        <th class="vth"><div><span>KB Suntik</span></div></th>
                        <th class="vth"><div><span>Jumlah Balita Sasaran Posyandu</span></div></th>
                        <th class="vth"><div><span>Jumlah Balita Punya KMS</span></div></th>
                        <th class="vth"><div><span>Jumlah Balita Ditimbang</span></div></th>
                        <th class="vth"><div><span>Jumlah Balita Naik BB</span></div></th>
                        <th class="vth"><div><span>BGM Laki-Laki</span></div></th>
                        <th class="vth"><div><span>BGM Perempuan</span></div></th>
                        <th class="vth"><div><span>Balita Mendapat Vitamin A</span></div></th>
                        <th class="vth"><div><span>KMS yang keluar</span></div></th>
                        <th class="vth"><div><span>Balita Dapat Fe 1</span></div></th>
                        <th class="vth"><div><span>Balita Dapat Fe 2</span></div></th>
                        <th class="vth"><div><span>Balita Dapat PMT</span></div></th>
                        <th class="vth"><div><span>Jumlah Balita Diare</span></div></th>
                        <th class="vth"><div><span>Jumlah Balita Diare Dapat Oralit</span></div></th>

                        <th valign="middle">Tanggal Isi</th>
                        <th valign="middle">Aksi</th>
                    </tr>
                <?php foreach($dataProvider->all() as $v): ?>
                    <tr>
                        <td><?= $v->tahun ?></td>
                        <td><?= $v->bulan ?></td>
                        
                        <td><?= $v->ih_periksa ?></td>
                        <td><?= $v->ih_fe ?></td>
                        <td><?= $v->i_asi ?></td>
                        <td><?= $v->kd_kondom ?></td>
                        <td><?= $v->kd_pil ?></td>
                        <td><?= $v->kd_suntik ?></td>
                        <td><?= $v->pb_jbsp ?></td>
                        <td><?= $v->pb_kms ?></td>
                        <td><?= $v->pb_timbang ?></td>
                        <td><?= $v->pb_naik ?></td>
                        <td><?= $v->pb_bgm_l ?></td>
                        <td><?= $v->pb_bgm_p ?></td>
                        <td><?= $v->blt_vit_a ?></td>
                        <td><?= $v->blt_kms_out ?></td>
                        <td><?= $v->blt_fe_1 ?></td>
                        <td><?= $v->blt_fe_2 ?></td>
                        <td><?= $v->blt_pmt ?></td>
                        <td><?= $v->blt_diare ?></td>
                        <td><?= $v->blt_diare_oralit ?></td>

                        <td><?= $v->created_at ?></td>
                        <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu-data/update?kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu-data/view?kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun.'&bulan='.$v->bulan.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
                <?php endforeach; ?>
                </table>

                NB : <br>
                - BGM : Bawah Garis Merah <br>
                - KMS : Kartu Menuju Sehat
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$counting = $dataPosyandu->count();
$scripts = <<<JS
var checking = {$counting};

if(checking == 0){
    $.alert('Mohon isi dulu data isian posyandu');
    goLoad({url:'/posyandu/posyandu-isian'});
}
JS;

$this->registerJs($scripts);