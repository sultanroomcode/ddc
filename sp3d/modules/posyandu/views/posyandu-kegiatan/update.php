<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\PosyanduKegiatan */

$this->title = 'Update Posyandu Kegiatan: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Posyandu Kegiatans', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'tahun' => $model->tahun, 'no_rw' => $model->no_rw, 'no' => $model->no, 'bulan' => $model->bulan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posyandu-kegiatan-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
