<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-8',
        'error' => '',
        'hint' => '',
    ],
]];
/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    if(isset(Yii::$app->user->identity->desa->kd_desa))
    {
        $user = Yii::$app->user->identity->desa->kd_desa;
    } else {
        $user = Yii::$app->user->identity->id;
    }
    $model->kd_desa = $user;
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);

    $model->genNum();
}
?>

<div class="posyandu-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>
    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'no')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-7">

            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'no_rt')->textInput() ?>
            
            <?= $form->field($model, 'no_rw')->textInput() ?>
            
            <?= $form->field($model, 'jenis')->dropdownList([
                'balita' => 'Balita',
                'remaja' => 'Remaja',
                'lansia' => 'Lansia',
            ]) ?>
            
            <?= $form->field($model, 'dusun_lingkungan')->textInput() ?>

            <?php // $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/index\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#posyandu-area', url : '/posyandu/posyandu/index'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);