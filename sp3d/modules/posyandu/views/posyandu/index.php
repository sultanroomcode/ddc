<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="posyandu-index">
    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#posyandu-area', url:'/posyandu/posyandu/create'})" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</a>
    </p>

    <table class="table">
        <tr>
            <th>No</th>
            <th>RT/RW</th>
            <th>Nama Posyandu</th>
            <th>Alamat</th>
            <th>Tanggal Isi</th>
            <th>Aksi</th>
        </tr>
    <?php foreach($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->no ?></td>
            <td><?= $v->no_rt .'/'. $v->no_rw ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->alamat ?></td>
            <td><?= $v->created_at ?></td>
            <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/update?kd_desa='.$v->kd_desa.'&no='.$v->no.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/view?kd_desa='.$v->kd_desa.'&no='.$v->no.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?>
                <a href="javascript:void(0)" onclick="deleteData('/posyandu/posyandu/delete?<?='kd_desa='.$v->kd_desa.'&no='.$v->no ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
            </td>
    <?php endforeach; ?>
    </table>
</div>
<?php
$urlBack = '/posyandu/posyandu/';
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#posyandu-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);