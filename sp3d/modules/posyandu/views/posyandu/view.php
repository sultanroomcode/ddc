<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Posyandu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/index\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
        <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#posyandu-area\', url:\'/posyandu/posyandu/update?kd_desa='.$model->kd_desa.'&no='.$model->no.'&no_rw='.$model->no_rw.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>' ?>
    </p>

    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'kabupaten.description',
                    'kecamatan.description',
                    'desa.description',
                    'kd_desa',
                    'no_rw',
                    'no_rt',
                    'dusun_lingkungan',
                    'no',
                    'nama',
                    'alamat',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'keterangan',
                    'created_by',
                    'updated_by',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXy({url:\'/posyandu/posyandu-isian/create?kd_desa='.$model->kd_desa.'&no='.$model->no.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Isian</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Strata</th>
                <th>Penanggung Jawab Umum</th>
                <th>Penanggung Jawab Operasional</th>
                <th>Sekretaris</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->isian as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->strata ?></td>
                    <td><?= $v->pj_umum ?></td>
                    <td><?= $v->pj_operasional ?></td>
                    <td><?= $v->sekretaris ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="goLoad({elm:'#posyandu-area', url:'/posyandu/posyandu-isian/view?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXy({url:'/posyandu/posyandu-isian/update?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/posyandu/posyandu-isian/delete?<?='kd_desa='.$v->kd_desa.'&no='.$v->no.'&tahun='.$v->tahun ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>            
        </div>
    </div>
    .
</div>
<?php
$urlBack = '/posyandu/posyandu/view?kd_desa='.$model->kd_desa.'&no_rw='.$model->no_rw.'&no='.$model->no;
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#posyandu-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);