<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */

$this->title = 'Update Posyandu: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Posyandus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'no_rw' => $model->no_rw, 'no' => $model->no]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posyandu-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
