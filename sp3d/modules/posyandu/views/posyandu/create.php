<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */

$this->title = 'Create Posyandu';
$this->params['breadcrumbs'][] = ['label' => 'Posyandus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posyandu-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
