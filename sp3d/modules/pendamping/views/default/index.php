<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'SP3D | Registrasi Pendamping Desa';
?>
<div class="wrapper animated bounceInDown">
    <div class="title"><h1>Pendamping Desa <b>SP3D</b></h1></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="top-header">
                    <img src="../imgf/logo.png">
                    <p>Dinas Pemberdayaan Masyarakat Dan Desa<br>Provinsi Jawa Timur</p>

                </div>
                <div class="box-content">

                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>        
                    <div class="row">
                        <div class="col-xs-12 col-sm-12" style="padding-top:150px;">
                            
                            <?= $form->field($model, 'email', ['options' => ['class' => 'form-group margino']])->textInput(['placeholder'=>'Masukkan Alamat Email'])->label('Email') ?>
                            <?= $form->field($model, 'verifyCode', ['options' => ['class' => 'form-group margino']])->widget(Captcha::className(), [
                                'template' => '{image}<br>{input}',
                                'captchaAction' => '/pendamping/default/captcha',
                                'options'=> ['placeholder' => 'Masukkan Karakter Kode Captcha diatas', 'class' => 'form-control']
                            ])->label(false) ?>
                            <div class="form-group">
                                <?= Html::submitButton('Daftar <span class="fa fa-lock" style="margin-top: -12em;font-size:12px; color:#fff"></span>', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>  
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$style =<<<CSS
body{ 
  background: url(../imgf/surabaya.png) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.margino {
    margin: 1em 1em -2em 1em;
}
.wrapper{
  margin-left: auto;
  margin-right: auto;
  max-width:26em; 
  margin-top: 40px;
}
@media (max-width: 500px){
 .wrapper{
  margin-top: 10px;
  } 
}
.title h1{
  text-align: center;
  max-width:50em; 
  padding: 5px 0 15px 0;
  font-size: 36px;
  font-weight: 600;
  color: #eee;
}
@media (max-width: 500px){
  .title h1{
    font-size: 25px;
  }
}
.box {
  background-color: #fff;
  margin-left: auto;
  margin-right: auto;
  max-width:26em; 
}
.box{
      -webkit-box-shadow: 0 10px 6px -6px #777;
         -moz-box-shadow: 0 10px 6px -6px #777;
              box-shadow: 0 10px 6px -6px #777;
    }
.top-header{
  max-width:26em;
  margin-left: auto;
  margin-right: auto; 
  background: url(../imgf/header-login.png )no-repeat right ;
  min-height: 100px;
}
.top-header img{
  padding-top: 10px;
  width: 50px;
  display: block;
  margin-left: auto;
  margin-right: auto;
}
.top-header p{
  margin: 5px 0;
  padding-bottom: 5px;
  text-align: center;
  color: #999;
  text-transform: uppercase;
  font-size: 12px;
  letter-spacing: 2px;
}
.box-content{
  padding: 0 15px;
}
.form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 1px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.btn {
  text-align: center;
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
      touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 0;
}
CSS;

$this->registerCss($style);