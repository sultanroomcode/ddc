<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\TaMdbUpload */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->user = Yii::$app->user->identity->id;
	// $model->nama_file = Yii::$app->user->identity->id;
}
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Profile -- Form</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

                <?= $form->field($model, 'nik')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'foto_profile_box')->fileInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'jenis_kelamin')->dropdownList(['L' => 'Laki-Laki', 'P' => 'Perempuan']) ?>

                <?= $form->field($model, 'user')->hiddenInput(['maxlength' => true])->label(false) ?>

                <progress id="prog" max="100" value="0" style="display:none;"></progress>
            	<div id="percent"></div>

            	<div class="progress">
            	  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><b class="prog-text"></b>
            	    <span class="sr-only" class="prog-text"></span>
            	  </div>
            	</div>

                <div class="form-group">
                    <button type="submit" name="sending" id="sending" class="btn btn-success"> Simpan </button>

                    <a href="javascript:void(0)" onclick="goLoad({url: '/pendamping/data/view'})" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
var bar = $('.progress-bar');
var percent = $('.prog-text');

$('form#{$model->formName()}').ajaxForm({
    beforeSend: function() {
        var percentVal = 0;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : '0%'}); 
        percent.html(percentVal+' %');
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    success: function() {
        var percentVal = 100;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    complete: function(xhr) {
        if(xhr.responseText == 1){
            $(this).trigger('reset');
            goLoad({url:'/pendamping/data/view'});
        } else {
            
        }
    }
}); 
JS;

$this->registerJs($script);