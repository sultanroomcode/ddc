<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use sp3d\models\User;
/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\TaMdbUpload */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord && $model2 != null){
    $model->nik = $model2->nik;
}
$arr = ['-' => 'Pilih Kabupaten'];
$kabArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');

$kabArr = $arr + $kabArr;//https://stackoverflow.com/questions/3292044/php-merge-two-arrays-while-keeping-keys-instead-of-reindexing
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Tambah Desa -- Form</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

                <?= $form->field($model, 'nik')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'kd_kab')->dropdownList($kabArr) ?>
                <?= $form->field($model, 'kd_kec')->dropdownList(['-' => 'Pilih Kabupaten']) ?>
                <?= $form->field($model, 'kd_desa')->dropdownList(['-' => 'Pilih Kecamatan']) ?>

                <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

                <progress id="prog" max="100" value="0" style="display:none;"></progress>
            	<div id="percent"></div>

            	<div class="progress">
            	  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><b class="prog-text"></b>
            	    <span class="sr-only" class="prog-text"></span>
            	  </div>
            	</div>

                <div class="form-group">
                    <button type="submit" name="sending" id="sending" class="btn btn-success"> Simpan </button>

                    <a href="javascript:void(0)" onclick="goLoad({url: '/pendamping/data/view'})" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$model2exist = ($model2 == null)?'null':'exist';
$script = <<<JS
$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
var bar = $('.progress-bar');
var percent = $('.prog-text');
var check = '{$model2exist}';

if(check == 'null'){
    $.alert('Silahkan mengisi data profile Anda terlebih dahulu');
    goLoad({url:'/pendamping/data/isi-profile'});
}

$('#sp3dpendampingpilihdesa-kd_kab').change(function(){
  var kode = $(this).val();
  $('#sp3dpendampingpilihdesa-kd_kec').html('<option value="-">Pilih Kecamatan</option>');
  $('#sp3dpendampingpilihdesa-kd_desa').html('<option value="-">Pilih Kecamatan</option>');
  if(kode !== ''){
    goLoad({elm:'#sp3dpendampingpilihdesa-kd_kec', url:'/pendamping/data/data-option?type=kecamatan&kode='+kode});
  }
});

$('#sp3dpendampingpilihdesa-kd_kec').change(function(){
  var kode = $(this).val();
  $('#sp3dpendampingpilihdesa-kd_desa').html('<option value="-">Pilih Desa</option>');
  if(kode !== ''){
    goLoad({elm:'#sp3dpendampingpilihdesa-kd_desa', url:'/pendamping/data/data-option?type=desa&kode='+kode});
  }
});

$('form#{$model->formName()}').ajaxForm({
    beforeSend: function() {
        var percentVal = 0;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : '0%'}); 
        percent.html(percentVal+' %');
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    success: function() {
        var percentVal = 100;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    complete: function(xhr) {
        if(xhr.responseText == 1){
            $(this).trigger('reset');
            goLoad({url:'/pendamping/data/view'});
        } else {
            
        }
    }
}); 
JS;

$this->registerJs($script);