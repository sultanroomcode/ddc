<?php
use sp3d\models\transaksi\Sp3dPendampingDesa;
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">View Akun Pendamping</h2>
            <div class="p-10">
            	ID Registrasi : <?= $model->id ?><br>
				Tanggal Registrasi : <?= $model->created_at ?><br>
				Email terhubung : <?= $model->email ?><br>
				Username : <?= $model->user->username ?><br>
				Status : <?= ($model->status == 10)?'Terverifikasi':'Belum Terverifikasi' ?><br>
				Tanggal Terverifikasi : <?= $model->verified_at ?>
				<br>
				<?php
				if($model->dataext != null){
					$m2 = $model->dataext;
					echo '<hr>NIK : '. $m2->nik.'<br>';
					echo 'Nama : '. $m2->nama.'<br>';
					echo 'TTL : '. $m2->tempat_lahir.', '. $m2->tanggal_lahir_format() .'<br>';
					echo 'Alamat : '. $m2->alamat.'<br>';
					echo 'Jenis Kelamin : '. (($m2->jenis_kelamin == 'L')?'Laki-Laki':'Perempuan').'<br>';
					?>
					<a href="javascript:void(0)" onclick="goLoad({url:'/pendamping/data/update-profile'})" class="btn btn-primary">Update Profil</a>
					<?php 
				} else {
					?>
					<a href="javascript:void(0)" onclick="goLoad({url:'/pendamping/data/isi-profile'})" class="btn btn-danger">Isi Profil</a>
					<?php
				}
				?>

				<a href="javascript:void(0)" onclick="goLoad({url:'/pendamping/data/tambah-desa'})" class="btn btn-danger">Tambah Desa Terhubung</a>

				<div class="block-area" id="tableStriped">
				    <h3 class="block-title">Desa Terhubung</h3>
				    <div class="table-responsive overflow">
				    	<table id="ta-mdb-list" class="tile table table-bordered table-striped" cellspacing="0">
						    <thead>
						        <tr>
						            <th>Nama Desa</th>
						            <th>Status Verifikasi Desa</th>
						            <th>Tanggal Verifikasi Desa</th>
						            <th>Tanggal Submit</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php if(isset($model->dataext->datadesa)){ foreach ($model->dataext->datadesa as $v): ?>
						        <tr>
						            <td><?= $v->desa->description ?></td>
						            <td><?= (($v->verified_status == 0)?'Belum Terverifikasi Desa':'Terverifikasi Desa') ?></td>
						            <td><?= $v->verified_at ?></td>
						            <td><?= $v->submited_at ?></td>
						        </tr>
						        <?php endforeach; } ?>
						    </tbody>
						</table>
				    </div>
				</div><br>
				.
			</div>
		</div>
	</div>
</div>

