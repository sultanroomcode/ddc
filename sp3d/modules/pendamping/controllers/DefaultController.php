<?php
namespace sp3d\modules\pendamping\controllers;

use Yii;
use yii\web\Controller;
use sp3d\models\transaksi\Sp3dPendampingDesaLogin;
/**
 * Default controller for the `pendamping` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    // url test pendamping/default/verify-register?mail=agussutarom@gmail.com&registrycode=7383ce10c007072bf25a46c9f6fec02b
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
    	$this->layout = '@sp3d/views/layouts/frontend';
    	$model = new Sp3dPendampingDesaLogin();
        $model->scenario = 'registrasi';
        
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->save()) {
                $status = true;
            } else {
                $status = false;
            }

            return $this->render('index-after-post', [
                'model' => $model,
                'status' => $status
            ]);
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }        
    }

    public function actionChangePassword($mail)
    {
        return Yii::$app->mailer->compose()
            ->setTo($mail)
            ->setFrom(['noreply@sp3d.dpmd.jatimprov.go.id' => 'SP3D System Server'])
            ->setSubject('validasi pendaftaran pendamping')
            ->setHtmlBody('<b>Testing Pendaftaran Sukses</b><br>Anda telah mendaftar untuk pengisian biodata Pendamping Desa untuk Sistem Aplikasi SP3D Provinsi Jawa Timur, untuk memvalidasi email <a href="https://sp3d.dpmd.jatimprov.go.id/pendamping/default/verify-register?mail='.$mail.'&registrycode='.md5($mail).'" target="_blank">klik disini</a>. abaikan email ini jika anda tidak merasa mendaftarkannya')
            ->send();
    }

    public function actionVerifyRegister($mail, $registrycode)
    {
        $model = Sp3dPendampingDesaLogin::findOne(['email' => $mail, 'status' => 0]);
        // var_dump($model);
        // echo md5($model->email_code);
        if($model != null && md5($model->email_code) == $registrycode){

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->status = 10;
                $model->verified_at = date('Y-m-d H:i:s');
                // var_dump($model);
                $model->save();
                $model->loadDataToUserThenLogin();
                $transaction->commit();
                Yii::info('berhasil bro');
            } catch(Exception $e){
                Yii::warning('Gagal bro '.$mail. $e->getMessage());
                $transaction->rollBack();
            }
        }
    }


}
