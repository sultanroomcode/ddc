<?php
namespace sp3d\modules\pendamping\controllers;

use Yii;
use yii\web\Controller;
use sp3d\models\User;
use sp3d\models\transaksi\Sp3dPendampingDesa;
use sp3d\models\transaksi\Sp3dPendampingDesaLogin;
use sp3d\models\transaksi\Sp3dPendampingPilihDesa;
/**
 * Default controller for the `pendamping` module
 */
class DataController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIsiProfile()
    {
        $model = new Sp3dPendampingDesa();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('form-profile', [
                'model' => $model,
            ]);
        }        
    }

    public function actionUpdateProfile()
    {
        $model = Sp3dPendampingDesa::findOne(['user' => Yii::$app->user->identity->id]);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('form-profile', [
                'model' => $model,
            ]);
        }
    }

    public function actionView()
    {
    	$model = Sp3dPendampingDesaLogin::findOne(['email' => Yii::$app->user->identity->email]);            
        return $this->renderAjax('view', [
            'model' => $model,
        ]);
    }


    public function actionTambahDesa()
    {
        $model = new Sp3dPendampingPilihDesa();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save(false)) echo 1; else echo 0;
        } else {
            $model2 = Sp3dPendampingDesa::findOne(['user' => Yii::$app->user->identity->id]);
            return $this->renderAjax('form-tambah-desa', [
                'model' => $model,
                'model2' => $model2,
            ]);
        }  
    }

    public function actionDesaTerhubung()
    {
        $model = Sp3dPendampingPilihDesa::findAll(['user' => Yii::$app->user->identity->id]);            
        return $this->renderAjax('view-desa-terhubung', [
            'model' => $model,
        ]);
    }

    public function actionDataOption($type='kabupaten', $kode=null)
    {
        switch ($type) {
            // case 'kabupaten':
            //     $model = User::find()->where(['type' => $tahun]);
            // break;
            case 'kecamatan':
                $model = User::find()->where(['type' => 'kecamatan']);
            break;
            case 'desa':
                $model = User::find()->where(['type' => 'desa']);
            break;
        }

        $model->andWhere('id LIKE :query')->addParams([':query'=> $kode.'%']);

        return $this->renderPartial('option', [
            'model' => $model,
            'type' => $type,
            'kode' => $kode,
        ]);
    }
}
