<?php

namespace sp3d\modules\pendamping;

/**
 * pendamping module definition class
 */
class Pendamping extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'sp3d\modules\pendamping\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
