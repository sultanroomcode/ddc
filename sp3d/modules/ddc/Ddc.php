<?php
namespace sp3d\modules\ddc;

/**
 * ddc module definition class
 */
class Ddc extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\ddc\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
