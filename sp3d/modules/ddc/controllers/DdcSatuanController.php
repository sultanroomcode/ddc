<?php

namespace sp3d\modules\ddc\controllers;

use Yii;
use sp3d\modules\ddc\models\DdcSatuan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DdcSatuanController implements the CRUD actions for DdcSatuan model.
 */
class DdcSatuanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DdcSatuan models.
     * @return mixed
     */
    public function actionIndex($modul)
    {
        return $this->renderAjax('index', [
            'modul' => $modul,
        ]);
    }

    /**
     * Displays a single DdcSatuan model.
     * @param string $modul
     * @param integer $no_satuan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionViewBox($modul)
    {
        $model = DdcSatuan::find()->where(['modul' => $modul]);
        return $this->renderAjax('view-box', [
            'model' => $model,
            'modul' => $modul
        ]);
    }

    public function actionView($modul, $no_satuan)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($modul, $no_satuan),
        ]);
    }

    /**
     * Creates a new DdcSatuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($modul)
    {
        $model = new DdcSatuan();
        $model->modul = $modul;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DdcSatuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $modul
     * @param integer $no_satuan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($modul, $no_satuan)
    {
        $model = $this->findModel($modul, $no_satuan);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DdcSatuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $modul
     * @param integer $no_satuan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($modul, $no_satuan)
    {
        $model = $this->findModel($modul, $no_satuan)->delete();
        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the DdcSatuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $modul
     * @param integer $no_satuan
     * @return DdcSatuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($modul, $no_satuan)
    {
        if (($model = DdcSatuan::findOne(['modul' => $modul, 'no_satuan' => $no_satuan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
