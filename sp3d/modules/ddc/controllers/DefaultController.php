<?php

namespace sp3d\modules\ddc\controllers;

use yii\web\Controller;

/**
 * Default controller for the `ddc` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }
}
