<?php

namespace sp3d\modules\ddc\controllers;

use Yii;
use sp3d\modules\ddc\models\DdcTkd;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DdcTkdController implements the CRUD actions for DdcTkd model.
 */
class DdcTkdController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DdcTkd models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDataTanah()
    {
        return $this->renderAjax('data-tanah');
    }

    /**
     * Displays a single DdcTkd model.
     * @param string $kd_desa
     * @param integer $no_tkd
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionViewBox($kd_desa)
    {
        $model = DdcTkd::find()->where(['kd_desa' => $kd_desa]); 
        return $this->renderAjax('view-box', [
            'model' => $model,
            'kd_desa' => $kd_desa
        ]);
    }

    public function actionView($kd_desa, $no_tkd)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $no_tkd),
        ]);
    }

    /**
     * Creates a new DdcTkd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DdcTkd();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DdcTkd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa 
     * @param integer $no_tkd
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $no_tkd)
    {
        $model = $this->findModel($kd_desa, $no_tkd);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DdcTkd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $no_tkd
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $no_tkd)
    {
        $model = $this->findModel($kd_desa, $no_tkd)->delete();
        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the DdcTkd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $no_tkd
     * @return DdcTkd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $no_tkd)
    {
        if (($model = DdcTkd::findOne(['kd_desa' => $kd_desa, 'no_tkd' => $no_tkd])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
