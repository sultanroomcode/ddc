<?php
namespace sp3d\modules\ddc\controllers;

use yii\web\Controller;

/**
 * Default controller for the `ddc` module
 */
class KabKecDesFormController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionForm($layout='v')
    {
        return $this->renderAjax('form', [
        	'layout' => $layout
        ]);
    }
}
