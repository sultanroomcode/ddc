<?php
namespace sp3d\modules\ddc\controllers;

use Yii;
use sp3d\modules\ddc\models\DdcKomoditasPodes;
use sp3d\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class RekapController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionTables()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('tables', [
            'data' => $data
        ]);
    }

    public function actionData($page=1, $perpage=10)
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $page = (int) $data['start'];
        $perpage = (int) $data['length'];
        //base
        $conn = Yii::$app->getDb();

        $type = 'kabupaten';
        $d_search = '';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $d_search = $data['desa-input'];
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $d_search = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $d_search = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $liking = "";
        // $liking = "AND dc.tahun = '".$data['tahun']."' ";
        switch ($type) {
            case 'desa':
            case 'kecamatan':
            case 'kabupaten':
                $liking .= "AND u.id LIKE '".$d_search."%'";
            break;
        }

        $orderby = '';

        switch ($data['order'][0]['column']) {
            case "0":
                $orderby = "ORDER BY u.description ".strtoupper($data['order'][0]['dir']);
            break;
            case "1":          
                $orderby = "ORDER BY dc.tahun ".strtoupper($data['order'][0]['dir']);
            break;
            case "2":
                $orderby = "ORDER BY dc.dana_pendapatan ".strtoupper($data['order'][0]['dir']);
            break;
            case "3":
                $orderby = "ORDER BY dc.dana_rab ".strtoupper($data['order'][0]['dir']);
            break;
        }

        $commandcount = $conn->createCommand("SELECT 
        COUNT(u.description) AS counted
        FROM user u LEFT JOIN desa_count dc ON dc.kd_desa = u.id WHERE u.type = 'desa' 
        ".$liking);

        $command = $conn->createCommand("SELECT 
        u.description, 
        IFNULL(dc.tahun,\"Belum Isi\") AS tahun,
        IFNULL(dc.dana_pendapatan,\"Belum Isi\") AS pendapatan,
        IFNULL(dc.dana_rab,\"Belum Isi\") AS pengeluaran,
        (SELECT COUNT(*) AS c FROM bumdes WHERE kd_desa = u.id COLLATE utf8_general_ci) AS bumdes_c,
        (SELECT COUNT(*) AS c FROM kpm WHERE kd_desa = u.id COLLATE utf8_general_ci) AS kpm_c,
        (SELECT COUNT(*) AS c FROM lembaga_adat WHERE kd_desa = u.id COLLATE utf8_general_ci) AS lembaga_adat_c,
        (SELECT COUNT(*) AS c FROM lpmd WHERE kd_desa = u.id COLLATE utf8_general_ci) AS lpmd_c,
        (SELECT COUNT(*) AS c FROM pkk WHERE kd_desa = u.id COLLATE utf8_general_ci) AS pkk_c,
        (SELECT COUNT(*) AS c FROM posyandu WHERE kd_desa = u.id COLLATE utf8_general_ci) AS posyandu_c,
        (SELECT COUNT(*) AS c FROM pasardesa WHERE kd_desa = u.id COLLATE utf8_general_ci) AS pasardesa_c,
        (SELECT COUNT(*) AS c FROM kartar WHERE kd_desa = u.id COLLATE utf8_general_ci) AS kartar_c 

        FROM bumdes bd 
        LEFT JOIN user u ON bd.kd_desa = u.id COLLATE utf8_general_ci
        LEFT JOIN desa_count dc ON dc.kd_desa = u.id 

        WHERE u.type = 'desa' 
        ".$liking." ".$orderby." LIMIT :offset, :limiting", [':offset' => (int) $page, ':limiting' => (int) $perpage]);

        $recorded['draw'] = (int) $data['draw'];
        $recordedcount = $commandcount->queryAll();
        $recorded['recordsTotal'] = $recorded['recordsFiltered'] = $recordedcount[0]['counted'];
        $recorded['data'] = $this->array_values_recursive($command->queryAll());
        
        return $this->asJson($recorded);
    }

    public function array_values_recursive($array) {
        $array = array_values( $array );
        for ( $i = 0, $n = count( $array ); $i < $n; $i++ ) {
            $element = $array[$i];
            if ( is_array( $element ) ) {
                $array[$i] = $this->array_values_recursive( $element );
            }
        }
        return $array;
    }

    public function actionBumdes()
    {
        return $this->renderAjax('bumdes');
    }

    public function actionBumdesTable()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('bumdes-table', [
            'data' => $data
        ]);
    }

    public function actionBumdesData($page=1, $perpage=10)
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $page = (int) $data['start'];
        $perpage = (int) $data['length'];
        //base
        $conn = Yii::$app->getDb();

        $type = 'kabupaten';
        $d_search = '';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $d_search = $data['desa-input'];
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $d_search = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $d_search = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $orderby = '';

        switch ($data['order'][0]['column']) {
            case "0":
                $orderby = "ORDER BY kb.description ".strtoupper($data['order'][0]['dir']);
            break;
            case "1":          
                $orderby = "ORDER BY kc.description ".strtoupper($data['order'][0]['dir']);
            break;
            case "2":
                $orderby = "ORDER BY ds.description ".strtoupper($data['order'][0]['dir']);
            break;
            case "3":
                $orderby = "ORDER BY bd.nama_bumdes ".strtoupper($data['order'][0]['dir']);
            break;
        }

        if($data['jenis'] == 1){
            //bumdes yang sudah terisi
            $liking = $liking2= "";
            // $liking = "AND dc.tahun = '".$data['tahun']."' ";
            switch ($type) {
                case 'desa':
                    $liking .= "AND bd.kd_desa = '".$d_search."'";
                    $liking2 .= "WHERE bd.kd_desa = '".$d_search."'";
                    break;
                case 'kecamatan':
                    $liking .= "AND bd.kd_kecamatan = '".$d_search."'";
                    $liking2 .= "WHERE bd.kd_kecamatan = '".$d_search."'";
                    break;
                case 'kabupaten':
                    $liking .= "AND bd.kd_kabupaten = '".$d_search."'";
                    $liking2 .= "WHERE bd.kd_kabupaten = '".$d_search."'";
                break;
            }

            $commandcount = $conn->createCommand("SELECT 
            COUNT(bd.nama_bumdes) AS counted
            FROM bumdes bd LEFT JOIN user u ON bd.kd_desa = u.id COLLATE utf8_general_ci WHERE u.type = 'desa' 
            ".$liking);

            $command = $conn->createCommand("SELECT 
            kb.description AS kab_desc,
            kc.description AS kec_desc,
            ds.description AS dsa_desc,
            bd.nama_bumdes
            FROM bumdes bd 
            LEFT JOIN user ds ON bd.kd_desa = ds.id COLLATE utf8_general_ci
            LEFT JOIN user kc ON bd.kd_kecamatan = kc.id COLLATE utf8_general_ci
            LEFT JOIN user kb ON bd.kd_kabupaten = kb.id COLLATE utf8_general_ci
            ".$liking2." ".$orderby." LIMIT :offset, :limiting", [':offset' => (int) $page, ':limiting' => (int) $perpage]);
        } else {
            //bumdes yang belum berisi
            $liking = $liking2= "";
            switch ($type) {
                case 'desa':
                    $liking .= "AND u.id = '".$d_search."'";
                    break;
                case 'kecamatan':
                case 'kabupaten':
                    $liking .= "AND u.id LIKE '".$d_search."%'";
                break;
            }

            $commandcount = $conn->createCommand("SELECT 
            COUNT(u.id) AS counted
            FROM user u LEFT JOIN bumdes bd ON bd.kd_desa = u.id COLLATE utf8_general_ci WHERE bd.nama_bumdes IS NULL AND u.type = 'desa'
            ".$liking);

            $command = $conn->createCommand("SELECT 
            kb.description AS kab_desc,
            kc.description AS kec_desc,
            u.description AS dsa_desc,
            '-' AS nama_bumdes
            FROM user u
            LEFT JOIN bumdes bd ON bd.kd_desa = u.id COLLATE utf8_general_ci
            LEFT JOIN user kc ON SUBSTR(u.id, 1,7) = kc.id
            LEFT JOIN user kb ON SUBSTR(u.id, 1,4) = kb.id
            WHERE bd.nama_bumdes IS NULL AND u.type = 'desa'
            ".$liking." ".$orderby." LIMIT :offset, :limiting", [':offset' => (int) $page, ':limiting' => (int) $perpage]);
        }

        $recorded['draw'] = (int) $data['draw'];
        $recordedcount = $commandcount->queryAll();
        $recorded['recordsTotal'] = $recorded['recordsFiltered'] = $recordedcount[0]['counted'];
        $recorded['data'] = $this->array_values_recursive($command->queryAll());
        // $recorded['data'] =$command->queryAll();
        
        return $this->asJson($recorded);
    }

    public function actionBumdesViewData($page=1, $perpage=10)
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $page = (int) $data['start'];
        $perpage = (int) $data['length'];
        //base
        $conn = Yii::$app->getDb();

        $type = 'kabupaten';
        $d_search = '';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $d_search = $data['desa-input'];
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $d_search = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $d_search = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $orderby = '';

        switch ($data['order'][0]['column']) {
            case "0":
                $orderby = "ORDER BY kb_nama ".strtoupper($data['order'][0]['dir']);
            break;
            case "1":          
                $orderby = "ORDER BY kc_nama ".strtoupper($data['order'][0]['dir']);
            break;
            case "2":
                $orderby = "ORDER BY ds_nama ".strtoupper($data['order'][0]['dir']);
            break;
            case "3":
                $orderby = "ORDER BY nama_bumdes ".strtoupper($data['order'][0]['dir']);
            break;
        }

        if($data['jenis'] == 1){
            $status_bumdes = 'yes';
        } else {
            $status_bumdes = 'no';
        }
            
        //bumdes yang sudah terisi
        $liking = $liking2= "WHERE status_bumdes = '$status_bumdes'";
        // $liking = "AND dc.tahun = '".$data['tahun']."' ";
        switch ($type) {
            case 'desa':
                $liking .= "AND ds_id = '".$d_search."'";
                $liking2 .= "AND ds_id = '".$d_search."'";
                break;
            case 'kecamatan':
                $liking .= "AND kc_id = '".$d_search."'";
                $liking2 .= "AND kc_id = '".$d_search."'";
                break;
            case 'kabupaten':
                $liking .= "AND kb_id = '".$d_search."'";
                $liking2 .= "AND kb_id = '".$d_search."'";
            break;
        }

        $commandcount = $conn->createCommand("SELECT 
        COUNT(nama_bumdes) AS counted
        FROM vbumdes_all ".$liking);

        $command = $conn->createCommand("SELECT 
        kb_nama AS kab_desc,
        kc_nama AS kec_desc,
        ds_nama AS dsa_desc,
        nama_bumdes
        FROM vbumdes_all
        ".$liking2." ".$orderby." LIMIT :offset, :limiting", [':offset' => (int) $page, ':limiting' => (int) $perpage]);

        $recorded['draw'] = (int) $data['draw'];
        $recordedcount = $commandcount->queryAll();
        $recorded['recordsTotal'] = $recorded['recordsFiltered'] = $recordedcount[0]['counted'];
        $recorded['data'] = $this->array_values_recursive($command->queryAll());
        // $recorded['data'] =$command->queryAll();
        
        return $this->asJson($recorded);
    }

    /* kpm */

    public function actionKpm()
    {
        return $this->renderAjax('kpm');
    }

    public function actionKpmTable()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('kpm-table', [
            'data' => $data
        ]);
    }

    public function actionKpmData($page=1, $perpage=10)
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $page = (int) $data['start'];
        $perpage = (int) $data['length'];
        //base
        $conn = Yii::$app->getDb();

        $type = 'kabupaten';
        $d_search = '';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $d_search = $data['desa-input'];
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $d_search = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $d_search = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $type = 'provinsi';
        }

        $orderby = '';

        switch ($data['order'][0]['column']) {
            case "0":
                $orderby = "ORDER BY kb.description ".strtoupper($data['order'][0]['dir']);
            break;
            case "1":          
                $orderby = "ORDER BY kc.description ".strtoupper($data['order'][0]['dir']);
            break;
            case "2":
                $orderby = "ORDER BY ds.description ".strtoupper($data['order'][0]['dir']);
            break;
            case "3":
                $orderby = "ORDER BY k.nama ".strtoupper($data['order'][0]['dir']);
            break;
        }

        if($data['jenis'] == 1){
            //kpm yang sudah terisi
            $liking = $liking2= "";
            // $liking = "AND dc.tahun = '".$data['tahun']."' ";
            switch ($type) {
                case 'desa':
                    $liking .= "AND k.kd_desa = '".$d_search."'";
                    $liking2 .= "WHERE k.kd_desa = '".$d_search."'";
                    break;
                case 'kecamatan':
                    $liking .= "AND k.kd_kecamatan = '".$d_search."'";
                    $liking2 .= "WHERE k.kd_kecamatan = '".$d_search."'";
                    break;
                case 'kabupaten':
                    $liking .= "AND k.kd_kabupaten = '".$d_search."'";
                    $liking2 .= "WHERE k.kd_kabupaten = '".$d_search."'";
                break;
            }

            $commandcount = $conn->createCommand("SELECT COUNT(c.counted) AS calc FROM (SELECT COUNT(k.nik) AS counted FROM kpm k LEFT JOIN user u ON k.kd_desa = u.id COLLATE utf8_general_ci WHERE u.type = 'desa' ".$liking." GROUP BY k.kd_desa) AS c ");//dijadikan hitungan desa

            $command = $conn->createCommand("SELECT 
            kb.description AS kab_desc,
            kc.description AS kec_desc,
            ds.description AS dsa_desc,
            COUNT(k.nik) AS c_kpm
            FROM kpm k 
            LEFT JOIN user ds ON k.kd_desa = ds.id COLLATE utf8_general_ci
            LEFT JOIN user kc ON k.kd_kecamatan = kc.id COLLATE utf8_general_ci
            LEFT JOIN user kb ON k.kd_kabupaten = kb.id COLLATE utf8_general_ci
            ".$liking2." GROUP BY k.kd_desa ".$orderby." LIMIT :offset, :limiting", [':offset' => (int) $page, ':limiting' => (int) $perpage]);
        } else {
            //bumdes yang belum berisi
            $liking = $liking2= "";
            switch ($type) {
                case 'desa':
                    $liking .= "AND u.id = '".$d_search."'";
                    break;
                case 'kecamatan':
                case 'kabupaten':
                    $liking .= "AND u.id LIKE '".$d_search."%'";
                break;
            }

            $commandcount = $conn->createCommand("SELECT 
            COUNT(u.id) AS calc
            FROM user u LEFT JOIN kpm k ON k.kd_desa = u.id COLLATE utf8_general_ci WHERE k.nama IS NULL AND u.type = 'desa' ".$liking);

            $command = $conn->createCommand("SELECT 
            kb.description AS kab_desc,
            kc.description AS kec_desc,
            u.description AS dsa_desc,
            '0' AS num_
            FROM user u
            LEFT JOIN kpm k ON k.kd_desa = u.id COLLATE utf8_general_ci
            LEFT JOIN user kc ON SUBSTR(u.id, 1,7) = kc.id
            LEFT JOIN user kb ON SUBSTR(u.id, 1,4) = kb.id
            WHERE k.nama IS NULL AND u.type = 'desa'
            ".$liking." ".$orderby." LIMIT :offset, :limiting", [':offset' => (int) $page, ':limiting' => (int) $perpage]);
        }

        $recorded['draw'] = (int) $data['draw'];
        $recordedcount = $commandcount->queryAll();
        $recorded['recordsTotal'] = $recorded['recordsFiltered'] = $recordedcount[0]['calc'];
        $recorded['data'] = $this->array_values_recursive($command->queryAll());
        // $recorded['data'] =$command->queryAll();
        
        return $this->asJson($recorded);
    }
}
