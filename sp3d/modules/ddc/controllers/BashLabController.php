<?php
namespace sp3d\modules\ddc\controllers;

use yii\web\Controller;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use sp3d\models\filterxls\ReadDdcExcelOnly;
use Yii;
/**
 * Default controller for the `ddc` module
 */
class BashLabController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $old_path = getcwd();
        var_dump($old_path);
		chdir('./userfile/3516080012/db/');//harus ditest multi user
		hell_exec('mdb-export 1524651756-3516080012.mde Ta_RAB > 1524651756-3516080012-rab.csv');

		$filter = new ReadDdcExcelOnly();
		$reader = new Csv();
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(true);
        $reader->setDelimiter(',');
        $a = range('C', 'Z');
        $b = ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO'];
        $filter->setRange(array_merge($a,$b));
        $spreadsheet = $reader->load("1524651756-3516080012-rab.csv");//pamekasan
        //trenggalek-2018.xlsx done
        $worksheet = $spreadsheet->getActiveSheet();
		chdir($old_path);

        return $this->render('index', ['worksheet' => $worksheet, 'filter' => $filter]);
    }
}
