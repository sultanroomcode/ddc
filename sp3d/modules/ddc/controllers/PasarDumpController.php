<?php
namespace sp3d\modules\ddc\controllers;

use sp3d\modules\ddc\models\DummyPasar;
use yii\web\Controller;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use sp3d\models\filterxls\ReadDdcExcelOnly;
use Yii;
/**
 * Default controller for the `ddc` module
 */
class PasarDumpController extends Controller
{
    private $kd_kabupaten;

    public function init()
    {
        $this->kd_kabupaten = '3525';
    }

    public function actionReadXls()
    {
        $reader = new Xlsx();
        $reader->setReadDataOnly(true);
        $reader->setLoadSheetsOnly(['gresik2018']);
        $filter = new ReadDdcExcelOnly();
        // $filter->setMinMax(7, 64);//kediri
        $filter->setMinMax(6, 111);//gresik
        $filter->setRange(range('B','R'));
        $reader->setReadFilter($filter);
        $spreadsheet = $reader->load("read-xls/pasar/2018-pasar-desa.xlsx");
        $worksheet = $spreadsheet->getActiveSheet();

        return $this->render('read-xls-pasar', ['worksheet' => $worksheet, 'filter' => $filter]);
    }

    /*  execution list  */
    public function actionExecutionList()
    {

    }

    public function actionPorting1()
    {
    	$kd_kabupaten = $this->kd_kabupaten;
        return $this->renderAjax('porting-1', [
        	'kd_kabupaten' => $kd_kabupaten
        ]); // fixing kecamatan
    }

    public function actionPorting2()
    {
        $kd_kabupaten = $this->kd_kabupaten;
        return $this->render('porting-2', [
            'kd_kabupaten' => $kd_kabupaten
        ]); // fixing desa
    }

    public function actionPorting3()
    {
        $kd_kabupaten = $this->kd_kabupaten;
        return $this->renderAjax('porting-3', [
            'kd_kabupaten' => $kd_kabupaten
        ]); // porting data data
    }

    public function actionFormUpdate($id)//bumdes
    {
        $data = Yii::$app->request->get();
        $data1 = Yii::$app->request->post();
        $model = Userfree::findOne($id);
        if (isset($data1['_csrf']) && isset($data1['Userfree'])) {
            // $data1 = Yii::$app->request->post();
            // var_dump($data1);
            $conn = Yii::$app->getDb();
            $command = $conn->createCommand("UPDATE user SET description = '".$data1['Userfree']['description']."' WHERE id = '".$id."'");

            $pr = $command->execute();
            if($pr == 1){ echo 1; } else echo 0;
        } else {
            return $this->render('form-update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    public function actionFormUpdateDummy()//bumdes
    {
        $data = Yii::$app->request->get();
        $data1 = Yii::$app->request->post();
        // var_dump($data1);
        $model = DummyPasar::findOne(['kd_kecamatan' => $data['kd_kecamatan'],'jml_pedagang' => $data['jml_pedagang'], 'jml_jenis_barang' => $data['jml_jenis_barang'], 'nama_desa' => $data['nama_desa']]);
        if (isset($data1['_csrf']) && isset($data1['DummyPasar'])) {
            // $data1 = Yii::$app->request->post();
            // var_dump($data1);
            $conn = Yii::$app->getDb();
            $sql = "UPDATE dummy_pasar SET nama_desa = '".$data1['DummyPasar']['nama_desa']."' WHERE nama_desa = '".$data1['DummyPasar']['nama_desa_lama']."' AND kd_kecamatan = '".$data['kd_kecamatan']."' AND jml_pedagang = ".$data['jml_pedagang']." AND jml_jenis_barang = ".$data['jml_jenis_barang'];
            $command = $conn->createCommand($sql);
            // echo $sql;
            $pr = $command->execute();
            if($pr == 1){ echo 1; } else echo 0;
        } else {
            return $this->render('form-update-dummy', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }
}
