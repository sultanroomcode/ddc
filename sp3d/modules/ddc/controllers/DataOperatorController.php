<?php
namespace sp3d\modules\ddc\controllers;
use yii\web\Controller;
use sp3d\modules\ddc\models\SignupOperator;
use ddcop\models\Operator;
use ddcop\models\OperatorDesa;
use Yii;

use sp3d\models\User;
class DataOperatorController extends Controller
{
    public function actionResetOperator($email)
    {
        $model = Operator::findOne(['email' => $email]);
        $model->setPassword('default');
        $model->generateAuthKey();
        return $model->save();
    }

    public function actionListOperator($lembaga=null)
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('list-operator', [
            'data' => $data
        ]);
    }

    public function actionViewList($lembaga=null)
    {
        $data = Yii::$app->request->get();
        $model = OperatorDesa::find()->joinWith('operator')->where(['operator.tipe_operator' => $data['tipe'], 'kd_desa' => Yii::$app->user->identity->id]);
        return $this->renderAjax('view-operator', [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionTambahOperator($lembaga=null)
    {
        $data = Yii::$app->request->get();

        $model = new SignupOperator();
        if($model->load(Yii::$app->request->post())){
            if($model->signup()){
                echo 1;
            } else {
                echo 0;
            }
        } else {
            return $this->renderAjax('_form-operator', [
                'data' => $data,
                'model' => $model
            ]);
        }
    }

    public function actionUpdateOperator($lembaga=null)
    {
        $data = Yii::$app->request->get();

        return $this->renderAjax('laporan-baru/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionHapusOperator($lembaga=null)
    {
        $data = Yii::$app->request->get();

        return $this->renderAjax('laporan-baru/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }
}
