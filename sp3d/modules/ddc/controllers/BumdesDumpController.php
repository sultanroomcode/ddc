<?php
namespace sp3d\modules\ddc\controllers;

use yii\web\Controller;
use Yii;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use sp3d\models\filterxls\ReadDdcExcelOnly;
use sp3d\models\Userfree;
use sp3d\modules\ddc\models\DummyBumdes;
/**
 * Default controller for the `ddc` module
 */
class BumdesDumpController extends Controller
{
    private $kd_kabupaten;

    public function init()
    {
        $this->kd_kabupaten = '3528';
    }

    public function actionUpdateXls()
    {
        $reader = new Xlsx();
        $reader->setReadDataOnly(true);
        $reader->setLoadSheetsOnly(['JAWA TIMUR']);
        $filter = new ReadDdcExcelOnly();
        // $filter->setMinMax(7, 10);
        // $filter->setMinMax(454, 605);//trenggalek        
        // $filter->setMinMax(7, 336);//sumenep
        // $filter->setMinMax(7, 138);//situbondo
        // $filter->setMinMax(1083, 1254);//kediri
        $filter->setMinMax(1806, 2003);//lumajang
        // $filter->setMinMax(3436, 3757);//sidoarjo
        // $filter->setMinMax(7036, 7215);//sampang //error on xls
        // $filter->setMinMax(7036, 7215);//probolinggo //error on xls
        // $filter->setMinMax(7, 287);//ponorogo //error on xls
        
        $a = range('C', 'Z');
        $b = ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO'];

        $filter->setRange(array_merge($a,$b));
        $reader->setReadFilter($filter);
        $spreadsheet = $reader->load("read-xls/bumdes/3502-ponorogo-2018.xlsx");
        //trenggalek-2018.xlsx done
        $worksheet = $spreadsheet->getActiveSheet();

        return $this->render('update-xls', ['worksheet' => $worksheet, 'filter' => $filter]);
    }

    public function actionReadCsv()
    {
        $filter = new ReadDdcExcelOnly();

        $reader = new Csv();
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(true);
        $reader->setDelimiter(',');
        $a = range('C', 'Z');
        $b = ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO'];
        $filter->setRange(array_merge($a,$b));
        // $spreadsheet = $reader->load("read-xls/bumdes/3519-madiun-2018-nonfix.csv");//madiun ok
        // $spreadsheet = $reader->load("read-xls/bumdes/3521-ngawi-2018-nonfix.csv");//ngawi ok
        // $spreadsheet = $reader->load("read-xls/bumdes/3513-probolinggo-2018-2.csv");//probolinggo ok
        // $spreadsheet = $reader->load("read-xls/bumdes/3526-bangkalan-2018-nonfix.csv");//bangkalan ok
        // $spreadsheet = $reader->load("read-xls/bumdes/3527-sampang-2018-nonwellformat.csv");//sampang ok
        $spreadsheet = $reader->load("read-xls/bumdes/3528-pamekasan-2018.csv");//pamekasan
        //trenggalek-2018.xlsx done
        $worksheet = $spreadsheet->getActiveSheet();
        $reader->setReadFilter($filter);
        return $this->render('read-csv', ['worksheet' => $worksheet]);
    }

    public function actionReadXls()
    {
        $reader = new Xlsx();
        $reader->setReadDataOnly(true);
        $reader->setLoadSheetsOnly(['JAWA TIMUR']);
        $filter = new ReadDdcExcelOnly();
        // $filter->setMinMax(7, 10);
        // $filter->setMinMax(454, 605);//trenggalek        
        // $filter->setMinMax(1426, 1805);//malang
        // $filter->setMinMax(1806, 2003);//lumajang     
        // $filter->setMinMax(2004, 2229);//jember     
        // $filter->setMinMax(7, 205);//banyuwangi     
        // $filter->setMinMax(3095, 3435);//pasuruan     
        // $filter->setMinMax(3436, 3757);//pasuruan     
        $filter->setMinMax(2, 203);//probolinggo
        // $filter->setMinMax(7, 138);//situbondo     
        // $filter->setMinMax(7, 336);//sumenep
        // $filter->setMinMax(7, 138);//situbondo
        // $filter->setMinMax(1083, 1425);//kediri
        // $filter->setMinMax(3436, 3757);//sidoarjo
        // $filter->setMinMax(7036, 7215);//sampang //error on xls
        // $filter->setMinMax(7036, 7215);//probolinggo //error on xls
        // $filter->setMinMax(7, 287);//ponorogo //error on xls
        // $filter->setMinMax(7, 305);//mojokerto     
        // $filter->setMinMax(7, 308);//jombang
        // $filter->setMinMax(4359, 4622);//jombang
        // $filter->setMinMax(2, 199);//madiun not fix file.. was xls before not xlsx
        // $filter->setMinMax(8, 214);//magetan
        // $filter->setMinMax(5241, 5659);//bojonegoro // kecamatan gayam tidak terdaftar di user identifikasi pertama adalah ngasem 3522190 menurut data desa begadon dan mojodelik kecamatan ngasem diubah menjadi gayam pada table user
        //desa katur, sudu, CENGUNGKLUNG tidak ada
        //tuband dan lamongan pakai xls
        // $filter->setMinMax(7, 336);//gresik, Sedapur Klagen, Guranganyar, Mranti tidak ada
        // $filter->setMinMax(7036, 7215);//sampang, non well format
        // $filter->setMinMax(8, 184);//pamekasan, non well format
        // $filter->setMinMax(7, 336);//sumenep, desa SADULANG, SAUR SAEBUS tidak ada
        // $filter->setMinMax(7724, 7742);//sumenep, desa SADULANG, SAUR SAEBUS tidak ada
        
        $a = range('C', 'Z');
        $b = ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO'];

        $filter->setRange(array_merge($a,$b));
        $reader->setReadFilter($filter);
        $spreadsheet = $reader->load("read-xls/bumdes/3513-probolinggo-2018-2.xlsx");

        //trenggalek-2018.xlsx done
        $worksheet = $spreadsheet->getActiveSheet();

        return $this->render('read-xls', ['worksheet' => $worksheet, 'filter' => $filter]);
    }

    /*  execution list  */
    public function actionExecutionList()
    {

    }
    
    public function actionPorting0($force=false)
    {
    	$kd_kabupaten = $this->kd_kabupaten;
        return $this->renderAjax('porting-0', [
        	'kd_kabupaten' => $kd_kabupaten,
            'force' => $force
        ]); // fixing data kecamatan
    }

    public function actionPorting1($force=0)
    {
    	$kd_kabupaten = $this->kd_kabupaten;
        return $this->render('porting-1', [
        	'kd_kabupaten' => $kd_kabupaten,
            'force' => (($force == false)?false:true)
        ]); // fixing data desa
    }

    public function actionPorting2()
    {
    	$kd_kabupaten = $this->kd_kabupaten;
        return $this->renderAjax('porting-2', [
        	'kd_kabupaten' => $kd_kabupaten
        ]);
    }

    public function actionPorting3()//bumdes
    {
    	$kd_kabupaten = $this->kd_kabupaten;
        return $this->renderAjax('porting-3', [
        	'kd_kabupaten' => $kd_kabupaten
        ]);
    }

    public function actionPorting4()//bumdes
    {
        $kd_kabupaten = $this->kd_kabupaten;
        return $this->renderAjax('porting-4', [
            'kd_kabupaten' => $kd_kabupaten
        ]);
    }

    public function actionPorting5()//bumdes
    {
        $kd_kabupaten = $this->kd_kabupaten;
        return $this->renderAjax('porting-5', [
            'kd_kabupaten' => $kd_kabupaten
        ]);
    }

    public function actionPorting6()//bumdes
    {
        return $this->renderAjax('porting-6');
    }

    public function actionFormUpdate($id)//bumdes
    {
        $data = Yii::$app->request->get();
        $data1 = Yii::$app->request->post();
        $model = Userfree::findOne($id);
        if (isset($data1['_csrf']) && isset($data1['Userfree'])) {
            // $data1 = Yii::$app->request->post();
            // var_dump($data1);
            $conn = Yii::$app->getDb();
            $command = $conn->createCommand("UPDATE user SET description = '".$data1['Userfree']['description']."' WHERE id = '".$id."'");

            $pr = $command->execute();
            if($pr == 1){ echo 1; } else echo 0;
        } else {
            return $this->render('form-update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    public function actionFormUpdateDummy($id)//bumdes
    {
        $data = Yii::$app->request->get();
        $data1 = Yii::$app->request->post();
        // var_dump($data1);
        $model = DummyBumdes::findOne(['kd_desa' => $id]);
        if (isset($data1['_csrf']) && isset($data1['DummyBumdes'])) {
            // $data1 = Yii::$app->request->post();
            // var_dump($data1);
            $conn = Yii::$app->getDb();
            $sql = "UPDATE dummy_bumdes2 SET nama_desa = '".$data1['DummyBumdes']['nama_desa']."' WHERE kd_desa = '".$id."'";
            $command = $conn->createCommand($sql);
            // echo $sql;
            $pr = $command->execute();
            if($pr == 1){ echo 1; } else echo 0;
        } else {
            return $this->render('form-update-dummy', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }
}
