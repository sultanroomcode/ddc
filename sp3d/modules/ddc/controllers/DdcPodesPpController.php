<?php
namespace sp3d\modules\ddc\controllers;

use Yii;
use sp3d\modules\ddc\models\DdcPodesPp;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DdcPodesPpController implements the CRUD actions for DdcPodesPp model.
 */
class DdcPodesPpController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DdcPodesPp models.
     * @return mixed
     */
    public function actionIndex($modul)
    {
        return $this->renderAjax('index', [
            'modul' => $modul
        ]);
    }

    public function actionViewBox($modul)
    {
        $model = DdcPodesPp::find()->where(['kd_desa' => Yii::$app->user->identity->id, 'jenis' => $modul]);

        return $this->renderAjax('view-box', [
            'model' => $model,
            'modul' => $modul
        ]);
    }

    /**
     * Displays a single DdcPodesPp model.
     * @param string $kd_desa
     * @param integer $id_date
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_date)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_date),
        ]);
    }

    /**
     * Creates a new DdcPodesPp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($modul)
    {
        $model = new DdcPodesPp();
        $model->jenis = $modul;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DdcPodesPp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_date
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_date)
    {
        $model = $this->findModel($kd_desa, $id_date);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DdcPodesPp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_date
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_date)
    {
        $model = $this->findModel($kd_desa, $id_date)->delete();
        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the DdcPodesPp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_date
     * @return DdcPodesPp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_date)
    {
        if (($model = DdcPodesPp::findOne(['kd_desa' => $kd_desa, 'id_date' => $id_date])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
