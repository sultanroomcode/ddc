<?php
namespace sp3d\modules\ddc\controllers;

use yii\web\Controller;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use sp3d\models\filterxls\ReadDdcExcelOnly;
use sp3d\models\components\SSE;
use Yii;
/**
 * Default controller for the `ddc` module
 */
class ServerEventLabController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionResponder()
    {
        return $this->renderAjax('responder');
    }

    public function actionIndex()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $data = array(
            'firstName'=>'Test',
            'lastName'=>'Last'
        );
        $str = json_encode($data);
        echo "data: {$str}\n\n";
        flush();
    }

    public function actionMessage()
    {
        $sse = new SSE();
        $counter = rand(1, 10);
        $t = time();

        //$sse->retry(3000);
        while ((time() - $t) < 15) {
            $curDate = date(DATE_ISO8601);
            $sse->event('ping',['time' => $curDate]);

            // Send a simple message at random intervals.

            $counter--;
            if (!$counter) {
                $sse->message("This is a message at time $curDate");
                $counter = rand(1, 10);
            }

            $sse->flush();
            sleep(5);
            // Every second, sent a "ping" event.
        }
        exit();
    }
}
