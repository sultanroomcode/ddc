<?php

namespace sp3d\modules\ddc\controllers;

use Yii;
use sp3d\modules\ddc\models\DdcPodes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DdcPodesController implements the CRUD actions for DdcPodes model.
 */
class DdcPodesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DdcPodes models.
     * @return mixed
     */
    public function actionIndex($modul)
    {
        return $this->renderAjax('index', [
            'modul' => $modul
        ]);
    }

    public function actionViewBox($modul)
    {
        $model = DdcPodes::find()->where(['kd_desa' => Yii::$app->user->identity->id, 'jenis' => $modul]);

        return $this->renderAjax('view-box', [
            'model' => $model,
            'modul' => $modul
        ]);
    }

    /**
     * Displays a single DdcPodes model.
     * @param string $kd_desa
     * @param integer $id_date
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_date)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_date),
        ]);
    }

    /**
     * Creates a new DdcPodes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($modul)
    {
        $model = new DdcPodes();
        $model->jenis = $modul;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DdcPodes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_date
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_date)
    {
        $model = $this->findModel($kd_desa, $id_date);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DdcPodes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_date
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_date)
    {
        $model = $this->findModel($kd_desa, $id_date)->delete();
        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the DdcPodes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_date
     * @return DdcPodes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_date)
    {
        if (($model = DdcPodes::findOne(['kd_desa' => $kd_desa, 'id_date' => $id_date])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
