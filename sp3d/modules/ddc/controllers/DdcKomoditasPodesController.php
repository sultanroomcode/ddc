<?php

namespace sp3d\modules\ddc\controllers;

use Yii;
use sp3d\modules\ddc\models\DdcKomoditasPodes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DdcKomoditasPodesController implements the CRUD actions for DdcKomoditasPodes model.
 */
class DdcKomoditasPodesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DdcKomoditasPodes models.
     * @return mixed
     */
    public function actionIndex($modul)
    {
        return $this->renderAjax('index', [
            'modul' => $modul
        ]);
    }

    public function actionViewBox($modul)
    {
        $model = DdcKomoditasPodes::find()->where(['jenis' => $modul]);

        return $this->renderAjax('view-box', [
            'model' => $model,
            'modul' => $modul
        ]);
    }

    /**
     * Displays a single DdcKomoditasPodes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DdcKomoditasPodes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($modul)
    {
        $model = new DdcKomoditasPodes();
        $model->jenis = $modul;
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DdcKomoditasPodes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DdcKomoditasPodes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the DdcKomoditasPodes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DdcKomoditasPodes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DdcKomoditasPodes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
