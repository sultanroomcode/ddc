<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcKomoditasPodes */

$this->title = 'Update Ddc Komoditas Podes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Komoditas Podes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ddc-komoditas-podes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
