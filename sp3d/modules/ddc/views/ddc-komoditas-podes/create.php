<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcKomoditasPodes */

$this->title = 'Create Ddc Komoditas Podes';
$this->params['breadcrumbs'][] = ['label' => 'Ddc Komoditas Podes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-komoditas-podes-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
