<?php 
use yii\helpers\Html;
$modul2 = ucwords(str_replace('-', ' ', $modul));
?>
<div class="ddc-podes-index">
    <h3><?= Html::encode($modul2) ?></h3>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#podes-zona', url:'/ddc/ddc-podes/create?modul=<?=$modul?>'});" class="btn btn-danger">Tambah Potensi</a>

    <a href="javascript:void(0)" onclick="getPDF()" class="btn btn-danger">Laporan</a>

    <table class="table table-striped">
        <tr>
            <th>Jenis</th>
            <th>Komoditas</th>
            <th>Luas Lahan Produksi</th>
            <th>Lahan Milik</th>
            <th>Hasil Produksi</th>
            <th>Aksi</th>
        </tr>
    <?php foreach ($model->all() as $v) {?>
        <tr>
            <td><?= $v->jenis ?></td>
            <td><?= $v->komoditasp->nama_komoditas ?></td>
            <td><?= $v->luas_lahan_produksi ?></td>
            <td><?= $v->lahan_milik ?></td>
            <td><?= $v->hasil_produksi. ' '.$v->satuanid->nama_satuan ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#podes-zona', url:'/ddc/ddc-podes/update?kd_desa=<?=$v->kd_desa?>&id_date=<?=$v->id_date?>'});" class="btn btn-danger">Update</a> 
                <a href="javascript:void(0)" onclick="deleteData('<?= $v->jenis?>', '/ddc/ddc-podes/delete?kd_desa=<?=$v->kd_desa?>&id_date=<?=$v->id_date?>', 'Hapus Data Potensi')" class="btn btn-danger">Hapus</a>
            </td>
        </tr>
    <?php } ?>
    </table>
</div>

<?php
$chapter = 'podes';
$scripts =<<<JS
    function getPDF(){
        window.open(base_url+'/sp3ddashboard/data-print/pdf?modul={$modul}&chapter={$chapter}', '_blank');
    }

    function deleteData(modul, url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#podes-zona', urlBack:'/ddc/ddc-podes/view-box?modul='+modul});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);