<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcPodes */

$this->title = 'Update Ddc Podes: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Podes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_date' => $model->id_date]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ddc-podes-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
