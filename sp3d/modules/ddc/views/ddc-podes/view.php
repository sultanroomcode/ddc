<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcPodes */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Podes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-podes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kd_desa' => $model->kd_desa, 'id_date' => $model->id_date], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_desa' => $model->kd_desa, 'id_date' => $model->id_date], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_date',
            'jenis',
            'komoditas',
            'luas_lahan_produksi',
            'lahan_milik',
            'hasil_produksi',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
