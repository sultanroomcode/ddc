<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-6',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_desa = Yii::$app->user->identity->id;
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_desa, 0, 4);
    $model->id_date = time();
}

$urlback = "goLoad({elm:'#podes-zona', url:'/ddc/ddc-podes/index?modul=".$model->jenis."'});";
?>

<div class="ddc-podes-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_date')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'jenis')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'komoditas')->dropdownList($model->arrKomoditas()) ?>

    <?= $form->field($model, 'luas_lahan_produksi')->widget(\yii\widgets\MaskedInput::className(), [
        'clientOptions' => [
            'alias' => 'decimal',
            'digits' => 2,
            'digitsOptional' => false,
            'radixPoint' => ',',
            'groupSeparator' => '.',
            'autoGroup' => true,
            'removeMaskOnSubmit' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'lahan_milik')->dropdownList($model->arrLahan()) ?>

    <?= $form->field($model, 'hasil_produksi')->widget(\yii\widgets\MaskedInput::className(), [
        'clientOptions' => [
            'alias' => 'decimal',
            'digits' => 2,
            'digitsOptional' => false,
            'radixPoint' => ',',
            'groupSeparator' => '.',
            'autoGroup' => true,
            'removeMaskOnSubmit' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'satuan')->dropdownList($model->arrSatuan()) ?>

    <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>
    
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);