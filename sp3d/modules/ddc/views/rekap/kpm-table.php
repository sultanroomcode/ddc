<?php 
if($data['jenis'] == 1)
{
    $t = 'Sudah Isi';
} else {
    $t = 'Belum Isi';
}
?>
<h2 align="center">Rekapitulasi Data KPM <?= $t ?></h2>

<table class="table compact" id="data-table-provinsi">
    <thead>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>(J)KPM</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>(J)KPM</th>
        </tr>
    </tfoot>
</table>

<?php
$url = "/ddc/rekap/kpm-data?jenis=".$data['jenis']."&kabupaten-input=".$data['kabupaten-input']."&kecamatan-input=".$data['kecamatan-input']."&desa-input=".$data['desa-input'];
$script =<<<JS
$('#data-table-provinsi').DataTable({
  "language": {
      "decimal": ",",
      "thousands": "."
  },
  "processing" : true,
  "serverSide" : true,
  "ajax" : base_url+'{$url}'
});
JS;

$this->registerJs($script);