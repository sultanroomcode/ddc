<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;
$id = Yii::$app->user->identity->id;
$tipeuser = Yii::$app->user->identity->type;
$arr = ['-' => ''];

if($tipeuser == 'provinsi'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
}

if($tipeuser == 'kabupaten'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
}

if($tipeuser == 'kecamatan'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'desa'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
}


$listArr = $arr + $listArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Rekap Data KPM</h2>
            <div class="p-10">
                <div class="row">
                    <div class="col-md-2">
                        <select id="jenis" class="form-control">
                            <option value="1">Sudah Isi</option>
                            <option value="0">Belum Isi</option>
                        </select>
                    </div>
                    <?php if($tipeuser == 'provinsi'){ ?>
                    <div class="col-md-3">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } 
                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten'){ //kecamatan ?>
                    <div class="col-md-3">
                        <select id="kecamatan" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?=  '<input type="hidden" id="kabupaten" value="'.$id.'">'; ?>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    }

                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten' || $tipeuser == 'kecamatan'){ //desa ?>
                    <div class="col-md-3">
                        <select id="desa" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    } ?>

                    <div class="col-md-1">
                        <button class="btn btn-block btn-danger" onclick="filteringAdd()">Filter</button>
                    </div>
                </div>
            	<hr>
            	<div id="rekap-zona"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<?php
$scripts =<<<JS
$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});


$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

function filteringAdd(){
    var jenis = $('#jenis').val();
    var desa = $('#desa').val();
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    var perpage = $("#perpaging").val();
    console.log('kab '+ kabupaten+' kec '+ kecamatan+' des ' + desa);
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    if(desa == '-' || desa == null){
        desa = '';
    }
    goLoad({elm:'#rekap-zona', url:'/ddc/rekap/kpm-table?jenis='+jenis+'&perpage='+perpage+'&kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan+'&desa-input='+desa});
}
JS;

$this->registerJs($scripts);