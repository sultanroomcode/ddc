<h2 align="center">Rekapitulasi Data Tahun <?= $data['tahun-anggaran'] ?></h2>

<table class="table compact" id="data-table-provinsi">
    <thead>
        <tr>            
            <th>Desa</th>
            <th>Tahun</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
            <th>(J)Bumdes</th>
            <th>(J)Posyandu</th>
            <th>(J)KPM</th>
            <th>(J)Lembaga Adat</th>
            <th>(J)LPMD</th>
            <th>(J)Pasar Desa</th>
            <th>(J)PKK</th>
            <th>(J)Kartar</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Desa</th>
            <th>Tahun</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
            <th>(J)Bumdes</th>
            <th>(J)Posyandu</th>
            <th>(J)KPM</th>
            <th>(J)Lembaga Adat</th>
            <th>(J)LPMD</th>
            <th>(J)Pasar Desa</th>
            <th>(J)PKK</th>
            <th>(J)Kartar</th>
        </tr>
    </tfoot>
</table>

<?php
$url = "/ddc/rekap/data?tahun=".$data['tahun-anggaran']."&kabupaten-input=".$data['kabupaten-input']."&kecamatan-input=".$data['kecamatan-input']."&desa-input=".$data['desa-input'];
$script =<<<JS
$('#data-table-provinsi').DataTable({
  "language": {
      "decimal": ",",
      "thousands": "."
  },
  "processing" : true,
  "serverSide" : true,
  "ajax" : base_url+'{$url}'
});
JS;

$this->registerJs($script);