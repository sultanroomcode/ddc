<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
if($layout == 'h'){//means vertical
	$vl = 4;
} else {
	$vl = 12;
}
?>

<div class="row">
	<div class="col-md-<?= $vl ?>">
    <div class="form-group">
		<label class="control-label col-sm-5" for="ddcform-tools">Kabupaten</label>
      <div class="col-sm-7">
        <select id="ddcform-kabupaten" class="form-control">
          <?php foreach($listArr as $k => $v): ?>
          <option value="<?= $k ?>"><?= $v ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>  
	</div>

	<div class="col-md-<?= $vl ?>">
    <div class="form-group">
    <label class="control-label col-sm-5" for="ddcform-tools">Kecamatan</label>
      <div class="col-sm-7">
        <select id="ddcform-kecamatan" class="form-control"></select>
      </div>
    </div>
  </div>

  <div class="col-md-<?= $vl ?>">
    <div class="form-group">
    <label class="control-label col-sm-5" for="ddcform-tools">Desa</label>
      <div class="col-sm-7">
      <select id="ddcform-desa" class="form-control"></select>
      </div>
    </div>
  </div>
</div>

<?php
$scripts =<<<JS
$('#ddcform-kabupaten').change(function(){
  var kode = $(this).val();
  $('#ddcform-kecamatan, #ddcform-desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#ddcform-kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});

$('#ddcform-kecamatan').change(function(){
  var kode = $(this).val();
  $('#ddcform-desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#ddcform-desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

$('#ddcform-desa').on('blur', function(){
    $('#bumdes-kd_desa').val($(this).val());
    $('#bumdes-kd_kecamatan').val($('#ddcform-kecamatan').val());
    $('#bumdes-kd_kabupaten').val($('#ddcform-kabupaten').val());
    console.log($('#bumdes-kd_desa').val()+' '+$('#bumdes-kd_kecamatan').val()+' '+$('#bumdes-kd_kabupaten').val());
});
JS;

$this->registerJs($scripts);