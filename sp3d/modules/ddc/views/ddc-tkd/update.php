<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcTkd */

$this->title = 'Update Ddc Tkd: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Tkds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'no_tkd' => $model->no_tkd]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ddc-tkd-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
