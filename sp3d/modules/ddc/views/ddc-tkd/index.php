<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ddc Tkds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-tkd-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ddc Tkd', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'no_tkd',
            'jenis_sertifikat',
            //'nomor',
            //'luas_lahan',
            //'lokasi',
            //'kd_desa_lokasi',
            //'kd_kecamatan_lokasi',
            //'kd_kabupaten_lokasi',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
