<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'DDC TKD';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tkd-tkd-view">
    <div class="tile">
        <h2 class="tile-title">TKD Desa</h2>
        <div class="p-10">
            <div id="tkd-zona"></div>
        </div>
    </div>
</div>

<?php
$kd_desa = Yii::$app->user->identity->id;
$script =<<<JS
goLoad({elm:'#tkd-zona', url:'/ddc/ddc-tkd/view-box?kd_desa={$kd_desa}'});
JS;

$this->registerJs($script);
?>