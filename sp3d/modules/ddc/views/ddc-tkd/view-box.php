<?php 
use yii\helpers\Html;
$modul2 = 'TKD';
?>
<div class="ddc-tkd-index">
    <h3><?= Html::encode($modul2) ?></h3>
    <a href="javascript:void(0)" onclick="goLoad({elm:'#tkd-zona', url:'/ddc/ddc-tkd/create?kd_desa=<?=$kd_desa?>'});" class="btn btn-danger">Tambah Tanah</a>
    <table class="table table-striped">
        <tr>
            <th>No. TKD</th>
            <th>Jenis Sertifikat</th>
            <th>Nomor Sertifikat</th>
            <th>Luas Lahan</th>
            <th>Hasil Produksi</th>
            <th>Aksi</th>
        </tr>
    <?php foreach ($model->all() as $v) {?>
        <tr>
            <td><?= $v->no_tkd ?></td>
            <td><?= $v->jenis_sertifikat ?></td>
            <td><?= $v->nomor ?></td>
            <td><?= $v->luas_lahan ?></td>
            <td>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#tkd-zona', url:'/ddc/ddc-tkd/update?kd_desa=<?=$v->kd_desa?>&no_tkd=<?=$v->no_tkd?>'});" class="btn btn-danger">Update</a> 
                <a href="javascript:void(0)" onclick="deleteData('<?= $v->no_tkd?>', '/ddc/ddc-tkd/delete?kd_desa=<?=$v->kd_desa?>&no_tkd=<?=$v->no_tkd?>', 'Hapus Data Potensi');" class="btn btn-danger">Hapus</a>
            </td>
        </tr>
    <?php } ?>
    </table>
</div>

<?php
$scripts =<<<JS
    function deleteData(modul, url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#tkd-zona', urlBack:'/ddc/ddc-tkd/view-box?kd_desa={$kd_desa}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);