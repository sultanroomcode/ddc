<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-6',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_desa = $model->kd_desa_lokasi = Yii::$app->user->identity->id;
    $model->kd_kecamatan = $model->kd_kecamatan_lokasi = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = $model->kd_kabupaten_lokasi = substr($model->kd_desa, 0, 4);
    $model->genNum();
}

$urlback = "goLoad({elm:'#tkd-zona', url:'/ddc/ddc-tkd/view-box?kd_desa=".$model->kd_desa."'});";
?>

<div class="ddc-tkd-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'no_tkd')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'jenis_sertifikat')->dropdownList([
        'sertifikat' => 'Sertifikat',
        'persil' => 'persil',
        'letter-c' => 'Letter C',
        'letter-d' => 'Letter D',
    ]) ?>

    <?= $form->field($model, 'nomor')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'jenis_lahan')->dropdownList([
        'sawah' => 'Tanah Sawah',
        'kering' => 'Tanah Kering',
        'hutan-desa' => 'Tanah Hutan Desa',
        'basah' => 'Tanah Basah/Rawa/Gambut/Pasang Surut',
        'perkebunan' => 'Tanah Perkebunan,',
        'fasilitas-umum' => 'Tanah Fasilitas Umum',
    ]) ?>

    <?= $form->field($model, 'luas_lahan')->widget(\yii\widgets\MaskedInput::className(), [
        'clientOptions' => [
            'alias' => 'decimal',
            'digits' => 2,
            'digitsOptional' => false,
            'radixPoint' => ',',
            'groupSeparator' => '.',
            'autoGroup' => true,
            'removeMaskOnSubmit' => true,
        ]
    ])->label('Luas Lahan (m<sup>2</sup>)') ?>

    <?= $form->field($model, 'lokasi')->dropdownList(['desa' => 'Desa', 'luar-desa' => 'Luar Desa']) ?>
    
    <div id="lokasi-2">
        <?= $form->field($model, 'kd_kabupaten_lokasi')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'kd_kecamatan_lokasi')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'kd_desa_lokasi')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);