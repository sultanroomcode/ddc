<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcTkd */

$this->title = 'Create Ddc Tkd';
$this->params['breadcrumbs'][] = ['label' => 'Ddc Tkds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-tkd-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
