<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$model->tipe_operator = $data['tipe'];
$model->kd_desa = Yii::$app->user->identity->id;
$urlback = '?type='.$data['tipe'].'&provinsi=0';//untuk load list harus pakai type daripada tipe
?>

<div class="operator-form">
    <?php $form = ActiveForm::begin($arrFormConfig); ?>
    <h3>Registrasi Operator <?= ucwords($model->tipe_operator) ?></h3>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'tipe_operator')->hiddenInput(['maxlength' => true])->label(false) ?>

    <div class="row">
        <div class="col-md-6">
        
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
        
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({elm:'#operator-zone', url:'/ddc/data-operator/list-operator{$urlback}'});
            $.alert('Berhasil menyimpan data');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);