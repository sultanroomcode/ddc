<?php 
$dataprovinsi = $data['provinsi']; 
$datatipe = $data['type']; 
?>
<div class="tile">
    <h2 class="tile-title">Registrasi Manual Operator</h2>
    <div class="p-10">
		<div id="operator-zone"></div>
	</div>
</div>
<?php
$script =<<<JS
goLoad({elm:'#operator-zone', url:'/ddc/data-operator/view-list?tipe={$datatipe}&provinsi={$dataprovinsi}'});
JS;
$this->registerJs($script);