<?php
echo 'INSERT INTO dummy_bumdes2 (
kd_kabupaten,nama_kabupaten,kd_kecamatan,nama_kecamatan,kd_desa,nama_desa,nama_bumdes,no_perdes,tahun_pendirian,sk_pengelola,data_pbumdes_ketua, data_pbumdes_no_ketua,data_pbumdes_sekretaris,data_pbumdes_no_sekretaris,data_pbumdes_bendahara,data_pbumdes_no_bendahara,jml_sdm_smp,jml_sdm_sma, jml_sdm_s1,jml_sdm_s2,alamat_kantor,prudes,jenis_uu_1,jenis_uu_2,jenis_uu_3,jenis_uu_4,jenis_uu_5,jenis_uu_6,unit_usaha_prioritas,penyertaan_modal,omset_per_tahun,keuntungan_per_tahun, jenis_bantuan_pemerintah,kerjasama_1,kerjasama_2,kerjasama_3,kerjasama_4,status,keterangan,status_move
) VALUES 
';
$i = 1;
foreach ($worksheet->getRowIterator() as $row) {
    $cellIterator = $row->getCellIterator();
    // echo '<td>'.var_dump($cellIterator).'</td>';
    // $cellIterator->setIterateOnlyExistingCells(true); // This loops through all cells,
                                                       //    even if a cell value is not set.
                                                       // By default, only cells that have a value
                                                       //    set will be iterated.
    echo "(";
    foreach ($cellIterator as $cell) {
        if($cell->getColumn() == 'I'){//tahun pendirian
          //jika kurang dari tahun 2000 lebih dari 2018 maka diset menjadi 2018
          $val = (int) substr($cell->getFormattedValue(), -4,4);

          if($val < 2000 || $val > 2018){
            echo "'-',";
          } else {
            echo "'".$val."',";
          }
        } else if($cell->getColumn() == 'AD' || $cell->getColumn() == 'AE' || $cell->getColumn() == 'AF') {
          $val = (int) str_replace([',','.'], '', $cell->getValue());
          echo "'".$val."',";
        } else if($cell->getColumn() == 'AL') {
          $val = (int) ($cell->getValue() == 'Tidak' || $cell->getValue() == '0')?'tidak':'aktif';
          echo "'".$val."',";
        } else {
          echo "'".str_replace("'", "_", $cell->getValue())."',";
        }
    }
    echo "0,0), <br>";
}