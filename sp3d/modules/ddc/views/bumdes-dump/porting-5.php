<?php
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\bumdes\models\BumdesPengelola;
use sp3d\modules\bumdes\models\BumdesModal;
use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use sp3d\modules\ddc\models\DummyBumdes;

$model = DummyBumdes::find()->where(['kd_kabupaten' => $kd_kabupaten, 'status_move' => 1]);
$model->andWhere(['<>','kd_desa_ddc', '']);
$i = 1;
function inserting($arr){
    $juu1 = new BumdesUnitUsaha();
    $juu1->kd_desa = $arr['kd_desa_ddc'];
    $juu1->id_bumdes = $arr['id_bumdes'];
    $juu1->id_uu = $arr['num'];
    $juu1->unit_usaha = strtoupper($arr['val']);
    $juu1->omset = $arr['omset'];
    $juu1->bh_status = '-';
    $juu1->bh_bentuk = '-';
    $juu1->bh_no = '-';
    $juu1->jml_karyawan = $arr['countingSDM'];
    $juu1->keuntungan = $arr['keuntungan'];
    $juu1->status = 'aktif';
    $juu1->foto = '';
    $juu1->keterangan = '';
    $juu1->created_by = 35;
    $juu1->updated_by = 35;
    $juu1->created_at = date('Y-m-d H:i:s');
    $juu1->updated_at = date('Y-m-d H:i:s');
    return $juu1->save(false);
}

foreach($model->all() as $v){
	// inserting on bumdes
	$connection = Yii::$app->db;
	// var_dump($v->penyertaan_modal);
 //    var_dump($v->omset_per_tahun);
 //    var_dump($v->keuntungan_per_tahun);    
   	$transaction = $connection->beginTransaction();

    try {
        $m = Bumdes::findOne(['kd_desa' => $v->kd_desa_ddc]);
        if($m != null){
            $countingSDM = (int) (($v->jml_sdm_smp == '')?0:$v->jml_sdm_smp) + (($v->jml_sdm_sma == '')?0:$v->jml_sdm_sma) + (($v->jml_sdm_s1 == '')?0:$v->jml_sdm_s1) + (($v->jml_sdm_s2 == '')?0:$v->jml_sdm_s2);

            $num = 1;
            //var_dump($countingSDM);
    	    if($v->jenis_uu_1 !== ''){
                //xplode
                $arr = explode(',', $v->jenis_uu_1);
                if(count($arr) == 1){
                    $arrval = [
                        'kd_desa_ddc' => $v->kd_desa_ddc,
                        'id_bumdes' => $m->id_bumdes,
                        'num' => $num,
                        'countingSDM' => $countingSDM,
                        'val' => $arr[0],
                        'omset' => $v->omset_per_tahun,
                        'keuntungan' => $v->keuntungan_per_tahun,
                    ];
                    inserting($arrval);
                    // var_dump($arrval);
                    // exit();
                    $num++;
                } else {
                    foreach($arr as $xx){
                        $arrval = [
                            'kd_desa_ddc' => $v->kd_desa_ddc,
                            'id_bumdes' => $m->id_bumdes,
                            'num' => $num,
                            'countingSDM' => $countingSDM,
                            'val' => $xx,
                            'omset' => $v->omset_per_tahun,
                            'keuntungan' => $v->keuntungan_per_tahun,
                        ];
                        inserting($arrval);
                        // var_dump($arrval);
                        $num++;
                    }
                }
            }

            if($v->jenis_uu_2 !== ''){
                //xplode
                $arr = explode(',', $v->jenis_uu_2);
                if(count($arr) == 1){
                    $arrval = [
                        'kd_desa_ddc' => $v->kd_desa_ddc,
                        'id_bumdes' => $m->id_bumdes,
                        'num' => $num,
                        'countingSDM' => $countingSDM,
                        'val' => $arr[0],
                        'omset' => $v->omset_per_tahun,
                        'keuntungan' => $v->keuntungan_per_tahun,
                    ];
                    inserting($arrval);
                    $num++;
                } else {
                    foreach($arr as $xx){
                        $arrval = [
                            'kd_desa_ddc' => $v->kd_desa_ddc,
                            'id_bumdes' => $m->id_bumdes,
                            'num' => $num,
                            'countingSDM' => $countingSDM,
                            'val' => $xx,
                            'omset' => $v->omset_per_tahun,
                            'keuntungan' => $v->keuntungan_per_tahun,
                        ];
                        inserting($arrval);
                        $num++;
                    }
                }
            }

            if($v->jenis_uu_3 !== ''){
                //xplode
                $arr = explode(',', $v->jenis_uu_3);
                if(count($arr) == 1){
                    $arrval = [
                        'kd_desa_ddc' => $v->kd_desa_ddc,
                        'id_bumdes' => $m->id_bumdes,
                        'num' => $num,
                        'countingSDM' => $countingSDM,
                        'val' => $arr[0],
                        'omset' => $v->omset_per_tahun,
                        'keuntungan' => $v->keuntungan_per_tahun,
                    ];
                    inserting($arrval);
                    $num++;
                } else {
                    foreach($arr as $xx){
                        $arrval = [
                            'kd_desa_ddc' => $v->kd_desa_ddc,
                            'id_bumdes' => $m->id_bumdes,
                            'num' => $num,
                            'countingSDM' => $countingSDM,
                            'val' => $xx,
                            'omset' => $v->omset_per_tahun,
                            'keuntungan' => $v->keuntungan_per_tahun,
                        ];
                        inserting($arrval);
                        $num++;
                    }
                }
            }

            if($v->jenis_uu_4 !== ''){
                //xplode
                $arr = explode(',', $v->jenis_uu_4);
                if(count($arr) == 1){
                    $arrval = [
                        'kd_desa_ddc' => $v->kd_desa_ddc,
                        'id_bumdes' => $m->id_bumdes,
                        'num' => $num,
                        'countingSDM' => $countingSDM,
                        'val' => $arr[0],
                        'omset' => $v->omset_per_tahun,
                        'keuntungan' => $v->keuntungan_per_tahun,
                    ];
                    inserting($arrval);
                    $num++;
                } else {
                    foreach($arr as $xx){
                        $arrval = [
                            'kd_desa_ddc' => $v->kd_desa_ddc,
                            'id_bumdes' => $m->id_bumdes,
                            'num' => $num,
                            'countingSDM' => $countingSDM,
                            'val' => $xx,
                            'omset' => $v->omset_per_tahun,
                            'keuntungan' => $v->keuntungan_per_tahun,
                        ];
                        inserting($arrval);
                        $num++;
                    }
                }
            }

            if($v->jenis_uu_5 !== ''){
                //xplode
                $arr = explode(',', $v->jenis_uu_5);
                if(count($arr) == 1){
                    $arrval = [
                        'kd_desa_ddc' => $v->kd_desa_ddc,
                        'id_bumdes' => $m->id_bumdes,
                        'num' => $num,
                        'countingSDM' => $countingSDM,
                        'val' => $arr[0],
                        'omset' => $v->omset_per_tahun,
                        'keuntungan' => $v->keuntungan_per_tahun,
                    ];
                    inserting($arrval);
                    $num++;
                } else {
                    foreach($arr as $xx){
                        $arrval = [
                            'kd_desa_ddc' => $v->kd_desa_ddc,
                            'id_bumdes' => $m->id_bumdes,
                            'num' => $num,
                            'countingSDM' => $countingSDM,
                            'val' => $xx,
                            'omset' => $v->omset_per_tahun,
                            'keuntungan' => $v->keuntungan_per_tahun,
                        ];
                        inserting($arrval);
                        $num++;
                    }
                }
            }

            if($v->jenis_uu_6 !== ''){
                //xplode
                $arr = explode(',', $v->jenis_uu_6);
                if(count($arr) == 1){
                    $arrval = [
                        'kd_desa_ddc' => $v->kd_desa_ddc,
                        'id_bumdes' => $m->id_bumdes,
                        'num' => $num,
                        'countingSDM' => $countingSDM,
                        'val' => $arr[0],
                        'omset' => $v->omset_per_tahun,
                        'keuntungan' => $v->keuntungan_per_tahun,
                    ];
                    inserting($arrval);
                    $num++;
                } else {
                    foreach($arr as $xx){
                        $arrval = [
                            'kd_desa_ddc' => $v->kd_desa_ddc,
                            'id_bumdes' => $m->id_bumdes,
                            'num' => $num,
                            'countingSDM' => $countingSDM,
                            'val' => $xx,
                            'omset' => $v->omset_per_tahun,
                            'keuntungan' => $v->keuntungan_per_tahun,
                        ];
                        inserting($arrval);
                        $num++;
                    }
                }
            }

            $status = 'OK';
        } else {
            $status = 'NOT FOUND';
        }
    	

        $transaction->commit();
    } catch(Exception $e) {
        echo $e->getMessage().'<br>';
        echo $e->getCode().'<br>';
        echo $e->getLine().'<br>';
        $transaction->rollback();
        $status = 'FAIL';
    }
    echo $i.'. '.$v->kd_desa_ddc.' - '.$status.'<hr>';
    // echo $i.'. '.$v->kd_desa_ddc.'<hr>';
    $i++;
    // sleep(1);//2 seconds
    // exit();
}

/*

*/