<?php
echo 'INSERT INTO dummy_bumdes2 (
kd_kabupaten,nama_kabupaten,kd_kecamatan,nama_kecamatan,kd_desa,nama_desa,nama_bumdes,no_perdes,tahun_pendirian,sk_pengelola,data_pbumdes_ketua, data_pbumdes_no_ketua,data_pbumdes_sekretaris,data_pbumdes_no_sekretaris,data_pbumdes_bendahara,data_pbumdes_no_bendahara,jml_sdm_smp,jml_sdm_sma, jml_sdm_s1,jml_sdm_s2,alamat_kantor,prudes,jenis_uu_1,jenis_uu_2,jenis_uu_3,jenis_uu_4,jenis_uu_5,jenis_uu_6,unit_usaha_prioritas,penyertaan_modal,omset_per_tahun,keuntungan_per_tahun, jenis_bantuan_pemerintah,kerjasama_1,kerjasama_2,kerjasama_3,kerjasama_4,status,keterangan,status_move
) VALUES 
';
$i = 1;
// echo $filter->getMin();
$min = $filter->getMin();
foreach ($worksheet->getRowIterator($filter->getMin(), $filter->getMax()) as $row) {
    $cellIterator = $row->getCellIterator();
    // echo '<td>'.var_dump($cellIterator).'</td>';
    $cellIterator->setIterateOnlyExistingCells(true); // This loops through all cells,
                                                       //    even if a cell value is not set.
                                                       // By default, only cells that have a value
                                                       //    set will be iterated.
    echo "(";
    foreach ($cellIterator as $cell) {
        if($cell->getColumn() == 'K'){
          //jika kurang dari tahun 2000 lebih dari 2018 maka diset menjadi 2018
          $val = (int) substr($cell->getFormattedValue(), -4,4);

          if($val < 2000 || $val > 2018){
            echo "'-',";
          } else {
            echo "'".$val."',";
          }
        } else if($cell->getColumn() == 'AN') {
          $val = (int) ($cell->getValue() == 'Tidak' || $cell->getValue() == '0')?'tidak':'aktif';
          echo "'".$val."',";
        } else {
          echo "'".str_replace("'", "_", $cell->getValue())."',";
        }
    }
    echo "0), <br>";
}

// 
                                            
 /*                                     
 INSERT INTO dummy_bumdes2 (
 1--kd_desa,
 2--nama_desa,
 3--kd_kecamatan,
 4--nama_kecamatan,
 5--kd_kabupaten,
 6--nama_kabupaten,
 7--nama_bumdes,
 8--no_perdes,
 9--tahun_pendirian,
 10--sk_pengelola,
 11--data_pbumdes_ketua,
 12--data_pbumdes_no_ketua,
 13--data_pbumdes_sekretaris,
 14--data_pbumdes_no_sekretaris,
 15--data_pbumdes_bendahara,
 16--data_pbumdes_no_bendahara,
 17--jml_sdm_smp,
 18--jml_sdm_sma,
 19--jml_sdm_s1,
 20--jml_sdm_s2,
 21--alamat_kantor,
 22--prudes,
 23--jenis_uu_1,
 24--jenis_uu_2,
 25--jenis_uu_3,
 26--jenis_uu_4,
 27--jenis_uu_5,
 28--jenis_uu_6,
 29--unit_usaha_prioritas,
 30--penyertaan_modal,
 31--omset_per_tahun,
 32--keuntungan_per_tahun,
 33--jenis_bantuan_pemerintah,
 34--kerjasama_1,
 35--kerjasama_2,
 36--kerjasama_3,
 37--kerjasama_4,
 38--status,
 39--keterangan,
 40--status_move ) VALUES 
 (
 'kd_kabupaten-3506',
 'nama_kabupaten-KEDIRI',
 'kd_kecamatan-350601',
 'nama_kecamatan-Semen',
 'kd_desa-3506012001',
 'nama_desa-Bulu',
 'nama_bumdes-Bulu Jaya',
 'no_perdes-No. 02 Tahun 2016',
 'tahun_pendirian-2016',
 'sk_pengelola--',
 'data_pbumdes_ketua--Khusnoyyin',
 'data_pbumdes_no_ketua',
 'data_pbumdes_sekretaris',
 'data_pbumdes_no_sekretaris',
 'data_pbumdes_bendahara',
 'data_pbumdes_no_bendahara',
 'jml_sdm_smp',
 'jml_sdm_sma',
 'jml_sdm_s1',
 'jml_sdm_s2',
 'alamat_kantor',
 'prudes',
 'jenis_uu_1--0',
 'jenis_uu_2--0',
 'jenis_uu_3--0',
 'jenis_uu_4--0',
 'jenis_uu_5--0',
 'jenis_uu_6--0',
 'unit_usaha_prioritas--0',
 'penyertaan_modal--',
 'omset_per_tahun--',
 'keuntungan_per_tahun--',
 'jenis_bantuan_pemerintah--',
 'kerjasama_1--',
 'kerjasama_2--',
 'kerjasama_3--',
 'kerjasama_4--',
 'status--Tidak',
 'keterangan--',
 0),
('3506','KEDIRI','350601','Semen','3506012002','Sidomulyo','Panji Mulya','No. 3 tahun 2016','2016','0','Ika W','','','','','','','','','','','','Pasar Desa','','','','','','','','','','','','','','','Tidak','',0),
('3506','KEDIRI','350601','Semen','3506012003','Puhrubuh','Sumber Makmur','No. 3 tahun 2016','2016','0','Saerofi Ahmad','','','','','','','','','','','','','','','','','','','','','','','','','','','Tidak','',0)*/


//('3506','KEDIRI','350605','Kandat','3506052010','Pule','Pule Makmur','02 tahun 2016 ttg Pembentukan dan pengelolaan Badan Usaha Milik Desa','2018','188/02/418.84.10/2016','H. Misbahul Anam','085649843305','Galang Setiawan','','Tulus Santoso','','','1','2','','kantor desa','Ternak Kambing','Peternakan','','','','','Peternakan','50000000','0','0','','','','','','1','usaha baru mulai',0),