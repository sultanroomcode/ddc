<?php
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\ddc\models\DummyBumdes;

$model = DummyBumdes::find()->where(['kd_kabupaten' => $kd_kabupaten, 'status_move' => 0]);
$model->andWhere(['<>','kd_desa_ddc', '']);
$i = 1;
foreach($model->all() as $v){
	// inserting on bumdes
	$connection = Yii::$app->db;
   	$transaction = $connection->beginTransaction();

    try {
    	$m = new Bumdes();
		$m->kd_desa = $v->kd_desa_ddc;
		$m->kd_kecamatan = substr($v->kd_desa_ddc, 0, 7);
		$m->kd_kabupaten = substr($v->kd_desa_ddc, 0, 4);
		$m->id_bumdes = time();
		$m->nama_bumdes = $v->nama_bumdes;
		$m->tahun_berdiri = $v->tahun_pendirian;
		$m->alamat = $v->alamat_kantor;
		$m->legal_perdes_no = $v->no_perdes;
		$m->legal_skdes_bumdes_no = '-';
		$m->status = $v->status;
		$m->created_by = '35';
		$m->updated_by = '35';
		$m->created_at = date('Y-m-d H:i:s');
		$m->updated_at = date('Y-m-d H:i:s');
		if($m->save(false)){
			$mdb = DummyBumdes::findOne(['kd_desa' => $v->kd_desa]);
			$mdb->status_move = 1;
			$mdb->update(false);
		}

		$status = 'OK';

        $transaction->commit();
    } catch(Exception $e) {
        var_dump($e);
        $transaction->rollback();
        $status = 'FAIL';
    }
    echo $i.'. '.$v->kd_desa.' - '.$status.'<br>';
    sleep(1);//2 seconds
    $i++;
    // exit();
}

// TRUNCATE bumdes;
// TRUNCATE bumdes_pengelola;