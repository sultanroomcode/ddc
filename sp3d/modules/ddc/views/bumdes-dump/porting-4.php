<?php
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\bumdes\models\BumdesPengelola;
use sp3d\modules\bumdes\models\BumdesModal;
use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use sp3d\modules\ddc\models\DummyBumdes;

$model = DummyBumdes::find()->where(['kd_kabupaten' => $kd_kabupaten, 'status_move' => 1]);
$model->andWhere(['<>','kd_desa_ddc', '']);
$i = 1;
foreach($model->all() as $v){
	// inserting on bumdes
	$connection = Yii::$app->db;
	// var_dump($v->penyertaan_modal);
 //    var_dump($v->omset_per_tahun);
 //    var_dump($v->keuntungan_per_tahun);    
   	$transaction = $connection->beginTransaction();

    try {
    	$m = Bumdes::findOne(['kd_desa' => $v->kd_desa_ddc]);
        if($m != null){
            // $m->id_bumdes
            //inserting modal
            $im = new BumdesModal();
            $im->kd_desa = $m->kd_desa;
			$im->id_bumdes = $m->id_bumdes;
			$im->tahun = 2018;
			$im->kd_kecamatan = $m->kd_kecamatan;
			$im->kd_kabupaten = $m->kd_kabupaten;
			$im->jml_aset = 0;
			$im->omset = (float) $v->omset_per_tahun;
			$im->keuntungan = (float) $v->keuntungan_per_tahun;
			$im->smb_pad = 0;
			$im->modal = (float) $v->penyertaan_modal;
			$im->sb_modal_desa = 0;
			$im->sb_modal_masyarakat = 0;
			$im->keterangan = 'i-prov';
			$im->created_by = $im->updated_by = 35;
            $im->created_at = $im->updated_at = date('Y-m-d H:i:s');
            $im->save(false);

            $status = 'OK';
        } else {
            $status = 'NOT FOUND';
        }
    	

        $transaction->commit();
    } catch(Exception $e) {
        // var_dump($e);
        $transaction->rollback();
        $status = 'FAIL';
    }
    echo $i.'. '.$v->kd_desa_ddc.' - '.$status.'<hr>';
    // echo $i.'. '.$v->kd_desa_ddc.'<hr>';
    $i++;
    sleep(1);//2 seconds
    // exit();
}

/*

*/