<?php
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\bumdes\models\BumdesPengelola;
use sp3d\modules\bumdes\models\BumdesModal;
use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use sp3d\modules\ddc\models\DummyBumdes;

$m = BumdesUnitUsaha::find()->select(['COUNT(*) AS bh_no', 'unit_usaha'])->orderBy('bh_no DESC')->groupBy('unit_usaha');

echo '<table border="1" style="border-collapse: collapse;">';
foreach ($m->all() as $v) {
    echo '<tr><td>'.$v->unit_usaha.'</td><td>'.$v->bh_no.'</td><td>UPDATE bumdes_unit_usaha SET unit_usaha = \''.$v->unit_usaha.'\' WHERE unit_usaha = \''.$v->unit_usaha.'\'</td></tr>';
}
echo '</table>';