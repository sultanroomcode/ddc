<?php
use sp3d\models\User;
use yii\helpers\Url;
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\bumdes\models\BumdesPengelola;
use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use sp3d\modules\ddc\models\DummyBumdes;

$model = DummyBumdes::find()->where(['kd_kabupaten' => $kd_kabupaten, 'kd_kecamatan_ddc' => '', 'status_move' => 0])->groupBy('kd_kecamatan');
echo "SELECT * FROM `dummy_bumdes2` WHERE kd_kecamatan_ddc = '';<br>";
echo '<div id="form-editor"></div';
foreach($model->all() as $v){
    //echo 'origin id : '.$v->kd_desa.'<br>';
    echo 'origin id (kec) : '.substr($v->kd_desa, 0, 7).'<br>';
    echo 'origin id (kab) : '.substr($v->kd_desa, 0, 4).'<br>';
    echo 'origin nama : '.$v->nama_desa.'<br>';
    echo 'origin nama (kec) : '.$v->nama_kecamatan.'<br>';
    echo 'origin nama (kab) : '.$v->nama_kabupaten.'<br>';
	//echo 'finding nama : '.strtoupper('desa '.$v->nama_desa).'<br>';
    echo 'SELECT * FROM user  WHERE type=\'kecamatan\' AND id LIKE \''.$v->kd_kabupaten.'%\' ORDER BY description;<br>';
    echo 'SELECT * FROM user  WHERE type=\'kecamatan\' AND description = \'KECAMATAN '.strtoupper($v->nama_kecamatan).'\';<br>';

    $m = User::find()->where(['description' => strtoupper('kecamatan '.$v->nama_kecamatan)]);
    $m->andWhere(['like', 'id', substr($v->kd_desa, 0, 4).'%', false]);

    if($force){
        $n = User::find()->where(['like', 'id', substr($v->kd_desa, 0, 4).'%', false]);
        $n->andWhere(['type' => 'kecamatan']);

        foreach ($n->all() as $y) {
            echo '-'. $y->description.' <a href="'.Url::to(['/ddc/bumdes-dump/form-update?id='.$y->id]).'">Edit Kecamatan</a> <br>';
        }
    }
    
    foreach($m->all() as $x){
        DummyBumdes::updateAll(['kd_kecamatan_ddc' => $x->id], ['kd_kecamatan' => $v->kd_kecamatan]);
        echo '- '.$x->id.' -- '.$x->description.'<br>';
    }
    echo '<hr>';
    // exit();
}