<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'DDC Podes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="podes-podes-view">
    <div class="tile">
        <h2 class="tile-title">Potensi Desa</h2>
        <div class="p-10">
            <div id="podes-zona"></div>
        </div>
    </div>
</div>

<?php
$script =<<<JS
goLoad({elm:'#podes-zona', url:'/ddc/ddc-podes-pp/view-box?modul={$modul}'});
JS;

$this->registerJs($script);
?>