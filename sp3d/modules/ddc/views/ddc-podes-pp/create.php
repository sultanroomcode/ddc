<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcPodes */

$this->title = 'Buat Potensi Desa '.$model->jenis;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Podes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-podes-create">
	<h3><?= $this->title ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
