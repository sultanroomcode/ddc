<?php
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\ddc\models\DummyPasar;
use sp3d\models\User;

$model = DummyPasar::find()->where(['kd_kabupaten' => $kd_kabupaten, 'kd_kecamatan' => ''])->groupBy('nama_kecamatan');
$i = 1;

foreach($model->all() as $v){
	// inserting on bumdes
	$connection = Yii::$app->db;
   	$transaction = $connection->beginTransaction();

    try {
    	$mo = User::find()->where(['description' => strtoupper('kecamatan '.$v->nama_kecamatan)])->andWhere(['like', 'id', $kd_kabupaten.'%', false]);
    	$val = $mo->one();
    	if($val != null){
    		DummyPasar::updateAll(['kd_kecamatan' => $val->id],['kd_kabupaten' => $kd_kabupaten, 'nama_kecamatan' => $v->nama_kecamatan]);
    	}

		$status = 'OK';
        $transaction->commit();
    } catch(Exception $e) {
        var_dump($e);
        $transaction->rollback();
        $status = 'FAIL';
    }
    echo $i.'. '.$v->nama_kecamatan.' - '.$status.'<br>';
    // sleep(1);//2 seconds
    $i++;
    // exit();
}