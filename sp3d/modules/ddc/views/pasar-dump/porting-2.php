<?php
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\ddc\models\DummyPasar;
use sp3d\models\User;

$model = DummyPasar::find()->where(['kd_kabupaten' => $kd_kabupaten, 'kd_desa' => '']);
$i = 1;

foreach($model->all() as $v){
    // inserting on bumdes
    $connection = Yii::$app->db;
    $transaction = $connection->beginTransaction();

    echo '<div class="container"><div class="row">';
    echo '<div class="col-md-6">';
    echo 'Nama Kabupaten : '.$v->nama_kabupaten.'<br>';
    echo 'Kode Kecamatan : <b>'. $v->kd_kecamatan.'</b><br>';
    echo 'Nama Kecamatan : '. $v->nama_kecamatan.'<br>';
    echo 'Nama Desa : '. $v->nama_desa.'<br>';
    echo 'Finding Nama : '.strtoupper('desa '.$v->nama_desa).'<br>';
    echo '<a href="javascript:void(0)" onclick="clearAll();goLoad({elm:\'#form-editor-'.$v->jml_pedagang.$v->jml_jenis_barang.'\', url:\'ddc/pasar-dump/form-update-dummy?jml_pedagang='.$v->jml_pedagang.'&jml_jenis_barang='.$v->jml_jenis_barang.'&kd_kecamatan='.$v->kd_kecamatan.'&nama_desa='.$v->nama_desa.'\'})">Edit Desa on Dummy</a><br>';
    echo '</div><div class="col-md-6">';
    try {
        $mo = User::find()->where(['description' => strtoupper('desa '.$v->nama_desa)])->andWhere(['like', 'id', $v->kd_kecamatan.'%', false]);
        $val = $mo->one();
        // var_dump($val);
        if($val != null){
            $command = $connection->createCommand("UPDATE dummy_pasar SET kd_desa='".$val->id."' WHERE kd_kabupaten = '".$v->kd_kabupaten."' AND kd_kecamatan = '".$v->kd_kecamatan."' AND nama_desa = '".$v->nama_desa."' AND jml_pedagang = ".$v->jml_pedagang." AND jml_jenis_barang = ".$v->jml_jenis_barang);
            $command->execute();

            // DummyPasar::findOne(['kd_kabupaten' => $v->kd_kabupaten, 'kd_kecamatan' => $v->kd_kecamatan, 'kd']);
            // DummyPasar::updateAll(['kd_kecamatan' => $val->id],['kd_kabupaten' => $kd_kabupaten, 'nama_kecamatan' => $v->nama_kecamatan]);
            echo "<b>Di user ada</b><br>";
            echo "ID : ".$val->id."<br>";
            echo "Tipe : ".$val->type."<br>";
            echo "Nama : ".$val->description."<br>";
        } else {
            echo "<b>Di user kosong</b><br>";
            $n2 = User::find()->where(['like', 'id', $v->kd_kecamatan.'%', false]);
            $n2->andWhere(['like', 'description', 'DESA '.substr($v->nama_desa, 0, 1).'%', false]);
            $n2->orderBy('description');
            foreach ($n2->all() as $xx) {
                echo '-'.$xx->id.' '. $xx->description.' :: '.$xx->password_default.' <a href="javascript:void(0)" onclick="clearAll();goLoad({elm:\'#form-editor-'.$v->jml_pedagang.$v->jml_jenis_barang.'\', url:\'ddc/bumdes-dump/form-update?id='.$xx->id.'\'})">Edit Desa</a><br>';
            }
        }

        $status = 'OK';
        $transaction->commit();
    } catch(Exception $e) {
        var_dump($e);
        $transaction->rollback();
        $status = 'FAIL';
    }
    echo $i.'. '.$v->nama_desa.' - '.$status.'<br><div class="form-editor-bx" id="form-editor-'.$v->jml_pedagang.$v->jml_jenis_barang.'"></div></div></div></div><hr>';
    // sleep(1);//2 seconds
    $i++;
    // exit();
}

$script =<<<JS
    function clearAll(){
        $('.form-editor-bx').text('-');
    }
JS;
$this->registerJs($script, \yii\web\View::POS_HEAD);