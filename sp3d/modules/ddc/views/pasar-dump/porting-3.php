<?php
use sp3d\models\Pasardesa;
use sp3d\models\PasardesaData;
use sp3d\modules\ddc\models\DummyPasar;
use sp3d\models\User;

$model = DummyPasar::find()->where(['kd_kabupaten' => $kd_kabupaten])->andWhere(['<>','kd_desa', '']);
$i = 1;

foreach($model->all() as $v){
    // inserting on bumdes
    $connection = Yii::$app->db;
    $transaction = $connection->beginTransaction();

    try {
        //add to pasar desa
        $c = Pasardesa::find()->where(['kd_desa' => $v->kd_desa])->max('id_pasar');

        $counting = ($c == null)?1: $c++;
        echo '::'.$counting.'::';
        $m = new PasarDesa();
        $m->kd_desa = $v->kd_desa;
        $m->kd_kecamatan = $v->kd_kecamatan;
        $m->kd_kabupaten = $v->kd_kabupaten;
        $m->kd_provinsi = substr($v->kd_kabupaten, 0,2);
        $m->id_pasar = $counting;
        $m->nama = $v->nama_pasar;
		$m->kepemilikan_tanah = $v->kepemilikan_tanah;
		$m->legalitas = $v->legalitas;
		$m->no_legalitas = '-';
		$m->tahun = $v->tahun_berdiri;
		$m->created_by = 35;
		$m->updated_by = 35;
		$m->created_at = date('Y-m-d H:i:s');
		$m->updated_at = date('Y-m-d H:i:s');

        if($m->save(false)){
        	//pasar desa data
        	$npd = new PasardesaData();
        	$npd->kd_desa = $m->kd_desa;
			$npd->id_pasar = $m->id_pasar;
			$npd->kd_isian = 2018;
			$npd->jml_pedagang = $v->jml_pedagang;
			$npd->jml_jenis_barang = $v->jml_jenis_barang;
			$npd->pras_ruko = $v->ruko;
			$npd->pras_kios = $v->kios;
			$npd->pras_los = $v->los;
			$npd->pras_lapak = $v->lapak;
			$npd->pras_lesehan =$v->lesehan;
			$npd->pras_kantor =$v->kantor;
			$npd->pras_aset = '';
			$npd->pras_pendukung = $v->prasarana_pendukung;
			$npd->pengelola = $v->pengelola;
			$npd->keterangan = '-';
			$npd->created_by = 35;
			$npd->updated_by = 35;
			$npd->created_at = date('Y-m-d H:i:s');
			$npd->updated_at = date('Y-m-d H:i:s');
			$npd->save(false);
        }

        $status = 'OK';
        $transaction->commit();
    } catch(Exception $e) {
        //var_dump($e);
        $transaction->rollback();
        $status = 'FAIL';
    }
    echo $i.'. '.$v->nama_desa.' - '.$status.'<br>';
    // sleep(1);//2 seconds
    $i++;
    // exit();
}
/*
pasardesa_data
jml_pedagang
jml_jenis_barang
ruko
kios
los
lapak
lesehan
kantor
prasarana_pendukung
tahun_berdiri
kepemilikan_tanah
legalitas
pengelola
mv_pasar
mv_pasar_data
*/
//kediri desa semen belum ada


