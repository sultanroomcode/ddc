<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ddc Satuans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="satuan-podes-view">
    <div class="tile">
        <h2 class="tile-title">Master Satuan</h2>
        <div class="p-10">
            <div id="satuan-podes-zona"></div>
        </div>
    </div>
</div>

<?php
$script =<<<JS
goLoad({elm:'#satuan-podes-zona', url:'/ddc/ddc-satuan/view-box?modul={$modul}'});
JS;

$this->registerJs($script);
?>