<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$urlback = "goLoad({elm:'#satuan-podes-zona', url:'/ddc/ddc-satuan/index?modul=".$model->modul."'});";

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\Bumdes */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->genNum();
}
?>

<div class="ddc-satuan-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'modul')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'no_satuan')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'nama_satuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-7">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);