<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcSatuan */

$this->title = 'Update Ddc Satuan: ' . $model->modul;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Satuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->modul, 'url' => ['view', 'modul' => $model->modul, 'no_satuan' => $model->no_satuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ddc-satuan-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
