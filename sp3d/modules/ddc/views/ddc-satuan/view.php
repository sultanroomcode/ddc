<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcSatuan */

$this->title = $model->modul;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Satuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-satuan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'modul' => $model->modul, 'no_satuan' => $model->no_satuan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'modul' => $model->modul, 'no_satuan' => $model->no_satuan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'modul',
            'no_satuan',
            'nama_satuan',
            'deskripsi',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
