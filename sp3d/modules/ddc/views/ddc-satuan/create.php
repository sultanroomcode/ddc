<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\ddc\models\DdcSatuan */

$this->title = 'Create Ddc Satuan';
$this->params['breadcrumbs'][] = ['label' => 'Ddc Satuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-satuan-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
