<?php
namespace sp3d\modules\ddc\models;

use Yii;
use sp3d\models\User;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "ddc_podes".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_date
 * @property string $jenis
 * @property int $komoditas
 * @property string $luas_lahan_produksi
 * @property string $lahan_milik
 * @property string $hasil_produksi
 * @property string $satuan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class DdcPodes extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ddc_podes';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_date'], 'required'],
            [['id_date', 'komoditas', 'satuan'], 'integer'],
            [['luas_lahan_produksi', 'hasil_produksi'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['jenis'], 'string', 'max' => 20],
            [['lahan_milik', 'keterangan'], 'string', 'max' => 100],
            [['kd_desa', 'id_date'], 'unique', 'targetAttribute' => ['kd_desa', 'id_date']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_date' => 'Id Date',
            'jenis' => 'Jenis',
            'komoditas' => 'Komoditas',
            'luas_lahan_produksi' => 'Luas Lahan Produksi (Ha)',
            'lahan_milik' => 'Lahan Milik',
            'hasil_produksi' => 'Hasil Produksi',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['modal', 'sb_modal_desa', 'sb_modal_masyarakat'], 'safe'],
        } 
        $this->luas_lahan_produksi = (float) strtr($this->luas_lahan_produksi,array(','=>'.'));
        $this->hasil_produksi = (float) strtr($this->hasil_produksi,array(','=>'.'));
        
        return true;
    } 

    public function getKomoditasp()
    {
        return $this->hasOne(DdcKomoditasPodes::className(), ['id' => 'komoditas']);
    }

    public function getSatuanid()
    {
        return $this->hasOne(DdcSatuan::className(), ['no_satuan' => 'satuan']);
    }

    public function arrSatuan()
    {
        return ArrayHelper::map(DdcSatuan::find()->where(['modul' => $this->jenis])->all(), 'no_satuan', 'nama_satuan');
    }

    public function arrLahan()
    {
        return [
            'perorangan' => 'Perorangan',
            'perusahaan' => 'Perusahaan',
            'pemerintah' => 'Pemerintah'
        ];
    }

    public function arrKomoditas()
    {
        return ArrayHelper::map(DdcKomoditasPodes::find()->where(['jenis' => $this->jenis])->all(), 'id', 'nama_komoditas');
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }
}
