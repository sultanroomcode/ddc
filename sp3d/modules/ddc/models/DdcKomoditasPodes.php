<?php
namespace sp3d\modules\ddc\models;

use Yii;
use sp3d\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "ddc_komoditas_podes".
 *
 * @property int $id
 * @property string $jenis
 * @property string $nama_komoditas
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class DdcKomoditasPodes extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ddc_komoditas_podes';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['jenis'], 'string', 'max' => 20],
            [['nama_komoditas'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis' => 'Jenis',
            'nama_komoditas' => 'Nama Komoditas',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getPodes()
    {
        return $this->hasMany(DdcPodes::className(), ['komoditas' => 'id']);
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->max('id');
        if($mx == null){
            $this->id = 1;
        } else {
            $this->id = $mx +1;
        }
    }
}
