<?php
namespace sp3d\modules\ddc\models;

use Yii;
use sp3d\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "ddc_tkd".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $no_tkd
 * @property string $jenis_sertifikat
 * @property string $nomor
 * @property string $luas_lahan
 * @property string $lokasi
 * @property string $kd_desa_lokasi
 * @property string $kd_kecamatan_lokasi
 * @property string $kd_kabupaten_lokasi
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class DdcTkd extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ddc_tkd';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'no_tkd'], 'required'],
            [['no_tkd'], 'integer'],
            [['luas_lahan'], 'safe'],
            [['kd_desa', 'kd_desa_lokasi'], 'string', 'max' => 12],
            [['kd_kecamatan', 'kd_kecamatan_lokasi'], 'string', 'max' => 8],
            [['kd_kabupaten', 'kd_kabupaten_lokasi'], 'string', 'max' => 5],
            [['jenis_sertifikat', 'jenis_lahan'], 'string', 'max' => 50],
            [['nomor'], 'string', 'max' => 100],
            [['lokasi'], 'string', 'max' => 10],
            [['kd_desa', 'no_tkd'], 'unique', 'targetAttribute' => ['kd_desa', 'no_tkd']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Desa',
            'kd_kecamatan' => 'Kecamatan',
            'kd_kabupaten' => 'Kabupaten',
            'no_tkd' => 'No Tkd',
            'jenis_sertifikat' => 'Jenis Sertifikat',
            'jenis_lahan' => 'Jenis Lahan',
            'nomor' => 'Nomor',
            'luas_lahan' => 'Luas Lahan (m<sup>2</sup>)',
            'lokasi' => 'Lokasi',
            'kd_desa_lokasi' => 'Kd Desa Lokasi',
            'kd_kecamatan_lokasi' => 'Kd Kecamatan Lokasi',
            'kd_kabupaten_lokasi' => 'Kd Kabupaten Lokasi',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['modal', 'sb_modal_desa', 'sb_modal_masyarakat'], 'safe'],
        } 
        $this->luas_lahan = (float) strtr($this->luas_lahan,array(','=>'.'));
        
        return true;
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('no_tkd');
        if($mx == null){
            $this->no_tkd = 1;
        } else {
            $this->no_tkd = $mx +1;
        }
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }
}
