<?php

namespace sp3d\modules\ddc\models;

use Yii;

/**
 * This is the model class for table "ddc_satuan".
 *
 * @property string $modul
 * @property int $no_satuan
 * @property string $nama_satuan
 * @property string $deskripsi
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class DdcSatuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ddc_satuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modul', 'no_satuan'], 'required'],
            [['no_satuan'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['modul', 'nama_satuan'], 'string', 'max' => 50],
            [['deskripsi'], 'string', 'max' => 100],
            [['created_by', 'updated_by'], 'string', 'max' => 10],
            [['modul', 'no_satuan'], 'unique', 'targetAttribute' => ['modul', 'no_satuan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'modul' => 'Modul',
            'no_satuan' => 'No Satuan',
            'nama_satuan' => 'Nama Satuan',
            'deskripsi' => 'Deskripsi',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->max('no_satuan');
        if($mx == null){
            $this->no_satuan = 1;
        } else {
            $this->no_satuan = $mx +1;
        }
    }
}
