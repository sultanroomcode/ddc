<?php
namespace sp3d\modules\ddc\models;

use yii\base\Model;
use Yii;
use Ramsey\Uuid\Uuid;
use ddcop\models\Operator;
use ddcop\models\OperatorDesa;
use ddcop\models\OperatorDetail;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
/**
 * Signup form
 */
class SignupOperator extends Model
{
    public $email;
    public $tipe_operator;
    public $password;

    public $kd_desa;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'tipe_operator', 'kd_desa'], 'required'],
            ['email', 'filter', 'filter' => 'trim'],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\ddcop\models\Operator', 'message' => 'email yang dimasukkan sudah pernah ada di Sistem.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();

            // var_dump($this->email);//prabowo@gerindra.com
            try {
                $user = new Operator();
                $uuidgen = Uuid::uuid4();
                $user->id = $uuidgen->getHex();
                $user->email = $this->email;
                $user->tipe_operator = $this->tipe_operator;
                $user->setPassword('default');
                // $user->setPassword($this->password);
                $user->generateAuthKey();
                // $user->status = $user->konfirmasi_email = $user->konfirmasi_desa = 0;
                $user->status = 10;
                $user->konfirmasi_email = 10;
                $user->konfirmasi_desa = 0;
                // ($user->save());
                if ($user->save()) {
                    //add 
                    $detail = new OperatorDetail();
                    $detail->id = $user->id;
                    $detail->nama = 'REGMANUAL';
                    $detail->tempat_lahir = 'SURABAYA';
                    $detail->tanggal_lahir = date('Y-m-d');
                    $detail->gender = 'L';
                    $detail->pendidikan = '0|SD';
                    $detail->no_hp = '-';
                    $detail->keterangan = '-';
                    if($detail->save()){
                        $desa = new OperatorDesa();
                        $desa->id = $user->id;
                        $desa->kd_desa = $this->kd_desa;
                        $desa->propose_at = date('Y-m-d H:i:s');
                        $desa->confirm_at = date('Y-m-d H:i:s');
                        $desa->save();
                        $this->sendToTransportEmail($user->email, $user->tipe_operator);
                    }
                }

                $transaction->commit();
            } catch(Exception $e) {
                $transaction->rollback();
            }

            return $user;
        }

        return null;
    }

    public function sendToTransportEmail($email, $module)
    {
        $ch = curl_init();
        $url = "http://artechindonesia.co.id/mailjet/mailing-registered.php?email=".$email."&module=".$module;
        // 'site/konfirmasi-email?kode='.md5($user->id).'&email='.$this->email;
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $outputsql = curl_exec($ch);
        curl_close($ch);
    }
}

