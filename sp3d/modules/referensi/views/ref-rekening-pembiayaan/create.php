<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekeningPembiayaan */

$this->title = 'Buat Referensi Rekening Pembiayaan Baru';
$this->params['breadcrumbs'][] = ['label' => 'Ref Rekening Pembiayaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rekening-pembiayaan-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'id_kabupaten' => $id_kabupaten,
    ]) ?>

</div>
