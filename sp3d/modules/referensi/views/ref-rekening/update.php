<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekening */

$this->title = 'Update Ref Rekening: ' . $model->id_kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rekenings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kabupaten, 'url' => ['view', 'id_kabupaten' => $model->id_kabupaten, 'kode' => $model->kode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-rekening-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
