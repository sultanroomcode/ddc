<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekening */

$this->title = 'Create Ref Rekening';
$this->params['breadcrumbs'][] = ['label' => 'Ref Rekenings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rekening-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
