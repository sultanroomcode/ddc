<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekening */

$this->title = $model->id_kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rekenings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rekening-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_kabupaten' => $model->id_kabupaten, 'kode' => $model->kode], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_kabupaten' => $model->id_kabupaten, 'kode' => $model->kode], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rekening/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Rekening</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-rekening/delete?id=<?= $model->id?>', urlBack:'ref-rekening/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Rekening</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kabupaten',
            'kode',
            'jenis',
            'tipe',
            'uraian',
            'keterangan',
        ],
    ]) ?>

</div>
