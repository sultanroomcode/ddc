<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Referensi Rekening</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
    <!-- post text -->
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/default/all'})" class="btn btn-sm btn-warning">Kembali</a>
    <?php if(Mimin::checkRoute('referensi/ref-rekening/*')): ?>
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening/create'})" class="btn btn-sm btn-warning">Tambah Referensi Rekening Baru</a><hr>
    <?php endif; ?>
    
    <table id="ref-rekening-table" class="display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Uraian</th>
                
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kode</th>
                <th>Uraian</th>

                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($dataProvider->all() as $v): ?>
            <tr>
                <td><?= $v->kode ?></td>
                <td><?= $v->uraian ?></td>

                <td>
                    <?php if(Mimin::checkRoute('referensi/ref-rekening/desa-update')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/referensi/ref-rekening/update?id_kabupaten=<?=$v->id_kabupaten?>&kode=<?=$v->kode?>'})" class="label label-warning">Update</a>
                    <?php endif; ?>

                    <?php if(Mimin::checkRoute('referensi/ref-rekening/desa-delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data Pajak Desa?', urlSend:'/referensi/ref-rekening/delete?id_kabupaten=<?=$v->id_kabupaten?>&kode=<?=$v->kode?>', urlBack:'/referensi/ref-rekening/index?id_kabupaten=<?=$v->id_kabupaten?>&tahun=<?=$tahun?>&effect=rubberBand', typeSend:'get'})" class="label label-danger">Hapus</a>
                    <?php endif; ?>                 

                    <?php if(Mimin::checkRoute('referensi/ref-rekening/desa-view')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/referensi/ref-rekening/view?id_kabupaten=<?=$v->id_kabupaten?>&kode=<?=$v->kode?>'})" class="label label-warning">View</a>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
    $('#ref-rekening-table').DataTable();
JS;

$this->registerJs($scripts);