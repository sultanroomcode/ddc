<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekening */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name RefRekening */
/* @var $generator string helper base name with camel to id Inflector ref-rekening */
/* @var $generator string helper base name with camel to word Inflector Ref Rekening */
?>

<div class="ref-rekening-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening/index'})" class="btn btn-warning">List Ref Rekening</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'id_kabupaten')->hiddenInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis')->dropdownList(['1' => 'Pendapatan', '2' => 'Kegiatan/Belanja', '3' => 'Pembiayaan']) ?>

    <?= $form->field($model, 'tipe')->dropdownList(['1' => 'Bidang', '2' => 'Program', '3' => 'Kegiatan']) ?>

    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ref-rekening/'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);