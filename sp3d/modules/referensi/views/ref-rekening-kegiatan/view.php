<?php
use yii\helpers\Html;
use hscstudio\mimin\components\Mimin;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekeningBelanja */

$this->title = $model->uraian;

?>
<div class="ref-rekening-kegiatan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if(Mimin::checkRoute('referensi/ref-rekening-kegiatan/update')): ?>
        <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening-kegiatan/update?id_kabupaten=<?=$model->id_kabupaten?>&kode=<?=$model->kode?>'})" class="label label-warning">Update</a>
        <?php endif; ?>

        <?php if(Mimin::checkRoute('referensi/ref-rekening-kegiatan/index')): ?>
        <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening-kegiatan/index?id_kabupaten=<?=$model->id_kabupaten?>'})" class="label label-warning">Index</a>
        <?php endif; ?>

        <?php if(Mimin::checkRoute('referensi/ref-rekening-kegiatan/delete')): ?>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus Data Kegiatan Desa?', urlSend:'/referensi/ref-rekening-kegiatan/delete?id_kabupaten=<?=$model->id_kabupaten?>&kode=<?=$model->kode?>', urlBack:'/referensi/ref-rekening-kegiatan/index?id_kabupaten=<?=$model->id_kabupaten?>&effect=rubberBand', typeSend:'post'})" class="label label-danger">Hapus</a>
        <?php endif; ?>  
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kabupaten',
            'kode',
            'tipe',
            'uraian',
            'keterangan',
        ],
    ]) ?>

</div>
