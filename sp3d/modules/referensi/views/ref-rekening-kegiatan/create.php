<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekeningKegiatan */

$this->title = 'Buat Referensi Rekening Kegiatan Baru';
$this->params['breadcrumbs'][] = ['label' => 'Ref Rekening Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rekening-kegiatan-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'id_kabupaten' => $id_kabupaten,
    ]) ?>

</div>
