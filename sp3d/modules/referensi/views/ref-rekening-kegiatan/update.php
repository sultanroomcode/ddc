<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekeningBelanja */

$this->title = 'Update Referensi Rekening Kegiatan: ' . $model->uraian;
?>
<div class="ref-rekening-kegiatan-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'id_kabupaten' => $id_kabupaten,
    ]) ?>

</div>
