<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRek4 */

$this->title = $model->Obyek;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rek4s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek4-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Obyek], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Obyek], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rek4/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Rek4</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-rek4/delete?id=<?= $model->id?>', urlBack:'ref-rek4/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Rek4</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Jenis',
            'Obyek',
            'Nama_Obyek',
            'Peraturan',
        ],
    ]) ?>

</div>
