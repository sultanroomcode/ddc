<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRek4 */

$this->title = 'Update Ref Rek4: ' . $model->Obyek;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rek4s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Obyek, 'url' => ['view', 'id' => $model->Obyek]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-rek4-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
