<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Rek4s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek4-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rek4/create'})" class="btn btn-warning">Buat Ref Rek4</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Jenis',
            'Obyek',
            'Nama_Obyek',
            'Peraturan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
