<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefBelOperasional */

$this->title = $model->ID_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ref Bel Operasionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bel-operasional-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_Keg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_Keg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-bel-operasional/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Bel Operasional</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-bel-operasional/delete?id=<?= $model->id?>', urlBack:'ref-bel-operasional/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Bel Operasional</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Keg',
        ],
    ]) ?>

</div>
