<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Bel Operasionals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bel-operasional-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-bel-operasional/create'})" class="btn btn-warning">Buat Ref Bel Operasional</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_Keg',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
