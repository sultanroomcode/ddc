<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefBelOperasional */

$this->title = 'Update Ref Bel Operasional: ' . $model->ID_Keg;
$this->params['breadcrumbs'][] = ['label' => 'Ref Bel Operasionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_Keg, 'url' => ['view', 'id' => $model->ID_Keg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-bel-operasional-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
