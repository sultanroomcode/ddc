<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefNeracaClose */

$this->title = 'Update Ref Neraca Close: ' . $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Neraca Closes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Rincian, 'url' => ['view', 'Kd_Rincian' => $model->Kd_Rincian, 'Kelompok' => $model->Kelompok]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-neraca-close-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
