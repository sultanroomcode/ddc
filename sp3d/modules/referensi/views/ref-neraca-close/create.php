<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefNeracaClose */

$this->title = 'Create Ref Neraca Close';
$this->params['breadcrumbs'][] = ['label' => 'Ref Neraca Closes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-neraca-close-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
