<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefNeracaClose */

$this->title = $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Neraca Closes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-neraca-close-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Kd_Rincian' => $model->Kd_Rincian, 'Kelompok' => $model->Kelompok], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Kd_Rincian' => $model->Kd_Rincian, 'Kelompok' => $model->Kelompok], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-neraca-close/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Neraca Close</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-neraca-close/delete?id=<?= $model->id?>', urlBack:'ref-neraca-close/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Neraca Close</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Rincian',
            'Kelompok',
        ],
    ]) ?>

</div>
