<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Neraca Closes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-neraca-close-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-neraca-close/create'})" class="btn btn-warning">Buat Ref Neraca Close</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Rincian',
            'Kelompok',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
