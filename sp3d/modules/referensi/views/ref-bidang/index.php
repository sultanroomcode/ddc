<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
?>    
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Referensi Bidang</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
    <!-- post text -->
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/default/all'})" class="btn btn-sm btn-warning">Kembali</a>
    <?php if(Mimin::checkRoute('referensi/ref-bidang/create')): ?>
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-bidang/create'})" class="btn btn-sm btn-warning">Tambah Referensi</a><hr>
    <?php endif; ?>
    
    <table id="ref-bidang-table" class="display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>Kode</th>               
                <th>Nama</th>

                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kode</th>               
                <th>Nama</th>

                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($dataProvider->all() as $v){ ?>
            <tr>
                <td><?= $v->kode ?></td>
                <td><?= $v->nama ?></td>

                <td>
                    <?php if(Mimin::checkRoute('referensi/ref-bidang/update')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-bidang/update?kode=<?=$v->kode?>'})" class="label label-warning">Update</a>
                    <?php endif; ?>

                    <?php if(Mimin::checkRoute('referensi/ref-bidang/delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus Data Referensi Bidang?', urlSend:'/referensi/ref-bidang/delete?kode=<?=$v->kode?>', urlBack:'/referensi/ref-bidang/index&effect=rubberBand', typeSend:'post'})" class="label label-danger">Hapus</a>
                    <?php endif; ?>   
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
    $('#ref-bidang-table').DataTable();
JS;

$this->registerJs($scripts);