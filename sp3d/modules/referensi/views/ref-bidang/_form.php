<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefBidang */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name RefBidang */
/* @var $generator string helper base name with camel to id Inflector ref-bidang */
/* @var $generator string helper base name with camel to word Inflector Ref Bidang */
if($model->isNewRecord){
    $model->setNewNumber();
}
?>

<div class="ref-bidang-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-bidang/index'})" class="btn btn-warning">List Ref Bidang</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'kode')->textInput(['readonly' => true, 'maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : '/referensi/ref-bidang/index'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);