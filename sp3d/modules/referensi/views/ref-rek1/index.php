<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Rek1s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek1-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rek1/create'})" class="btn btn-warning">Buat Ref Rek1</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Akun',
            'Nama_Akun',
            'NoLap',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
