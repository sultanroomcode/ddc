<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRek1 */

$this->title = 'Update Ref Rek1: ' . $model->Akun;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rek1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Akun, 'url' => ['view', 'id' => $model->Akun]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-rek1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
