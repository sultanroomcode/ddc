<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRek1 */

$this->title = $model->Akun;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rek1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Akun], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Akun], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rek1/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Rek1</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-rek1/delete?id=<?= $model->id?>', urlBack:'ref-rek1/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Rek1</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Akun',
            'Nama_Akun',
            'NoLap',
        ],
    ]) ?>

</div>
