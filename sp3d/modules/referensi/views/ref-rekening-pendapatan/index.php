<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
?>    
<!-- Box Comment -->
<div class="box box-widget animated <?=($effect)?$effect:'slideInRight'?>">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Referensi Rekening Pendapatan</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
    <!-- post text -->
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/default/all'})" class="btn btn-sm btn-warning">Kembali</a>
    <?php if(Mimin::checkRoute('referensi/ref-rekening-pendapatan/create')): ?>
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening-pendapatan/create?id_kabupaten=<?=$id?>'})" class="btn btn-sm btn-warning">Tambah Referensi</a><hr>
    <?php endif; ?>
    
    <table id="ref-rekening-pendapatan-table" class="display table table-striped cell-border table-bordered" width="100%">
        <thead>
            <tr>
                <th>Kode</th>               
                <th>Tipe</th>
                <th>Uraian</th>
                <th>Keterangan</th>

                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kode</th>               
                <th>Tipe</th>
                <th>Uraian</th>
                <th>Keterangan</th>

                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($dataProvider->all() as $v){ ?>
            <tr>
                <td><?= $v->kode ?></td>
                <td><?= $v->tipe ?></td>
                <td><?= $v->uraian ?></td>
                <td><?= $v->keterangan ?></td>
                
                <td>
                    <?php if(Mimin::checkRoute('referensi/ref-rekening-pendapatan/update')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening-pendapatan/update?id_kabupaten=<?=$id?>&kode=<?=$v->kode?>'})" class="label label-warning">Update</a>
                    <?php endif; ?>

                    <?php if(Mimin::checkRoute('referensi/ref-rekening-pendapatan/view')): ?>
                    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening-pendapatan/view?id_kabupaten=<?=$id?>&kode=<?=$v->kode?>'})" class="label label-warning">View</a>
                    <?php endif; ?>

                    <?php if(Mimin::checkRoute('referensi/ref-rekening-pendapatan/delete')): ?>
                    <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus Data Kegiatan Desa?', urlSend:'/referensi/ref-rekening-pendapatan/delete?id_kabupaten=<?=$id?>&kode=<?=$v->kode?>', urlBack:'/referensi/ref-rekening-pendapatan/index?id_kabupaten=<?=$id?>&effect=rubberBand', typeSend:'post'})" class="label label-danger">Hapus</a>
                    <?php endif; ?>   
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
    $('#ref-rekening-pendapatan-table').DataTable({
        dom: "<'row'<'col-sm-2'l><'col-sm-4'f><'col-sm-6'p>>" +
         "<'row'<'col-sm-12'tr>>" +
         "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    });
JS;

$this->registerJs($scripts);