<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekeningPendapatan */

$this->title = 'Buat Referensi Rekening Pendapatan Baru';
$this->params['breadcrumbs'][] = ['label' => 'Ref Rekening Pendapatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rekening-pendapatan-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'id_kabupaten' => $id_kabupaten,
    ]) ?>

</div>
