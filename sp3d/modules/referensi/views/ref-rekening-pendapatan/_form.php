<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekeningPendapatan */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name RefRekeningPendapatan */
/* @var $generator string helper base name with camel to id Inflector ref-rekening-pendapatan */
/* @var $generator string helper base name with camel to word Inflector Ref Rekening Pendapatan */
if($model->isNewRecord){
    $model->id_kabupaten = $id_kabupaten;//from update, and create.php
}
?>

<div class="ref-rekening-pendapatan-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'/referensi/ref-rekening-pendapatan/index'})" class="btn btn-warning">List Ref Rekening Pendapatan</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'id_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'tipe')->dropdownList(['1' =>'Bidang', '2' => 'Program', '3' => 'Kegiatan', '4' => 'Rincian Kegiatan']) ?>
            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : '/referensi/ref-rekening-pendapatan/index'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);