<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-setting-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-setting/create'})" class="btn btn-warning">Buat Ref Setting</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Kd_Prov',
            'Kd_Kab',
            'AutoSPD',
            'AutoSPP',
            // 'AutoSPM',
            // 'AutoSP2D',
            // 'AutoTBP',
            // 'AutoSTS',
            // 'AutoSPJ',
            // 'AutoBukti',
            // 'AutoSSP',
            // 'FiturSBU',
            // 'AppVer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
