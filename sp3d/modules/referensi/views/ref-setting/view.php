<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSetting */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Ref Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-setting-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-setting/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Setting</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-setting/delete?id=<?= $model->id?>', urlBack:'ref-setting/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Setting</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'Kd_Prov',
            'Kd_Kab',
            'AutoSPD',
            'AutoSPP',
            'AutoSPM',
            'AutoSP2D',
            'AutoTBP',
            'AutoSTS',
            'AutoSPJ',
            'AutoBukti',
            'AutoSSP',
            'FiturSBU',
            'AppVer',
        ],
    ]) ?>

</div>
