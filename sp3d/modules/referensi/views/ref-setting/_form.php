<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSetting */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name RefSetting */
/* @var $generator string helper base name with camel to id Inflector ref-setting */
/* @var $generator string helper base name with camel to word Inflector Ref Setting */
?>

<div class="ref-setting-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ref-setting/index'})" class="btn btn-warning">List Ref Setting</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Prov')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Kab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'AutoSPD')->textInput() ?>

    <?= $form->field($model, 'AutoSPP')->textInput() ?>

    <?= $form->field($model, 'AutoSPM')->textInput() ?>

    <?= $form->field($model, 'AutoSP2D')->textInput() ?>

    <?= $form->field($model, 'AutoTBP')->textInput() ?>

    <?= $form->field($model, 'AutoSTS')->textInput() ?>

    <?= $form->field($model, 'AutoSPJ')->textInput() ?>

    <?= $form->field($model, 'AutoBukti')->textInput() ?>

    <?= $form->field($model, 'AutoSSP')->textInput() ?>

    <?= $form->field($model, 'FiturSBU')->textInput() ?>

    <?= $form->field($model, 'AppVer')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ref-setting/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ref Setting')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ref-setting/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);