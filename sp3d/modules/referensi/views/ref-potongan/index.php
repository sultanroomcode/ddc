<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Potongans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-potongan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-potongan/create'})" class="btn btn-warning">Buat Ref Potongan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Rincian',
            'Kd_Potongan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
