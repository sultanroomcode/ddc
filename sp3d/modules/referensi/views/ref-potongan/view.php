<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefPotongan */

$this->title = $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Potongans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-potongan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_Potongan' => $model->Kd_Potongan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_Potongan' => $model->Kd_Potongan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-potongan/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Potongan</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-potongan/delete?id=<?= $model->id?>', urlBack:'ref-potongan/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Potongan</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Rincian',
            'Kd_Potongan',
        ],
    ]) ?>

</div>
