<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefPotongan */

$this->title = 'Update Ref Potongan: ' . $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Potongans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Rincian, 'url' => ['view', 'Kd_Rincian' => $model->Kd_Rincian, 'Kd_Potongan' => $model->Kd_Potongan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-potongan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
