<?php
use sp3d\models\referensi\RefRek1;
use sp3d\models\referensi\RefRek2;
use sp3d\models\referensi\RefRek3;
use sp3d\models\referensi\RefRek4;
$showtab = 'true';
?>
<br>
<div class="row">
    <div class="col-md-12">
        Kode Rekening : <?= $rek ?><br>
        <?php
        $userkab = Yii::$app->user->identity->id;
        switch ($rek) {
            case '2':
                $model = RefRek2::find()->where(['kd_kabupaten' => $userkab]);
                if($model->count() > 0){
                    echo '<table class="datatab row-border compact">';
                    echo '<thead><tr><td>Akun</td><td>Kelompok</td><td>Nama Kelompok</td></tr></thead>';
                    echo '<tfoot><tr><td>Akun</td><td>Kelompok</td><td>Nama Kelompok</td></tr></tfoot>';
                    echo '<tbody>';
                    foreach($model->all() as $v):
                        echo '<tr><td>'.$v->Akun.'</td><td>'.$v->Kelompok.'</td><td>'.$v->Nama_Kelompok.'</td></tr>';
                    endforeach;
                    echo '</tbody>';
                    echo '<table>';
                } else {
                    $showtab = 'false';
                    echo '<a href="javascript:void(0)" onclick="goLoad({elm:\'#table-rekening\', url:\'/referensi/kode-referensi/import-kode?rek=2\'})" class="btn btn-success">Import Kode Rekening 2</a>';
                }
            break;
            case '3':
                $model = RefRek3::find()->where(['kd_kabupaten' => $userkab]);
                if($model->count() > 0){
                    echo '<table class="datatab row-border compact">';
                    echo '<thead><tr><td>Kelompok</td><td>Jenis</td><td>Nama Jenis</td></tr></thead>';
                    echo '<tfoot><tr><td>Kelompok</td><td>Jenis</td><td>Nama Jenis</td></tr></tfoot>';
                    echo '<tbody>';
                    foreach($model->all() as $v):
                        echo '<tr><td>'.$v->Kelompok.'</td><td>'.$v->Jenis.'</td><td>'.$v->Nama_Jenis.'</td></tr>';
                    endforeach;
                    echo '</tbody>';
                    echo '<table>';
                } else {
                    $showtab = 'false';
                    echo '<a href="javascript:void(0)" onclick="goLoad({elm:\'#table-rekening\', url:\'/referensi/kode-referensi/import-kode?rek=3\'})" class="btn btn-success">Import Kode Rekening 3</a>';
                }
            break;
            case '4':
                $model = RefRek4::find()->where(['kd_kabupaten' => $userkab]);
                if($model->count() > 0){
                    echo '<table class="datatab row-border compact">';
                    echo '<thead><tr><td>Jenis</td><td>Obyek</td><td>Nama Obyek</td></tr></thead>';
                    echo '<tfoot><tr><td>Jenis</td><td>Obyek</td><td>Nama Obyek</td></tr></tfoot>';
                    echo '<tbody>';
                    foreach($model->all() as $v):
                        echo '<tr><td>'.$v->Jenis.'</td><td>'.$v->Obyek.'</td><td>'.$v->Nama_Obyek.'</td></tr>';
                    endforeach;
                    echo '</tbody>';
                    echo '<table>';
                } else {
                    $showtab = 'false';
                    echo '<a href="javascript:void(0)" onclick="goLoad({elm:\'#table-rekening\', url:\'/referensi/kode-referensi/import-kode?rek=4\'})" class="btn btn-success">Import Kode Rekening 4</a>';
                }
            break;
            default:
                $model = RefRek1::find()->where(['kd_kabupaten' => $userkab]);
                if($model->count() > 0){
                    echo '<table class="datatab row-border compact">';
                    echo '<thead><tr><td>Akun</td><td>Nama Akun</td></tr></thead>';
                    echo '<tfoot><tr><td>Akun</td><td>Nama Akun</td></tr></tfoot>';
                    echo '<tbody>';
                    foreach($model->all() as $v):
                        echo '<tr><td>'.$v->Akun.'</td><td>'.$v->Nama_Akun.'</td></tr>';
                    endforeach;
                    echo '</tbody>';
                    echo '<table>';
                } else {
                    $showtab = 'false';
                    echo '<a href="javascript:void(0)" onclick="goLoad({elm:\'#table-rekening\', url:\'/referensi/kode-referensi/import-kode?rek=1\'})" class="btn btn-success">Import Kode Rekening 1</a>';
                }
            break;
        }
        ?>
    </div>
</div>
<?php

$scripts =<<<JS
var showtab = '{$showtab}';
    if(showtab == 'true'){
        $('.datatab').DataTable();    
    }
JS;

$this->registerJs($scripts);