<?php
use sp3d\models\referensi\RefRek1;
use sp3d\models\referensi\RefRek2;
use sp3d\models\referensi\RefRek3;
use sp3d\models\referensi\RefRek4;
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Kode Referensi Kabupaten</h2>
            <div class="p-10">
                <?php
                $model1 = RefRek1::find()->where(['kd_kabupaten' => Yii::$app->user->identity->id]);
                ?>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#table-rekening', url:'/referensi/kode-referensi/check-kode?rek=1'})" class="btn btn-success">Rekening 1 : <?= $model1->count() ?> Buah</a> 

                <?php
                $model2 = RefRek2::find()->where(['kd_kabupaten' => Yii::$app->user->identity->id]);
                ?>

                <a href="javascript:void(0)" onclick="goLoad({elm:'#table-rekening', url:'/referensi/kode-referensi/check-kode?rek=2'})" class="btn btn-success">Rekening 2 : <?= $model2->count() ?> Buah</a>

                <?php
                $model3 = RefRek3::find()->where(['kd_kabupaten' => Yii::$app->user->identity->id]);
                ?>

                <a href="javascript:void(0)" onclick="goLoad({elm:'#table-rekening', url:'/referensi/kode-referensi/check-kode?rek=3'})" class="btn btn-success">Rekening 3 : <?= $model3->count() ?> Buah</a>

                <?php
                $model4 = RefRek4::find()->where(['kd_kabupaten' => Yii::$app->user->identity->id]);
                ?>
                <a href="javascript:void(0)" onclick="goLoad({elm:'#table-rekening', url:'/referensi/kode-referensi/check-kode?rek=4'})" class="btn btn-success">Rekening 4 : <?= $model4->count() ?> Buah</a>


                <div id="table-rekening"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>