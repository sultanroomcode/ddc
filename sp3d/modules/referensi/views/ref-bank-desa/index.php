<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Bank Desas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bank-desa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-bank-desa/create'})" class="btn btn-warning">Buat Ref Bank Desa</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tahun',
            'Kd_Desa',
            'Kd_Rincian',
            'NoRek_Bank',
            'Nama_Bank',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
