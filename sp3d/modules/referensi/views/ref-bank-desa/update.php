<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefBankDesa */

$this->title = 'Update Ref Bank Desa: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ref Bank Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Desa' => $model->Kd_Desa, 'Kd_Rincian' => $model->Kd_Rincian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-bank-desa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
