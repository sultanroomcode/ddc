<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefPerangkat */

$this->title = 'Create Ref Perangkat';
$this->params['breadcrumbs'][] = ['label' => 'Ref Perangkats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-perangkat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
