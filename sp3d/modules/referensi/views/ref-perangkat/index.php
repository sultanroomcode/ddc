<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Perangkats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-perangkat-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-perangkat/create'})" class="btn btn-warning">Buat Ref Perangkat</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kode',
            'Nama_Perangkat',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
