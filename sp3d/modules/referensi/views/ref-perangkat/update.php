<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefPerangkat */

$this->title = 'Update Ref Perangkat: ' . $model->Kode;
$this->params['breadcrumbs'][] = ['label' => 'Ref Perangkats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kode, 'url' => ['view', 'id' => $model->Kode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-perangkat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
