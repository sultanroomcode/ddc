<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefPerangkat */

$this->title = $model->Kode;
$this->params['breadcrumbs'][] = ['label' => 'Ref Perangkats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-perangkat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kode], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kode], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-perangkat/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Perangkat</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-perangkat/delete?id=<?= $model->id?>', urlBack:'ref-perangkat/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Perangkat</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kode',
            'Nama_Perangkat',
        ],
    ]) ?>

</div>
