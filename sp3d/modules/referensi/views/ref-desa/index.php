<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Desas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-desa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-desa/create'})" class="btn btn-warning">Buat Ref Desa</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Kec',
            'Kd_Desa',
            'Nama_Desa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
