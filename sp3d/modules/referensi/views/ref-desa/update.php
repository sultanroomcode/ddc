<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefDesa */

$this->title = 'Update Ref Desa: ' . $model->Kd_Desa;
$this->params['breadcrumbs'][] = ['label' => 'Ref Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Desa, 'url' => ['view', 'id' => $model->Kd_Desa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-desa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
