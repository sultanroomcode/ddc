<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefDesa */

$this->title = $model->Kd_Desa;
$this->params['breadcrumbs'][] = ['label' => 'Ref Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-desa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kd_Desa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kd_Desa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-desa/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Desa</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-desa/delete?id=<?= $model->id?>', urlBack:'ref-desa/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Desa</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Kec',
            'Kd_Desa',
            'Nama_Desa',
        ],
    ]) ?>

</div>
