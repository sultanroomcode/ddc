<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRekeningBelanja */

$this->title = 'Buat Referensi Rekening Belanja Baru';
$this->params['breadcrumbs'][] = ['label' => 'Ref Rekening Belanjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rekening-belanja-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'id_kabupaten' => $id_kabupaten,
    ]) ?>

</div>
