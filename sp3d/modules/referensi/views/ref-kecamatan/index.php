<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Kecamatans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kecamatan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-kecamatan/create'})" class="btn btn-warning">Buat Ref Kecamatan</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Kec',
            'Nama_Kecamatan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
