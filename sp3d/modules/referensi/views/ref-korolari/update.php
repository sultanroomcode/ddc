<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefKorolari */

$this->title = 'Update Ref Korolari: ' . $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Korolaris', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Rincian, 'url' => ['view', 'id' => $model->Kd_Rincian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-korolari-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
