<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefKorolari */

$this->title = $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Korolaris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-korolari-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kd_Rincian], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kd_Rincian], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-korolari/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Korolari</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-korolari/delete?id=<?= $model->id?>', urlBack:'ref-korolari/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Korolari</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Rincian',
            'Kd_RekDB',
            'Kd_RekKD',
            'Jenis',
        ],
    ]) ?>

</div>
