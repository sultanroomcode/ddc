<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Korolaris';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-korolari-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-korolari/create'})" class="btn btn-warning">Buat Ref Korolari</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Rincian',
            'Kd_RekDB',
            'Kd_RekKD',
            'Jenis',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
