<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefBunga */

$this->title = 'Update Ref Bunga: ' . $model->Kd_Bunga;
$this->params['breadcrumbs'][] = ['label' => 'Ref Bungas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Bunga, 'url' => ['view', 'id' => $model->Kd_Bunga]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-bunga-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
