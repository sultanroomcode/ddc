<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Bungas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bunga-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-bunga/create'})" class="btn btn-warning">Buat Ref Bunga</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Bunga',
            'Kd_Admin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
