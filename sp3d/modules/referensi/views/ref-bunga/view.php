<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefBunga */

$this->title = $model->Kd_Bunga;
$this->params['breadcrumbs'][] = ['label' => 'Ref Bungas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bunga-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kd_Bunga], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kd_Bunga], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-bunga/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Bunga</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-bunga/delete?id=<?= $model->id?>', urlBack:'ref-bunga/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Bunga</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Bunga',
            'Kd_Admin',
        ],
    ]) ?>

</div>
