<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefBunga */

$this->title = 'Create Ref Bunga';
$this->params['breadcrumbs'][] = ['label' => 'Ref Bungas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bunga-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
