<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRek2 */

$this->title = $model->Kelompok;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rek2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek2-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kelompok], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kelompok], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rek2/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Rek2</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-rek2/delete?id=<?= $model->id?>', urlBack:'ref-rek2/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Rek2</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Akun',
            'Kelompok',
            'Nama_Kelompok',
        ],
    ]) ?>

</div>
