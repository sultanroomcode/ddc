<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRek2 */

$this->title = 'Update Ref Rek2: ' . $model->Kelompok;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rek2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kelompok, 'url' => ['view', 'id' => $model->Kelompok]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-rek2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
