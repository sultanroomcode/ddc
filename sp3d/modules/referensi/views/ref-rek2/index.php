<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Rek2s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek2-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rek2/create'})" class="btn btn-warning">Buat Ref Rek2</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Akun',
            'Kelompok',
            'Nama_Kelompok',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
