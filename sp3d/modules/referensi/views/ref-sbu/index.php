<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Sbus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-sbu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-sbu/create'})" class="btn btn-warning">Buat Ref Sbu</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kd_Rincian',
            'Kode_SBU',
            'NoUrut_SBU',
            'Nama_SBU',
            'Nilai',
            // 'Satuan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
