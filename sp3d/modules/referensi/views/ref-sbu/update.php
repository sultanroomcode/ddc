<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSBU */

$this->title = 'Update Ref Sbu: ' . $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Sbus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kd_Rincian, 'url' => ['view', 'Kd_Rincian' => $model->Kd_Rincian, 'Kode_SBU' => $model->Kode_SBU]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-sbu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
