<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSBU */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name RefSBU */
/* @var $generator string helper base name with camel to id Inflector ref-sbu */
/* @var $generator string helper base name with camel to word Inflector Ref Sbu */
?>

<div class="ref-sbu-form">
    <a href="javascript:void(0)" onclick="goLoad({url:'ref-sbu/index'})" class="btn btn-warning">List Ref Sbu</a>

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Kd_Rincian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kode_SBU')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NoUrut_SBU')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama_SBU')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nilai')->textInput() ?>

    <?= $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : 'ref-sbu/index-npm?id={$data->xxx}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});

//if-use-upload-file
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    var \$data = new FormData($('form#Ref Sbu')[0]);

    $.ajax({
        url : \$form.attr('action'),
        data : \$data,
        type:'post',
        processData: false,
        contentType: false,
        success:function(res){
            if(res == 1){
                $(\$form).trigger('reset');
                //do next
                goLoad({url : 'ref-sbu/index'});
            } else {
                console.log('Fail but not error');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Fail but not error'+ textStatus); 
        }
    });

    return false;
});
JS;

$this->registerJs($script);