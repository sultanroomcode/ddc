<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSBU */

$this->title = $model->Kd_Rincian;
$this->params['breadcrumbs'][] = ['label' => 'Ref Sbus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-sbu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Kd_Rincian' => $model->Kd_Rincian, 'Kode_SBU' => $model->Kode_SBU], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Kd_Rincian' => $model->Kd_Rincian, 'Kode_SBU' => $model->Kode_SBU], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-sbu/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Sbu</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-sbu/delete?id=<?= $model->id?>', urlBack:'ref-sbu/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Sbu</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kd_Rincian',
            'Kode_SBU',
            'NoUrut_SBU',
            'Nama_SBU',
            'Nilai',
            'Satuan',
        ],
    ]) ?>

</div>
