<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSBU */

$this->title = 'Create Ref Sbu';
$this->params['breadcrumbs'][] = ['label' => 'Ref Sbus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-sbu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
