<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSumber */

$this->title = 'Update Ref Sumber: ' . $model->Kode;
$this->params['breadcrumbs'][] = ['label' => 'Ref Sumbers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kode, 'url' => ['view', 'id' => $model->Kode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-sumber-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
