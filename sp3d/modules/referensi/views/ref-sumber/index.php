<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Sumbers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-sumber-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-sumber/create'})" class="btn btn-warning">Buat Ref Sumber</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kode',
            'Nama_Sumber',
            'Urut',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
