<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefSumber */

$this->title = $model->Kode;
$this->params['breadcrumbs'][] = ['label' => 'Ref Sumbers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-sumber-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kode], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kode], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-sumber/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Sumber</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-sumber/delete?id=<?= $model->id?>', urlBack:'ref-sumber/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Sumber</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kode',
            'Nama_Sumber',
            'Urut',
        ],
    ]) ?>

</div>
