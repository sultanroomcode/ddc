<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\referensi\RefRek3 */

$this->title = $model->Jenis;
$this->params['breadcrumbs'][] = ['label' => 'Ref Rek3s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek3-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Jenis], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Jenis], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        Ajax
        <a href="javascript:void(0)" onclick="goLoad({url:'ref-rek3/update?id=<?= $model->id?>'})" class="btn btn-warning">Update Ref Rek3</a>
        <a href="javascript:void(0)" onclick="goSendLoad({msg:'Hapus ?', urlSend:'ref-rek3/delete?id=<?= $model->id?>', urlBack:'ref-rek3/index', typeSend:'post'})" class="btn btn-warning">Hapus Ref Rek3</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kelompok',
            'Jenis',
            'Nama_Jenis',
            'Formula',
        ],
    ]) ?>

</div>
