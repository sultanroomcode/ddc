<?php
namespace sp3d\modules\referensi;

class Referensi extends \yii\base\Module
{
    public $controllerNamespace = 'sp3d\modules\referensi\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
