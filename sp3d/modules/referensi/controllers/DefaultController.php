<?php

namespace frontend\modules\referensi\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAll()
    {
        return $this->renderAjax('all');
    }
}
