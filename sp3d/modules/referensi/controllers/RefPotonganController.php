<?php

namespace frontend\modules\referensi\controllers;

use Yii;
use frontend\models\referensi\RefPotongan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefPotonganController implements the CRUD actions for RefPotongan model.
 */
class RefPotonganController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefPotongan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefPotongan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefPotongan model.
     * @param string $Kd_Rincian
     * @param string $Kd_Potongan
     * @return mixed
     */
    public function actionView($Kd_Rincian, $Kd_Potongan)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Kd_Rincian, $Kd_Potongan),
        ]);
    }

    /**
     * Creates a new RefPotongan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefPotongan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefPotongan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Kd_Rincian
     * @param string $Kd_Potongan
     * @return mixed
     */
    public function actionUpdate($Kd_Rincian, $Kd_Potongan)
    {
        $model = $this->findModel($Kd_Rincian, $Kd_Potongan);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefPotongan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Kd_Rincian
     * @param string $Kd_Potongan
     * @return mixed
     */
    public function actionDelete($Kd_Rincian, $Kd_Potongan)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the RefPotongan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Kd_Rincian
     * @param string $Kd_Potongan
     * @return RefPotongan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Kd_Rincian, $Kd_Potongan)
    {
        if (($model = RefPotongan::findOne(['Kd_Rincian' => $Kd_Rincian, 'Kd_Potongan' => $Kd_Potongan])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
