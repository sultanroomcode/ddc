<?php

namespace frontend\modules\referensi\controllers;

use Yii;
use frontend\models\referensi\RefBankDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefBankDesaController implements the CRUD actions for RefBankDesa model.
 */
class RefBankDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefBankDesa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefBankDesa::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefBankDesa model.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Desa, $Kd_Rincian)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Tahun, $Kd_Desa, $Kd_Rincian),
        ]);
    }

    /**
     * Creates a new RefBankDesa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefBankDesa();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefBankDesa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Desa, $Kd_Rincian)
    {
        $model = $this->findModel($Tahun, $Kd_Desa, $Kd_Rincian);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefBankDesa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Rincian
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Desa, $Kd_Rincian)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the RefBankDesa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param string $Kd_Desa
     * @param string $Kd_Rincian
     * @return RefBankDesa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Desa, $Kd_Rincian)
    {
        if (($model = RefBankDesa::findOne(['Tahun' => $Tahun, 'Kd_Desa' => $Kd_Desa, 'Kd_Rincian' => $Kd_Rincian])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
