<?php

namespace frontend\modules\referensi\controllers;

use Yii;
use frontend\models\referensi\RefRekening;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefRekeningController implements the CRUD actions for RefRekening model.
 */
class RefRekeningController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefRekening models.
     * @return mixed
     */
    public function actionIndex($effect ='slideInRight')
    {
        $dataProvider = RefRekening::find()->where(['id_kabupaten' => '35.23.']);//tuban

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
            'effect' => $effect
        ]);
    }

    /**
     * Displays a single RefRekening model.
     * @param string $id_kabupaten
     * @param string $kode
     * @return mixed
     */
    public function actionView($id_kabupaten, $kode)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id_kabupaten, $kode),
        ]);
    }

    /**
     * Creates a new RefRekening model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefRekening();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefRekening model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id_kabupaten
     * @param string $kode
     * @return mixed
     */
    public function actionUpdate($id_kabupaten, $kode)
    {
        $model = $this->findModel($id_kabupaten, $kode);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefRekening model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id_kabupaten
     * @param string $kode
     * @return mixed
     */
    public function actionDelete($id_kabupaten, $kode)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the RefRekening model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id_kabupaten
     * @param string $kode
     * @return RefRekening the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_kabupaten, $kode)
    {
        if (($model = RefRekening::findOne(['id_kabupaten' => $id_kabupaten, 'kode' => $kode])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
