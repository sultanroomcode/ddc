<?php

namespace frontend\modules\referensi\controllers;

use Yii;
use frontend\models\referensi\RefRekeningKegiatan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefRekeningKegiatanController implements the CRUD actions for RefRekeningKegiatan model.
 */
class RefRekeningKegiatanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefRekeningKegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $id_kabupaten = '35.23.';
        $dataProvider = RefRekeningKegiatan::find()->where(['id_kabupaten' => $id_kabupaten]);

        return $this->renderAjax('index', [
            'effect' => null,
            'id' => $id_kabupaten,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefRekeningKegiatan model.
     * @param string $id_kabupaten
     * @param string $kode
     * @return mixed
     */
    public function actionView($id_kabupaten, $kode)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id_kabupaten, $kode),
        ]);
    }

    /**
     * Creates a new RefRekeningKegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_kabupaten)
    {
        $model = new RefRekeningKegiatan();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'effect' => null,
                'id_kabupaten' => $id_kabupaten,
            ]);
        }
    }

    /**
     * Updates an existing RefRekeningKegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id_kabupaten
     * @param string $kode
     * @return mixed
     */
    public function actionUpdate($id_kabupaten, $kode)
    {
        $model = $this->findModel($id_kabupaten, $kode);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'effect' => null,
                'id_kabupaten' => $id_kabupaten,
            ]);
        }
    }

    /**
     * Deletes an existing RefRekeningKegiatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id_kabupaten
     * @param string $kode
     * @return mixed
     */
    public function actionDelete($id_kabupaten, $kode)
    {
        if($this->findModel($id_kabupaten, $kode)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the RefRekeningKegiatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id_kabupaten
     * @param string $kode
     * @return RefRekeningKegiatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_kabupaten, $kode)
    {
        if (($model = RefRekeningKegiatan::findOne(['id_kabupaten' => $id_kabupaten, 'kode' => $kode])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
