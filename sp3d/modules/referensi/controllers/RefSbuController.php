<?php

namespace frontend\modules\referensi\controllers;

use Yii;
use frontend\models\referensi\RefSBU;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefSbuController implements the CRUD actions for RefSBU model.
 */
class RefSbuController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefSBU models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefSBU::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefSBU model.
     * @param string $Kd_Rincian
     * @param string $Kode_SBU
     * @return mixed
     */
    public function actionView($Kd_Rincian, $Kode_SBU)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Kd_Rincian, $Kode_SBU),
        ]);
    }

    /**
     * Creates a new RefSBU model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefSBU();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefSBU model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Kd_Rincian
     * @param string $Kode_SBU
     * @return mixed
     */
    public function actionUpdate($Kd_Rincian, $Kode_SBU)
    {
        $model = $this->findModel($Kd_Rincian, $Kode_SBU);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefSBU model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Kd_Rincian
     * @param string $Kode_SBU
     * @return mixed
     */
    public function actionDelete($Kd_Rincian, $Kode_SBU)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the RefSBU model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Kd_Rincian
     * @param string $Kode_SBU
     * @return RefSBU the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Kd_Rincian, $Kode_SBU)
    {
        if (($model = RefSBU::findOne(['Kd_Rincian' => $Kd_Rincian, 'Kode_SBU' => $Kode_SBU])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
