<?php
namespace sp3d\modules\referensi\controllers;

use yii\web\Controller;

class KodeReferensiController extends Controller
{
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionKabupaten()
    {
        return $this->renderAjax('kabupaten');
    }

    public function actionCheckKode($rek=1)
    {
        return $this->renderAjax('check-kode', [
            'rek' => $rek
        ]);
    }

    public function actionAll()
    {
        return $this->renderAjax('all');
    }
}
