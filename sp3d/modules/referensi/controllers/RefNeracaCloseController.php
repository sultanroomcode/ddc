<?php

namespace frontend\modules\referensi\controllers;

use Yii;
use frontend\models\referensi\RefNeracaClose;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefNeracaCloseController implements the CRUD actions for RefNeracaClose model.
 */
class RefNeracaCloseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefNeracaClose models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefNeracaClose::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefNeracaClose model.
     * @param string $Kd_Rincian
     * @param string $Kelompok
     * @return mixed
     */
    public function actionView($Kd_Rincian, $Kelompok)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($Kd_Rincian, $Kelompok),
        ]);
    }

    /**
     * Creates a new RefNeracaClose model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefNeracaClose();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefNeracaClose model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Kd_Rincian
     * @param string $Kelompok
     * @return mixed
     */
    public function actionUpdate($Kd_Rincian, $Kelompok)
    {
        $model = $this->findModel($Kd_Rincian, $Kelompok);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefNeracaClose model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Kd_Rincian
     * @param string $Kelompok
     * @return mixed
     */
    public function actionDelete($Kd_Rincian, $Kelompok)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the RefNeracaClose model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Kd_Rincian
     * @param string $Kelompok
     * @return RefNeracaClose the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Kd_Rincian, $Kelompok)
    {
        if (($model = RefNeracaClose::findOne(['Kd_Rincian' => $Kd_Rincian, 'Kelompok' => $Kelompok])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
