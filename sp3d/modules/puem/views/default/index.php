<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">PUEM Store</h2>
            <div class="p-10" id="puem-area">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$urlBack = '/puem-editor/puem-store/all-store';
$scripts = <<<JS
	goLoad({elm:'#puem-area', url:'/puem-editor/puem-store/all-store'});
	
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#puem-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }

    function deleteDataDetail(o){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:o.url, msg: o.msg, elmChange: o.elm, urlBack:o.urlBack});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);