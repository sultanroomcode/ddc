<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreProductPhoto */

$this->title = 'Update Puem Store Product Photo: ' . $model->idp;
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Product Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idp, 'url' => ['view', 'idp' => $model->idp, 'idf' => $model->idf]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="puem-store-product-photo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
