<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreContact */
/* @var $form yii\widgets\ActiveForm */
$arrFormConfig = [
'id' => $model->formName(), 
'options' => ['enctype' => 'multipart/form-data'],
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord)
{
    $model->genNum();
}

$urlback = '/puem-editor/puem-store-product-photo/all-photo?idp='.$model->idp.'&kd_desa='.$model->arr_parent['kd_desa'].'&kd_toko='.$model->arr_parent['kd_toko'];
?>

<div class="puem-store-product-photo-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'idp')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'idf')->hiddenInput()->label(false) ?>
    <?php if($model->isNewRecord){ ?>
        <?= $form->field($model, 'tmp_src')->fileInput(['maxlength' => true]) ?>
    <?php } ?>

    <?= $form->field($model, 'caption')->textarea(['maxlength' => true]) ?>


    <progress id="prog" max="100" value="0" style="display:none;"></progress>
    <div id="percent"></div>

    <div class="progress">
      <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><b class="prog-text"></b>
        <span class="sr-only" class="prog-text"></span>
      </div>
    </div>

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#puem-store-product-photo-area\',url:\''.$urlback.'\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax

var bar = $('.progress-bar');
var percent = $('.prog-text');

$('form#{$model->formName()}').ajaxForm({
    beforeSend: function() {
        var percentVal = 0;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : '0%'}); 
        percent.html(percentVal+' %');
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    success: function() {
        var percentVal = 100;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    complete: function(xhr) {
        if(xhr.responseText == 1){
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#puem-store-product-photo-area', url : '{$urlback}'});
        } else {
            $.alert('Gagal menyimpan data');
        }
    }
}); 
JS;

$this->registerJs($script);