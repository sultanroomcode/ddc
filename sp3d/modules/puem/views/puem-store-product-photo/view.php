<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreProductPhoto */

$this->title = $model->idp;
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Product Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puem-store-product-photo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idp' => $model->idp, 'idf' => $model->idf], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idp' => $model->idp, 'idf' => $model->idf], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idp',
            'idf',
            'src',
            'caption',
            'created_by',
            'created_at',
        ],
    ]) ?>

</div>
