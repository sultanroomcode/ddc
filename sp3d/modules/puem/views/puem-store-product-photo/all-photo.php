<?php 
use yii\helpers\Url;
?>
<a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-store-product-photo-area', url:'/puem-editor/puem-store-product-photo/create?idp=<?= $data['idp']?>&kd_desa=<?=$data['kd_desa']?>&kd_toko=<?=$data['kd_toko'] ?>'})">Upload Foto Baru</a>

<div class="row">
<?php foreach($dataProvider->all() as $v): ?>
    <div class="col-md-3">
	    <img src="<?= Url::base().'/userfile/'.$data['kd_desa'].'/puem/'.$v->src ?>" class="img-responsive">
	    <br>
		<?= $v->caption ?>
	    <br>
	    <a href="javascript:void(0)" class="btn btn-block btn-success" onclick="goLoad({elm:'#puem-store-product-photo-area', url:'/puem-editor/puem-store-product-photo/update?idp=<?= $v->idp ?>&idf=<?= $v->idf ?>&kd_desa=<?=$data['kd_desa']?>&kd_toko=<?=$data['kd_toko'] ?>'})">Edit</a>
	    <a href="javascript:void(0)" class="btn btn-block btn-danger" onclick="deleteDataDetail({url:'/puem-editor/puem-store-product-photo/delete?idp=<?= $v->idp ?>&idf=<?= $v->idf ?>&kd_desa=<?= $data['kd_desa'] ?>', msg:'Berhasil hapus data', elm:'#puem-store-product-photo-area', urlBack:'/puem-editor/puem-store-product-photo/all-photo?idp=<?=$v->idp?>&kd_desa=<?= $data['kd_desa']?>&kd_toko=<?=$data['kd_toko'] ?>'})">Hapus</a>        
    </div>
<?php endforeach; ?>
</div>