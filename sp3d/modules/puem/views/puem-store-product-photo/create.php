<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreProductPhoto */

$this->title = 'Create Puem Store Product Photo';
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Product Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puem-store-product-photo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
