<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreContact */

$this->title = $model->idp;
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puem-store-contact-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idp' => $model->idp, 'idc' => $model->idc], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idp' => $model->idp, 'idc' => $model->idc], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idp',
            'idc',
            'type_contact',
            'text_contact',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
