<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreContact */
/* @var $form yii\widgets\ActiveForm */
$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$urlback = '/puem-editor/puem-store-contact/all-contact?kd_desa='.$model->kd_desa.'&kd_toko='.$model->kd_toko;
?>

<div class="puem-store-contact-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'kd_toko')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'type_contact')->dropdownList([
        'whatsapp' => 'Whatsapp (WA)',
        'facebook' => 'Facebook',
        'instagram' => 'Instagram',
        'telegram' => 'Telegram',
        'twitter' => 'Twitter',
    ]) ?>

    <?= $form->field($model, 'text_contact')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#puem-contact-area\',url:\''.$urlback.'\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#puem-contact-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);