<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreContact */

$this->title = 'Create Puem Store Contact';
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puem-store-contact-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
