<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreContact */

$this->title = 'Update Puem Store Contact: ' . $model->idc;
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idc, 'url' => ['view', 'idp' => $model->idc, 'idc' => $model->idc]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="puem-store-contact-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
