<a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-contact-area', url:'/puem-editor/puem-store-contact/create?kd_desa=<?= $data['kd_desa']?>&kd_toko=<?=$data['kd_toko'] ?>'})">Buat Kontak Baru</a>

<table class="table table-striped table-bordered">
<?php foreach($dataProvider->all() as $v): ?>
    <tr>
        <td><i class="fa fa-<?= $v->type_contact ?>"></i></td>
        <td><?= $v->text_contact ?></td>
        <td>
            <a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-contact-area', url:'/puem-editor/puem-store-contact/update?idc=<?= $v->idc?>'})">Edit</a>
            <a href="javascript:void(0)" class="btn btn-success" onclick="deleteData('/puem-editor/puem-store-contact/delete?idc=<?= $v->idc?>', 'Berhasil hapus data')">Hapus</a>            
        </td>
    </tr>
<?php endforeach; ?>
</table>