<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreProduct */

$this->title = 'Create Puem Store Product';
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puem-store-product-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
