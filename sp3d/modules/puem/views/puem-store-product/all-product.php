<?php 
use yii\helpers\Url;
?>
<a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-product-area', url:'/puem-editor/puem-store-product/create?kd_desa=<?= $data['kd_desa']?>&kd_toko=<?=$data['kd_toko'] ?>'})">Isi Produk Baru</a>

<table class="table table-striped table-bordered">
<?php foreach($dataProvider->all() as $v): ?>
    <tr>
        <td><img src="<?= Url::base().'/userfile/'.$v->kd_desa.'/puem/'.$v->foto_produk ?>" height="30"></td>
        <td><?= $v->nama ?></td>
        <td><?= $v->deskripsi ?></td>
        <td><?= $v->harga ?></td>
        <td><?= count($v->photo) ?></td>
    
        <td>
            <a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-product-area', url:'/puem-editor/puem-store-product/update?id=<?= $v->idp ?>'})">Edit</a>
            <a href="javascript:void(0)" class="btn btn-success" onclick="deleteDataDetail({url:'/puem-editor/puem-store-product/delete?id=<?= $v->idp ?>&kd_desa=<?= $data['kd_desa']?>', msg:'Berhasil hapus data', elm:'#puem-product-area', urlBack:'/puem-editor/puem-store-product/all-product?kd_desa=<?= $data['kd_desa']?>&kd_toko=<?=$data['kd_toko'] ?>'})">Hapus</a>
            <a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-product-area', url:'/puem-editor/puem-store-product/view?id=<?= $v->idp ?>'})">Lihat Data</a>
            
        </td>
    </tr>
<?php endforeach; ?>
</table>