<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreProduct */

$this->title = 'Update Puem Store Product: ' . $model->idp;
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idp, 'url' => ['view', 'id' => $model->idp]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="puem-store-product-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
