<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStoreProduct */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Puem Store Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$urlback = '/puem-editor/puem-store-product/all-product?kd_desa='.$model->kd_desa.'&kd_toko='.$model->kd_toko;
?>
<div class="puem-store-product-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-product-area', url:'<?=$urlback?>'})">Kembali</a>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idp',
            'kd_desa',
            'kd_toko',
            'nama',
            'harga',
            'ukuran',
            'kapasitas_produksi',
            'bahan_baku',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <div id="puem-store-product-photo-area"></div>
</div>
<?php
$scripts = <<<JS
    goLoad({elm:'#puem-store-product-photo-area', url:'/puem-editor/puem-store-product-photo/all-photo?idp={$model->idp}&kd_desa={$model->kd_desa}&kd_toko={$model->kd_toko}'});
JS;

$this->registerJs($scripts);