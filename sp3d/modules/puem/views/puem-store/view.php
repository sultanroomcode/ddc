<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStore */

$this->title = $model->pemilik;
$this->params['breadcrumbs'][] = ['label' => 'Puem Stores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puem-store-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-area', url:'/puem-editor/puem-store/update?kd_desa=<?= $model->kd_desa?>&kd_toko=<?=$model->kd_toko ?>'})">Edit</a>
        <a href="javascript:void(0)" class="btn btn-success" onclick="deleteData('/puem-editor/puem-store/delete?kd_desa=<?= $model->kd_desa?>&kd_toko=<?=$model->kd_toko ?>', 'Berhasil hapus data')">Hapus</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'kd_toko',
            'pemilik',
            'alamat',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <!-- kontak -->
    <h3>Kontak</h3>

    <div id="puem-contact-area"></div>

    <!-- produk -->
    <h3>Produk</h3>

    <div id="puem-product-area"></div>
</div>
<?php
$params_ = 'kd_desa='.$model->kd_desa.'&kd_toko='.$model->kd_toko;
$scripts = <<<JS
    goLoad({elm:'#puem-contact-area', url:'/puem-editor/puem-store-contact/all-contact?{$params_}'});
    goLoad({elm:'#puem-product-area', url:'/puem-editor/puem-store-product/all-product?{$params_}'});
JS;

$this->registerJs($scripts);