<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStore */

$this->title = 'Update Puem Store: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Puem Stores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'kd_toko' => $model->kd_toko]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="puem-store-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
