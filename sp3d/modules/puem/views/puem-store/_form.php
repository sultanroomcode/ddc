<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use sp3d\models\User;

//get kabupaten
$arr = ['-' => ''];
$kabupatenArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');
$kabupatenArr = $arr + $kabupatenArr;

//set bootstrap form
$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\posyandu\models\Posyandu */
/* @var $form yii\widgets\ActiveForm */
if(!$model->isNewRecord){
    /*$kabupatenArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');
    $kabupatenArr = $arr + $kabupatenArr;

    $mkec = User::find()->where(['type' => 'kecamatan']);
    $mkec->andWhere('id LIKE :query')->addParams([':query'=> $model->kd_kabupaten.'%']);
    $mkec->orderBy('description ASC');
    $kecamatanArr = ArrayHelper::map($mkec->all(), 'id', 'description');
    $kecamatanArr = $arr + $kecamatanArr;

    $mdesa = User::find()->where(['type' => 'desa']);
    $mdesa->andWhere('id LIKE :query')->addParams([':query'=> $model->kd_kecamatan.'%']);
    $mdesa->orderBy('description ASC');
    $desaArr = ArrayHelper::map($mdesa->all(), 'id', 'description');
    $desaArr = $arr + $desaArr;*/
} else {
    $kecamatanArr = ['-' => ''];
    $desaArr = ['-' => ''];
}
?>

<div class="puem-store-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>
    <?php if($model->isNewRecord){ ?>
        <?= $form->field($model, 'kd_kabupaten')->dropdownList($kabupatenArr) ?>

        <?= $form->field($model, 'kd_kecamatan')->dropdownList($kecamatanArr) ?>

        <?= $form->field($model, 'kd_desa')->dropdownList($desaArr) ?>

        <?= $form->field($model, 'kd_toko')->textInput(['readonly' => true]) ?>
    <?php } else { ?>
        <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
        <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
        <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
        <?= $form->field($model, 'kd_toko')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?php } ?>

    <?= $form->field($model, 'pemilik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#puem-area\',url:\'/puem-editor/puem-store/all-store\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
$('#puemstore-kd_kabupaten').change(function(){
  var kode = $(this).val();
  $('#puemstore-kd_kecamatan, #puemstore-kd_desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#puemstore-kd_kecamatan', url:'/pendamping/data/data-option?type=kecamatan&kode='+kode});
  }
});

$('#puemstore-kd_kecamatan').change(function(){
  var kode = $(this).val();
  $('#puemstore-kd_desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#puemstore-kd_desa', url:'/pendamping/data/data-option?type=desa&kode='+kode});
  }
});

$('#puemstore-kd_desa').change(function(){
  var kode = $(this).val();
  if(kode !== ''){    
    $.get(base_url+'/puem-editor/puem-store/get-new-id?kd_desa='+kode, function(res){
        $('#puemstore-kd_toko').val(res);
    });
  }
});

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#puem-area', url : '/puem-editor/puem-store/all-store'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);