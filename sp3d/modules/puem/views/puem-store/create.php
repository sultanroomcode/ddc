<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\puem\models\PuemStore */

$this->title = 'Create Puem Store';
$this->params['breadcrumbs'][] = ['label' => 'Puem Stores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puem-store-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
