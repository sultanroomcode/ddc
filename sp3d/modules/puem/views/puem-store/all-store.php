<a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-area', url:'/puem-editor/puem-store/create'})">Buat Store Baru</a>

<table class="table table-striped table-bordered">
<?php foreach($dataProvider->all() as $v): ?>
    <tr>
        <td><?= $v->kd_desa ?></td>
        <td><?= $v->kd_kecamatan ?></td>
        <td><?= $v->kd_kabupaten ?></td>
        <td><?= $v->kd_toko ?></td>
        <td><?= $v->pemilik ?></td>
        <td><?= $v->alamat ?></td>
        <td>
            <a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-area', url:'/puem-editor/puem-store/update?kd_desa=<?= $v->kd_desa?>&kd_toko=<?=$v->kd_toko ?>'})">Edit</a>
            <a href="javascript:void(0)" class="btn btn-success" onclick="deleteData('/puem-editor/puem-store/delete?kd_desa=<?= $v->kd_desa?>&kd_toko=<?=$v->kd_toko ?>', 'Berhasil hapus data')">Hapus</a>
            <a href="javascript:void(0)" class="btn btn-success" onclick="goLoad({elm:'#puem-area', url:'/puem-editor/puem-store/view?kd_desa=<?= $v->kd_desa?>&kd_toko=<?=$v->kd_toko ?>'})">Isi Data</a>
            
        </td>
    </tr>
<?php endforeach; ?>
</table>