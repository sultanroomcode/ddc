<?php
namespace sp3d\modules\puem\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
/**
 * This is the model class for table "puem_store_product_photo".
 *
 * @property int $idp
 * @property int $idf
 * @property string $src
 * @property string $caption
 * @property string $created_by
 * @property string $created_at
 */
class PuemStoreProductPhoto extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $tmp_src, $arr_parent, $kd_desa, $kd_toko;
    public static function tableName()
    {
        return 'puem_store_product_photo';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => null,
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idp', 'idf'], 'required'],
            [['idp', 'idf'], 'integer'],
            [['tmp_src'], 'file', 'extensions' => 'jpg'],
            [['src'], 'string', 'max' => 100],
            [['caption'], 'string', 'max' => 150],
            [['idp', 'idf'], 'unique', 'targetAttribute' => ['idp', 'idf']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idp' => 'Idp',
            'idf' => 'Idf',
            'src' => 'Foto',
            'tmp_src' => 'Foto',
            'caption' => 'Caption',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['idp' => $this->idp])->max('idf');
        if($mx == null){
            $this->idf = 1;
        } else {
            $this->idf = $mx +1;
        }
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['omset', 'keuntungan'], 'safe'],
        } 
        $this->upload();
        return true;
    } 

    public function upload()
    {
        $this->validate();
        $this->tmp_src = UploadedFile::getInstance($this, 'tmp_src');
        // echo "string ".time();
        // var_dump($this->tmp_src);
        if (isset($this->tmp_src) && $this->tmp_src->size != 0){
            $url = 'userfile/'.$this->kd_desa.'/puem/';
            $this->makeDir($url);
            $newname = 'p-sub-'.time().'-'.$this->kd_desa.'-'.$this->kd_toko.'.'.$this->tmp_src->extension;
            if($this->isNewRecord){
                $callback = $this->tmp_src->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->src) && $this->src !== '' && $this->src != null){
                    unlink($url.$this->src);
                }
                $callback = $this->tmp_src->saveAs($url.$newname);               
            }
            $this->src = $newname;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
