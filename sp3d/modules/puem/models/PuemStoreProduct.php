<?php
namespace sp3d\modules\puem\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
/**
 * This is the model class for table "puem_store_product".
 *
 * @property int $idp
 * @property string $kd_desa
 * @property int $kd_toko
 * @property string $nama
 * @property string $harga
 * @property string $ukuran
 * @property string $kapasitas_produksi
 * @property string $bahan_baku
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PuemStoreProduct extends ActiveRecord
{
    public $tmp_foto_produk;
    public static function tableName()
    {
        return 'puem_store_product';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_toko', 'jml_view'], 'integer'],
            [['deskripsi'], 'string', 'min' => 6],
            [['tmp_foto_produk'], 'file', 'extensions' => 'jpg'],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama', 'harga', 'ukuran', 'foto_produk'], 'string', 'max' => 100],
            [['kapasitas_produksi'], 'string', 'max' => 150],
            [['bahan_baku'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idp' => 'Idp',
            'kd_desa' => 'Kd Desa',
            'kd_toko' => 'Kd Toko',
            'nama' => 'Nama',
            'tmp_foto_produk' => 'Foto Produk',
            'foto_produk' => 'Foto Produk',
            'harga' => 'Harga',
            'ukuran' => 'Ukuran',
            'kapasitas_produksi' => 'Kapasitas Produksi',
            'bahan_baku' => 'Bahan Baku',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //events
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['omset', 'keuntungan'], 'safe'],
        } 
        $this->upload();
        return true;
    } 

    public function getPhoto()
    {
        return $this->hasMany(PuemStoreProductPhoto::className(), ['idp' => 'idp']);
    }

    public function upload()
    {
        $this->validate();
        $this->tmp_foto_produk = UploadedFile::getInstance($this, 'tmp_foto_produk');
        // echo "string ".time();
        // var_dump($this->tmp_foto_produk);
        if (isset($this->tmp_foto_produk) && $this->tmp_foto_produk->size != 0){
            $url = 'userfile/'.$this->kd_desa.'/puem/';
            $this->makeDir($url);
            $newname = 'p-main-'.time().'-'.$this->kd_desa.'-'.$this->kd_toko.'.'.$this->tmp_foto_produk->extension;
            if($this->isNewRecord){
                $callback = $this->tmp_foto_produk->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->foto_produk) && $this->foto_produk !== '' && $this->foto_produk != null){
                    unlink($url.$this->foto_produk);
                }
                $callback = $this->tmp_foto_produk->saveAs($url.$newname);               
            }
            $this->foto_produk = $newname;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
