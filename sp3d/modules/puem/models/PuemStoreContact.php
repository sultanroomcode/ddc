<?php
namespace sp3d\modules\puem\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "puem_store_contact".
 *
 * @property int $idp
 * @property int $idc
 * @property string $type_contact
 * @property string $text_contact
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PuemStoreContact extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'puem_store_contact';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'kd_toko'], 'required'],
            [['kd_desa', 'kd_toko', 'idc'], 'integer'],
            [['type_contact'], 'string', 'max' => 50],
            [['text_contact'], 'string', 'max' => 150],
            [['idc'], 'unique', 'targetAttribute' => ['idc']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kode Desa',
            'kd_toko' => 'Kode Toko',
            'idc' => 'Idc',
            'type_contact' => 'Type Contact',
            'text_contact' => 'Text Contact',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
