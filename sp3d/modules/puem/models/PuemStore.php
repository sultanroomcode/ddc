<?php
namespace sp3d\modules\puem\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "puem_store".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $kd_toko
 * @property string $pemilik
 * @property string $alamat
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PuemStore extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'puem_store';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'kd_toko'], 'required'],
            [['kd_toko'], 'integer'],
            [['deskripsi'], 'string', 'min' => 10],
            [['kd_desa'], 'string', 'min' => 10, 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 10],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['pemilik'], 'string', 'max' => 100],
            [['alamat'], 'string', 'max' => 150],
            [['kd_desa', 'kd_toko'], 'unique', 'targetAttribute' => ['kd_desa', 'kd_toko']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'kd_toko' => 'Kd Toko',
            'pemilik' => 'Pemilik',
            'alamat' => 'Alamat',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('kd_toko');
        if($mx == null){
            $this->kd_toko = 1;
        } else {
            $this->kd_toko = $mx +1;
        }
    }

    public function getKontak()
    {
        return $this->hasMany(PuemStoreContact::className(), ['kd_desa' => 'kd_desa', 'kd_toko' => 'kd_toko']);
    }

    public function getProduk()
    {
        return $this->hasMany(PuemStoreProduct::className(), ['kd_desa' => 'kd_desa', 'kd_toko' => 'kd_toko']);
    }
}
