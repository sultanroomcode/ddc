<?php

namespace sp3d\modules\puem;

/**
 * puem module definition class
 */
class Puem extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\puem\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
