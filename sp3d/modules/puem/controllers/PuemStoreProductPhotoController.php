<?php

namespace sp3d\modules\puem\controllers;

use Yii;
use sp3d\modules\puem\models\PuemStoreProductPhoto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PuemStoreProductPhotoController implements the CRUD actions for PuemStoreProductPhoto model.
 */
class PuemStoreProductPhotoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAllPhoto()
    {
        $dataget = Yii::$app->request->get();
        $dataProvider = PuemStoreProductPhoto::find();
        $dataProvider->where(['idp' => $dataget['idp']]);

        return $this->renderAjax('all-photo', [
            'dataProvider' => $dataProvider,
            'data' => $dataget
        ]);
    }

    /**
     * Lists all PuemStoreProductPhoto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PuemStoreProductPhoto::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PuemStoreProductPhoto model.
     * @param integer $idp
     * @param integer $idf
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idp, $idf)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($idp, $idf),
        ]);
    }

    /**
     * Creates a new PuemStoreProductPhoto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $dataget = Yii::$app->request->get();
        $model = new PuemStoreProductPhoto();
        $model->idp = $dataget['idp'];
        $model->kd_desa = $dataget['kd_desa'];
        $model->kd_toko = $dataget['kd_toko'];
        $model->arr_parent = [
            'kd_desa' => $dataget['kd_desa'],
            'kd_toko' => $dataget['kd_toko'],
        ];

         if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PuemStoreProductPhoto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idp
     * @param integer $idf
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idp, $idf)
    {
        $dataget = Yii::$app->request->get();
        $model = $this->findModel($idp, $idf);
        $model->kd_desa = $dataget['kd_desa'];
        $model->kd_toko = $dataget['kd_toko'];
        $model->arr_parent = [
            'kd_desa' => $dataget['kd_desa'],
            'kd_toko' => $dataget['kd_toko'],
        ];

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PuemStoreProductPhoto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idp
     * @param integer $idf
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idp, $idf, $kd_desa)
    {
        $model = $this->findModel($idp, $idf);
        $url = 'userfile/'.$kd_desa.'/puem/';

        if(file_exists($url.$model->src) && $model->src !== '' && $model->src != null){
            unlink($url.$model->src);
        }

        if($model->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the PuemStoreProductPhoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idp
     * @param integer $idf
     * @return PuemStoreProductPhoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idp, $idf)
    {
        if (($model = PuemStoreProductPhoto::findOne(['idp' => $idp, 'idf' => $idf])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
