<?php

namespace sp3d\modules\puem\controllers;

use Yii;
use sp3d\modules\puem\models\PuemStoreContact;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PuemStoreContactController implements the CRUD actions for PuemStoreContact model.
 */
class PuemStoreContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PuemStoreContact models.
     * @return mixed
     */
    public function actionAllContact()
    {
        $dataget = Yii::$app->request->get();
        $dataProvider = PuemStoreContact::find();
        $dataProvider->where(['kd_desa' => $dataget['kd_desa'], 'kd_toko' => $dataget['kd_toko']]);

        return $this->renderAjax('all-contact', [
            'dataProvider' => $dataProvider,
            'data' => $dataget
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PuemStoreContact::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PuemStoreContact model.
     * @param integer $idp
     * @param integer $idc
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idp, $idc)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($idp, $idc),
        ]);
    }

    /**
     * Creates a new PuemStoreContact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetNewId()
    {

    }
    
    public function actionCreate()
    {
        $dataget = Yii::$app->request->get();
        $model = new PuemStoreContact();
        

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            if(count($dataget) > 0){
                $model->kd_desa = $dataget['kd_desa'];
                $model->kd_toko = $dataget['kd_toko'];
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PuemStoreContact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idp
     * @param integer $idc
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idc)
    {
        $model = $this->findModel($idc);

         if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PuemStoreContact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idp
     * @param integer $idc
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idc)
    {
        $model = $this->findModel($idc);

        if($model->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the PuemStoreContact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idp
     * @param integer $idc
     * @return PuemStoreContact the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idc)
    {
        if (($model = PuemStoreContact::findOne(['idc' => $idc])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
