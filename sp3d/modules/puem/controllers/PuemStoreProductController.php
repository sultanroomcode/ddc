<?php

namespace sp3d\modules\puem\controllers;

use Yii;
use sp3d\modules\puem\models\PuemStoreProduct;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PuemStoreProductController implements the CRUD actions for PuemStoreProduct model.
 */
class PuemStoreProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PuemStoreProduct models.
     * @return mixed
     */
    public function actionAllProduct()
    {
        $dataget = Yii::$app->request->get();
        $dataProvider = PuemStoreProduct::find();
        $dataProvider->where(['kd_desa' => $dataget['kd_desa'], 'kd_toko' => $dataget['kd_toko']]);

        return $this->renderAjax('all-product', [
            'dataProvider' => $dataProvider,
            'data' => $dataget
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PuemStoreProduct::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PuemStoreProduct model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PuemStoreProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $dataget = Yii::$app->request->get();
        $model = new PuemStoreProduct();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            if(count($dataget) > 0){
                $model->kd_desa = $dataget['kd_desa'];
                $model->kd_toko = $dataget['kd_toko'];
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PuemStoreProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

         if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PuemStoreProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $kd_desa)
    {
        $model = $this->findModel($id);
        $url = 'userfile/'.$kd_desa.'/puem/';
        //chek product foto first
        foreach ($model->photo as $v) {
            if(file_exists($url.$v->src) && $v->src !== '' && $v->src != null){
                unlink($url.$v->src);
            }

            $v->delete();
        }

        if(file_exists($url.$model->foto_produk) && $model->foto_produk !== '' && $model->foto_produk != null){
            unlink($url.$model->foto_produk);
        }

        if($model->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the PuemStoreProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PuemStoreProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PuemStoreProduct::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
