<?php

namespace sp3d\modules\puem\controllers;

use Yii;
use sp3d\modules\puem\models\PuemStore;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PuemStoreController implements the CRUD actions for PuemStore model.
 */
class PuemStoreController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PuemStore models.
     * @return mixed
     */
    public function actionAllStore()
    {
        $dataProvider = PuemStore::find();
        return $this->renderAjax('all-store', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PuemStore::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PuemStore model.
     * @param string $kd_desa
     * @param integer $kd_toko
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $kd_toko)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $kd_toko),
        ]);
    }

    /**
     * Creates a new PuemStore model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetNewId($kd_desa)
    {
        $model = new PuemStore;
        $model->kd_desa = $kd_desa;
        $model->genNum();

        return $model->kd_toko;
    }

    public function actionCreate()
    {
        $model = new PuemStore();

         if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PuemStore model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $kd_toko
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $kd_toko)
    {
        $model = $this->findModel($kd_desa, $kd_toko);

         if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PuemStore model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $kd_toko
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $kd_toko)
    {
        $model = $this->findModel($kd_desa, $kd_toko);
        //delete contact first
        foreach($model->kontak as $v){
            $v->delete();
        }

        //delete product then
        $url = 'userfile/'.$model->kd_desa.'/puem/';
        foreach($model->produk as $x){
            foreach ($x->photo as $y) {
                if(file_exists($url.$y->src) && $y->src !== '' && $y->src != null){
                    unlink($url.$y->src);
                }

                $y->delete();
            }

            if(file_exists($url.$x->foto_produk) && $x->foto_produk !== '' && $x->foto_produk != null){
                unlink($url.$x->foto_produk);
            }
            $x->delete();
        }
        //last delete store
        if($model->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the PuemStore model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $kd_toko
     * @return PuemStore the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $kd_toko)
    {
        if (($model = PuemStore::findOne(['kd_desa' => $kd_desa, 'kd_toko' => $kd_toko])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
