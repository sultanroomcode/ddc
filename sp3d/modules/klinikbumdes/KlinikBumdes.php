<?php

namespace sp3d\modules\klinikbumdes;

/**
 * klinikbumdes module definition class
 */
class KlinikBumdes extends \yii\base\Module
{
    public $controllerNamespace = 'sp3d\modules\klinikbumdes\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
