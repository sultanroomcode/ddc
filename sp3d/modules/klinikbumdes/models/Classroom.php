<?php
namespace sp3d\modules\klinikbumdes\models;

use yii\base\Model;
use Yii;

class Classroom extends Model
{
    public $email, $service, $alias, $codeInvites;
    public $courseId, $name, $section, $descriptionHeading, $description, $room, $ownerId, $courseState;

    public function rules()
    {
        return [
            [['name', 'section','descriptionHeading', 'description', 'room', 'courseState'], 'required'],
            [['alias'], 'safe'],
            [['email'], 'email'],
            [['courseId'], 'integer'],
        ];
    }

    /**
     * Signs user up.
     * https://github.com/gsuitedevs/php-samples
     * https://developers.google.com/classroom/guides/manage-courses
     * https://github.com/googleapis/google-api-php-client
     * https://www.programmableweb.com/api/google-classroom/sdks
     * https://classroom.google.com/w/MzI3Mjc4ODM0NzZa/t/all -> agussutarom@gmail.com
     * @return User|null the saved model or null if saving fails
     */
    public function init()
    {
        parent::init();
        $this->lihatService();
    }

    function masuk()
    {
        $client = new \Google_Client();
        $client->setApplicationName('Klinik Bumdes Data Desa Center');
        $client->setScopes([\Google_Service_Classroom::CLASSROOM_COURSES, \Google_Service_Classroom::CLASSROOM_COURSEWORK_ME, \Google_Service_Classroom::CLASSROOM_ANNOUNCEMENTS, \Google_Service_Classroom::CLASSROOM_PROFILE_EMAILS, \Google_Service_Classroom::CLASSROOM_PROFILE_PHOTOS, \Google_Service_Classroom::CLASSROOM_ROSTERS, \Google_Service_Classroom::CLASSROOM_COURSEWORK_STUDENTS]);
        $client->setAuthConfig('lib/credentials.json');
        // $client->setAuthConfig(Yii::getAlias('@sp3d').'/modules/klinikbumdes/models/credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = 'lib/token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }


    public function lihatService()
    {
        $client = $this->masuk();
        $this->service = new \Google_Service_Classroom($client);
    }

    public function lihatKelas()
    {
        // Print the first 10 courses the user has access to.
        $optParams = array(
          'pageSize' => 10
        );

        $results = $this->service->courses->listCourses($optParams);

        if (count($results->getCourses()) == 0) {
          $data = ['message' => "Tidak ada kelas", 'status' => 0, 'data' => []];
        } else {
          $data = ['message' => "Ada kok kelasnya!", 'status' => 1, 'data' => ['result' => $results->getCourses()]];
        }

        return $data;
    }

    public function detailKelas($id)
    {
        // Print the first 10 courses the user has access to.
        $courseId = $id;
        try {
          $course = $this->service->courses->get($courseId);
          $data = ['message' => "Kelas Ada!", 'status' => 1, 'data' => $course];
        } catch (Google_Service_Exception $e) {
          if ($e->getCode() == 404) {
            $data = ['message' => "Kelas Tidak Ditemukan!", 'status' => 0, 'data' => null];
          } else {
            throw $e;
          }
        }

        return $data;
    }

    public function setKelas($course)
    {
        $arr = [
            'name' => $course->name,
            'section' => $course->section,
            'descriptionHeading' => $course->descriptionHeading,
            'description' => $course->description,
            'room' => $course->room,
            'courseState' => $course->courseState
        ];

        $this->setAttributes($arr);
    }

    public function hapusKelas($id)
    {
        // Print the first 10 courses the user has access to.
        $courseId = $id;
        try {
          $course = $this->service->courses->delete($courseId);
          $data = ['message' => "Kelas sudah terhapus!", 'status' => 1, 'data' => $course];
        } catch (Google_Service_Exception $e) {
          if ($e->getCode() == 404) {
            $data = ['message' => "Kelas Tidak Ditemukan!", 'status' => 0, 'data' => null];
          } else {
            throw $e;
          }
        }

        return $data;
    }

    public function isiGuru()
    {
      $courseId = $this->courseId;
      $teacherEmail = $this->email;
      $teacher = new \Google_Service_Classroom_Teacher(array(
        'userId' => $teacherEmail
      ));

      try {
        $teacher = $this->service->courses_teachers->create($courseId, $teacher);
        $data = ['message' => "user ".$teacher->profile->name->fullName." telah dimasukkan sebagai guru!", 'status' => 1, 'data' => $teacher];
      } catch (\Google_Service_Exception $e) {
        if ($e->getCode() == 409) {
          $data = ['message' => "user {$teacherEmail} sudah masuk sebagai guru!", 'status' => 0, 'data' => null];
        } else {
          throw $e;
        }
      }
    }

    public function isiMurid()
    {
      $courseId = $this->courseId;
      $studentEmail = $this->email;
      $enrollmentCode = $this->codeInvites;
      $student = new \Google_Service_Classroom_Student(array(
        'userId' => $studentEmail
      ));
      $params = array(
        'enrollmentCode' => $enrollmentCode
      );
      try {
        $student = $this->service->courses_students->create($courseId, $student, $params);
        $data = ['message' => "user telah didaftarkan!", 'status' => 0, 'data' => $student];
      } catch (Google_Service_Exception $e) {
        if ($e->getCode() == 409) {
          $data = ['message' => "user {$studentEmail} sudah masuk!", 'status' => 0, 'data' => null];
        } else {
          throw $e;
        }
      }

    }

    public function buatKelas()
    {
        $arr = [
          'name' => $this->name,
          'section' => $this->section,
          'descriptionHeading' => $this->descriptionHeading,
          'description' => $this->description,
          'room' => $this->room,
          'ownerId' => 'me',
          'courseState' => $this->courseState
        ];

        $course = new \Google_Service_Classroom_Course($arr);

        $course = $this->service->courses->create($course);
        $data = ['message' => "Berhasil Buat Kelas", 'status' => 1, 'data' => ['nama_kelas' => $course->name, 'id_kelas' => $course->id]];

        return $data;
    }

    public function updateKelas()
    {
        try {
            $course = new \Google_Service_Classroom_Course([
              'name' => $this->name,
              'section' => $this->section,
              'descriptionHeading' => $this->descriptionHeading,
              'description' => $this->description,
              'room' => $this->room,
              'courseState' => $this->courseState
            ]);
            $params = [
              'updateMask' => 'name,section,descriptionHeading,description,room,courseState'
            ];


            $course = $this->service->courses->patch($this->courseId, $course, $params);
            $data = ['message' => "Berhasil Update Kelas", 'status' => 1, 'data' => ['nama_kelas' => $course->name, 'id_kelas' => $course->id]];
        } catch (Google_Service_Exception $e) {
          if ($e->getCode() == 404) {
            $data = ['message' => "Kelas Tidak Ditemukan!", 'status' => 0, 'data' => null];
          } else {
            throw $e;
          }
        }


        return $data;
    }
}
