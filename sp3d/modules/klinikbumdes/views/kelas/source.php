<?php 
use yii\helpers\Url;
$data = $model->lihatKelas();
?>
<a href="javascript:void(0)" class="btn btn-danger" onclick="goLoad({elm:'#klinik-bumdes-zona', url:'/klinikbumdes/kelas/buat'})">Tambah Kelas</a>
<table id="klinikbumdes-table" class="table table-striped">
<thead>
	<tr>
		<th>ID</th>
		<th>Nama Kelas</th>
		<th>Status Kelas</th>
		<th>Aksi</th>
	</tr>
</thead>
<tbody>
<?php 
// var_dump($data);
foreach ($data['data']['result'] as $course) {
	echo '<tr><td>'.$course->getId().'</td><td>'.$course->getName().'</td><td>'.$course->getCourseState().'</td>
	<td><a href="javascript:void(0)" onclick="goLoad({elm:\'#klinik-bumdes-zona\', url:\'/klinikbumdes/kelas/kelas?id='.$course->getId().'\'})" class="btn btn-danger">Lihat Kelas</a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#klinik-bumdes-zona\', url:\'/klinikbumdes/kelas/update?id='.$course->getId().'\'})" class="btn btn-danger">Update</a>';
	if($course->getCourseState() == 'ARCHIVED'){
		echo '<a href="javascript:void(0)" onclick="confirmationDelete({nama:\''.$course->getName().'\', kodekelas:\''.$course->getId().'\'})" class="btn btn-danger">Hapus</a>';
	}
	echo '</td></tr>';
}
?>
</tbody>
</table>
<?php
$scripts =<<<JS
	$('#klinikbumdes-table').DataTable();

	function confirmationDelete(obj){
		$.confirm({
		    title: 'Hapus Kelas?',
		    content: 'Tindakan ini akan menghapus data kelas '+obj.nama,
		    autoClose: 'batal|10000',
		    buttons: {
		        deleteUser: {
		            text: 'hapus',
		            action: function () {
		                goLoad({url: '/klinikbumdes/kelas/hapus?id='+obj.kodekelas});
		            }
		        },
		        batal: function () {
		            $.alert('Tindakan dibatalkan');
		        }
		    }
		});
	}
JS;

$this->registerJs($scripts);