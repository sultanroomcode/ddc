<?php 
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
	'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/klinikbumdes/kelas/source';
$urlback = 'goLoad({elm:\'#klinik-bumdes-zona\', url:\''.$url.'\'})';
?>

<div class="bumdes-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'section')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'descriptionHeading')->textInput(['maxlength' => true]) ?>
        
            <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

            <?= $form->field($model, 'room')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ownerId')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'courseState')->dropdownList(['ACTIVE' => 'Aktif', 'PROVISIONED' => 'Draft']) ?>
			
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-7">
					<?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
			        <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
				</div>
			</div>
        </div>
    </div>
    
    <div class="form-group">
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
    	console.log(res);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert(res.message);
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);
?>