<div class="klinik-bumdes-view">
    <div class="tile">
        <h2 class="tile-title">Klinik BUMDesa</h2>
        <div class="p-10">
			<div id="klinik-bumdes-zona"></div>
		</div>
	</div>
</div>
<?php
$script =<<<JS
goLoad({elm:'#klinik-bumdes-zona', url:'/klinikbumdes/kelas/source'});
JS;
$this->registerJs($script);