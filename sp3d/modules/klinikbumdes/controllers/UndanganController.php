<?php
namespace sp3d\modules\klinikbumdes\controllers;

use Yii;
use yii\web\Controller;
use sp3d\modules\klinikbumdes\models\Classroom;
/**
 * Default controller for the `klinikbumdes` module
 */
class UndanganController extends Controller
{
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionSource()
    {
        $model = new Classroom();

        return $this->renderAjax('source', [
            'model' => $model
        ]);
    }

    public function actionBuat()
    {
        $model = new Classroom();

        return $this->renderAjax('buat', [
            'model' => $model
        ]);
    }

    public function actionKelas($id)
    {
        $model = new Classroom();

        return $this->renderAjax('kelas', [
            'model' => $model,
            'id' => $id
        ]);
    }    
}
