<?php
namespace sp3d\modules\klinikbumdes\controllers;

use Yii;
use yii\web\Controller;
use sp3d\modules\klinikbumdes\models\Classroom;
/**
 * Default controller for the `klinikbumdes` module
 */
class KelasController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSrc()
    {
    	$data = Yii::getAlias('@sp3d').'/modules/klinikbumdes/models/credentials.json';
    	var_dump(file_get_contents($data));
    }

    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionSource()
    {
        $model = new Classroom();

        return $this->renderAjax('source', [
            'model' => $model
        ]);
    }

    public function actionBuat()
    {
        $model = new Classroom();

        if ($model->load(Yii::$app->request->post())) {
            $data = $model->buatKelas();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return json_encode($data);
        } else {
            return $this->renderAjax('buat', [
                'model' => $model
            ]);
        }
    }

    public function actionKelas($id)
    {
        $model = new Classroom();

        return $this->renderAjax('kelas', [
            'model' => $model,
            'id' => $id
        ]);
    }

    public function actionUpdate($id)
    {
        $model = new Classroom();
        $model->courseId = $id;

        if ($model->load(Yii::$app->request->post())) {
            $data = $model->updateKelas($id);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            echo json_encode($data);
        } else {
            $res = $model->detailKelas($id);
            $data = $res['data'];
            // $model->name = $data->name;
            $model->setKelas($data);
            return $this->renderAjax('update', [
                'model' => $model,
                'id' => $id
            ]);
        }
    }

    public function actionHapus($id)
    {
        $model = new Classroom();
        //prekondisi, syarat untuk dihapus adalah status kelas harus diset menjadi arsip
        $data = $model->hapusKelas($id);
        if($data['status'] == 1){
            echo "<script>$.alert('".$data['message']."');goLoad({url: '/klinikbumdes/kelas'});</script>";
        } else {
            echo "<script>$.alert('".$data['message']."');goLoad({url: '/klinikbumdes/kelas'});</script>";
        }
    }
}
