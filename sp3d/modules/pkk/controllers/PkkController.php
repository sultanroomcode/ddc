<?php
namespace sp3d\modules\pkk\controllers;

use Yii;
use sp3d\modules\pkk\models\Pkk;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PkkController implements the CRUD actions for Pkk model.
 */
class PkkController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pkk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Pkk::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pkk model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $kd_desa = Yii::$app->user->identity->id;
        $model = Pkk::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/pkk/pkk/create\'});</script>';
        } else {
            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        }        
    }

    public function actionViewInBox($kd_desa)
    {
        $model = Pkk::findOne(['kd_desa' => $kd_desa]);

        if($model == null){
            //redirect to create
            echo '<script>goLoad({url:\'/pkk/pkk/create\'});</script>';
        } else {
            return $this->renderAjax('view-in-box', [
                'model' => $model,
            ]);
        }        
    }

    /**
     * Creates a new Pkk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pkk();
        $model->kd_desa = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pkk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa)
    {
        $model = $this->findModel($kd_desa);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pkk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id)->delete();
        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the Pkk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Pkk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pkk::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
