<?php

namespace sp3d\modules\pkk\controllers;

use yii\web\Controller;

/**
 * Default controller for the `pkk` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
