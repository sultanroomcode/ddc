<?php
namespace sp3d\modules\pkk\controllers;

use Yii;
use sp3d\modules\pkk\models\PkkSumberDana;
use sp3d\modules\pkk\models\Pkk;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PkkPelatihanController implements the CRUD actions for PkkPelatihan model.
 */
class PkkSumberDanaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PkkSumberDana models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PkkSumberDana::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PkkSumberDana model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionView($kd_desa, $id, $tipe)
    {
        return $this->renderAjax('view', [
            'data' => ['kd_desa' => $kd_desa, 'id' => $id, 'tipe' => $tipe],
            'model' => PkkSumberDana::findAll(['kd_desa' => $kd_desa, 'id' => $id, 'tipe' => $tipe]),
        ]);
    }

    public function actionViewInBox($kd_desa)
    {
        $model = Pkk::findOne(['kd_desa' => $kd_desa]);
        return $this->renderAjax('view-in-box', [
            'model' => $model,
            'kd_desa' => $kd_desa
        ]);
    }

    /**
     * Creates a new PkkSumberDana model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa)
    {
        $model = new PkkSumberDana();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing PkkSumberDana model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing PkkSumberDana model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $idp)
    {
        $model = $this->findModel($kd_desa, $id, $idp)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the PkkSumberDana model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $idp
     * @return PkkSumberDana the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $idp)
    {
        if (($model = PkkSumberDana::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'idp' => $idp])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
