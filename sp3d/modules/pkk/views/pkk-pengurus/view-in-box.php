<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-pkk-pengurus', url:'/pkk/pkk-pengurus/create?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger"><?=(count($model->pengurus) == 0)?'Isi':'Tambah'?> Pengurus</a>

<?php 
echo '<table class="table table-striped"><tr><th>Nama</th><th>Jabatan</th><th>Tanggal Lahir</th><th>Pendidikan</th><th>Pelatihan</th><th>Aksi</th></tr>';
foreach($model->pengurus as $v){
    echo '<tr><th>'.$v->nama.'</th><th>'.$v->jabatandetail->nama_jabatan.'</th><th>'.$v->tanggal_lahir.'</th><th>'.substr($v->pendidikan, 2).'</th><td>'.count($v->pelatihan).'</td><th><a href="javascript:void(0)" onclick="openModalXyf({welm:\'600px\',url:\'/pkk/pkk-pengurus/view?id_pengurus='.$v->id_pengurus.'&kd_desa='.$v->kd_desa.'\'})" class="btn btn-danger">View</a></th></tr>';
}
echo '</table>';	
?>