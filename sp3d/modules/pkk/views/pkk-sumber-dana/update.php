<?php
use yii\helpers\Html;

$this->title = 'Update Sumberdana PKK';
$this->params['breadcrumbs'][] = ['label' => 'Lkmd Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_kegiatan' => $model->id_kegiatan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lkmd-kegiatan-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
