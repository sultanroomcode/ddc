<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-pkk-kegiatan', url:'/pkk/pkk-kegiatan/create?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger"><?=(count($model->kegiatan) == 0)?'Isi':'Tambah'?> Kegiatan</a>

<?php 
echo '<table class="table table-striped"><tr><th>Jenis Kegiatan</th><th>Nama Kegiatan</th><th>Tanggal Kegiatan</th><th>Narasumber</th><th>Sumber Dana</th><th>Aksi</th></tr>';
foreach($model->kegiatan as $v){
    echo '<tr><th>'.$v->showJenis().'</th><th>'.$v->nama.'</th><th>'.$v->tanggal_mulai.'</th><th>'.$v->narasumber.'</th><th>'.$v->sumber_dana.'</th><th>Aksi</th></tr>';
}
echo '</table>';	
?>