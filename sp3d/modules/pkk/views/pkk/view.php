<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\pkk\models\Lkmd */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Lkmds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pkk-view">
    <div class="tile">
        <h2 class="tile-title">PKK</h2>
        <div class="p-10">
            <div id="zona-pkk-data"></div>
            <div id="zona-pkk-pembina"></div>
            <div id="zona-pkk-pengurus"></div>
            <div id="zona-pkk-sumberdana"></div>
            <div id="zona-pkk-kegiatan"></div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>
<?php 
$kd_desa = $model->kd_desa;
$script = <<<JS
goLoad({elm:'#zona-pkk-data', url:'/pkk/pkk/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-pkk-pengurus', url:'/pkk/pkk-pengurus/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-pkk-pembina', url:'/pkk/pkk-anggota-pembina/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-pkk-sumberdana', url:'/pkk/pkk-sumber-dana/view-in-box?kd_desa={$kd_desa}'});
goLoad({elm:'#zona-pkk-kegiatan', url:'/pkk/pkk-kegiatan/view-in-box?kd_desa={$kd_desa}'});
JS;
$this->registerJs($script);
?>