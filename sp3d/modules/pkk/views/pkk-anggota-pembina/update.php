<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lkmd\models\LkmdAnggotaPembina */

$this->title = 'Update Pembina PKK : ' . $model->nama;
?>
<div class="lkmd-anggota-pembina-update">
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
