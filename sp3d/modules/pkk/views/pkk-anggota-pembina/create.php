<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\lkmd\models\LkmdAnggotaPembina */

$this->title = 'Daftar Pembina PKK';
$this->params['breadcrumbs'][] = ['label' => 'Lkmd Anggota Pembinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lkmd-anggota-pembina-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
