<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
if($model->pembina_status_ada == 'ada'){
	?>
	<a href="javascript:void(0)" onclick="goLoad({elm:'#zona-pkk-pembina', url:'/pkk/pkk-anggota-pembina/create?kd_desa=<?=$model->kd_desa?>'})" class="btn btn-danger"><?=(count($model->pembina) == 0)?'Isi':'Tambah'?> Pembina</a>
	<!-- jika kosong text berubah menjadi isi pembina -->
	<?php
	echo '<table class="table table-striped"><tr><th>Nama</th><th>Jabatan</th><th>Tanggal Lahir</th><th>Pendidikan</th><th>Pelatihan</th><th>Aksi</th></tr>';
	foreach($model->pembina as $v){
	    echo '<tr><th>'.$v->nama.'</th><th>'.$v->jabatandetail->nama_jabatan.'</th><th>'.$v->tanggal_lahir.'</th><th>'.substr($v->pendidikan, 2).'</th><td>'.count($v->pelatihan).'</td><th><a href="javascript:void(0)" onclick="openModalXyf({welm:\'600px\',url:\'/pkk/pkk-anggota-pembina/view?id_anggota='.$v->id_anggota.'&kd_desa='.$v->kd_desa.'\'})" class="btn btn-danger">View</a></th></tr>';
	}
	echo '</table>';	
} else {
	echo "PKK belum memiliki pembina";
}
?>