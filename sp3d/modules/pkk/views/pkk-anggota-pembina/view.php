<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\lkmd\models\LkmdAnggotaPembina */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Lkmd Anggota Pembinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lkmd-anggota-pembina-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'jabatan',
                'value' => function($m){
                    return $m->jabatandetail->nama_jabatan;
                }
            ],
            'nama',
            [
                'label' => 'TTL',
                'attribute' => 'tempat_lahir',
                'value' => function($m){
                    return $m->tempat_lahir.', '.date('d-F-Y', strtotime($m->tanggal_lahir));
                }
            ],
            'alamat',
            // 'pendidikan',
            [
                'attribute' => 'pendidikan',
                'value' => function($m){
                    return substr($m->pendidikan,2);
                }
            ],
            'status_aktif',
        ],
    ]) ?>

    <h4>Catatan Pelatihan</h4>
    <div id="list-catatan-pelatihan"></div>
</div>
<?php 
$script = <<<JS
goLoad({elm:'#list-catatan-pelatihan', url:'/pkk/pkk-pelatihan/view?id={$model->id_anggota}&kd_desa={$model->kd_desa}&tipe=pembina'});
JS;

$this->registerJs($script);