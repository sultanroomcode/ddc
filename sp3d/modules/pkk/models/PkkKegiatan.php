<?php
namespace sp3d\modules\pkk\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "pkk_kegiatan".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_kegiatan
 * @property string $nama
 * @property string $jenis
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $sumber_dana
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PkkKegiatan extends ActiveRecord
{
    public $arr = [
        1 => '1. Penghayatan dan Pengamalan PANCASILA',
        2 => '2. Gotong Royong',
        3 => '3. Pangan',
        4 => '4. Sandang',
        5 => '5. Perumahan dan Tata Laksana Rumah Tangga',
        6 => '6. Pendidikan dan Keterampilan',
        7 => '7. Kesehatan',
        8 => '8. Pengembangan Kehidupan Berkoperasi',
        9 => '9. Kelestarian Lingkungan Hidup',
        10 => '10. Perencanaan Sehat',
    ];

    public static function tableName()
    {
        return 'pkk_kegiatan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa', 'id_kegiatan'], 'required'],
            [['id_kegiatan'], 'integer'],
            [['tanggal_mulai'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['nama'], 'string', 'max' => 120],
            [['jenis', 'narasumber'], 'string', 'max' => 100],
            [['sumber_dana'], 'string', 'max' => 20],
            [['kd_desa', 'id_kegiatan'], 'unique', 'targetAttribute' => ['kd_desa', 'id_kegiatan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_kegiatan' => 'Id Kegiatan',
            'nama' => 'Nama Kegiatan',
            'jenis' => 'Jenis Kegiatan',
            'tanggal_mulai' => 'Tanggal Kegiatan',
            'narasumber' => 'Narasumber',
            'sumber_dana' => 'Sumber Dana',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diubah Oleh',
            'created_at' => 'Dibuat pada tanggal',
            'updated_at' => 'Diubah pada tanggal',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id_kegiatan');
        if($mx == null){
            $this->id_kegiatan = 1;
        } else {
            $this->id_kegiatan = $mx +1;
        }
    }

    public function showJenis()
    {
        return $this->arr[$this->jenis];
    }
}
