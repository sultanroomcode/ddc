<?php
namespace sp3d\modules\pkk\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use sp3d\models\transaksi\Sp3dMasterJabatan;
/**
 * This is the model class for table "pkk_anggota_pembina".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_anggota
 * @property string $jabatan
 * @property string $nama
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $pendidikan
 * @property string $status_aktif
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PkkAnggotaPembina extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pkk_anggota_pembina';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa', 'id_anggota','nama', 'alamat', 'tempat_lahir', 'tanggal_lahir'], 'required'],
            [['id_anggota'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['jenis_kelamin'], 'string', 'max' => 1],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['jabatan'], 'string', 'max' => 50],
            [['nama', 'alamat', 'tempat_lahir', 'tanggal_lahir', 'pendidikan', 'status_aktif'], 'string', 'max' => 100],
            [['kd_desa', 'id_anggota'], 'unique', 'targetAttribute' => ['kd_desa', 'id_anggota']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_anggota' => 'Id Anggota',
            'jabatan' => 'Jabatan',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'jenis_kelamin' => 'Jenis Kelamin',
            
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'pendidikan' => 'Jenjang Pendidikan Terakhir',
            'status_aktif' => 'Status Aktif',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diubah Oleh',
            'created_at' => 'Dibuat pada tanggal',
            'updated_at' => 'Diubah pada tanggal',
        ];
    }

    public function getLembaga()
    {
        return $this->hasOne(Pkk::className(), ['kd_desa' => 'kd_desa']);
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id_anggota');
        if($mx == null){
            $this->id_anggota = 1;
        } else {
            $this->id_anggota = $mx +1;
        }
    }

    public function getJabatandetail()
    {
        return $this->hasOne(Sp3dMasterJabatan::className(), ['id' => 'jabatan']);
    }

    public function getPelatihan()
    {
        return $this->hasMany(PkkPelatihan::className(), ['id' => 'id_anggota', 'kd_desa' => 'kd_desa'])->onCondition(['tipe' => 'pembina']);
    }
}
