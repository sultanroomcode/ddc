<?php

namespace sp3d\modules\pkk;

/**
 * pkk module definition class
 */
class Pkk extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'sp3d\modules\pkk\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
