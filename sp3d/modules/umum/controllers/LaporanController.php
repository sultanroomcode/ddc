<?php
namespace sp3d\modules\umum\controllers;

use yii\web\Controller;

/**
 * Default controller for the `umum` module
 */
class LaporanController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($tahun=2016)
    {
        return $this->renderAjax('index', [
            'tahun' => $tahun
        ]);
    }

    public function actionLaporan($page, $kd)
    {
        return $this->renderAjax('laporan-'. $page, [
            'kd' => $kd
        ]);
    }

    public function actionDetailUpload($tahun)//permintaan pak novi
    {
        return $this->renderAjax('detail-upload', [
            'tahun' => $tahun
        ]);
    }

    public function actionDetailKabupatenKirim($status, $tahun)
    {
        return $this->renderAjax('detail-kabupaten-kirim', [
            'status' => $status,
            'tahun' => $tahun
        ]);
    }

    public function actionDetailKecamatanKirim($status, $tahun)
    {
        return $this->renderAjax('detail-kecamatan-kirim', [
            'status' => $status,
            'tahun' => $tahun
        ]);
    }

    public function actionDetailDesaKirim($status, $tahun)
    {
        return $this->renderAjax('detail-desa-kirim', [
            'status' => $status,
            'tahun' => $tahun
        ]);
    }
}
