<?php
namespace sp3d\modules\umum\controllers;

use Yii;
use sp3d\models\AuthAssignment;
use sp3d\models\User;
use sp3d\models\Log;
use sp3d\models\transaksi\TaUserDesa;
use sp3d\models\transaksi\Sp3dProfileDesa;
use sp3d\models\IsiPengguna;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
- index
- reset-password
- management
- management-data
- password-desa
- setting
- user-detail
- profile
- profile-setting
- pingDomain - function
- info
- pesan
- log
- ganti-password
- view
- generate-insert-baru
- admin-baru
- dispemas-baru
- test-baru
- buat-baru
- create
- update
- delete
- findModel - function
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $dataProvider = new ActiveDataProvider([
        //     'query' => User::find(),
        // ]);
        $dataProvider = User::find();

        return $this->renderAjax('user-index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGantiPassword()
    {
        $model = User::findOne(['username' => Yii::$app->user->identity->username]);
        if ($model->load(Yii::$app->request->post())){
            $model->setPassword($model->password_baru);
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('ganti-password', [
                'model' => $model,
            ]);
        }
    }

    public function actionResetPassword($id)
    {
        //khusus selain desa
        $user = User::findOne($id);
        $user->setPassword($user->password_default);
        if($user->save(false)) echo 1; else echo 0;
    }

    public function actionManagement()
    {
        //khusus selain desa
        return $this->renderAjax('management');
    }

    public function actionManagementKabupaten()
    {
        //khusus selain desa
        return $this->renderAjax('management-kabupaten');
    }

    public function actionManagementData()
    {
        //khusus selain desa
        $dttable = Yii::$app->request->get();

        // var_dump($dttable);
        // print_r($dttable);
        $data = User::find()->select(['id', 'username', 'description', 'type']);
        $datap = $data->offset($dttable['start'])->limit($dttable['length']);//data paging

        if($dttable['kabupaten'] != '-' && $dttable['kecamatan'] == ''){
            $datap->where(['LIKE', 'id', $dttable['kabupaten'].'%', false]);
        }

        if($dttable['kecamatan'] != ''){
            $datap->where(['LIKE', 'id', $dttable['kecamatan'].'%', false]);
        }

        if($dttable['search']['value'] != ''){
            $datap->andWhere(['LIKE', 'username', $dttable['search']['value']]);
            $datap->orWhere(['LIKE', 'description', $dttable['search']['value']]);
        }

        $dataserve = ArrayHelper::toArray($datap->all(), ['sp3d\models\User' => 
            [
            'id',
            'description',
            'type',
            'action' => function($p){
                return '<a href="javascript:void(0)" onclick="confirmReset('.$p->id.')" class="btn btn-sm">reset password</a> <a href="javascript:void(0)" onclick="openModalXy({url:\'/umum/user/detail-pengguna?id='.$p->id.'\'})" class="btn btn-sm">Detail</a>';
            }]
        ]);

        $foor = [
            'draw' => (int) $dttable['draw'],
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $datap->count(),
            'data' => $dataserve
        ];
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }

    //setting - khusus desa
    public function actionSetting()
    {
        if(Yii::$app->user->identity->type != 'desa'){
            echo "<script>$.alert('Setting belum ada untuk selain desa');goLoad({url:'/umum/default/dashboard'});</script>";
        } else {
            $model = TaUserDesa::findOne(Yii::$app->user->identity->id);
            if($model == null){
                $crmodel = new TaUserDesa();
                $crmodel->Id = Yii::$app->user->identity->id;
                $crmodel->siskeu_id = '';
                $crmodel->kd_kabupaten = substr($crmodel->Id, 2,2);
                $crmodel->Kd_Kec = substr($crmodel->Id, 4,3);
                $crmodel->Kd_Desa = substr($crmodel->Id, 7,3);
                $crmodel->save();

                echo "<script>$.alert('Konfigurasi telah dibuat. Mohon isi kode siskeudes');goLoad({url:'/umum/user/setting'});</script>";
            } else {
                if ($model->load(Yii::$app->request->post())){
                    if($model->save()){ echo 1; } else echo 0;
                } else {
                    return $this->renderAjax('setting', [
                        'model' => $model,
                    ]);
                }
            }
        }
    }

    //profile
    public function actionProfile()
    {
        $foor = Sp3dProfileDesa::findOne(Yii::$app->user->identity->id);
        if($foor == null){
            $foor = [
                'user' =>    "3526160003",
                'foto_profile' =>    "../css/SuperAdmin-1-0-3/Template/1-0-3/img/profile-pic.png",
                'deskripsi' =>   "Deskripsi Akun Ini. Untuk mengisi deskripsi klik profile setting"
            ];
        } else {
            $foor['foto_profile'] = '../userfile/'.$foor['user'].'/'.$foor['foto_profile'];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }

    //detail-pengguna
    public function actionDetailPengguna($id)
    {
        $data['data1'] = User::findOne($id);
        $data['data2'] = Sp3dProfileDesa::findOne($id);
        
        return $this->renderAjax('detail-pengguna', [
            'data' => $data,
        ]);
    }
    //profile-setting
    public function actionProfileSetting()
    {
        $model = Sp3dProfileDesa::findOne(Yii::$app->user->identity->id);
        if($model == null) {
            $modelnew = new Sp3dProfileDesa();
            if ($modelnew->load(Yii::$app->request->post())){
                if($modelnew->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('profile-setting', [
                    'model' => $modelnew,
                ]);
            }   
        } else {
            if ($model->load(Yii::$app->request->post())){
                if($model->save()) echo 1; else echo 0;
            } else {
                return $this->renderAjax('profile-setting', [
                    'model' => $model,
                ]);
            }   
        }
    }

    public function actionInfo()
    {
        return $this->renderAjax('info');
    }

    public function actionDownload()
    {
        return $this->renderAjax('download');
    }

    public function actionPesan()
    {
        return $this->renderAjax('pesan');
    }

    public function actionPesanList()
    {
        return $this->renderAjax('pesan-list');
    }



    public function actionLog()
    {
        $dataProvider = Log::find()->where(['kd_pum' => Yii::$app->user->identity->id])->limit(15)->orderBy('tanggal_aksi DESC');
        return $this->renderAjax('log', [
            'dataProvider' => $dataProvider
        ]);
    }
}
