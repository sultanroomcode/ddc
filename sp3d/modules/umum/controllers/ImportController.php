<?php
namespace sp3d\modules\umum\controllers;
use Yii;
use yii\web\Controller;
use sp3d\models\MdbExploiter;
use sp3d\models\transaksi\TaRAB;
use yii\helpers\ArrayHelper;
use sp3d\models\transaksi\TaUserDesa;
use sp3d\models\transaksi\Sp3dVerifikasiKec;
use sp3d\models\transaksi\TaMdbUpload;
use sp3d\models\MdbRunRevert;
use sp3d\models\DesaCount;
use sp3d\models\KecamatanCount;
use sp3d\models\KabupatenCount;
use sp3d\models\ProvinsiCount;
use yii\web\Response;
use yii\db\Query;
/**
 * Default controller for the `umum` module
 */
class ImportController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $urlConnectServerWin;    
    // public $urlConnectServerWin =  '192.168.43.92';
    public function init()
    {
         $this->urlConnectServerWin =  Yii::$app->params['urlServer'];
    }

    public function actionMdbImport()
    {
        $model = new TaMdbUpload();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('mdb-import', [
                'model' => $model
            ]);
        }        
    }

    public function actionMdbImportDrop()
    {
        //check user_desa
        $modelcheck = TaUserDesa::findOne(Yii::$app->user->identity->id);

        if($modelcheck == null || $modelcheck->siskeu_id == ''){
            echo "<script>$.alert('Mohon isi kode siskeudes dulu');goLoad({url:'/umum/user/setting'});</script>";
            return false;
        }

        $model = new TaMdbUpload();
        if ($model->load(Yii::$app->request->post())) {
            // var_dump($model->upload());
            if($model->save()){
                echo 1;
            } else { 
                echo 0; var_dump($model->getErrors()); 
            }

        } else {
            return $this->renderAjax('mdb-import-drop', [
                'model' => $model,
                'modelcheck' => $modelcheck
            ]);
        }        
    }

    public function actionMdbExploiter()
    {
        $param = Yii::$app->request->get();

        $model = TaMdbUpload::findOne(['user' => $param['user'], 'id' => $param['id']]);
        if($model != null){
            $file_location = './userfile/'.$param['user'].'/db/'.$model->nama_file;
            $mdb = new MdbExploiter($param['tahun'], $param['user'], $param['id'], $file_location);

            if($param['activity'] == 'parse'){
                $mdb->parse();

                //update model
                $model->date_send = date('Y-m-d H:i:s');
                $model->f_ftp_send = 'Y';
                $model->save();
                return '<script>$.alert("Database sudah diekstrak");goLoad({url:"/umum/import/new-flow-mdb-list"});</script>';
            } else if($param['activity'] == 'run-sql'){
                $mdb->runSQL();

                return '<script>$.alert("Data sudah diimport");goLoad({url:"/umum/import/new-flow-mdb-list"});</script>';
            }
        }
    }

    public function actionNewFlowMdbList()
    {
        //check user_desa
        $modelcheck = TaUserDesa::findOne(Yii::$app->user->identity->id);

        if($modelcheck == null || $modelcheck->siskeu_id == ''){
            echo "<script>$.alert('Mohon isi kode siskeudes dulu');goLoad({url:'/umum/user/setting'});</script>";
            return false;
        }

        $model = TaMdbUpload::find()->where(['user' => Yii::$app->user->identity->id])->orderBy('date_upload DESC');

        return $this->renderAjax('new-flow-mdb-list', [
            'model' => $model,
            'modelcheck' => $modelcheck
        ]);
    }

    public function actionMdbList()
    {
        //check user_desa
        $modelcheck = TaUserDesa::findOne(Yii::$app->user->identity->id);

        if($modelcheck == null || $modelcheck->siskeu_id == ''){
            echo "<script>$.alert('Mohon isi kode siskeudes dulu');goLoad({url:'/umum/user/setting'});</script>";
            return false;
        }

        $model = TaMdbUpload::find()->where(['user' => Yii::$app->user->identity->id])->orderBy('date_upload DESC');

        return $this->renderAjax('mdb-list', [
            'model' => $model,
            'modelcheck' => $modelcheck
        ]);
    }

    public function actionMdbSendCurl()
    {
        $param = Yii::$app->request->get();
        $model = TaMdbUpload::findOne(['user' => Yii::$app->user->identity->id, 'f_ftp_send' => 'N', 'id' => $param['id']]);
        if($model->checkExistData() == false){
            $target_url = $this->urlConnectServerWin.'/sp3dclient/receiver.php';
            // $target_url = 'http://localhost/yii/siapatakut_sv/sp3dsiskeudes/receiver.php';
            $dbmde = $model->user;
            $file_name_with_full_path = './userfile/'.$dbmde.'/db/'.$model->nama_file;
            
            if (function_exists('curl_file_create')) { // php 5.5+
                $cFile = curl_file_create($file_name_with_full_path);
            } else { // 
                $cFile = '@' . realpath($file_name_with_full_path);
            }
            $post = [
                'id' => $model->id,
                // 'extra_info' => time(),
                'directory' => $dbmde,
                'file_contents'=> $cFile
            ];
            try {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$target_url);
                curl_setopt($ch, CURLOPT_POST,1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                $status  = [];
                if($output !== ''){
                    $status = json_decode($output, true);//when true will return assoc arry
                    // var_dump($output);
                    // var_dump($status);
                } else {
                    // var_dump($output);
                    // var_dump($status);
                    $status = null;
                }
                

                if($status != null && $status['num'] == 1){
                    // $model->tahun = $status['data']['tahun'];//pakai tahun yang ada di form import-drop
                    $model->f_ftp_send = 'Y';
                    $model->date_send = $status['data']['tgl_kirim'];
                    $model->save();

                    echo '<script>$.alert("'.$status['message'].' pada '.date("d-F-Y H:i", strtotime($status['data']['tgl_kirim'])).'");goLoad({url:"/umum/import/mdb-list"});</script>';
                } else {
                    echo '<script>$.alert("'.$status['message'].'");goLoad({url:"/umum/import/mdb-list"});</script>';
                }
                if (FALSE === $output)
                throw new \yii\base\Exception(curl_error($ch), curl_errno($ch));

                curl_close ($ch);
            } catch(Exception $e) {

                trigger_error(sprintf(
                    'Curl failed with error #%d: %s',
                    $e->getCode(), $e->getMessage()),
                    E_USER_ERROR);

            }
        } else {
            echo '<script>$.alert("Data sudah ada");goLoad({url:"/umum/import/mdb-list"});</script>';
        }
    }
    
    //parsing
    public function actionMdbCheck()
    {
        $param = Yii::$app->request->get();
        $model = TaMdbUpload::findOne(['user' => $param['user'], 'f_ftp_send' => 'Y', 'f_ftp_back' => 'N', 'id' => $param['id']]);
        $strStatus = 0;

        if($model !== null){
            // create curl resource
            $ch = curl_init();
            // set url
            $parameter = 'tahun='.$model->tahun.'&sisuser='.$model->siskeudes->siskeu_id.'&imp_id='.$model->id.'&user='.$model->user.'&name='.$model->nama_file;
            // var_dump($parameter);
            // http://192.168.43.92/sp3dclient/running-odbc.php?tahun=2017&sisuser=08.08.&imp_id=1522291178&user=3526160003&name=1522291189-3526160003.mde //example
            $url = $this->urlConnectServerWin."/sp3dclient/running-odbc.php?".$parameter;
            // $url = "http://localhost/yii/siapatakut_sv/sp3dsiskeudes/running-odbc-local.php?".$parameter;
            // http://localhost/yii/siapatakut_sv/sp3dsiskeudes/running-odbc-local.php?tahun=2017&sisuser=08.08.&imp_id=1522291178&user=3526160003&name=1522291189-3526160003.mde //example

            curl_setopt($ch, CURLOPT_URL, $url);
            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // $output contains the output string
            $outputsql = curl_exec($ch);
            if($outputsql != ''){
                //var_dump($outputsql);
                $badchar=array(
                    // control characters
                    chr(0), chr(1), chr(2), chr(3), chr(4), chr(5), chr(6), chr(7), chr(8), chr(9), chr(10),
                    chr(11), chr(12), chr(13), chr(14), chr(15), chr(16), chr(17), chr(18), chr(19), chr(20),
                    chr(21), chr(22), chr(23), chr(24), chr(25), chr(26), chr(27), chr(28), chr(29), chr(30),
                    chr(31),'…',
                    // non-printing characters
                    chr(127)
                );

                //replace the unwanted chars
                $outputsql = str_replace($badchar, ' ', $outputsql);
                $outputsql = preg_replace('/[\x85]/', '', $outputsql);
                $outputsql = preg_replace('/[\xF8]/', '', $outputsql);
                $outputsql = preg_replace('/[\xD8]/', '', $outputsql);
                $outputsql = preg_replace('/[\xBD]/', '', $outputsql);
                $outputsql = preg_replace('/[\x92]/', '', $outputsql);
                $outputsql = preg_replace('/[\xB3]/', '', $outputsql);
                $outputsql = preg_replace('/[\xB2]/', '', $outputsql);
                $outputsql = preg_replace('/[\x96]/', '', $outputsql);
                
                $xpSQL = explode(';', $outputsql);
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {
                    foreach($xpSQL as $runsql){
                        $connection->createCommand($runsql)
                        ->execute();
                    }
                    
                    $model->f_ftp_back = 'Y';
                    $model->date_back = date('Y-m-d H:i:s');
                    $model->save();

                    //add kecamatan verification
                    /*$modelvr = new Sp3dVerifikasiKec();
                    $modelvr->imp_id = $model->id;
                    $modelvr->tahun = $model->tahun;
                    $modelvr->kd_desa = Yii::$app->user->identity->id;
                    $modelvr->nik = '---';
                    $modelvr->verified_status = 0;
                    $modelvr->save();*/
                    $transaction->commit();
                    $strStatus = 1;
                } catch(Exception $e) {
                    var_dump($e);
                    $transaction->rollback();
                    $strStatus = 0;
                }
            } else {
                $strStatus = 0;
            }
            // close curl resource to free up system resources
            curl_close($ch);
        }

        if($strStatus == 1){
            // echo '<script>$.alert("Data Keuangan Desa yang telah disetujui telah dikirimkan ke kecamatan untuk di periksa");</script>';
        } else {
            echo '<script>$.alert("Gagal mengirim data");</script>';
        }        

        // return $this->renderAjax('mdb-refer-list', [
        //     'strStatus' => $strStatus
        // ]);
        if(isset($param['counting']) && $param['counting'] == 1 && $model != null){
            echo '<script>goLoad({url:"/umum/counting/mdb-normalize-count?tahun='.$model->tahun.'&kd_desa='.$model->user.'"});</script>';
        } else {
            echo '<script>goLoad({url:"/umum/import/mdb-list"});</script>';
        }

        // return true;
    }

    public function actionMdbDelete()
    {
        $param = Yii::$app->request->get();
        
        $modelrvt = new MdbRunRevert;
        $modelrvt->initial(Yii::$app->user->identity->id, $param['id'], $this->urlConnectServerWin);
        if($modelrvt->doRemove()){
            echo '<script>$.alert("Sudah di hapus");goLoad({url:"/umum/import/new-flow-mdb-list"});</script>';
        }
    }

    public function actionMdbRunRevert()
    {
        $param = Yii::$app->request->get();
        
        $modelrvt = new MdbRunRevert;
        $modelrvt->initial(Yii::$app->user->identity->id, $param['id'], $this->urlConnectServerWin);
        if($modelrvt->doRevert()){
            echo '<script>$.alert("Sudah di revert");goLoad({url:"/umum/import/new-flow-mdb-list"});</script>';
        }
    }

    /* [canceled]
    public function actionMdbVerifikasi()
    {
        $param = Yii::$app->request->get();
        $model = Sp3dVerifikasiKec::findOne(['imp_id' => $param['id'], 'kd_desa' => $param['user']]);
        $model->verifiedQuery();
        echo "<script>$.alert('Data terkirim telah di ubah menjadi terverifikasi'); goLoad({url:'/umum/import/mdb-normalize-count?tahun=".$param['tahun']."&kd_desa=".$param['user']."'});</script>";
    }*/

    public function actionExcellImport()
    {
        $model = new TaMdbUpload();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('excell-import', [
                'model' => $model
            ]);
        }        
    }

    public function actionExcellList()
    {
        $model = TaMdbUpload::find()->where(['user' => Yii::$app->user->identity->id])->orderBy('date_upload DESC');
        
        return $this->renderAjax('excell-list', [
            'model' => $model
        ]);
    }

    public function actionUpdateTahun(){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("UPDATE Ta_RAB SET Tahun = '2017';");
        $command->execute();

        $command = $connection->createCommand("UPDATE Ta_RABRinci SET Tahun = '2017';");
        $command->execute();

        $command = $connection->createCommand("UPDATE Ta_Kegiatan SET Tahun = '2017';");
        $command->execute();

        echo time();
    }

    public function actionApiSync()
    {
        $model = new TaMdbUpload();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('api-sync', [
                'model' => $model
            ]);
        }  
    }

    public function actionProvinsiMdbList()
    {
        return $this->renderAjax('provinsi-mdb-list');
    }

    public function actionMdbData()
    {
        //khusus selain desa
        $dttable = Yii::$app->request->get();

        $tbl = [
            0 => 'user',
            1 => 'tahun',
            2 => 'f_ftp_send',
            3 => 'f_ftp_back',
            4 => 'nama_file',
            5 => 'date_upload',
            6 => 'date_send',
            7 => 'date_back',
        ];

        // var_dump($dttable);
        // print_r($dttable);
        $data = TaMdbUpload::find()->select(['user', 'tahun', 'f_ftp_send', 'f_ftp_back', 'nama_file', 'date_upload', 'date_send', 'date_back']);
        $datap = $data->offset($dttable['start'])->limit($dttable['length']);//data paging

        if($dttable['kabupaten'] != '-' && $dttable['kecamatan'] == '' && $dttable['desa'] == ''){
            $datap->where(['LIKE', 'user', $dttable['kabupaten'].'%', false]);
        }

        if($dttable['kecamatan'] != '' && $dttable['desa'] == ''){
            $datap->where(['LIKE', 'user', $dttable['kecamatan'].'%', false]);
        }

        if($dttable['desa'] != ''){
            $datap->where(['user' => $dttable['desa']]);
        }

        if($dttable['search']['value'] != ''){
            $datap->andWhere(['LIKE', 'user', $dttable['search']['value']]);
        }

        if(count($dttable['order']) > 1){
            for($i =0; $i < count($dttable['order']); $i++)
            {
                $datap->orderBy($tbl[$dttable['order'][$i]['column']].' '.strtoupper($dttable['order'][$i]['dir']));
            }
        } else {
            $datap->orderBy($tbl[$dttable['order'][0]['column']].' '.strtoupper($dttable['order'][0]['dir']));
        }

        $dataserve = $datap->all();
        $dataServer = ArrayHelper::toArray($dataserve, ['sp3d\models\transaksi\TaMdbUpload' => 
            [
            'user',
            'tahun',
            // 'siskeu_id',
            'f_ftp_send',
            'f_ftp_back',
            'date_upload',
            'date_send',
            'date_back',
            'action' => function($p){
                return '<a href="'.Yii::getAlias('@sp3durl/userfile/'.$p->user.'/db/'.$p->nama_file).'" class="btn btn-sm">Download MDE</a>';
            }
        ]], true);

        $foor = [
            'draw' => (int) $dttable['draw'],
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $datap->count(),
            'data' => $dataServer
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }
}

