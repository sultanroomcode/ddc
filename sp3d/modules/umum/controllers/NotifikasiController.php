<?php
namespace sp3d\modules\umum\controllers;

use Yii;
use sp3d\models\AuthAssignment;
use sp3d\models\User;
use sp3d\models\Log;
use sp3d\modules\umum\models\DdcNotifikasi;
use sp3d\models\transaksi\Sp3dVerifikasiKec;
use sp3d\models\transaksi\Sp3dVerifikasiRevisiKec;
use sp3d\models\transaksi\Sp3dVerifikatorKec;
use sp3d\models\IsiPengguna;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;

class NotifikasiController extends Controller
{
    public function beforeAction($action)
    {
        if(Yii::$app->user->identity == null){
            return $this->redirect(['/site/login']);
            exit();
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionReadNotifikasi($id, $time)
    {
        $m = DdcNotifikasi::findOne(['n_from' => $id, 'send_at' => $time]);
        $m->status_read = 'R';
        $m->save();
    }

    public function actionGetNotifikasi()
    {
        $id = Yii::$app->user->identity->id;
        $c = DdcNotifikasi::find()->where(['n_to' => $id])->andWhere(['status_read' => 'S']);
        // echo $c->createCommand()->getRawSql();
        if($c->count() > 0){
            echo '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/notifikasi/index\'})"><small class="label label-danger"><i class="fa fa-bullhorn"></i> ada '.$c->count().' notifikasi baru</small></a>';
        }
    }
}
