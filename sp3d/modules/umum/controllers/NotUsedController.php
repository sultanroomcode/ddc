<?php
namespace sp3d\modules\umum\controllers;

use Yii;
use sp3d\models\AuthAssignment;
use sp3d\models\User;
use sp3d\models\transaksi\TaUserDesa;
use sp3d\models\transaksi\Sp3dProfileDesa;
use sp3d\models\IsiPengguna;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
- index
- reset-password
- management
- management-data
- password-desa
- setting
- user-detail
- profile
- profile-setting
- pingDomain - function
- info
- pesan
- log
- ganti-password
- view
- generate-insert-baru
- admin-baru
- dispemas-baru
- test-baru
- buat-baru
- create
- update
- delete
- findModel - function
 */
class NotUsedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    
    public function pingDomain($domain){
        /*$status    = 0;
        if(fsockopen ($domain, 80, $errno, $errstr, 10)){
            $starttime = microtime(true);
            $file      = fsockopen ($domain, 80, $errno, $errstr, 10);
            $stoptime  = microtime(true);

            if (!$file) $status = -1;  // Site is down
            else {
                fclose($file);
                $status = ($stoptime - $starttime) * 1000;
                $status = floor($status);
            }
        } else {
            $status = 0;
        }
        return $status;*/

        $fp = fsockopen($domain, 80, $errno, $errstr, 30);
        if (!$fp) {
            return "$errstr ($errno)<br />\n";
        } else {
            $out = "GET / HTTP/1.1\r\n";
            $out .= "Host: $domain\r\n";
            $out .= "Connection: Close\r\n\r\n";
            fwrite($fp, $out);
            while (!feof($fp)) {
                $getout = fgets($fp, 128);
            }
            fclose($fp);
            return $getout;
        }
    }

    public function actionPasswordDesa()
    {
        //khusus desa
        echo Yii::$app->security->generatePasswordHash('enteraja');
    }

    public function actionGenerateInsertBaru($page=1)
    {
        $page = (int) $page;
        $file = fopen('dbdes/data-'.$page.'.csv', 'r');
        $limit = 1;
        // $sql_user = 'INSERT INTO user (`id`,`type`,`username`,`description`,`auth_key`,`password_default`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`) VALUES ';
        $row = 0;
        $rowdata = [];
        while(($data = fgetcsv($file, 10000, ';')) !== false){
            $num = count($data);
            $row++;
            if($num != 3) continue;
            echo "Baris $row<br>";
            $type = ((strlen(trim($data[1])) == 7)?'kecamatan':'desa'); 
            echo $data[0].'<br>';//nama
            echo $data[1].'<br>';//id
            echo $data[2].'<hr>';//pass
            // $sql_user .= "('$data[1]', '$type', '$data[1]', '".strtoupper($type)." $data[0]', 'auth', 'pass', 'passhash', 'pass', 'email', 10, '$data[2]'),";

            $transaction = Yii::$app->db->beginTransaction();

            try {
                $user = new User();
                $user->id = $data[1];
                $user->username = $data[1];
                $user->description = strtoupper($type).' '.$data[0];
                $user->type = $type;
                $user->email = $data[1].'@dpmd.jatimprov.go.id';
                $user->password_default = trim($data[2]);
                $user->setPassword(trim($data[2]));
                $user->generateAuthKey();
                if(!$user->save(false)) throw new Exception('Gagal isi user');
                
                    //membuat assigment baru
                $u_a = new AuthAssignment();
                $u_a->item_name = $type;
                $u_a->user_id = (string) $user->id;
                $u_a->created_at = time();
                if(!$u_a->save()) throw new Exception('Gagal isi assigment '. print_r($u_a->errors));
                
                $transaction->commit();
                Yii::info('berhasil bro');
            } catch(Exception $e){
                Yii::warning('Gagal bro '.$data[0]. $e->getMessage());
                $transaction->rollBack();
            }
        }

        // $sql_user .= ';';
        // echo $sql_user;
        fclose($file);
        if($page < $limit){
            $page++;
            echo '<script>document.location="?page='.$page.'"; </script>';
        } else {
            echo "Selesai";
        }
    }
    
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    

    public function actionAdminBaru()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $user = new User();
            $user->username = 'admin';
            $user->email = 'admin@dispemas.tubankab.go.id';
            $user->setPassword('enteraja');
            $user->generateAuthKey();
            if(!$user->save()) throw new Exception('Gagal isi user');
            
                //membuat assigment baru
            $u_a = new AuthAssignment();
            $u_a->item_name = 'admin';
            $u_a->user_id = (string) $user->id;
            $u_a->created_at = time();
            if(!$u_a->save()) throw new Exception('Gagal isi assigment '. print_r($u_a->errors));
            
            $transaction->commit();
            Yii::info('berhasil bro');
        } catch(Exception $e){
             Yii::warning('Gagal bro '. $e->getMessage());
            $transaction->rollBack();
        }
        
        return $this->render('test');
    }

    public function actionDispemasBaru()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $user = new User();
            $user->username = 'dispemas';
            $user->email = 'anon@dispemas.tubankab.go.id';
            $user->setPassword('dispemasaja');
            $user->generateAuthKey();
            if(!$user->save()) throw new Exception('Gagal isi user');
            
                //membuat assigment baru
            $u_a = new AuthAssignment();
            $u_a->item_name = 'dpmdkb';
            $u_a->user_id = (string) $user->id;
            $u_a->created_at = time();
            if(!$u_a->save()) throw new Exception('Gagal isi assigment '. print_r($u_a->errors));
            
            $transaction->commit();
            Yii::info('berhasil bro');
        } catch(Exception $e){
             Yii::warning('Gagal bro '. $e->getMessage());
            $transaction->rollBack();
        }
        
        return $this->render('test');
    }

    public function actionTestBaru()
    {
        $model = User::findOne(['username' => 'jamprong']);
        if($model->chapter == null){
            $chapter = new TaUserDesa();
            $chapter->Id = $model->id;
            $chapter->Kd_Kec = '01';
            $chapter->Kd_Desa = '01.03';
            $chapter->save();
        }
    }

    public function actionBuatBaru()
    {
        $model = new IsiPengguna();

        if ($model->load(Yii::$app->request->post())){
            if($model->savePengguna()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('baru', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){

            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()) echo 1; else echo 0;
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
