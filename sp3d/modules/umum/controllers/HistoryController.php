<?php
namespace sp3d\modules\umum\controllers;
use Yii;
use yii\web\Controller;
use sp3d\models\Log;
/**
 * Default controller for the `umum` module
 */
class HistoryController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLine()
    {
        $dataProvider = Log::find();
        return $this->renderAjax('line', [
            'dataProvider' => $dataProvider,
        ]);
    }
}

