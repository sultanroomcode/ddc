<?php
namespace sp3d\modules\umum\controllers;

use yii\web\Controller;

/**
 * Default controller for the `umum` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionFrontBoard()
    {
        return $this->renderAjax('front-board');
    }

    public function actionDashboard()
    {
        return $this->renderAjax('dashboard');
    }
}
