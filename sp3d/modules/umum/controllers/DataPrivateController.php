<?php
namespace sp3d\modules\umum\controllers;

use yii\web\Controller;
use sp3d\models\User;
use sp3d\models\DesaCount;
use sp3d\models\KecamatanCount;
use sp3d\models\KabupatenCount;
use sp3d\models\ProvinsiCount;
use yii\web\Response;
use Yii;
/**
 * Default controller for the `umum` module
 */
class DataPrivateController extends Controller
{
    public function actionDataInfoDetail($user)
    {
        $kdcount = strlen($user);
        $provd = strtoupper('Jawa Timur');
        switch ($kdcount) {
            case 2://provinsi
                $provd = User::findOne($user);

                $foo = [
                    'nama_desa' => '',
                    'nama_kecamatan' => '',
                    'nama_kabupaten' => '',
                    'nama_provinsi' => $provd->description,
                    'kd_siskeudes' => '-'
                ];
            break;

            case 4://kabupaten
                $kabd = User::findOne($user);

                $foo = [
                    'nama_desa' => '',
                    'nama_kecamatan' => '',
                    'nama_kabupaten' => $kabd->description,
                    'nama_provinsi' => $provd,
                    'kd_siskeudes' => '-'
                ];
            break;

            case 7://kecamatan
                $kabd = User::findOne(substr($user, 0, 4));
                $kecd = User::findOne($user);

                $foo = [
                    'nama_desa' => '',
                    'nama_kecamatan' => $kecd->description,
                    'nama_kabupaten' => $kabd->description,
                    'nama_provinsi' => $provd,
                    'kd_siskeudes' => '-'
                ];
            break;
            
            case 10://desa
            default:
                $kabd = User::findOne(substr($user, 0, 4));
                $kecd = User::findOne(substr($user, 0, 7));
                $desd = User::findOne($user);

                $foo = [
                    'nama_desa' => $desd->description,
                    'nama_kecamatan' => $kecd->description,
                    'nama_kabupaten' => $kabd->description,
                    'nama_provinsi' => $provd,
                    'kd_siskeudes' => (isset($desd->chapter->siskeu_id))?$desd->chapter->siskeu_id:'--'
                ];
            break;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $foo;
    }

    public function actionDataCountAccess($tahun, $kd)
    {
        //querying based on year
        $kdcount = strlen($kd);

        switch ($kdcount) {
            case 2:
                $dc = ProvinsiCount::findOne(['kd_provinsi' => $kd, 'tahun' => $tahun]);
            break;

            case 4:
                $dc = KabupatenCount::findOne(['kd_kabupaten' => $kd, 'tahun' => $tahun]);
            break;

            case 7:
                $dc = KecamatanCount::findOne(['kd_kecamatan' => $kd, 'tahun' => $tahun]);
            break;
            
            case 10:
            default:
                $dc = DesaCount::findOne(['kd_desa' => $kd, 'tahun' => $tahun]);
            break;
        }
        

        if($dc != null){
            $foo = [
                'pendapatan' => $dc->nf($dc->dana_pendapatan),
                'belanja' => $dc->nf($dc->dana_rab),
                'pembiayaan' => $dc->nf($dc->dana_penerimaan),
            ];
        } else {
            $foo = [
                'pendapatan' => 0,
                'belanja' => 0,
                'pembiayaan' => 0,
            ];
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $foo;
    }
}