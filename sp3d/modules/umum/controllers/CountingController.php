<?php
namespace sp3d\modules\umum\controllers;
use Yii;
use yii\web\Controller;
use sp3d\models\transaksi\TaRAB;
use sp3d\models\transaksi\TaUserDesa;
use sp3d\models\transaksi\Sp3dVerifikasiKec;
use sp3d\models\transaksi\TaMdbUpload;
use sp3d\models\MdbRunRevert;
use sp3d\models\DesaCount;
use sp3d\models\KecamatanCount;
use sp3d\models\KabupatenCount;
use sp3d\models\ProvinsiCount;
use yii\db\Query;
/**
 * Default controller for the `umum` module
 */
class CountingController extends Controller
{
    public function actionProvinsiNormalizePage()
    {
        return $this->renderAjax('provinsi-normalize-page');
    }
    
    public function actionProvinsiNormalizeCount($tahun=null)
    {
        $kd_provinsi = '35';
        echo "<div class=\"row\">
        <div class=\"col-md-12\">
            <!-- Main Chart -->
            <div class=\"tile\">
                <h2 class=\"tile-title\">Normalisasi Hitungan</h2>
                <div class=\"p-10\">
                <table class='tile table table-bordered table-striped'>";
        $tahun = ($tahun == null)?date('Y'):$tahun;//10
        
        $mprov = ProvinsiCount::findOne(['tahun' => $tahun, 'kd_provinsi' => $kd_provinsi]);
        if($mprov == null){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("INSERT INTO `provinsi_count`(`tahun`, `kd_provinsi`,`dana_anggaran`, `dana_pencairan`, `dana_kegiatan`, `dana_mutasi`, `dana_pajak`, `dana_pendapatan`, `dana_rab`, `dana_penerimaan`, `dana_pengeluaran`, `dana_spj`, `dana_spp`, `dana_sts`, `dana_tbp`, `data_pencairan`, `data_anggaran`, `data_kegiatan`, `data_bidang`, `data_mutasi`, `data_pajak`, `data_pendapatan`, `data_rab`, `data_penerimaan`, `data_pengeluaran`, `data_spj`, `data_spp`, `data_sts`, `data_tbp`) VALUES ('".$tahun."','".$kd_provinsi."',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");

            $command->execute();
            echo '<tr><td>Inisiasi hitungan data APBDES '.$tahun.' di tingkat Provinsi telah Dibuat</td></tr>';
        } else {
            //count pendapatan
            $mpdpt = TaRAB::find()->where(['Tahun' => $tahun])->andWhere(['like', 'Kd_Rincian', '4.%', false]);
            //count RAB
            $mrab = TaRAB::find()->where(['Tahun' => $tahun])->andWhere(['like', 'Kd_Rincian', '5.%', false]);
            //count penerimaan
            $min = TaRAB::find()->where(['Tahun' => $tahun])->andWhere(['like', 'Kd_Rincian', '6.1.%', false]);
            //count pengeluaran
            $mout = TaRAB::find()->where(['Tahun' => $tahun])->andWhere(['like', 'Kd_Rincian', '6.2.%', false]);

            $mprov->dana_pendapatan = $mpdpt->sum('Anggaran');
            $mprov->data_pendapatan = $mpdpt->count();
            $mprov->dana_rab = $mrab->sum('Anggaran');
            $mprov->data_rab = $mrab->count();
            $mprov->dana_penerimaan = $min->sum('Anggaran');
            $mprov->data_penerimaan = $min->count();
            $mprov->dana_pengeluaran = ($mout->sum('Anggaran') == null)?0:$mout->sum('Anggaran') ;
            $mprov->data_pengeluaran = $mout->count();
            $mprov->save(false);

            echo '<tr><td>Hitungan data APBDES '.$tahun.' di tingkat Provinsi telah dihitung ulang</td></tr>';
        }
    }

    public function actionMdbNormalizeCount($tahun=null, $kd_desa=null)
    {
        echo "<div class=\"row\">
        <div class=\"col-md-12\">
            <!-- Main Chart -->
            <div class=\"tile\">
                <h2 class=\"tile-title\">Perhitungan Kembali Dana APBDES</h2>
                <div class=\"p-10\">";
            if($kd_desa == null){
                //diakses dari desa
                echo "<a href=\"javascript:void(0)\" onclick=\"goLoad({url: '/umum/import/mdb-list'})\" class=\"btn btn-danger\" title=\"List Database\"><i class=\"fa fa-database\"></i></a>";
            }
        echo "<table class='tile table table-bordered table-striped'>";
        $tahun = ($tahun == null)?date('Y'):$tahun;//10
        $kd_desa_bps = (string) ($kd_desa != null)?$kd_desa:Yii::$app->user->identity->id;//10
        $kd_kecamatan = (string) substr($kd_desa_bps, 0, 7);
        $kd_kabupaten = (string) substr($kd_desa_bps, 0, 4);
        $kd_provinsi = (string) substr($kd_desa_bps, 0, 2);

        //if not isset
        $mdesa = DesaCount::findOne(['tahun' => $tahun, 'kd_desa' => $kd_desa_bps]);
        if($mdesa == null){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("INSERT INTO `desa_count`(`tahun`, `kd_provinsi`, `kd_kabupaten`, `kd_kecamatan`, `kd_desa`, `dana_anggaran`, `dana_pencairan`, `dana_kegiatan`, `dana_mutasi`, `dana_pajak`, `dana_pendapatan`, `dana_rab`, `dana_penerimaan`, `dana_pengeluaran`, `dana_spj`, `dana_spp`, `dana_sts`, `dana_tbp`, `data_pencairan`, `data_anggaran`, `data_kegiatan`, `data_bidang`, `data_mutasi`, `data_pajak`, `data_pendapatan`, `data_rab`, `data_penerimaan`, `data_pengeluaran`, `data_spj`, `data_spp`, `data_sts`, `data_tbp`) VALUES ('".$tahun."','".$kd_provinsi."','".$kd_kabupaten."','".$kd_kecamatan."','".$kd_desa_bps."',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");

            $command->execute();

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Desa telah dibuat</td></tr>';
            $mpdpt = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '4.%', false]);
            //count RAB
            $mrab = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '5.%', false]);
            //count penerimaan
            $min = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '6.1.%', false]);
            //count pengeluaran
            $mout = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '6.2.%', false]);
            //reinit
            $mdesa = DesaCount::findOne(['tahun' => $tahun, 'kd_desa' => $kd_desa_bps]);
            $mdesa->dana_pendapatan = $mpdpt->sum('Anggaran');
            $mdesa->data_pendapatan = $mpdpt->count();
            $mdesa->dana_rab = $mrab->sum('Anggaran');
            $mdesa->data_rab = $mrab->count();
            $mdesa->dana_penerimaan = $min->sum('Anggaran');
            $mdesa->data_penerimaan = $min->count();
            $mdesa->dana_pengeluaran = ($mout->sum('Anggaran') == null)?0:$mout->sum('Anggaran') ;
            $mdesa->data_pengeluaran = $mout->count();
            $mdesa->save(false);

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Desa telah diupdate</td></tr>';
        } else {
            //count pendapatan
            $mpdpt = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '4.%', false]);
            //count RAB
            $mrab = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '5.%', false]);
            //count penerimaan
            $min = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '6.1.%', false]);
            //count pengeluaran
            $mout = TaRAB::find()->where(['user_id' => $kd_desa_bps,'Tahun' => $tahun, 'f_verified' => 1])->andWhere(['like', 'Kd_Rincian', '6.2.%', false]);

            $mdesa->dana_pendapatan = $mpdpt->sum('Anggaran');
            $mdesa->data_pendapatan = $mpdpt->count();
            $mdesa->dana_rab = $mrab->sum('Anggaran');
            $mdesa->data_rab = $mrab->count();
            $mdesa->dana_penerimaan = $min->sum('Anggaran');
            $mdesa->data_penerimaan = $min->count();
            $mdesa->dana_pengeluaran = ($mout->sum('Anggaran') == null)?0:$mout->sum('Anggaran') ;
            $mdesa->data_pengeluaran = $mout->count();
            $mdesa->save(false);

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Desa telah diupdate</td></tr>';
        }
        //kecamatan
        $mkec = KecamatanCount::findOne(['tahun' => $tahun, 'kd_kecamatan' => $kd_kecamatan]);
        if($mkec == null){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("INSERT INTO `kecamatan_count`(`tahun`, `kd_provinsi`, `kd_kabupaten`, `kd_kecamatan`, `dana_anggaran`, `dana_pencairan`, `dana_kegiatan`, `dana_mutasi`, `dana_pajak`, `dana_pendapatan`, `dana_rab`, `dana_penerimaan`, `dana_pengeluaran`, `dana_spj`, `dana_spp`, `dana_sts`, `dana_tbp`, `data_pencairan`, `data_anggaran`, `data_kegiatan`, `data_bidang`, `data_mutasi`, `data_pajak`, `data_pendapatan`, `data_rab`, `data_penerimaan`, `data_pengeluaran`, `data_spj`, `data_spp`, `data_sts`, `data_tbp`) VALUES ('".$tahun."','".$kd_provinsi."','".$kd_kabupaten."','".$kd_kecamatan."',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");

            $command->execute();

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Kecamatan telah dibuat</td></tr>';

            $dsCount = DesaCount::find()->where(['Tahun' => $tahun, 'kd_kecamatan' => $kd_kecamatan]);
            //re init
            $mkec = KecamatanCount::findOne(['tahun' => $tahun, 'kd_kecamatan' => $kd_kecamatan]);
            $mkec->dana_pendapatan = $dsCount->sum('dana_pendapatan');
            $mkec->data_pendapatan = $dsCount->sum('data_pendapatan');
            $mkec->dana_rab = $dsCount->sum('dana_rab');
            $mkec->data_rab = $dsCount->sum('data_rab');
            $mkec->dana_penerimaan = $dsCount->sum('dana_penerimaan');
            $mkec->data_penerimaan = $dsCount->sum('data_penerimaan');
            $mkec->dana_pengeluaran = $dsCount->sum('dana_pengeluaran');
            $mkec->data_pengeluaran = $dsCount->sum('data_pengeluaran');
            $mkec->save(false);

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Kecamatan telah diperbaharui</td></tr>';
        } else {
            //count pendapatan
            $dsCount = DesaCount::find()->where(['Tahun' => $tahun, 'kd_kecamatan' => $kd_kecamatan]);

            $mkec->dana_pendapatan = $dsCount->sum('dana_pendapatan');
            $mkec->data_pendapatan = $dsCount->sum('data_pendapatan');
            $mkec->dana_rab = $dsCount->sum('dana_rab');
            $mkec->data_rab = $dsCount->sum('data_rab');
            $mkec->dana_penerimaan = $dsCount->sum('dana_penerimaan');
            $mkec->data_penerimaan = $dsCount->sum('data_penerimaan');
            $mkec->dana_pengeluaran = $dsCount->sum('dana_pengeluaran');
            $mkec->data_pengeluaran = $dsCount->sum('data_pengeluaran');
            $mkec->save(false);

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Kecamatan telah diperbaharui</td></tr>';
        }

        $mkab = KabupatenCount::findOne(['tahun' => $tahun, 'kd_kabupaten' => $kd_kabupaten]);
        if($mkab == null){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("INSERT INTO `kabupaten_count`(`tahun`, `kd_provinsi`, `kd_kabupaten`,`dana_anggaran`, `dana_pencairan`, `dana_kegiatan`, `dana_mutasi`, `dana_pajak`, `dana_pendapatan`, `dana_rab`, `dana_penerimaan`, `dana_pengeluaran`, `dana_spj`, `dana_spp`, `dana_sts`, `dana_tbp`, `data_pencairan`, `data_anggaran`, `data_kegiatan`, `data_bidang`, `data_mutasi`, `data_pajak`, `data_pendapatan`, `data_rab`, `data_penerimaan`, `data_pengeluaran`, `data_spj`, `data_spp`, `data_sts`, `data_tbp`) VALUES ('".$tahun."','".$kd_provinsi."','".$kd_kabupaten."',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");

            $command->execute();
            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Kabupaten telah dibuat</td></tr>';

            $kcCount = KecamatanCount::find()->where(['Tahun' => $tahun, 'kd_kabupaten' => $kd_kabupaten]);
            //reinit
            $mkab = KabupatenCount::findOne(['tahun' => $tahun, 'kd_kabupaten' => $kd_kabupaten]);
            $mkab->dana_pendapatan = $kcCount->sum('dana_pendapatan');
            $mkab->data_pendapatan = $kcCount->sum('data_pendapatan');
            $mkab->dana_rab = $kcCount->sum('dana_rab');
            $mkab->data_rab = $kcCount->sum('data_rab');
            $mkab->dana_penerimaan = $kcCount->sum('dana_penerimaan');
            $mkab->data_penerimaan = $kcCount->sum('data_penerimaan');
            $mkab->dana_pengeluaran = $kcCount->sum('dana_pengeluaran');
            $mkab->data_pengeluaran = $kcCount->sum('data_pengeluaran');
            $mkab->save(false);

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Kabupaten telah diperbaharui</td></tr>';
        } else {
            //count pendapatan
            $kcCount = KecamatanCount::find()->where(['Tahun' => $tahun, 'kd_kabupaten' => $kd_kabupaten]);

            $mkab->dana_pendapatan = $kcCount->sum('dana_pendapatan');
            $mkab->data_pendapatan = $kcCount->sum('data_pendapatan');
            $mkab->dana_rab = $kcCount->sum('dana_rab');
            $mkab->data_rab = $kcCount->sum('data_rab');
            $mkab->dana_penerimaan = $kcCount->sum('dana_penerimaan');
            $mkab->data_penerimaan = $kcCount->sum('data_penerimaan');
            $mkab->dana_pengeluaran = $kcCount->sum('dana_pengeluaran');
            $mkab->data_pengeluaran = $kcCount->sum('data_pengeluaran');
            $mkab->save(false);

            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Kabupaten telah diperbaharui</td></tr>';
        }

        $mprov = ProvinsiCount::findOne(['tahun' => $tahun, 'kd_provinsi' => $kd_provinsi]);
        if($mprov == null){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("INSERT INTO `provinsi_count`(`tahun`, `kd_provinsi`,`dana_anggaran`, `dana_pencairan`, `dana_kegiatan`, `dana_mutasi`, `dana_pajak`, `dana_pendapatan`, `dana_rab`, `dana_penerimaan`, `dana_pengeluaran`, `dana_spj`, `dana_spp`, `dana_sts`, `dana_tbp`, `data_pencairan`, `data_anggaran`, `data_kegiatan`, `data_bidang`, `data_mutasi`, `data_pajak`, `data_pendapatan`, `data_rab`, `data_penerimaan`, `data_pengeluaran`, `data_spj`, `data_spp`, `data_sts`, `data_tbp`) VALUES ('".$tahun."','".$kd_provinsi."',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");

            $command->execute();
            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Provinsi telah dibuat</td></tr>';

            $kabCount = KabupatenCount::find()->where(['Tahun' => $tahun, 'kd_provinsi' => $kd_provinsi]);
            
            $mprov->dana_pendapatan = $kabCount->sum('dana_pendapatan');
            $mprov->data_pendapatan = $kabCount->sum('data_pendapatan');
            $mprov->dana_rab = $kabCount->sum('dana_rab');
            $mprov->data_rab = $kabCount->sum('data_rab');
            $mprov->dana_penerimaan = $kabCount->sum('dana_penerimaan');
            $mprov->data_penerimaan = $kabCount->sum('data_penerimaan');
            $mprov->dana_pengeluaran = $kabCount->sum('dana_pengeluaran');
            $mprov->data_pengeluaran = $kabCount->sum('data_pengeluaran');
            $mprov->save(false);
            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Provinsi telah diperbaharui</td></tr>';
        } else {
            //count pendapatan
            $kabCount = KabupatenCount::find()->where(['Tahun' => $tahun, 'kd_provinsi' => $kd_provinsi]);
            
            $mprov->dana_pendapatan = $kabCount->sum('dana_pendapatan');
            $mprov->data_pendapatan = $kabCount->sum('data_pendapatan');
            $mprov->dana_rab = $kabCount->sum('dana_rab');
            $mprov->data_rab = $kabCount->sum('data_rab');
            $mprov->dana_penerimaan = $kabCount->sum('dana_penerimaan');
            $mprov->data_penerimaan = $kabCount->sum('data_penerimaan');
            $mprov->dana_pengeluaran = $kabCount->sum('dana_pengeluaran');
            $mprov->data_pengeluaran = $kabCount->sum('data_pengeluaran');
            $mprov->save(false);
            echo '<tr><td>Perhitungan APBDES '.$tahun.' Tingkat Provinsi telah diperbaharui</td></tr>';
        }

        //jika desa
        if(Yii::$app->user->identity->type == 'desa'){
            $connection = Yii::$app->getDb();
            $user_id = Yii::$app->user->identity->id;
            $command = $connection->createCommand("UPDATE `Ta_Kegiatan` `k`, (SELECT user_id,Kd_Keg, SUM(Anggaran) AS summ FROM `Ta_RABRinci` WHERE `user_id` = '$user_id' GROUP BY `Kd_Keg`) AS `summing` SET k.Pagu = summing.summ WHERE k.user_id = summing.user_id COLLATE utf8_unicode_ci AND k.Kd_Keg = summing.Kd_Keg COLLATE utf8_unicode_ci");

            $command->execute();
            echo '<tr><td>Nilai Kegiatan Desa di Tahun '.$tahun.' telah diperbaharui</td></tr>';
        }

        echo '</table></div></div></div></div>';
        echo '<script>$.alert("Berhasil melakukan perhitungan APBDES pada Aplikasi SP3D");</script>';
    }
}

