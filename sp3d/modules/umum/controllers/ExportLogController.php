<?php

namespace sp3d\modules\umum\controllers;

use Yii;
use sp3d\modules\umum\models\DdcExportLog;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExportLogController implements the CRUD actions for DdcExportLog model.
 */
class ExportLogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DdcExportLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DdcExportLog::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DdcExportLog model.
     * @param string $user
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($user, $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($user, $id),
        ]);
    }

    /**
     * Creates a new DdcExportLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DdcExportLog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user' => $model->user, 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DdcExportLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $user
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($user, $id)
    {
        $model = $this->findModel($user, $id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user' => $model->user, 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DdcExportLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $user
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($user, $id)
    {
        $this->findModel($user, $id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DdcExportLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $user
     * @param integer $id
     * @return DdcExportLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user, $id)
    {
        if (($model = DdcExportLog::findOne(['user' => $user, 'id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
