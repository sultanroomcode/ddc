<?php
namespace sp3d\modules\umum\controllers;

use yii\web\Controller;
use sp3d\models\VdataEntri;
use Yii;
/**
 * Default controller for the `umum` module
 */
class StatistikController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($tahun=2016, $id=null, $type='desa')
    {
        return $this->renderAjax('index-'.$type, [
        	'tahun' => $tahun,
            'id' => $id
        ]);
    }

    // public function actionDynamic()
    // {
    //     //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
    //     $data = Yii::$app->request->get();
        
    //     return $this->renderAjax('index-'.$type, [
    //         'tahun' => $tahun,
    //         'id' => $id
    //     ]);
    // }

    public function actionEntrian()
    {
        $data = Yii::$app->request->get();
        $model = VdataEntri::find()->select(['SUM(c) AS c', 'tahun'])->groupBy('tahun');
        return $this->renderAjax('entrian', [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionEntrianDetail()
    {
        $data = Yii::$app->request->get();
        $model = VdataEntri::find()->where(['tahun' => $data['tahun']]);

        return $this->renderAjax('entrian-detail', [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionPasarStat()
    {
        $data = Yii::$app->request->get();
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $kode = null;
            $type = 'provinsi';
        }
        $data['type'] = $type;
        $data['kode'] = $kode;

        return $this->renderAjax('pasar-stat-'.$type, [
            'type' => $type,
            'data' => $data
        ]);
    }

    public function actionBumdesStat()
    {
        $data = Yii::$app->request->get();
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $kode = null;
            $type = 'provinsi';
        }
        $data['type'] = $type;
        $data['kode'] = $kode;

        return $this->renderAjax('bumdes-stat-'.$data['tipedata'].'-'.$type, [
            'type' => $type,
            'data' => $data
        ]);
    }

    public function actionRequestChart()
    {
        $data = Yii::$app->request->get();
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $tingkat = 'kecamatan';
            $kode = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $tingkat = 'kabupaten';
            $kode = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $kode = null;
            $tingkat = 'provinsi';
        }
        $data['tingkat'] = $tingkat;
        $data['kode'] = $kode;

        return $this->renderAjax('request-chart', [
            'data' => $data
        ]);
    }

    public function actionOtherStat()
    {
        $data = Yii::$app->request->get();
        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $data['kecamatan-input'];
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $data['kabupaten-input'];
        } else {
            // 'provinsi'
            $kode = null;
            $type = 'provinsi';
        }
        $data['type'] = $type;
        $data['kode'] = $kode;

        return $this->renderAjax('other-stat-'.$data['data'].'-'.$type, [
            'type' => $type,
            'data' => $data
        ]);
    }

    public function actionPendampingStat($type='kecamatan')
    {
        return $this->renderAjax('pendamping-stat-'.$type, [
            'type' => $type
        ]);
    }

    public function actionChart($tahun=2016, $type='out', $tingkat=null, $kd=null)
    {
        $data = Yii::$app->request->get();
    	switch ($type) {
            //pendamping
            case 'age-pendamping':
                $outp = $this->renderAjax('chart-age-pendamping', ['tahun' => $tahun, 'data' => $data]);
            break;
            case 'gender-pendamping':
                $outp = $this->renderAjax('chart-gender-pendamping', ['tahun' => $tahun, 'data' => $data]);
            break;
            //kades
    		case 'age-kades':
    			$outp = $this->renderAjax('chart-age-kades', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
    		break;
            case 'long-order-kades':
                $outp = $this->renderAjax('chart-long-order-kades', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'gender-kades':
                $outp = $this->renderAjax('chart-gender-kades', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'pendidikan-kades':
                $outp = $this->renderAjax('chart-pendidikan-kades', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'pelatihan-kades':
                $outp = $this->renderAjax('chart-pelatihan-kades', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'pelatihan-perangkat':
                $outp = $this->renderAjax('chart-pelatihan-perangkat', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'pelatihan-bpd':
                $outp = $this->renderAjax('chart-pelatihan-bpd', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;

            //perangkat
            case 'age-perangkat':
                $outp = $this->renderAjax('chart-age-perangkat', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'gender-perangkat':
                $outp = $this->renderAjax('chart-gender-perangkat', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'pendidikan-perangkat':
                $outp = $this->renderAjax('chart-pendidikan-perangkat', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            //bpd
            case 'age-bpd':
                $outp = $this->renderAjax('chart-age-bpd', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'gender-bpd':
                $outp = $this->renderAjax('chart-gender-bpd', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            case 'pendidikan-bpd':
                $outp = $this->renderAjax('chart-pendidikan-bpd', ['tahun' => $tahun, 'tingkat' => $tingkat, 'id' => $kd, 'data' => $data]);
            break;
            //provinsi
            case 'out':
                $outp = $this->renderAjax('chart-out-provinsi', ['tahun' => $tahun, 'data' => $data]);
            break;
    		case 'in':
    			$outp = $this->renderAjax('chart-in-provinsi', ['tahun' => $tahun, 'data' => $data]);
    		break;
    		case 'kabupaten-input':
    			$outp = $this->renderAjax('chart-kabupaten-input', ['tahun' => $tahun, 'data' => $data]);
    		break;
    		case 'kecamatan-input':
    			$outp = $this->renderAjax('chart-kecamatan-input', ['tahun' => $tahun, 'data' => $data]);
    		break;
    		case 'desa-input':
    			$outp = $this->renderAjax('chart-desa-input', ['tahun' => $tahun, 'data' => $data]);
    		break;
            //kabupaten
            case 'out-kab':
                $outp = $this->renderAjax('chart-out-kabupaten', ['tahun' => $tahun, 'kd' => $kd, 'data' => $data]);
            break;
            case 'in-kab':
                $outp = $this->renderAjax('chart-in-kabupaten', ['tahun' => $tahun, 'kd' => $kd, 'data' => $data]);
            break;
            case 'desa-input-on-kab':
                $outp = $this->renderAjax('chart-desa-input-on-kabupaten', ['tahun' => $tahun, 'kd' => $kd, 'data' => $data]);
            break;
            case 'kec-input-on-kab':
                $outp = $this->renderAjax('chart-kecamatan-input-on-kabupaten', ['tahun' => $tahun, 'kd' => $kd, 'data' => $data]);
            break;
            //kecamatan
            case 'out-kec':
                $outp = $this->renderAjax('chart-out-kecamatan', ['tahun' => $tahun, 'kd' => $kd, 'data' => $data]);
            break;
            case 'in-kec':
                $outp = $this->renderAjax('chart-in-kecamatan', ['tahun' => $tahun, 'kd' => $kd, 'data' => $data]);
            break;
            case 'desa-input-on-kec':
                $outp = $this->renderAjax('chart-desa-input-on-kecamatan', ['tahun' => $tahun, 'kd' => $kd, 'data' => $data]);
            break;
            //bumdes
            case 'bumdes-tahun':
                $outp = $this->renderAjax('chart-bumdes-tahun', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'bumdes-status':
                $outp = $this->renderAjax('chart-bumdes-status', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'bumdes-region':
                $outp = $this->renderAjax('chart-bumdes-region', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'bumdes-modal':
                $outp = $this->renderAjax('chart-bumdes-modal', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'bumdes-modal-omset':
                $outp = $this->renderAjax('chart-bumdes-modal-omset', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'bumdes-modal-keuntungan':
                $outp = $this->renderAjax('chart-bumdes-modal-keuntungan', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'bumdes-unit-usaha':
                $outp = $this->renderAjax('chart-bumdes-unit-usaha', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'bumdes-klasifikasi':
                $outp = $this->renderAjax('chart-bumdes-klasifikasi', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
            //pasardesa
            case 'pasar-desa-tahun':
                $outp = $this->renderAjax('chart-pasar-tahun', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'pasar-desa-region':
                $outp = $this->renderAjax('chart-pasar-region', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'pasar-desa-jml-pedagang':
                $outp = $this->renderAjax('chart-pasar-jumlah-pedagang', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'pasar-desa-jml-kios':
                $outp = $this->renderAjax('chart-pasar-jumlah-kios', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'pasar-desa-jml-lapak':
                $outp = $this->renderAjax('chart-pasar-jumlah-lapak', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'pasar-desa-jml-lesehan':
                $outp = $this->renderAjax('chart-pasar-jumlah-lesehan', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'pasar-desa-jml-los':
                $outp = $this->renderAjax('chart-pasar-jumlah-los', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;

            case 'pasar-desa-jml-ruko':
                $outp = $this->renderAjax('chart-pasar-jumlah-ruko', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
            //TKD
            case 'tkd-luas-lahan':
            case 'tkd-jenis-lahan':
            case 'tkd-jenis-sertifikat':
            case 'tkd-lokasi':
            case 'pkk-pembina':
            case 'lpmd-pembina':
            case 'kartar-pembina':
            case 'age-kpm':
            case 'gender-kpm':
            case 'pendidikan-kpm':
            case 'kpm-status':
            case 'kpm-pendidikan':
            case 'data-tambahan-umum':
                $outp = $this->renderAjax('chart-'.$type, ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
            //podes
            case 'podes-pertanian-pangan-luas-lahan-produksi':
            case 'podes-pertanian-buah-luas-lahan-produksi':
            case 'podes-pertanian-apotik-luas-lahan-produksi':
            case 'podes-perkebunan-luas-lahan-produksi':
            case 'podes-kehutanan-luas-lahan-produksi':
            case 'podes-peternakan-luas-lahan-produksi':
            case 'podes-perikanan-luas-lahan-produksi':
                $outp = $this->renderAjax('chart-podes-luas-lahan-produksi', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
            case 'podes-pertanian-pangan-lahan-milik':
            case 'podes-pertanian-buah-lahan-milik':
            case 'podes-pertanian-apotik-lahan-milik':
            case 'podes-perkebunan-lahan-milik':
            case 'podes-kehutanan-lahan-milik':
            case 'podes-peternakan-lahan-milik':
            case 'podes-perikanan-lahan-milik':
                $outp = $this->renderAjax('chart-podes-lahan-milik', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
            case 'podes-pertanian-pangan-komoditas':
            case 'podes-pertanian-buah-komoditas':
            case 'podes-pertanian-apotik-komoditas':
            case 'podes-perkebunan-komoditas':
            case 'podes-kehutanan-komoditas':
            case 'podes-peternakan-komoditas':
            case 'podes-perikanan-komoditas':
                $outp = $this->renderAjax('chart-podes-komoditas', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
            case 'podes-pertanian-pangan-satuan':
            case 'podes-pertanian-buah-satuan':
            case 'podes-pertanian-apotik-satuan':
            case 'podes-perkebunan-satuan':
            case 'podes-kehutanan-satuan':
            case 'podes-peternakan-satuan':
            case 'podes-perikanan-satuan':
                $outp = $this->renderAjax('chart-podes-satuan', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
            case 'podes-pertanian-pangan-hasil-produksi':
            case 'podes-pertanian-buah-hasil-produksi':
            case 'podes-pertanian-apotik-hasil-produksi':
            case 'podes-perkebunan-hasil-produksi':
            case 'podes-kehutanan-hasil-produksi':
            case 'podes-peternakan-hasil-produksi':
            case 'podes-perikanan-hasil-produksi':
                $outp = $this->renderAjax('chart-podes-hasil-produksi', ['tahun' => $tahun, 'tingkat' => $tingkat, 'kd' => $kd, 'data' => $data]);
            break;
        }
        return $outp;
    }
}
