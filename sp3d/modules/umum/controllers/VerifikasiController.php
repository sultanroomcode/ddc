<?php
namespace sp3d\modules\umum\controllers;

use Yii;
use ddcop\models\Operator;
use ddcop\models\OperatorDesa;
use sp3d\models\AuthAssignment;
use sp3d\models\User;
use sp3d\models\Log;
use sp3d\models\transaksi\Sp3dVerifikasiKec;
use sp3d\models\transaksi\Sp3dVerifikasiRevisiKec;
use sp3d\models\transaksi\Sp3dVerifikatorKec;
use sp3d\models\IsiPengguna;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;

class VerifikasiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionVerifikasiDataCenter()
    {
        $dttable = Yii::$app->request->get();
        $data = Sp3dVerifikasiKec::find()->select(['imp_id', 'verified_status', 'tahun', 'kd_desa', 'nik']);
        $datap = $data->offset($dttable['start'])->limit($dttable['length']);//data paging
        $datap->where(['LIKE', 'user', Yii::$app->user->identity->id.'%', false]);
        $datap->andWhere(['tahun' => $dttable['tahun'], 'verified_status' => $dttable['status']]);
        if($dttable['search']['value'] != ''){
            // $datap->where(['LIKE', 'username', $dttable['search']['value']]);
            // $datap->orWhere(['LIKE', 'description', $dttable['search']['value']]);
        }

        $dataserve = ArrayHelper::toArray($datap->all(), ['sp3d\models\transaksi\Sp3dVerifikasiKec' => 
            [
            'imp_id',
            'tahun',
            'kd_desa',
            'nik',
            'action' => function($p){
                return ($p->verified_status == 0)?'<a href="javascript:void(0)" onclick="openModalXyf({url:\'/umum/verifikasi/update-verifikasi?kd_desa='.$p->kd_desa.'&tahun='.$p->tahun.'&id='.$p->imp_id.'\'})" class="btn btn-sm">Verifikasi</a> <a href="javascript:void(0)" onclick="openModalXyf({url:\'/umum/verifikasi/embed-pdf?kd_desa='.$p->kd_desa.'&tahun='.$p->tahun.'&id='.$p->imp_id.'\'})" class="btn btn-sm">Lihat</a>':'--';
            }]
        ]);

        $foor = [
            'draw' => (int) $dttable['draw'],
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $datap->count(),
            'data' => $dataserve
        ];
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }

    public function actionEmbedPdf()
    {
        $param = Yii::$app->request->get();
        return $this->renderAjax('embed-pdf', [
            'param' => $param,
        ]);
    }

    public function actionVerifikatorDataCenter()
    {
        $dttable = Yii::$app->request->get();
        $data = Sp3dVerifikatorKec::find()->select(['nik', 'nama', 'user']);
        $datap = $data->offset($dttable['start'])->limit($dttable['length']);//data paging
        $datap->where(['user' => Yii::$app->user->identity->id ]);
        if($dttable['search']['value'] != ''){
            $datap->where(['LIKE', 'nama', $dttable['search']['value']]);
            $datap->orWhere(['LIKE', 'nik', $dttable['search']['value']]);
        }

        $dataserve = ArrayHelper::toArray($datap->all(), ['sp3d\models\transaksi\Sp3dVerifikatorKec' => 
            [
            'nik',
            'nama',
            'action' => function($p){
                return '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/verifikasi/update-verifikator?nik='.$p->nik.'&user='.$p->user.'\'})" class="btn btn-sm">Update</a>';
            }]
        ]);

        $foor = [
            'draw' => (int) $dttable['draw'],
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $datap->count(),
            'data' => $dataserve
        ];
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }

    public function actionListVerifikator()
    {
        $dataProvider = Sp3dVerifikatorKec::find()->where(['user' => Yii::$app->user->identity->id]);

        return $this->renderAjax('list-verifikator', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTambahVerifikator()
    {
        $model = new Sp3dVerifikatorKec();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('create-verifikator', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateVerifikator($nik, $user)
    {
        $model = $this->findModelVerifikator($nik, $user);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nik' => $model->nik, 'user' => $model->user]);
        } else {
            return $this->renderAjax('update-verifikator', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeleteVerifikator($nik, $user)
    {
        $this->findModelVerifikator($nik, $user)->delete();

        return $this->redirect(['index']);
    }

    protected function findModelVerifikator($nik, $user)
    {
        if (($model = Sp3dVerifikatorKec::findOne(['nik' => $nik, 'user' => $user])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRevisiVerifikasi($kd_desa, $tahun, $id)
    {
        $model = new Sp3dVerifikasiRevisiKec();
        $modver = Sp3dVerifikasiKec::findOne(['kd_desa' => $kd_desa, 'tahun' => $tahun, 'imp_id' => $id]);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
                $modver->verified_status = 2;
                $modver->save();
            } else echo 0;
        } else {
            return $this->renderAjax('revisi-verifikasi', [
                'model' => $model,
                'modver' => $modver
            ]);
        }
    }

    public function actionViewRevisi($kd_desa, $tahun, $id)
    {
        $model = Sp3dVerifikasiRevisiKec::findOne(['kd_desa' => $kd_desa, 'year' => $tahun, 'id' => $id]);
        return $this->renderAjax('view-revisi', [
            'model' => $model
        ]);
    }

    public function actionCreateVerifikasi()
    {
        $model = new Sp3dVerifikatorKec();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nik' => $model->nik, 'user' => $model->user]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateVerifikasi($kd_desa, $id, $tahun)
    {
        if(($model = Sp3dVerifikasiKec::findOne(['kd_desa' => $kd_desa, 'imp_id' => $id, 'verified_status' => 0, 'tahun' => $tahun]))  == null) {
            throw new NotFoundHttpException('Halaman verifikasi tidak ada.');
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('update-verifikasi', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeleteVerifikasi($nik, $user)
    {
        $this->findModel($nik, $user)->delete();

        return $this->redirect(['index']);
    }

    protected function findModelVerifikasi($nik, $user)
    {
        if (($model = Sp3dVerifikatorKec::findOne(['nik' => $nik, 'user' => $user])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    //2018-11-18T12:29:19+07:00
    public function actionOperator()
    {
        return $this->renderAjax('operator');
    }

    public function actionOperatorDataCenter()
    {
        $dttable = Yii::$app->request->get();
        $data = Operator::find()->select(['email', 'status', 'konfirmasi_email', 'tipe_operator', 'created_at']);
        $datap = $data->offset($dttable['start'])->limit($dttable['length']);//data paging

        // var_dump($dttable['order'][0]['column']);
        // var_dump($dttable['order'][0]['dir']);
        
        if($dttable['search']['value'] != ''){
            $datap->where(['LIKE', 'email', $dttable['search']['value']]);
        }
        //order
        switch ($dttable['order'][0]['column']) {
            case 1:
                $ordcolm = 'status';
            break;
            case 2:
                $ordcolm = 'konfirmasi_email';
            break;
            case 3:
                $ordcolm = 'tipe_operator';
            break;

            case 4:
                $ordcolm = 'created_at';
            break;
            
            default:
                //0
                $ordcolm = 'email';
            break;
        }

        $datap->orderBy($ordcolm.' '.strtoupper($dttable['order'][0]['dir']));



        $dataserve = ArrayHelper::toArray($datap->all(), ['ddcop\models\Operator' => 
            [
            'email',
            'status' => function($p){
                return ($p->status == 0)?'Tidak Aktif':'Aktif';
            },
            'konfirmasi_email' => function($p){
                return ($p->konfirmasi_email == 0)?'Belum Konfirmasi':'Terkonfirmasi';
            },
            'tipe_operator',
            'created_at' => function($p){
                return date('d-F-Y H:i:s', $p->created_at);
            },
            'action' => function($p){
                return '<a href="javascript:void(0)" onclick="openModalXy({url:\'/umum/verifikasi/update-operator?email='.$p->email.'\'})" class="btn btn-sm">Update</a>';
            }]
        ]);

        $foor = [
            'draw' => (int) $dttable['draw'],
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $datap->count(),
            'data' => $dataserve
        ];
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }

    public function actionUpdateOperator($email)
    {
        if(($model = Operator::findOne(['email' => $email]))  == null) {
            throw new NotFoundHttpException('Halaman verifikasi tidak ada.');
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->save(false)){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('update-operator', [
                'model' => $model,
            ]);
        }
    }

    //2018-11-20T00:36:21+07:00
    public function actionDesaOperator()
    {
        return $this->renderAjax('desa-operator');
    }

    public function actionDesaOperatorDataCenter()
    {
        $dttable = Yii::$app->request->get();
        $data = OperatorDesa::find()->joinWith('operator')->select(['operator_desa.id', 'operator.email', 'kd_desa', 'operator_desa.status', 'propose_at', 'confirm_at']);
        $datap = $data->offset($dttable['start'])->limit($dttable['length']);//data paging

        // var_dump($dttable['order'][0]['column']);
        // var_dump($dttable['order'][0]['dir']);
        if($dttable['kabupaten'] != '-' && $dttable['kecamatan'] == ''){
            $datap->where(['LIKE', 'kd_desa', $dttable['kabupaten'].'%', false]);
        }

        if($dttable['kabupaten'] != '-' && $dttable['kecamatan'] != '' && $dttable['desa'] == ''){
            $datap->where(['LIKE', 'kd_desa', $dttable['kecamatan'].'%', false]);
        }

        if($dttable['kabupaten'] != '-' && $dttable['kecamatan'] != '' && $dttable['desa'] != ''){
            $datap->where(['kd_desa' => $dttable['desa']]);
        }

        if($dttable['search']['value'] != ''){
            $datap->andWhere(['LIKE', 'kd_desa', $dttable['search']['value']]);
        }
        //order
        switch ($dttable['order'][0]['column']) {
            case 1:
                $ordcolm = 'kd_desa';
            break;
            case 2:
                $ordcolm = 'status';
            break;
            case 3:
                $ordcolm = 'propose_at';
            break;

            case 4:
                $ordcolm = 'confirm_at';
            break;
            
            default:
                //0
                $ordcolm = 'operator_desa.id';
            break;
        }

        $datap->orderBy($ordcolm.' '.strtoupper($dttable['order'][0]['dir']));
        // var_dump($data->all());

        $dataserve = ArrayHelper::toArray($datap->all(), ['ddcop\models\OperatorDesa' => 
            [
            'id' => function($p){
                return $p->email;
            },
            'kd_desa',
            'status' => function($p){
                return ($p->status == 0)?'Tidak Aktif':'Aktif';
            },
            'propose_at' => function($p){
                return date('d-F-Y H:i:s', strtotime($p->propose_at));
            },
            'confirm_at' => function($p){
                return date('d-F-Y H:i:s', strtotime($p->confirm_at));
            },
            'action' => function($p){
                return '<a href="javascript:void(0)" onclick="openModalXy({url:\'/umum/verifikasi/update-desa-operator?id='.$p->id.'&kd_desa='.$p->kd_desa.'\'})" class="btn btn-sm">Update</a>';
            }]
        ]);

        $foor = [
            'draw' => (int) $dttable['draw'],
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $datap->count(),
            'data' => $dataserve
        ];
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return $foor;
    }

    public function actionUpdateDesaOperator($id, $kd_desa)
    {
        if(($model = OperatorDesa::findOne(['id' => $id, 'kd_desa' => $kd_desa]))  == null) {
            throw new NotFoundHttpException('Halaman verifikasi tidak ada.');
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->save(false)){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('update-desa-operator', [
                'model' => $model,
            ]);
        }
    }
}
