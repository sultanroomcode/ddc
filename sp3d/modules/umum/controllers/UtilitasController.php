<?php
namespace sp3d\modules\umum\controllers;

use Yii;
use sp3d\models\AuthAssignment;
use sp3d\models\transaksi\Sp3dMasterJabatan;
use sp3d\models\transaksi\Sp3dMasterJenisPelatihan;
use sp3d\models\transaksi\Sp3dMasterJabatanTipe;
use sp3d\models\Log;
use sp3d\models\transaksi\TaUserDesa;
use sp3d\models\transaksi\Sp3dProfileDesa;
use sp3d\models\IsiPengguna;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
- index
- reset-password
 */
class UtilitasController extends Controller
{
    /**
     * @inheritdoc
     */
    public $urlConnectServerWin;   
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    // public $urlConnectServerWin =  '192.168.43.92';
    public function init()
    {
         $this->urlConnectServerWin =  Yii::$app->params['urlServer'];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionMasterJabatanTipe()
    {
        $dataProvider = Sp3dMasterJabatanTipe::find();

        return $this->renderAjax('master-jabatan-tipe', [
            'model' => $dataProvider,
        ]);
    }

    public function actionTambahMasterJabatanTipe()
    {
        $model = new Sp3dMasterJabatanTipe();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('_form-master-jabatan-tipe', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateMasterJabatanTipe($id)
    {
        $model = Sp3dMasterJabatanTipe::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('_form-master-jabatan-tipe', [
                'model' => $model,
            ]);
        }
    }
    
    //master jabatan
    public function actionMasterJabatan()
    {
        $data = Yii::$app->request->get();
        $dataProvider = Sp3dMasterJabatan::find()->orderBy('urut');

        if(isset($data['tipe'])){
            $dataProvider->where(['tipe' => $data['tipe']]);
        }

        return $this->renderAjax('master-jabatan', [
            'model' => $dataProvider,
            'data' => $data
        ]);
    }

    public function actionTambahMasterJabatan()
    {
        $data = Yii::$app->request->get();
        $model = new Sp3dMasterJabatan();
        if(isset($data['tipe'])){
            $model->tipe = $data['tipe'];
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('_form-master-jabatan', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    public function actionUpdateMasterJabatan($id)
    {
        $model = Sp3dMasterJabatan::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('_form-master-jabatan', [
                'model' => $model,
            ]);
        }
    }

    //master jenis pelatihan
    public function actionMasterJenisPelatihan()
    {
        $data = Yii::$app->request->get();
        $dataProvider = Sp3dMasterJenisPelatihan::find()->orderBy('urut');

        if(isset($data['tipe'])){
            $dataProvider->where(['tipe' => $data['tipe']]);
        }

        return $this->renderAjax('master-jenis-pelatihan', [
            'model' => $dataProvider,
            'data' => $data
        ]);
    }

    public function actionTambahMasterJenisPelatihan()
    {
        $data = Yii::$app->request->get();
        $model = new Sp3dMasterJenisPelatihan();
        if(isset($data['tipe'])){
            $model->tipe = $data['tipe'];
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('_form-master-jenis-pelatihan', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    public function actionUpdateMasterJenisPelatihan($id, $tipe)
    {
        $model = Sp3dMasterJenisPelatihan::findOne(['id' => $id, 'tipe' => $tipe]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo 1;
            } else echo 0;
        } else {
            return $this->renderAjax('_form-master-jenis-pelatihan', [
                'model' => $model,
            ]);
        }
    }

    public function actionPingWinServer()
    {
        $ch = curl_init();
        // set url
        $url = $this->urlConnectServerWin."/sp3dclient/ping-server.php";
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 7); //timeout in seconds
        // $output contains the output string
        $outputError = curl_exec($ch);
        curl_close($ch);

        return $this->renderAjax('ping-win-server', [
            'outputError' => $outputError
        ]);
    }
}
