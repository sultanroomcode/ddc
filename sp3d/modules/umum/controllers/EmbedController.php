<?php
namespace sp3d\modules\umum\controllers;

use yii\web\Controller;

/**
 * Default controller for the `umum` module
 */
class EmbedController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionViewDoc()
    {
        //<iframe class="doc" src="https://docs.google.com/gview?url=http://writing.engr.psu.edu/workbooks/formal_report_template.doc&embedded=true"></iframe>
    }
}
