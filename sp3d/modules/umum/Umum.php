<?php
namespace sp3d\modules\umum;

/**
 * umum module definition class
 */
class Umum extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'sp3d\modules\umum\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
