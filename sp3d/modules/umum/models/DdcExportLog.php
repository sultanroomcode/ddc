<?php

namespace sp3d\modules\umum\models;

use Yii;

/**
 * This is the model class for table "ddc_export_log".
 *
 * @property string $user
 * @property int $id
 * @property string $file
 * @property string $generated_at
 * @property string $expired_at
 */
class DdcExportLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ddc_export_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user', 'id'], 'required'],
            [['id'], 'integer'],
            [['generated_at', 'expired_at'], 'safe'],
            [['user'], 'string', 'max' => 12],
            [['file'], 'string', 'max' => 100],
            [['user', 'id'], 'unique', 'targetAttribute' => ['user', 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user' => 'User',
            'id' => 'ID',
            'file' => 'File',
            'generated_at' => 'Generated At',
            'expired_at' => 'Expired At',
        ];
    }
}
