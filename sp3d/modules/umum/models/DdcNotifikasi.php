<?php

namespace sp3d\modules\umum\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ddc_notifikasi".
 *
 * @property string $n_from
 * @property string $n_to
 * @property string $message
 * @property string $open_url
 * @property string $status_read
 * @property string $send_at
 * @property string $opened_at
 */
class DdcNotifikasi extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ddc_notifikasi';
    }

    public function behaviors()
    {
        return ['timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['send_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['opened_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['n_from', 'n_to'], 'required'],
            // [['send_at', 'opened_at'], 'safe'],
            [['n_from', 'n_to'], 'string', 'max' => 32],
            [['message', 'open_url'], 'string', 'max' => 255],
            [['status_read'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'n_from' => 'N From',
            'n_to' => 'N To',
            'message' => 'Message',
            'open_url' => 'Open Url',
            'status_read' => 'Status Read',
            'send_at' => 'Send At',
            'opened_at' => 'Opened At',
        ];
    }

    public function show_status()
    {
        switch ($this->status_read) {
            case 'R':
                return 'Telah Dibaca';
                break;
            
            default:
                return 'Telah Terkirim';
                break;
        }
    }
}
