<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Pengaturan Kode Siskeudes</h2>
            <div class="p-10">
                <span class="show-pop btn btn-sm" data-animation="pop" data-content="<p><b>Info</b><br>Setting ID Siskeudes Anda.<br>Sesuaikan dengan kode yang ada pada file database (*.mde).<br> Format : xx.xx.<br>Contoh : 01.04.</p>">Info</span>
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
                <?= $form->field($model, 'siskeu_id')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '99.99.',
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Isi' : 'Sinkronisasi', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php $script = <<<JS
//regularly-ajax
$("[data-toggle=popover]").popover();
$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil Mengsinkronisasikan Kode Siskeudes dengan kode SP3D');
            goLoad({url : '/umum/user/setting'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);