<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\referensi\RefKecamatan;

$datakec = ArrayHelper::map(RefKecamatan::find()->all(), 'Kd_Kec','Nama_Kecamatan');
$model->email = 'xxx@dispemas.tubankab.go.id';

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name User */
/* @var $generator string helper base name with camel to id Inflector user */
/* @var $generator string helper base name with camel to word Inflector User */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'status')->dropdownList(['10' => 'Aktif', '0' => 'Tidak Aktif']) ?>
    <?= $form->field($model, 'kd_assignment')->dropdownList(['admin' => 'Admin', 'dpmdkb' => 'DPMDKB', 'kecamatan' => 'Kecamatan', 'desa' => 'Desa', 'kabupaten' => 'kabupaten']) ?>
    <?= $form->field($model, 'kd_kecamatan')->dropdownList($datakec, ['onchange' => 'cariDesa()']) ?>
    <?= $form->field($model, 'kd_desa')->dropdownList(['' => '---']) ?>

    <div class="form-group">
        <?= Html::submitButton('Buat', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $script = <<<JS

function cariDesa(){
    var urlid = $('#isipengguna-kd_kecamatan').val();
    if(urlid !== ''){
        $.post(base_url+'/data-umum/referensi-desa2?kid='+urlid+'&form=1').done(function(res){
            $('#isipengguna-kd_desa').html(res);
        });
    } else {
        $('#isipengguna-kd_desa').html("<option value=''>---</option>");
    }
}

cariDesa();

//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : '/umum/user/index'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);