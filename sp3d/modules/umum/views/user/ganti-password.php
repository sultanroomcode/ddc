<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator string helper base name User */
/* @var $generator string helper base name with camel to id Inflector user */
/* @var $generator string helper base name with camel to word Inflector User */
?>

<div class="row animated slideInLeft">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Ganti Password</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'password_baru')->passwordInput() ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Ganti', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <!--  Recent Postings -->
        <!-- Dynamic Chart -->
        </div>
    <div class="clearfix"></div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);