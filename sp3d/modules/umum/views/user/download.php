<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Download</h2>
            <div class="p-10">                
                <div class="listview list-click">
                    <header class="listview-header media">                            
                        <ul class="list-inline pull-right m-t-5 m-b-0">
                            <li class="pagin-value hidden-xs">1-1</li>
                            <li>
                                <a href="javascript:void(0)" title="Previous" class="tooltips">
                                    <i class="sa-list-back"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" title="Next" class="tooltips">
                                    <i class="sa-list-forwad"></i>
                                </a>
                            </li>
                        </ul>
                        
                        <ul class="list-inline list-mass-actions pull-left">
                        </ul>

                        <div class="clearfix"></div>
                    </header>

                    <!-- <div class="media message-header o-visible">
                        <img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">
                        <div class="pull-right dropdown m-t-10">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="p-5">Pilihan</a>
                            <ul class="dropdown-menu text-right">
                                <li><a href="http://datadesacenter.dpmd.jatimprov.go.id/lib/Modul-APBDes-SP3D-V.01.pdf" target="_blank">Unduh</a></li>
                            </ul>
                        </div>
                        <div class="media-body">
                            <span class="f-bold pull-left m-b-5">Materi-Sp3d-v01.pdf [pdf]</span>
                            <div class="clearfix"></div>
                            <span class="dropdown m-t-5">
                                Di tambahkan <a href="javascript:void(0)" class="underline">Admin</a> pada 21-Maret-2018
                            </span>
                        </div>
                    </div>

                    <div class="media message-header o-visible">
                        <img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">
                        <div class="pull-right dropdown m-t-10">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="p-5">Pilihan</a>
                            <ul class="dropdown-menu text-right">
                                <li><a href="http://datadesacenter.dpmd.jatimprov.go.id/lib/Materi-SP3D-v01.pdf" target="_blank">Unduh</a></li>
                            </ul>
                        </div>
                        <div class="media-body">
                            <span class="f-bold pull-left m-b-5">Modul-APBDes-SP3D-V.01.pdf [pdf]</span>
                            <div class="clearfix"></div>
                            <span class="dropdown m-t-5">
                                Di tambahkan <a href="javascript:void(0)" class="underline">Admin</a> pada 22-Maret-2018
                            </span>
                        </div>
                    </div> -->

                    <div class="media message-header o-visible">
                        <img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">
                        <div class="pull-right dropdown m-t-10">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="p-5">Pilihan</a>
                            <ul class="dropdown-menu text-right">
                                <li><a href="http://datadesacenter.dpmd.jatimprov.go.id/lib/materi-ddc.pptx" target="_blank">Unduh</a></li>
                            </ul>
                        </div>
                        <div class="media-body">
                            <span class="f-bold pull-left m-b-5">Materi DDC [pptx]</span>
                            <div class="clearfix"></div>
                            <span class="dropdown m-t-5">
                                Di tambahkan <a href="javascript:void(0)" class="underline">Admin</a> pada 30-Oktober-2019
                            </span>
                        </div>
                    </div>

                    <div class="media message-header o-visible">
                        <img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">
                        <div class="pull-right dropdown m-t-10">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="p-5">Pilihan</a>
                            <ul class="dropdown-menu text-right">
                                <li><a href="http://datadesacenter.dpmd.jatimprov.go.id/lib/pergub-ddc.pdf" target="_blank">Unduh</a></li>
                            </ul>
                        </div>
                        <div class="media-body">
                            <span class="f-bold pull-left m-b-5">Pergub [pdf]</span>
                            <div class="clearfix"></div>
                            <span class="dropdown m-t-5">
                                Di tambahkan <a href="javascript:void(0)" class="underline">Admin</a> pada 22-Maret-2018
                            </span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>