<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$arr = ['-' => ''];
$kecArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', Yii::$app->user->identity->id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');

$kecArr = $arr + $kecArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Management User Kabupaten</h2>
            <div class="tile-config dropdown">
                <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
            </div>
            <div class="p-10">
                <div class="row">
                    <div class="col-md-5">
                        <input type="hidden" id="kabupaten" value="<?= Yii::$app->user->identity->id ?>">
                        <select id="kecamatan" class="form-control">
                            <?php foreach($kecArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button class="btn btn-block btn-danger" onclick="filteringAdd()">Filter</button>
                    </div>
                </div>
                <br>
                <table id="user-management-list" class="tile table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Nama</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div id="modal-custom" style="display: none;">
    <div style="width: 900px; background: rgba(0,0,0,0.8); border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
    </div>
</div>
<?php
/*http://localhost/yii/siapatakut_sv/sp3d/web/umum/user/management-data?draw=2&columns%5B0%5D%5Bdata%5D=id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=description&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=type&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=false&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=action&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=false&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1526709981679*/
$scripts =<<<JS

$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan').html('<option value="-"></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/pendamping/data/data-option?type=kecamatan&kode='+kode});
  }
});
    $('#user-management-list').DataTable({
        "columns": [
            { "data": "id",},
            { "data": "description"},
            { "data": "type", 'searchable':false },
            { "data": "action", 'searchable' : false, 'orderable': false }
        ],
        searchDelay: 1500,
        ajax: {
            url: base_url+'/umum/user/management-data',
            data : function(d){
                //https://medium.com/code-kings/datatables-js-how-to-update-your-data-object-for-ajax-json-data-retrieval-c1ac832d7aa5
                d.kabupaten = $('#kabupaten').val();
                d.kecamatan = $('#kecamatan').val();
            }
        },
        processing:true,
        serverSide:true
    });

    function filteringAdd(){
        $('#user-management-list').DataTable().ajax.reload();
        return false;
    }

    function confirmReset(id){
        $.confirm({
            title: 'Reset Password '+id+' ?',
            content: 'Apakah Anda akan me-reset password pengguna',
            autoClose: 'cancelAction|10000',
            type: 'red',
            buttons: {
                deleteUser: {
                    text: 'Reset Password',
                    action: function () {
                        //then
                        $.get(base_url+'/umum/user/reset-password?id='+id, function(data){
                            if(data == 1){
                                $.alert('Password Sudah Direset!');
                            } else {
                                $.alert('Gagal Dalam Mereset Password!');
                            }
                        });
                    }
                },
                cancelAction:{
                    text:'Batalkan',
                    action:function () {
                        $.alert('Reset Dibatalkan');
                    }
                }
            }
        });
    }

    var modalxy = new Custombox.modal({
      content: {
        effect: 'slidetogether',
        id:'form-xy',
        target: '#modal-custom',
        positionY: 'top',
        positionX: 'center',
        delay:2000,
        onClose: function(){
            
        }
      },
      overlay:{
        color:'#2A00FF'
      }
    });

    function openModalXy(o){
        $('#form-engine').html(' ');
        $('#form-engine').load(base_url+o.url);
        modalxy.open();    
    }
JS;

$this->registerJs($scripts);