<?php
use sp3d\models\transaksi\Sp3dVerifikasiRevisiKec;
$directoryIcon = Yii::$app->params['directoryIcon'];
//untuk saat ini menggunakan sp3d revisi dulu kedepannya akan ditambahkan message center/selain log
if(Yii::$app->user->identity->type == 'desa'){
$model = Sp3dVerifikasiRevisiKec::find()->where(['kd_desa' => Yii::$app->user->identity->id]);
foreach ($model->all() as $v):
?>
<div class="media" >
    <div class="pull-left">
        <img width="40" src="<?= $directoryIcon ?>img/profile-pics/1.jpg" alt="">
    </div>
    <div class="media-body">
        <small class="text-muted"><?= $v->user ?> - <?= $v->cDate(true); ?></small><br>
        <a class="t-overflow" href="javascript:void(0)" onclick="goLoad({url:'/umum/import/mdb-list'})">Kode Import (<?= $v->id ?>) :: <?= $v->message ?></a>
    </div>
</div>
<?php
endforeach;
} else {
	echo 'Belum ada pesan masuk';
}