<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if($model->isNewRecord){
    $model->user = Yii::$app->user->identity->id;
    $model->kd_kecamatan = substr($model->user, 0,7);
    $model->kd_kabupaten = substr($model->user, 0,4);
}
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart --> 
        <div class="tile">
            <h2 class="tile-title">Profile Setting</h2>
            <div class="p-10">
                <span class="show-pop btn btn-sm" data-animation="pop" data-content="<p><b>Info</b><br>Upload Foto Anda yang berformat (*.jpg)</p>">Info</span>
                <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>
                <?= $form->field($model, 'user')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'foto_profile_box')->fileInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'jml_rt')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'jml_rw')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Mohon Pakai Alamat gmail']) ?>
                <?= $form->field($model, 'deskripsi')->textarea() ?>

                <progress id="prog" max="100" value="0" style="display:none;"></progress>
                <div id="percent"></div>

                <div class="progress">
                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><b class="prog-text"></b>
                    <span class="sr-only" class="prog-text"></span>
                  </div>
                </div> 

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Upload' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php $script = <<<JS
//regularly-ajax
var bar = $('.progress-bar');
var percent = $('.prog-text');

$("[data-toggle=popover]").popover();
$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
$('form#{$model->formName()}').ajaxForm({
    beforeSend: function() {
        var percentVal = 0;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : '0%'}); 
        percent.html(percentVal+' %');
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    success: function() {
        var percentVal = 100;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    complete: function(xhr) {
        if(xhr.responseText == 1){
            $(this).trigger('reset');
            goLoad({url:'/umum/default/dashboard'});
        } else {
            
        }
    }
}); 
JS;

$this->registerJs($script);