<?php
use hscstudio\mimin\components\Mimin;
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
<div class="box-header with-border">
  <div class="user-block">
    <img class="img-circle" src="css/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">Admin.</a></span>
    <span class="description">Tabel Pengguna</span>
  </div>
  <!-- /.user-block -->
  <div class="box-tools">
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
      <i class="fa fa-circle-o"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
  <!-- /.box-tools -->
</div>
<!-- /.box-header -->
<div class="box-body">
  	<!-- post text -->
  	<a href="javascript:void(0)" onclick="goLoad({url:'/umum/user'})" class="btn btn-sm btn-warning">Kembali</a>
  	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-create')): ?>
	<a href="javascript:void(0)" onclick="goLoad({url:'/umum/user/buat-baru'})" class="btn btn-sm btn-warning">Tambah Akun Login Baru</a><hr>
	<?php endif; ?>
  	
  	<table id="ta-anggaran-table" class="display table" cellspacing="0" width="100%">
	    <thead>
	        <tr>
	            <th>Username</th>
	            <th>Email</th>
	            <th>Kode Kecamatan</th>
	            <th>Kode Desa</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Username</th>
	            <th>Email</th>
	            <th>Kode Kecamatan</th>
	            <th>Kode Desa</th>
	            <th>Action</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($dataProvider->all() as $v): ?>
	        <tr>
	            <td><?= $v->username ?></td>
	            <td><?= $v->email ?></td>
	            <td><?= ($v->chapter ==null)?'-':$v->chapter->Kd_Kec ?></td>
	            <td><?= ($v->chapter==null)?'-':$v->chapter->Kd_Desa ?></td>
	            <td>
	            	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-update')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-update'})" class="label label-warning">Edit</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-delete')): ?>
	            	<a href="javascript:void(0)" onclick="goSendLoad({elmChange:'#desa-container',msg:'Hapus Data?', urlSend:'/transaksi/ta-anggaran/desa-delete', urlBack:'/transaksi/ta-anggaran/desa-index', typeSend:'get'})" class="label label-danger">Hapus</a>
	            	<?php endif; ?>

	            	<?php if(Mimin::checkRoute('transaksi/ta-anggaran/desa-view')): ?>
	            	<a href="javascript:void(0)" onclick="goLoad({elm:'#desa-container', url:'/transaksi/ta-anggaran/desa-view'})" class="label label-warning">View</a>
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#ta-anggaran-table').DataTable();
JS;

$this->registerJs($scripts);