<?php
use yii\helpers\Url;
?>
<iframe width="100%" height="450" src="<?= Url::to(['/data-umum/pdf?bagian=apbdes&tahun='.$param['tahun'].'&kd_desa='.$param['kd_desa']]) ?>"></iframe>
<?php if(Yii::$app->user->identity->type != 'desa'){ ?>
<a href="javascript:void(0)" onclick="reopenModalXyf({url:'/umum/verifikasi/update-verifikasi?kd_desa=<?=$param['kd_desa']?>&tahun=<?=$param['tahun']?>&id=<?=$param['id']?>'})" class="btn btn-danger">Verifikasi</a> 
<a href="javascript:void(0)" onclick="reopenModalXyf({url:'/umum/verifikasi/revisi-verifikasi?kd_desa=<?=$param['kd_desa']?>&tahun=<?=$param['tahun']?>&id=<?=$param['id']?>'})" class="btn btn-danger">Berikan Revisi</a> 
<?php } ?>