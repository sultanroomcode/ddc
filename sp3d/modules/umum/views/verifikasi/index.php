<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Verifikasi Kecamatan</h2>
            <div class="tile-config dropdown">
                <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
                <ul class="dropdown-menu pull-right text-right"> 
                    <li><a href="javascript:void(0)" onclick="goLoad({url:'/umum/verifikasi/list-verifikator'})">List Verifikator</a></li>
                    <li><a href="javascript:void(0)" onclick="goLoad({url:'/umum/verifikasi/tambah-verifikator'})">Tambah Verifikator</a></li>
                </ul>
            </div>
            <div class="p-10">
                <div class="row">
                    <form id="send-verification">
                    <div class="col-md-4">
                        <select id="v-status" class="form-control">
                            <option value="1">Sudah Terverifikasi</option>
                            <option value="0">Belum Terverifikasi</option>
                            <option value="2">Revisi</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select id="v-tahun" class="form-control">
                            <option value="2018">2018</option>
                            <option value="2017">2017</option>
                            <option value="2016">2016</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <a href="javascript:void(0)" onclick="reloadTable()" class="btn btn-danger btn-md btn-block">Lihat</a>
                    </div>
                    </form>
                </div>
                <br><br>
                <table id="table-verifikasi" class="tile table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tahun</th>
                            <th>Kode Desa</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
$('#table-verifikasi').DataTable({
    "columns": [
        { "data": "imp_id",},
        { "data": "tahun", 'searchable':false },
        { "data": "kd_desa", 'searchable':true },
        { "data": "action", 'searchable' : false, 'orderable': false }
    ],
    searchDelay: 1500,
    ajax: {
        url: base_url+'/umum/verifikasi/verifikasi-data-center',
        data : function(d){
            //https://medium.com/code-kings/datatables-js-how-to-update-your-data-object-for-ajax-json-data-retrieval-c1ac832d7aa5
            d.tahun = $('#v-tahun').val();
            d.status = $('#v-status').val();
        }
    },
    processing:true,
    serverSide:true
});

function reloadTable()
{
    $('#table-verifikasi').DataTable().ajax.reload();
}
JS;

$this->registerJs($script);