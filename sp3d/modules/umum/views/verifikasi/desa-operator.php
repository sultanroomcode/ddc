<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$arr = ['-' => ''];
$kabArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');

$kabArr = $arr + $kabArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Daftar Operator Desa</h2>
            <div class="p-10">
                <div class="row">
                    <div class="col-md-3">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($kabArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select id="kecamatan" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select id="desa" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button class="btn btn-block btn-danger" onclick="filteringAdd()">Filter</button>
                    </div>
                </div>
                <br>

                <table id="table-operator" class="tile table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Kode Desa</th>
                            <th>Status</th>
                            <th>Tanggal Pengajuan</th>
                            <th>Tanggal Konfirmasi Desa</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/pendamping/data/data-option?type=kecamatan&kode='+kode});
  }
});

$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/pendamping/data/data-option?type=desa&kode='+kode});
  }
});


$('#table-operator').DataTable({
    "columns": [
        { "data": "id", 'searchable':false, 'orderable':true },
        { "data": "kd_desa", 'searchable':true, 'orderable':true },
        { "data": "status", 'searchable':false, 'orderable':true },
        { "data": "propose_at", 'searchable':false, 'orderable':true },
        { "data": "confirm_at", 'searchable':false, 'orderable':true },
        { "data": "action", 'searchable' : false, 'orderable': false }
    ],
    searchDelay: 1500,
    ajax: {
        url: base_url+'/umum/verifikasi/desa-operator-data-center',
        data : function(d){
            //https://medium.com/code-kings/datatables-js-how-to-update-your-data-object-for-ajax-json-data-retrieval-c1ac832d7aa5
            d.kabupaten = $('#kabupaten').val();
            d.kecamatan = $('#kecamatan').val();
            d.desa = $('#desa').val();
        }
    },
    processing:true,
    serverSide:true
});

function filteringAdd(){
    $('#table-operator').DataTable().ajax.reload();
    return false;
}

var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#2A00FF'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}
JS;

$this->registerJs($script);