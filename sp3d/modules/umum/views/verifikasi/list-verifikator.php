<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Daftar Verifikator</h2>
            <div class="p-10">
                <table id="table-verifikator" class="tile table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
$('#table-verifikator').DataTable({
    "columns": [
        { "data": "nik", 'searchable':false },
        { "data": "nama", 'searchable':false },
        { "data": "action", 'searchable' : false, 'orderable': false }
    ],
    searchDelay: 1500,
    ajax: {
        url: base_url+'/umum/verifikasi/verifikator-data-center'
    },
    processing:true,
    serverSide:true
});

JS;

$this->registerJs($script);