<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

if($model->isNewRecord){
    $model->id = $modver->imp_id;
    $model->kd_desa = $modver->kd_desa;
    $model->year = $modver->tahun;
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Revisi Verifikasi</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

                <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'year')->hiddenInput(['maxlength' => true])->label(false) ?>

                <?= $form->field($model, 'message')->textarea() ?>                

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Isi Revisi Verifikasi' : 'Verifikasi', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
if(Yii::$app->params['offline']){
    $this->registerCssFile('//localhost/local-cdn/bower_components/summernote/dist/summernote.css');
    $this->registerJsFile('//localhost/local-cdn/bower_components/summernote/dist/summernote.min.js');
} else {
    $this->registerCssFile('//sp3d.dpmd.jatimprov.go.id/local-cdn/bower_components/summernote/dist/summernote.css');
    $this->registerJsFile('//sp3d.dpmd.jatimprov.go.id/local-cdn/bower_components/summernote/dist/summernote.min.js');
}
$script = <<<JS
$('#sp3dverifikasirevisikec-message').summernote({
    height: 200,
    toolbar: [
        ["font", ["bold", "underline", "clear"]],
        ["para", ["ul", "ol"]],
        ["view", ["fullscreen"]]
    ]
});
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil mengisi revisi');
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);