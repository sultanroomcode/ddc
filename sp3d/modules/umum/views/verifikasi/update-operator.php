<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Verifikasi</h2>
            
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

                <?= $form->field($model, 'status')->dropdownList([
                    0 => 'Tidak Aktif',
                    10 => 'Aktif'
                ]) ?>

                <?= $form->field($model, 'konfirmasi_email')->dropdownList([
                    0 => 'Belum Konfirmasi',
                    10 => 'Konfirmasi'
                ]) ?>

                <?= $form->field($model, 'tipe_operator')->dropdownList([
                    'kpm' => 'Kader Pemberdayaan Masyarakat',
                    'bumdes' => 'Bumdes',
                    'posyandu' => 'Posyandu'
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Buat Verifikasi' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            // $(\$form).trigger('reset');
            $.alert('Berhasil mengubah verifikasi');
            
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);