<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dVerifikatorKec */

$this->title = 'Update Sp3d Verifikator Kec: ' . $model->nik;
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Verifikator Kecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nik, 'url' => ['view', 'nik' => $model->nik, 'user' => $model->user]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sp3d-verifikator-kec-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
