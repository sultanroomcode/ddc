<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use sp3d\models\transaksi\Sp3dVerifikatorKec;

//check verifikator
$verifikator = Sp3dVerifikatorKec::find()->where(['user' => Yii::$app->user->identity->id]);
$listVerifikator = [];
if($verifikator->count() > 0){
    $listVerifikator = ArrayHelper::map($verifikator->all(), 'nik', function($p){ return $p->nik. ' - '. $p->nama; });
} else {
    echo "<script>goLoad({url:'/umum/verifikasi/tambah-verifikator'}); $.alert('Belum ada verifikator!!');</script>";
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Verifikasi</h2>
            <div class="tile-config dropdown">
                <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
                <ul class="dropdown-menu pull-right text-right"> 
                    <li><a href="javascript:void(0)" onclick="goLoad({url:'/umum/verifikasi/list-verifikator'})">List Verifikator</a></li>
                </ul>
            </div>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

                <?= $form->field($model, 'imp_id')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'tahun')->hiddenInput(['maxlength' => true])->label(false) ?>

                <?= $form->field($model, 'nik')->dropdownList($listVerifikator) ?>

                <?= $form->field($model, 'verified_status')->dropdownList([
                    // 0 => 'Tidak Memverifikasi',
                    1 => 'Verifikasi'
                ]) ?>                

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Buat Verifikasi' : 'Verifikasi', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil di verifikasi');
            goLoad({url: '/umum/import/mdb-verifikasi?tahun={$model->tahun}&user={$model->kd_desa}&id={$model->imp_id}&counting=1'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);