<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Daftar Operator</h2>
            <div class="p-10">
                <table id="table-operator" class="tile table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Email Status</th>
                            <th>Tipe</th>
                            <th>Tanggal</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
$('#table-operator').DataTable({
    "columns": [
        { "data": "email", 'searchable':true, 'orderable':true },
        { "data": "status", 'searchable':false, 'orderable':true },
        { "data": "konfirmasi_email", 'searchable':false, 'orderable':true },
        { "data": "tipe_operator", 'searchable':false, 'orderable':true },
        { "data": "created_at", 'searchable':false, 'orderable':true },
        { "data": "action", 'searchable' : false, 'orderable': false }
    ],
    searchDelay: 1500,
    ajax: {
        url: base_url+'/umum/verifikasi/operator-data-center'
    },
    processing:true,
    serverSide:true
});

var modalxy = new Custombox.modal({
  content: {
    effect: 'slidetogether',
    id:'form-xy',
    target: '#modal-custom',
    positionY: 'top',
    positionX: 'center',
    delay:2000,
    onClose: function(){
        
    }
  },
  overlay:{
    color:'#2A00FF'
  }
});

function openModalXy(o){
    $('#form-engine').html(' ');
    $('#form-engine').load(base_url+o.url);
    modalxy.open();    
}
JS;

$this->registerJs($script);