<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\Sp3dVerifikatorKec */

$this->title = 'Create Sp3d Verifikator Kec';
$this->params['breadcrumbs'][] = ['label' => 'Sp3d Verifikator Kecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp3d-verifikator-kec-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
