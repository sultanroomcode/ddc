<?php
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">View Revisi Verifikasi</h2>
            <div class="p-10">
                Kode Import : <?= $model->id ?><br>
                Tahun : <?= $model->year ?><br>
                Pesan : <?= html_entity_decode($model->message) ?><br>
                <iframe width="100%" height="450" src="<?= Url::to(['/data-umum/pdf?bagian=apbdes&tahun='.$model->year.'&kd_desa='.$model->kd_desa]) ?>"></iframe>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
