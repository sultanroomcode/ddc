<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Provinsi Normalize</h2>
            <div class="p-10">
            	<!-- <a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-import-drop'})" class="show-pop btn btn-danger" title="Upload Database Baru" data-animation="pop" data-content="<p>Menambahkan Database MDE baru. perhatikan tahun Anggaran pada file Database yang akan Anda upload.</p>"><i class="fa fa-cloud-upload"></i> <i class="fa fa-database"></i></a>
				<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-normalize-count'})" class="btn btn-danger" title="Normalisasi Hitungan"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-list-ol"></i></a> -->
				<div id="div-norm-2016"></div>
				<div id="div-norm-2017"></div>
				<div id="div-norm-2018"></div>
			</div>
		</div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
	goLoad({elm:'#div-norm-2016', url:'/umum/counting/provinsi-normalize-count?tahun=2016'});
	goLoad({elm:'#div-norm-2017', url:'/umum/counting/provinsi-normalize-count?tahun=2017'});
	goLoad({elm:'#div-norm-2018', url:'/umum/counting/provinsi-normalize-count?tahun=2018'});
JS;

$this->registerJs($scripts);