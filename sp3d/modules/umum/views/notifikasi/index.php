<?php
use sp3d\modules\umum\models\DdcNotifikasi;
?>

<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Pusat Notifikasi</h2>
            <div class="p-10">
                <table class="table">
					<tr>
						<th>Dari</th>
						<th>Ke</th>
						<th>Pesan</th>
						<th>Status</th>
						<th>Tanggal</th>
						<th>Aksi</th>
					</tr>
					<?php
					$mod = DdcNotifikasi::find()->where(['n_to' => Yii::$app->user->identity->id]);
					foreach ($mod->all() as $v) {
					?>
					<tr>
						<td><?= $v->n_from ?></td>
						<td><?= $v->n_to ?></td>
						<td><?= $v->message ?></td>
						<td><?= $v->show_status() ?></td>
						<td><?= $v->send_at ?></td>
						<td><a href="javascript:void(0)" class="btn btn-xs" onclick="myReadNotif({id:'<?= $v->n_from ?>', time:'<?= $v->send_at ?>', url:'<?= $v->open_url ?>'})"><i class="fa fa-send"></i></a></td>
					</tr>
					<?php
					}
					?>
				</table>
            </div>
        <!-- Dynamic Chart -->
	    </div>
    </div>
    <div class="clearfix"></div>
</div>

<?php
$scriptJs = <<<JS
    function myReadNotif(o){
    	$.ajax(base_url+'/umum/notifikasi/read-notifikasi?id='+o.id+'&time='+o.time);
    	goLoad({url: o.url});
    }
JS;

$this->registerJs($scriptJs);