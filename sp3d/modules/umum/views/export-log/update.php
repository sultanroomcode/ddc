<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\umum\models\DdcExportLog */

$this->title = 'Update Ddc Export Log: ' . $model->user;
$this->params['breadcrumbs'][] = ['label' => 'Ddc Export Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user, 'url' => ['view', 'user' => $model->user, 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ddc-export-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
