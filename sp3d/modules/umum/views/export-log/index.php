<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ddc Export Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ddc-export-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ddc Export Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user',
            'id',
            'file',
            'generated_at',
            'expired_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
