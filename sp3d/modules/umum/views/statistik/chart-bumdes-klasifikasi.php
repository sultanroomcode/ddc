<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
switch ($tingkat) {
     case 'desa':
        $clausa = "WHERE v.ds_id  = '".$kd."'";
    break;
    case 'kecamatan':
    	$clausa = "WHERE v.kc_id = '".$kd."'";
    break;
    case 'kabupaten':
        $clausa = "WHERE v.kb_id = '".$kd."'";
    break;
    default://provinsi
        // $clausa = "AND kd_provinsi  = '".$id."'";
    break;
}

$bonparam = '&kabupaten-input='.$data['kabupaten-input'].'&kecamatan-input='.$data['kecamatan-input'];

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(*) AS hit, kfb.nilai_akhir_kategori AS klasifikasi FROM klasifikasi_final_bumdes kfb LEFT JOIN (
    SELECT 0 as id, 'MAJU' as klasifikasi
    UNION SELECT 1 as id, 'BERKEMBANG' as klasifikasi
    UNION SELECT 2 as id, 'PEMULA' as klasifikasi
    ) kfb_fake ON kfb.nilai_akhir_kategori = kfb_fake.klasifikasi LEFT JOIN vuserfull v ON v.ds_id = kfb.kd_desa COLLATE utf8_unicode_ci ".$clausa." GROUP BY kfb.nilai_akhir_kategori ORDER BY kfb_fake.id");//
$resultan = $model->queryAll();

$status = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Klasifikasi BUMDesa</th><th>Jumlah</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['klasifikasi'].'</td><td>'.$v['hit'].'</td></tr>';
    $status[] = '\''.$v['klasifikasi'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['klasifikasi']."'}";
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <div id="chart-bumdes-klasifikasi" style="width: 100%;height:300px;">-</div>
            </div>
            <div class="col-md-6"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-bumdes-klasifikasi'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan Klasifikasi',
                type:'pie',
                radius: ['1%', '75%'],
                // label: {
                    formatter: '{b}: ({d}%)',
                // },
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    function eConsole(param) {
       if (typeof param.seriesIndex != 'undefined') {
            // datarr = {title: param.data.name,valuePercent:param.percent};
            // console.log(param);
            var needle = encodeURI(param.name);
            openModalXyf({welm:'900px',url:'/sp3ddashboard/data-chart/data-bumdes?objek=bumdes&subjek=klasifikasi&modul=klasifikasi<?=$bonparam?>&needle='+needle});
            //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
            //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
        }            
    }
    myChart.setOption(option);
    myChart.on('click', eConsole);
</script>