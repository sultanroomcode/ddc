<?php
//KECAMATAN
use hscstudio\mimin\components\Mimin;
use sp3d\models\transaksi\TaRAB;
use sp3d\models\transaksi\TaRABRinci;
use sp3d\models\DesaCount;
use yii\helpers\Url;
$formatt = new DesaCount();
$conn = Yii::$app->db;
// $detectTKD = TaRABRinci::findOne(['tahun' => $tahun, 'Kd_Rincian' => '1.1.1.02.01']);//perlu tahun, namun untuk sat ini jangan dimasukkan dulu
$model = $conn->createCommand("SELECT SUM(`Anggaran`) AS Ang, `sumberdana` FROM `Ta_RABRinci` WHERE `tahun` ='$tahun' AND `Kd_Rincian` LIKE '4.%' AND user_id LIKE '".$kd."%' GROUP BY `sumberdana`");

$resultan = $model->queryAll();

$sourcedana = [];
$puresource = [];
$nilaidana =0;
$tabledana = '<table class="table table-striped">';
foreach($resultan as $v){

    // if($detectTKD != null && $v['sumberdana'] == 'PAD'){
    //     //maka ada Bengkok kurangi dengan bengkok
    //     $sourcedana[] = "{value: ".($v['Ang']-$detectTKD->Anggaran).", name:'".$v['sumberdana']."'}";
    //     $nilaidana += ($v['Ang'] - $detectTKD->Anggaran);
    // } else {
        $sourcedana[] = "{value: ".$v['Ang'].", name:'".$v['sumberdana']."'}";
        $nilaidana += $v['Ang'];
    // }
    $puresource[] = "{value: ".($v['Ang']).", name:'".$v['sumberdana']."'}";
    $tabledana .= '<tr><td>'.$v['sumberdana'].'</td><td align="right">'.$formatt->nf($v['Ang']).'</td></tr>';
    
}
$tabledana .= '<tr><td></td><td align="right">'.$formatt->nf($nilaidana).'</td></tr>';
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-pendapatan-provinsi" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-pendapatan-provinsi'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Pendapatan',
                type:'pie',
                radius: ['1%', '55%'],
                // label: {
                //     normal: {
                //         formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
                //         backgroundColor: '#eee',
                //         borderColor: '#aaa',
                //         borderWidth: 1,
                //         borderRadius: 4,
                //         // shadowBlur:3,
                //         // shadowOffsetX: 2,
                //         // shadowOffsetY: 2,
                //         // shadowColor: '#999',
                //         // padding: [0, 7],
                //         // rich: {
                //         //     a: {
                //         //         color: '#999',
                //         //         lineHeight: 15,
                //         //         align: 'center'
                //         //     },
                //         //     // abg: {
                //         //     //     backgroundColor: '#333',
                //         //     //     width: '100%',
                //         //     //     align: 'right',
                //         //     //     height: 22,
                //         //     //     borderRadius: [4, 4, 0, 0]
                //         //     // },
                //         //     hr: {
                //         //         borderColor: '#aaa',
                //         //         width: '100%',
                //         //         borderWidth: 0.5,
                //         //         height: 0
                //         //     },
                //         //     b: {
                //         //         fontSize: 16,
                //         //         lineHeight: 33
                //         //     },
                //         //     per: {
                //         //         color: '#eee',
                //         //         backgroundColor: '#334455',
                //         //         padding: [2, 4],
                //         //         borderRadius: 2
                //         //     }
                //         // }
                //     }
                // },
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>