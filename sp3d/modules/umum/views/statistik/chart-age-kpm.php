<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();
$id = $kd;
$clausa = '';
// if(Yii::$app->user->identity->type != 'desa'){
//     $clausa = "WHERE user LIKE '".Yii::$app->user->identity->id."%'";
// }
switch ($tingkat) {
    case 'desa':
        $clausa = "WHERE user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "WHERE kd_kecamatan  = '".$id."'";
        $kecamatan = $id;
        $kabupaten = substr($id, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "WHERE kd_kabupaten  = '".$id."'";
        $kecamatan = '';
        $kabupaten = $id;
    break;
    default://provinsi
        // $clausa = "WHERE kd_provinsi  = '".$id."'";
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;//2018-
$model = $conn->createCommand("SELECT 
CASE WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 20 THEN '0-20th'
    WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 30 THEN '21-30th'
    WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 50 THEN '31-50th'
    WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 70 THEN '51-70th'
    WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) > 70 THEN '71-100th' END AS umur,
COUNT(*) hit
FROM kpm ".$clausa." GROUP BY umur");
$resultan = $model->queryAll();

$umur = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Umur</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['umur'].' th</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$v['umur'].'\')" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.$v['umur'].'\')" class="btn btn-danger">export</a></td></tr>';
    $umur[] = '\''.$v['umur'].'\'';
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-umur-kpm" style="width: 100%;height:300px;">-</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-umur-kpm'));

    option = {
        backgroundColor:'#fff',
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $umur)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    myChart.setOption(option);

    function goView(needle) {
        openModalXyf({welm:'900px',url:'/laporan/kpm/data?objek=kpm&subjek=umur&modul=kpm-umur&subjekval='+needle+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});            
    }

    function goExport(needle){
        window.open(base_url+'/laporan/kpm/export?objek=kpm&subjek=umur&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>