<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;
$id = $kd;
$user = User::find()->where(['type' => 'desa'])->count();
switch ($tingkat) {
    case 'desa':
        // $query = "WHERE user  = '".$id."'";
    break;
    case 'kecamatan':
        $query = "SELECT count(*) AS hit, user.id, description AS nama_kabupaten FROM bumdes LEFT JOIN user ON user.id = bumdes.kd_desa COLLATE utf8_unicode_ci WHERE bumdes.kd_kecamatan  = '".$id."' GROUP BY bumdes.kd_desa";
        $colName = 'Nama Desa';
        $chartType = 'pie';
    break;
    case 'kabupaten':
        $query = "SELECT count(*) AS hit, user.id, description AS nama_kabupaten FROM bumdes LEFT JOIN user ON user.id = bumdes.kd_kecamatan COLLATE utf8_unicode_ci WHERE bumdes.kd_kabupaten  = '".$id."' GROUP BY bumdes.kd_kecamatan";
        $colName = 'Nama Kecamatan';
        $chartType = 'pie';
    break;
    default://provinsi
        $query = "SELECT count(*) AS hit, user.id, description AS nama_kabupaten FROM bumdes LEFT JOIN user ON user.id = bumdes.kd_kabupaten COLLATE utf8_unicode_ci GROUP BY kd_kabupaten";
        $colName = 'Nama Kabupaten';
        $chartType = 'stack';
    break;
}

$bonparam = '&kabupaten-input='.$data['kabupaten-input'].'&kecamatan-input='.$data['kecamatan-input'];

$conn = Yii::$app->db;
$model = $conn->createCommand($query);
$resultan = $model->queryAll();

$kabupaten = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>'.$colName.'</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['nama_kabupaten'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="openData('.$v['id'].')" class="btn btn-danger">Lihat</a></td></tr>';
    if($chartType == 'pie'){
        $puresource[] = "{value: ".($v['hit']).", name:'".$v['nama_kabupaten']."'}";
    } else {
        $puresource[] = "{value: ".($v['hit']).", name:'".$v['nama_kabupaten']."'}";
        //stack
        $kabupaten[] = '\''.$v['nama_kabupaten'].'\'';
        $hitung[] = $v['hit'];
    }
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-bumdes-region" style="width: 100%;height:500px;">aa</div>
        <div class="row">
            <div class="col-md-12">
                <div style="overflow-y: auto; height: 300px;"><?= $tabledana ?></div>
            </div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-bumdes-region'));
    <?php if($chartType == 'pie' || $chartType == 'stack'){ ?>
    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan daerah/desa',
                type:'pie',
                radius: ['1%', '55%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    <?php } ?>

    <?php if($chartType == 'stack'){ ?>
    /*var labelOption = {
        normal: {
            show: true,
            position: 'insideBottom',
            rotate: 90,
            textStyle: {
                align: 'left',
                verticalAlign: 'middle'
            }
        }
    };

    option = {
        backgroundColor:'#fff',
        height:300,
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $kabupaten)?>],
                axisTick: {
                    alignWithLabel: true
                },
                axisLabel:{
                  rotate:80,
                  interval:0
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'pie',
                barWidth: '15%',
                label:labelOption,
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };*/
    <?php } ?>
    /*function eConsole(param) {
       if (typeof param.seriesIndex != 'undefined') {
            // datarr = {title: param.data.name,valuePercent:param.percent};
            // console.log(param);
            var needle = encodeURI(param.name);
            openModalXyf({welm:'900px',url:'/sp3ddashboard/data-chart/data-bumdes?objek=bumdes&subjek=region&modul=region<?=$bonparam?>&needle='+needle});
            '.$v['id'].'
            //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
            //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
        }            
    }*/
    myChart.setOption(option);
    // myChart.on('click', eConsole);
    function openData(id)
    {
        openModalXyf({welm:'900px',url:'/sp3ddashboard/data-chart/data-bumdes?objek=bumdes&subjek=region&modul=region<?=$bonparam?>&needle='+id});
    }
</script>