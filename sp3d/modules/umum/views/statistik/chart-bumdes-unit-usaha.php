<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
switch ($tingkat) {
     case 'desa':
        $clausa = "WHERE kd_desa  = '".$kd."'";
    break;
    case 'kecamatan':
    	$clausa = "WHERE kd_kecamatan = '".$kd."'";
    break;
    case 'kabupaten':
        $clausa = "WHERE kd_kabupaten = '".$kd."'";
    break;
    default://provinsi
        // $clausa = "AND kd_provinsi  = '".$id."'";
    break;
}

$bonparam = '&kabupaten-input='.$data['kabupaten-input'].'&kecamatan-input='.$data['kecamatan-input'];

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(*) AS hit, btuu.title AS unit_usaha FROM bumdes_unit_usaha buu LEFT JOIN bumdes_tipe_unit_usaha btuu ON buu.unit_usaha = btuu.id COLLATE utf8_unicode_ci ".$clausa." GROUP BY unit_usaha  ORDER BY hit DESC LIMIT 20");//
$resultan = $model->queryAll();

$status = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Unit Usaha</th><th>Jumlah</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['unit_usaha'].'</td><td>'.$v['hit'].'</td></tr>';
    $status[] = '\''.$v['unit_usaha'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['unit_usaha']."'}";
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <div id="chart-bumdes-unit_usaha" style="width: 100%;height:300px;">-</div>
            </div>
            <div class="col-md-6"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-bumdes-unit_usaha'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan Unit Usaha',
                type:'pie',
                radius: ['1%', '75%'],
                // label: {
                    formatter: '{b}: ({d}%)',
                // },
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    function eConsole(param) {
       if (typeof param.seriesIndex != 'undefined') {
            // datarr = {title: param.data.name,valuePercent:param.percent};
            // console.log(param);
            var needle = encodeURI(param.name);
            openModalXyf({welm:'900px',url:'/sp3ddashboard/data-chart/data-bumdes?objek=bumdes&subjek=unit-usaha&modul=unit-usaha<?=$bonparam?>&needle='+needle});
            //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
            //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
        }            
    }
    myChart.setOption(option);
    myChart.on('click', eConsole);
</script>