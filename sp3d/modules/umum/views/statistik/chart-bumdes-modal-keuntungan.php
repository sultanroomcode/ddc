<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
$id = $kd;
// if(Yii::$app->user->identity->type != 'desa'){
//     $clausa = "WHERE user LIKE '".Yii::$app->user->identity->id."%'";
// }
switch ($tingkat) {
    case 'desa':
        $clausa = "WHERE user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "WHERE kd_kecamatan  = '".$id."'";
    break;
    case 'kabupaten':
        $clausa = "WHERE kd_kabupaten  = '".$id."'";
    break;
    default://provinsi
        // $clausa = "WHERE kd_provinsi  = '".$id."'";
    break;
}

$bonparam = '&kabupaten-input='.$data['kabupaten-input'].'&kecamatan-input='.$data['kecamatan-input'];

$conn = Yii::$app->db;//2018-
$model = $conn->createCommand("SELECT 
CASE WHEN (keuntungan) <= 1000000 THEN '0-1jt'
     WHEN (keuntungan) <= 5000000 THEN '1-5jt'
     WHEN (keuntungan) <= 10000000 THEN '5-10jt'
     WHEN (keuntungan) <= 50000000 THEN '10-50jt'
     WHEN (keuntungan) > 50000000 THEN '50jt-keatas' END AS modal_diff,
COUNT(*) hit
FROM bumdes_modal ".$clausa." GROUP BY modal_diff");
$resultan = $model->queryAll();

$umur = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Rentang Keuntungan</th><th>Jumlah</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['modal_diff'].'</td><td>'.$v['hit'].'</td></tr>';
    $umur[] = '\''.$v['modal_diff'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['modal_diff']."'}";
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-bumdes-modal-keuntungan" style="width: 100%;height:300px;">-</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-bumdes-modal-keuntungan'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan keuntungan',
                type:'pie',
                radius: ['1%', '75%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };

    function eConsole(param) {
       if (typeof param.seriesIndex != 'undefined') {
            // datarr = {title: param.data.name,valuePercent:param.percent};
            // console.log(param);
            openModalXyf({welm:'900px',url:'/sp3ddashboard/data-chart/data-bumdes?objek=bumdes&subjek=keuntungan&modul=keuntungan<?=$bonparam?>&needle='+param.name});
            //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
            //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
        }            
    }

    myChart.setOption(option);
    myChart.on('click', eConsole);
</script>