<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
// if(Yii::$app->user->identity->type != 'desa'){
//     $clausa = "WHERE user LIKE '".Yii::$app->user->identity->id."%'";
// }
switch ($tingkat) {
    case 'desa':
        $clausa = "WHERE user  = '".$kd."'";
    break;
    case 'kecamatan':
        $clausa = "WHERE kd_kecamatan  = '".$kd."'";
        $kecamatan = $kd;
        $kabupaten = substr($kd, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "WHERE kd_kabupaten  = '".$kd."'";
        $kecamatan = '';
        $kabupaten = $kd;
    break;
    default://provinsi
        $clausa = "WHERE kd_provinsi  = '".$kd."'";
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;//2018-
$model = $conn->createCommand("SELECT COUNT(*) hit, CASE WHEN (tahun) <= 1990 THEN '0-1990'
     WHEN (tahun) <= 2000 THEN '1991-2000'
     WHEN (tahun) <= 2010 THEN '2001-2010'
     WHEN (tahun) <= ".date('Y')." THEN '2011-".date('Y')."'
     END AS tahun_diff FROM `pasardesa` $clausa GROUP BY tahun_diff;");
$resultan = $model->queryAll();

$umur = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Rentang Tahun</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['tahun_diff'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$v['tahun_diff'].'\')" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.$v['tahun_diff'].'\')" class="btn btn-danger">export</a></td></tr>';
    $umur[] = '\''.$v['tahun_diff'].'\'';
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-pasar-tahun" style="width: 100%;height:350px;">aa</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-pasar-tahun'));

    option = {
        backgroundColor:'#fff',
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $umur)?>],
                axisTick: {
                    alignWithLabel: true
                },
                axisLabel:{
                  rotate:45,
                  interval:0
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '20%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    myChart.setOption(option);

    function goView(needle) {
        openModalXyf({welm:'900px',url:'/laporan/pasar-desa/data?objek=pasar-desa&subjek=tahun&subjekval='+needle+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});
    }

    function goExport(needle){
        window.open(base_url+'/laporan/pasar-desa/export?objek=pasar-desa&subjek=tahun&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>