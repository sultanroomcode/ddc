<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
// if(Yii::$app->user->identity->type != 'desa'){
//     $clausa = "WHERE user LIKE '".Yii::$app->user->identity->id."%'";
// }
switch ($tingkat) {
    case 'desa':
        $clausa = "AND user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "AND kd_kecamatan  = '".$id."'";
        $kecamatan = $id;
        $kabupaten = substr($id, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "AND kd_kabupaten  = '".$id."'";
        $kecamatan = '';
        $kabupaten = $id;
    break;
    default://provinsi
        $clausa = "AND kd_provinsi  = '".$id."'";
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;//2018-
$model = $conn->createCommand("SELECT 
a.kategori,
b.jabatan,
c.id,
c.tipe,
c.nama_pelatihan,
COUNT(*) hit
FROM sp3d_perangkat_pelatihan a 
LEFT JOIN sp3d_perangkat_desa b ON a.kd_desa = b.user AND a.nik = b.nik 
LEFT JOIN sp3d_master_jenis_pelatihan c ON a.kategori = c.id AND a.tipe = c.tipe 
WHERE b.jabatan = '".$data['jabatan']."' ".$clausa." GROUP BY a.kategori");
$resultan = $model->queryAll();

$umur = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Umur</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['nama_pelatihan'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$v['id'].'\',\''.$v['tipe'].'\',\''.$v['jabatan'].'\')" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.$v['id'].'\',\''.$v['tipe'].'\',\''.$v['jabatan'].'\')" class="btn btn-danger">export</a></td></tr>';
    $umur[] = '\''.$v['kategori'].'\'';
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-pelatihan-bpd" style="width: 100%;height:300px;">-</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-pelatihan-bpd'));

    option = {
        backgroundColor:'#fff',
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $umur)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    
    myChart.setOption(option);

    function goView(id,tipe,jabatan) {
        openModalXyf({welm:'900px',url:'/sp3ddashboard/data-chart/data-pemerintah-desa?jabatan='+jabatan+'&tipe='+tipe+'&objek=bpd&subjek=pelatihan&modul=bpd-pelatihan&needle='+id+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});            
    }

    function goExport(id,tipe,jabatan){
        window.open(base_url+'/sp3ddashboard/data-export/data-pemerintah-desa-chart?jabatan='+jabatan+'&tipe='+tipe+'&objek=bpd&subjek=pelatihan&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+id, '_blank');
    }
</script>