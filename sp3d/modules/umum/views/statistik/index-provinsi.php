<div class="row">
    <div class="col-md-12">
        <!-- Main Chart PROPINSI -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Rencana Pendapatan Provinsi <?= $tahun ?></h2>
                    <div class="p-10" id="in-statistik">In</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Rencana Belanja Provinsi <?= $tahun ?></h2>
                    <div class="p-10" id="out-statistik">Out</div>  
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Dynamic Chart -->

        <!--  Recent Postings -->
        <div class="row">
            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Progres Entri Data Kabupaten <?= $tahun ?></h2>
                    <div class="p-10" id="kab-input-statistik">-</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Progres Entri Data Kecamatan <?= $tahun ?></h2>
                    <div class="p-10" id="kec-input-statistik">-</div>  
                </div>
            </div>

            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Progres Entri Data Desa <?= $tahun ?></h2>
                    <div class="p-10" id="desa-input-statistik">-</div>  
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Dynamic Chart -->
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scriptJs = <<<JS
    goLoad({elm: '#in-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=in'});
    goLoad({elm: '#out-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=out'});
    goLoad({elm: '#kab-input-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=kabupaten-input'});
    goLoad({elm: '#kec-input-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=kecamatan-input'});
    goLoad({elm: '#desa-input-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=desa-input'});
JS;

$this->registerJs($scriptJs);