<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
if(Yii::$app->user->identity->type != 'desa'){
    $clausa = "WHERE sp3d_pendamping_pilih_desa.verified_status = 10 AND sp3d_pendamping_pilih_desa.kd_desa LIKE '".Yii::$app->user->identity->id."%'";
}

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(*) AS hit, jenis_kelamin AS jk FROM sp3d_pendamping_desa LEFT JOIN sp3d_pendamping_pilih_desa ON sp3d_pendamping_desa.nik = sp3d_pendamping_pilih_desa.nik ".$clausa." GROUP BY jenis_kelamin");
$resultan = $model->queryAll();

$gender = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Gender</th><th>Jumlah</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['jk'].'</td><td>'.$v['hit'].'</td></tr>';
    $gender[] = '\''.$v['jk'].'\'';
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-gender-pendamping" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-gender-pendamping'));

    option = {
        backgroundColor:'#fff',
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $gender)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>