<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;
$id = $kd;
$user = User::find()->where(['type' => 'desa'])->count();
switch ($tingkat) {
    case 'desa':
        // $query = "WHERE user  = '".$id."'";
    break;
    case 'kecamatan':
        $query = "SELECT count(*) AS hit, description AS nama_kabupaten, user.id FROM pasardesa LEFT JOIN user ON user.id = pasardesa.kd_desa COLLATE utf8_unicode_ci WHERE pasardesa.kd_kecamatan  = '".$id."' GROUP BY pasardesa.kd_desa";
        $colName = 'Nama Desa';
        $chartType = 'pie';
        $kecamatan = $id;
        $kabupaten = substr($id, 0, 4);
    break;
    case 'kabupaten':
        $query = "SELECT count(*) AS hit, description AS nama_kabupaten, user.id FROM pasardesa LEFT JOIN user ON user.id = pasardesa.kd_kecamatan COLLATE utf8_unicode_ci WHERE pasardesa.kd_kabupaten  = '".$id."' GROUP BY pasardesa.kd_kecamatan";
        $colName = 'Nama Kecamatan';
        $chartType = 'stack';
        $kecamatan = '';
        $kabupaten = $id;
    break;
    default://provinsi
        $query = "SELECT count(*) AS hit, description AS nama_kabupaten, user.id FROM pasardesa LEFT JOIN user ON user.id = pasardesa.kd_kabupaten COLLATE utf8_unicode_ci GROUP BY kd_kabupaten";
        $colName = 'Nama Kabupaten';
        $chartType = 'stack';
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;
$model = $conn->createCommand($query);
$resultan = $model->queryAll();

$kabupatenchart = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>'.$colName.'</th><th>Jumlah Pasar</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['nama_kabupaten'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$tingkat.'\',\''.$v['id'].'\')" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.$tingkat.'\',\''.$v['id'].'\')" class="btn btn-danger">export</a></td></tr>';
    if($chartType == 'pie'){
        $puresource[] = "{value: ".($v['hit']).", name:'".$v['nama_kabupaten']."'}";
    } else {
        //stack
        $kabupatenchart[] = '\''.$v['nama_kabupaten'].'\'';
        $hitung[] = $v['hit'];
    }
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-pasardesa-region" style="width: 100%;height:350px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-pasardesa-region'));
    <?php if($chartType == 'pie'){ ?>
    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan daerah/desa',
                type:'pie',
                radius: ['1%', '55%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    <?php } ?>

    <?php if($chartType == 'stack'){ ?>
    var labelOption = {
        normal: {
            show: true,
            position: 'insideBottom',
            rotate: 90,
            textStyle: {
                align: 'left',
                verticalAlign: 'middle'
            }
        }
    };

    option = {
        backgroundColor:'#fff',
        height:300,
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $kabupatenchart)?>],
                axisTick: {
                    alignWithLabel: true
                },
                axisLabel:{
                  rotate:80,
                  interval:0
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '15%',
                label:labelOption,
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    <?php } ?>
    myChart.setOption(option);

    function goView(type,needle) {
        openModalXyf({welm:'900px',url:'/laporan/pasar-desa/data?objek=pasar-desa&subjek=region&subjekval='+needle+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});//untuk region needle tidak diperhatikan
    }

    function goExport(type,needle){
        window.open(base_url+'/laporan/pasar-desa/export?objek=pasar-desa&subjek=region&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>