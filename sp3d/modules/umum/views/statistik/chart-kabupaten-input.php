<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'kabupaten'])->count();

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT substr(user, 1, 4) AS kode FROM ta_mdb_upload WHERE tahun = '$tahun' GROUP BY kode");
$resultan = $model->queryAll();

$sudah = count($resultan);
$belum = $user - $sudah;

$puresource = [];
$tabledana = '<table class="table table-striped">';
$tabledana .= '<tr><td>Kab. Belum Kirim</td><td>'.$belum.'</td><td></td></tr>';
// $tabledana .= '<tr><td>Kab. Belum Kirim</td><td>'.$belum.'</td><td><a href="javascript:void(0)" onclick="openModalXyf({url:\'/umum/laporan/detail-kabupaten-kirim?tahun='.$tahun.'&status=0\'})" class="label label-danger">Klik Untuk Lihat</a></td></tr>';
$tabledana .= '<tr><td>Kab. Sudah Kirim</td><td>'.$sudah.'</td><td><a href="javascript:void(0)" onclick="openModalXyf({url:\'/umum/laporan/detail-kabupaten-kirim?tahun='.$tahun.'&status=1\'})" class="label label-danger">Klik Untuk Lihat</a></td></tr>';
$tabledana .= '</table>';


//override
$puresource = [
    '{value: '.$belum.', name:"Kab. Belum Kirim"}',
    '{value: '.$sudah.', name:"Kab. Sudah Kirim"}',
];
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-kabupaten-input" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-kabupaten-input'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Kabupaten Input',
                type:'pie',
                radius: ['1%', '55%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>