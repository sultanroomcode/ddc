<?php
$tahun = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Tahun</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($model->all() as $v){
    $tabledana .= '<tr><td>'.$v['tahun'].'</td><td>'.$v['c'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$v['tahun'].'\')" class="btn btn-danger">lihat</a></td></tr>';
    $tahun[] = '\''.$v['tahun'].'\'';
    $hitung[] = $v['c'];
}
$tabledana .= '</table>';
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Rekap Data Entrian</h2>
            <div class="p-10">
                <div class="row">
                    <!-- Box Comment -->
                    <div class="box box-widget animated slideInUp">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5"><div id="chart-entrian" style="width: 100%;height:300px;">-</div></div>
                                <div class="col-md-7"><?= $tabledana ?></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-entrian'));

    option = {
        backgroundColor:'#fff',
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $tahun)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    myChart.setOption(option);

    function goView(needle) {
        openModalXyf({welm:'900px',url:'/umum/statistik/entrian-detail?tahun='+needle});
    }
</script>