<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->andWhere(['LIKE', 'username', $kd.'%', false])->count();//where like kecamatan

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(DISTINCT user) AS hitung FROM ta_mdb_upload WHERE tahun = '$tahun' AND user LIKE '".$kd."%'");//where like kecamatan
$resultan = $model->queryAll();

$sudah = $resultan[0]['hitung'];
$belum = $user - $sudah;

$puresource = [];
$tabledana = '<table class="table table-striped">';
$tabledana .= '<tr><td>Desa Belum Kirim</td><td>'.$belum.'</td></tr>';
$tabledana .= '<tr><td>Desa Sudah Kirim</td><td>'.$sudah.'</td></tr>';
$tabledana .= '</table>';


//override
$puresource = [
    '{value: '.$belum.', name:"Desa Belum Kirim"}',
    '{value: '.$sudah.', name:"Desa Sudah Kirim"}',
];
?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-desa-input" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-desa-input'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Desa Input',
                type:'pie',
                radius: ['1%', '55%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>