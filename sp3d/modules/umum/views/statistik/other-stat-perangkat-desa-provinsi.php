<div class="row">
    <div class="col-md-12">
        <!-- Main Chart PROPINSI -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Perangkat Desa Berdasarkan Jenis Kelamin</h2>
                    <div class="p-10" id="in-statistik">In</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Perangkat Desa Berdasarkan Pendidikan</h2>
                    <div class="p-10" id="out-statistik">Out</div>  
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h2 class="tile-title">Perangkat Desa Berdasarkan Umur</h2>
                    <div class="p-10" id="age-statistik">In</div>  
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$user = ($data['kode'] == null)?Yii::$app->user->identity->id:$data['kode'];
$scriptJs = <<<JS
    goLoad({elm: '#in-statistik', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=gender-perangkat&kd={$user}'});    
    goLoad({elm: '#out-statistik', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pendidikan-perangkat&kd={$user}'});   
    goLoad({elm: '#age-statistik', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=age-perangkat&kd={$user}'});    
JS;

$this->registerJs($scriptJs);