<?php
$tahun = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Modul</th><th>Jumlah</th></tr>';
foreach($model->all() as $v){
    $tabledana .= '<tr><td>'.$v['fdata'].'</td><td>'.$v['c'].'</td></tr>';
    $puresource[] = "{value: ".($v['c']).", name:'".$v['fdata']."'}";
}
$tabledana .= '</table>';
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Rekap Data Entrian Detail</h2>
            <div class="p-10">
                <div class="row">
                    <!-- Box Comment -->
                    <div class="box box-widget animated slideInUp">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-7"><div id="chart-entrian-detail" style="width: 100%;height:500px;">-</div></div>
                                <div class="col-md-5"><?= $tabledana ?></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-entrian-detail'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan entrian detail',
                type:'pie',
                radius: ['1%', '50%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>