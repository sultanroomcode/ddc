<div class="row">
    <div class="col-md-12">
        <!-- Main Chart PROPINSI -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Rencana Pendapatan Kabupaten <?= $tahun ?></h2>
                    <div class="p-10" id="in-statistik">In</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Rencana Belanja Kabupaten <?= $tahun ?></h2>
                    <div class="p-10" id="out-statistik">Out</div>  
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Dynamic Chart -->

        <!--  Recent Postings -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Progres Entri Data Kecamatan <?= $tahun ?></h2>
                    <div class="p-10" id="kec-input-statistik">-</div>  
                </div>
            </div>
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Progres Entri Data Desa <?= $tahun ?></h2>
                    <div class="p-10" id="desa-input-statistik">-</div>  
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Dynamic Chart -->
    </div>
    <div class="clearfix"></div>
</div>
<?php
$user = ($id != null)?$id:Yii::$app->user->identity->id;
$scriptJs = <<<JS
    goLoad({elm: '#in-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=in-kab&kd={$user}'});
    goLoad({elm: '#out-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=out-kab&kd={$user}'});
    goLoad({elm: '#kec-input-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=kec-input-on-kab&kd={$user}'});
    goLoad({elm: '#desa-input-statistik', url: '/umum/statistik/chart?tahun={$tahun}&type=desa-input-on-kab&kd={$user}'});
JS;

$this->registerJs($scriptJs);