<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
$xp = explode('-',$data['type']);
$test = $xp[0].'-'.$xp[1];

if($test == 'podes-pertanian'){
    $objek = $xp[0].'-'.$xp[1].'-'.$xp[2];
    $jenis = $xp[1].'-'.$xp[2];
} else if($test == 'podes-perkebunan'){
    $objek = $xp[0].'-'.$xp[1];
    $jenis = $xp[1];
} else {
    $objek = $xp[0].'-'.$xp[1];
    $jenis = $xp[1];
}

switch($objek)
{
    case 'podes-pertanian-pangan':
    case 'podes-pertanian-buah':
    case 'podes-pertanian-apotik':
    case 'podes-perkebunan':
        $table = 'ddc_podes';
    break;
    case 'podes-kehutanan':
    case 'podes-peternakan':
    case 'podes-perikanan':
        $table = 'ddc_podes_pp';
    break;
}
// if(Yii::$app->user->identity->type != 'desa'){
//     $clausa = "WHERE user LIKE '".Yii::$app->user->identity->id."%'";
// }
switch ($tingkat) {
    case 'desa':
        $clausa = "WHERE kd_desa  = '".$kd."'";
    break;
    case 'kecamatan':
        $kecamatan = $kd;
        $kabupaten = substr($kd, 0, 4);
        $clausa = "WHERE kd_kecamatan  = '".$kd."'";
    break;
    case 'kabupaten':
        $kecamatan = '';
        $kabupaten = $kd;
        $clausa = "WHERE kd_kabupaten  = '".$kd."'";
    break;
    default://provinsi
        $kecamatan = '';
        $kabupaten = '';
        // $clausa = "AND kd_provinsi  = '".$id."'";
    break;
}

$conn = Yii::$app->db;//
$model = $conn->createCommand("SELECT COUNT(*) hit, p.satuan AS subjek_diff, k.nama_satuan
                FROM ".$table." p LEFT JOIN ddc_satuan k ON p.satuan = k.no_satuan WHERE p.jenis = '".$jenis."' $clausa GROUP BY subjek_diff");
$resultan = $model->queryAll();

$umur = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Jenis Komoditas</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['nama_satuan'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$v['subjek_diff'].'\')" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.$v['subjek_diff'].'\')" class="btn btn-danger">export</a></td></tr>';
    $umur[] = '\''.$v['nama_satuan'].'\'';
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['nama_satuan']."'}";
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-podes-satuan" style="width: 100%;height:350px;">-</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-podes-satuan'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan satuan',
                type:'pie',
                radius: ['1%', '75%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    function eConsole(param) {
       if (typeof param.seriesIndex != 'undefined') {
            console.log(param);
        }            
    }
    myChart.setOption(option);
    myChart.on('click', eConsole);

    function goView(needle) {
        openModalXyf({welm:'900px',url:'/laporan/podes/data?objek=<?=$objek?>&subjek=satuan&subjekval='+needle+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});
    }
    function goExport(needle){
        window.open(base_url+'/laporan/podes/export?objek=<?=$objek?>&subjek=satuan&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>