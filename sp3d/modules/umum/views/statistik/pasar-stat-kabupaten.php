<div class="row">
    <div class="col-md-12">
        <!-- Main Chart PROPINSI -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Tahun Berdiri</h2>
                    <div class="p-10" id="pasar-year">Year</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Kabupaten</h2>
                    <div class="p-10" id="pasar-region">Region</div>  
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$user = ($data['kode'] == null)?Yii::$app->user->identity->id:$data['kode'];
$scriptJs = <<<JS
    goLoad({elm: '#pasar-year', url: '/umum/statistik/chart?tingkat=kabupaten&tahun=&type=pasar-tahun&kd={$user}'});    
    goLoad({elm: '#pasar-region', url: '/umum/statistik/chart?tingkat=kabupaten&tahun=&type=pasar-region&kd={$user}'});    
JS;

$this->registerJs($scriptJs);