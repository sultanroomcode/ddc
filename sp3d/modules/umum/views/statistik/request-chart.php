<div class="row">
    <div class="col-md-12">
        <div id="in-statistik"></div>  
    </div>
</div>
<?php
$user = ($data['kode'] == null)?Yii::$app->user->identity->id:$data['kode'];
$tingkat = $data['tingkat'];
switch($data['objek']){
    case 'kepala-desa':
        $objek = 'kades';
    break;
    case 'perangkat-desa':
        $objek = 'perangkat';
    break;
    case 'bpd':
        $objek = 'bpd';
    break;
    case 'bumdes':
        $objek = 'bumdes';
    break;
    case 'pkk':
        $objek = 'pkk';
    break;
    case 'kartar':
        $objek = 'kartar';
    break;
    case 'lpmd':
        $objek = 'lpmd';
    break;
    case 'lembagaadat':
        $objek = 'lembagaadat';
    break;
}      

switch ($data['subjek']) {
    case 'pelatihan':
        $type = 'pelatihan-'.$objek;
    break;
    case 'lama-jabatan':
        $type = 'long-order-'.$objek;
    break;
    case 'pendidikan':
        $type = 'pendidikan-'.$objek;
    break;
    case 'umur':
        $type = 'age-'.$objek;
    break;
    case 'jenis-kelamin':    
        $type = 'gender-'.$objek;
    break;
    //BUMDES
    case 'keuntungan':    
        $type = $objek.'-modal-keuntungan';
    break;
    case 'modal':
        $type = $objek.'-modal';
    break;
    case 'omset':
        $type = $objek.'-modal-omset';
    break;
    case 'tahun':
        $type = $objek.'-tahun';
    break;
    case 'region':
        $type = $objek.'-region';
    break;
    case 'unit-usaha':    
        $type = $objek.'-unit-usaha';
    break;
    case 'klasifikasi':    
        $type = $objek.'-klasifikasi';
    break;
    
    default:
    
    break;
}
$jabatan = $data['jabatan'];
$kabupaten = $data['kabupaten-input'];
$kecamatan = $data['kecamatan-input'];
$scriptJs = <<<JS
    goLoad({elm: '#in-statistik', url: '/umum/statistik/chart?tingkat={$tingkat}&tahun=&jabatan={$jabatan}&type={$type}&kd={$user}&kabupaten-input={$kabupaten}&kecamatan-input={$kecamatan}'});
JS;

$this->registerJs($scriptJs);