<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
// if(Yii::$app->user->identity->type != 'desa'){
//     $clausa = "WHERE user LIKE '".Yii::$app->user->identity->id."%'";
// }
switch ($tingkat) {
    case 'desa':
        $clausa = "WHERE kd_desa  = '".$kd."'";
    break;
    case 'kecamatan':
        $kecamatan = $kd;
        $kabupaten = substr($kd, 0, 4);
        $clausa = "LEFT JOIN pasardesa ON pasardesa.kd_desa = pasardesa_data.kd_desa WHERE pasardesa.kd_kecamatan  = '".$kd."'";
    break;
    case 'kabupaten':
        $kecamatan = '';
        $kabupaten = $kd;
        $clausa = "LEFT JOIN pasardesa ON pasardesa.kd_desa = pasardesa_data.kd_desa WHERE pasardesa.kd_kabupaten  = '".$kd."'";
    break;
    default://provinsi
        // $clausa = "AND kd_provinsi  = '".$id."'";
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;//2018-
$model = $conn->createCommand("SELECT COUNT(*) hit, CASE WHEN (jml_pedagang) <= 50 THEN '1-50'
     WHEN (jml_pedagang) <= 100 THEN '51-100'
     WHEN (jml_pedagang) <= 150 THEN '101-150'
     WHEN (jml_pedagang) <= 200 THEN '150-200'
     WHEN (jml_pedagang) > 200  THEN '200-up'
     END AS jml_pedagang_diff FROM `pasardesa_data` $clausa GROUP BY jml_pedagang_diff;");
$resultan = $model->queryAll();

$umur = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Rentang Jumlah Pedagang</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['jml_pedagang_diff'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$v['jml_pedagang_diff'].'\')" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.$v['jml_pedagang_diff'].'\')" class="btn btn-danger">export</a></td></tr>';
    $umur[] = '\''.$v['jml_pedagang_diff'].'\'';
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['jml_pedagang_diff']."'}";
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-pasar-jumlah-pedagang" style="width: 100%;height:350px;">-</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-pasar-jumlah-pedagang'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan Jumlah Pedagang',
                type:'pie',
                radius: ['1%', '55%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    myChart.setOption(option);

    function goView(needle) {
        openModalXyf({welm:'900px',url:'/laporan/pasar-desa/data?objek=pasar-desa&subjek=jml-pedagang&subjekval='+needle+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});
    }

    function goExport(needle){
        window.open(base_url+'/laporan/pasar-desa/export?objek=pasar-desa&subjek=jml-pedagang&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>