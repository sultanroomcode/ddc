<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();
$id = $kd;
$clausa = '';
switch ($tingkat) {
    case 'desa':
        $clausa = "WHERE user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "WHERE kd_kecamatan  = '".$id."'";
        $kecamatan = $id;
        $kabupaten = substr($id, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "WHERE kd_kabupaten  = '".$id."'";
        $kecamatan = '';
        $kabupaten = $id;
    break;
    default://provinsi
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(*) AS hit, gender AS jk FROM kpm ".$clausa." GROUP BY gender");
$resultan = $model->queryAll();

$gender = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Gender</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['jk'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" class="btn btn-danger" onclick="goExport(\''.$v['jk'].'\')">export</a></td></tr>';
    $gender[] = '\''.$v['jk'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['jk']."'}";
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-6"><div id="chart-gender-kpm" style="width: 100%;height:300px;">-</div></div>
            <div class="col-md-6"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-gender-kpm'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan gender',
                type:'pie',
                radius: ['1%', '75%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };

    function eConsole(param) {
       if (typeof param.seriesIndex != 'undefined') {
            // datarr = {title: param.data.name,valuePercent:param.percent};
            // console.log(param);
            openModalXyf({welm:'900px',url:'/laporan/kpm/data?objek=kpm&subjek=jenis-kelamin&modul=kpm-jenis-kelamin&subjekval='+param.name+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});
            //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
            //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
        }            
    }

    myChart.setOption(option);
    myChart.on('click', eConsole);

    function goExport(needle){
        window.open(base_url+'/laporan/kpm/export?objek=kpm&subjek=jenis-kelamin&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>