<?php
use hscstudio\mimin\components\Mimin;
use sp3d\models\transaksi\TaRAB;
use sp3d\models\DesaCount;
use yii\helpers\Url;
$formatt = new DesaCount();
$conn = Yii::$app->db;
$sql1 = "SELECT SUBSTR(tk.`ID_Keg`, 2, 1) as Keg, tk.`Tahun`, rb.nama, COUNT(tk.ID_Keg) as c_keg FROM Ta_Kegiatan tk LEFT JOIN ref_bidang rb ON SUBSTR(tk.`ID_Keg`, 2, 1) = SUBSTR(rb.`kode`, 2, 1)  WHERE tk.`Tahun` ='$tahun' AND kd_kabupaten = '".$kd."' GROUP BY Keg;";

$sql2 = "SELECT SUM( a.`Anggaran` ) AS Ang, SUBSTR( a.`Kd_Keg` , 7, 2 ) AS Kode, a.`tahun`, b.kode, b.nama
FROM `Ta_RABRinci` a
INNER JOIN `ref_bidang` b ON b.kode = SUBSTR( a.`Kd_Keg` , 7, 2)
COLLATE utf8_unicode_ci
WHERE a.`tahun` ='".$tahun."' AND a.kd_kabupaten = '".$kd."' AND SUBSTR( a.`Kd_Rincian` , 1, 1 ) = '5'
GROUP BY Kode";
$model = $conn->createCommand($sql1);
$model2 = $conn->createCommand($sql2);
// echo $sql1;
// echo $sql2;

$resultcount = $model->queryAll();
$resultang = $model2->queryAll();

$destdana = $valdana = [];
// echo "<hr>";
// var_dump($resultcount);
// echo "<hr>";
// var_dump($resultang);
$nilaidana = $jmldata =0;
$tabledana = '<table class="table">';
$tabledana .= '<tr><td>Kode</td><td>Uraian</td><td>Anggaran</td><td>Keg</td></tr>';
foreach($resultang as $k => $v){
    $destdana[] = "'".$v['kode']."'";
    $valdana[] = $v['Ang'];
    $nilaidana += $v['Ang'];
    $jmldata += $resultcount[$k]['c_keg'];
    
    $tabledana .= '<tr><td>'.$v['kode'].'</td><td>'.$v['nama'].'</td><td align="right">'.$formatt->nf($v['Ang']).'</td><td>'.$formatt->nfo($resultcount[$k]['c_keg']).'</td></tr>';    
}
$tabledana .= '<tr><td></td><td></td><td align="right">'.$formatt->nf($nilaidana).'</td><td>'.$formatt->nfo($jmldata).'</td></tr>';
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12"><div id="chart-belanja-provinsi" style="width: 100%;height:300px;">aa</div></div>
            <div class="col-md-12"><h2>Table Penyerapan Dana Belanja</h2><?= $tabledana ?>
            </div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-belanja-provinsi'));
    option = {
        color: ['#00f'],
        backgroundColor:'#fff',
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $destdana)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $valdana)?>]
            }
        ]
    };
    
    myChart.setOption(option);
</script>