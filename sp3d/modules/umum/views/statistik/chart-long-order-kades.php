<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
switch ($tingkat) {
    case 'desa':
        $clausa = "AND user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "AND kd_kecamatan  = '".$id."'";
        $kecamatan = $id; 
        $kabupaten = substr($kecamatan, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "AND kd_kabupaten  = '".$id."'";
        $kecamatan = ''; 
        $kabupaten = $id;
    break;
    default://provinsi
        $kecamatan = ''; 
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;//2018-
$model = $conn->createCommand("SELECT 
(DATE_FORMAT(NOW(), '%Y') - tahun_awal) AS umur,
COUNT(*) hit
FROM sp3d_perangkat_desa WHERE jabatan = 'kepala-desa' ".$clausa." GROUP BY umur");
$resultan = $model->queryAll();

$umur = $hitung = [];
$tabledana = '<table class="table">';
$tabledana = '<table class="table table-striped"><tr><th>Lama Jabatan</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['umur'].' th</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="openModalXyf({welm:\'900px\',url:\'/sp3ddashboard/data-chart/data-pemerintah-desa?objek=kepala-desa&subjek=lama-jabatan&modul=kepala-desa-lama-jabatan&kabupaten-input='.$kabupaten.'&kecamatan-input='.$kecamatan.'&needle='.urlencode($v['umur']).'\'});" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.urlencode($v['umur']).'\')" class="btn btn-danger">export</a></td></tr>';
    $umur[] = '\''.$v['umur'].'\'';
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-masa-kades" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-masa-kades'));

    option = {
        backgroundColor:'#fff',
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $umur)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    myChart.setOption(option);
    function goExport(needle){
        window.open(base_url+'/sp3ddashboard/data-export/data-pemerintah-desa-chart?objek=kepala-desa&subjek=lama-jabatan&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>