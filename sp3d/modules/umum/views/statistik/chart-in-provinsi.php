<?php
use hscstudio\mimin\components\Mimin;
use sp3d\models\transaksi\TaRAB;
use sp3d\models\transaksi\TaRABRinci;
use sp3d\models\DesaCount;
use yii\helpers\Url;
$formatt = new DesaCount();
$conn = Yii::$app->db;
$detectTKD = TaRABRinci::findOne(['Tahun' => $tahun, 'Kd_Rincian' => '1.1.1.02.01']);//perlu tahun, namun untuk sat ini jangan dimasukkan dulu
$model = $conn->createCommand("SELECT SUM(`t`.`Anggaran`) AS Ang, `t`.`sumberdana`, `r`.`Nama_Sumber` FROM `Ta_RABRinci` `t` LEFT JOIN `Ref_Sumber` `r` ON `r`.`Kode` = `t`.`sumberdana` COLLATE utf8_unicode_ci WHERE `tahun` ='$tahun' AND `Kd_Rincian` LIKE '4.%' GROUP BY `sumberdana`");

$model2 = $conn->createCommand("SELECT SUM(`t`.`Anggaran`) AS Anggtotal FROM `Ta_RABRinci` `t` WHERE `tahun` ='$tahun' AND `Kd_Rincian` LIKE '4.%'");

$resultan = $model->queryAll();
$resultan2 = $model2->queryAll();
$nilaibagi = (float) $resultan2[0]['Anggtotal'];
$sourcedana = [];
$puresource = [];
$nilaidana =0;
$tabledana = '<table class="table table-striped">';
foreach($resultan as $v){

    if($detectTKD != null && $v['sumberdana'] == 'PAD'){
        //maka ada Bengkok kurangi dengan bengkok
        $sourcedana[] = "{value: ".($v['Ang']-$detectTKD->Anggaran).", name:'".$v['sumberdana']." ".(($v['Ang'] / $nilaibagi) * 100)."%'}";
        $nilaidana += ($v['Ang'] - $detectTKD->Anggaran);
    } else {
        $sourcedana[] = "{value: ".$v['Ang'].", name:'".$v['sumberdana']."'}";
        $nilaidana += $v['Ang'];
    }
    $puresource[] = "{value: ".($v['Ang']).", name:'".$v['sumberdana']."'}";
    $tabledana .= '<tr><td>'.$v['sumberdana'].'</td><td>'.number_format((($v['Ang'] / $nilaibagi) * 100), 2).' %</td><td>'.$v['Nama_Sumber'].'</td><td align="right">'.$formatt->nf($v['Ang']).'</td></tr>';
    
}
$tabledana .= '<tr><td></td><td></td><td></td><td align="right">'.$formatt->nf($nilaidana).'</td></tr>';
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-pendapatan-provinsi" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-pendapatan-provinsi'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Pendapatan',
                type:'pie',
                radius: ['1%', '55%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>