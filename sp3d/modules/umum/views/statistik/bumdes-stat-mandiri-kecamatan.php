<div class="row">
    <div class="col-md-12">
        <!-- Main Chart PROPINSI -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Bumdes Berdasarkan Tahun Berdiri</h2>
                    <div class="p-10" id="bumdes-year">In</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Bumdes Berdasarkan Status Aktif</h2>
                    <div class="p-10" id="bumdes-status">Out</div>  
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Bumdes Berdasarkan Kabupaten</h2>
                    <div class="p-10" id="bumdes-region">In</div>  
                </div>
            </div>

            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Bumdes Berdasarkan Modal</h2>
                    <div class="p-10" id="bumdes-modal">In</div>  
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Bumdes Berdasarkan Keuntungan</h2>
                    <div class="p-10" id="bumdes-modal-keuntungan">In</div>  
                </div>
            </div>

            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Bumdes Berdasarkan Omset</h2>
                    <div class="p-10" id="bumdes-modal-omset">In</div>  
                </div>
            </div>

            <div class="col-md-12">
                <div class="tile">
                    <h2 class="tile-title">Bumdes Berdasarkan Unit Usaha</h2>
                    <div class="p-10" id="bumdes-unit-usaha">In</div>  
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$user = ($data['kode'] == null)?Yii::$app->user->identity->id:$data['kode'];
$scriptJs = <<<JS
    goLoad({elm: '#bumdes-year', url: '/umum/statistik/chart?tingkat=kecamatan&tahun=&type=bumdes-tahun&kd={$user}'});    
    goLoad({elm: '#bumdes-status', url: '/umum/statistik/chart?tingkat=kecamatan&tahun=&type=bumdes-status&kd={$user}'});   
    goLoad({elm: '#bumdes-region', url: '/umum/statistik/chart?tingkat=kecamatan&tahun=&type=bumdes-region&kd={$user}'});    
    goLoad({elm: '#bumdes-modal', url: '/umum/statistik/chart?tingkat=kecamatan&tahun=&type=bumdes-modal&kd={$user}'});  
    goLoad({elm: '#bumdes-modal-keuntungan', url: '/umum/statistik/chart?tingkat=kecamatan&tahun=&type=bumdes-modal-keuntungan&kd={$user}'});    
    goLoad({elm: '#bumdes-modal-omset', url: '/umum/statistik/chart?tingkat=kecamatan&tahun=&type=bumdes-modal-omset&kd={$user}'}); 
    goLoad({elm: '#bumdes-unit-usaha', url: '/umum/statistik/chart?tingkat=kecamatan&tahun=&type=bumdes-unit-usaha&kd={$user}'});  
JS;

$this->registerJs($scriptJs);