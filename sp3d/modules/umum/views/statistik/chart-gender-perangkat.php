<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
switch ($tingkat) {
    case 'desa':
        $clausa = "AND user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "AND kd_kecamatan  = '".$id."'";
        $kecamatan = $id;
        $kabupaten = substr($id, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "AND kd_kabupaten  = '".$id."'";
        $kecamatan = '';
        $kabupaten = $id;
    break;
    default://provinsi
        $clausa = "AND kd_provinsi  = '".$id."'";
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(*) AS hit, jenis_kelamin AS jk FROM sp3d_perangkat_desa WHERE jabatan != 'kepala-desa' AND jabatan = '".$data['jabatan']."' AND tipe ='desa' ".$clausa." GROUP BY jenis_kelamin");
$resultan = $model->queryAll();

$gender = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Gender</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['jk'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" class="btn btn-danger" onclick="goExport(\''.$v['jk'].'\')">export</a></td></tr>';
    $gender[] = '\''.$v['jk'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['jk']."'}";
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-gender-perangkat" style="width: 100%;height:300px;">-</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-gender-perangkat'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan gender',
                type:'pie',
                radius: ['1%', '75%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    function eConsole(param) {
       if (typeof param.seriesIndex != 'undefined') {
            // datarr = {title: param.data.name,valuePercent:param.percent};
            // console.log(param);
            openModalXyf({welm:'900px',url:'/sp3ddashboard/data-chart/data-pemerintah-desa?objek=perangkat-desa&jabatan=<?=$data['jabatan']?>&subjek=jenis-kelamin&modul=perangkat-desa-jenis-kelamin&needle='+param.name+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});
            //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
            //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
        }            
    }

    myChart.setOption(option);
    myChart.on('click', eConsole);

    function goExport(needle){
        window.open(base_url+'/sp3ddashboard/data-export/data-pemerintah-desa-chart?objek=perangkat-desa&jabatan=<?=$data['jabatan']?>&subjek=jenis-kelamin&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>