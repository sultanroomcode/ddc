<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();
$id = $kd;
$clausa = $clausa2 = '';

switch ($tingkat) {
    case 'desa':
        $clausa = "WHERE user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "WHERE kd_kecamatan  = '".$id."'";
        $kecamatan = $id;
        $kabupaten = substr($id, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "WHERE kd_kabupaten  = '".$id."'";
        $kecamatan = '';
        $kabupaten = $id;
    break;
    default://provinsi
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(*) AS hit, status_aktif FROM kpm ".$clausa." GROUP BY status_aktif");
$resultan = $model->queryAll();

$pendidikan = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Pendidikan Terakhir</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['status_aktif'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" onclick="goView(\''.$v['status_aktif'].'\')" class="btn btn-danger">lihat</a> <a href="javascript:void(0)" onclick="goExport(\''.$v['status_aktif'].'\')" class="btn btn-danger">export</a></td></tr>';
    $pendidikan[] = '\''.$v['status_aktif'].'\'';
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-5"><div id="chart-pendidikan-kades" style="width: 100%;height:300px;">-</div></div>
            <div class="col-md-7"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-pendidikan-kades'));

    option = {
        backgroundColor:'#fff',
        color: ['#00f'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $pendidikan)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    myChart.setOption(option);

    function goView(needle) {
        openModalXyf({welm:'900px',url:'/laporan/kpm/data?objek=kpm&subjek=status&modul=kpm-status&subjekval='+needle+'&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});            
    }

    function goExport(needle){
        window.open(base_url+'/laporan/kpm/export?objek=kpm&subjek=status&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval='+needle, '_blank');
    }
</script>