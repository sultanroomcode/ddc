<div class="row">
    <div class="col-md-12">
        <!-- Main Chart PROPINSI -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Pendamping Desa Berdasarkan Jenis Kelamin</h2>
                    <div class="p-10" id="gender-statistik">In</div>  
                </div>
            </div>

            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Pendamping Desa Berdasarkan Umur</h2>
                    <div class="p-10" id="age-statistik">In</div>  
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$user = Yii::$app->user->identity->id;
$scriptJs = <<<JS
    goLoad({elm: '#gender-statistik', url: '/umum/statistik/chart?tahun=&type=gender-pendamping&kd={$user}'});    
    goLoad({elm: '#age-statistik', url: '/umum/statistik/chart?tahun=&type=age-pendamping&kd={$user}'});    
JS;

$this->registerJs($scriptJs);