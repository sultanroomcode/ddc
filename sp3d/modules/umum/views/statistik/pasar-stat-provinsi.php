<div class="row">
    <div class="col-md-12">
        <!-- Main Chart PROPINSI -->
        <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Tahun Berdiri</h2>
                    <div class="p-10" id="pasar-year">Year</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-6">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Kabupaten</h2>
                    <div class="p-10" id="pasar-region">Region</div>  
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Jumlah Pedagang</h2>
                    <div class="p-10" id="pasar-jumlah-pedagang">Year</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Jumlah Kios</h2>
                    <div class="p-10" id="pasar-jumlah-kios">Region</div>  
                </div>
            </div>

            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Jumlah Los</h2>
                    <div class="p-10" id="pasar-jumlah-los">Region</div>  
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Jumlah Lapak</h2>
                    <div class="p-10" id="pasar-jumlah-lapak">Year</div>  
                </div>
            </div>
            <!-- Tasks to do -->
            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Jumlah Lesehan</h2>
                    <div class="p-10" id="pasar-jumlah-lesehan">Region</div>  
                </div>
            </div>

            <div class="col-md-4">
                <div class="tile">
                    <h2 class="tile-title">Pasar Desa Berdasarkan Jumlah Ruko</h2>
                    <div class="p-10" id="pasar-jumlah-ruko">Region</div>  
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$user = ($data['kode'] == null)?Yii::$app->user->identity->id:$data['kode'];
$scriptJs = <<<JS
    goLoad({elm: '#pasar-year', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-tahun&kd={$user}'});    
    goLoad({elm: '#pasar-region', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-region&kd={$user}'});    

    goLoad({elm: '#pasar-jumlah-pedagang', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-jumlah-pedagang&kd={$user}'});    
    goLoad({elm: '#pasar-jumlah-kios', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-jumlah-kios&kd={$user}'});    
    goLoad({elm: '#pasar-jumlah-los', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-jumlah-los&kd={$user}'});    

    goLoad({elm: '#pasar-jumlah-lapak', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-jumlah-lapak&kd={$user}'});    
    goLoad({elm: '#pasar-jumlah-lesehan', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-jumlah-lesehan&kd={$user}'});    
    goLoad({elm: '#pasar-jumlah-ruko', url: '/umum/statistik/chart?tingkat=provinsi&tahun=&type=pasar-jumlah-ruko&kd={$user}'});    
JS;

$this->registerJs($scriptJs);