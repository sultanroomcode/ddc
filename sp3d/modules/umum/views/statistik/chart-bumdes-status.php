<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
switch ($tingkat) {
     case 'desa':
        $clausa = "WHERE kd_desa  = '".$kd."'";
    break;
    case 'kecamatan':
        $clausa = "WHERE kd_kecamatan  = '".$kd."'";
    break;
    case 'kabupaten':
        $clausa = "WHERE kd_kabupaten  = '".$kd."'";
    break;
    default://provinsi
        // $clausa = "AND kd_provinsi  = '".$id."'";
    break;
}

$bonparam = '&kabupaten-input='.$data['kabupaten-input'].'&kecamatan-input='.$data['kecamatan-input'];

$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT count(*) AS hit, status FROM bumdes ".$clausa." GROUP BY status");
$resultan = $model->queryAll();

$status = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Status Bumdes</th><th>Jumlah</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['status'].'</td><td>'.$v['hit'].'</td></tr>';
    $status[] = '\''.$v['status'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['status']."'}";
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-bumdes-status" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-bumdes-status'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan gender',
                type:'pie',
                radius: ['1%', '55%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>