<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();

$clausa = '';
if(Yii::$app->user->identity->type != 'desa'){
    $clausa = "WHERE sp3d_pendamping_pilih_desa.verified_status = 10 AND sp3d_pendamping_pilih_desa.kd_desa LIKE '".Yii::$app->user->identity->id."%'";
}

$conn = Yii::$app->db;//2018-
$model = $conn->createCommand("SELECT 
CASE WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 20 THEN '1-20'
     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 30 THEN '20-30'
     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 50 THEN '30-50'
     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 50 THEN '50-100' END AS umur,
COUNT(*) hit
FROM sp3d_pendamping_desa LEFT JOIN sp3d_pendamping_pilih_desa ON sp3d_pendamping_desa.nik = sp3d_pendamping_pilih_desa.nik ".$clausa." GROUP BY umur");
$resultan = $model->queryAll();

$umur = $hitung = [];
$tabledana = '<table class="table table-striped"><tr><th>Umur</th><th>Jumlah</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['umur'].' th</td><td>'.$v['hit'].'</td></tr>';
    $umur[] = '\''.$v['umur'].'\'';
    $hitung[] = $v['hit'];
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="chart-umur-kades" style="width: 100%;height:300px;">aa</div>
        <div class="row">
            <div class="col-md-12"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-umur-kades'));

    option = {
        backgroundColor:'#fff',
        color: ['#0ff'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',//padding
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                color : '#fff',
                data : [<?=implode(',', $umur)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $hitung)?>]
            }
        ]
    };
    myChart.setOption(option);
</script>