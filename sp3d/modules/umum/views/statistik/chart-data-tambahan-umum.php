<?php
use hscstudio\mimin\components\Mimin;
use yii\helpers\Url;
use sp3d\models\User;

$user = User::find()->where(['type' => 'desa'])->count();
$id = $kd;
$clausa = $clausa_join = '';
switch ($tingkat) {
    case 'desa':
        $clausa = "AND user  = '".$id."'";
    break;
    case 'kecamatan':
        $clausa = "AND kd_kecamatan  = '".$id."'";
        $clausa_join = "SELECT count(*) AS hit, 'Belum Isi' AS jk FROM (SELECT SUBSTR(a.id, 1,4) AS kb,SUBSTR(a.id, 1,7) AS kc  FROM user a LEFT JOIN sp3d_profile_desa b ON a.id = b.user COLLATE utf8_general_ci WHERE a.type = 'desa' AND b.email IS NULL HAVING kc = '".$id."') ABC";
        $kecamatan = $id;
        $kabupaten = substr($id, 0, 4);
    break;
    case 'kabupaten':
        $clausa = "AND kd_kabupaten  = '".$id."'";
        $clausa_join = "SELECT count(*) AS hit, 'Belum Isi' AS jk FROM (SELECT SUBSTR(a.id, 1,4) AS kb,SUBSTR(a.id, 1,7) AS kc  FROM user a LEFT JOIN sp3d_profile_desa b ON a.id = b.user COLLATE utf8_general_ci WHERE a.type = 'desa' AND b.email IS NULL HAVING kb = '".$id."') ABC";
        $kecamatan = '';
        $kabupaten = $id;
    break;
    default://provinsi
        $clausa_join = "SELECT count(*) AS hit, 'Belum Isi' AS jk, SUBSTR(a.id, 1,4) AS kb,SUBSTR(a.id, 1,7) AS kc  FROM user a LEFT JOIN sp3d_profile_desa b ON a.id = b.user COLLATE utf8_general_ci WHERE a.type = 'desa' AND b.email IS NULL";
        $kecamatan = '';
        $kabupaten = '';
    break;
}

$conn = Yii::$app->db;
//sudah isi
$model = $conn->createCommand("SELECT count(*) AS hit, 'Sudah Isi' AS jk FROM sp3d_profile_desa WHERE LENGTH(user) = 10 ".$clausa);
//belum isi
$model2 = $conn->createCommand($clausa_join);
$resultan = $model->queryAll();
$resultan2 = $model2->queryAll();

$gender = $hitung = $puresource = [];
$tabledana = '<table class="table table-striped"><tr><th>Status</th><th>Jumlah</th><th>Aksi</th></tr>';
foreach($resultan as $v){
    $tabledana .= '<tr><td>'.$v['jk'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" class="btn btn-danger" onclick="goView()">lihat</a> <a href="javascript:void(0)" class="btn btn-danger" onclick="goExport(\''.$v['jk'].'\')">export</a></td></tr>';
    $gender[] = '\''.$v['jk'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['jk']."'}";
}

foreach($resultan2 as $v){
    $tabledana .= '<tr><td>'.$v['jk'].'</td><td>'.$v['hit'].'</td><td><a href="javascript:void(0)" class="btn btn-danger" onclick="goView2()">lihat</a> <a href="javascript:void(0)" class="btn btn-danger" onclick="goExport2(\''.$v['jk'].'\')">export</a></td></tr>';
    $gender[] = '\''.$v['jk'].'\'';
    $hitung[] = $v['hit'];
    $puresource[] = "{value: ".($v['hit']).", name:'".$v['jk']."'}";
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-6"><div id="chart-profile" style="width: 100%;height:300px;">-</div></div>
            <div class="col-md-6"><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-profile'));

    option = {
        backgroundColor:'#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name:'Chart berdasarkan status isi data tambahan',
                type:'pie',
                radius: ['1%', '75%'],
                data:[<?=implode(',', $puresource)?>]
            }
        ]
    };

    function goView() {
        // datarr = {title: param.data.name,valuePercent:param.percent};
        // console.log(param);
        openModalXyf({welm:'900px',url:'/laporan/data-tambahan/data?objek=data-tambahan&subjek=umum&modul=data-tambahan-umum&subjekval=&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});
        //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
        //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
    }

    function goView2() {
        // datarr = {title: param.data.name,valuePercent:param.percent};
        // console.log(param);
        openModalXyf({welm:'900px',url:'/laporan/data-tambahan/belum?kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>'});
        //https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value (param value)
        //https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
    }

    myChart.setOption(option);

    function goExport(needle){
        window.open(base_url+'/laporan/data-tambahan/export?objek=data-tambahan&subjek=umum&kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>&subjekval=-', '_blank');
    }

    function goExport2(needle){
        window.open(base_url+'/laporan/data-tambahan/export-belum?kabupaten-input=<?=$kabupaten?>&kecamatan-input=<?=$kecamatan?>', '_blank');
    }
</script>