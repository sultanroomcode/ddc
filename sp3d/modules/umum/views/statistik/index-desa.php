<div class="row">
    <div class="col-md-12">
        <!-- Main Chart DESA -->
        <div class="tile">
            <h2 class="tile-title">Statistik <?= $tahun ?></h2>
            <div class="p-10">
            	Pendapatan :<br><div id="grafik-pendapatan"></div>
                Belanja : <div id="grafik-belanja"></div><br> 
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
goLoad({elm : '#grafik-pendapatan', url:'/transaksi/ta-rab/pendapatan-desa-visual?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
goLoad({elm : '#grafik-belanja', url:'/transaksi/ta-rab/desa-visual?id={$id}&tahun={$tahun}&effect=lightSpeedIn'});
JS;
$this->registerJs($scripts);