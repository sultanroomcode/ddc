<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Master Jabatan Tipe</h2>
            <div class="p-10">
            	<a href="javascript:void(0)" onclick="goLoad({url: '/umum/utilitas/tambah-master-jabatan-tipe'})" class="show-pop btn btn-danger" title="Tambah Jabatan Baru Tipe" data-animation="pop" data-content="<p>Menambahkan Jabatan baru. Yang akan dipilih user desa.</p>"><i class="fa fa-plus"></i></a>
				<!-- <a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-normalize-count'})" class="btn btn-danger" title="Normalisasi Hitungan"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-list-ol"></i></a> -->

				<div class="block-area" id="tableStriped">
				    <h3 class="block-title">Aktifitas Upload</h3>
				    <div class="table-responsive overflow">
				    	<table id="ta-master-jabatan" class="tile table table-bordered table-striped" cellspacing="0">
						    <thead>
						        <tr>
						            <th>ID</th>
						            <th>Nama Jabatan</th>
						            <th>Deskripsi</th>
						            <th>Status</th>
						            <th>Action</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($model->all() as $v): ?>
						        <tr>
						            <td><?= $v->id ?></td>
						            <td><?= $v->nama_tipe_jabatan ?></td>
						            <td><?= $v->description ?></td>
						            <td><?= $v->status ?></td>
						            <td>
					            		<a href="javascript:void(0)" onclick="goLoad({url: '/umum/utilitas/update-master-jabatan-tipe?id=<?=$v->id?>'})" title="Edit Jabatan Tipe" class="label label-info"><i class="fa fa-pencil"></i></a>
					            		
						            </td>
						        </tr>
						        <?php endforeach; ?>
						    </tbody>
						</table>
				    </div>
				</div><br>.
			</div>
	    </div>
    	<div class="clearfix"></div>
    </div>
</div>
<?php
$scripts =<<<JS
	$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
	$('#ta-master-jabatan').DataTable();
JS;

$this->registerJs($scripts);