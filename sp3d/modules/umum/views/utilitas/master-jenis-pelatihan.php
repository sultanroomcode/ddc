<?php 
$url = '/umum/utilitas/tambah-master-jenis-pelatihan';
if(isset($data['tipe'])){
    $url = '/umum/utilitas/tambah-master-jenis-pelatihan?tipe='.$data['tipe'];
}
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Master Jenis pelatihan</h2>
            <div class="p-10">
            	<a href="javascript:void(0)" onclick="goLoad({url: '<?=$url?>'})" class="show-pop btn btn-danger" title="Tambah Jenis Pelatihan Baru" data-animation="pop" data-content="<p>Menambahkan Jenis Pelatihan baru. Yang akan dipilih user desa.</p>"><i class="fa fa-plus"></i></a>
				<!-- <a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-normalize-count'})" class="btn btn-danger" title="Normalisasi Hitungan"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-list-ol"></i></a> -->

				<div class="block-area" id="tableStriped">
				    <h3 class="block-title">Aktifitas Upload</h3>
				    <div class="table-responsive overflow">
				    	<table id="ta-master-jabatan" class="tile table table-bordered table-striped" cellspacing="0">
						    <thead>
						        <tr>
						            <th>Urutan</th>
						            <th>ID</th>
						            <th>Nama Pelatihan</th>
						            <th>Tipe</th>
						            <th>Deskripsi</th>
						            <th>Status</th>
						            <th>Action</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($model->all() as $v): ?>
						        <tr>
						            <td><?= $v->urut ?></td>
						            <td><?= $v->id ?></td>
						            <td><?= $v->nama_pelatihan ?></td>
						            <td><?= (isset($v->tipejabatan))?$v->tipejabatan->nama_tipe_jabatan:'' ?></td>
						            <td><?= $v->description ?></td>
						            <td><?= $v->status ?></td>
						            <td>
					            		<a href="javascript:void(0)" onclick="goLoad({url: '/umum/utilitas/update-master-jenis-pelatihan?id=<?=$v->id?>&tipe=<?=$v->tipe?>'})" title="Edit Jabatan" class="label label-info"><i class="fa fa-pencil"></i></a>
					            		
						            </td>
						        </tr>
						        <?php endforeach; ?>
						    </tbody>
						</table>
				    </div>
				</div><br>.
			</div>
	    </div>
    	<div class="clearfix"></div>
    </div>
</div>
<?php
$scripts =<<<JS
	$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
	$('#ta-master-jabatan').DataTable();
JS;

$this->registerJs($scripts);