<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use sp3d\models\transaksi\Sp3dMasterJabatanTipe;
$arr = ArrayHelper::map(Sp3dMasterJabatanTipe::find()->all(), 'id','nama_tipe_jabatan');
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Form Master Jenis Pelatihan</h2>
            <div class="p-10">

                <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
                <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'nama_pelatihan')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'tipe')->dropdownList($arr) ?>
                <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
                <?= $form->field($model, 'urut')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'status')->dropdownList([10 => 'Aktif', 0 => 'Non-Aktif']) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$urlback = '/umum/utilitas/master-jenis-pelatihan?tipe='.$model->tipe;
$script = <<<JS
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            goLoad({url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);