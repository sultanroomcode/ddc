<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Import Data</h2>
            <div class="p-10">
            	<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-import-drop'})" class="show-pop btn btn-danger" title="Upload Database Baru" data-animation="pop" data-content="<p>Menambahkan Database MDE baru. perhatikan tahun Anggaran pada file Database yang akan Anda upload.</p>">Upload Data</a>
				<!-- <a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-normalize-count'})" class="btn btn-danger" title="Normalisasi Hitungan"><i class="fa fa-spin fa-refresh"></i> <i class="fa fa-list-ol"></i></a> -->

				<div class="block-area" id="tableStriped">
				    <div class="table-responsive overflow">
				    	<table id="ta-mdb-list" class="tile table table-bordered table-striped" cellspacing="0">
						    <thead>
						        <tr>
						            <th>ID</th>
						            <th>Tahun</th>
						            <th>Tanggal Upload</th>
						            <th>Nama File</th>
						            <th>Kirim</th>
						            <th>Kalkulasi</th>
						            <th>Action</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($model->all() as $v): ?>
						        <tr>
						            <td><?= $v->id ?></td>
						            <td><?= $v->tahun ?></td>
						            <td><?= $v->date_upload ?></td>
						            <td><?= $v->nama_file ?></td>
						            <td><?= ($v->f_ftp_send == 'N')?'<i class="fa fa-remove"></i>':'<i class="fa fa-check"></i><sub>('.$v->date_send.')</sub>' ?></td>
						            <td><?= ($v->f_ftp_back == 'N')?'<i class="fa fa-remove"></i>':'<i class="fa fa-check"></i><sub>('.$v->date_back.')</sub>'?></td>
						            <td>
						            	<?php
						            	if($v->f_ftp_send == 'N' && $v->nama_file != '-'){
						            		?>
						            		<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-send-curl?id=<?=$v->id?>'})" title="Kirim Data" class="label label-info">Kirim <i class="fa fa-send"></i></a>
						            		
						            		<?php
						            	} else if($v->f_ftp_send == 'N' && $v->nama_file == '-') {
						            		?>
						            		<a href="javascript:void(0)" onclick="confirmationDelete('<?=$v->id?>')" title="Hapus" class="show-pop label label-warning" data-animation="pop" data-content="<p>Akan Menghapus Data</p>"><i class="fa fa-trash"></i></a>
						            		<?php
						            	} else if($v->f_ftp_send == 'Y' && $v->f_ftp_back == 'N') {
						            		?>
						            		<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-check?user=<?=$v->user?>&id=<?=$v->id?>&counting=1'})" title="Kalkulasi Data" class="show-pop label label-primary" title="Ambil,  Verifikasi dan Kalkulasi Data" data-animation="pop" data-content="<p>Tindakan ini akan memparsing file database mde dan akan menghitung data dari desa sampai propinsi</p>">Kalkulasi <i class="fa fa-eye"></i></a>
						            		<?php
						            	} else if($v->f_ftp_send == 'Y' && $v->f_ftp_back == 'Y') {
						            		?>
						            		<a href="javascript:void(0)" onclick="confirmationRevert('<?=$v->id?>')" title="Hapus" class="show-pop label label-danger" data-animation="pop" data-content="<p>Akan Menghapus Seluruh Data yang telah di parsing</p>">Hapus <i class="fa fa-trash"></i></a>
						            		
						            		<a href="javascript:void(0)" onclick="confirmationCounting('<?=$v->tahun?>')" title="Hapus" class="show-pop label label-info" data-animation="pop" data-content="<p>Tindakan ini akan menghitung ulang nominal data</p>">Kalkulasi <i class="fa fa-spin fa-refresh"></i></a>
						            		<?php
						            	} 
						            	?>
						            </td>
						        </tr>
						        <?php endforeach; ?>
						    </tbody>
						</table>
				    </div>
				</div><br>
				<b>Nb</b> : Untuk penghitungan mohon untuk mengikuti proses import SisKeuDes terlebih dahulu<br>
			</div>
	    </div>
    	<div class="clearfix"></div>
    </div>
</div>
<?php
$scripts =<<<JS
	$('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
	$('#ta-mdb-list').DataTable();

	function confirmationDelete(id){
		$.confirm({
		    title: 'Hapus Data Ini?',
		    content: 'Tindakan ini akan menghapus data yang kemungkinan dapat menimbulkan error',
		    autoClose: 'batal|10000',
		    buttons: {
		        deleteUser: {
		            text: 'Hapus Data',
		            action: function () {
		                goLoad({url: '/umum/import/mdb-delete?id='+id});
		            }
		        },
		        batal: function () {
		            $.alert('Tindakan dibatalkan');
		        }
		    }
		});
	}

	function confirmationRevert(id){
		$.confirm({
		    title: 'Hapus Seluruh Data?',
		    content: 'Tindakan ini akan menghapus seluruh data, termasuk database mde dan akan menghitung ulang',
		    autoClose: 'batal|10000',
		    buttons: {
		        deleteUser: {
		            text: 'Hapus Seluruh Data',
		            action: function () {
		                //$.alert('Data telah di revert!');
		                goLoad({url: '/umum/import/mdb-run-revert?id='+id});
		            }
		        },
		        batal: function () {
		            $.alert('Tindakan dibatalkan');
		        }
		    }
		});
	}

	function confirmationCounting(tahun){
		$.confirm({
		    title: 'Hitung Ulang Nominal Data?',
		    content: 'Tindakan ini akan menghitung ulang nominal data',
		    autoClose: 'batal|10000',
		    buttons: {
		        deleteUser: {
		            text: 'hitung ulang',
		            action: function () {
		                $.alert('Telah Dihitung Ulang!');
		                goLoad({url: '/umum/import/mdb-normalize-count?tahun='+tahun});
		            }
		        },
		        batal: function() {
		            $.alert('Tindakan dibatalkan');
		        }
		    }
		});
	}
JS;

$this->registerJs($scripts);