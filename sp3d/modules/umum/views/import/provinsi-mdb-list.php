<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$arr = ['-' => ''];
$kabArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');

$kabArr = $arr + $kabArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Management MDB</h2>
            <div class="p-10">
                <div class="row">
                    <div class="col-md-3">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($kabArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select id="kecamatan" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select id="desa" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button class="btn btn-block btn-danger" onclick="filteringAdd()">Filter</button>
                    </div>
                </div>
                <br>
                <table id="mdb-management-list" class="tile table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Tahun</th>
                            <th>Send</th>
                            <th>Back</th>
                            <th>Tanggal Upload</th>
                            <th>Tanggal Send</th>
                            <th>Tanggal Back</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div id="modal-custom" style="display: none;">
    <div style="width: 900px; background: rgba(0,0,0,0.8); border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
    </div>
</div>
<?php
$scripts =<<<JS

$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/pendamping/data/data-option?type=kecamatan&kode='+kode});
  }
});


$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/pendamping/data/data-option?type=desa&kode='+kode});
  }
});

    $('#mdb-management-list').DataTable({
        "columns": [
            { "data": "user", 'orderable': true},
            { "data": "tahun",'searchable':false, 'orderable': true },
            { "data": "f_ftp_send",'searchable':false, 'orderable': true },
            { "data": "f_ftp_back",'searchable':false, 'orderable': true },
            { "data": "date_upload",'searchable':false, 'orderable': true },
            { "data": "date_send",'searchable':false, 'orderable': true },
            { "data": "date_back",'searchable':false, 'orderable': true },
            { "data": "action", 'searchable' : false, 'orderable': false }
        ],
        searchDelay: 1500,
        ajax: {
            url: base_url+'/umum/import/mdb-data',
            data : function(d){
                //https://medium.com/code-kings/datatables-js-how-to-update-your-data-object-for-ajax-json-data-retrieval-c1ac832d7aa5
                d.kabupaten = $('#kabupaten').val();
                d.kecamatan = $('#kecamatan').val();
                d.desa = $('#desa').val();
            }
        },
        processing:true,
        serverSide:true
    });

    function filteringAdd(){
        $('#mdb-management-list').DataTable().ajax.reload();
        return false;
    }

    function confirmReset(id){
        $.confirm({
            title: 'Reset Password '+id+' ?',
            content: 'Apakah Anda akan me-reset password pengguna',
            autoClose: 'cancelAction|10000',
            type: 'red',
            buttons: {
                deleteUser: {
                    text: 'Reset Password',
                    action: function () {
                        //then
                        $.get(base_url+'/umum/user/reset-password?id='+id, function(data){
                            if(data == 1){
                                $.alert('Password Sudah Direset!');
                            } else {
                                $.alert('Gagal Dalam Mereset Password!');
                            }
                        });
                    }
                },
                cancelAction:{
                    text:'Batalkan',
                    action:function () {
                        $.alert('Reset Dibatalkan');
                    }
                }
            }
        });
    }

    var modalxy = new Custombox.modal({
      content: {
        effect: 'slidetogether',
        id:'form-xy',
        target: '#modal-custom',
        positionY: 'top',
        positionX: 'center',
        delay:2000,
        onClose: function(){
            
        }
      },
      overlay:{
        color:'#2A00FF'
      }
    });

    function openModalXy(o){
        $('#form-engine').html(' ');
        $('#form-engine').load(base_url+o.url);
        modalxy.open();    
    }
JS;

$this->registerJs($scripts);