<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Import Via Excell</h2>
            <div class="p-10">
            	<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/excell-import'})" class="btn btn-danger"><i class="fa fa-cloud-upload"></i> <i class="fa fa-file-excel-o"></i></a>
				<table id="ta-mdb-list" class="compact" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Tanggal Upload</th>
				            <th>Nama File</th>
				            <th>Status Send</th>
				            <th>Status Generate</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot>
				        <tr>
				            <th>Tanggal Upload</th>
				            <th>Nama File</th>
				            <th>Status Send</th>
				            <th>Status Generate</th>
				            <th>Action</th>
				        </tr>
				    </tfoot>
				    <tbody>
				    	<?php foreach ($model->all() as $v): ?>
				        <tr>
				            <td><?= $v->date_upload ?></td>
				            <td><?= $v->nama_file ?></td>
				            <td><?= ($v->f_ftp_send == 'N')?'<i class="fa fa-remove"></i>':'<i class="fa fa-check"></i><sub>('.$v->date_send.')</sub>' ?></td>
				            <td><?= ($v->f_ftp_back == 'N')?'<i class="fa fa-remove"></i>':'<i class="fa fa-check"></i><sub>('.$v->date_back.')</sub>'?></td>
				            <td>
				            	<?php
				            	if($v->f_ftp_send == 'N'){
				            		?>
				            		<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-send?id=<?=$v->id?>'})" title="Kirim untuk diparsing" class="label label-info"><i class="fa fa-send"></i></a>
				            		<?php
				            	} else if($v->f_ftp_send == 'Y' && $v->f_ftp_back == 'N') {
				            		?>
				            		<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-check?id=<?=$v->id?>'})" title="Cek data parsing" class="label label-primary"><i class="fa fa-eye"></i></a>
				            		<?php
				            	} else if($v->f_ftp_send == 'Y' && $v->f_ftp_back == 'Y') {
				            		?>
				            		<a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-run?id=<?=$v->id?>'})" title="Jalankan" class="label label-primary"><i class="fa fa-gear"></i></a>
				            		<?php
				            	}
				            	?>
				            </td>
				        </tr>
				        <?php endforeach; ?>
				    </tbody>
				</table>

            </div>  
        </div>

        <!--  Recent Postings -->
        <div class="row">
            <div class="col-md-6"></div>
            <!-- Tasks to do -->
            <div class="col-md-6"></div>
        </div>
        <div class="clearfix"></div>
        <!-- Dynamic Chart -->
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
	$('#ta-mdb-list').DataTable();
JS;

$this->registerJs($scripts);