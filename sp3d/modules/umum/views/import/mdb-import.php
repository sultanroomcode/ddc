<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\TaMdbUpload */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
	$model->id = time();
	$model->user = Yii::$app->user->identity->id;
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Import Via MDE - Form</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

                <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true])->label(false) ?>

                <?= $form->field($model, 'nama_file_front')->fileInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'user')->hiddenInput(['maxlength' => true])->label(false) ?>

                <progress id="prog" max="100" value="0" style="display:none;"></progress>
            	<div id="percent"></div>

            	<div class="progress">
            	  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><b class="prog-text"></b>
            	    <span class="sr-only" class="prog-text"></span>
            	  </div>
            	</div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-cloud-upload"></i>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-list'})" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

        <!--  Recent Postings -->
        <div class="row">
            <div class="col-md-6"></div>
            <!-- Tasks to do -->
            <div class="col-md-6"></div>
        </div>
        <div class="clearfix"></div>
        <!-- Dynamic Chart -->
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
var bar = $('.progress-bar');
var percent = $('.prog-text');
   
$('form#{$model->formName()}').ajaxForm({
    beforeSend: function() {
        var percentVal = 0;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : '0%'}); 
        percent.html(percentVal+' %');
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    success: function() {
        var percentVal = 100;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
	complete: function(xhr) {
        if(xhr.responseText == 1){
            $(this).trigger('reset');
            goLoad({url:'/umum/import/mdb-list'});
        } else {
            
        }
	}
}); 
JS;

$this->registerJs($script);