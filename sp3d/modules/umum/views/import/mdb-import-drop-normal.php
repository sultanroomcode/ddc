<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\models\transaksi\TaMdbUpload */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
	$model->id = time();
    $model->user = Yii::$app->user->identity->id;
	// $model->nama_file = Yii::$app->user->identity->id;
}
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Import Data</h2>
            <div class="p-10">
                <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

                <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true])->label(false) ?>

                <?= $form->field($model, 'nama_file_front')->fileInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'tahun')->dropdownList(['2016' => '2016', '2017' => '2017', '2018' => '2018', '2019' => '2019']) ?>

                <?= $form->field($model, 'user')->hiddenInput(['maxlength' => true])->label(false) ?>

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-gr-gray btn-sm">
                        <input id="tamdbupload-f_aggree" name="TaMdbUpload[f_aggree]" value="Y" type="checkbox" /> Verifikasi
                    </label>
                </div>

                <progress id="prog" max="100" value="0" style="display:none;"></progress>
            	<div id="percent"></div>

            	<div class="progress">
            	  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><b class="prog-text"></b>
            	    <span class="sr-only" class="prog-text"></span>
            	  </div>
            	</div>

                <div class="form-group">
                    <button type="submit" name="sending" id="sending" class="btn btn-success show-pop" data-animation="pop" data-content="<p>Dengan menekan tombol Verifikasi dan tombol upload ini<br>maka Anda setuju bahwasannya Anda telah memeriksa file mde.</p>" title="Upload Database Baru">Proses Upload <i class="fa fa-cloud-upload"></i></button>

                    <a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-list'})" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$script = <<<JS
JS;

$this->registerJs($script);