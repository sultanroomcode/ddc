<?php
$directoryIcon = '../css/SuperAdmin-1-0-3/Template/1-0-3/';
?>
<div class="block-area animated slideInRight">
    <div class="row" >
        <div class="col-md-10 col-md-offset-1" style="background: url(../img/synopsis.png); background-size:1079px 512px; ">
            <div class="row">
                <div class="col-md-5" style="padding-top: 250px;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2859791.9579254766!2d111.0651107711698!3d-8.161299850613279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2da393f79feeb5c5%3A0x1030bfbca7cb850!2sJawa+Timur!5e0!3m2!1sid!2sid!4v1553532224487" width="430" height="245" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <div class="col-md-7" style="padding: 19px;font-size: 13px;">
                    <p><h4>Provinsi Jawa Timur</h4> terletak di bagian timur Pulau Jawa.  Ibu kotanya di Surabaya. Luas wilayahnya 47.922 km². Penduduknya 42.030.633 jiwa (sensus 2017). Jawa Timur memiliki wilayah terluas di antara 6 provinsi di Pulau Jawa. Wilayah Jawa Timur juga memiliki Pulau Madura, Pulau Bawean, Pulau Kangean serta sejumlah pulau-pulau kecil di Laut Jawa (Kepulauan Masalembu), dan Samudera Hindia (Pulau Sempu, dan Nusa Barung). Gubernur pertama Jawa Timur adalah R. Soerjo, yang juga dikenal sebagai pahlawan nasional. Sekarang (2009- 12 Pebruari 2019/ dua periode) dipimpin oleh Pakde Karwo (Dr.Soekarwo) sebagai Gubernur dan Gus Ipul (Saifullah Yusuf) sebagai Wakil Gubernur. Mayoritas penduduk Jawa Timur adalah Suku Jawa, namun demikian, etnisitas di Jawa Timur lebih heterogen, hampir semua suku ada di Jawa Timur. 
                    Dialek Bahasa Jawa Timur dikenal dengan Bahasa Jawa Timuran, dialek Surabaya dikenal dengan Boso Suroboyoan.
                    <br>
                    <b>Jawa Timur</b> dikenal sebagai pusat Kawasan Timur Indonesia, dan memiliki signifikansi perekonomian yang cukup tinggi, yakni berkontribusi 14,85% terhadap Produk Domestik Bruto nasional. Pariwisata di <b>Jawa Timur</b> terkenal dengan Wisata Gunung Bromo, Malang dan Batu Kota wisata, Wisata Budaya Madura, Tretes dan Trawas di Mojokerto, Kebun Raya Purwodadi dan Wisata Candi-Candi tinggalan kerajaaan masa lalu. Wisata bahari Lamongan, Pantai Plengkung dan Kawah Ijen di Banyuwangi, Pantai Watu Ulo di Jember, Bendungan Selorejo di Blitar, Pantai Prigi, Pantai Pelang, dan Pantai Pasir Putih di Trenggalek, Pantai Popoh di Tulungagung, Pantai Ngliyep dan Goa China di Malang dan Pantai Kenjeran di Surabaya. Wisata Religi : sejumlah makam para wali, Sunan Ampel di Surabaya, Sunan Giri, dan Sunan Gresik di Gresik, Sunan Drajat di Paciran (Lamongan), dan Sunan Bonang di Tuban. Goa-goa menarik, yaitu: Gua Maharani di Lamongan, dan Gua Akbar di Tuban, serta Goa Gong yang berada di Kabupaten Pacitan yang terkenal sebagai gua terindah di Asia Tenggara. Surabaya merupakan pusat pemerintahan, dan pusat bisnis <b>Jawa Timur</b>, di mana terdapat Tugu Pahlawan, Museum Mpu Tantular, Kebun Binatang Surabaya, Monumen Kapal Selam, Kawasan Ampel, dan Kawasan Tunjungan.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="block-area animated slideInRight">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="tile-light p-5 m-b-15">                
                <div class="listview icon-list">
                    <?php if(Yii::$app->user->identity->type == 'desa'): ?>
                    <div class="media">
                        <i class="icon pull-left">&#61744</i>
                        <div class="media-body" id="info-detail-profile-desa">Desa <?= Yii::$app->user->identity->id?></div>
                    </div>
                    <?php endif; ?>
                    
                    <?php if(Yii::$app->user->identity->type == 'desa' || Yii::$app->user->identity->type == 'kecamatan'): ?>
                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body" id="info-detail-profile-kecamatan">Kecamatan <?= substr(Yii::$app->user->identity->id, 0, 7)?></div>
                    </div>
                    <?php endif; ?>

                    <?php if(Yii::$app->user->identity->type == 'desa' || Yii::$app->user->identity->type == 'kecamatan' || Yii::$app->user->identity->type == 'kabupaten'): ?>
                    <div class="media">
                        <i class="icon pull-left">&#61713</i>
                        <div class="media-body" id="info-detail-profile-kabupaten">Kabupaten <?= substr(Yii::$app->user->identity->id, 0, 4)?></div>
                    </div>
                    <?php endif; ?>
                    <div class="media">
                        <i class="icon pull-left">&#61742</i>
                        <div class="media-body" id="info-detail-profile-provinsi"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<?php 
$user = Yii::$app->user->identity->id;
$script = <<<JS
kd = {$user};

function getFrontDetailInfo(user)
{
    $.getJSON(base_url+'/umum/data-private/data-info-detail?user='+user, function(res) {
        if(res != ''){
            if(res.nama_desa != ''){ $('#info-detail-profile-desa').html(res.nama_desa); }
            if(res.nama_kecamatan != ''){ $('#info-detail-profile-kecamatan').html(res.nama_kecamatan); }
            if(res.nama_kabupaten != ''){ $('#info-detail-profile-kabupaten').html(res.nama_kabupaten); }
            if(res.nama_provinsi != ''){ $('#info-detail-profile-provinsi').html(res.nama_provinsi); }
        }
    });   
}
getFrontDetailInfo(kd);
JS;

$this->registerJs($script);