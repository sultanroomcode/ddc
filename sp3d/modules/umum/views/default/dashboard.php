<?php
$directoryIcon = '../css/SuperAdmin-1-0-3/Template/1-0-3/';
?>
<h4 class="page-title" id="page-title"><?= Yii::$app->user->identity->id ?> PROFILE</h4>
                
<div class="block-area animated slideInRight">
    <div class="row">
        <div class="col-md-9">
            <div class="tile-light p-5 m-b-15">
                <div class="cover p-relative">
                    <img src="<?= $directoryIcon ?>img/cover-bg.jpg" class="w-100" alt="">
                    <img class="profile-pic" src="<?= $directoryIcon ?>img/profile-pic.png" alt="">
                    <div class="profile-btn">
                        <button class="btn btn-alt btn-sm" onclick="goLoad({url:'/umum/user/profile-setting'})"><i class="icon-user-2"></i> <span>Profile Setting</span></button>
                        <button class="btn btn-alt btn-sm" onclick="goLoad({url:'/umum/user/ganti-password'})"><i class="icon-user-2"></i> <span>Password</span></button>
                    </div>
                </div>
                <div class="p-5 m-t-15" id="profile-deskripsi">
                    Deskripsi
                </div>
            </div>            
        </div>
        
        <div class="col-md-3">
            <!-- About Me -->
            <div class="tile">
                <h2 class="tile-title">Tentang</h2>
                <div class="tile-config dropdown">
                    <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
                    <ul class="dropdown-menu pull-right text-right"> 
                        <li><a href="javascript:void(0)" onclick="goLoad({url:'/umum/user/edit-profile'})">Edit</a></li>
                    </ul>
                </div>
                
                <div class="listview icon-list">
                    <?php if(Yii::$app->user->identity->type == 'desa'): ?>
                    <div class="media">
                        <i class="icon pull-left">&#61744</i>
                        <div class="media-body" id="info-detail-profile-siskeudes">Kode Siskeudes : <?= Yii::$app->user->identity->id?></div>
                    </div>
                    <div class="media">
                        <i class="icon pull-left">&#61744</i>
                        <div class="media-body" id="info-detail-profile-desa">Desa <?= Yii::$app->user->identity->id?></div>
                    </div>
                    <?php endif; ?>
                    
                    <?php if(Yii::$app->user->identity->type == 'desa' || Yii::$app->user->identity->type == 'kecamatan'): ?>
                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body" id="info-detail-profile-kecamatan">Kecamatan <?= substr(Yii::$app->user->identity->id, 0, 7)?></div>
                    </div>
                    <?php endif; ?>

                    <?php if(Yii::$app->user->identity->type == 'desa' || Yii::$app->user->identity->type == 'kecamatan' || Yii::$app->user->identity->type == 'kabupaten'): ?>
                    <div class="media">
                        <i class="icon pull-left">&#61713</i>
                        <div class="media-body" id="info-detail-profile-kabupaten">Kabupaten <?= substr(Yii::$app->user->identity->id, 0, 4)?></div>
                    </div>
                    <?php endif; ?>

                    <?php if(Yii::$app->user->identity->profile != null && Yii::$app->user->identity->type == 'desa'): ?>
                    <div class="media">
                        <i class="icon pull-left">&#61713</i>
                        <div class="media-body" id="info-detail-profile-rt">Jumlah RT : <?=Yii::$app->user->identity->profile->jml_rt ?></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61713</i>
                        <div class="media-body" id="info-detail-profile-rt">Jumlah RW : <?=Yii::$app->user->identity->profile->jml_rw ?></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61713</i>
                        <div class="media-body" id="info-detail-profile-rt">Email : <?=Yii::$app->user->identity->profile->email ?></div>
                    </div>
                    <?php endif; ?>


                    
                    <div class="media">
                        <i class="icon pull-left">&#61742</i>
                        <div class="media-body">Provinsi Jawa Timur</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <div class="tile">
                <h2 class="tile-title">Data Isian 2018</h2>
                <!-- <div class="tile-config dropdown">
                    <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
                    <ul class="dropdown-menu pull-right text-right"> 
                        <li><a href="javascript:void(0)">Edit</a></li>
                        <li><a href="javascript:void(0)">Delete</a></li>
                    </ul>
                </div> -->
                
                <div class="listview icon-list">
                    
                    <div class="media">
                        <a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa-page?tahun=2018'})">
                        <i class="icon pull-left">&#61744</i>
                        <div class="media-body">Data</div>
                        </a>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Pendapatan : <span id="count-pendapatan-2018"></span></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Belanja : <span id="count-belanja-2018"></span></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Pembiayaan : <span id="count-pembiayaan-2018"></span></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="tile">
                <h2 class="tile-title">Data Isian 2017</h2>
                <!-- <div class="tile-config dropdown">
                    <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
                    <ul class="dropdown-menu pull-right text-right"> 
                        <li><a href="javascript:void(0)">Edit</a></li>
                        <li><a href="javascript:void(0)">Delete</a></li>
                    </ul>
                </div> -->
                
                <div class="listview icon-list">
                    <div class="media">
                        <a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa-page?tahun=2017'})">
                        <i class="icon pull-left">&#61744</i>
                        <div class="media-body">Data</div>
                        </a>
                    </div>
                    
                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Pendapatan : <span id="count-pendapatan-2017"></span></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Belanja : <span id="count-belanja-2017"></span></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Pembiayaan : <span id="count-pembiayaan-2017"></span></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="tile">
                <h2 class="tile-title">Data Isian 2016</h2>
                <!-- <div class="tile-config dropdown">
                    <a data-toggle="dropdown" href="javascript:void(0)" class="tooltips tile-menu" title="" data-original-title="Options"></a>
                    <ul class="dropdown-menu pull-right text-right"> 
                        <li><a href="javascript:void(0)">Edit</a></li>
                        <li><a href="javascript:void(0)">Delete</a></li>
                    </ul>
                </div> -->
                
                <div class="listview icon-list">
                    <div class="media">
                        <a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa-page?tahun=2016'})">
                        <i class="icon pull-left">&#61744</i>
                        <div class="media-body">Data</div>
                        </a>
                    </div>
                    
                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Pendapatan : <span id="count-pendapatan-2016"></span></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Belanja : <span id="count-belanja-2016"></span></div>
                    </div>

                    <div class="media">
                        <i class="icon pull-left">&#61753</i>
                        <div class="media-body">Pembiayaan : <span id="count-pembiayaan-2016"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
$user = Yii::$app->user->identity->id;
$script = <<<JS
kd = {$user};
$.getJSON(base_url+'/umum/user/profile', function(res) {
    if(res != ''){
        $('.profile-pic').attr('src', res.foto_profile);
        $('#profile-deskripsi').html(res.deskripsi);
    }
});

function getDataPrivate(tahun, kd)
{
    $.getJSON(base_url+'/umum/data-private/data-count-access?tahun='+tahun+'&kd='+kd, function(res) {
        if(res != ''){
            $('#count-pendapatan-'+tahun).html(res.pendapatan);
            $('#count-belanja-'+tahun).html(res.belanja);
            $('#count-pembiayaan-'+tahun).html(res.pembiayaan);
        }
    });   
}

function getDetailInfo(user)
{
    $.getJSON(base_url+'/umum/data-private/data-info-detail?user='+user, function(res) {
        if(res != ''){
            if(res.kd_siskeudes == ''){ $.alert('Mohon isi kode siskeudes dulu');goLoad({url:'/umum/user/setting'}); } else { $('#info-detail-profile-siskeudes').html(res.kd_siskeudes); }
            if(res.nama_desa != ''){ $('#info-detail-profile-desa').html(res.nama_desa); }
            if(res.nama_kecamatan != ''){ $('#info-detail-profile-kecamatan').html(res.nama_kecamatan); }
            if(res.nama_kabupaten != ''){ $('#info-detail-profile-kabupaten').html(res.nama_kabupaten); }
            if(res.nama_provinsi != ''){ $('#info-detail-profile-provinsi').html(res.nama_provinsi); }
        }
    });   
}

getDataPrivate('2016', kd);
getDataPrivate('2017', kd);
getDataPrivate('2018', kd);
getDetailInfo(kd);
JS;

$this->registerJs($script);