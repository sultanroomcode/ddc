<?php
use sp3d\models\TaMdbUpload;
// $formater = new TaDesa();
$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT DISTINCT substr(tmu.user, 1, 7) AS kode_kec, kec.description AS kec_desc, substr(tmu.user, 1, 4) AS kode_kab, kab.description AS kab_desc FROM ta_mdb_upload tmu LEFT JOIN user kab ON kab.id = substr(tmu.user, 1, 4) LEFT JOIN user kec ON kec.id = substr(tmu.user, 1, 7) WHERE tahun = '$tahun'");
$result = $model->queryAll();
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Kecamatan Upload</h2>
            <div class="p-10">
            	<b>Detail Kecamatan Yang Telah Mengupload</b>
				<table class="table" border="2" style="border-collapse: collapse;">
				<tr>
					<th>Kabupaten</th>
					<th>Kecamatan</th>
				</tr>
				<?php
				$c = 0;
				foreach($result as $v){
					echo "<tr><td>".$v['kab_desc']."</td><td>".$v['kec_desc']."</td></tr>";
				}
				?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>