<?php
use sp3d\models\TaMdbUpload;
// $formater = new TaDesa();
$conn = Yii::$app->db;
$sql = "SELECT 
u.description AS nama, 
-- COUNT(DISTINTC tmudesa.user) AS c_desa,
IFNULL(kc.dana_pendapatan, 0) AS pendapatan, 
IFNULL(kc.dana_rab, 0) AS belanja 
FROM user u 
LEFT JOIN kabupaten_count kc ON u.id = kc.kd_kabupaten AND kc.tahun = '$tahun' 
-- LEFT JOIN ta_mdb_upload tmudesa ON tmudesa.user LIKE CONCAT(u.id ,'%') AND tmukab.tahun = '2016' 
WHERE u.type = 'kabupaten'";
$model = $conn->createCommand($sql);
$result = $model->queryAll();
//harus menambhakan kolom kabupaten dan kecamatan di mdb_upload
?>
<div class="row animated slideInRight">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Desa Upload</h2>
            <div class="p-10">
            	<b>Detail Rekap APBDesa per Kabupaten Yang Telah Mengupload</b>
				<table class="table" border="2" style="border-collapse: collapse;">
				<tr>
					<th>Kabupaten</th>
					<!-- <th>Kecamatan</th>
					<th>Desa</th> -->
					<th>Pendapatan</th>
					<th>Belanja</th>
				</tr>
				<?php
				$c = 0;
				// echo $sql;
				foreach($result as $v){
					echo "<tr><td>".$v['nama']."</td><td>".$v['pendapatan']."</td><td>".$v['belanja']."</td></tr>";
				}
				?>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>