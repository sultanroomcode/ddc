<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Laporan <?= $tahun ?></h2>
            <div class="p-10">--</div>  
        </div>

        <!--  Recent Postings -->
        <div class="row">
            <div class="col-md-6"></div>
            <!-- Tasks to do -->
            <div class="col-md-6"></div>
        </div>
        <div class="clearfix"></div>
        <!-- Dynamic Chart -->
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scriptJs = <<<JS
    $('.show-pop').webuiPopover({trigger:'hover', style:'inverse'});
JS;

$this->registerJs($scriptJs);