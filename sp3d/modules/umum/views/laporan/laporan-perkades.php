<?php
use frontend\models\transaksi\TaDesa;
$formater = new TaDesa();
$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT `t`.`Kd_Rincian`, `rrp`.`uraian`,  `t`.`Anggaran` FROM (
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB` WHERE `kegiatan` = 'pendapatan' AND `Kd_Desa` = '$kd'
UNION
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB_Ext` WHERE `kegiatan` = 'pendapatan' AND `Kd_Desa` = '$kd'
) AS `t` LEFT JOIN `ref_rekening_pendapatan` `rrp` ON `rrp`.`kode` = `t`.`Kd_Rincian` COLLATE utf8_unicode_ci ORDER BY `Kd_Rincian` ASC");
$result = $model->queryAll();

$model = $conn->createCommand("SELECT `t`.`Kd_Rincian`, `rrp`.`uraian`,  `t`.`Anggaran` FROM (
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB` WHERE `kegiatan` = 'belanja' AND `Kd_Desa` = '$kd'
UNION
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB_Ext` WHERE `kegiatan` = 'belanja' AND `Kd_Desa` = '$kd'
) AS `t` LEFT JOIN `ref_rekening_kegiatan` `rrp` ON `rrp`.`kode` = `t`.`Kd_Rincian` COLLATE utf8_unicode_ci ORDER BY `Kd_Rincian` ASC");
$resultbel = $model->queryAll();

$model = $conn->createCommand("SELECT `t`.`Kd_Rincian`, `rrp`.`uraian`,  `t`.`Anggaran` FROM (
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB` WHERE `kegiatan` = 'belanja' AND `Kd_Desa` = '$kd'
UNION
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB_Ext` WHERE `kegiatan` = 'belanja' AND `Kd_Desa` = '$kd'
) AS `t` LEFT JOIN `ref_rekening_kegiatan` `rrp` ON `rrp`.`kode` = `t`.`Kd_Rincian` COLLATE utf8_unicode_ci ORDER BY `Kd_Rincian` ASC");
$resultbelperkades = $model->queryAll();

$model = $conn->createCommand("SELECT `t`.`Kd_Rincian`, `rrp`.`uraian`,  `t`.`Anggaran` FROM (
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB` WHERE (`kegiatan` = 'penerimaan' OR `kegiatan` = 'pengeluaran') AND `Kd_Desa` = '$kd'
UNION
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB_Ext` WHERE (`kegiatan` = 'penerimaan' OR `kegiatan` = 'pengeluaran') AND `Kd_Desa` = '$kd'
) AS `t` LEFT JOIN `ref_rekening_pembiayaan` `rrp` ON `rrp`.`kode` = `t`.`Kd_Rincian` COLLATE utf8_unicode_ci ORDER BY `Kd_Rincian` ASC");
$resultbiaya = $model->queryAll();
?>
<a href="javascript:void(0)" onclick="goLoad({url:'/umum/laporan-test/laporan?page=perkades&kd=<?=$kd?>'})" class="btn btn-xs btn-info"><i class="fa fa-spin fa-refresh"></i></a>
<?php
$model = $conn->createCommand("SELECT SUM(`Anggaran`) AS Ang, `sumberdana` FROM `Ta_RAB` WHERE `kegiatan` = 'pendapatan' GROUP BY `sumberdana`");
$resultan = $model->queryAll();
?>
<h2>PERKADES</h2>
<b>Pendapatan</b>
<table border="1" style="border-collapse: collapse;">
<?php
foreach($resultan as $v){
	echo "<tr><td>".$v['sumberdana']."</td><td align=\"right\">".$formater->nf($v['Ang'])."</td></tr>";
}
?>
</table>
<hr>
<b>Contoh Laporan</b>
<table border="2" style="border-collapse: collapse;">
<tr>
	<th>Kode Rekening</th>
	<th>Uraian</th>
	<th>Anggaran</th>
	<th>Keterangan</th>
</tr>
<tr>
	<th>1</th>
	<th>PENDAPATAN</th>
	<th></th>
	<th></th>
</tr>
<?php
foreach($result as $v){
	echo "<tr><td>".$v['Kd_Rincian']."</td><td>".$v['uraian']."</td><td align=\"right\">".$formater->nf($v['Anggaran'])."</td><td></td></tr>";
}
?>
<tr>
	<th height="10" colspan="4" bgcolor="#001"></th>
</tr>
<tr>
	<th>2.</th>
	<th>BELANJA</th>
	<th></th>
	<th></th>
</tr>
<?php
foreach($resultbel as $v){
	echo "<tr><td>".$v['Kd_Rincian']."</td><td>".$v['uraian']."</td><td align=\"right\">".$formater->nf($v['Anggaran'])."</td><td></td></tr>";
}
?>
<tr>
	<td colspan="2" align="center">JUMLAH BELANJA</td>
	<td>TOTAL</td>
	<td></td>
</tr>
<tr>
	<td colspan="2" align="center">SURPLUS / DEFISIT</td>
	<td>TOTAL</td>
	<td></td>
</tr>
<tr>
	<th height="10" colspan="4" bgcolor="#001"></th>
</tr>
<tr>
	<th>3.</th>
	<th>PEMBIAYAAN</th>
	<th></th>
	<th></th>
</tr>
<?php
foreach($resultbiaya as $v){
	echo "<tr><td>".$v['Kd_Rincian']."</td><td>".$v['uraian']."</td><td align=\"right\">".$formater->nf($v['Anggaran'])."</td><td></td></tr>";
}
?>
<tr>
	<td colspan="2">Pembiayaan Netto (PENERIMAAN PEMBIAYAAN)</td>
	<td>TOTAL</td>
	<td></td>
</tr>
<tr>
	<td colspan="2">SILPA tahun berjalan (SELISIH ANTARA PEMBIAYAAN NETTO DENGAN HASIL SURPLUS / DEFISIT)</td>
	<td>TOTAL</td>
	<td></td>
</tr>
</table>