<?php foreach($dataProvider->all() as $v): ?>
<div class="media p-l-5">
    <div class="pull-left">
        <img width="40" src="img/profile-pics/1.jpg" alt="">
    </div>
    <div class="media-body">
        <small class="text-muted">2 Hours ago by Adrien San</small><br/>
        <span class="t-overflow">Cras molestie fermentum nibh, ac semper</span>
    </div>
</div>
<?php endforeach; ?>
<div class="media p-5 text-center l-100">
    <small><?= time() ?></small>
</div>
