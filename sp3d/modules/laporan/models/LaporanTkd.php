<?php
namespace sp3d\modules\laporan\models;
use Yii;
use yii\base\Model;

use sp3d\modules\ddc\models\DdcTkd;

class LaporanTkd extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    potensi olah data : lokasi, luas_lahan, jenis_sertifikat, region
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        $this->pageToOpen = $this->reportType;
        switch ($this->reportType) {
            case 'tkd-lokasi':
            case 'tkd-luas-lahan':
            case 'tkd-jenis-lahan':
            case 'tkd-jenis-sertifikat':
                $this->model = DdcTkd::find();
                $this->pageToOpen = 'tkd-umum';//region dan tahun menggunakan view yang sama
            break;
            
            default: exit('no choice'); break;
        }
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        if($this->data['subjekval'] != '-' && $this->data['subjekval'] != null){
            switch($this->data['subjek']){
                case 'lokasi':
                    $this->model->andWhere(['lokasi' => $this->data['subjekval']]);
                break;
                case 'luas-lahan':
                    $exp = explode('-', $this->data['subjekval']);
                    // var_dump($exp);
                    if(count($exp) > 1){
                        $this->model->andWhere(['>=','luas_lahan', $exp[0]]);
                        $this->model->andWhere(['<=','luas_lahan', $exp[1]]);
                    } else {
                        $this->model->andWhere(['luas_lahan' => $this->data['subjekval']]);
                    }
                break;
                case 'jenis-lahan':
                    $this->model->andWhere(['jenis_lahan' => $this->data['subjekval']]);
                break;
                case 'jenis-sertifikat':
                    $this->model->andWhere(['jenis_sertifikat' => $this->data['subjekval']]);
                break;

                case 'lokasi':
                    $this->model->andWhere(['lokasi' => $this->data['subjekval']]);
                break;
            }
            ini_set('memory_limit', '512M');
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }

    public function excellAttributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            case 'lokasi':
            case 'luas-lahan':
            case 'jenis-lahan':
            case 'jenis-sertifikat':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten'],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan'],
                    ['attribute' => 'desa.description', 'label' => 'Desa'],
                    ['attribute' => 'kd_desa', 'label' => 'Desa'],
                    ['attribute' => 'no_tkd', 'label' => 'id'],
                    ['attribute' => 'jenis_sertifikat',],
                    ['attribute' => 'luas_lahan', 'value' => function($m){
                        return number_format($m->luas_lahan,3,',','.');
                    }],
                    ['attribute' => 'nomor',  'label' => 'Nomor Sertifikat'],
                    ['attribute' => 'jenis_lahan',],
                    ['attribute' => 'lokasi',],
                ];
            break;
        }

        return $arr;
    }
}
