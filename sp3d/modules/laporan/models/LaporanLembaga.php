<?php
namespace sp3d\modules\laporan\models;
use Yii;
use yii\base\Model;

use sp3d\modules\ddc\models\DdcTkd;
use sp3d\modules\pkk\models\Pkk;
use sp3d\modules\pkk\models\PkkAnggotaPembina;
use sp3d\modules\pkk\models\PkkKegiatan;
use sp3d\modules\pkk\models\PkkPengurus;
use sp3d\modules\lpmd\models\Lpmd;
use sp3d\modules\lpmd\models\LpmdAnggotaPembina;
use sp3d\modules\lpmd\models\LpmdKegiatan;
use sp3d\modules\lpmd\models\LpmdPengurus;
use sp3d\modules\kartar\models\Kartar;
use sp3d\modules\kartar\models\KartarAnggotaPembina;
use sp3d\modules\kartar\models\KartarKegiatan;
use sp3d\modules\kartar\models\KartarPengurus;
use sp3d\modules\lembagaadat\models\LembagaAdat;
use sp3d\modules\lembagaadat\models\LembagaAdatKegiatan;
use sp3d\modules\lembagaadat\models\LembagaAdatPengurus;

class LaporanLembaga extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    potensi olah data : lokasi, luas_lahan, jenis_sertifikat, region
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        $this->pageToOpen = $this->reportType;
        switch ($this->reportType) {
            case 'pkk-pembina':
                $this->model = Pkk::find();
                $this->pageToOpen = 'pkk-umum';//region dan tahun menggunakan view yang sama
            break;

            case 'lpmd-pembina':
                $this->model = Lpmd::find();
                $this->pageToOpen = 'lpmd-umum';//region dan tahun menggunakan view yang sama
            break;

            case 'kartar-pembina':
                $this->model = Kartar::find();
                $this->pageToOpen = 'kartar-umum';//region dan tahun menggunakan view yang sama
            break;
            case 'lembagaadat-sekretariat':
                $this->model = LembagaAdat::find();
                $this->pageToOpen = 'lembaga-adat-umum';//region dan tahun menggunakan view yang sama
            break;
            
            default: exit('no choice'); break;
        }
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        if($this->data['subjekval'] != '-' && $this->data['subjekval'] != null){
            switch($this->data['subjek']){
                case 'pembina':
                    $this->model->andWhere(['pembina_status_ada' => $this->data['subjekval']]);
                break;
                case 'jumlah-pembina':
                    $this->model->andWhere(['pembina_status_ada' => $this->data['subjekval']]);
                break;
                case 'sekretariat':
                    $this->model->andWhere(['pembina_status_ada' => $this->data['subjekval']]);
                break;
            }
            ini_set('memory_limit', '512M');
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }

    public function excellAttributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            case 'pembina':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten'],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan'],
                    ['attribute' => 'desa.description', 'label' => 'Desa'],
                    ['attribute' => 'ditetapkan_oleh',],
                    ['attribute' => 'sk_lembaga',],
                    ['attribute' => 'pembina_status_ada',]
                ];

                if($this->pageToOpen == 'lembaga-adat-umum'){
                    $arr = [];
                    $arr = [
                        ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten'],
                        ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan'],
                        ['attribute' => 'desa.description', 'label' => 'Desa'],
                        ['attribute' => 'nama',],
                        ['attribute' => 'jenis',],
                        ['attribute' => 'sk_no',],
                        ['attribute' => 'status_legalitas',],
                        ['attribute' => 'kesekertariatan_status_ada',],
                        ['attribute' => 'status_kesekertariatan',],
                    ];
                }
            break;
        }

        return $arr;
    }
}
