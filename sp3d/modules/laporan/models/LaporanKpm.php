<?php
namespace sp3d\modules\laporan\models;
use Yii;
use yii\base\Model;

use sp3d\modules\kpm\models\Kpm;
use sp3d\modules\kpm\models\KpmPendidikan;
use sp3d\modules\kpm\models\KpmPelatihan;
use sp3d\modules\kpm\models\KpmPekerjaan;

class LaporanKpm extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    potensi olah data : lokasi, luas_lahan, jenis_sertifikat, region
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        $this->pageToOpen = $this->reportType;
        switch ($this->reportType) {
            case 'kpm-umur':
            case 'kpm-pendidikan':
            case 'kpm-jenis-kelamin':
            case 'kpm-status':
                $this->model = Kpm::find();
                $this->pageToOpen = 'kpm-umum';//region dan tahun menggunakan view yang sama
            break;
            
            default: exit('no choice'); break;
        }
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        if($this->data['subjekval'] != '-' && $this->data['subjekval'] != null){
            switch($this->data['subjek']){
                case 'umur':
                    $this->model->select(['*', "CASE WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 20 THEN '0-20th'
                WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 30 THEN '21-30th'
                WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 50 THEN '31-50th'
                WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 70 THEN '51-70th'
                WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) > 70 THEN '71-100th' END AS subjekval"]);
                    $this->model->andFilterHaving(['subjekval' => $this->data['subjekval']]);
                break;
                case 'pendidikan':
                    $this->model->andWhere(['pendidikan' => $this->data['subjekval']]);
                break;
                case 'jenis-kelamin':
                    $this->model->andWhere(['gender' => $this->data['subjekval']]);
                break;
                case 'status':
                    $this->model->andWhere(['status_aktif' => $this->data['subjekval']]);
                break;
            }
            ini_set('memory_limit', '512M');
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }

    public function excellAttributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            case 'umur':
            case 'pendidikan':
            case 'jenis-kelamin':
            case 'status':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten'],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan'],
                    ['attribute' => 'desa.description', 'label' => 'Desa'],
                    ['attribute' => 'nama',],
                    ['attribute' => 'tempat_lahir',],
                    ['attribute' => 'tanggal_lahir', 'value' => function($m){
                        return date('d-F-Y', strtotime($m->tanggal_lahir));
                    }],
                    ['attribute' => 'gender', 'value' => function($m){
                        return $m->gender == 'L'?'Laki-Laki':'Perempuan';
                    }],
                    ['attribute' => 'pendidikan', 'value' => function($m){
                        return substr($m->pendidikan,2);
                    }],
                    ['attribute' => 'status_aktif',],
                ];
            break;
        }

        return $arr;
    }
}
