<?php
namespace sp3d\modules\laporan\models;
use Yii;
use yii\base\Model;

use sp3d\modules\bumdes\models\Bumdes;

class LaporanBumdes extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    potensi olah data : lokasi, luas_lahan, jenis_sertifikat, region
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        $this->pageToOpen = $this->reportType;
        switch ($this->reportType) {
            case 'bumdes-rekap':
                $this->model = Bumdes::find();
                $this->pageToOpen = 'bumdes-rekap';//region dan tahun menggunakan view yang sama
            break;
            
            default: exit('no choice'); break;
        }
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        switch($this->data['subjek']){
            case 'rekap':
                $this->model->andWhere(['' => $this->data['subjekval']]);
            break;
        }
        set_time_limit(60);
        ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
    }

    public function result()
    {
        return $this->model;
    }

    public function excellAttributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            case 'umur':
            case 'pendidikan':
            case 'jenis-kelamin':
            case 'status':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten'],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan'],
                    ['attribute' => 'desa.description', 'label' => 'Desa'],
                    ['attribute' => 'nama',],
                    ['attribute' => 'tempat_lahir',],
                    ['attribute' => 'tanggal_lahir', 'value' => function($m){
                        return date('d-F-Y', strtotime($m->tanggal_lahir));
                    }],
                    ['attribute' => 'gender', 'value' => function($m){
                        return $m->gender == 'L'?'Laki-Laki':'Perempuan';
                    }],
                    ['attribute' => 'pendidikan', 'value' => function($m){
                        return substr($m->pendidikan,2);
                    }],
                    ['attribute' => 'status_aktif',],
                ];
            break;
        }

        return $arr;
    }
}
