<?php
namespace sp3d\modules\laporan\models;
use Yii;
use yii\base\Model;

use sp3d\models\Pasardesa;
use sp3d\models\PasardesaData;

class LaporanPasar extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        $this->pageToOpen = $this->reportType;
        switch ($this->reportType) {
            case 'pasar-desa-tahun':
            case 'pasar-desa-region':
                $this->model = Pasardesa::find();
                $this->pageToOpen = 'pasar-desa-tahun';//region dan tahun menggunakan view yang sama
            break;
            case 'pasar-desa-jml-pedagang':
            case 'pasar-desa-jml-kios':
            case 'pasar-desa-jml-los':
            case 'pasar-desa-jml-lapak':
            case 'pasar-desa-jml-lesehan':
            case 'pasar-desa-jml-ruko':
                $this->model = PasardesaData::find()->joinWith('pasar');
                $this->pageToOpen = 'pasar-desa-jumlah';
            break;
            
            default: exit('no choice'); break;
        }
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        if($this->data['subjekval'] != '-' && $this->data['subjekval'] != null){
            switch($this->data['subjek']){
                case 'tahun':
                    $exp = explode('-', $this->data['subjekval']);
                    // var_dump($exp);
                    if(count($exp) > 1){
                        $this->model->andWhere(['>=','tahun', $exp[0]]);
                        $this->model->andWhere(['<=','tahun', $exp[1]]);
                    } else {
                        $this->model->andWhere(['tahun' => $this->data['subjekval']]);
                    }
                break;
                case 'region':
                    $wc = (int) strlen($this->data['subjekval']);
                    if($wc == 4){
                        $this->model->andWhere(['kd_kabupaten' => $this->data['subjekval']]);
                    }

                    if($wc == 7){
                        $this->model->andWhere(['kd_kecamatan' => $this->data['subjekval']]);
                    }
                break;
                case 'jml-pedagang':
                    $exp = explode('-', $this->data['subjekval']);
                    // var_dump($exp);
                    if(count($exp) > 1){
                        $this->model->andWhere(['>=','jml_pedagang', $exp[0]]);
                        $this->model->andWhere(['<=','jml_pedagang', $exp[1]]);
                    } else {
                        $this->model->andWhere(['jml_pedagang' => $this->data['subjekval']]);
                    }
                break;
                case 'jml-kios':
                case 'jml-los':
                case 'jml-lapak':
                case 'jml-lesehan':
                case 'jml-ruko':
                    $exp = explode('-', $this->data['subjekval']);
                    $exp1 = explode('-', $this->data['subjek']);
                    // var_dump($exp);
                    if(count($exp) > 1){
                        $this->model->andWhere(['>=','pras_'.$exp1[1], $exp[0]]);
                        $this->model->andWhere(['<=','pras_'.$exp1[1], $exp[1]]);
                    } else {
                        $this->model->andWhere(['pras_'.$exp1[1] => $this->data['subjekval']]);
                    }
                break;
            }
            ini_set('memory_limit', '512M');
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }

    public function excellAttributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            //BUMDESA
            case 'tahun':
                $arr = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'nama',],
                    ['attribute' => 'tahun',],
                    ['attribute' => 'kepemilikan_tanah'],
                    ['attribute' => 'legalitas'],
                    ['attribute' => 'no_legalitas'],
                ];
            break;
            case 'jml-pedagang':
            case 'jml-kios':
            case 'jml-los':
            case 'jml-lapak':
            case 'jml-lesehan':
            case 'jml-ruko':
                $arr = [
                    ['attribute' => 'pasar.kabupaten.description', 'label' => 'Kabupaten'],
                    ['attribute' => 'pasar.kecamatan.description', 'label' => 'Kecamatan'],
                    ['attribute' => 'pasar.desa.description', 'label' => 'Desa'],
                    ['attribute' => 'pasar.nama', 'label' => 'Nama Pasar'],
                    ['attribute' => 'pasar.tahun', 'label' => 'Tahun Berdiri'],
                    ['attribute' => 'jml_pedagang', 'label' => 'Jumlah Pedagang'],
                    ['attribute' => 'pras_kios', 'label' => 'Jumlah Kios'],
                    ['attribute' => 'pras_los', 'label' => 'Jumlah Los'],
                    ['attribute' => 'pras_lapak', 'label' => 'Jumlah Lapak'],
                    ['attribute' => 'pras_lesehan', 'label' => 'Jumlah Lesehan'],
                    ['attribute' => 'pras_ruko', 'label' => 'Jumlah RuKo'],
                ];
            break;
        }

        return $arr;
    }
}
