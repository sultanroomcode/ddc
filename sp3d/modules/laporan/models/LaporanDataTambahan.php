<?php
namespace sp3d\modules\laporan\models;
use Yii;
use yii\base\Model;

use sp3d\models\transaksi\Sp3dProfileDesa;

class LaporanDataTambahan extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    potensi olah data : lokasi, luas_lahan, jenis_sertifikat, region
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        $this->pageToOpen = $this->reportType;
        switch ($this->reportType) {
            case 'data-tambahan-umum':
                $this->model = Sp3dProfileDesa::find();
                $this->model->where('LENGTH(user)  = 10');
                $this->pageToOpen = 'data-tambahan-umum';//region dan tahun menggunakan view yang sama
            break;
            
            default: exit('no choice s'); break;
        }
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        if($this->data['subjekval'] != '-' && $this->data['subjekval'] != null){
            switch($this->data['subjek']){
                case 'umum':
                    // $this->model->andWhere(['status_aktif' => $this->data['subjekval']]);
                break;
            }
            ini_set('memory_limit', '512M');
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }

    public function excellAttributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            case 'umum':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten'],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan'],
                    ['attribute' => 'desa.description', 'label' => 'Desa'],
                    ['attribute' => 'email',],
                    ['attribute' => 'jml_rt',],
                    ['attribute' => 'jml_rw',],
                    ['attribute' => 'created_at',],
                    ['attribute' => 'updated_at',],
                ];
            break;
        }

        return $arr;
    }
}
