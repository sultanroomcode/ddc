<?php
namespace sp3d\modules\laporan\models;
use Yii;
use yii\base\Model;

use sp3d\modules\ddc\models\DdcPodes;
use sp3d\modules\ddc\models\DdcPodesPp;
use sp3d\modules\ddc\models\DdcKomoditasPodes;

class LaporanPodes extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    potensi olah data : lokasi, luas_lahan, jenis_sertifikat, region
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'];
        $this->pageToOpen = $this->reportType;
        switch ($this->reportType) {
            case 'podes-pertanian-pangan':
            case 'podes-pertanian-buah':
            case 'podes-pertanian-apotik':
            case 'podes-perkebunan':
                $this->model = DdcPodes::find()->where(['ddc_podes.jenis' => substr($data['objek'], 6)]);
                $this->pageToOpen = 'podes';
            break;
            case 'podes-kehutanan':
            case 'podes-peternakan':
            case 'podes-perikanan':
                $this->model = DdcPodesPp::find()->where(['ddc_podes_pp.jenis' => substr($data['objek'], 6)]);
                $this->pageToOpen = 'podes-pp';//region dan tahun menggunakan view yang sama
            break;
            
            default: exit('no choice'); break;
        }
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        if($this->data['subjekval'] != '-' && $this->data['subjekval'] != null){
            switch($this->data['subjek']){
                case 'luas-lahan-produksi':
                    $this->model->select(['*', "CASE
                    WHEN (luas_lahan_produksi) <= 1000 THEN '0-1000'
                    WHEN (luas_lahan_produksi) <= 2000 THEN '1001-2000'
                    WHEN (luas_lahan_produksi) <= 3000 THEN '2001-3000'
                    WHEN (luas_lahan_produksi) <= 4000 THEN '3001-4000'
                    WHEN (luas_lahan_produksi) <= 5000 THEN '4001-5000'
                    WHEN (luas_lahan_produksi) > 5000 THEN '5000-10000' END AS subjekval"]);
                    $this->model->andFilterHaving(['subjekval' => $this->data['subjekval']]);
                break;
                case 'hasil-produksi':
                    $this->model->select(['*', "CASE
                    WHEN (hasil_produksi) <= 1000 THEN '0-1000'
                    WHEN (hasil_produksi) <= 2000 THEN '1001-2000'
                    WHEN (hasil_produksi) <= 3000 THEN '2001-3000'
                    WHEN (hasil_produksi) <= 4000 THEN '3001-4000'
                    WHEN (hasil_produksi) <= 5000 THEN '4001-5000'
                    WHEN (hasil_produksi) > 5000 THEN '5000-10000' END AS subjekval"]);
                    $this->model->andFilterHaving(['subjekval' => $this->data['subjekval']]);
                break;
                case 'lahan-milik':
                    $this->model->andWhere(['lahan_milik' => $this->data['subjekval']]);
                break;
                case 'satuan':
                    $this->model->joinWith('satuanid');
                    $this->model->andWhere(['ddc_satuan.no_satuan' => $this->data['subjekval']]);
                break;
                case 'komoditas':
                    $this->model->joinWith('komoditasp');
                    $this->model->andWhere(['ddc_komoditas_podes.id' => $this->data['subjekval']]);
                break;
            }
            ini_set('memory_limit', '512M');
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }

    public function excellAttributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            case 'luas-lahan-produksi':
            case 'hasil-produksi':
            case 'lahan-milik':
            case 'satuan':
            case 'komoditas':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten'],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan'],
                    ['attribute' => 'desa.description', 'label' => 'Desa'],
                    ['attribute' => 'jenis',],
                    ['attribute' => 'komoditasp.nama_komoditas',],
                    ['attribute' => 'luas_lahan_produksi',],
                    ['attribute' => 'lahan_milik',],
                    ['attribute' => 'hasil_produksi',],
                    ['attribute' => 'satuanid.nama_satuan',],
                ];
            break;
        }

        return $arr;
    }
}
