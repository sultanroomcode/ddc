<?php
namespace sp3d\modules\laporan\controllers;

use Yii;
use yii\web\Controller;
use sp3d\models\VbumdesAll;
use sp3d\models\VbumdesRating;
use sp3d\modules\bumdes\models\VbumdesKlasifikasi;
use sp3d\modules\laporan\models\LaporanBumdes;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;

class BumdesController extends Controller
{
    //pkk, kartar, lpmd, lembaga adat
	public function actionDivLaporan()
    {
    	$data = Yii::$app->request->get();
        return $this->renderAjax('div-laporan', [
        	'data' => $data
        ]);
    }

    public function actionRekap()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('rekap', [
            'data' => $data
        ]);
    }

    public function actionKlasifikasi()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('klasifikasi', [
            'data' => $data
        ]);
    }


    public function actionKlasifikasiData()
    {
        $data = Yii::$app->request->get();
        $model = VbumdesKlasifikasi::find();

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $kode = $data['kecamatan-input'];
            $model->andWhere('kc_id = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $kode = $data['kabupaten-input'];
            $model->andWhere('kb_id = :query')->addParams([':query'=> $kode]);
            set_time_limit(120);
            ini_set('memory_limit', '-1');
        } else {
            // 'provinsi'
            set_time_limit(480);
            ini_set('memory_limit', '-1');
        }

        if(isset($data['tipe']) && ($data['tipe'] != '-' || empty($data['tipe'])))
        {
            $model->andWhere('nilai_akhir_kategori = :nak')->addParams([':nak' => $data['tipe']]);
        }


        if(isset($data['export']) && $data['export'] == 1)
        {
                $exporter = new Spreadsheet([
                'dataProvider' => new ActiveDataProvider([
                    'query' => $model,
                    'pagination' => [
                        'pageSize' => 100, // export batch size
                    ],
                ]),
                'columns' => [
                    ['attribute' => 'kb_id', 'label' => 'Kode Kabupaten'],
                    ['attribute' => 'kb_nama', 'label' => 'Nama Kabupaten'],
                    ['attribute' => 'kc_id', 'label' => 'Kode Kecamatan'],
                    ['attribute' => 'kc_nama', 'label' => 'Nama Kecamatan'],
                    ['attribute' => 'ds_id', 'label' => 'Kode Desa'],
                    ['attribute' => 'ds_nama', 'label' => 'Nama Desa'],
                    ['attribute' => 'nama_bumdes'],

                    ['attribute' => 'hasil_penilaian'],
                    ['attribute' => 'nilai_akhir'],
                    ['attribute' => 'nilai_akhir_kategori']
                ]
            ]);
            $timeId = time();
            $exporter->send('file-bumdes-klasifikasi-'.$timeId.'.xls');
        } else {
            return $this->renderAjax('klasifikasi-data', [
                'data' => $data,
                'model' => $model,
            ]);
        }

    }


    public function actionDivChart()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('div-chart', [
            'data' => $data
        ]);
    }

    public function actionData()
    {
        $data = Yii::$app->request->get();
        $model = new LaporanBumdes();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        return $this->renderAjax('data/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionExportGeneral()
    {
        $data = Yii::$app->request->get();
        $model = VbumdesAll::find();
        $model->where(['status_bumdes' => $data['status']]);

        if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $kode = $data['kecamatan-input'];
            $model->andWhere('kc_id = :query')->addParams([':query'=> $kode]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $kode = $data['kabupaten-input'];
            $model->andWhere('kb_id = :query')->addParams([':query'=> $kode]);
            set_time_limit(120);
            ini_set('memory_limit', '-1');
        } else {
            // 'provinsi'
            set_time_limit(480);
            ini_set('memory_limit', '-1');
        }

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'kb_id', 'label' => 'Kode Kabupaten'],
                ['attribute' => 'kb_nama', 'label' => 'Nama Kabupaten'],
                ['attribute' => 'kc_id', 'label' => 'Kode Kecamatan'],
                ['attribute' => 'kc_nama', 'label' => 'Nama Kecamatan'],
                ['attribute' => 'ds_id', 'label' => 'Kode Desa'],
                ['attribute' => 'ds_nama', 'label' => 'Nama Desa'],
                ['attribute' => 'nama_bumdes']
            ]
        ]);
        $timeId = time();
        $exporter->send('file-bumdes-general-'.$timeId.'.xls');
    }

    public function actionExport()
    {
        $data = Yii::$app->request->get();
        $model = new LaporanBumdes();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->result(),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => $model->excellAttributes()
        ]);
        $timeId = time();
        $exporter->send('file-'.$data['objek'].'-'.$data['subjek'].'-'.$timeId.'.xls');
    }

    public function actionFormDynamic()
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        // $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        $keypad = $data['objek'].'-'.$data['subjek'];
        switch ($keypad) {
            case 'pkk-pembina':
            case 'kartar-pembina':
            case 'lpmd-pembina':
            	 $sql ="SELECT pembina_status_ada AS subjekval
                FROM ".$data['objek']." GROUP BY subjekval";
            break;
            case 'lembagaadat-sekretariat':
                 $sql ="SELECT kesekertariatan_status_ada AS subjekval
                FROM lembaga_adat GROUP BY subjekval";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        if($data['subjek'] != 'region'){
            foreach($resultan as $v){
                $key = $val = $v['subjekval'];
                $responseval .= '<option value="'.$key.'">'.$val.'</td></tr>';
            }    
        }
        
        return $responseval;
    }

    /*
    laporan/bumdes/all-result-status-bumdes
    */
    public function actionAllResultStatusBumdes()
    {
        $model = new VbumdesRating();
        $arr = $model->dataKabupaten();

        foreach($arr as $k => $v){
         $this->exporter($k, false);//save into file
        }

        //then zip
        $zip = new \ZipArchive();
        if ($zip->open('bumdes-klasifikasi.zip', \ZipArchive::CREATE)) {
            foreach($arr as $k => $v){
                $zip->addFile("bumdes-result-".$k.".xls");
            }
            $zip->close();

            foreach ($arr as $k => $v) {
                unlink("bumdes-result-".$k.".xls");
            }
            return "OK";
        } else { return "ERROR!"; }
    }

    public function exporter($key, $export = true)
    {
        $model = VbumdesRating::find()->where(['kb_id' => $key]);

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'kb_id', 'label' => 'Kode Kabupaten'],
                ['attribute' => 'kb_nama', 'label' => 'Nama Kabupaten'],
                ['attribute' => 'kc_id', 'label' => 'Kode Kecamatan'],
                ['attribute' => 'kc_nama', 'label' => 'Nama Kecamatan'],
                ['attribute' => 'ds_id', 'label' => 'Kode Desa'],
                ['attribute' => 'ds_nama', 'label' => 'Nama Desa'],
                ['attribute' => 'nama_bumdes'],

                // ['attribute' => 'hasil_penilaian'],
                ['attribute' => 'nilai_akhir'],
                ['attribute' => 'nilai_akhir_kategori']
            ]
        ]);
        $timeId = time();
        if($export){
            $exporter->send('bumdes-result-'.$timeId.'.xls');
        } else {
            $exporter->save('bumdes-result-'.$key.'.xls');          
        }
    }
}
