<?php
namespace sp3d\modules\laporan\controllers;

use Yii;
use yii\web\Controller;
use sp3d\modules\laporan\models\LaporanTkd;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;

class TkdController extends Controller
{
	public function actionDivLaporan()
    {
    	$data = Yii::$app->request->get();
        return $this->renderAjax('div-laporan', [
        	'data' => $data
        ]);
    }

    public function actionDivChart()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('div-chart', [
            'data' => $data
        ]);
    }

    public function actionData()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanTkd();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        return $this->renderAjax('data/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionExport()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanTkd();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->result(),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => $model->excellAttributes()
        ]);
        $timeId = time();
        $exporter->send('file-tkd-'.$data['subjek'].'-'.$timeId.'.xls');
    }

    public function actionFormDynamic()
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        // $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        switch ($data['subjek']) {
            case 'luas-lahan':
            	 $sql ="SELECT CASE
                WHEN (luas_lahan) <= 500 THEN '0-500'
                WHEN (luas_lahan) <= 1000 THEN '501-1000'
			    WHEN (luas_lahan) <= 1500 THEN '1001-1500'
                WHEN (luas_lahan) <= 2000 THEN '1501-2000'
                WHEN (luas_lahan) <= 2500 THEN '2001-2500'
			    WHEN (luas_lahan) <= 3000 THEN '2501-3000'
			    WHEN (luas_lahan) > 3000  THEN '3000-10000' END AS subjekval
                FROM ddc_tkd GROUP BY subjekval";
            break;

            case 'jenis-lahan':
                 $sql ="SELECT jenis_lahan AS subjekval FROM ddc_tkd GROUP BY subjekval";
            break;

            case 'jenis-sertifikat':
                 $sql ="SELECT jenis_sertifikat AS subjekval FROM ddc_tkd GROUP BY subjekval";
            break;

            case 'lokasi':
                 $sql ="SELECT lokasi AS subjekval FROM ddc_tkd GROUP BY subjekval";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        if($data['subjek'] != 'region'){
            foreach($resultan as $v){
                $key = $val = $v['subjekval'];
                $responseval .= '<option value="'.$key.'">'.$val.'</td></tr>';
            }    
        }
        
        return $responseval;
    }
}
