<?php
namespace sp3d\modules\laporan\controllers;

use Yii;
use yii\web\Controller;
use sp3d\modules\laporan\models\LaporanPodes;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;

class PodesController extends Controller
{
	public function actionDivLaporan()
    {
    	$data = Yii::$app->request->get();
        return $this->renderAjax('div-laporan', [
        	'data' => $data
        ]);
    }

    public function actionDivChart()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('div-chart', [
            'data' => $data
        ]);
    }

    public function actionData()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanPodes();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        return $this->renderAjax('data/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionExport()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanPodes();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->result(),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => $model->excellAttributes()
        ]);
        $timeId = time();
        $exporter->send('file-'.$data['objek'].'-'.$data['subjek'].'-'.$timeId.'.xls');
    }

    public function actionFormDynamic()
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        // $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        switch ($data['objek']) {
            case 'podes-pertanian-pangan':
            case 'podes-pertanian-buah':
            case 'podes-pertanian-apotik':
            case 'podes-perkebunan':
                $table = 'ddc_podes';
            break;
            case 'podes-kehutanan':
            case 'podes-peternakan':
            case 'podes-perikanan':
                $table = 'ddc_podes_pp';
            break;
        }
        switch ($data['subjek']) {
            case 'satuan':
                $sql ="SELECT CONCAT(k.no_satuan,'--',k.nama_satuan) AS subjekval
                FROM ".$table." p LEFT JOIN ddc_satuan k ON p.satuan = k.no_satuan WHERE k.modul = '".substr($data['objek'], 6)."' GROUP BY subjekval";
            break;
            case 'komoditas':
                $sql ="SELECT CONCAT(k.id,'--',k.nama_komoditas) AS subjekval
                FROM ".$table." p LEFT JOIN ddc_komoditas_podes k ON p.komoditas = k.id WHERE k.jenis = '".substr($data['objek'], 6)."' GROUP BY subjekval";
            break;
            case 'luas-lahan-produksi':
            	 $sql ="SELECT CASE
                WHEN (luas_lahan_produksi) <= 1000 THEN '0-1000'
                WHEN (luas_lahan_produksi) <= 2000 THEN '1001-2000'
                WHEN (luas_lahan_produksi) <= 3000 THEN '2001-3000'
                WHEN (luas_lahan_produksi) <= 4000 THEN '3001-4000'
                WHEN (luas_lahan_produksi) <= 5000 THEN '4001-5000'
                WHEN (luas_lahan_produksi) > 5000 THEN '5000-10000' END AS subjekval
                FROM ".$table." GROUP BY subjekval";
            break;

            case 'lahan-milik':
                 $sql ="SELECT lahan_milik AS subjekval FROM ".$table." GROUP BY subjekval";
            break;
            case 'hasil-produksi':
                 $sql ="SELECT CASE
                WHEN (hasil_produksi) <= 1000 THEN '0-1000'
                WHEN (hasil_produksi) <= 2000 THEN '1001-2000'
                WHEN (hasil_produksi) <= 3000 THEN '2001-3000'
                WHEN (hasil_produksi) <= 4000 THEN '3001-4000'
                WHEN (hasil_produksi) <= 5000 THEN '4001-5000'
                WHEN (hasil_produksi) > 5000 THEN '5000-10000' END AS subjekval
                FROM ".$table." GROUP BY subjekval";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        if($data['subjek'] != 'region'){
            foreach($resultan as $v){
                if($data['subjek'] == 'komoditas' || $data['subjek'] == 'satuan'){
                    $value = explode('--',$v['subjekval']);
                    $key = $value[0];
                    $val = $value[1];
                } else {
                    $key = $val = $v['subjekval'];
                }
                $responseval .= '<option value="'.$key.'">'.$val.'</option>';
            }    
        }
        
        return $responseval;
    }
}
