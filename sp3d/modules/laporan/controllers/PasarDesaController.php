<?php
namespace sp3d\modules\laporan\controllers;

use Yii;
use yii\web\Controller;
use sp3d\modules\laporan\models\LaporanPasar;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;

class PasarDesaController extends Controller
{
	public function actionDivLaporan()
    {
    	$data = Yii::$app->request->get();
        return $this->renderAjax('div-laporan', [
        	'data' => $data
        ]);
    }

    public function actionDivChart()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('div-chart', [
            'data' => $data
        ]);
    }

    public function actionFormDynamic()
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        // $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        switch ($data['subjek']) {
            case 'status':
                $sql ="SELECT status AS subjekval FROM bumdes GROUP BY subjekval";
            break;    
            case 'jml-kios':
            	 $sql ="SELECT CASE
                WHEN (pras_kios) <= 50 THEN '0-50'
                WHEN (pras_kios) <= 100 THEN '51-100'
			    WHEN (pras_kios) <= 150 THEN '101-150'
			    WHEN (pras_kios) <= 200 THEN '150-200'
			    WHEN (pras_kios) > 200  THEN '200-1000' END AS subjekval
                FROM pasardesa_data GROUP BY subjekval";
            break;
            case 'jml-los':
            	 $sql ="SELECT CASE
                WHEN (pras_los) <= 50 THEN '0-50'
                WHEN (pras_los) <= 100 THEN '51-100'
			    WHEN (pras_los) <= 150 THEN '101-150'
			    WHEN (pras_los) <= 200 THEN '150-200'
			    WHEN (pras_los) > 200  THEN '200-1000' END AS subjekval
                FROM pasardesa_data GROUP BY subjekval";
            break;
            case 'jml-lapak':
            	 $sql ="SELECT CASE
                WHEN (pras_lapak) <= 50 THEN '0-50'
                WHEN (pras_lapak) <= 100 THEN '51-100'
			    WHEN (pras_lapak) <= 150 THEN '101-150'
			    WHEN (pras_lapak) <= 200 THEN '150-200'
			    WHEN (pras_lapak) > 200  THEN '200-1000' END AS subjekval
                FROM pasardesa_data GROUP BY subjekval";
            break;
            case 'jml-lesehan':
            	 $sql ="SELECT CASE
                WHEN (pras_lesehan) <= 50 THEN '0-50'
                WHEN (pras_lesehan) <= 100 THEN '51-100'
			    WHEN (pras_lesehan) <= 150 THEN '101-150'
			    WHEN (pras_lesehan) <= 200 THEN '150-200'
			    WHEN (pras_lesehan) > 200  THEN '200-1000' END AS subjekval
                FROM pasardesa_data GROUP BY subjekval";
            break;
            case 'jml-ruko':
            	 $sql ="SELECT CASE
                WHEN (pras_ruko) <= 50 THEN '0-50'
                WHEN (pras_ruko) <= 100 THEN '51-100'
			    WHEN (pras_ruko) <= 150 THEN '101-150'
			    WHEN (pras_ruko) <= 200 THEN '150-200'
			    WHEN (pras_ruko) > 200  THEN '200-1000' END AS subjekval
                FROM pasardesa_data GROUP BY subjekval";
            break;
            case 'jml-pedagang':
                $sql ="SELECT CASE
                WHEN (jml_pedagang) <= 50 THEN '0-50'
                WHEN (jml_pedagang) <= 100 THEN '51-100'
			    WHEN (jml_pedagang) <= 150 THEN '101-150'
			    WHEN (jml_pedagang) <= 200 THEN '150-200'
			    WHEN (jml_pedagang) > 200  THEN '200-1000' END AS subjekval
                FROM pasardesa_data GROUP BY subjekval";
            break;
            case 'region':
            case 'tahun':
                $sql ="SELECT tahun AS subjekval FROM `pasardesa` GROUP BY subjekval;";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        if($data['subjek'] != 'region'){
            foreach($resultan as $v){
                $key = $val = $v['subjekval'];
                $responseval .= '<option value="'.$key.'">'.$val.'</td></tr>';
            }    
        }
        
        return $responseval;
    }

    public function actionData()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanPasar();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        return $this->renderAjax('data/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionExport()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanPasar();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->result(),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => $model->excellAttributes()
        ]);
        $timeId = time();
        $exporter->send('file-pasar-desa-'.$data['subjek'].'-'.$timeId.'.xls');
    }
}
