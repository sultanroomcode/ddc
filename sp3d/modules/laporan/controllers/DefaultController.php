<?php
namespace sp3d\modules\laporan\controllers;

use yii\web\Controller;

/**
 * Default controller for the `laporan` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionFormDynamic()
    {
        return $this->render('index');
    }
}
