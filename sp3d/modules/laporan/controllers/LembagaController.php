<?php
namespace sp3d\modules\laporan\controllers;

use Yii;
use yii\web\Controller;
use sp3d\modules\laporan\models\LaporanLembaga;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;

class LembagaController extends Controller
{
    //pkk, kartar, lpmd, lembaga adat
	public function actionDivLaporan()
    {
    	$data = Yii::$app->request->get();
        return $this->renderAjax('div-laporan', [
        	'data' => $data
        ]);
    }

    public function actionDivChart()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('div-chart', [
            'data' => $data
        ]);
    }

    public function actionData()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanLembaga();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        return $this->renderAjax('data/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionExport()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanLembaga();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->result(),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => $model->excellAttributes()
        ]);
        $timeId = time();
        $exporter->send('file-'.$data['objek'].'-'.$data['subjek'].'-'.$timeId.'.xls');
    }

    public function actionFormDynamic()
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        // $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        $keypad = $data['objek'].'-'.$data['subjek'];
        switch ($keypad) {
            case 'pkk-sumber-dana':
            case 'pkk-kegiatan':
            case 'pkk-kepengurusan':
                $sql ="SELECT pembina_status_ada AS subjekval
                FROM ".$data['objek']." GROUP BY subjekval";
            break;
            case 'kartar-pembina':
            case 'pkk-pembina':
            case 'lpmd-pembina':
            	 $sql ="SELECT pembina_status_ada AS subjekval
                FROM ".$data['objek']." GROUP BY subjekval";
            break;
            case 'lembagaadat-sekretariat':
                 $sql ="SELECT kesekertariatan_status_ada AS subjekval
                FROM lembaga_adat GROUP BY subjekval";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        if($data['subjek'] != 'region'){
            foreach($resultan as $v){
                $key = $val = $v['subjekval'];
                $responseval .= '<option value="'.$key.'">'.$val.'</td></tr>';
            }    
        }
        
        return $responseval;
    }
}
