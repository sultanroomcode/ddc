<?php
namespace sp3d\modules\laporan\controllers;

use Yii;
use yii\web\Controller;
use sp3d\modules\laporan\models\LaporanDataTambahan;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;
use sp3d\models\User;
use yii\db\Query;

class DataTambahanController extends Controller
{
	public function actionDivLaporan()
    {
    	$data = Yii::$app->request->get();
        return $this->renderAjax('div-laporan', [
        	'data' => $data
        ]);
    }

    public function actionDivChart()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('div-chart', [
            'data' => $data
        ]);
    }

    public function actionData()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanDataTambahan();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        return $this->renderAjax('data/'.$model->pageToOpen, [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionBelum()
    {
        $data = Yii::$app->request->get();
        $conn = Yii::$app->db;

        if($data['kabupaten-input'] == '' && $data['kecamatan-input'] == ''){
            $id = '';
            $tingkat = 'provinsi';
        } else if($data['kabupaten-input'] != '' && $data['kecamatan-input'] != '') {
            $id = $data['kecamatan-input'];
            $tingkat = 'kecamatan';
        } else if($data['kabupaten-input'] != '' && $data['kecamatan-input'] == '') {
            $id = $data['kabupaten-input'];
            $tingkat = 'kabupaten';
        }

        $model = User::find()->select(['a.id AS id', 'a.description AS nama_desa', 'SUBSTR(a.id, 1,4) AS kd_kabupaten', 'kb.description AS nama_kabupaten', 'SUBSTR(a.id, 1,7) AS kd_kecamatan', 'kc.description AS nama_kecamatan'])
        ->from('user a')
        ->join('LEFT JOIN', 'user kc', 'kc.id = SUBSTR(a.id, 1,7)')
        ->join('LEFT JOIN', 'user kb', 'kb.id = SUBSTR(a.id, 1,4)')
        ->join('LEFT JOIN', 'sp3d_profile_desa b', 'a.id = b.user COLLATE utf8_general_ci')
        ->where("a.type = 'desa' AND b.jml_rt IS NULL");

        switch ($tingkat) {
            case 'kecamatan':
                $model->having("kd_kecamatan = '".$id."'");
            break;
            case 'kabupaten':
                $model->having("kd_kabupaten = '".$id."'");
            break;
            default://provinsi
                
            break;
        }
            
        return $this->renderAjax('data/belum', [
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionExport()
    {
        $data = Yii::$app->request->get();
        $data['subjekval'] = trim(urldecode($data['subjekval']));
        $model = new LaporanDataTambahan();
        $model->dataSet($data);
        $model->dataRegion();
        $model->dataSelect();

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->result(),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => $model->excellAttributes()
        ]);
        $timeId = time();
        $exporter->send('file-data-profile-'.$data['subjek'].'-'.$timeId.'.xls');
    }

    public function actionExportBelum()
    {
        $data = Yii::$app->request->get();
        if($data['kabupaten-input'] == '' && $data['kecamatan-input'] == ''){
            $id = '';
            $tingkat = 'provinsi';
        } else if($data['kabupaten-input'] != '' && $data['kecamatan-input'] != '') {
            $id = $data['kecamatan-input'];
            $tingkat = 'kecamatan';
        } else if($data['kabupaten-input'] != '' && $data['kecamatan-input'] == '') {
            $id = $data['kabupaten-input'];
            $tingkat = 'kabupaten';
        }

        $model = User::find()->select(['a.id AS id', 'a.description AS nama_desa', 'SUBSTR(a.id, 1,4) AS kd_kabupaten', 'kb.description AS nama_kabupaten', 'SUBSTR(a.id, 1,7) AS kd_kecamatan', 'kc.description AS nama_kecamatan'])
        ->from('user a')
        ->join('LEFT JOIN', 'user kc', 'kc.id = SUBSTR(a.id, 1,7)')
        ->join('LEFT JOIN', 'user kb', 'kb.id = SUBSTR(a.id, 1,4)')
        ->join('LEFT JOIN', 'sp3d_profile_desa b', 'a.id = b.user COLLATE utf8_general_ci')
        ->where("a.type = 'desa' AND b.jml_rt IS NULL");

        switch ($tingkat) {
            case 'kecamatan':
                $model->having("kd_kecamatan = '".$id."'");
            break;
            case 'kabupaten':
                $model->having("kd_kabupaten = '".$id."'");
            break;
            default://provinsi
                
            break;
        }

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                // 'query' => $model->all(),
                'query' => $model,
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'nama_kabupaten'],
                ['attribute' => 'nama_kecamatan'],
                ['attribute' => 'nama_desa'],
            ]
        ]);
        $timeId = time();
        $exporter->send('file-data-belum-isi-profile-'.$timeId.'.xls');
    }

    public function actionFormDynamic()
    {
        $data = Yii::$app->request->get();
        // var_dump($data);
        // $objek = ($data['jabatan'] != '-')?$data['jabatan']:$data['objek'];
        switch ($data['subjek']) {
            case 'umum':
                 $sql ="SELECT status_aktif AS subjekval FROM kpm GROUP BY subjekval";
            break;
        }

        $conn = Yii::$app->db;//2018-
        $model = $conn->createCommand($sql);
        $resultan = $model->queryAll();
        $responseval = '<option value="-">-</option>';
        if($data['subjek'] != 'umum'){
            foreach($resultan as $v){
                if($data['subjek'] == 'pendidikan'){
                    $val = substr($v['subjekval'], 2);
                    $key = $v['subjekval'];
                } else {
                    $key = $val = $v['subjekval'];
                }
                $responseval .= '<option value="'.$key.'">'.$val.'</td></tr>';
            }    
        }
        
        return $responseval;
    }
}
