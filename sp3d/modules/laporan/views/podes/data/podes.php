<h2 align="center">Daftar Potensi Desa di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kpm">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Jenis</th>
            <th>Komoditas</th>
            <th>Luas Lahan</th>
            <th>Lahan Milik</th>
            <th>Hasil Produksi</th>
            <th>Satuan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Jenis</th>
            <th>Komoditas</th>
            <th>Luas Lahan</th>
            <th>Lahan Milik</th>
            <th>Hasil Produksi</th>
            <th>Satuan</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <td><?= $v->kabupaten->description ?></td>
            <td><?= $v->kecamatan->description ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= $v->jenis ?></td>
            <td><?= $v->komoditasp->nama_komoditas ?></td>
            <td><?= $v->luas_lahan_produksi ?></td>
            <td><?= $v->lahan_milik ?></td>
            <td><?= $v->hasil_produksi ?></td>
            <td><?= $v->satuanid->nama_satuan ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kpm').DataTable();
JS;

$this->registerJs($script);


