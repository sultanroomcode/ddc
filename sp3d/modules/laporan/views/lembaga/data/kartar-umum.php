<?php 
$subjectArr = [
    'pembina' => ['label' => 'Status Pembina', 'val' => 'pembina_status_ada'],
];
?>
<h2 align="center">Daftar Karang Taruna di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kartar">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>            
            <th>Ditetapkan</th>
            <th>SK Lembaga</th>
            <th><?= $subjectArr[$data['subjek']]['label'] ?></th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>  
            <th>Ditetapkan</th>
            <th>SK Lembaga</th>
            <th><?= $subjectArr[$data['subjek']]['label'] ?></th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <td><?= $v->kabupaten->description ?></td>
            <td><?= $v->kecamatan->description ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= $v->ditetapkan_oleh ?></td>
            <td><?= $v->sk_lembaga ?></td>
            <td><?= $v->{$subjectArr[$data['subjek']]['val']} ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kartar').DataTable();
JS;

$this->registerJs($script);


