<h2 align="center">Daftar Tanah Kas Desa di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-tkd">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>            
            <th>Jenis Sertifikat</th>
            <th>Nomor Sertifikat</th>
            <th>Luas Lahan</th>
            <th>Lokasi</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>  
            <th>Jenis Sertifikat</th>
            <th>Nomor Sertifikat</th>
            <th>Luas Lahan</th>
            <th>Lokasi</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <td><?= $v->kabupaten->description ?></td>
            <td><?= $v->kecamatan->description ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= $v->jenis_sertifikat ?></td>
            <td><?= $v->nomor ?></td>
            <td><?= $v->luas_lahan ?></td>
            <td><?= $v->lokasi ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-tkd').DataTable();
JS;

$this->registerJs($script);


