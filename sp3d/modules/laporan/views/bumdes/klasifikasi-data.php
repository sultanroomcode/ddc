<div class="box-body">  	
  	<table id="klasifikasi-bumdes-table" class="display table" cellspacing="0" width="100%">
	    <thead>
	        <tr>
	            <th>Kabupaten</th>
	            <th>Kecamatan</th>
	            <th>Desa</th>
	            <th>Nama Bumdes</th>
	            <th>Jumlah Poin</th>
	            <th>Nilai Akhir</th>
	            <th>Kategori</th>
	        </tr>
	    </thead>
	    <tfoot>
	        <tr>
	            <th>Kabupaten</th>
	            <th>Kecamatan</th>
	            <th>Desa</th>
	            <th>Nama Bumdes</th>
	            <th>Jumlah Poin</th>
	            <th>Nilai Akhir</th>
	            <th>Kategori</th>
	        </tr>
	    </tfoot>
	    <tbody>
	    	<?php foreach ($model->all() as $v): ?>
	        <tr>
	            <td><?= $v->kb_nama ?></td>
	            <td><?= $v->kc_nama ?></td>
	            <td><?= $v->ds_nama ?></td>
	            <td><?= $v->nama_bumdes ?></td>
	            <td><?= $v->hasil_penilaian ?></td>
	            <td><?= $v->nilai_akhir ?></td>
	            <td><?= $v->nilai_akhir_kategori ?></td>
	        </tr>
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>
<!-- /.box -->
<?php
$scripts =<<<JS
	$('#klasifikasi-bumdes-table').DataTable();
JS;

$this->registerJs($scripts);