<?php
use sp3d\models\User;
use sp3d\models\transaksi\Sp3dMasterJabatan;
use yii\helpers\ArrayHelper;
$id = Yii::$app->user->identity->id;
$tipeuser = Yii::$app->user->identity->type;
$arr = ['-' => ''];
$listArr = [];

if($tipeuser == 'provinsi'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
}

if($tipeuser == 'kabupaten'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
}

$listArr = $arr + $listArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">DAFTAR KLASIFIKASI BUMDES</h2>
            <div class="p-10">
                <div class="row">
                    <?php if($tipeuser == 'provinsi'){ ?>
                    <div class="col-md-2">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } 
                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten'){ ?>
                    <div class="col-md-2">
                    	<?php if($tipeuser == 'kabupaten'){ ?>
	                    	<input type="hidden" id="kabupaten" value="<?= substr($id, 0, 4) ?>">
	                    <?php } ?>
	                    
                        <select id="kecamatan" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    }
                    ?>
                    <div class="col-md-2">
                        <select id="tipe-klasifikasi" class="form-control">
                            <option value="-">-</option>
                            <option value="PEMULA">Pemula</option>
                            <option value="BERKEMBANG">Berkembang</option>
                            <option value="MAJU">Maju</option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <button class="btn btn-danger" onclick="filteringAdd()">Filter</button>
                        <button class="btn btn-danger" onclick="filteringExport()">Export</button>
                    </div>
                </div>
            	<hr>
            	<div id="report-list-zona"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
var tipeklasifikasi = $('#tipe-klasifikasi').val();

$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});

$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

function filteringAdd(){
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    var tipe_klasifikasi = $('#tipe-klasifikasi').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }

    goLoad({elm:'#report-list-zona', url:'/laporan/bumdes/klasifikasi-data?tipe='+tipe_klasifikasi+'&kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan});
}

function filteringExport(){
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    var tipe_klasifikasi = $('#tipe-klasifikasi').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    
    window.open(base_url+'/laporan/bumdes/klasifikasi-data?export=1&tipe='+tipe_klasifikasi+'&kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan, '_blank');
}
JS;

$this->registerJs($scripts);