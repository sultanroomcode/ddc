<h2 align="center">Daftar Data Tambahan di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-tambahan-umum">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>       
            <td><?= $v->nama_kabupaten ?></td>
            <td><?= $v->nama_kecamatan ?></td>
            <td><?= $v->nama_desa ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-tambahan-umum').DataTable();
JS;

$this->registerJs($script);


