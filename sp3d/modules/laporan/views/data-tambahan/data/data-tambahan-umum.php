<h2 align="center">Daftar Data Desa di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-tambahan-umum">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Email</th>
            <th>Jumlah RT</th>
            <th>Jumlah RW</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Email</th>
            <th>Jumlah RT</th>
            <th>Jumlah RW</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <td><?= $v->kabupaten->description ?></td>
            <td><?= $v->kecamatan->description ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= $v->email ?></td>
            <td><?= $v->jml_rt ?></td>
            <td><?= $v->jml_rw ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-tambahan-umum').DataTable();
JS;

$this->registerJs($script);


