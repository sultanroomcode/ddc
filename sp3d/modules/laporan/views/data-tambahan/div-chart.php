<?php
use sp3d\models\User;
use yii\helpers\ArrayHelper;
$id = Yii::$app->user->identity->id;
$tipeuser = Yii::$app->user->identity->type;
$arr = ['-' => ''];
$listArr = [];
$data['objek'] = 'data-tambahan';
$objectArr = [
    'data-tambahan' => 'Data Desa',
];
//pembina, jumlah-pengurus-aktif, jumlah-kegiatan, 
$subjectArr = [
    'umum' => 'Umum',
];

if($tipeuser == 'provinsi'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
}

if($tipeuser == 'kabupaten'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
}

$listArr = $arr + $listArr;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">DAFTAR <?= $objectArr[$data['objek']] ?> Berdasarkan <?= $subjectArr[$data['subjek']] ?></h2>
            <div class="p-10">
                <div class="row">
                    <input type="hidden" id="objek" value="<?= $data['objek'] ?>">
                    <input type="hidden" id="subjek" value="<?= $data['subjek'] ?>">
                    <?php if($tipeuser == 'provinsi'){ ?>
                    <div class="col-md-3">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } 
                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten'){ ?>
                    <div class="col-md-3">
                        <?php if($tipeuser == 'kabupaten'){ ?>
                            <input type="hidden" id="kabupaten" value="<?= substr($id, 0, 4) ?>">
                        <?php } ?>
                        
                        <select id="kecamatan" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    }
                    ?>

                    <div class="col-md-3">
                        <button class="btn btn-danger" onclick="filteringGrafik()">Grafik</button>
                    </div>
                </div>
                <hr>
                <div id="report-chart-zona"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$scripts =<<<JS
var objek = $('#objek').val();
var subjek = $('#subjek').val();

$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});

$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

function filteringGrafik(){
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    goLoad({elm:'#report-chart-zona', url:'/chart/default/request-chart?objek='+objek+'&subjek='+subjek+'&jabatan=&kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan});
}
JS;

$this->registerJs($scripts);