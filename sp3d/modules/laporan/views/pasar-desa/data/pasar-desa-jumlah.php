<?php 
$subjectArr = [
    'jml-pedagang' => ['label' => 'Jumlah Pedagang', 'val' => 'jml_pedagang'],
    'jml-los' => ['label' => 'Jumlah Los', 'val' => 'pras_los'],
    'jml-lapak' => ['label' => 'Jumlah Lapak', 'val' => 'pras_lapak'],
    'jml-kios' => ['label' => 'Jumlah Kios', 'val' => 'pras_kios'],
    'jml-ruko' => ['label' => 'Jumlah Ruko', 'val' => 'pras_ruko'],
    'jml-lesehan' => ['label' => 'Jumlah Lesehan', 'val' => 'pras_lesehan'],
];
?>
<h2 align="center">Daftar Pasar Desa di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-pasar-data">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>            
            <th>Nama</th>
            <th>Tahun Berdiri</th>
            <th>Tahun Isian</th>
            <th><?=$subjectArr[$data['subjek']]['label']?></th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>         
            <th>Nama</th>
            <th>Tahun Berdiri</th>
            <th>Tahun Isian</th>
            <th><?=$subjectArr[$data['subjek']]['label']?></th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <td><?= $v->pasar->kabupaten->description ?></td>
            <td><?= $v->pasar->kecamatan->description ?></td>
            <td><?= $v->pasar->desa->description ?></td>
            <td><?= $v->pasar->nama ?></td>
            <td><?= $v->pasar->tahun ?></td>
            <td><?= date('Y', $v->kd_isian) ?></td>
            <td><?= $v->{$subjectArr[$data['subjek']]['val']} ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-pasar-data').DataTable();
JS;

$this->registerJs($script);