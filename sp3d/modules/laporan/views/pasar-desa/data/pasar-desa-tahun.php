<h2 align="center">Daftar Pasar Desa di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-pasar">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>            
            <th>Nama</th>
            <th>Tahun</th>
            <th>Kepemilikan Tanah</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>         
            <th>Nama</th>
            <th>Tahun</th>
            <th>Kepemilikan Tanah</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <th><?= $v->kabupaten->description ?></th>
            <th><?= $v->kecamatan->description ?></th>
            <th><?= $v->desa->description ?></th>         
            <td><?= $v->nama ?></td>
            <td><?= $v->tahun ?></td>
            <td><?= $v->kepemilikan_tanah ?></td>
            <td>Aksi</td>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-pasar').DataTable();
JS;

$this->registerJs($script);