<h2 align="center">Daftar KPM di Provinsi Jawa Timur</h2>
<table class="table compact" id="data-kpm">
    <thead>
        <tr>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
            <th>Pendidikan</th>
            <th>Status Aktif</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>   
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Nama</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
            <th>Pendidikan</th>
            <th>Status Aktif</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->result()->all() as $v): ?>
        <tr>       
            <td><?= $v->kabupaten->description ?></td>
            <td><?= $v->kecamatan->description ?></td>
            <td><?= $v->desa->description ?></td>
            <td><?= $v->nama ?></td>
            <td><?= $v->tempat_lahir ?></td>
            <td><?= $v->tanggal_lahir ?></td>
            <td><?= $v->gender=='L'?'Laki-Laki':'Perempuan' ?></td>
            <td><?= substr($v->pendidikan, 2) ?></td>
            <td><?= $v->status_aktif ?></td>
            <td>Aksi</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-kpm').DataTable();
JS;

$this->registerJs($script);


