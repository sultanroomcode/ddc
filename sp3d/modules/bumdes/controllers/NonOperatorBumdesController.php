<?php
namespace sp3d\modules\bumdes\controllers;

use yii\web\Controller;

/**
 * Default controller for the `bumdes` module
 */
class NonOperatorBumdesController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($bumdes='mandiri')
    {
        return $this->renderAjax('index', [
        	'bumdes' => $bumdes
        ]);
    }

    public function actionBumdes($j='mandiri')
    {
        return $this->renderAjax('bumdes', [
        	'bumdes' => $j
        ]);
    }
}
