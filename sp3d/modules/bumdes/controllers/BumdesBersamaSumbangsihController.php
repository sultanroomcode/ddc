<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaSumbangsih;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaSumbangsihController implements the CRUD actions for BumdesBersamaSumbangsih model.
 */
class BumdesBersamaSumbangsihController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaSumbangsih models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaSumbangsih::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaSumbangsih model.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return mixed
     */
    public function actionView($kd_desa, $id, $tahun)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_desa, $id, $tahun),
        ]);
    }

    /**
     * Creates a new BumdesBersamaSumbangsih model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BumdesBersamaSumbangsih();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'tahun' => $model->tahun]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaSumbangsih model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $tahun)
    {
        $model = $this->findModel($kd_desa, $id, $tahun);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'tahun' => $model->tahun]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaSumbangsih model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $tahun)
    {
        $this->findModel($kd_desa, $id, $tahun)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BumdesBersamaSumbangsih model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return BumdesBersamaSumbangsih the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $tahun)
    {
        if (($model = BumdesBersamaSumbangsih::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'tahun' => $tahun])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
