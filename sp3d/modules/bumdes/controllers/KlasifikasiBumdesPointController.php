<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\KlasifikasiBumdesPoint;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KlasifikasiBumdesPointController implements the CRUD actions for KlasifikasiBumdesPoint model.
 */
class KlasifikasiBumdesPointController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KlasifikasiBumdesPoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => KlasifikasiBumdesPoint::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KlasifikasiBumdesPoint model.
     * @param string $kode
     * @param integer $skor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kode, $skor)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kode, $skor),
        ]);
    }

    /**
     * Creates a new KlasifikasiBumdesPoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KlasifikasiBumdesPoint();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kode' => $model->kode, 'skor' => $model->skor]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KlasifikasiBumdesPoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kode
     * @param integer $skor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kode, $skor)
    {
        $model = $this->findModel($kode, $skor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kode' => $model->kode, 'skor' => $model->skor]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KlasifikasiBumdesPoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kode
     * @param integer $skor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kode, $skor)
    {
        $this->findModel($kode, $skor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KlasifikasiBumdesPoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kode
     * @param integer $skor
     * @return KlasifikasiBumdesPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kode, $skor)
    {
        if (($model = KlasifikasiBumdesPoint::findOne(['kode' => $kode, 'skor' => $skor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    //other 
    
}
