<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaPerkembangan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaPerkembanganController implements the CRUD actions for BumdesBersamaPerkembangan model.
 */
class BumdesBersamaPerkembanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaPerkembangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaPerkembangan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaPerkembangan model.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $tahun
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_kabupaten, $id_bb, $tahun)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $tahun),
        ]);
    }

    /**
     * Creates a new BumdesBersamaPerkembangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BumdesBersamaPerkembangan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'tahun' => $model->tahun]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BumdesBersamaPerkembangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $tahun
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $tahun)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $tahun);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'tahun' => $model->tahun]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BumdesBersamaPerkembangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $tahun
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_kabupaten, $id_bb, $tahun)
    {
        $this->findModel($kd_kabupaten, $id_bb, $tahun)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BumdesBersamaPerkembangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $tahun
     * @return BumdesBersamaPerkembangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $tahun)
    {
        if (($model = BumdesBersamaPerkembangan::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'tahun' => $tahun])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
