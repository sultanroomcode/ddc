<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaModal;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaModalController implements the CRUD actions for BumdesBersamaModal model.
 */
class BumdesBersamaModalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaModal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaModal::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaModal model.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $tahun
     * @return mixed
     */
    public function actionView($kd_kabupaten, $id_bb, $tahun)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $tahun),
        ]);
    }

    /**
     * Creates a new BumdesBersamaModal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_kabupaten, $id_bb)
    {
        $model = new BumdesBersamaModal();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaModal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $tahun
     * @return mixed
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $tahun)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $tahun);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'tahun' => $tahun];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaModal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $tahun
     * @return mixed
     */
    public function actionDelete($kd_kabupaten, $id_bb, $tahun)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $tahun)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersamaModal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $tahun
     * @return BumdesBersamaModal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $tahun)
    {
        if (($model = BumdesBersamaModal::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'tahun' => $tahun])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
