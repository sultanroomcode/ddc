<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaKomoditas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaKomoditasController implements the CRUD actions for BumdesBersamaKomoditas model.
 */
class BumdesBersamaKomoditasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaKomoditas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaKomoditas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaKomoditas model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $id_komoditas
     * @return mixed
     */
    public function actionView($kd_kabupaten, $id_bb, $id_komoditas)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $id_komoditas),
        ]);
    }

    /**
     * Creates a new BumdesBersamaKomoditas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_kabupaten, $id_bb)
    {
        $model = new BumdesBersamaKomoditas();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaKomoditas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param integer $id
     * @param integer $id_komoditas
     * @return mixed
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $id_komoditas)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_komoditas);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_komoditas' => $id_komoditas];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaKomoditas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param integer $id
     * @param integer $id_komoditas
     * @return mixed
     */
    public function actionDelete($kd_kabupaten, $id_bb, $id_komoditas)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_komoditas)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersamaKomoditas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param integer $id
     * @param integer $id_komoditas
     * @return BumdesBersamaKomoditas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $id_komoditas)
    {
        if (($model = BumdesBersamaKomoditas::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_komoditas' => $id_komoditas])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
