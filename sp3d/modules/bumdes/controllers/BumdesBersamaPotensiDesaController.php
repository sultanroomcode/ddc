<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaPotensiDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaPotensiDesaController implements the CRUD actions for BumdesBersamaPotensiDesa model.
 */
class BumdesBersamaPotensiDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaPotensiDesa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaPotensiDesa::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaPotensiDesa model.
     * @param string $kd_kabupaten
     * @param integer $id
     * @param integer $id_potensi
     * @return mixed
     */
    public function actionView($kd_kabupaten, $id_bb, $id_potensi)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $id_potensi),
        ]);
    }

    /**
     * Creates a new BumdesBersamaPotensiDesa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_kabupaten, $id_bb)
    {
        $model = new BumdesBersamaPotensiDesa();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaPotensiDesa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param integer $id
     * @param integer $id_potensi
     * @return mixed
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $id_potensi)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_potensi);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_potensi' => $id_potensi];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaPotensiDesa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param integer $id
     * @param integer $id_potensi
     * @return mixed
     */
    public function actionDelete($kd_kabupaten, $id_bb, $id_potensi)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_potensi)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersamaPotensiDesa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param integer $id
     * @param integer $id_potensi
     * @return BumdesBersamaPotensiDesa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $id_potensi)
    {
        if (($model = BumdesBersamaPotensiDesa::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_potensi' => $id_potensi])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
