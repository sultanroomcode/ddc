<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesPotensiDesa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesPotensiDesaController implements the CRUD actions for BumdesPotensiDesa model.
 */
class BumdesPotensiDesaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesPotensiDesa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesPotensiDesa::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesPotensiDesa model.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $id_potensi
     * @return mixed
     */
    public function actionView($kd_desa, $id, $id_potensi)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_desa, $id, $id_potensi),
        ]);
    }

    /**
     * Creates a new BumdesPotensiDesa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id_bumdes, $provinsi=0)
    {
        $model = new BumdesPotensiDesa();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes,'provinsi' => $provinsi];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesPotensiDesa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $id_potensi
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id_bumdes, $id_potensi, $provinsi=0)
    {
        $model = $this->findModel($kd_desa, $id_bumdes, $id_potensi);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_potensi' => $id_potensi, 'provinsi' => $provinsi];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesPotensiDesa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $id_potensi
     * @return mixed
     */
    public function actionDelete($kd_desa, $id_bumdes, $id_potensi)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($kd_desa, $id_bumdes, $id_potensi)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesPotensiDesa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param integer $id_potensi
     * @return BumdesPotensiDesa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_bumdes, $id_potensi)
    {
        if (($model = BumdesPotensiDesa::findOne(['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_potensi' => $id_potensi])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
