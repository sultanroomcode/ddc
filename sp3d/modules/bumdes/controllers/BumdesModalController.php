<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesModal;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesModalController implements the CRUD actions for BumdesModal model.
 */
class BumdesModalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesModal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesModal::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesModal model.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return mixed
     */
    public function actionView($kd_desa, $id_bumdes, $tahun)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_bumdes, $tahun),
        ]);
    }

    /**
     * Creates a new BumdesModal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_bumdes, $kd_desa, $provinsi=0)
    {
        $model = new BumdesModal();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'provinsi' => $provinsi];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesModal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id_bumdes, $tahun, $provinsi=0)
    {
        $model = $this->findModel($kd_desa, $id_bumdes, $tahun);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'provinsi' => $provinsi];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesModal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return mixed
     */
    public function actionDelete($kd_desa, $id_bumdes, $tahun)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($kd_desa, $id_bumdes, $tahun)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesModal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return BumdesModal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_bumdes, $tahun)
    {
        if (($model = BumdesModal::findOne(['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'tahun' => $tahun])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
