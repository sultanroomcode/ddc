<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersama;
use sp3d\modules\bumdes\models\BumdesBersamaDesaAnggota;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaController implements the CRUD actions for BumdesBersama model.
 */
class BumdesBersamaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersama models.
     * @return mixed
     */
    public function actionIndexProvinsi()
    {
        $dataProvider = BumdesBersama::find();//substr(Yii::$app->user->identity->id, 0,4)

        return $this->renderAjax('index-provinsi', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = BumdesBersama::find()->where(['created_by' => Yii::$app->user->identity->id]);//substr(Yii::$app->user->identity->id, 0,4)

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersama model.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @return mixed
     */
    public function actionView($kd_kabupaten, $id_bb)
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb),
            'data' => $data
        ]);
    }

    public function actionViewOnly()//bisa melihat hanya jika punya akses ke bumdes
    {
        $dataProvider = BumdesBersamaDesaAnggota::find()->where(['kd_desa' => Yii::$app->user->identity->id]);
        return $this->renderAjax('view-only', [
            'model' => $dataProvider
        ]);
    }

    public function actionViewDetailOnly($id_bumdes)
    {
        $dataProvider = BumdesBersamaDesaAnggota::findOne(['kd_desa' => Yii::$app->user->identity->id, 'kd_kabupaten' => substr(Yii::$app->user->identity->id, 0, 4), 'id_bb' => $id_bumdes]);
        return $this->renderAjax('view-detail-only', [
            'model' => $dataProvider
        ]);
    }

    /**
     * Creates a new BumdesBersama model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $data = Yii::$app->request->get();
        $model = new BumdesBersama();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    public function actionManual()
    {
        $model = new BumdesBersama();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo json_encode(['id_bb' => $model->id_bb, 'status' => 1]);
            } else {
                echo json_encode(['status' => 0]);
            }
        } else {
            return $this->renderAjax('create-manual', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersama model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @return mixed
     */
    public function actionUpdate($kd_kabupaten, $id_bb)
    {
        $data = Yii::$app->request->get();
        $model = $this->findModel($kd_kabupaten, $id_bb);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersama model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @return mixed
     */
    public function actionDelete($kd_kabupaten, $id_bb)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersama model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @return BumdesBersama the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb)
    {
        if (($model = BumdesBersama::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
