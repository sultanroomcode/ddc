<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaDesaAnggota;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaDesaAnggotaController implements the CRUD actions for BumdesBersamaDesaAnggota model.
 */
class BumdesBersamaDesaAnggotaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaDesaAnggota models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaDesaAnggota::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaDesaAnggota model.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $kd_desa
     * @return mixed
     */
    public function actionView($kd_kabupaten, $id_bb, $kd_desa)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $kd_desa),
        ]);
    }

    /**
     * Creates a new BumdesBersamaDesaAnggota model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_kabupaten, $id_bb)
    {
        $model = new BumdesBersamaDesaAnggota();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaDesaAnggota model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $kd_kabupaten
     * @return mixed
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $kd_desa)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $kd_desa);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'kd_desa' => $kd_desa];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaDesaAnggota model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $kd_desa
     * @return mixed
     */
    public function actionDelete($kd_kabupaten, $id_bb, $kd_desa)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $kd_desa)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersamaDesaAnggota model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param string $kd_desa
     * @return BumdesBersamaDesaAnggota the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $kd_desa)
    {
        if (($model = BumdesBersamaDesaAnggota::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'kd_desa' => $kd_desa])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
