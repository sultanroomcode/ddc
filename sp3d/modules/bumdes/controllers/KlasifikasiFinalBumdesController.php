<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\KlasifikasiFinalBumdes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KlasifikasiFinalBumdesController implements the CRUD actions for KlasifikasiFinalBumdes model.
 */
class KlasifikasiFinalBumdesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KlasifikasiFinalBumdes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => KlasifikasiFinalBumdes::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KlasifikasiFinalBumdes model.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_bumdes)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_bumdes),
        ]);
    }

    /**
     * Creates a new KlasifikasiFinalBumdes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KlasifikasiFinalBumdes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KlasifikasiFinalBumdes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_bumdes)
    {
        $model = $this->findModel($kd_desa, $id_bumdes);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KlasifikasiFinalBumdes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_bumdes)
    {
        $this->findModel($kd_desa, $id_bumdes)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KlasifikasiFinalBumdes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @return KlasifikasiFinalBumdes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_bumdes)
    {
        if (($model = KlasifikasiFinalBumdes::findOne(['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
