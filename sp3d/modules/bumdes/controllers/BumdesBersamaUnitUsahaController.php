<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaUnitUsaha;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaUnitUsahaController implements the CRUD actions for BumdesBersamaUnitUsaha model.
 */
class BumdesBersamaUnitUsahaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaUnitUsaha models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaUnitUsaha::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaUnitUsaha model.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_uu
     * @return mixed
     */
    public function actionView($kd_kabupaten, $id_bb, $id_uu)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $id_uu),
        ]);
    }

    /**
     * Creates a new BumdesBersamaUnitUsaha model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_kabupaten, $id_bb)
    {
        $model = new BumdesBersamaUnitUsaha();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaUnitUsaha model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_uu
     * @return mixed
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $id_uu)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_uu);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_uu' => $id_uu];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaUnitUsaha model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_uu
     * @return mixed
     */
    public function actionDelete($kd_kabupaten, $id_bb, $id_uu)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_uu)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersamaUnitUsaha model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_uu
     * @return BumdesBersamaUnitUsaha the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $id_uu)
    {
        if (($model = BumdesBersamaUnitUsaha::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_uu' => $id_uu])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
