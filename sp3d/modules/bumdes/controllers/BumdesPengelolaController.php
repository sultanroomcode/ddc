<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesPengelola;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesPengelolaController implements the CRUD actions for BumdesPengelola model.
 */
class BumdesPengelolaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesPengelola models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesPengelola::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesPengelola model.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_pengelola
     * @return mixed
     */
    public function actionView($kd_desa, $id_bumdes, $id_pengelola)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_bumdes, $id_pengelola),
        ]);
    }

    /**
     * Creates a new BumdesPengelola model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_bumdes, $kd_desa, $provinsi=0)
    {
        $model = new BumdesPengelola();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'provinsi' => $provinsi];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesPengelola model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_pengelola
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id_bumdes, $id_pengelola, $provinsi=0)
    {
        $model = $this->findModel($kd_desa, $id_bumdes, $id_pengelola);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_pengelola' => $id_pengelola, 'provinsi' => $provinsi];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesPengelola model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_pengelola
     * @return mixed
     */
    public function actionDelete($kd_desa, $id_bumdes, $id_pengelola)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($kd_desa, $id_bumdes, $id_pengelola)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesPengelola model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_pengelola
     * @return BumdesPengelola the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_bumdes, $id_pengelola)
    {
        if (($model = BumdesPengelola::findOne(['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_pengelola' => $id_pengelola])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
