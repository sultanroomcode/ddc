<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesMedia;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesMediaController implements the CRUD actions for BumdesMedia model.
 */
class BumdesMediaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesMedia::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesMedia model.
     * @param integer $id_bumdes
     * @param integer $id_media
     * @param string $kd_desa
     * @return mixed
     */
    public function actionView($id_bumdes, $id_media, $kd_desa)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id_bumdes, $id_media, $kd_desa),
        ]);
    }

    /**
     * Creates a new BumdesMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_bumdes, $kd_desa, $provinsi=0)
    {
        $model = new BumdesMedia();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'provinsi' => $provinsi];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_bumdes
     * @param integer $id_media
     * @param string $kd_desa
     * @return mixed
     */
    public function actionUpdate($id_bumdes, $id_media, $kd_desa, $provinsi=0)
    {
        $model = $this->findModel($id_bumdes, $id_media, $kd_desa);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_media' => $id_media, 'provinsi' => $provinsi];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_bumdes
     * @param integer $id_media
     * @param string $kd_desa
     * @return mixed
     */
    public function actionDelete($id_bumdes, $id_media, $kd_desa)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($id_bumdes, $id_media, $kd_desa)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_bumdes
     * @param integer $id_media
     * @param string $kd_desa
     * @return BumdesMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_bumdes, $id_media, $kd_desa)
    {
        if (($model = BumdesMedia::findOne(['id_bumdes' => $id_bumdes, 'id_media' => $id_media, 'kd_desa' => $kd_desa])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
