<?php
namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\Bumdes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesController implements the CRUD actions for Bumdes model.
 */
class BumdesController extends Controller
{    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndexProvinsi()
    {
        $dataProvider = Bumdes::find();

        return $this->renderAjax('index-provinsi', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListReportBumdes()
    {
        return $this->renderAjax('list-report-bumdes');
    }

    public function actionListReport()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('list-report', [
            'data' => $data
        ]);
    }

    public function actionChartReport()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('chart-report', [
            'data' => $data
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = Bumdes::find();//->where(['created_by' => Yii::$app->user->identity->id]);
        if(isset(Yii::$app->user->identity->type) && Yii::$app->user->identity->type == 'desa'){
            $dataProvider->where(['kd_desa' => Yii::$app->user->identity->id]);
        } else {
            $dataProvider->where(['kd_desa' => Yii::$app->user->identity->desa->kd_desa]);
        }

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionView($kd_desa, $id_bumdes)
    {
    	$data = Yii::$app->request->get();
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_bumdes),
            'data' => $data
        ]);
    }

    public function actionViewOnly()
    {
        $dataProvider = Bumdes::find()->where(['kd_desa' => Yii::$app->user->identity->id]);
        return $this->renderAjax('view-only', [
            'model' => $dataProvider
        ]);
    }

    public function actionViewDetailOnly($id_bumdes)
    {
        $dataProvider = $this->findModel(Yii::$app->user->identity->id, $id_bumdes);
        return $this->renderAjax('view-detail-only', [
            'model' => $dataProvider
        ]);
    }

    public function actionViewDetailOnlyProvinsi($kd_desa, $id_bumdes)
    {
        $dataProvider = $this->findModel($kd_desa, $id_bumdes);
        return $this->renderAjax('view-detail-only', [
            'model' => $dataProvider
        ]);
    }

    public function actionManual()
    {
        $model = new Bumdes();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                echo json_encode(['id_bumdes' => $model->id_bumdes, 'status' => 1]);
            } else {
                echo json_encode(['status' => 0]);
            }
        } else {
            return $this->renderAjax('create-manual', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate()
    {
    	$data = Yii::$app->request->get();
        $model = new Bumdes();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    public function actionUpdate($kd_desa, $id_bumdes)
    {
    	$data = Yii::$app->request->get();
        $model = $this->findModel($kd_desa, $id_bumdes);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    public function actionDelete($kd_desa, $id_bumdes)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($kd_desa, $id_bumdes)->delete();

        if($model) echo 1; else echo 0;
    }

    protected function findModel($kd_desa, $id_bumdes)
    {
        if (($model = Bumdes::findOne(['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
