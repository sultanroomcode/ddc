<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaPendapatan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaPendapatanController implements the CRUD actions for BumdesBersamaPendapatan model.
 */
class BumdesBersamaPendapatanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaPendapatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaPendapatan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaPendapatan model.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return mixed
     */
    public function actionView($kd_desa, $id, $tahun)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_desa, $id, $tahun),
        ]);
    }

    /**
     * Creates a new BumdesBersamaPendapatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BumdesBersamaPendapatan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'tahun' => $model->tahun]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaPendapatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id, $tahun)
    {
        $model = $this->findModel($kd_desa, $id, $tahun);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'tahun' => $model->tahun]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaPendapatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return mixed
     */
    public function actionDelete($kd_desa, $id, $tahun)
    {
        $this->findModel($kd_desa, $id, $tahun)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BumdesBersamaPendapatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id
     * @param string $tahun
     * @return BumdesBersamaPendapatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id, $tahun)
    {
        if (($model = BumdesBersamaPendapatan::findOne(['kd_desa' => $kd_desa, 'id' => $id, 'tahun' => $tahun])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
