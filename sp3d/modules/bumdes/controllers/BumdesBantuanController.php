<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBantuan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBantuanController implements the CRUD actions for BumdesBantuan model.
 */
class BumdesBantuanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBantuan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBantuan::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBantuan model.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_bantuan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_desa, $id_bumdes, $id_bantuan)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_bumdes, $id_bantuan),
        ]);
    }

    /**
     * Creates a new BumdesBantuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_desa, $id_bumdes, $provinsi=0)
    {
        $model = new BumdesBantuan();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'provinsi' => $provinsi];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBantuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_bantuan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_desa, $id_bumdes, $id_bantuan, $provinsi=0)
    {
        $model = $this->findModel($kd_desa, $id_bumdes, $id_bantuan);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_bantuan' => $id_bantuan, 'provinsi' => $provinsi];

            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBantuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_bantuan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_desa, $id_bumdes, $id_bantuan)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($kd_desa, $id_bumdes, $id_bantuan)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBantuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_bantuan
     * @return BumdesBantuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_bumdes, $id_bantuan)
    {
        if (($model = BumdesBantuan::findOne(['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_bantuan' => $id_bantuan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
