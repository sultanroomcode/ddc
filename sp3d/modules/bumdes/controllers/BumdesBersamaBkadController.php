<?php
namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaBkad;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaBkadController implements the CRUD actions for BumdesBersamaBkad model.
 */
class BumdesBersamaBkadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaBkad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaBkad::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaBkad model.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_anggota
     * @return mixed
     */
    public function actionView($kd_kabupaten, $id_bb, $id_anggota)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $id_anggota),
        ]);
    }

    /**
     * Creates a new BumdesBersamaBkad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_kabupaten, $id_bb)
    {
        $model = new BumdesBersamaBkad();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb];

            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaBkad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_anggota
     * @return mixed
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $id_anggota)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_anggota);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_anggota' =>  $id_anggota];

            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaBkad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_anggota
     * @return mixed
     */
    public function actionDelete($kd_kabupaten, $id_bb, $id_anggota)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_anggota)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersamaBkad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_anggota
     * @return BumdesBersamaBkad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $id_anggota)
    {
        if (($model = BumdesBersamaBkad::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_anggota' => $id_anggota])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
