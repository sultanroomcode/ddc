<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesUnitUsahaController implements the CRUD actions for BumdesUnitUsaha model.
 */
class BumdesUnitUsahaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesUnitUsaha models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesUnitUsaha::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesUnitUsaha model.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_uu
     * @return mixed
     */
    public function actionView($kd_desa, $id_bumdes, $id_uu)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_desa, $id_bumdes, $id_uu),
        ]);
    }

    /**
     * Creates a new BumdesUnitUsaha model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_bumdes, $kd_desa, $provinsi=0)
    {
        $model = new BumdesUnitUsaha();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'provinsi' => $provinsi];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesUnitUsaha model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_uu
     * @return mixed
     */
    public function actionUpdate($kd_desa, $id_bumdes, $id_uu, $provinsi=0)
    {
        $model = $this->findModel($kd_desa, $id_bumdes, $id_uu);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_uu' => $id_uu, 'provinsi' => $provinsi];

            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesUnitUsaha model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_uu
     * @return mixed
     */
    public function actionDelete($kd_desa, $id_bumdes, $id_uu)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($kd_desa, $id_bumdes, $id_uu)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesUnitUsaha model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_desa
     * @param integer $id_bumdes
     * @param integer $id_uu
     * @return BumdesUnitUsaha the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_desa, $id_bumdes, $id_uu)
    {
        if (($model = BumdesUnitUsaha::findOne(['kd_desa' => $kd_desa, 'id_bumdes' => $id_bumdes, 'id_uu' => $id_uu])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
