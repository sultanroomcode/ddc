<?php

namespace sp3d\modules\bumdes\controllers;

use Yii;
use sp3d\modules\bumdes\models\BumdesBersamaPengawas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesBersamaPengawasController implements the CRUD actions for BumdesBersamaPengawas model.
 */
class BumdesBersamaPengawasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BumdesBersamaPengawas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BumdesBersamaPengawas::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BumdesBersamaPengawas model.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_pengawas
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_kabupaten, $id_bb, $id_pengawas)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($kd_kabupaten, $id_bb, $id_pengawas),
        ]);
    }

    /**
     * Creates a new BumdesBersamaPengawas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kd_kabupaten, $id_bb)
    {
        $model = new BumdesBersamaPengawas();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb];
            return $this->renderAjax('create', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Updates an existing BumdesBersamaPengawas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_pengawas
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_kabupaten, $id_bb, $id_pengawas)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_pengawas);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) echo 1; else echo 0;
        } else {
            $data = ['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_pengawas' => $id_pengawas];
            return $this->renderAjax('update', [
                'model' => $model,
                'data' => $data
            ]);
        }
    }

    /**
     * Deletes an existing BumdesBersamaPengawas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_pengawas
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_kabupaten, $id_bb, $id_pengawas)
    {
        $model = $this->findModel($kd_kabupaten, $id_bb, $id_pengawas)->delete();

        if($model) echo 1; else echo 0;
    }

    /**
     * Finds the BumdesBersamaPengawas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_kabupaten
     * @param string $id_bb
     * @param integer $id_pengawas
     * @return BumdesBersamaPengawas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_kabupaten, $id_bb, $id_pengawas)
    {
        if (($model = BumdesBersamaPengawas::findOne(['kd_kabupaten' => $kd_kabupaten, 'id_bb' => $id_bb, 'id_pengawas' => $id_pengawas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
