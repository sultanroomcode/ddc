<?php

namespace sp3d\modules\bumdes;

/**
 * bumdes module definition class
 */
class Bumdes extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'sp3d\modules\bumdes\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
