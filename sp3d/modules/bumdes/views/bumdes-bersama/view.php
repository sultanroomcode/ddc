<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersama */

$this->title = $model->nama_bb;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersamas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$ext_url = '';
$url = '/bumdes/bumdes-bersama/index';
$urlback = 'goLoad({elm:\'#bumdes-area\', url:\''.$url.'\'})';
if(isset($data['provinsi']) && $data['provinsi'] == 1){
    $ext_url = '&provinsi=1';
    $url = '/bumdes/bumdes-bersama/index-provinsi';
    $urlback = 'goLoad({url:\''.$url.'\'})';
}

?>
<div class="bumdes-bersama-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p><?= '<a href="javascript:void(0)" onclick="'.$urlback.'" class="btn btn-success"><i class="fa fa-backward"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-bersama/update?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>' ?></p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'id_bb',
            'nama_bb',
            'tahun_berdiri',
            'alamat',
            'legal_bkad_no',
            'legal_perkades_bersama_no',
            'legal_skdes_bersama_no',
            'creator.detail.nama',
            'updater.detail.nama',
            'created_at',
            'updated_at',
        ],
    ]) ?>


    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({welm:\'600px\', url:\'/bumdes/bumdes-bersama-bkad/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Struktur BKAD</a>' ?>
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Pendidikan</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->bkad as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->pendidikan ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-bkad/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_anggota='.$v->id_anggota ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-bkad/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_anggota='.$v->id_anggota ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-bkad/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_anggota='.$v->id_anggota ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-pengawas/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Struktur Pengawas BUMdesa Bersama</a>' ?>
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Pendidikan</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->pengawas as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->pendidikan ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-pengawas/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_pengawas='.$v->id_pengawas ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-pengawas/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_pengawas='.$v->id_pengawas ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-pengawas/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_pengawas='.$v->id_pengawas ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-pengelola/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Struktur Pengurus BUMdesa Bersama</a>' ?>
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Pendidikan</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->pengelola as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->pendidikan ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-pengelola/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_pengelola='.$v->id_pengelola ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-pengelola/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_pengelola='.$v->id_pengelola ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-pengelola/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_pengelola='.$v->id_pengelola ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-unit-usaha/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Unit Usaha</a>' ?>
            <table class="table">
            <tr>
                <th>Unit Usaha</th>
                <th>Omset</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->unitUsaha as $v): ?>
                <tr>
                    <td><?= $v->unit_usaha ?></td>
                    <td><?= $v->bh_status ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-unit-usaha/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_uu='.$v->id_uu ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-unit-usaha/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_uu='.$v->id_uu ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-unit-usaha/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_uu='.$v->id_uu ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-komoditas/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Komoditas</a>' ?>
            <table class="table">
            <tr>
                <th>Nama Produk</th>
                <th>Jumlah</th>
                <th>Satuan</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->komoditas as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->nf($v->jumlah) ?></td>
                    <td><?= $v->satuan ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-komoditas/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_komoditas='.$v->id_komoditas ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-komoditas/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_komoditas='.$v->id_komoditas ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-komoditas/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_komoditas='.$v->id_komoditas ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-potensi-desa/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Potensi</a>' ?>
            <table class="table">
            <tr>
                <th>ID</th>
                <th>Kategori</th>
                <th>Jenis</th>
                <th>Luas</th>
                <th>Unit</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->potensi as $v): ?>
                <tr>
                    <td><?= $v->id_potensi ?></td>
                    <td><?= $v->kategori ?></td>
                    <td><?= $v->jenis ?></td>
                    <td><?= $v->nf($v->luas) ?></td>
                    <td><?= $v->nf($v->unit) ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-potensi-desa/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_potensi='.$v->id_potensi ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-potensi-desa/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_potensi='.$v->id_potensi ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-potensi-desa/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_potensi='.$v->id_potensi ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-modal/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Modal Bumdes Bersama dan Perkembangannya</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Modal</th>
                <th>Sumber Modal Desa</th>
                <th>Sumber Modal Masyarakat</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->modal as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->modal ?></td>
                    <td><?= $v->sb_modal_desa ?></td>
                    <td><?= $v->sb_modal_masyarakat ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-modal/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&tahun='.$v->tahun ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-modal/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&tahun='.$v->tahun ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-modal/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&tahun='.$v->tahun ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-desa-anggota/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Desa Anggota BKAD</a>' ?>
            <table class="table">
            <tr>
                <th>Desa</th>
                <th>Kecamatan</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->desaAnggota as $v): ?>
                <tr>
                    <td><?= $v->desa->description ?></td>
                    <td><?= $v->kecamatan->description ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-desa-anggota/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&kd_desa='.$v->kd_desa ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>                        
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-desa-anggota/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&kd_desa='.$v->kd_desa ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bersama-media/create?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Foto Unit Usaha dan Link Video Kreatif Youtube</a>' ?>
            <table class="table">
            <tr>
                <th>Media</th>
                <th>Tipe</th>
                <th>Keterangan</th>
                <th align="right">Aksi</th>
            </tr>
            <?php foreach($model->media as $v): ?>
                <tr>
                    <td><?= $v->id_media ?></td>
                    <td><?= $v->tipe ?></td>
                    <td><?= $v->keterangan ?></td>
                    <td align="right">
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-media/view?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_media='.$v->id_media ?>'})" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bersama-media/update?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_media='.$v->id_media ?>'})" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bersama-media/delete?<?='kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&id_media='.$v->id_media ?>', 'Berhasil Hapus Data')" class="btn btn-success"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>
    .
</div>
<?php
$urlBack = '/bumdes/bumdes-bersama/view?kd_kabupaten='.$model->kd_kabupaten.'&id_bb='.$model->id_bb;
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#bumdes-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);