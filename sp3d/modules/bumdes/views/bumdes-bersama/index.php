<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Bersama';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-index">
    <p>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#bumdes-area', url:'/bumdes/bumdes-bersama/create'})" class="btn btn-success">Data Baru</a>
    </p>

    <table class="table">
        <tr>
            <th>Nama Bumdes</th>
            <th>Tahun Berdiri</th>
            <th>Alamat</th>
            <th>BKAD</th>
            <th>Perkades</th>
            <th>SKDes</th>
            <th>Aksi</th>
        </tr>
    <?php 
    foreach ($dataProvider->all() as $v): ?>
        <tr>
            <td><?= $v->nama_bb ?></td>
            <td><?= $v->tahun_berdiri ?></td>
            <td><?= $v->alamat ?></td>
            <td><?= $v->legal_bkad_no ?></td>
            <td><?= $v->legal_perkades_bersama_no ?></td>
            <td><?= $v->legal_skdes_bersama_no ?></td>
            <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-bersama/update?kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-bersama/view?kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
