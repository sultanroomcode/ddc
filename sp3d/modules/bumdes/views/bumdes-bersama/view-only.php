<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\Bumdes */
$this->params['breadcrumbs'][] = ['label' => 'Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-index">
    <table class="table">
        <tr>
            <th>Nama Bumdes</th>
            <th>Tahun Berdiri</th>
            <th>Alamat</th>
            <th>Perdes</th>
            <th>SKDes</th>
            <th>Aksi</th>
        </tr>
    <?php 
    // echo $model->createCommand()->getRawSql();
    foreach ($model->all() as $v): ?>
        <tr>
            <td><?= $v->bumdesbersama->nama_bb ?></td>
            <td><?= $v->bumdesbersama->tahun_berdiri ?></td>
            <td><?= $v->bumdesbersama->alamat ?></td>
            <td><?= $v->bumdesbersama->legal_perkades_bersama_no ?></td>
            <td><?= $v->bumdesbersama->legal_skdes_bersama_no ?></td>
            <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-bersama/view-detail-only?id_bumdes='.$v->id_bb.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
