<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersama */

$this->title = 'Data BUMdesa Bersama';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersamas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
