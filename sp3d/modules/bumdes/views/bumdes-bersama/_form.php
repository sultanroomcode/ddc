<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersama */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_kabupaten = substr(Yii::$app->user->identity->desa->kd_desa, 0, 4);
}

$url = '/bumdes/bumdes-bersama/index';
$urlback = 'goLoad({elm:\'#bumdes-area\', url:\''.$url.'\'})';
if(isset($data['provinsi']) && $data['provinsi'] == 1){
    $ext_url = '&provinsi=1';
    $url = '/bumdes/bumdes-bersama/index-provinsi';
    $urlback = 'goLoad({url:\''.$url.'\'})';
}
?>

<div class="bumdes-bersama-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'nama_bb')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tahun_berdiri')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>
        
            <fieldset>
                <legend>Legalitas BKAD dan BUMdesa Bersama</legend>
                <?= $form->field($model, 'legal_bkad_no')->textInput(['maxlength' => true])->label('Nomor Peraturan Bersama Kepala Desa Tentang Pembentukan BKAD') ?>

                <?= $form->field($model, 'legal_perkades_bersama_no')->textInput(['maxlength' => true])->label('Nomor Peraturan Bersama Kepala Desa') ?>

                <?= $form->field($model, 'legal_skdes_bersama_no')->textInput(['maxlength' => true])->label('Nomor SK Bersama Kepala Desa Tentang Pengurus BUMdesa Bersama') ?>
            </fieldset>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);