<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use sp3d\models\User;
use yii\helpers\ArrayHelper;

$listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
?>

<div class="bumdes-bersama-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>    

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'kd_kabupaten')->dropdownList($listArr) ?>

            <?= $form->field($model, 'nama_bb')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tahun_berdiri')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>
        
            <fieldset>
                <legend>Legalitas BKAD dan BUMdesa Bersama</legend>
                <?= $form->field($model, 'legal_bkad_no')->textInput(['maxlength' => true])->label('Nomor Peraturan Bersama Kepala Desa Tentang Pembentukan BKAD') ?>

                <?= $form->field($model, 'legal_perkades_bersama_no')->textInput(['maxlength' => true])->label('Nomor Peraturan Bersama Kepala Desa') ?>

                <?= $form->field($model, 'legal_skdes_bersama_no')->textInput(['maxlength' => true])->label('Nomor SK Bersama Kepala Desa Tentang Pengurus BUMdesa Bersama') ?>
            </fieldset>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-bersama/index\'})" class="btn btn-success"><i class="fa fa-backward"></i></a>' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        res = JSON.parse(res);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({url : '/bumdes/bumdes-bersama/view?provinsi=1&kd_kabupaten='+$('#bumdesbersama-kd_kabupaten').val()+'&id_bb='+res.id_bb});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);