<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersama */

$this->title = 'Update : ' . $model->nama_bb;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersamas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_kabupaten, 'url' => ['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
