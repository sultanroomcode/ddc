<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$bb = $model->bumdesbersama;
?>
<div class="bumdes-index">
    <?= DetailView::widget([
        'model' => $bb,
        'attributes' => [
            'kd_kabupaten',
            'id_bb',
            'nama_bb',
            'tahun_berdiri',
            'alamat',
            'legal_bkad_no',
            'legal_perkades_bersama_no',
            'legal_skdes_bersama_no',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>
    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Pendidikan</th>
            </tr>
            <?php foreach($bb->bkad as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->pendidikan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Desa</th>
                <th>Kecamatan</th>
            </tr>
            <?php foreach($bb->desaAnggota as $v): ?>
                <tr>
                    <td><?= $v->kd_desa ?></td>
                    <td><?= $v->kd_kecamatan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Pendidikan</th>
            </tr>
            <?php foreach($bb->pengelola as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->pendidikan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Media</th>
                <th>Tipe</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($bb->media as $v): ?>
                <tr>
                    <td><?= $v->id_media ?></td>
                    <td><?= $v->tipe ?></td>
                    <td><?= $v->keterangan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Modal</th>
                <th>Sumber Modal Desa</th>
                <th>Sumber Modal Masyarakat</th>
            </tr>
            <?php foreach($bb->modal as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->modal ?></td>
                    <td><?= $v->sb_modal_desa ?></td>
                    <td><?= $v->sb_modal_masyarakat ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Unit Usaha</th>
                <th>Omset</th>
            </tr>
            <?php foreach($bb->unitUsaha as $v): ?>
                <tr>
                    <td><?= $v->unit_usaha ?></td>
                    <td><?= $v->bh_status ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
.
