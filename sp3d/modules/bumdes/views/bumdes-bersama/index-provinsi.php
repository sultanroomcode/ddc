<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Bersama';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title"><?= $this->title ?></h2>
            <div class="p-10" id="bumdes-area">
                <table class="table">
                    <tr>
                        <th>Nama Bumdes</th>
                        <th>Tahun Berdiri</th>
                        <th>Alamat</th>
                        <th>BKAD</th>
                        <th>Perkades</th>
                        <th>SKDes</th>
                        <th>Aksi</th>
                    </tr>
                <?php 
                foreach ($dataProvider->all() as $v): ?>
                    <tr>
                        <td><?= $v->nama_bb ?></td>
                        <td><?= $v->tahun_berdiri ?></td>
                        <td><?= $v->alamat ?></td>
                        <td><?= $v->legal_bkad_no ?></td>
                        <td><?= $v->legal_perkades_bersama_no ?></td>
                        <td><?= $v->legal_skdes_bersama_no ?></td>
                        <td><?= ((Yii::$app->user->identity->type == 'provinsi' && Yii::$app->user->identity->id == $v->created_by)?'<a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-bersama/update?kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&provinsi=1\'})" class="btn btn-success">Edit</a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-bersama/view?kd_kabupaten='.$v->kd_kabupaten.'&id_bb='.$v->id_bb.'&provinsi=1\'})" class="btn btn-success">Isi</a>':'') ?></td>
                    </tr>
                <?php endforeach; ?>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
