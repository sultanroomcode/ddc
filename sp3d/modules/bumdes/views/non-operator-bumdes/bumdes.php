<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Bumdes</h2>
            <div class="row">
            	
            </div>
            <div class="p-10" id="bumdes-area">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$kd_desa = Yii::$app->user->identity->id;
$bumdes_url = ($bumdes == 'bersama')?'/bumdes/bumdes-bersama/view-only?kd_desa='.$kd_desa:'/bumdes/bumdes/view-only?kd_desa='.$kd_desa;
$scripts = <<<JS
goLoad({elm:'#bumdes-area', url:'{$bumdes_url}'});
JS;

$this->registerJs($scripts);