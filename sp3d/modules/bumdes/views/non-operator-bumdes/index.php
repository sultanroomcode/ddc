<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Bumdes</h2>
            <div class="row">
            	<div class="col-md-3">
            		<select onchange="goLoad({elm:'#bumdes-area', url:'/bumdes/non-operator-bumdes/bumdes?j='+this.value});" class="form-control" name="bumdes-opt" id="bumdes-opt">
		            	<option value="mandiri">Mandiri</option>
		            	<option value="bersama">Bersama</option>
		            </select>
            	</div>
            </div>
            <div class="p-10" id="bumdes-area">
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>