<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Dampaks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-dampak-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bumdes Dampak', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_bumdes',
            'no',
            //'dampak_ekonomi',
            //'dampak_sosial',
            //'dampak_pembangunan_desa',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
