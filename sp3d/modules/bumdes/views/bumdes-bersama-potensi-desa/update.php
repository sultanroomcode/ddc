<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaPotensiDesa */

$this->title = 'Update Potensi Desa: ' . $model->jenis;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Potensi Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id' => $model->id, 'id_potensi' => $model->id_potensi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-potensi-desa-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
