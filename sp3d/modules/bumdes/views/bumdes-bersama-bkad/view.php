<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaBkad */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Bkads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-bkad-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'id_bb',
            'id_anggota',
            'nama',
            'alamat',
            'pendidikan',
            'jabatan',
            'pekerjaan',
            'hp',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
