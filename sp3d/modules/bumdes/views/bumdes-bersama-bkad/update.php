<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaBkad */

$this->title = 'Update Anggota BKAD Bumdes Bersama : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Bkads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_kabupaten, 'url' => ['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'id_anggota' => $model->id_anggota]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-bkad-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
