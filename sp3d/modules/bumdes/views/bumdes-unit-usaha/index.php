<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Unit Usahas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-unit-usaha-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bumdes Unit Usaha', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'id_bumdes',
            'id_uu',
            'unit_usaha',
            'omset',
            // 'bh_status',
            // 'bh_bentuk',
            // 'bh_no',
            // 'jml_karyawan',
            // 'keuntungan',
            // 'status',
            // 'keterangan',
            // 'created_by',
            // 'updated_by',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
