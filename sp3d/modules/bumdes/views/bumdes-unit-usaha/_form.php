<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use sp3d\modules\bumdes\models\BumdesTipeUnitUsaha;

$arrFormConfig = [
'id' => $model->formName(), 
'options' => ['enctype' => 'multipart/form-data'],
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesUnitUsaha */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = ($data['provinsi'] == 1)?$data['kd_desa']:Yii::$app->user->identity->desa->kd_desa;
    $model->id_bumdes = $data['id_bumdes'];
    $model->genNum();//generate number
}

$arrTipe = ArrayHelper::map(BumdesTipeUnitUsaha::find()->orderBy('order')->all(), 'id', 'title');

if($data['provinsi'] == 1){
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'].'&provinsi=1';
} else {
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'];
}
?>

<div class="bumdes-unit-usaha-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_bumdes')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'id_uu')->hiddenInput()->label(false) ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'unit_usaha')->dropdownList($arrTipe) ?>

        
            <fieldset>
                <legend>Badan Hukum BUMDesa</legend>
                <?= $form->field($model, 'bh_status')->dropdownList(['sudah' => 'Memiliki', 'belum' => 'Belum Memiliki']) ?>

                <?= $form->field($model, 'bh_bentuk')->dropdownList(['-' => 'Belum', 'ojk' => 'Otoritas Jasa Keuangan/LKM', 'pt' => 'PT']) ?>
                
                <?= $form->field($model, 'bh_no')->textInput(['maxlength' => true]) ?>
            </fieldset>

            <?= $form->field($model, 'jml_karyawan')->textInput() ?>

            <?= $form->field($model, 'status')->dropdownList(['aktif' => 'Aktif', 'nonaktif' => 'Non-Aktif']) ?>
            
            <?= $form->field($model, 'afoto_src')->fileInput(['maxlength' => true]) ?>

            <?php // $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <progress id="prog" max="100" value="0" style="display:none;"></progress>
            <div id="percent"></div>

            <div class="progress">
              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><b class="prog-text"></b>
                <span class="sr-only" class="prog-text"></span>
              </div>
            </div>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
var bar = $('.progress-bar');
var percent = $('.prog-text');

$('form#{$model->formName()}').ajaxForm({
    beforeSend: function() {
        var percentVal = 0;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : '0%'}); 
        percent.html(percentVal+' %');
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    success: function() {
        var percentVal = 100;
        bar.attr('aria-valuenow',percentVal); 
        bar.css({'width' : percentVal+'%'}); 
        percent.html(percentVal+' %');
    },
    complete: function(xhr) {
        if(xhr.responseText == 1){
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#bumdes-area', url : '{$urlback}'});
        } else {
            $.alert('Gagal menyimpan data');
        }
    }
}); 
JS;

$this->registerJs($script);