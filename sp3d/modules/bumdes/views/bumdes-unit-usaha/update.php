<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesUnitUsaha */

$this->title = 'Update Unit Usaha BUMDesa : ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Unit Usahas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'id_uu' => $model->id_uu]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-unit-usaha-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
