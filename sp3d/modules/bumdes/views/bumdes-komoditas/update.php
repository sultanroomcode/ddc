<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesKomoditas */

$this->title = 'Update Produk Unggulan Desa : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Komoditas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'id_komoditas' => $model->id_komoditas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-komoditas-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
