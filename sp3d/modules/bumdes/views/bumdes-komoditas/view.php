<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesKomoditas */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Komoditas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-komoditas-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'kd_kecamatan',
            'kd_desa',
            'id_bumdes',
            'id_komoditas',
            'nama',
            'uraian',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
