<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPemasaranProduk */

$this->title = 'Pemasaran Produk/Jasa BUMDesa';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Pemasaran Produks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-pemasaran-produk-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
