<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPemasaranProduk */

$this->title = $model->lokasi_pemasaran;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Pemasaran Produks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-pemasaran-produk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_bumdes',
            'no',
            'lokasi_pemasaran',
            'keterangan:ntext',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
