<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use sp3d\models\transaksi\Sp3dMasterJabatan;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_kabupaten = substr(Yii::$app->user->identity->desa->kd_desa, 0, 4);
    $model->id_bb = $data['id_bb'];
    $model->genNum();//generate number
}

$jabatanList = ArrayHelper::map(Sp3dMasterJabatan::find()->where(['tipe' => 'pbumdes', 'status' => '10'])->orderBy('urut')->all(), 'id', 'nama_jabatan');
$urlback = '/bumdes/bumdes-bersama/view?kd_kabupaten='.$data['kd_kabupaten'].'&id_bb='.$data['id_bb'];
/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaPengawas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bumdes-bersama-pengawas-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_bb')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_pengawas')->hiddenInput()->label(false) ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>

            <?= $form->field($model, 'pendidikan')->dropdownList([
                '0|SD' => 'Sekolah Dasar', 
                '1|SMP' => 'Sekolah Menengah Pertama', 
                '2|SMA' => 'Sekolah Menengah Atas', 
                '3|D1' => 'Diploma I', 
                '4|D2' => 'Diploma II', 
                '5|D3' => 'Diploma III', 
                '6|D4' => 'Diploma IV', 
                '7|S1' => 'Sarjana', 
                '8|S2' => 'Magister', 
                '9|S3' => 'Doktoral', 
            ]) ?>

            <?= $form->field($model, 'jabatan')->dropdownList($jabatanList) ?>

            <?= $form->field($model, 'pekerjaan')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'hp')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#bumdes-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);