<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaPengawas */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Pengawas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-pengawas-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'id_bb',
            'id_pengawas',
            'nama',
            'alamat',
            'pendidikan',
            'jabatan',
            'pekerjaan',
            'hp',
            'tipe',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
