<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaPengawas */

$this->title = 'Update Pengawas : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Pengawas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_kabupaten, 'url' => ['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'id_pengawas' => $model->id_pengawas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-pengawas-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
