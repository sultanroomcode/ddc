<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Pembukuans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-pembukuan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bumdes Pembukuan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_bumdes',
            'no',
            //'uraian',
            //'status_ada',
            //'status_benar',
            //'pelaporan',
            //'bentuk_laporan',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
