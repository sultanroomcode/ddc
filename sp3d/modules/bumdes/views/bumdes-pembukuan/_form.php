<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$arr = [
    'mutasi-kas' => 'Mutasi Kas',
    'laporan-laba-rugi' => 'Laporan Raba Rugi',
    'neraca' => 'Neraca',
    'laporan-keuangan-konsolidasi' => 'Laporan Keuangan Konsolidasi',
];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPotensiDesa */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_desa = ($data['provinsi'] == 1)?$data['kd_desa']:Yii::$app->user->identity->desa->kd_desa;
    $model->kd_kabupaten = substr($model->kd_desa, 0, 4);
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->id_bumdes = $data['id_bumdes'];
    $model->genNum();//generate number
}

if($data['provinsi'] == 1){
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'].'&provinsi=1';
} else {
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'];
}
?>

<div class="bumdes-pembukuan-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_bumdes')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'no')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'uraian')->dropdownList($arr) ?>

    <?= $form->field($model, 'status_ada')->dropdownList(['tidak' => 'Tidak Ada', 'ada' => 'Ada']) ?>

    <?= $form->field($model, 'status_benar')->dropdownList(['tidak' => 'Tidak Benar', 'benar' => 'Benar']) ?>

    <?= $form->field($model, 'pelaporan')->dropdownList(['belum' => 'Belum', 'rutin' => 'Rutin']) ?>

    <?= $form->field($model, 'bentuk_laporan')->dropdownList(['manual' => 'Manual', 'berbasis-komputer' => 'Berbasis Komputer']) ?>

    <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-7">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#bumdes-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);