<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Bumdes Manual</h2>
            <br>
            <a href="javascript:void(0)" class="btn btn-danger" onclick="goLoad({elm:'#bumdes-area', url:'/bumdes/bumdes/manual'})">Tambah Bumdes</a>
            <a href="javascript:void(0)" class="btn btn-danger" onclick="goLoad({elm:'#bumdes-area', url:'/bumdes/bumdes-bersama/manual'})">Tambah Bumdes Bersama</a>
            <div class="p-10" id="bumdes-area"></div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>