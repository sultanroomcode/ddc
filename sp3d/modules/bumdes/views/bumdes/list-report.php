<?php
use sp3d\models\User;
use sp3d\models\transaksi\Sp3dMasterJabatan;
use yii\helpers\ArrayHelper;
$id = Yii::$app->user->identity->id;
$tipeuser = Yii::$app->user->identity->type;
$arr = ['-' => ''];

$subjectArr = [
    'keuntungan' => 'Keuntungan',
    'modal' => 'Modal',
    'omset' => 'Omset',
    'tahun' => 'Tahun',
    'region' => 'Regional',
    'unit-usaha' => 'Unit Usaha',
];

if($tipeuser == 'provinsi'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');    
}

if($tipeuser == 'kabupaten'){
    $listArr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $id.'%', false])->orderBy('description ASC')->all(), 'id', 'description');    
}

$listArr = $arr + $listArr;
$data['objek'] = 'bumdes';
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">DAFTAR BUMDES Berdasarkan <?= $subjectArr[$data['subjek']] ?></h2>
            <div class="p-10">
                <div class="row">
                    <input type="hidden" id="objek" value="<?= $data['objek'] ?>">
                    <input type="hidden" id="jabatan" value="<?= $data['objek'] ?>">
                    <input type="hidden" id="subjek" value="<?= $data['subjek'] ?>">
                    <?php if($tipeuser == 'provinsi'){ ?>
                    <div class="col-md-2">
                        <select id="kabupaten" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } 
                    if($tipeuser == 'provinsi' || $tipeuser == 'kabupaten'){ ?>
                    <div class="col-md-2">
                    	<?php if($tipeuser == 'kabupaten'){ ?>
	                    	<input type="hidden" id="kabupaten" value="<?= substr($id, 0, 4) ?>">
	                    <?php } ?>
	                    
                        <select id="kecamatan" class="form-control">
                            <?php foreach($listArr as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php } else { 
                        echo '<input type="hidden" id="kabupaten" value="'.substr($id, 0, 4).'">'; 
                        echo '<input type="hidden" id="kecamatan" value="'.$id.'">'; 
                    }
                    ?>

                    <div class="col-md-3">
                        <select class="form-control" id="subjekval"></select>
                    </div>

                    <div class="col-md-3">
                        <button class="btn btn-danger" onclick="filteringAdd()">Filter</button>
                        <!-- <button class="btn btn-danger" onclick="filteringExport()">Export</button> -->
                    </div>
                </div>
            	<hr>
            	<div id="report-list-zona"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div id="modal-custom" style="display: none;">
    <div style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 20px;" id="form-engine">
    </div>
</div>
<?php
$scripts =<<<JS
var objek = $('#objek').val();
var subjek = $('#subjek').val();
var subjekval = $('#subjekval').val();
goLoad({elm:'#subjekval', url:'/sp3ddashboard/data-center/data-form-complete-bumdes?objek='+objek+'&subjek='+subjek});

$('#kabupaten').change(function(){
  var kode = $(this).val();
  $('#kecamatan, #desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan', url:'/sp3ddashboard/data-center/data-option?type=kecamatan&kode='+kode});
  }
});

$('#kecamatan').change(function(){
  var kode = $(this).val();
  $('#desa').html('<option value=""></option>');
  if(kode !== ''){
    goLoad({elm:'#desa', url:'/sp3ddashboard/data-center/data-option?type=desa&kode='+kode});
  }
});

function filteringAdd(){
    var jabatan = $('#jabatan').val();
    var subjek = $('#subjek').val();
    var subjekval = encodeURI($('#subjekval').val());
    var objek = $('#objek').val();
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    goLoad({elm:'#report-list-zona', url:'/sp3ddashboard/data-center/data-laporan?jabatan='+jabatan+'&objek='+objek+'&subjek='+subjek+'&subjekval='+subjekval+'&kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan});
}

function filteringExport(){
    var jabatan = $('#jabatan').val();
    var subjek = $('#subjek').val();
    var objek = $('#objek').val();
    var kecamatan = $('#kecamatan').val();
    var kabupaten = $('#kabupaten').val();
    if(kabupaten == '-' || kabupaten == null){
        kabupaten = '';
    }
    if(kecamatan == '-' || kecamatan == null){
        kecamatan = '';
    }
    window.open(base_url+'/sp3ddashboard/data-export/data-laporan?objek='+objek+'&subjek='+subjek+'&kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan, '_blank');
    // goLoad({elm:'#report-list-kades-zona', url:'/sp3ddashboard/data-export/data-kades?kabupaten-input='+kabupaten+'&kecamatan-input='+kecamatan});
}
JS;

$this->registerJs($scripts);