<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\Bumdes */

$this->title = 'Update Bumdes: ' . $model->nama_bumdes;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'data' => $data,
        'model' => $model,
    ]) ?>

</div>
