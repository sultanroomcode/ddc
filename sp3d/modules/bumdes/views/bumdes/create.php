<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\Bumdes */

$this->title = 'Data Bumdes';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'data' => $data,
        'model' => $model,
    ]) ?>

</div>
