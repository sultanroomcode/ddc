<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
	'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];
?>

<div class="bumdes-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <div class="row">
        <div class="col-md-5">
            <div id="form-external"></div>

            <?= $form->field($model, 'nama_bumdes')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tahun_berdiri')->textInput(['maxlength' => true]) ?>
        
            <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>

            <?= $form->field($model, 'legal_perdes_no')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'legal_skdes_bumdes_no')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropdownList(['aktif' => 'Aktif', 'tidak' => 'Tidak Aktif']) ?>
			
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-7">
					<?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>
			</div>
        </div>
    </div>
    
    <div class="form-group">
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
goLoad({elm:'#form-external', url:'/ddc/kab-kec-des-form/form'});
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        res = JSON.parse(res);
        if(res.status == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({url : '/bumdes/bumdes/view?provinsi=1&kd_desa='+$('#bumdes-kd_desa').val()+'&id_bumdes='+res.id_bumdes});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);
