<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
	'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

$url = '/bumdes/bumdes/index';
$urlback = 'goLoad({elm:\'#bumdes-area\', url:\''.$url.'\'})';
if(isset($data['provinsi']) && $data['provinsi'] == 1){
    $ext_url = '&provinsi=1';
    $url = '/bumdes/bumdes/index-provinsi';
    $urlback = 'goLoad({url:\''.$url.'\'})';
}


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\Bumdes */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->id_bumdes = time();
    $model->kd_desa = Yii::$app->user->identity->desa->kd_desa;
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
}
?>

<div class="bumdes-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_bumdes')->hiddenInput(['maxlength' => true])->label(false) ?>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'nama_bumdes')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tahun_berdiri')->textInput(['maxlength' => true]) ?>
        
            <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>

            <?= $form->field($model, 'legal_perdes_no')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'legal_skdes_bumdes_no')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropdownList(['aktif' => 'Aktif', 'tidak' => 'Tidak Aktif']) ?>
			
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-7">
					<?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			        <a href="javascript:void(0)" onclick="<?=$urlback?>" class="btn btn-success"><i class="fa fa-backward"></i></a>
				</div>
			</div>
        </div>
    </div>
    
    <div class="form-group">
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            $(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            {$urlback}
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);