<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\Bumdes */

$this->title = $model->nama_bumdes;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$ext_url = '';
$url = '/bumdes/bumdes/index';
$urlback = 'goLoad({elm:\'#bumdes-area\', url:\''.$url.'\'})';
if(isset($data['provinsi']) && $data['provinsi'] == 1){
    $ext_url = '&provinsi=1';
    $url = '/bumdes/bumdes/index-provinsi';
    $urlback = 'goLoad({url:\''.$url.'\'})';
}
?>
<div class="bumdes-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= '<a href="javascript:void(0)" onclick="'.$urlback.'" class="btn btn-success"><i class="fa fa-backward"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes/update?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success">Edit</a>' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kabupaten.description',
            'kecamatan.description',
            'desa.description',
            'kd_desa',
            // 'id_bumdes',
            'nama_bumdes',
            'klasifikasifinal.hasil_penilaian',
            'klasifikasifinal.nilai_akhir',
            'klasifikasifinal.nilai_akhir_kategori',
            'tahun_berdiri',
            'alamat',
            'legal_perdes_no',
            'legal_skdes_bumdes_no',
            'creator.detail.nama',
            // 'updater.detail.nama',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <!-- Struktur Penasehat/pengawas -->

    <div id="row">
        <div class="col-md-12">
        	<?php //var_dump($ext_url) ?>
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({wlem:\'900px\',url:\'/bumdes/bumdes-pengawas/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Struktur Penasehat &amp; Pengawas BUMDesa</a>' ?>
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Pendidikan</th>
                <th>Alamat</th>
                <th>No. HP</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pengawas as $v): ?>
                <?php $pendidikan = explode('|', $v->pendidikan); ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->tipejabatan->nama_jabatan ?></td>
                    <td><?= $pendidikan[1] ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->hp ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pengawas/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_pengawas='.$v->id_pengawas ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pengawas/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_pengawas='.$v->id_pengawas ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-pengawas/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_pengawas='.$v->id_pengawas ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Struktur Pengelola BumDes -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-pengelola/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Organisasi Pengelola BUMDesa</a>' ?>
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Pendidikan</th>
                <th>Alamat</th>
                <th>No. HP</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pengelola as $v): ?>
                <?php $pendidikan = explode('|', $v->pendidikan); ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->jabatan ?></td>
                    <td><?= $pendidikan[1] ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->hp ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pengelola/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_pengelola='.$v->id_pengelola ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pengelola/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_pengelola='.$v->id_pengelola ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-pengelola/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_pengelola='.$v->id_pengelola ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Program Kerja -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-program-kerja/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Program Kerja</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Uraian</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->proker as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->uraian ?></td>
                    <td><?= $v->keterangan ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-program-kerja/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-program-kerja/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-program-kerja/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>   

    <!-- Unit Usaha -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-unit-usaha/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Unit Usaha</a>' ?>
            <table class="table">
            <tr>
                <th>Unit Usaha</th>
                <th>Omset</th>
                <th>Keuntungan</th>
                <th>Status BUMDesa</th>
                <th>Bentuk BUMDesa</th>
                <th>Status Aktif</th>
                <th>Prioritas</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->unitUsaha as $v): ?>
                <tr>
                    <td><?= $v->tipe->title ?></td>
                    <td><?= $v->nf($v->omset) ?></td>
                    <td><?= $v->nf($v->keuntungan) ?></td>
                    <td><?= $v->bh_status ?></td>
                    <td><?= strtoupper($v->bh_bentuk) ?></td>
                    <td><?= $v->status ?></td>
                    <td><?= $v->status_prioritas ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-unit-usaha/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_uu='.$v->id_uu ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-unit-usaha/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_uu='.$v->id_uu ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-unit-usaha/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_uu='.$v->id_uu ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Modal dan Perkembangan Bumdes -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-modal/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Modal BUMDesa dan Perkembangannya</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Sumber Modal Desa</th>
                <th>Sumber Modal Masyarakat</th>
                <th>Total Modal</th>
                <th>Sumbangan PAD</th>
                <th>Keuntungan</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->modal as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->nf($v->sb_modal_desa) ?></td>
                    <td><?= $v->nf($v->sb_modal_masyarakat) ?></td>
                    <td><?= $v->nf($v->modal) ?></td>
                    <td><?= $v->nf($v->smb_pad) ?></td>
                    <td><?= $v->nf($v->keuntungan) ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-modal/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&tahun='.$v->tahun ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-modal/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&tahun='.$v->tahun ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-modal/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&tahun='.$v->tahun ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Bantuan untuk bumdes -->
    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-bantuan/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Bantuan/Hibah</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Nama</th>
                <th>Bantuan</th>
                <th>Nilai</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->bantuan as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->jenis_bantuan ?></td>
                    <td><?= $v->nf($v->nilai) ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bantuan/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_bantuan='.$v->id_bantuan ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-bantuan/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_bantuan='.$v->id_bantuan ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-bantuan/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_bantuan='.$v->id_bantuan ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Kerjasama bumdes dengan stakeholder -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-kerjasama/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Kerjasama</a>' ?>
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Nama</th>
                <th>MOU</th>
                <th>Bentuk Kerjasama</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->kerjasama as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->no_mou ?></td>
                    <td><?= $v->bentuk_kerjasama ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-kerjasama/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_kerjasama='.$v->id_kerjasama ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-kerjasama/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_kerjasama='.$v->id_kerjasama ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-kerjasama/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_kerjasama='.$v->id_kerjasama ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>    

    <!-- Pemasaran Produk -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-pemasaran-produk/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Pemasaran Produk</a>' ?>
            <table class="table">
            <tr>
                <th>Lokasi Pemasaran</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pemasaran as $v): ?>
                <tr>
                    <td><?= $v->lokasi_pemasaran ?></td>
                    <td><?= $v->keterangan ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pemasaran-produk/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pemasaran-produk/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-pemasaran-produk/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>   

    <!-- Pembukuan Bumdes -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-pembukuan/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Pembukuan</a>' ?>
            <table class="table">
            <tr>
                <th>Uraian</th>
                <th>Status Ada</th>
                <th>Status Benar</th>
                <th>Proses Pelaporan</th>
                <th>Bentuk Pelaporan</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pembukuan as $v): ?>
                <tr>
                    <td><?= $v->uraian ?></td>
                    <td><?= $v->status_ada ?></td>
                    <td><?= $v->status_benar ?></td>
                    <td><?= $v->pelaporan ?></td>
                    <td><?= $v->bentuk_laporan ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pembukuan/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pembukuan/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-pembukuan/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>     

    <!-- Pertanggungjawaban -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-pertanggungjawaban/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Pertanggungjawaban</a>' ?>
            <table class="table">
            <tr>
                <th>Uraian</th>
                <th>Status Lengkap</th>
                <th>Proses Tanggungjawab</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->pertanggungjawaban as $v): ?>
                <tr>
                    <td><?= $v->uraian ?></td>
                    <td><?= $v->status_lengkap ?></td>
                    <td><?= $v->proses_tgjwb ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pertanggungjawaban/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-pertanggungjawaban/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-pertanggungjawaban/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>    

    <!-- Dampak -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-dampak/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Dampak</a>' ?>
            <table class="table">
            <tr>
                <th>Dampak Sosial</th>
                <th>Dampak Ekonomi</th>
                <th>Dampak Pembangunan Desa</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->dampak as $v): ?>
                <tr>
                    <td><?= $v->dampak_sosial ?></td>
                    <td><?= $v->dampak_ekonomi ?></td>
                    <td><?= $v->dampak_pembangunan_desa ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-dampak/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-dampak/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-dampak/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&no='.$v->no ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>    

    <!-- Produk Unggulan DEsa -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-komoditas/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Produk Unggulan Desa</a>' ?>
            <table class="table">
            <tr>
                <th>Nama Produk</th>
                <th>Jumlah</th>
                <th>Satuan</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->komoditas as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->nf($v->jumlah) ?></td>
                    <td><?= $v->satuan ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-komoditas/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_komoditas='.$v->id_komoditas ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-komoditas/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_komoditas='.$v->id_komoditas ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-komoditas/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_komoditas='.$v->id_komoditas ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Potensi Desa -->
    <?php
    /*
    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-potensi-desa/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Potensi Desa</a>' ?>
            <table class="table">
            <tr>
                <th>ID</th>
                <th>Kategori</th>
                <th>Jenis</th>
                <th>Luas</th>
                <th>Unit</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->potensi as $v): ?>
                <tr>
                    <td><?= $v->id_potensi ?></td>
                    <td><?= $v->kategori ?></td>
                    <td><?= $v->jenis ?></td>
                    <td><?= $v->nf($v->luas) ?></td>
                    <td><?= $v->unit ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-potensi-desa/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_potensi='.$v->id_potensi ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-potensi-desa/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_potensi='.$v->id_potensi ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-potensi-desa/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_potensi='.$v->id_potensi ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>
    */
    ?>

    <!-- Foto Unit Usaha dan Link Video Kretif -->

    <div id="row">
        <div class="col-md-12">
            <?= '<a href="javascript:void(0)" onclick="openModalXyf({url:\'/bumdes/bumdes-media/create?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url.'\'})" class="btn btn-success"><i class="fa fa-plus"></i> Foto Unit Usaha dan Link Video Kreatif Youtube</a>' ?>
            <table class="table">
            <tr>
                <th>Media</th>
                <th>Tipe</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($model->media as $v): ?>
                <tr>
                    <td><?= $v->id_media ?></td>
                    <td><?= $v->tipe ?></td>
                    <td><?= $v->keterangan ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-media/view?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_media='.$v->id_media ?>'})" class="btn btn-success">Lihat</a>
                        <a href="javascript:void(0)" onclick="openModalXyf({url:'/bumdes/bumdes-media/update?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_media='.$v->id_media ?>'})" class="btn btn-success">Edit</a>
                        <a href="javascript:void(0)" onclick="deleteData('/bumdes/bumdes-media/delete?<?='kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&id_media='.$v->id_media ?>', 'Berhasil Hapus Data')" class="btn btn-success">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>   
    .
</div>
<?php
$urlBack = '/bumdes/bumdes/view?kd_desa='.$model->kd_desa.'&id_bumdes='.$model->id_bumdes.$ext_url;
$scripts =<<<JS
    function deleteData(url, msg){
        $.confirm({
            title: 'Hapus Data?',
            content: 'Tindakan ini akan menghapus data',
            autoClose: 'batal|10000',
            buttons: {
                deleteUser: {
                    text: 'Hapus Data',
                    action: function () {
                        goTransmit({typeSend:'post', urlSend:url, msg: msg, elmChange:'#bumdes-area', urlBack:'{$urlBack}'});
                    }
                },
                batal: function () {
                    $.alert('Tindakan dibatalkan');
                }
            }
        });
    }
JS;

$this->registerJs($scripts);