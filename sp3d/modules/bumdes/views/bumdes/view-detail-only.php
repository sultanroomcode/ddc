<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="bumdes-index">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_bumdes',
            'nama_bumdes',
            [
                'label' => 'Klasifikasi Bumdes / Nilai Akhir',
                'value' => function($m){
                    return $m->klasifikasifinal->nilai_akhir_kategori.' / '.$m->klasifikasifinal->nilai_akhir;
                } 

            ],
            // 'klasifikasifinal.hasil_penilaian',
            // 'klasifikasifinal.nilai_akhir',
            // 'klasifikasifinal.nilai_akhir_kategori',
            'tahun_berdiri',
            'alamat',
            'legal_perdes_no',
            'legal_skdes_bumdes_no',
            // 'created_by',
            // 'updated_by',
            // 'created_at',
            // 'updated_at',
        ],
    ]) ?>

    <!-- Struktur Penasehat/pengawas -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Pendidikan</th>
                <th>Alamat</th>
                <th>No. HP</th>
            </tr>
            <?php foreach($model->pengawas as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->tipejabatan->nama_jabatan ?></td>
                    <td><?= $v->pendidikan ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->hp ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Struktur Pengelola BumDes -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Pendidikan</th>
                <th>Alamat</th>
                <th>No. HP</th>
            </tr>
            <?php foreach($model->pengelola as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->jabatan ?></td>
                    <td><?= $v->pendidikan ?></td>
                    <td><?= $v->alamat ?></td>
                    <td><?= $v->hp ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Unit Usaha -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Unit Usaha</th>
                <th>Omset</th>
                <th>Keuntungan</th>
                <th>Status BUMDesa</th>
                <th>Bentuk BUMDesa</th>
                <th>Status Aktif</th>
                <th>Prioritas</th>
            </tr>
            <?php foreach($model->unitUsaha as $v): ?>
                <tr>
                    <td><?= $v->tipe->title ?></td>
                    <td><?= $v->nf($v->omset) ?></td>
                    <td><?= $v->nf($v->keuntungan) ?></td>
                    <td><?= $v->bh_status ?></td>
                    <td><?= strtoupper($v->bh_bentuk) ?></td>
                    <td><?= $v->status ?></td>
                    <td><?= $v->status_prioritas ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>
    <!-- Bantuan untuk bumdes -->
    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Nama</th>
                <th>Bantuan</th>
                <th>Nilai</th>
            </tr>
            <?php foreach($model->bantuan as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->jenis_bantuan ?></td>
                    <td><?= $v->nf($v->nilai) ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Kerjasama bumdes dengan stakeholder -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Nama</th>
                <th>MOU</th>
                <th>Bentuk Kerjasama</th>
            </tr>
            <?php foreach($model->kerjasama as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->no_mou ?></td>
                    <td><?= $v->bentuk_kerjasama ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Produk Unggulan DEsa -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Nama Produk</th>
                <th>Jumlah</th>
                <th>Satuan</th>
            </tr>
            <?php foreach($model->komoditas as $v): ?>
                <tr>
                    <td><?= $v->nama ?></td>
                    <td><?= $v->nf($v->jumlah) ?></td>
                    <td><?= $v->satuan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Modal dan Perkembangan Bumdes -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Sumber Modal Desa</th>
                <th>Sumber Modal Masyarakat</th>
                <th>Total Modal</th>
                <th>Sumbangan PAD</th>
                <th>Keuntungan</th>
            </tr>
            <?php foreach($model->modal as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->nf($v->sb_modal_desa) ?></td>
                    <td><?= $v->nf($v->sb_modal_masyarakat) ?></td>
                    <td><?= $v->nf($v->modal) ?></td>
                    <td><?= $v->nf($v->smb_pad) ?></td>
                    <td><?= $v->nf($v->keuntungan) ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Potensi Desa -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>ID</th>
                <th>Kategori</th>
                <th>Jenis</th>
                <th>Luas</th>
                <th>Unit</th>
            </tr>
            <?php foreach($model->potensi as $v): ?>
                <tr>
                    <td><?= $v->id_potensi ?></td>
                    <td><?= $v->kategori ?></td>
                    <td><?= $v->jenis ?></td>
                    <td><?= $v->nf($v->luas) ?></td>
                    <td><?= $v->unit ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>

    <!-- Foto Unit Usaha dan Link Video Kretif -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Media</th>
                <th>File</th>
                <th>Tipe</th>
                <th>Keterangan</th>
            </tr>
            <?php foreach($model->media as $v): ?>
                <tr>
                    <td><?= $v->id_media ?></td>
                    <td><?= $v->imageUpload() ?></td>
                    <td><?= $v->tipe ?></td>
                    <td><?= $v->keterangan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>   

    <!-- Program Kerja -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Tahun</th>
                <th>Uraian</th>
                <th>Keterangan</th>
            </tr>
            <?php foreach($model->proker as $v): ?>
                <tr>
                    <td><?= $v->tahun ?></td>
                    <td><?= $v->uraian ?></td>
                    <td><?= $v->keterangan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>   

    <!-- Pemasaran Produk -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Lokasi Pemasaran</th>
                <th>Keterangan</th>
            </tr>
            <?php foreach($model->pemasaran as $v): ?>
                <tr>
                    <td><?= $v->lokasi_pemasaran ?></td>
                    <td><?= $v->keterangan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>    

    <!-- Pembukuan Bumdes -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Uraian</th>
                <th>Status Ada</th>
                <th>Status Benar</th>
                <th>Proses Pelaporan</th>
                <th>Bentuk Pelaporan</th>
            </tr>
            <?php foreach($model->pembukuan as $v): ?>
                <tr>
                    <td><?= $v->uraian ?></td>
                    <td><?= $v->status_ada ?></td>
                    <td><?= $v->status_benar ?></td>
                    <td><?= $v->pelaporan ?></td>
                    <td><?= $v->bentuk_laporan ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>    

    <!-- Pertanggungjawaban -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Uraian</th>
                <th>Status Lengkap</th>
                <th>Proses Tanggungjawab</th>
            </tr>
            <?php foreach($model->pertanggungjawaban as $v): ?>
                <tr>
                    <td><?= $v->uraian ?></td>
                    <td><?= $v->status_lengkap ?></td>
                    <td><?= $v->proses_tgjwb ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>    

    <!-- Dampak -->

    <div id="row">
        <div class="col-md-12">
            <table class="table">
            <tr>
                <th>Dampak Sosial</th>
                <th>Dampak Ekonomi</th>
                <th>Dampak Pembangunan Desa</th>
            </tr>
            <?php foreach($model->dampak as $v): ?>
                <tr>
                    <td><?= $v->dampak_sosial ?></td>
                    <td><?= $v->dampak_ekonomi ?></td>
                    <td><?= $v->dampak_pembangunan_desa ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>   
</div>
.
