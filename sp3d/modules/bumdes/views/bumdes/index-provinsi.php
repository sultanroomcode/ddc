<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Mandiri';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title"><?= $this->title ?></h2>
            <div class="p-10" id="bumdes-area">
                <table class="table">
                    <tr>
                        <th>Nama Bumdes</th>
                        <th>Tahun Berdiri</th>
                        <th>Alamat</th>
                        <th>Perdes</th>
                        <th>SKDes</th>
                        <th>Aksi</th>
                    </tr>
                <?php 
                foreach ($dataProvider->all() as $v): ?>
                    <tr>
                        <td><?= $v->nama_bumdes ?></td>
                        <td><?= $v->tahun_berdiri ?></td>
                        <td><?= $v->alamat ?></td>
                        <td><?= $v->legal_perdes_no ?></td>
                        <td><?= $v->legal_skdes_bumdes_no ?></td>
                        <td><?= ((Yii::$app->user->identity->type == 'provinsi' && Yii::$app->user->identity->id == $v->created_by)?'<a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes/update?kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&provinsi=1\'})" class="btn btn-success">Edit</a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes/view?kd_desa='.$v->kd_desa.'&id_bumdes='.$v->id_bumdes.'&provinsi=1\'})" class="btn btn-success">Isi</a>':'') ?></td>
                    </tr>
                <?php endforeach; ?>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>