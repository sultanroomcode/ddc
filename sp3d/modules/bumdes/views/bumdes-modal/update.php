<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesModal */

$this->title = 'Update Penyertaan Modal BUMDesa: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Modals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-modal-update">
    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
