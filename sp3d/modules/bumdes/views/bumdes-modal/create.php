<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesModal */

$this->title = 'Penyertaan Modal BUMDesa';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Modals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-modal-create">  
	<h2><?= $this->title ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
