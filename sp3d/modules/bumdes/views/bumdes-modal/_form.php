<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesModal */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){    
    $model->kd_desa = ($data['provinsi'] == 1)?$data['kd_desa']:Yii::$app->user->identity->desa->kd_desa;
    $model->kd_kecamatan = substr($model->kd_desa, 0, 7);
    $model->kd_kabupaten = substr($model->kd_kecamatan, 0, 4);
    $model->modal = $model->sb_modal_desa = $model->sb_modal_masyarakat = 0;
    $model->id_bumdes = $data['id_bumdes'];
}

if($data['provinsi'] == 1){
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'].'&provinsi=1';
} else {
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'];
}

$v = range((int) date('Y'), (int) 2006);
$val = array_combine($v, $v);
?>

<div class="bumdes-modal-form">
    <?php $form = ActiveForm::begin($arrFormConfig); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>
    
            <?= $form->field($model, 'kd_kecamatan')->hiddenInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>
            
            <?= $form->field($model, 'id_bumdes')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'tahun')->dropdownList($val) ?>

            <?= $form->field($model, 'sb_modal_desa')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'sb_modal_masyarakat')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'modal')->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' => 'form-control', 'readonly' => true],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'omset')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'keuntungan')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'jml_aset')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'smb_pad')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?php // $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
var ret = "data-123".replace('data-','');
console.log(ret); 

$('#bumdesmodal-sb_modal_masyarakat, #bumdesmodal-sb_modal_desa').change(function(){
    var sbmd_ms = $('#bumdesmodal-sb_modal_masyarakat').val();
    var sbmd_ds = $('#bumdesmodal-sb_modal_desa').val();
    // console.log($('#bumdesmodal-sb_modal_masyarakat').val() + ' ' + $('#bumdesmodal-sb_modal_desa').val());
    s_ms = sbmd_ms.toString();
    s_ds = sbmd_ds.toString();
    //console.log(sbmd_ms + ' 1 ' + sbmd_ds);
    r1_ms = s_ms.replace(/\./g,'');
    r1_ds = s_ds.replace(/\./g,'');
    //console.log(r1_ms + ' 2 ' + r1_ds);
    r2_ms = r1_ms.replace(',','.');
    r2_ds = r1_ds.replace(',','.');
    //console.log(r2_ms + ' 3 ' + r2_ds);

    $('#bumdesmodal-modal').val(parseFloat(r2_ms)+parseFloat(r2_ds));
});

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#bumdes-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);