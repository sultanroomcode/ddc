<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPengelola */

$this->title = 'Update Pengelola Bumdes : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Pengelolas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'id_pengelola' => $model->id_pengelola]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-pengelola-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
