<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPengelola */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Pengelolas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-pengelola-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'id_bumdes',
            'id_pengelola',
            'nama',
            'alamat',
            'pendidikan',
            'jabatan',
            'pekerjaan',
            'hp',
            // 'tipe',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
