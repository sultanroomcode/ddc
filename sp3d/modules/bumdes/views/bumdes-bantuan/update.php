<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBantuan */

$this->title = 'Ubah Bantuan : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bantuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'id_bantuan' => $model->id_bantuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bantuan-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
