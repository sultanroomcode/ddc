<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use sp3d\models\transaksi\Sp3dMasterJabatan;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPengawas */
/* @var $form yii\widgets\ActiveForm */

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_desa = ($data['provinsi'] == 1)?$data['kd_desa']:Yii::$app->user->identity->desa->kd_desa;
    $model->id_bumdes = $data['id_bumdes'];
    $model->genNum();//generate number
}

$jabatanList = ArrayHelper::map(Sp3dMasterJabatan::find()->where(['tipe' => 'pbumdes', 'status' => '10'])->orderBy('urut')->all(), 'id', 'nama_jabatan');
if($data['provinsi'] == 1){
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'].'&provinsi=1';
} else {
    $urlback = '/bumdes/bumdes/view?kd_desa='.$data['kd_desa'].'&id_bumdes='.$data['id_bumdes'];
}
?>

<div class="bumdes-pengawas-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_desa')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_bumdes')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'id_pengawas')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'tipe')->dropdownList(['PGWS' => 'Pengawas', 'PNSH' => 'Penasehat'])->label('Kategori') ?>
    
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'pendidikan')->dropdownList([
        '0|SD' => 'Sekolah Dasar', 
        '1|SMP' => 'Sekolah Menengah Pertama', 
        '2|SMA' => 'Sekolah Menengah Atas', 
        '3|D1' => 'Diploma I', 
        '4|D2' => 'Diploma II', 
        '5|D3' => 'Diploma III', 
        '6|D4' => 'Diploma IV', 
        '7|S1' => 'Sarjana', 
        '8|S2' => 'Magister', 
        '9|S3' => 'Doktoral', 
    ]) ?>


    <?= $form->field($model, 'jabatan')->dropdownList($jabatanList) ?>

    <?= $form->field($model, 'pekerjaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tupoksi')->dropdownList(['jalan' => 'Jalan', 'belum' => 'Belum']) ?>

    <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-7">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#bumdes-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);