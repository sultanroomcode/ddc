<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPengawas */

$this->title = 'Update Data Pengawas BUMDesa : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Pengawas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'id_pengawas' => $model->id_pengawas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-pengawas-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
