<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesMedia */

$this->title = $model->id_bumdes;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-media-view">
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'id_bumdes',
            'id_media',
            'kd_kecamatan',
            'kd_kabupaten',
            'file_foto',
            'video_url:url',
            'tipe',
            'keterangan',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
