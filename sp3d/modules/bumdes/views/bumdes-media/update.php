<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesMedia */

$this->title = 'Update Media BUMDesa : ';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_bumdes, 'url' => ['view', 'id_bumdes' => $model->id_bumdes, 'id_media' => $model->id_media, 'kd_kabupaten' => $model->kd_kabupaten]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-media-update">   

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
