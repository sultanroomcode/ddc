<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Bersama Pengelolas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-pengelola-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bumdes Bersama Pengelola', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_kabupaten',
            'id_bb',
            'id_pengelola',
            'nama',
            'alamat',
            // 'pendidikan',
            // 'jabatan',
            // 'pekerjaan',
            // 'hp',
            // 'tipe',
            // 'created_by',
            // 'updated_by',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
