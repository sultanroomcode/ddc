<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaPengelola */

$this->title = 'Update Bumdes Bersama Pengelola: ' . $model->kd_kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Pengelolas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_kabupaten, 'url' => ['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'id_pengelola' => $model->id_pengelola]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-pengelola-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
