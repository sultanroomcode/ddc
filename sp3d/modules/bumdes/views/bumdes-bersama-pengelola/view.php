<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaPengelola */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Pengelolas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-pengelola-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'id_bb',
            'id_pengelola',
            'nama',
            'alamat',
            'pendidikan',
            'jabatan',
            'pekerjaan',
            'hp',
            // 'tipe',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
