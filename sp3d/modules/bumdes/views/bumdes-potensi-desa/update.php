<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPotensiDesa */

$this->title = 'Update Potensi BUMDesa : ' . $model->jenis;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Potensi Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'id_potensi' => $model->id_potensi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-potensi-desa-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
