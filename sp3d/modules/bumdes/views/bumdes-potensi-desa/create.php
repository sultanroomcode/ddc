<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPotensiDesa */

$this->title = 'Potensi BUMDesa';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Potensi Desas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-potensi-desa-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
