<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaUnitUsaha */

$this->title = 'Update  Unit Usaha Bumdes Bersama : ' . $model->kd_kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Unit Usahas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_kabupaten, 'url' => ['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'id_uu' => $model->id_uu]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-unit-usaha-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
