<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaUnitUsaha */

$this->title = 'Isi Unit Usaha Bumdes Bersama';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Unit Usahas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-unit-usaha-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
