<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use sp3d\models\User;
use yii\bootstrap\ActiveForm;

$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaDesaAnggota */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->kd_kabupaten = substr(Yii::$app->user->identity->desa->kd_desa, 0, 4);
    $model->id_bb = $data['id_bb'];
    $model->kd_kecamatan = substr(Yii::$app->user->identity->desa->kd_desa, 0, 7);
}

$arr = ArrayHelper::map(User::find()->where(['type' => 'kecamatan'])->andWhere(['LIKE', 'id', $model->kd_kabupaten.'%', false])->all(), 'id', 'description');
$urlback = '/bumdes/bumdes-bersama/view?kd_kabupaten='.$data['kd_kabupaten'].'&id_bb='.$data['id_bb'];
?>

<div class="bumdes-bersama-desa-anggota-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_bb')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'kd_kecamatan')->dropdownList($arr, ['onchange' => 'getDesa()']) ?>

    <?= $form->field($model, 'kd_desa')->dropdownList([]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
function getDesa(){
    var kode = $('#bumdesbersamadesaanggota-kd_kecamatan').val();
    goLoad({elm:'#bumdesbersamadesaanggota-kd_desa', url:'/pendamping/data/data-option?type=desa&kode='+kode});
}

getDesa();

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#bumdes-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);