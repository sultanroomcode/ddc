<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaDesaAnggota */

$this->title = 'Isi Desa Anggota Bumdes Bersama';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Desa Anggotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-desa-anggota-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
