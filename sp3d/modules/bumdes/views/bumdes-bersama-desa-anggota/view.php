<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaDesaAnggota */

$this->title = $model->desa->description;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Desa Anggotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-desa-anggota-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'id_bb',
            'kd_desa',
            'desa.description',
            'kd_kecamatan',
            'kecamatan.description',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
