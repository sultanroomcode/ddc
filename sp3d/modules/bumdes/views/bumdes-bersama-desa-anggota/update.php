<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaDesaAnggota */

$this->title = 'Update Desa Anggota Bumdes Bersama : ' . $model->kd_kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Desa Anggotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_kabupaten, 'url' => ['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'kd_desa' => $model->kd_desa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-desa-anggota-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
