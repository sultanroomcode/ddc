<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaModal */

$this->title = $model->kd_kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Modals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-modal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'tahun' => $model->tahun], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'tahun' => $model->tahun], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_kabupaten',
            'id_bb',
            'tahun',
            'kd_kecamatan',
            'modal',
            'sb_modal_desa',
            'sb_modal_masyarakat',
            'sb_pad',
            'pendapatan_bumdes',
            'keterangan',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
