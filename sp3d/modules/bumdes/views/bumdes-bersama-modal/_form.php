<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaModal */
/* @var $form yii\widgets\ActiveForm */
$arrFormConfig = [
'id' => $model->formName(), 
'layout' => 'horizontal',
'fieldConfig' => [
    'enableError' => true,
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-5',
        // 'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]];

if($model->isNewRecord){
    $model->kd_kabupaten = substr(Yii::$app->user->identity->desa->kd_desa, 0, 4);
    $model->kd_kecamatan = substr(Yii::$app->user->identity->desa->kd_desa, 0, 7);
    $model->id_bb = $data['id_bb'];
}
$urlback = '/bumdes/bumdes-bersama/view?kd_kabupaten='.$data['kd_kabupaten'].'&id_bb='.$data['id_bb'];

$arrTahun = ['2016' => '2016', '2017' => '2017', '2018' => '2018'];
?>

<div class="bumdes-bersama-modal-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'kd_kabupaten')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'id_bb')->hiddenInput(['maxlength' => true])->label(false) ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'tahun')->dropdownList($arrTahun) ?>

            <?= $form->field($model, 'kd_kecamatan')->textInput(['maxlength' => true]) ?>    

            <?= $form->field($model, 'modal')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'pendapatan_bumdes')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => ',',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ]
            ]) ?>
        
            <fieldset>
                <legend>Sumber</legend>
                <?= $form->field($model, 'sb_modal_desa')->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'decimal',
                        'digits' => 2,
                        'digitsOptional' => false,
                        'radixPoint' => ',',
                        'groupSeparator' => '.',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,
                    ]
                ]) ?>
                <?= $form->field($model, 'sb_modal_masyarakat')->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'decimal',
                        'digits' => 2,
                        'digitsOptional' => false,
                        'radixPoint' => ',',
                        'groupSeparator' => '.',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,
                    ]
                ]) ?>

                <?= $form->field($model, 'sb_pad')->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'decimal',
                        'digits' => 2,
                        'digitsOptional' => false,
                        'radixPoint' => ',',
                        'groupSeparator' => '.',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,
                    ]
                ]) ?>
            </fieldset>

            <?php // $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $script = <<<JS
//regularly-ajax
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
        \$form.attr('action'),
        \$form.serialize()
    ).done(function(res){
        if(res == 1){
            //$(\$form).trigger('reset');
            $.alert('Berhasil menyimpan data');
            goLoad({elm:'#bumdes-area', url : '{$urlback}'});
        } else {
            console.log('Fail but not error');
        }
    }).fail(function(){
        console.log('Server Error');
    });

    return false;
});
JS;

$this->registerJs($script);