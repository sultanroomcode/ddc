<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bumdes Bersama Modals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-bersama-modal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bumdes Bersama Modal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_kabupaten',
            'id_bb',
            'tahun',
            'kd_kecamatan',
            'modal',
            // 'sb_modal_desa',
            // 'sb_modal_masyarakat',
            // 'sb_pad',
            // 'pendapatan_bumdes',
            // 'keterangan',
            // 'created_by',
            // 'updated_by',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
