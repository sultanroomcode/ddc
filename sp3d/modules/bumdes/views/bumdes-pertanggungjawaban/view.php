<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPertanggungjawaban */

$this->title = $model->uraian;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Pertanggungjawabans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-pertanggungjawaban-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_bumdes',
            'no',
            'uraian',
            'proses_tgjwb',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
