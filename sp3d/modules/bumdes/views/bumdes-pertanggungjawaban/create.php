<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesPertanggungjawaban */

$this->title = 'Isi Pertanggungjawaban';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Pertanggungjawabans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-pertanggungjawaban-create">
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
