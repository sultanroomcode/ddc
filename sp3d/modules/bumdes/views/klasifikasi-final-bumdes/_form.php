<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiFinalBumdes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="klasifikasi-final-bumdes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_bumdes')->textInput() ?>

    <?= $form->field($model, 'hasil_penilaian')->textInput() ?>

    <?= $form->field($model, 'nilai_akhir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nilai_akhir_kategori')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
