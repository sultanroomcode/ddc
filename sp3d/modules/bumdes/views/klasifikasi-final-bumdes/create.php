<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiFinalBumdes */

$this->title = 'Create Klasifikasi Final Bumdes';
$this->params['breadcrumbs'][] = ['label' => 'Klasifikasi Final Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klasifikasi-final-bumdes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
