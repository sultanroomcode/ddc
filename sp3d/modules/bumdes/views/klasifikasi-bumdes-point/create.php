<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiBumdesPoint */

$this->title = 'Create Klasifikasi Bumdes Point';
$this->params['breadcrumbs'][] = ['label' => 'Klasifikasi Bumdes Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klasifikasi-bumdes-point-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
