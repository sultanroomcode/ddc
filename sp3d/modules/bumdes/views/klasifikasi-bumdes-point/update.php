<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiBumdesPoint */

$this->title = 'Update Klasifikasi Bumdes Point: ' . $model->kode;
$this->params['breadcrumbs'][] = ['label' => 'Klasifikasi Bumdes Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode, 'url' => ['view', 'kode' => $model->kode, 'skor' => $model->skor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="klasifikasi-bumdes-point-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
