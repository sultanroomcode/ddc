<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Klasifikasi Bumdes Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klasifikasi-bumdes-point-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Klasifikasi Bumdes Point', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kode',
            'skor',
            'indikator',
            'point_bobot',
            'nilai_max',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
