<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiBumdesPoint */

$this->title = $model->kode;
$this->params['breadcrumbs'][] = ['label' => 'Klasifikasi Bumdes Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klasifikasi-bumdes-point-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kode' => $model->kode, 'skor' => $model->skor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kode' => $model->kode, 'skor' => $model->skor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kode',
            'skor',
            'indikator',
            'point_bobot',
            'nilai_max',
        ],
    ]) ?>

</div>
