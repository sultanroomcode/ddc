<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesBersamaKomoditas */

$this->title = 'Update Komoditas Bumdes Bersama : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Bersama Komoditas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_kabupaten, 'url' => ['view', 'kd_kabupaten' => $model->kd_kabupaten, 'id_bb' => $model->id_bb, 'id_komoditas' => $model->id_komoditas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-bersama-komoditas-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
