<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesProgramKerja */

$this->title = $model->uraian;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Program Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-program-kerja-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'kd_kecamatan',
            'kd_kabupaten',
            'id_bumdes',
            'no',
            'uraian',
            'keterangan:ntext',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
