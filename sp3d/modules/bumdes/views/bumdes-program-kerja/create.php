<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesProgramKerja */

$this->title = 'Program Kerja BUMDesa';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Program Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-program-kerja-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
