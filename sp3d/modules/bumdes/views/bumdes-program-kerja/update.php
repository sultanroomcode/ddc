<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesProgramKerja */

$this->title = 'Update Program Kerja: '. $model->uraian;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Program Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'no' => $model->no]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-program-kerja-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
