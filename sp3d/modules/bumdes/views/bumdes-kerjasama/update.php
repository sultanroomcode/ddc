<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesKerjasama */

$this->title = 'Update Kerjasama BUMDesa ';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Kerjasamas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes, 'id_kerjasama' => $model->id_kerjasama]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-kerjasama-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
