<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Klasifikasi Bumdes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klasifikasi-bumdes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Klasifikasi Bumdes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kd_desa',
            'id_bumdes',
            'legalitas',
            'kelembagaan',
            'adm_keuangan',
            //'permodalan',
            //'unit_usaha',
            //'omset',
            //'keuntungan',
            //'pad',
            //'naker',
            //'mitra',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
