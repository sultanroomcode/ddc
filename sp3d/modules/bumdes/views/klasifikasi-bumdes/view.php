<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiBumdes */

$this->title = $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Klasifikasi Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klasifikasi-bumdes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_desa',
            'id_bumdes',
            'legalitas',
            'kelembagaan',
            'adm_keuangan',
            'permodalan',
            'unit_usaha',
            'omset',
            'keuntungan',
            'pad',
            'naker',
            'mitra',
        ],
    ]) ?>

</div>
