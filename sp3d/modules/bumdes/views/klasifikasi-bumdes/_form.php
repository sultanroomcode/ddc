<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiBumdes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="klasifikasi-bumdes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_desa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_bumdes')->textInput() ?>

    <?= $form->field($model, 'legalitas')->textInput() ?>

    <?= $form->field($model, 'kelembagaan')->textInput() ?>

    <?= $form->field($model, 'adm_keuangan')->textInput() ?>

    <?= $form->field($model, 'permodalan')->textInput() ?>

    <?= $form->field($model, 'unit_usaha')->textInput() ?>

    <?= $form->field($model, 'omset')->textInput() ?>

    <?= $form->field($model, 'keuntungan')->textInput() ?>

    <?= $form->field($model, 'pad')->textInput() ?>

    <?= $form->field($model, 'naker')->textInput() ?>

    <?= $form->field($model, 'mitra')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
