<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\KlasifikasiBumdes */

$this->title = 'Update Klasifikasi Bumdes: ' . $model->kd_desa;
$this->params['breadcrumbs'][] = ['label' => 'Klasifikasi Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_desa, 'url' => ['view', 'kd_desa' => $model->kd_desa, 'id_bumdes' => $model->id_bumdes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="klasifikasi-bumdes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
