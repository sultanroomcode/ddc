<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesTipeUnitUsaha */

$this->title = 'Update BUMDesa Tipe Unit Usaha: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Tipe Unit Usahas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-tipe-unit-usaha-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
