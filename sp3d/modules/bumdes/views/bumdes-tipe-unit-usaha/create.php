<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sp3d\modules\bumdes\models\BumdesTipeUnitUsaha */

$this->title = 'Isi Tipe Usaha BUMDesa';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes Tipe Unit Usahas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-tipe-unit-usaha-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
