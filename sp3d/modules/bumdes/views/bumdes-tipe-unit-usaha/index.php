<div class="row">
    <div class="col-md-12">
        <!-- Main Chart -->
        <div class="tile">
            <h2 class="tile-title">Tipe Unit Usaha Bumdes</h2>
            <div class="p-10" id="bumdes-area">
                <p>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#bumdes-area', url:'/bumdes/bumdes-tipe-unit-usaha/create'})" class="btn btn-success"><i class="fa fa-plus"></i></a>
                </p>
                
                
                
                
                

                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Label</th>
                        <th>Urutan</th>
                        <th>Aksi</th>
                    </tr>
                <?php 
                foreach ($dataProvider->all() as $v): ?>
                    <tr>
                        <td><?= $v->id ?></td>
                        <td><?= $v->title ?></td>
                        <td><?= $v->order ?></td>
                        <td><?= '<a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-tipe-unit-usaha/update?id='.$v->id.'\'})" class="btn btn-success"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="goLoad({elm:\'#bumdes-area\', url:\'/bumdes/bumdes-tipe-unit-usaha/view?id='.$v->id.'\'})" class="btn btn-success"><i class="fa fa-eye"></i></a>' ?></td>
                    </tr>
                <?php endforeach; ?>
                </table>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>
</div>
