<?php

namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
/**
 * This is the model class for table "bumdes_kerjasama".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_bumdes
 * @property int $id_kerjasama
 * @property string $tahun
 * @property string $nama
 * @property string $no_mou
 * @property string $bentuk_kerjasama
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesKerjasama extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bumdes_kerjasama';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'id_kerjasama'], 'required'],
            [['id_bumdes', 'id_kerjasama'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['tahun'], 'string', 'max' => 4],
            [['nama', 'bentuk_kerjasama'], 'string', 'max' => 100],
            [['no_mou'], 'string', 'max' => 50],
            [['keterangan'], 'string', 'max' => 255],            
            [['kd_desa', 'id_bumdes', 'id_kerjasama'], 'unique', 'targetAttribute' => ['kd_desa', 'id_bumdes', 'id_kerjasama']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bumdes' => 'Id Bumdes',
            'id_kerjasama' => 'Id Kerjasama',
            'tahun' => 'Tahun',
            'nama' => 'Nama Instansi',
            'no_mou' => 'No Mou',
            'bentuk_kerjasama' => 'Bentuk Kerjasama',
            'keterangan' => 'Keterangan',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //event
    public function afterSave($isNew, $old) {
        if ($isNew){  
            
        } else {

        }

        // $connection = Yii::$app->getDb();
        Yii::$app->db->createCommand("UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_mitra vkm ON kb.kd_desa = vkm.kd_desa AND kb.id_bumdes = vkm.id_bumdes SET kb.mitra = vkm.mitra_poin
        WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes",  [':kd_desa' => $this->kd_desa, ':id_bumdes' => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();

        
   }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('id_kerjasama');
        if($mx == null){
            $this->id_kerjasama = 1;
        } else {
            $this->id_kerjasama = $mx +1;
        }
    }
}
