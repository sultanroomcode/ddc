<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "bumdes_pertanggungjawaban".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_bumdes
 * @property int $no
 * @property string $uraian
 * @property string $status_lengkap
 * @property string $proses_tgjwb
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesPertanggungjawaban extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bumdes_pertanggungjawaban';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'no'], 'required'],
            [['id_bumdes', 'no'], 'integer'],
            // [['created_at', 'updated_at'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['status_lengkap'], 'string', 'max' => 20],
            [['uraian'], 'string', 'max' => 255],
            [['proses_tgjwb'], 'string', 'max' => 100],
            // [['created_by', 'updated_by'], 'string', 'max' => 10],
            [['kd_desa', 'id_bumdes', 'no'], 'unique', 'targetAttribute' => ['kd_desa', 'id_bumdes', 'no']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bumdes' => 'Id Bumdes',
            'no' => 'No',
            'uraian' => 'Uraian',
            'status_lengkap' => 'Status Lengkap',
            'proses_tgjwb' => 'Mekanisme Tanggungjawab',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('no');
        if($mx == null){
            $this->no = 1;
        } else {
            $this->no = $mx +1;
        }
    }
}
