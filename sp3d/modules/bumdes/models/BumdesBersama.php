<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use ddcop\models\Operator;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_bersama".
 *
 * @property string $kd_kabupaten
 * @property string $id_bb
 * @property string $nama_bb
 * @property string $tahun_berdiri
 * @property string $alamat
 * @property string $legal_bkad_no
 * @property string $legal_perkades_bersama_no
 * @property string $legal_skdes_bersama_no
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersama extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bumdes_bersama';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kabupaten'], 'required'],
            [['kd_kabupaten'], 'string', 'max' => 12],
            [['id_bb'], 'string', 'max' => 15],
            [['nama_bb', 'alamat'], 'string', 'max' => 150],
            [['tahun_berdiri'], 'string', 'max' => 4],
            [['legal_bkad_no', 'legal_perkades_bersama_no', 'legal_skdes_bersama_no'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bb' => 'ID BUMDesa Bersama',
            'nama_bb' => 'Nama Bumdes Bersama',
            'tahun_berdiri' => 'Tahun Berdiri',
            'alamat' => 'Alamat',
            'legal_bkad_no' => 'Peraturan BKAD No',
            'legal_perkades_bersama_no' => 'Peraturan Kepala Desa Bersama No',
            'legal_skdes_bersama_no' => 'SK Kepala Desa Bersama No',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
    //event
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            $this->id_bb = time();
        } 

        return true;
    } 

    //
    public function getCreator()
    {
        return $this->hasOne(Operator::className(), ['id' => 'created_by']);
    }
    public function getUpdater()
    {
        return $this->hasOne(Operator::className(), ['id' => 'updated_by']);
    }


    public function getBkad()
    {
        return $this->hasMany(BumdesBersamaBkad::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getDesaAnggota()
    {
        return $this->hasMany(BumdesBersamaDesaAnggota::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getMedia()
    {
        return $this->hasMany(BumdesBersamaMedia::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getModal()
    {
        return $this->hasMany(BumdesBersamaModal::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getPengelola()
    {
        return $this->hasMany(BumdesBersamaPengelola::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getPengawas()
    {
        return $this->hasMany(BumdesBersamaPengawas::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getUnitUsaha()
    {
        return $this->hasMany(BumdesBersamaUnitUsaha::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getPotensi()
    {
        return $this->hasMany(BumdesBersamaPotensiDesa::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getKomoditas()
    {
        return $this->hasMany(BumdesBersamaKomoditas::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }
}
