<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
/**
 * This is the model class for table "bumdes_unit_usaha".
 *
 * @property string $kd_desa
 * @property integer $id_bumdes
 * @property integer $id_uu
 * @property string $unit_usaha
 * @property integer $omset
 * @property string $bh_status
 * @property string $bh_bentuk
 * @property string $bh_no
 * @property integer $jml_karyawan
 * @property integer $keuntungan
 * @property string $status
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesUnitUsaha extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public $afoto_src;
    public static function tableName()
    {
        return 'bumdes_unit_usaha';
    }

    public function behaviors() // behaviour jangan sampai kosong ini akan berpengaruh pada foreach di, Invalid argument supplied for foreach()
    // 1. in /var/www/html/yii/ddc/vendor/yiisoft/yii2/base/Component.php at line 733
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'id_uu'], 'required'],
            [['afoto_src'], 'file', 'extensions' => 'jpg'],
            [['id_bumdes', 'id_uu', 'jml_karyawan'], 'integer'],
            [['omset', 'keuntungan', 'foto', 'status_prioritas'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['unit_usaha', 'bh_no'], 'string', 'max' => 50],
            [['bh_status', 'bh_bentuk', 'status'], 'string', 'max' => 5],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_kabupaten' => 'Kode Kabupaten',
            'id_bumdes' => 'ID Bumdes',
            'id_uu' => 'ID Unit Usaha',
            'unit_usaha' => 'Unit Usaha',
            'omset' => 'Omset Per Tahun',
            
            'bh_status' => 'Status Badan Hukum BUMDesa',
            'bh_bentuk' => 'Bentuk BUMDesa',
            'bh_no' => 'No. Pengesahan Badan Hukum',

            'afoto_src' => 'Foto Unit Usaha',
            
            'jml_karyawan' => 'Jumlah Karyawan',
            'keuntungan' => 'Keuntungan Per Tahun',
            'status' => 'Status Aktif',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
    //event

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['omset', 'keuntungan'], 'safe'],
        } 
        $this->upload();
        $this->kd_kabupaten = substr($this->kd_desa, 0, 4);
        $this->kd_kecamatan = substr($this->kd_desa, 0, 7);
        $this->omset = (float) $this->omset;
        $this->keuntungan = (float) $this->keuntungan;
        return true;
    } 

    public function afterSave($isNew, $old) {
        if ($isNew){  
            
        } else {

        }

        // $connection = Yii::$app->getDb();
        Yii::$app->db->createCommand("UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_unit_usaha vkuu ON kb.kd_desa = vkuu.kd_desa AND kb.id_bumdes = vkuu.id_bumdes SET kb.unit_usaha = vkuu.unit_usaha_poin 
            WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes",  [':kd_desa' => $this->kd_desa, ':id_bumdes' => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();
   }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('id_uu');
        if($mx == null){
            $this->id_uu = 1;
        } else {
            $this->id_uu = $mx +1;
        }
    }

    public function upload()
    {
        $this->validate();
        $this->afoto_src = UploadedFile::getInstance($this, 'afoto_src');
        // echo "string ".time();
        // var_dump($this->afoto_src);
        if (isset($this->afoto_src) && $this->afoto_src->size != 0){
            $url = 'userfile/'.$this->kd_desa.'/bumdes/unit-usaha/';
            $this->makeDir($url);
            $newname = 'uu-'.time().'-'.$this->kd_desa.'.'.$this->afoto_src->extension;
            if($this->isNewRecord){
                $callback = $this->afoto_src->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->foto) && $this->foto !== '' && $this->foto != null){
                    unlink($url.$this->foto);
                }
                $callback = $this->afoto_src->saveAs($url.$newname);               
            }
            $this->foto = $newname;
        } else {
            $this->foto = 'default.jpg';
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    public function getTipe()
    {
        return $this->hasOne(BumdesTipeUnitUsaha::className(), ['id' => 'unit_usaha']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten']);
    }

    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa']);
    }

    public function getBumdes()
    {
        return $this->hasOne(Bumdes::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }
}
