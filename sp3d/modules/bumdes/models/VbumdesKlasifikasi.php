<?php

namespace sp3d\modules\bumdes\models;

use Yii;

/**
 * This is the model class for table "vbumdes_klasifikasi".
 *
 * @property string $ds_id
 * @property string $ds_nama
 * @property string $kc_id
 * @property string $kc_nama
 * @property string $kb_id
 * @property string $kb_nama
 * @property string $nama_bumdes
 * @property string $tahun_berdiri
 * @property string $alamat
 * @property string $kd_desa
 * @property int $id_bumdes
 * @property int $legalitas
 * @property int $kelembagaan
 * @property int $adm_keuangan
 * @property int $permodalan
 * @property int $unit_usaha
 * @property int $omset
 * @property int $keuntungan
 * @property int $pad
 * @property int $naker
 * @property int $mitra
 * @property int $hasil_penilaian
 * @property string $nilai_akhir
 * @property string $nilai_akhir_kategori
 */
class VbumdesKlasifikasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vbumdes_klasifikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_bumdes', 'legalitas', 'kelembagaan', 'adm_keuangan', 'permodalan', 'unit_usaha', 'omset', 'keuntungan', 'pad', 'naker', 'mitra', 'hasil_penilaian'], 'integer'],
            [['nilai_akhir'], 'number'],
            [['ds_id', 'kc_id', 'kb_id'], 'string', 'max' => 15],
            [['ds_nama', 'kc_nama', 'kb_nama'], 'string', 'max' => 255],
            [['nama_bumdes', 'alamat'], 'string', 'max' => 150],
            [['tahun_berdiri'], 'string', 'max' => 4],
            [['kd_desa'], 'string', 'max' => 12],
            [['nilai_akhir_kategori'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ds_id' => 'Ds ID',
            'ds_nama' => 'Ds Nama',
            'kc_id' => 'Kc ID',
            'kc_nama' => 'Kc Nama',
            'kb_id' => 'Kb ID',
            'kb_nama' => 'Kb Nama',
            'nama_bumdes' => 'Nama Bumdes',
            'tahun_berdiri' => 'Tahun Berdiri',
            'alamat' => 'Alamat',
            'kd_desa' => 'Kd Desa',
            'id_bumdes' => 'Id Bumdes',
            'legalitas' => 'Legalitas',
            'kelembagaan' => 'Kelembagaan',
            'adm_keuangan' => 'Adm Keuangan',
            'permodalan' => 'Permodalan',
            'unit_usaha' => 'Unit Usaha',
            'omset' => 'Omset',
            'keuntungan' => 'Keuntungan',
            'pad' => 'Pad',
            'naker' => 'Naker',
            'mitra' => 'Mitra',
            'hasil_penilaian' => 'Hasil Penilaian',
            'nilai_akhir' => 'Nilai Akhir',
            'nilai_akhir_kategori' => 'Nilai Akhir Kategori',
        ];
    }
}
