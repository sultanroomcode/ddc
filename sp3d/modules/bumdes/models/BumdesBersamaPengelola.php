<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_bersama_pengelola".
 *
 * @property string $kd_kabupaten
 * @property string $id_bb
 * @property integer $id_pengelola
 * @property string $nama
 * @property string $alamat
 * @property string $pendidikan
 * @property string $jabatan
 * @property string $pekerjaan
 * @property string $hp
 * @property string $tipe
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersamaPengelola extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'bumdes_bersama_pengelola';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kabupaten', 'id_bb', 'nama', 'alamat', 'hp'], 'required'],
            [['id_pengelola'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 12],
            [['id_bb'], 'string', 'max' => 15],
            [['nama'], 'string', 'max' => 150],
            [['alamat'], 'string', 'max' => 255],
            [['pendidikan'], 'string', 'max' => 8],
            [['pekerjaan'], 'string', 'max' => 100],
            [['jabatan'], 'string', 'max' => 50],
            [['hp'], 'string', 'max' => 16],
            [['tipe'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bb' => 'Id Bb',
            'id_pengelola' => 'Id Pengelola',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'pendidikan' => 'Pendidikan',
            'jabatan' => 'Jabatan',
            'pekerjaan' => 'Pekerjaan',
            'hp' => 'Hp',
            'tipe' => 'Tipe',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_kabupaten' => $this->kd_kabupaten, 'id_bb' => $this->id_bb])->max('id_pengelola');
        if($mx == null){
            $this->id_pengelola = 1;
        } else {
            $this->id_pengelola = $mx +1;
        }
    }
}
