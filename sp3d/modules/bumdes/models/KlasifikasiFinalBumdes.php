<?php

namespace sp3d\modules\bumdes\models;

use Yii;

/**
 * This is the model class for table "klasifikasi_final_bumdes".
 *
 * @property string $kd_desa
 * @property int $id_bumdes
 * @property int $hasil_penilaian
 * @property string $nilai_akhir
 * @property string $nilai_akhir_kategori
 */
class KlasifikasiFinalBumdes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'klasifikasi_final_bumdes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes'], 'required'],
            [['id_bumdes', 'hasil_penilaian'], 'integer'],
            [['nilai_akhir'], 'number'],
            [['kd_desa'], 'string', 'max' => 12],
            [['nilai_akhir_kategori'], 'string', 'max' => 20],
            [['kd_desa', 'id_bumdes'], 'unique', 'targetAttribute' => ['kd_desa', 'id_bumdes']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'id_bumdes' => 'Id Bumdes',
            'hasil_penilaian' => 'Hasil Penilaian',
            'nilai_akhir' => 'Nilai Akhir',
            'nilai_akhir_kategori' => 'Kategori',
        ];
    }
}
