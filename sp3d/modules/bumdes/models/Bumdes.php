<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use ddcop\models\Operator;
use sp3d\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property integer $id_bumdes
 * @property string $nama_bumdes
 * @property string $tahun_berdiri
 * @property string $alamat
 * @property string $legal_perdes_no
 * @property string $legal_skdes_bumdes_no
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Bumdes extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'bumdes';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'nama_bumdes', 'tahun_berdiri', 'legal_perdes_no', 'legal_skdes_bumdes_no'], 'required'],
            [['id_bumdes'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['status'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten',], 'string', 'max' => 5],
            [['legal_perdes_no', 'legal_skdes_bumdes_no'], 'string', 'max' => 100],
            [['nama_bumdes', 'alamat'], 'string', 'max' => 150],
            [['tahun_berdiri'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kabupaten.description' => 'Kabupaten',
            'kecamatan.description' => 'Kecamatan',
            'desa.description' => 'Desa',
            'kd_desa' => 'Desa',
            'kd_kecamatan' => 'Kecamatan',
            'kd_kabupaten' => 'Kabupaten',
            'id_bumdes' => 'Kode BumDesa',
            'klasifikasifinal.hasil_penilaian' => 'Klasifikasi Bumdes / Hasil Penilaian',
            'klasifikasifinal.nilai_akhir' => 'Klasifikasi Bumdes / Nilai Akhir',
            'klasifikasifinal.nilai_akhir_kategori' => 'Klasifikasi Bumdes / Kategori',
            'creator.detail.nama' => 'Nama Pengisi',
            'nama_bumdes' => 'Nama Bumdes',
            'tahun_berdiri' => 'Tahun Berdiri',
            'alamat' => 'Alamat',
            'legal_perdes_no' => 'No. Perdes',
            'legal_skdes_bumdes_no' => 'SK Kepala Desa',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
    //event
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            
        } 

        return true;
    } 


    public function afterSave($isNew, $old) {
        if ($isNew){  
            $kl = new KlasifikasiFinalBumdes();
            $kl->kd_desa = $this->kd_desa;
            $kl->id_bumdes = $this->id_bumdes;
            $kl->hasil_penilaian = 0;
            $kl->nilai_akhir = 0;
            $kl->nilai_akhir_kategori = 'RINTISAN';
            $kl->save();

            $kl2 = new KlasifikasiBumdes();
            $kl2->kd_desa = $this->kd_desa;
            $kl2->id_bumdes = $this->id_bumdes;
            $kl2->legalitas = $kl2->kelembagaan = $kl2->adm_keuangan = $kl2->permodalan = $kl2->unit_usaha = $kl2->omset = $kl2->keuntungan = $kl2->pad = $kl2->naker = $kl2->mitra = 1;
            $kl2->save();
        } 

        Yii::$app->db->createCommand("UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_legalitas vkl ON kb.kd_desa = vkl.kd_desa AND kb.id_bumdes = vkl.id_bumdes SET kb.legalitas = (vkl.default_legal + vkl.have_perdes + vkl.have_unit_usaha + vkl.have_unit_usaha_badan_hukum)
            WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes", [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();
   }

    public function afterDelete()
    {
        // Remove
        $m = KlasifikasiBumdes::findOne(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->delete();
        $m2 = KlasifikasiFinalBumdes::findOne(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->delete();
        parent::afterDelete();
    }

    public function getCreator()
    {
        return $this->hasOne(Operator::className(), ['id' => 'created_by']);
    }
    public function getUpdater()
    {
        return $this->hasOne(Operator::className(), ['id' => 'updated_by']);
    }



    public function getMedia()
    {
        return $this->hasMany(BumdesMedia::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getModal()
    {
        return $this->hasMany(BumdesModal::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getPengelola()
    {
        return $this->hasMany(BumdesPengelola::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getPengawas()
    {
        return $this->hasMany(BumdesPengawas::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getUnitUsaha()
    {
        return $this->hasMany(BumdesUnitUsaha::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getPotensi()
    {
        return $this->hasMany(BumdesPotensiDesa::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getKomoditas()
    {
        return $this->hasMany(BumdesKomoditas::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getKerjasama()
    {
        return $this->hasMany(BumdesKerjasama::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes'])->
      orderBy(['tahun' => SORT_DESC]);;
    }

    public function getBantuan()
    {
        return $this->hasMany(BumdesBantuan::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getDampak()
    {
        return $this->hasMany(BumdesDampak::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getPemasaran()
    {
        return $this->hasMany(BumdesPemasaranProduk::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getPembukuan()
    {
        return $this->hasMany(BumdesPembukuan::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getPertanggungjawaban()
    {
        return $this->hasMany(BumdesPertanggungjawaban::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getProker()
    {
        return $this->hasMany(BumdesProgramKerja::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    /* zona */

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }

    /* klasifikasi */

    public function getKlasifikasi()
    {
        return $this->hasOne(KlasifikasiBumdes::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getKlasifikasifinal()
    {
        return $this->hasOne(KlasifikasiFinalBumdes::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }
}
