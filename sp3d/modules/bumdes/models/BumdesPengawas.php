<?php
namespace sp3d\modules\bumdes\models;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use sp3d\models\transaksi\Sp3dMasterJabatan;

/**
 * This is the model class for table "bumdes_pengawas".
 *
 * @property string $kd_desa
 * @property int $id_bumdes
 * @property int $id_pengawas
 * @property string $nama
 * @property string $alamat
 * @property string $pendidikan
 * @property string $jabatan
 * @property string $pekerjaan
 * @property string $hp
 * @property string $tipe
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesPengawas extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bumdes_pengawas';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'id_pengawas', 'nama', 'alamat', 'hp'], 'required'],
            [['id_bumdes', 'id_pengawas'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama'], 'string', 'max' => 150],
            [['alamat'], 'string', 'max' => 255],
            [['pendidikan', 'tupoksi'], 'string', 'max' => 8],
            [['pekerjaan'], 'string', 'max' => 100],
            [['jabatan'], 'string', 'max' => 50],
            [['hp'], 'string', 'max' => 16],
            [['tipe'], 'string', 'max' => 4],
            [['kd_desa', 'id_bumdes', 'id_pengawas'], 'unique', 'targetAttribute' => ['kd_desa', 'id_bumdes', 'id_pengawas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'id_bumdes' => 'Id Bumdes',
            'id_pengawas' => 'Id Pengawas',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'pendidikan' => 'Pendidikan',
            'jabatan' => 'Jabatan',
            'pekerjaan' => 'Pekerjaan',
            'hp' => 'Hp',
            'tipe' => 'Tipe',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //event
    public function afterSave($isNew, $old) {
        if ($isNew){  
            
        } else {

        }

        // $connection = Yii::$app->getDb();
        Yii::$app->db->createCommand("UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_kelembagaan vkk ON kb.kd_desa = vkk.kd_desa AND kb.id_bumdes = vkk.id_bumdes SET kb.kelembagaan = (vkk.have_pengawas+vkk.have_penasehat+vkk.have_pengelola+vkk.have_karyawan)
            WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes",  [':kd_desa' => $this->kd_desa, ':id_bumdes' => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();
   }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('id_pengawas');
        if($mx == null){
            $this->id_pengawas = 1;
        } else {
            $this->id_pengawas = $mx +1;
        }
    }

    public function getTipejabatan()
    {
        return $this->hasOne(Sp3dMasterJabatan::className(), ['id' => 'jabatan']);
    }
}
