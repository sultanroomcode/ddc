<?php
namespace sp3d\modules\bumdes\models;
use Yii;

/**
 * This is the model class for table "bumdes_bersama_perkembangan".
 *
 * @property string $kd_kabupaten
 * @property string $id_bb
 * @property int $tahun
 * @property string $total
 * @property string $pendapatan
 * @property string $sumbangsih_pad
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersamaPerkembangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bumdes_bersama_perkembangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_kabupaten', 'id_bb', 'tahun'], 'required'],
            [['tahun'], 'integer'],
            [['total', 'pendapatan', 'sumbangsih_pad'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['kd_kabupaten'], 'string', 'max' => 12],
            [['id_bb'], 'string', 'max' => 15],
            [['created_by', 'updated_by'], 'string', 'max' => 10],
            [['kd_kabupaten', 'id_bb', 'tahun'], 'unique', 'targetAttribute' => ['kd_kabupaten', 'id_bb', 'tahun']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bb' => 'Id Bb',
            'tahun' => 'Tahun',
            'total' => 'Total',
            'pendapatan' => 'Pendapatan',
            'sumbangsih_pad' => 'Sumbangsih Pad',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
