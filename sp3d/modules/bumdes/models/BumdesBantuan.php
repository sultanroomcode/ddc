<?php

namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
/**
 * This is the model class for table "bumdes_bantuan".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_bumdes
 * @property int $id_bantuan
 * @property string $tahun
 * @property string $nama
 * @property string $jenis_bantuan
 * @property string $nilai
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBantuan extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'bumdes_bantuan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'id_bantuan'], 'required'],
            [['id_bumdes', 'id_bantuan'], 'integer'],
            [['nilai'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['tahun'], 'string', 'max' => 4],
            [['nama', 'jenis_bantuan'], 'string', 'max' => 100],
            [['keterangan'], 'string', 'max' => 255],
            [['kd_desa', 'id_bumdes', 'id_bantuan'], 'unique', 'targetAttribute' => ['kd_desa', 'id_bumdes', 'id_bantuan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bumdes' => 'Id Bumdes',
            'id_bantuan' => 'Id Bantuan',
            'tahun' => 'Tahun',
            'nama' => 'Nama Instansi',
            'jenis_bantuan' => 'Jenis Bantuan',
            'nilai' => 'Nilai',
            'keterangan' => 'Keterangan',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['modal', 'sb_modal_desa', 'sb_modal_masyarakat'], 'safe'],
        } 
        $this->nilai = (float) $this->nilai;

        return true;
    } 

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('id_bantuan');
        if($mx == null){
            $this->id_bantuan = 1;
        } else {
            $this->id_bantuan = $mx +1;
        }
    }
}
