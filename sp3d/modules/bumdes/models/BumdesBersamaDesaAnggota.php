<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use sp3d\models\User;
/**
 * This is the model class for table "bumdes_bersama_desa_anggota".
 *
 * @property string $kd_kabupaten
 * @property string $id_bb
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersamaDesaAnggota extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bumdes_bersama_desa_anggota';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kabupaten', 'id_bb', 'kd_desa'], 'required'],
            [['kd_kabupaten', 'kd_desa'], 'string', 'max' => 12],
            [['id_bb'], 'string', 'max' => 15],
            [['kd_kecamatan'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kode Kabupaten',
            'id_bb' => 'ID BUMDesa Bersama',
            'kd_desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kecamatan.description' => 'Nama Kecamatan',
            'desa.description' => 'Nama Desa',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    public function getBumdesbersama()
    {
        return $this->hasOne(BumdesBersama::className(), ['kd_kabupaten' => 'kd_kabupaten', 'id_bb' => 'id_bb']);
    }

    public function getKecamatan()
    {
        return  $this->hasOne(User::className(), ['id' => 'kd_kecamatan']);
    }

    public function getDesa()
    {
        return  $this->hasOne(User::className(), ['id' => 'kd_desa']);
    }
}
