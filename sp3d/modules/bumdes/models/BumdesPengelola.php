<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_pengelola".
 *
 * @property string $kd_desa
 * @property integer $id_bumdes
 * @property integer $id_pengelola
 * @property string $nama
 * @property string $alamat
 * @property string $pendidikan
 * @property string $jabatan
 * @property string $pekerjaan
 * @property string $hp
 * @property string $tipe
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesPengelola extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bumdes_pengelola';
    }

    public function behaviors()
    {
         return [
             // [
             //     'class' => BlameableBehavior::className(),
             //     'createdByAttribute' => 'created_by',
             //     'updatedByAttribute' => 'updated_by',
             // ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes'], 'required'],
            [['id_bumdes', 'id_pengelola'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama'], 'string', 'max' => 150],
            [['alamat'], 'string', 'max' => 255],
            [['pendidikan', 'tupoksi'], 'string', 'max' => 8],
            [['pekerjaan'], 'string', 'max' => 100],
            [['jabatan'], 'string', 'max' => 50],
            [['hp'], 'string', 'max' => 16],
            [['tipe'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kode Desa',
            'id_bumdes' => 'ID Bumdes',
            'id_pengelola' => 'ID Pengelola',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'pendidikan' => 'Pendidikan',
            'jabatan' => 'Jabatan',
            'pekerjaan' => 'Pekerjaan',
            'hp' => 'Nomor HP',
            'tipe' => 'Tipe',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
    //event
    public function afterSave($isNew, $old) {
        if ($isNew){  
            
        } else {

        }

        // $connection = Yii::$app->getDb();
        Yii::$app->db->createCommand("UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_kelembagaan vkk ON kb.kd_desa = vkk.kd_desa AND kb.id_bumdes = vkk.id_bumdes SET kb.kelembagaan = (vkk.have_pengawas+vkk.have_penasehat+vkk.have_pengelola+vkk.have_karyawan)
            WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes",  [':kd_desa' => $this->kd_desa, ':id_bumdes' => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_tenaga_kerja vktk ON kb.kd_desa = vktk.kd_desa AND kb.id_bumdes = vktk.id_bumdes SET kb.naker = vktk.naker_poin
            WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes",  [':kd_desa' => $this->kd_desa, ':id_bumdes' => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();
   }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('id_pengelola');
        if($mx == null){
            $this->id_pengelola = 1;
        } else {
            $this->id_pengelola = $mx +1;
        }
    }

    public function varArr()
    {
        $arr = [
            'KETUA' => 'Ketua',
            'WKETUA' => 'Wakil Ketua',
            'SEKRETARIS' => 'Sekretaris',
            'BENDAHARA' => 'Bendahara',
            'KUNITUSAHA' => 'Kepala Unit Usaha',
        ];

        return $arr;
    }

    public function getTipejabatan()
    {
        return $this->hasOne(Sp3dMasterJabatan::className(), ['id' => 'jabatan']);
    }
}
