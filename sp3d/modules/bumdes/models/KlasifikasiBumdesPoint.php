<?php

namespace sp3d\modules\bumdes\models;

use Yii;

/**
 * This is the model class for table "klasifikasi_bumdes_point".
 *
 * @property string $kode
 * @property int $skor
 * @property string $indikator
 * @property int $point_bobot
 * @property int $nilai_max
 */
class KlasifikasiBumdesPoint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'klasifikasi_bumdes_point';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode', 'skor'], 'required'],
            [['skor', 'point_bobot', 'nilai_max'], 'integer'],
            [['kode'], 'string', 'max' => 50],
            [['indikator'], 'string', 'max' => 255],
            [['kode', 'skor'], 'unique', 'targetAttribute' => ['kode', 'skor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'skor' => 'Skor',
            'indikator' => 'Indikator',
            'point_bobot' => 'Point Bobot',
            'nilai_max' => 'Nilai Max',
        ];
    }
}
