<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "bumdes_pembukuan".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property int $id_bumdes
 * @property int $no
 * @property string $uraian
 * @property string $status_ada
 * @property string $status_benar
 * @property string $pelaporan
 * @property string $bentuk_laporan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesPembukuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bumdes_pembukuan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'no'], 'required'],
            [['id_bumdes', 'no'], 'integer'],
            // [['created_at', 'updated_at'], 'safe'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten', 'status_ada', 'status_benar', 'pelaporan'], 'string', 'max' => 5],
            [['uraian'], 'string', 'max' => 255],
            [['bentuk_laporan'], 'string', 'max' => 20],
            // [['created_by', 'updated_by'], 'string', 'max' => 10],
            [['kd_desa', 'id_bumdes', 'no'], 'unique', 'targetAttribute' => ['kd_desa', 'id_bumdes', 'no']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bumdes' => 'Id Bumdes',
            'no' => 'No',
            'uraian' => 'Jenis Pembukuan',
            'status_ada' => 'Keberadaan',
            'status_benar' => 'Status Pengisian Buku',
            'pelaporan' => 'Status Pelaporan',
            'bentuk_laporan' => 'Bentuk Laporan',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //event

    public function afterSave($isNew, $old) {
        if ($isNew){  
            
        } else {

        }

        // $connection = Yii::$app->getDb();
        Yii::$app->db->createCommand("
            UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_adm_keuangan vkau ON kb.kd_desa = vkau.kd_desa AND kb.id_bumdes = vkau.id_bumdes SET kb.adm_keuangan = (vkau.have_mutasi_kas + vkau.have_laba_rugi + vkau.have_neraca + vkau.have_konsolidasi)
            WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes",  [':kd_desa' => $this->kd_desa, ':id_bumdes' => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();
   }

    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('no');
        if($mx == null){
            $this->no = 1;
        } else {
            $this->no = $mx +1;
        }
    }
}
