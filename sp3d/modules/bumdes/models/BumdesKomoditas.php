<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_komoditas".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $id
 * @property integer $id_komoditas
 * @property string $nama
 * @property string $uraian
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesKomoditas extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'bumdes_komoditas';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'id_komoditas'], 'required'],
            [['id_bumdes', 'id_komoditas'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['satuan'], 'string', 'max' => 10],
            [['jumlah'], 'number'],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama'], 'string', 'max' => 150],
            [['uraian'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_desa' => 'Kd Desa',
            'id_bumdes' => 'ID',
            'id_komoditas' => 'Id Komoditas',
            'nama' => 'Produk Unggulan Desa',
            'uraian' => 'Rencana Pemanfaatan Produk',
            // 'uraian' => 'Uraian Produk Unggulan Desa',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'id_bumdes' => $this->id_bumdes])->max('id_komoditas');
        if($mx == null){
            $this->id_komoditas = 1;
        } else {
            $this->id_komoditas = $mx +1;
        }
    }
}
