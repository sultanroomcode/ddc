<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
/**
 * This is the model class for table "bumdes_bersama_media".
 *
 * @property string $kd_kabupaten
 * @property string $id_bb
 * @property integer $id_media
 * @property string $file_foto
 * @property string $video_url
 * @property string $tipe
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersamaMedia extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $afoto_src;
    public static function tableName()
    {
        return 'bumdes_bersama_media';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kabupaten', 'id_bb', 'id_media'], 'required'],
            [['afoto_src'], 'file', 'extensions' => 'jpg'],
            [['id_media'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['id_bb'], 'string', 'max' => 15],
            [['file_foto', 'video_url'], 'string', 'max' => 50],
            [['tipe'], 'string', 'max' => 10],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bb' => 'Id Bb',
            'id_media' => 'Id Media',
            'file_foto' => 'File Foto',
            'afoto_src' => 'File Foto',
            'video_url' => 'Video Url',
            'tipe' => 'Tipe',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }
    //events
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['omset', 'keuntungan'], 'safe'],
        } 
        $this->upload();
        return true;
    } 

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_kabupaten' => $this->kd_kabupaten, 'id_bb' => $this->id_bb])->max('id_media');
        if($mx == null){
            $this->id_media = 1;
        } else {
            $this->id_media = $mx +1;
        }
    }

    public function upload()
    {
        $this->validate();
        $this->afoto_src = UploadedFile::getInstance($this, 'afoto_src');
        // echo "string ".time();
        // var_dump($this->afoto_src);
        if (isset($this->afoto_src) && $this->afoto_src->size != 0){
            $url = 'userfile/'.$this->kd_kabupaten.'/bumdes-bersama/'.$this->id_bb.'/media/';
            $this->makeDir($url);
            $newname = 'md-'.time().'-'.$this->kd_kabupaten.'.'.$this->afoto_src->extension;
            if($this->isNewRecord){
                $callback = $this->afoto_src->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->file_foto) && $this->file_foto !== '' && $this->file_foto != null){
                    unlink($url.$this->file_foto);
                }
                $callback = $this->afoto_src->saveAs($url.$newname);               
            }
            $this->file_foto = $newname;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
