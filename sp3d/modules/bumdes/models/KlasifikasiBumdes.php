<?php

namespace sp3d\modules\bumdes\models;

use Yii;

/**
 * This is the model class for table "klasifikasi_bumdes".
 *
 * @property string $kd_desa
 * @property int $id_bumdes
 * @property int $legalitas
 * @property int $kelembagaan
 * @property int $adm_keuangan
 * @property int $permodalan
 * @property int $unit_usaha
 * @property int $omset
 * @property int $keuntungan
 * @property int $pad
 * @property int $naker
 * @property int $mitra
 */
class KlasifikasiBumdes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'klasifikasi_bumdes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes'], 'required'],
            [['id_bumdes', 'legalitas', 'kelembagaan', 'adm_keuangan', 'permodalan', 'unit_usaha', 'omset', 'keuntungan', 'pad', 'naker', 'mitra'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_desa', 'id_bumdes'], 'unique', 'targetAttribute' => ['kd_desa', 'id_bumdes']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'id_bumdes' => 'Id Bumdes',
            'legalitas' => 'Legalitas',
            'kelembagaan' => 'Kelembagaan',
            'adm_keuangan' => 'Adm Keuangan',
            'permodalan' => 'Permodalan',
            'unit_usaha' => 'Unit Usaha',
            'omset' => 'Omset',
            'keuntungan' => 'Keuntungan',
            'pad' => 'Pad',
            'naker' => 'Naker',
            'mitra' => 'Mitra',
        ];
    }
}
