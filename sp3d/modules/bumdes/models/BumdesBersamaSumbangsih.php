<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_bersama_sumbangsih".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $id
 * @property string $tahun
 * @property integer $nilai
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersamaSumbangsih extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bumdes_bersama_sumbangsih';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id', 'tahun'], 'required'],
            [['id', 'nilai'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_desa'], 'string', 'max' => 12],
            [['tahun'], 'string', 'max' => 4],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_desa' => 'Kd Desa',
            'id' => 'ID',
            'tahun' => 'Tahun Sumbangsih',
            'nilai' => 'Nilai Sumbangsih',
            'keterangan' => 'Keterangan',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
