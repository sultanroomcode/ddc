<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_modal".
 *
 * @property string $kd_desa
 * @property integer $id_bumdes
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property string $modal
 * @property string $keuntungan
 * @property string $omset
 * @property string $jml_aset
 * @property string $smb_pad
 * @property string $sb_modal_desa
 * @property string $sb_modal_masyarakat
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesModal extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'bumdes_modal';
    }

    public function behaviors()
    {
         return [
             // [
             //     'class' => BlameableBehavior::className(),
             //     'createdByAttribute' => 'created_by',
             //     'updatedByAttribute' => 'updated_by',
             // ],
             // 'timestamp' => [
             //     'class' => 'yii\behaviors\TimestampBehavior',
             //     'attributes' => [
             //         ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
             //         ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
             //     ],
             //     'value' => new \yii\db\Expression('NOW()'),
             // ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_bumdes', 'tahun'], 'required'],
            [['id_bumdes'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['keuntungan', 'jml_aset', 'smb_pad', 'modal', 'omset', 'sb_modal_desa', 'sb_modal_masyarakat'], 'safe'],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kode Desa',
            'tahun' => 'Tahun',
            'jml_aset' => 'Jumlah Total Aset yang Dimiliki BUMDesa',
            'smb_pad' => 'Sumbangan PAD',
            'keuntungan' => 'Keuntungan Bersih Yang Dihasilkan Unit Usaha',
            'id_bumdes' => 'ID Bumdes',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_kabupaten' => 'Kode Kabupaten',
            'modal' => 'Total Modal',
            'omset' => 'Keuntungan Kotor Yang Dihasilkan Unit Usaha',
            'sb_modal_desa' => 'Penyertaan Modal Desa',
            'sb_modal_masyarakat' => 'Penyertaan Modal Masyarakat',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    //event
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['modal', 'sb_modal_desa', 'sb_modal_masyarakat'], 'safe'],
        } 
        $this->modal = (float) $this->modal;
        $this->sb_modal_masyarakat = (float) $this->sb_modal_masyarakat;
        $this->sb_modal_desa = (float) $this->sb_modal_desa;

        $this->omset = (float) $this->omset;
        $this->jml_aset = (float) $this->jml_aset;
        $this->keuntungan = (float) $this->keuntungan;
        $this->smb_pad = (float) $this->smb_pad;

        return true;
    } 

    public function afterSave($isNew, $old) {
        if ($isNew){  
            
        } else {

        }

        // $connection = Yii::$app->getDb();
        Yii::$app->db->createCommand("
            UPDATE klasifikasi_bumdes kb JOIN v2_klasifikasi_uang vku ON kb.kd_desa = vku.kd_desa AND kb.id_bumdes = vku.id_bumdes SET 
                kb.permodalan = vku.modal_poin,
                kb.omset = vku.omset_poin,
                kb.keuntungan = vku.keuntungan_poin,
                kb.pad = vku.smb_pad_poin
            WHERE kb.kd_desa = :kd_desa AND kb.id_bumdes = :id_bumdes",  [':kd_desa' => $this->kd_desa, ':id_bumdes' => $this->id_bumdes])->execute();

        Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $this->kd_desa, ":id_bumdes" => $this->id_bumdes])->execute();
   }

    public function getBumdes()
    {
        return $this->hasOne(Bumdes::className(), ['kd_desa' => 'kd_desa', 'id_bumdes' => 'id_bumdes']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten']);
    }

    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa']);
    }
}
