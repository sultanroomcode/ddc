<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_bersama_modal".
 *
 * @property string $kd_kabupaten
 * @property string $id_bb
 * @property string $tahun
 * @property string $kd_kecamatan
 * @property string $modal
 * @property string $sb_modal_desa
 * @property string $sb_modal_masyarakat
 * @property string $sb_pad
 * @property string $pendapatan_bumdes
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersamaModal extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'bumdes_bersama_modal';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kabupaten', 'id_bb', 'tahun'], 'required'],
            [['modal', 'sb_modal_desa', 'sb_modal_masyarakat', 'sb_pad', 'pendapatan_bumdes'], 'safe'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            [['id_bb'], 'string', 'max' => 15],
            [['tahun'], 'string', 'max' => 4],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'id_bb' => 'ID BUMDesa Bersama',
            'tahun' => 'Tahun',
            'kd_kecamatan' => 'Kd Kecamatan',
            'modal' => 'Modal',
            'sb_modal_desa' => 'Sumber Modal Desa',
            'sb_modal_masyarakat' => 'Sumber Modal Masyarakat',
            'sb_pad' => 'Sumber PAD (Pendapatan Asli Desa)',
            'pendapatan_bumdes' => 'Pendapatan Bumdes',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    //event
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            // [['modal', 'sb_modal_desa', 'sb_modal_masyarakat', 'sb_pad', 'pendapatan_bumdes'], 'safe'],
        } 
        $this->modal = (float) $this->modal;
        $this->sb_modal_masyarakat = (float) $this->sb_modal_masyarakat;
        $this->sb_modal_desa = (float) $this->sb_modal_desa;
        $this->sb_pad = (float) $this->sb_pad;
        $this->pendapatan_bumdes = (float) $this->pendapatan_bumdes;


        return true;
    } 
}
