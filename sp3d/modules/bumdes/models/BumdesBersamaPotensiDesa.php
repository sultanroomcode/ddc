<?php
namespace sp3d\modules\bumdes\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "bumdes_bersama_potensi_desa".
 *
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $id
 * @property integer $id_potensi
 * @property string $nama
 * @property string $uraian
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class BumdesBersamaPotensiDesa extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'bumdes_bersama_potensi_desa';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kabupaten', 'id_bb', 'id_potensi'], 'required'],
            [['id_bb', 'id_potensi', 'unit'], 'integer'],
            [['kd_kabupaten'], 'string', 'max' => 5],
            
            [['luas'], 'number'],
            [['jenis'], 'string', 'max' => 100],
            [['kategori'], 'string', 'max' => 50],
            [['alamat'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd Kabupaten',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_desa' => 'Kd Desa',
            'id_bb' => 'ID',
            'id_potensi' => 'Id Potensi',
            'kategori' => 'Kategori Potensi',
            'jenis' => 'Jenis Potensi',
            'luas' => 'Luas Wilayah Potensi',
            'unit' => 'Jumlah Unit',
            'alamat' => 'Alamat',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_kabupaten' => $this->kd_kabupaten, 'id_bb' => $this->id_bb])->max('id_potensi');
        if($mx == null){
            $this->id_potensi = 1;
        } else {
            $this->id_potensi = $mx +1;
        }
    }
}
