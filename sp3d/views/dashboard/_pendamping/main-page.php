<?php
use frontend\models\referensi\RefKecamatan;
use frontend\models\referensi\RefDesa;
$id = Yii::$app->user->identity->chapter->Kd_Desa;
$data = RefDesa::find()->where(['Kd_Desa' => $id])->one();
?>
<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/kecamatan?id=<?=$data->kecamatan->Kd_Kec?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-primary">Kembali</a>
<a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/default/desa?id=<?=$id?>&tahun=<?=$tahun?>'})" class="btn btn-sm btn-primary"><i class="fa fa-refresh fa-spin"></i> Refresh</a>
<h2><?= substr($data->kecamatan->Nama_Kecamatan,10) ?> - <?= substr($data->Nama_Desa, 11) ?> | <?= $tahun ?></h2>

<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="info-box bg-green">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Anggaran</a></span>
				<span class="info-box-number">41,410</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					70% Increase in 30 Days
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-orange">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Anggaran</a></span>
				<span class="info-box-number">41,410</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					70% Increase in 30 Days
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>

		<div class="info-box bg-red">
			<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>

			<div class="info-box-content">
				<span class="info-box-text"><a href="javascript:void(0)" onclick="goLoad({url:'/transaksi/default/kecamatan'})" class="btn btn-danger btn-xs"><i class="fa fa-map-marker"></i> Anggaran</a></span>
				<span class="info-box-number">41,410</span>

				<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
				</div>
				<span class="progress-description">
					70% Increase in 30 Days
				</span>
			</div>
			<!-- /.info-box-content -->
		</div>
	</div>
	<!-- /.info-box -->
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Bar Chart</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="chart">
					<canvas id="barChart" style="height:230px"></canvas>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- /.row -->