<?php
/* @var $this yii\web\View */
$this->title = 'PusDaDesa | Dashboard';
?>
<?= $this->render('_header-nav') ?>
<?= $this->render('_aside') ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?= Yii::$app->user->identity->viewRole() ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" id="stc-top-box">
      <?= $this->render('_'.Yii::$app->user->identity->viewRole().'/head-page') ?>

      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12 connectedSortable" id="top-main-panel">
          <div id="main-container"></div>
        </section>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
//$this->registerCssFile('//localhost/local-cdn/bower_components/datatables/media/css/dataTables.bootstrap.min.css');
$cssf =<<<CSS
a.linkto {
    color: #fff;
}
CSS;
$this->registerCss($cssf);
if(Yii::$app->params['offline']){
  $this->registerCssFile('//localhost/local-cdn/bower_components/sweetalert/dist/sweetalert.css');
  $this->registerCssFile('//localhost/local-cdn/DataTables/datatables.min.css');
  $this->registerCssFile('//localhost/local-cdn/bower_components/animate.css/animate.min.css');
  $this->registerCssFile('//localhost/local-cdn/vakata-jstree/dist/themes/default/style.min.css');
  $this->registerCssFile('//localhost/local-cdn/bower_components/custombox/dist/custombox.min.css');
  $this->registerCssFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css');

  $this->registerJsFile('//localhost/local-cdn/DataTables/datatables.min.js');
  $this->registerJsFile('//localhost/local-cdn/bower_components/sweetalert/dist/sweetalert.min.js');
  $this->registerJsFile('//localhost/local-cdn/jquery.bootstrap-growl.min.js');
  $this->registerJsFile('//localhost/local-cdn/vakata-jstree/dist/jstree.min.js');
  $this->registerJsFile('//localhost/local-cdn/bower_components/custombox/dist/custombox.min.js');
  $this->registerJsFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js');
  $this->registerJsFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/js/i18n/defaults-id_ID.min.js');
  $this->registerJsFile('//localhost/local-cdn/echarts.min.js');
  $this->registerJsFile('js/jquery.form.min.js');  
} else {
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css');
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/dataTables.bootstrap.min.css');
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css');
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.4/themes/default/style.min.css');
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/custombox/4.0.3/custombox.min.css');
  $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css');

  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/jquery.dataTables.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.4/jstree.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/custombox/4.0.3/custombox.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-id_ID.min.js');
  $this->registerJsFile('http://files.mediatuban.com/echarts.min.js');
  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js');
}
