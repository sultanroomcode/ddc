<li class="dropdown active">
    <a class="sa-side-home" href="javascript:void(0)" onclick="goLoad({url: '/umum/default/dashboard'})">
        <span class="menu-item">Dashboard</span>
    </a>
</li>
<li class="dropdown">
    <a class="sa-side-chart" href="javascript:void(0)">
        <span class="menu-item">Statistik</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/other-stat?type=provinsi'})">Lainnya</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/pendamping-stat?type=provinsi'})">Pendamping</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2018&type=provinsi'})">2018</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2017&type=provinsi'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2016&type=provinsi'})">2016</a></li>
    </ul>
</li>

<li class="dropdown">
    <a class="sa-side-laporan" href="javascript:void(0)">
        <span class="menu-item">Laporan</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2018'})">2018</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2017'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2016'})">2016</a></li>
    </ul>
</li>

<li>
    <a class="sa-side-user" href="javascript:void(0)" onclick="goLoad({url: '/umum/user/management'})">
        <span class="menu-item">User</span>
    </a>
</li>

<li class="dropdown">
    <a class="sa-side-user" href="javascript:void(0)" onclick="goLoad({url: '/umum/user/management'})">
        <span class="menu-item">Tool</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/provinsi-mdb-list'})">List Upload MDB</a></li>
    </ul>
</li>

<li>
    <a class="sa-side-refresh" href="javascript:void(0)" onclick="goLoad({url: '/umum/import/provinsi-normalize-page'})">
        <span class="menu-item">Refresh Hitungan</span>
    </a>
</li>

<li>
    <a class="sa-side-history" href="javascript:void(0)" onclick="goLoad({url: '/umum/user/log'})">
        <span class="menu-item">Log</span>
    </a>
</li>