<li class="dropdown active">
    <a class="sa-side-home" href="javascript:void(0)" onclick="goLoad({url: '/umum/default/dashboard'})">
        <span class="menu-item">Dashboard</span>
    </a>
</li>

<li class="dropdown">
    <a class="sa-side-kode-referensi" href="javascript:void(0)" onclick="goLoad({url: '/referensi/kode-referensi/kabupaten'})">
        <span class="menu-item">Kode Referensi</span>
    </a>
</li>

<li class="dropdown">
    <a class="sa-side-chart" href="javascript:void(0)">
        <span class="menu-item">Statistik</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/other-stat?type=kabupaten'})">Lainnya</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/pendamping-stat?type=kabupaten'})">Pendamping</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2018&type=kabupaten'})">2018</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2017&type=kabupaten'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2016&type=kabupaten'})">2016</a></li>
    </ul>
</li>

<li class="dropdown">
    <a class="sa-side-laporan" href="javascript:void(0)">
        <span class="menu-item">Laporan</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2018'})">2018</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2017'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2016'})">2016</a></li>
    </ul>
</li>

<li>
    <a class="sa-side-history" href="javascript:void(0)" onclick="goLoad({url: '/umum/user/log'})">
        <span class="menu-item">Catatan Aktivitas</span>
    </a>
</li>