<li class="dropdown active">
    <a class="sa-side-data-pemerintahan-desa" href="javascript:void(0)" onclick="goLoad({url: '/umum/default/dashboard'})">
        <span class="menu-item">Data Pemerintahan Desa</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/ta-kepala-desa/list-kepala-desa'})">Data Kepala Desa</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/sp3d-pendamping-desa/list-pendamping'})">Data Pendamping Desa</a></li>
    </ul>
</li>

<li class="dropdown">
    <a class="sa-side-perencanaan-desa" href="javascript:void(0)" onclick="goLoad({url: '/umum/default/dashboard'})">
        <span class="menu-item">perencanaan Desa</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/ta-kepala-desa/list'})">RPJM Desa</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-list'})">Import Siskeudes</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/default/dashboard'})">APBDes</a></li>
    </ul>
</li>

<li class="dropdown">
    <a class="sa-side-potensi-desa" href="javascript:void(0)" onclick="goLoad({url: '/umum/default/dashboard'})">
        <span class="menu-item">Potensi Desa</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/ta-kepala-desa/list-kepala-desa'})">Batas Wilayah</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/sp3d-pendamping-desa/list-pendamping'})">Potensi</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/ta-kepala-desa/list'})">SDM</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/transaksi/ta-kepala-desa/list'})">Kelembagaan BUMDES</a></li>
    </ul>
</li>

<!-- <li class="dropdown">
    <a class="sa-side-import" href="javascript:void(0)">
        <span class="menu-item">Import</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/mdb-list'})">Siskeudes</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/excell-list'})">XLS</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/import/api-sync'})">Sync API</a></li> 
    </ul>
</li>
-->
<li class="dropdown">
    <a class="sa-side-chart" href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=null'})">
        <span class="menu-item">Statistik</span>
    </a>
    <!-- <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index'})">Beranda</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2017'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2016'})">2016</a></li>
    </ul> -->
</li>

<li class="dropdown">
    <a class="sa-side-laporan" href="javascript:void(0)">
        <span class="menu-item">Laporan</span>
    </a>
    <!-- <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index'})">2018</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2017'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2016'})">2016</a></li>
    </ul> -->
</li>

<li>
    <a class="sa-side-download" href="javascript:void(0)" onclick="goLoad({url: '/umum/user/download'})">
        <span class="menu-item">Download</span>
    </a>
</li>

<li>
    <a class="sa-side-history" href="javascript:void(0)" onclick="goLoad({url: '/umum/user/log'})">
        <span class="menu-item">Catatan Aktivitas</span>
    </a>
</li>