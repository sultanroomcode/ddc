<li class="dropdown active">
    <a class="sa-side-home" href="javascript:void(0)" onclick="goLoad({url: '/umum/default/dashboard'})">
        <span class="menu-item">Beranda</span>
    </a>
</li>
<li class="dropdown">
    <a class="sa-side-chart" href="javascript:void(0)">
        <span class="menu-item">Statistik</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/other-stat?type=kecamatan'})">Lainnya</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/pendamping-stat?type=kecamatan'})">Pendamping</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2018&type=kecamatan'})">2018</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2017&type=kecamatan'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/statistik/index?tahun=2016&type=kecamatan'})">2016</a></li>
    </ul>
</li>

<li class="dropdown">
    <a class="sa-side-laporan" href="javascript:void(0)">
        <span class="menu-item">Laporan</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2018'})">2018</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2017'})">2017</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/laporan/index?tahun=2016'})">2016</a></li>
    </ul>
</li>

<!-- <li class="dropdown">
    <a class="sa-side-verifikasi" href="javascript:void(0)" onclick="goLoad({url: '/umum/verifikasi'})">
        <span class="menu-item">Verifikasi</span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/verifikasi'})">Daftar Verifikasi</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/verifikasi/list-verifikator'})">Daftar Verifikator</a></li>
        <li><a href="javascript:void(0)" onclick="goLoad({url: '/umum/verifikasi/tambah-verifikator'})">Tambah Verifikator</a></li>
    </ul>
</li> -->

<li>
    <a class="sa-side-history" href="javascript:void(0)" onclick="goLoad({url: '/umum/user/log'})">
        <span class="menu-item">Catatan Aktivitas</span>
    </a>
</li>