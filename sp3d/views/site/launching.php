<?php
use yii\helpers\Url;
Url::to(['site/force-login'], true);
$this->title = 'Launching SP3D Pemerintah Provinsi Jawa Timur';
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12" id="master">
			<div id="header"><img src="../header.png" style="width: 70%"></div>
			<div id="main"><p id="hellolaunch" style="display: none;">Wait <span id="count">5</span> secs</p>
			<a href="javascript:void(0)" onclick="clicktolaunch()" id="counting" class="btn btn-lg btnlarger btn-danger">MULAI</a></div>
			<div id="footer"><img src="../foot.png" style="width: 70%"></div>
		</div>
	</div>
</div>

<?php
$this->registerCssFile('@web/css/launching.css');
$scriptcss =<<<CSS
.btn, .btnlarger {
	font-size: 100px;
}
CSS;
$script =<<<JS
var counter = 5;
function clicktolaunch(){
	var counter = 5;
	//$('#hellolaunch').show();
	setInterval(function() {
	counter--;
	if (counter >= 0) {
	  span = document.getElementById("counting");
	  span.innerHTML = counter;
	}
	// Display 'counter' wherever you want to display it.
	if (counter === 0) {
		// $('#hellolaunch').hide();
	    //alert('this is where it happens');
	    // document.location = 'http://sp3d.dpmd.jatimprov.go.id/site/fl';
	    document.location = 'http://localhost/yii/siapatakut_sv/sp3d/web/site/fl';
	    clearInterval(counter);
	}

	}, 1000);
	return false;
}
JS;
$this->registerCss($scriptcss);
$this->registerJs($script, \yii\web\View::POS_BEGIN);