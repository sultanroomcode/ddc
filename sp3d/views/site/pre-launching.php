<?php
$this->title = 'Pre-Launching SP3D Pemerintah Provinsi Jawa Timur';
?>
<div class="fullwidth clearfix">
	<div id="topcontainer" class="bodycontainer clearfix" data-uk-scrollspy="{cls:'uk-animation-fade', delay: 300, repeat: true}">
		<p><img src="../logo.png"></p>
		<h1><span>SP3D</span><br />Dinas Pemberdayaan Masyarakat dan Desa<br>Provinsi Jawa Timur</h1>
		<p>Launching kurang ... </p>		
	</div>
</div>

<div class="fullwidth colour1 clearfix">
	<div id="countdown" class="bodycontainer clearfix" data-uk-scrollspy="{cls:'uk-animation-fade', delay: 300, repeat: true}">

		<div id="countdowncont" class="clearfix">
			<ul id="countscript">
				<li>
					<span class="days">00</span>
					<p>Hari</p>
				</li>
				<li>
					<span class="hours">00</span>
					<p>Jam</p>
				</li>
				<li class="clearbox">
					<span class="minutes">00</span>
					<p>Menit</p>
				</li>
				<li>
					<span class="seconds">00</span>
					<p>Detik</p>
				</li>
			</ul>
		</div>
	
	</div>
</div>

<div class="fullwidth clearfix">
	<div id="footercont" class="bodycontainer clearfix" data-uk-scrollspy="{cls:'uk-animation-fade', delay: 300, repeat: true}">		
		<p>SP3D <a title="download website templates" href="http://www.downloadwebsitetemplates.co.uk" rel="external">Dispemas Jatim</a></p>
	</div>
</div>
<?php
$this->registerCssFile('@web/css/pre-launching.css');
$this->registerJsFile('@web/js/countdown.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJsFile('@web/js/countdown-scripts.js', ['depends' => \yii\web\JqueryAsset::className()]);