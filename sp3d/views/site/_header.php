<header>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="topleft-info">
                        <li><i class="fa fa-phone"></i> +62 ... ... ...</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Cari dengan kata kunci..." type="text" value="" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value="">
                            <span class="sb-icon-search" title="Click to start searching"></span>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                <a class="navbar-brand" href=""><img src="css/Sailor/img/logo.png" alt="" width="199" height="52" /></a>
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                    <li class="dropdown active">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Beranda <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Beranda slider 1</a></li>
                            <li><a href="#">Beranda slider 2</a></li>

                        </ul>

                    </li>
                    <li><a href="portfolio.html">Tentang Kami</a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Blog <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="blog-rightsidebar.html">Postingan 1</a></li>
                            <li><a href="blog-leftsidebar.html">Postingan 2</a></li>
                            <li><a href="post-rightsidebar.html">Postingan 3</a></li>
                            <li><a href="post-leftsidebar.html">Postingan 4</a></li>
                        </ul>
                    </li>
                    <li><a href="contact.html">Kontak Kami</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>