<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Data Desa Center | Provinsi Jawa Timur';
$urllog = 'http://localhost/ddc/sp3d/web/';
if(Yii::$app->params['offline'] == false){
  $urllog = 'http://datadesacenter.dpmd.jatimprov.go.id/';
}
?>
  <div class="page">
      <header class="page-head">
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-corporate-light" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="53px" data-xl-stick-up-offset="53px" data-xxl-stick-up-offset="53px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="bg-white context-dark novi-background bg-cover">
              <div class="rd-navbar-inner">
                <div class="rd-navbar-aside-wrap">
                  <div class="rd-navbar-aside">
                    <div class="rd-navbar-aside-toggle" data-rd-navbar-toggle=".rd-navbar-aside"><span></span></div>
                    <div class="rd-navbar-aside-content">
                      <ul class="rd-navbar-aside-group list-units">
                        <li>
                          <div class="unit flex-row unit-spacing-xs align-items-center">
                            <div class="unit-left"><span class="novi-icon icon icon-xxs icon-primary fa-clock-o"></span></div>
                            <div class="unit-body"><span class="time text-dark">Data Desa Center - Provinsi Jatim</span></div>
                          </div>
                        </li>
                        <li>
                          <div class="unit flex-row unit-spacing-xs align-items-center">
                            <div class="unit-left"><span class="novi-icon icon icon-xxs icon-primary fa-map-marker"></span></div>
                            <div class="unit-body"><a class="link-secondary text-dark" href="#">JL. AHMAD YANI NO. 152C</a></div>
                          </div>
                        </li>
                        <li>
                          <div class="unit flex-row unit-spacing-xs align-items-center">
                            <div class="unit-left"><span class="novi-icon icon icon-xxs icon-primary fa-phone"></span></div>
                            <div class="unit-body"><a class="link-secondary text-dark" href="#">Telp. (031) 8292591</a></div>
                          </div>
                        </li>
                      </ul>
                      <div class="rd-navbar-aside-group">
                        <ul class="list-inline list-inline-reset">
                          <li><a class="novi-icon icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-facebook" href="#"></a></li>
                          <li><a class="novi-icon icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-twitter" href="#"></a></li>
                          <li><a class="novi-icon icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-google-plus" href="#"></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="novi-background bg-cover shadow">
              <div class="rd-navbar-inner ">
                <div class="rd-navbar-group">
                  <div class="rd-navbar-panel">
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button><a class="rd-navbar-brand brand" href="index.html"><img src="<?= $urllog ?>images/logo/SP3D.png" alt="" width="430" height="149"/></a>
                  </div>
                  <div class="rd-navbar-group-asside">
                    <div class="rd-navbar-nav-wrap">
                      <div class="rd-navbar-nav-inner">
                        
                          <a href="login.php" class="btn btn-xs btn-info float-right text-white mr-2">Login  <i class="fa fa-sign-in"></i></a>
                          <a href="" class="btn btn-xs button-primary float-right text-white mr-2">Dashboard <i class="fa fa-dashboard"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>

      <section>
        <div class="swiper-container swiper-slider swiper-variant-1 bg-black" data-loop="true" data-autoplay="5000" data-simulate-touch="true">
          <div class="swiper-wrapper text-center">
            <div class="swiper-slide overlay-5" data-slide-bg="<?= $urllog ?>images/logo/background.png">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row row-fix justify-content-md-center">
                    <div class="col-md-11 col-lg-10 col-xl-9 ">
                      <div class="slide-logo text-center" data-caption-animate="fadeInUp" data-caption-delay="0s"><img src="<?= $urllog ?>images/logo/logo-provinsi.png">
                      </div><br>
                      <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s">Selamat Datang</div>
                      <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s">DATA DESA CENTER</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide overlay-5" data-slide-bg="<?= $urllog ?>images/logo/background.png">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row row-fix justify-content-md-center">
                    <div class="col-xl-1 d-none d-xl-inline-block d-xxl-none"></div>
                    <div class="col-md-11 col-lg-10 col-xl-9 ">
                      <div class="slide-logo text-center" data-caption-animate="fadeInUp" data-caption-delay="0s"><img src="<?= $urllog ?>images/logo/logo-provinsi.png">
                      </div><br>
                      <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s">Selamat Datang</div>
                      <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s">DATA DESA CENTER</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-scrollbar d-xl-none"></div>
          <div class="swiper-nav-wrap">
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
          </div>
        </div>
      </section>

      <section class="section-50 section-md-80 novi-background bg-cover">
        <div class="container text-center">
          <h3 class="text-dark">Data Desa Center</h3>
          <p class="text-gray-05 text-18"><b>Data Desa Center</b> adalah himpunan data Desa di Jawa Timur ( <b>7.724</b> Desa, <b>602</b> Kecamatan, <b>29</b> Kabupaten dan <b>1</b> Kota Batu), yang berisi folder data : (1) Data Perencanaan Desa/APBDes, (2) Data Potensi Desa, (3) Data Lembaga Desa, (4) Data Lembaga Kemasyarakatan Desa, (5) Data Posyandu, (6) Data Lembaga Adat Desa, (7) Data BUMDes, (8) Data KPM Desa, (9) Data Aset Desa, dan lainnya. Data Desa Center sebagai aplikasi penghumpunan data desa guna pengamanan data untuk transparansi dan akuntabilitas percepatan pembangunan dan pemberdayaan masyarakat di Jawa Timur.</p>
          <br>
          <div class="row row-40 row-offset-3">
            <div class="col-md-6 col-lg-4 height-fill">
              <article class="icon-box shadow">
                <div class="box-top">
                  <div class="box-icon"><span class="novi-icon icon icon-primary icon-lg-bigger mercury-ico mercury-icon-card"></span></div>
                  <div class="box-header">
                    <h5>Posyandu</h5>
                  </div>
                </div>
                <div class="divider"></div>
                <div class="box-body">
                  <p class="text-gray-05">Posyandu sebagai wadah peran serta masyarakat yang menyelenggarakan sistem pelayanan bidang kesehatan.</p>
                  <a href="" class="btn btn-info btn-xs mt-3 btn-block">Login</a>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-lg-4 height-fill">
              <article class="icon-box shadow">
                <div class="box-top">
                  <div class="box-icon"><span class="novi-icon icon icon-primary icon-lg-bigger mercury-ico mercury-icon-group"></span></div>
                  <div class="box-header">
                    <h5>Kader Pemberdayaan Masyarakat</h5>
                  </div>
                </div>
                <div class="divider"></div>
                <div class="box-body">
                  <p class="text-gray-05">Mendorong partisipasi masyarakat untuk terlibat secara aktif dalam proses pembangunan desa.</p>
                  <a href="" class="btn btn-info btn-xs mt-3 btn-block">Login</a>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-lg-4 height-fill mx-auto">
              <article class="icon-box shadow">
                <div class="box-top">
                  <div class="box-icon"><span class="novi-icon icon icon-primary icon-lg-bigger mercury-ico mercury-icon-house"></span></div>
                  <div class="box-header">
                    <h5>BUMDesa</h5>
                  </div>
                </div>
                <div class="divider"></div>
                <div class="box-body">
                  <p class="text-gray-05">badan usaha milik desa yang didirikan atas dasar kebutuhan dan potensi desa untuk meningkatkan kesejahteraan masyarakat.</p>
                  <a href="" class="btn btn-info btn-xs mt-3 btn-block">Login</a>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>

      <section class="section-50 novi-background bg-cover">
        <div class="container text-center">
          <div class="row">
            
            <div class="col-lg-3 col-md-6 col-sm-6 mt-3">
               <select class="custom-select custom-select-sm" id="tahun-anggaran">
                <option value="2019" selected="selected">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
              </select>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-3">
               <select class="custom-select custom-select-sm" id="kabupaten-input">
                <option value="" selected="selected">Semua</option>
              </select>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-3">
               <select class="custom-select custom-select-sm" id="kecamatan-input">
                <option value="" selected="selected">Semua</option>
              </select>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-3">
               <select class="custom-select custom-select-sm" id="desa-input">
                <option value="" selected="selected">Semua</option>
              </select>
            </div>
            
          </div>
          <br>
          <a href="" class="btn btn-success btn-sm mt-3">Proses</a>
        </div>
      </section>

      <section>
        <div class="section parallax-container bg-black" data-parallax-img="images/bg-image-3.jpg">
          <div class="parallax-content">
            <div class="section-60 section-md-100 overlay-9">
              <div class="container">
                <h3 class="text-center">Informasi Data</h3>
                <div class="row row-30 row-offset-1 align-items-sm-end">
                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-money-3 "></span>
                      <div class="text-large counter" id="count-pendapatan"></div>
                      <h5 class="box-header">Perencanaan Pendapatan</h5>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-money"></span>
                      <div class="text-large counter" id="count-belanja"></div>
                      <h5 class="box-header">Perencanaan Pembelanjaan </h5>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-chart-up-2"></span>
                      <div class="text-large counter" id="count-pembiayaan"></div>
                      <h5 class="box-header">Perencanaan Pembiayaan</h5>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-social"></span>
                      <div class="text-large counter counter" id="count-desa-input"></div>
                      <h5 class="box-header">Desa Sudah Entri</h5>
                    </div>
                  </div>
                </div>

               
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section-50 section-md-80 novi-background bg-cover">
        <div class="container">
          <h3 class="text-center">Informasi Data
          <div class="divider mt-3"></div>
          </h3>

          <!-- KELEMBAGAAN -->
          <div class="row">
            <div class="col-12 mt-3">
              <h5 class="text-dark">KELEMBAGAAN</h5>
              <p class="text-gray-05">Informasi grafik berdasarkan data Pendidikan </p>
            </div>
              <div class="col-md-4">
                <h6 class="text-gray-05"> <span class="icon novi-icon icon-primary mercury-icon-user"></span>Kepala Desa</h6>
                <div class="box shadow">
                </div>
              </div>
              <div class="col-md-4">
                <h6 class="text-gray-05"> <span class="icon novi-icon icon-primary mercury-icon-user"></span>Perangkat</h6>
                <div class="box shadow">
                  
                </div>
              </div>
              <div class="col-md-4">
                <h6 class="text-gray-05"> <span class="icon novi-icon icon-primary mercury-icon-user"></span>B P D</h6>
                <div class="box shadow">
                  
                </div>
              </div>
          </div>

          <!-- RAPBDES -->
          <div class="row">
            <div class="col-12 mt-3">
              <h5 class="text-dark mb-2">RAPBDES</h5>
            </div>
            <div class="col-md-12">
              <!-- <p class="text-gray-05">Grafik Unit Usaha</p> -->
              <div class="box shadow"></div>
            </div>
          </div>

          <!-- BUMDES -->
          <div class="row">
            <div class="col-12 mt-3">
              <h5 class="text-dark mb-2">BUMDES</h5>
            </div>
            <div class="col-md-6">
              <p class="text-gray-05">Grafik Unit Usaha</p>
              <div class="box shadow"></div>
            </div>
            <div class="col-md-6">
              <p class="text-gray-05">Kabupaten</p>
              <div class="box shadow"></div>
            </div>
          </div>

          <!-- KLINIK BUMDES -->
          <div class="row">
            <div class="col-12 mt-3">
              <h5 class="text-dark mb-2">KLINIK BUMDES</h5>
            </div>
            <div class="col-md-12">
              <p class="text-gray-05">Kurikulum</p>
              <div class="box shadow"></div>
            </div>
          </div>

          <!-- DATA KPM -->
          <div class="row">
            <div class="col-12 mt-3">
              <h5 class="text-dark mb-2">DATA KPM</h5>
            </div>
            <div class="col-md-12">
              <p class="text-gray-05">Kader Pemberdayaan Masyarakat</p>
              <div class="box shadow"></div>
            </div>
          </div>


        </div>
      </section>


      <!-- <section>
          <div class="cover">
            <img src="<?= $urllog ?>images/logo/cover.png">
          </div>
      </section> -->

      <section>
        <div class="section parallax-container bg-black" data-parallax-img="images/bg-image-3.jpg">
          <div class="parallax-content">
            <div class="section-60 section-md-100 overlay-9">
              <div class="container">
                <h3 class="text-center">Informasi Pengguna</h3>

                 <div class="row row-30 row-offset-1 align-items-sm-end">
                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-user "></span>
                      <div class="text-large counter" id="ga-counter-now">0</div>
                      <h5 class="box-header">User Online </h5>
                    </div>
                  </div>

                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-time"></span>
                      <div class="text-large counter" id="ga-counter-today">0</div>
                      <h5 class="box-header">Jumlah Kunjungan Hari Ini </h5>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-case"></span>
                      <div class="text-large counter" id="ga-counter-week">0</div>
                      <h5 class="box-header">Jumlah Kunjungan Satu Minggu Terakhir </h5>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="box-counter"><span class="icon novi-icon icon-primary icon-lg mercury-icon-case"></span>
                      <div class="text-large counter" id="ga-counter-month">0</div>
                      <h6 class="box-header">Jumlah Kunjungan Satu Bulan Terakhir </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="section-60 section-md-80 section-lg-90 section-lg-top-30 section-lg-bottom-0 novi-background">
        <div class="container">
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </div>
      </section>

      <footer class="section-single-footer novi-background bg-cover bg-ebony-clay context-dark">
        <div class="container text-center">
          <div class="row row-fix">
            <div class="col-12">
              <p class="">
                <p>DINAS PEMBERDAYAAN MASYARAKAT DAN DESA PROVINSI JAWA TIMUR</p>
                <span>JL. AHMAD YANI NO. 152C Telp. (031) 8292591</span><br>
                <span>Surabaya Jawa Timur</span><br>
                <span>Copyright &nbsp;&#169;&nbsp;</span><span class="copyright-year"></span><span> Data Desa Center</span>
              </p>
            </div>
          </div>
        </div>
      </footer>

    </div>
    <div class="snackbars" id="form-output-global"></div>
<?php
$tahuntahun = '';
$script =<<<JS
function getDataProvinsi(tahun)
{
    $.getJSON(base_url+'/data-umum/data-count-provinsi?tahun='+tahun, function(res) {
        if(res != ''){
            $('#count-dana-desa').html(res.dana_desa);
            $('#count-pendapatan').html(res.pendapatan);
            $('#count-belanja').html(res.belanja);
            $('#count-pembiayaan').html(res.pembiayaan);
            $('#count-desa-input').html(res.desa_input);
        }
    });   
}

function getDataAnalitik()
{
    $.getJSON('http://artechindonesia.co.id/ga-ddc-public/', function(res) {
        if(res != ''){
            result = res.result;
            $('#ga-counter-now').html(result.total_visitor_now);
            $('#ga-counter-today').html(result.total_visitor_today);
            $('#ga-counter-week').html(result.total_visitor_week);
            $('#ga-counter-month').html(result.total_visitor_month);
        }
    });

    /*$.ajax({
         url:"http://localhost:8989/",
         dataType: 'json', // Notice! JSONP <-- P (lowercase)
         success:function(json){
             // do stuff with json (in this case an array)
             alert("Success");
         },
         error:function(){
             alert("Error");
         }      
    });*/
}

$(window).scroll(function() {
    var wScroll = $(this).scrollTop();
    if (wScroll > 200){
    
        $('.triggbtn').css(
        {
            'display' : 'none'
        })

    } else {

        $('.triggbtn').css(
        {
            'display' : 'block'
        })
    }
});

new WOW().init();
$('.data').DataTable({
  "language": {
      "decimal": ",",
      "thousands": "."
  }
});

$('form#data-provinsi-form').submit(function(){
    var forminput = $(this).serialize();
    //$('div#form-area').html(forminput);
    $('#data-area-container').show(1000);
    $.ajax({url:base_url+'/data-umum/data-table-provinsi', data:forminput, success:function(res){
        $('#data-area').html(res);
    }});
    return false;
});

$('#tahun-anggaran').change(function(){
  var tahun = $(this).val();
  getDataProvinsi(tahun);
  $('#kabupaten-input, #kecamatan-input, #desa-input').html('<option value="" selected>Semua</option>');
  goLoad({elm:'#kabupaten-input', url:'/data-umum/data-option?tahun='+tahun});
});

$('#kabupaten-input').change(function(){
  var kode = $(this).val();
  var tahun = $('#tahun-anggaran').val();
  $('#kecamatan-input, #desa-input').html('<option value="">Semua</option>');
  if(kode !== ''){
    goLoad({elm:'#kecamatan-input', url:'/data-umum/data-option?tahun='+tahun+'&type=kecamatan&kode='+kode});
  }
});

$('#kecamatan-input').change(function(){
  var kode = $(this).val();
  var tahun = $('#tahun-anggaran').val();
  $('#desa-input').html('<option value="">Semua</option>');
  if(kode !== ''){
    goLoad({elm:'#desa-input', url:'/data-umum/data-option?tahun='+tahun+'&type=desa&kode='+kode});
  }
});

getDataProvinsi($('#tahun-anggaran').val());
getDataAnalitik();

$('#modal-view-desa').click(function(e){
   $('#data-area-container').show(1000);
   var tahun = $('#tahun-anggaran').val();
    $.ajax({url:base_url+'/data-umum/data-user-import', data:{tahun: tahun}, success:function(res){
        $('#data-area').html(res);
    }});
    return false;
});
JS;

$this->registerCssFile('@web/css/site-extend.css');
$this->registerJs($script);
// $this->registerJsFile('css/DataTables/datatables.min.js');