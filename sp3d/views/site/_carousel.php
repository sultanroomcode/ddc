<?php
use yii\helpers\Url;
?>
<section>
        <!-- *** HOMEPAGE CAROUSEL ***
_________________________________________________________ -->

        <div class="home-carousel">

            <div class="dark-mask"></div>

            <div class="container">
                <div class="homepage owl-carousel">
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-5 right">
                                <h1>SP3D</h1>
                                <p>Mewujudkan pengelolaan keuangan desa yang transparan, akuntabel dan partisipatif</p>
                            </div>
                            <div class="col-sm-7">
                                <img class="img-responsive" src="<?= Url::to(['/'], false) ?>img/template-homepage.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">

                            <div class="col-sm-7 text-center">
                                <img class="img-responsive" src="<?= Url::to(['/'], false) ?>img/template-mac.png" alt="">
                            </div>

                            <div class="col-sm-5">
                                <h2>Transparan</h2>
                                <ul class="list-style-none">
                                    <li>Keterbukaan bagi masyarakat untuk mengetahui dan mendapatkan akses informasi keuangan desa</li>
                                    
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-5 right">
                                <h1>Akuntabel</h1>
                                <ul class="list-style-none">
                                    <li>Perwujudan pertanggungjawaban pengelolaan dan pengendalian sumber daya dalam rangka pencapaian tujuan</li>
                                </ul>
                            </div>
                            <div class="col-sm-7">
                                <img class="img-responsive" src="<?= Url::to(['/'], false) ?>img/template-easy-customize.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-7">
                                <img class="img-responsive" src="<?= Url::to(['/'], false) ?>img/template-easy-code.png" alt="">
                            </div>
                            <div class="col-sm-5">
                                <h1>Partisipatif</h1>
                                <ul class="list-style-none">
                                    <li>Penyelenggaraan pemerintah desa yang mengikutsertakan kelembagaan desa dan unsur masyarakat desa</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.project owl-slider -->
            </div>
        </div>

        <!-- *** HOMEPAGE CAROUSEL END *** -->
    </section>