<?php
use hscstudio\mimin\components\Mimin;
use frontend\models\transaksi\TaRAB;
use frontend\models\KecamatanCount;
use yii\helpers\Url;
$formatt = new KecamatanCount();
$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT SUM( `Anggaran` ) AS Ang, `rk`.`uraian` , SUBSTR( `Kd_Rincian` , 1, 3 ) AS `bidang_kegiatan`
FROM `Ta_RAB` `r`
LEFT JOIN `ref_rekening_kegiatan` `rk` ON `rk`.`kode` = SUBSTR( `Kd_Rincian` , 1, 3 )
WHERE `r`.`kd_kecamatan` = '".$kd_kecamatan."'
AND `r`.`kegiatan` = 'belanja'
GROUP BY `bidang_kegiatan`");

$resultan = $model->queryAll();

$destdana = $valdana = [];
$nilaidana =0;
$tabledana = '<table class="table">';
foreach($resultan as $v){
    $destdana[] = "'".$v['bidang_kegiatan']."'";
    $valdana[] = $v['Ang'];
    $nilaidana += $v['Ang'];
    
    $tabledana .= '<tr><td>'.$v['bidang_kegiatan'].'</td><td>'.$v['uraian'].'</td><td>'.$formatt->nf($v['Ang']).'</td></tr>';
    
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12"><div id="chart-belanja" style="width: 100%;height:300px;">aa</div></div>
            <div class="col-md-12"><h2>Table Penyerapan Belanja Pada Bidang</h2><?= $tabledana ?></div>
        </div>
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart-belanja'));
    option = {
        color: ['#3398DB'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : [<?=implode(',', $destdana)?>],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'a',
                type:'bar',
                barWidth: '60%',
                data:[<?=implode(',', $valdana)?>]
            }
        ]
    };


    
    myChart.setOption(option);
</script>