<?php
use hscstudio\mimin\components\Mimin;
use frontend\models\transaksi\TaRAB;
use frontend\models\KecamatanCount;
use yii\helpers\Url;
$formatt = new KecamatanCount();
$conn = Yii::$app->db;
$model = $conn->createCommand("SELECT SUM(`Anggaran`) AS Ang, `sumberdana` FROM `Ta_RAB` WHERE `kegiatan` = 'pendapatan' GROUP BY `sumberdana`");//kurang tahun

$resultan = $model->queryAll();

$sourcedana = [];
$nilaidana =0;
$tabledana = '<table class="table">';
foreach($resultan as $v){
	$sourcedana[] = "{value: ".$v['Ang'].", name:'".$v['sumberdana']."'}";
	$nilaidana += $v['Ang'];
	$tabledana .= '<tr><td>'.$v['sumberdana'].'</td><td>'.$formatt->nf($v['Ang']).'</td></tr>';
	
}
$tabledana .= '</table>';

?>
<!-- Box Comment -->
<div class="box box-widget animated slideInUp">
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div id="chart-pendapatan" style="width: 900px;height:300px;"></div>
			</div>
			<div class="col-md-12">
				<h3>Pendapatan Kecamatan Tahun <?= $tahun ?></h3>
				<?= $tabledana ?>
			</div>
		</div>
	</div>
</div>
<!-- /.box -->
<script type="text/javascript">
	var myChart = echarts.init(document.getElementById('chart-pendapatan'));

	option = {
	    tooltip: {
	        trigger: 'item',
	        formatter: "{a} <br/>{b}: {c} ({d}%)"
	    },
	    legend: {
	        orient: 'vertical',
	        x: 'left',
	        data:[<?='"' . implode('","', $sourcedana) . '"';?>]
	    },
	    series: [
	        {
	            name:'Pendapatan',
	            type:'pie',
	            selectedMode: 'single',
	            radius: [0, '30%'],

	            label: {
	                normal: {
	                    position: 'inner'
	                }
	            },
	            labelLine: {
	                normal: {
	                    show: false
	                }
	            },
	            data:[
	                {value:<?=($nilaidana * 0.7)?>, name:'70 %', selected:true},
	                {value:<?=($nilaidana * 0.3)?>, name:'30 %'}
	            ]
	        },
	        {
	            name:'Pendapatan',
	            type:'pie',
	            radius: ['40%', '55%'],
	            label: {
	                normal: {
	                    formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
	                    backgroundColor: '#eee',
	                    borderColor: '#aaa',
	                    borderWidth: 1,
	                    borderRadius: 4,
	                    // shadowBlur:3,
	                    // shadowOffsetX: 2,
	                    // shadowOffsetY: 2,
	                    // shadowColor: '#999',
	                    // padding: [0, 7],
	                    rich: {
	                        a: {
	                            color: '#999',
	                            lineHeight: 15,
	                            align: 'center'
	                        },
	                        // abg: {
	                        //     backgroundColor: '#333',
	                        //     width: '100%',
	                        //     align: 'right',
	                        //     height: 22,
	                        //     borderRadius: [4, 4, 0, 0]
	                        // },
	                        hr: {
	                            borderColor: '#aaa',
	                            width: '100%',
	                            borderWidth: 0.5,
	                            height: 0
	                        },
	                        b: {
	                            fontSize: 16,
	                            lineHeight: 33
	                        },
	                        per: {
	                            color: '#eee',
	                            backgroundColor: '#334455',
	                            padding: [2, 4],
	                            borderRadius: 2
	                        }
	                    }
	                }
	            },
	            data:[<?=implode(',', $sourcedana)?>]
	        }
	    ]
	};
	myChart.setOption(option);
</script>