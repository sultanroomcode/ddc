<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use sp3d\assets\FrontAppAsset;
use sp3d\assets\OnlineFrontAppAsset;
use common\widgets\Alert;

if(Yii::$app->params['offline']){
	FrontAppAsset::register($this);
} else {
	OnlineFrontAppAsset::register($this);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?php 
    if(!Yii::$app->params['offline']){
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137483781-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-137483781-1');
    </script>
    <?php } ?>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="demo" data-url="<?=Url::base(true)?>">
<div class="wrapper">
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</div>
</body>
</html>
<?php $this->endPage() ?>
