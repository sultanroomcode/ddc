<?php 
use frontend\models\KecamatanCount;
$kode = Yii::$app->user->identity->chapter->Kd_Kec;
$data = KecamatanCount::findOne(['kd_kecamatan' => $kode]);
?>
<h1>Dashboard</h1>

Dana Anggaran : Rp. <?= $data->nf($data->dana_anggaran) ?><br>
Dana Pencairan : Rp. <?= $data->nf($data->dana_pencairan) ?><br>
Dana Kegiatan : Rp. <?= $data->nf($data->dana_kegiatan) ?><br>