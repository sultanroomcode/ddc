<div class="row">
    <div class="col-md-5">
        <canvas id="myChart2" style=""></canvas>
    </div>
    <div id="table-data-desa" class="col-md-7">
        <table class="table">
            <tr>
                <td>Total Desa</td>
                <td>Desa Sudah Kirim</td>
                <td>Desa Belum Kirim</td>
            </tr>

            <tr>
                <td>400</td>
                <td>300</td>
                <td>100</td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
function get_chart(datachart){
    var colors = new Array();
    for (var i = 0; i < datachart.label.length; i++) {
        colors.push(randomColor({
           luminosity: 'random',
           hue: 'random'
        }));
    }

    var ctx = $("#myChart2");
    var myChart2 = new Chart(ctx, {
        type: 'pie',
        animation:{
            animateScale:true
        },
        data: {
            labels: datachart.label,
            datasets: [{
                label: '# of Votes',
                data: datachart.value,
                backgroundColor: colors,
                hoverBackgroundColor: colors,
                borderWidth: 1
            }]
        }
    });
}

get_chart({label:['Desa Belum Kirim', 'Desa Sudah Kirim'], value:[40, 80]});
</script>