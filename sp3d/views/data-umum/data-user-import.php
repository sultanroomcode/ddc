<?php
use sp3d\models\transaksi\TaMdbUpload;

$datax = TaMdbUpload::findBySql("SELECT a.description as k_desa, b.description as k_kec, c.description as k_kab FROM ta_mdb_upload m LEFT JOIN user a ON a.id = m.user LEFT JOIN user b ON b.id = LEFT(m.user, 7) LEFT JOIN user c ON c.id = LEFT(m.user, 4) WHERE m.f_ftp_send = 'Y' AND m.f_ftp_back = 'Y' AND m.tahun = :tahun", ['tahun' => $data['tahun']]);
?>
<h3 align="center">Desa Yang Sudah Mengirim APBDesa <?= $data['tahun'] ?></h3>
<table class="table compact" id="data-table-provinsi">
    <thead>
        <tr>            
            <th>Desa</th>
            <th>Kecamatan</th>
            <th>Kabupaten</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Desa</th>
            <th>Kecamatan</th>
            <th>Kabupaten</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($datax->all() as $v): ?>
        <tr>                
            <td><?= $v['k_desa'] ?></td>
            <td><?= $v['k_kec'] ?></td>
            <td><?= $v['k_kab'] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
