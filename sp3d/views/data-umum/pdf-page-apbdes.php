<?php
//ini sudah akan di edit
use frontend\models\transaksi\TaDesa;
use frontend\models\User;

$formater = new TaDesa();
$conn = Yii::$app->db;
//pendapatan
$model = $conn->createCommand("SELECT `t`.`Kd_Rincian` , `rrp`.`Nama_Obyek` AS `uraian`,  `t`.`Anggaran` FROM (
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB` WHERE `Kd_Rincian` LIKE '4.%' AND `user_id` = '$kd_desa' AND `Tahun` = '$tahun'
UNION
SELECT `Kd_Rincian` COLLATE utf8_unicode_ci AS `Kd_Rincian`,`Anggaran` FROM `Ta_RAB_Ext` WHERE `kegiatan` = 'pendapatan' AND `user_id` = '$kd_desa' AND `Tahun` = '$tahun'
) AS `t` LEFT JOIN `Ref_Rek4` `rrp` ON `rrp`.`Obyek` = `t`.`Kd_Rincian` COLLATE utf8_unicode_ci ORDER BY `Kd_Rincian` ASC");
$result = $model->queryAll();

$model = $conn->createCommand("SELECT `t`.`Kd_Rincian`, `rrp`.`Nama_Obyek` AS `uraian`,  `t`.`Anggaran` FROM (
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB` WHERE `Kd_Rincian` LIKE '5.%' AND `user_id` = '$kd_desa' AND `Tahun` = '$tahun'
UNION
SELECT `Kd_Rincian` COLLATE utf8_unicode_ci AS `Kd_Rincian`,`Anggaran` FROM `Ta_RAB_Ext` WHERE `Kd_Rincian` LIKE '5.%' AND `user_id` = '$kd_desa' AND `Tahun` = '$tahun'
) AS `t` LEFT JOIN `Ref_Rek4` `rrp` ON `rrp`.`Obyek` = `t`.`Kd_Rincian` COLLATE utf8_unicode_ci ORDER BY `Kd_Rincian` ASC");
$resultbel = $model->queryAll();

$model = $conn->createCommand("SELECT `t`.`Kd_Rincian`, `rrp`.`Nama_Obyek` AS `uraian`,  `t`.`Anggaran` FROM (
SELECT `Kd_Rincian`,`Anggaran` FROM `Ta_RAB` WHERE `Kd_Rincian` LIKE '6.%' AND `user_id` = '$kd_desa'
UNION
SELECT `Kd_Rincian` COLLATE utf8_unicode_ci AS `Kd_Rincian`,`Anggaran` FROM `Ta_RAB_Ext` WHERE `Kd_Rincian` LIKE '6.%' AND `user_id` = '$kd_desa'
) AS `t` LEFT JOIN `Ref_Rek4` `rrp` ON `rrp`.`Obyek` = `t`.`Kd_Rincian` COLLATE utf8_unicode_ci ORDER BY `Kd_Rincian` ASC");
$resultbiaya = $model->queryAll();
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    table tr td, th {
        padding: 3px;
    }

    .midcen {
        text-align: center;
        vertical-align: middle;
    }

    div.niveau
    {
        padding-left: 2mm;
        /*border: 0.2mm #000 solid;*/
    }
    .no_top {
        border-top: none !important;
        padding: 5px;
    }
    .no_bott {
        border-bottom: none !important;
    }
    .no_top_bott {
        border-top: none !important;
        border-bottom: none !important;
        padding: 5px;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 9pt;">
    <bookmark title="Pendapatan" level="0" ></bookmark>
    <div class="niveau">
        <?php 
        $kabd = User::findOne(substr($kd_desa, 0, 4));
        $kecd = User::findOne(substr($kd_desa, 0, 7));
        $desd = User::findOne($kd_desa);

        echo "<h2>Rencana Anggaran Belanja<br> ".$desd->description." <br> ".$kecd->description."<br>".$kabd->description."<br> Provinsi Jawa Timur</h2>";
        ?>
        <table border="2" style="border-collapse: collapse;">
            <tr>
                <th align="center" valign="middle">KODE<br>REKENING</th>
                <th align="center" valign="middle">URAIAN</th>
                <th align="center" valign="middle">ANGGARAN</th>
                <th align="center" valign="middle" width="70">KET</th>
            </tr>
            <tr>
                <th>1</th>
                <th>PENDAPATAN</th>
                <th></th>
                <th></th>
            </tr>
            <?php
            $c = 0;
            foreach($result as $v){
                echo "<tr><td>".$v['Kd_Rincian']."</td><td>".$formater->ww($v['uraian'], 60)."</td><td align=\"right\">".$formater->nf($v['Anggaran'])."</td><td></td></tr>";
                    //count ANggaran
                    $c += $v['Anggaran'];
            }
            ?>
            <tr>
                <td colspan="2" align="center">JUMLAH PENDAPATAN</td>
                <td align="right"><b><?= $formater->nf($c) ?></b></td>
                <td></td>
            </tr>
            <tr>
                <th height="10" colspan="4" bgcolor="#001"></th>
            </tr>
            <tr>
                <th>2.</th>
                <th>BELANJA</th>
                <th></th>
                <th></th>
            </tr>
            <?php
            $d = 0;
            foreach($resultbel as $v){
                echo "<tr><td>".$v['Kd_Rincian']."</td><td>".$formater->ww($v['uraian'], 60)."</td><td align=\"right\">".$formater->nf($v['Anggaran'])."</td><td></td></tr>";
                $d += $v['Anggaran'];
            }
            ?>
            <tr>
                <td colspan="2" align="center">JUMLAH BELANJA</td>
                <td align="right"><b><?= $formater->nf($d) ?></b></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2" align="center">SURPLUS / DEFISIT</td>
                <td align="right"><b>( <?= $formater->nf(abs($c-$d)) ?> )</b></td>
                <td></td>
            </tr>
            <tr>
                <th height="10" colspan="4" bgcolor="#001"></th>
            </tr>
            <tr>
                <th>3.</th>
                <th>PEMBIAYAAN</th>
                <th></th>
                <th></th>
            </tr>
            <?php
            $e = $f = 0;
            foreach($resultbiaya as $v){
                echo "<tr><td>".$v['Kd_Rincian']."</td><td>".$formater->ww($v['uraian'], 60)."</td><td align=\"right\">".$formater->nf($v['Anggaran'])."</td><td></td></tr>";
                if(substr($v['Kd_Rincian'], 0, 4) == '3.1.'){
                    $e += $v['Anggaran'];
                } else {
                    $f += $v['Anggaran'];
                }
            }
            ?>
            <!-- <tr>
                <td colspan="2">Pembiayaan Netto</td>
                <td align="right"><?= $formater->nf($e-$f) ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">SiLPA tahun berjalan</td>
                <td align="right"><?= $formater->nf(($c-$d)+($e-$f)) ?></td>
                <td></td>
            </tr> -->
            </table>
    </div>
</page>
