<div class="container">
	<div class="row">
		<div class="col-md-5">
			<canvas id="line-example" style="height:100px;"></canvas>
		</div>
		<div class="col-md-7">
		<table class="table">
			<tr>
				<td>Total Desa</td>
				<td>Desa Yang Mengirim</td>
			</tr>

			<tr>
				<td><?=$all?></td>
				<td><?=$desa?></td>
			</tr>
		</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var ctx = document.getElementById("line-example");
	var myChart = new Chart(ctx, {
	    type: 'pie',
	    data: {
	        labels: ["Desa (*)", "Desa Mengirim"],
	        datasets: [{
	            label: '# of Votes',
	            data: [<?=$all?>, <?=$desa?>],
	            backgroundColor: [
	                'rgba(255, 200, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	            ],
	            borderColor: [
	                'rgba(255,200,132,1)',
	                'rgba(54, 162, 235, 1)',
	            ],
	            borderWidth: 1
	        }]
	    }
	});
</script>