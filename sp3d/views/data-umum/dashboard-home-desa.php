<?php 
use frontend\models\DesaCount;
$kode = Yii::$app->user->identity->chapter->Kd_Desa;
$data = DesaCount::findOne(['kd_desa' => $kode]);
?>
<h1>Dashboard</h1>

Dana Anggaran : Rp. <?= $data->nf($data->dana_anggaran) ?><br>
Dana Pencairan : Rp. <?= $data->nf($data->dana_pencairan) ?><br>
Dana Kegiatan : Rp. <?= $data->nf($data->dana_kegiatan) ?><br>