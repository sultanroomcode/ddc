<?php
use yii\helpers\Url;
?>
<h1>Laporan</h1>

<div class="tile">
	<div class="row">
		<div class="col-md-6">
			<div class="list-group block">
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=data-rpjm-desa&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item active">Laporan RPJM <i class="fa fa-file-pdf-o"></i> <span class="badge">14</span></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=daftar-perangkat-desa&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">Laporan Perangkat <i class="fa fa-file-pdf-o"></i></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=data-bidang&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">Laporan Bidang <i class="fa fa-file-pdf-o"></i></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=daftar-tpk-desa&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">Laporan TPK <i class="fa fa-file-pdf-o"></i></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=bagian-penganggaran&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">Laporan Penganggaran <i class="fa fa-file-pdf-o"></i></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=apbdes&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">Laporan APBDes <i class="fa fa-file-pdf-o"></i></a>
			</div>
		</div>
		<div class="col-md-6">
			<div target="_blank" class="list-group block">
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=laporan-realisasi-apbdes-desa&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item active">PDF Laporan Realisasi Pelaksanaan APBDes  <i class="fa fa-file-pdf-o"></i> <span class="badge">14</span></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=laporan-dana-desa&kode=1&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">Laporan Dana Desa Tahap 1 <i class="fa fa-file-pdf-o"></i></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=laporan-dana-desa&kode=2&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">Laporan Dana Desa Tahap 2 <i class="fa fa-file-pdf-o"></i></a>
			     <a href="<?= Url::to(['/data-umum/pdf?bagian=laporan-realisasi-apbdes-desa-akhir&kd_desa='.$id], true) ?>" target="_blank" class="list-group-item">PDF Laporan Realisasi Pelaksanaan APBDes Akhir <i class="fa fa-file-pdf-o"></i></a>
			     <a href="javascript:void(0)" onclick="goLoad({url:'/umum/laporan-test/laporan?page=apbdes&kd=<?=$id?>'})" target="_blank" class="list-group-item">Laporan Test APBDes <i class="fa fa-file-pdf-o"></i></a>
			     <a href="javascript:void(0)" onclick="goLoad({url:'/umum/laporan-test/laporan?page=perkades&kd=<?=$id?>'})"  target="_blank" class="list-group-item">Laporan Test Perkades <i class="fa fa-file-pdf-o"></i></a>
			</div>
		</div>
	</div>
</div>
