<style type="text/css">
	.pad {
		padding: 5px;
	}
</style>
<div class="container">
	<div class="row pane-chart" id="main-pane">
		<div class="col-md-5">
			<div id="chart-akumulasi-div" style="width: 100%;height:300px;">aa</div>
		</div>
		<div class="col-md-7">
			Anggaran APB Desa <b>Se-Kabupaten Tuban</b> :<br>

			<table border="1" style="border-collapse: collapse; padding: 5px;">
				<tr>
					<th class="pad"></th>
					<th class="pad">Jumlah Dana</th>
					<th class="pad">Jumlah Data</th>
				</tr>

				<tr>
					<td class="pad">Pendapatan <a href="javascript:void(0)" class="btn btn-xs btn-danger" onclick="hidElmExc({showPane:'div#div-chart-1'})"><i class="fa fa-pie-chart"></i></a></td>
					<td class="pad" align="right">Rp. <?= $data->nf($data->dana_pendapatan)?></td>
					<td class="pad"><?= $data->nfo($data->data_pendapatan)?> data</td>
				</tr>

				<tr>
					<td class="pad">Belanja <a href="javascript:void(0)" class="btn btn-xs btn-info" onclick="hidElmExc({showPane:'div#div-chart-2'})"><i class="fa fa-bar-chart"></i></a></td>
					<td class="pad" align="right">Rp. <?= $data->nf($data->dana_rab)?></td>
					<td class="pad"><?= $data->nfo($data->data_rab)?> data</td>
				</tr>

				
				<tr>
					<td class="pad">Penerimaan Pembiayaan</td>
					<td class="pad" align="right">Rp. <?= $data->nf($data->dana_penerimaan)?></td>
					<td class="pad"><?= $data->nfo($data->data_penerimaan)?> data</td>
				</tr>

				<tr>
					<td class="pad">Pengeluaran Pembiayaan</td>
					<td class="pad" align="right">Rp. <?= $data->nf($data->dana_pengeluaran)?></td>
					<td class="pad"><?= $data->nfo($data->data_pengeluaran)?> data</td>
				</tr>

				<tr>
					<td class="pad">Pajak</td>
					<td class="pad" align="right">Rp. <?= $data->nf($data->dana_pajak)?></td>
					<td class="pad"><?= $data->nfo($data->data_pajak)?> data</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row pane-chart" id="div-chart-1">
		<a href="javascript:void(0)" class="btn btn-xs btn-info" onclick="hidElmExc({showPane:'div#main-pane'})"><i class="fa fa-home"></i></a>
		<div class="col-md-12" id="chart-akumulasi-pendapatan">
		</div>
	</div>

	<div class="row pane-chart" id="div-chart-2">
		<a href="javascript:void(0)" class="btn btn-xs btn-info" onclick="hidElmExc({showPane:'div#main-pane'})"><i class="fa fa-home"></i></a>
		<div class="col-md-12" id="chart-akumulasi-belanja">
		</div>
	</div>
</div>

<script type="text/javascript">
	function hidElmExc(o){
		$('.pane-chart').hide();
		$(o.showPane).show(1000);
	}
	$('#div-chart-1, #div-chart-2').hide();
    var myChart = echarts.init(document.getElementById('chart-akumulasi-div'));
    option = {
        color: ['#3398DB'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'        
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : ['Pendapatan', 'Belanja', 'Penerimaan','Pengeluaran'],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Total Dana',
                type:'bar',
                barWidth: '60%',
                data:[<?=$data->dana_pendapatan.','.$data->dana_rab.','.$data->dana_penerimaan.','.$data->dana_pengeluaran?>]
            }
        ]
    };


    
    myChart.setOption(option);
    goLoad({elm:'#chart-akumulasi-pendapatan', url:'data-visual/pendapatan-kabupaten'});
    goLoad({elm:'#chart-akumulasi-belanja', url:'data-visual/belanja-kabupaten'});
</script>