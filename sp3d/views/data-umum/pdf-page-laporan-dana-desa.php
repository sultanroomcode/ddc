<?php
use frontend\models\transaksi\TaDesa;
use frontend\models\transaksi\TaDanaDesa;
$data = TaDesa::findOne(['Kd_Desa' => $kd_desa]);
$datadd = TaDanaDesa::find()->where(['Kd_Desa' => $kd_desa]);
$nama_desa = substr($data->desa->Nama_Desa, 16);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
    }
-->
</style>

<page orientation="paysage" backtop="3mm" backbottom="3mm" backleft="12mm" backright="7mm" style="font-size: 10pt">
    <bookmark title="Sommaire" level="0" ></bookmark>
    <div class="niveau">
        <b style="text-align: center;"><br>
        LAPORAN REALISASI PENYERAPAN DANA DESA<br>
        TAHAP <?=$kode?> TAHUN ANGGARAN <?= $tahun ?><br>
        PEMERINTAHAN DESA <?= $nama_desa ?><br>
        KECAMATAN <?= $data->desa->kecamatan->Nama_Kecamatan ?><br>
        KABUPATEN/KOTA TUBAN</b><br>

        Pagu Desa Rp. ...
        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 8%; padding: 2mm;">NOMOR</th>
                <th style="width: 10%; padding: 2mm;">URAIAN</th>
                <th style="width: 8%; padding: 2mm;">URAIAN<br>OUTPUT</th>
                <th style="width: 8%; padding: 2mm;">VOLUME<br>OUTPUT</th>
                <th style="width: 12%; padding: 2mm;">CARA<br>PENGADAAN</th>
                <th style="width: 10%; padding: 2mm;">ANGGARAN<br>Rp</th>
                <th style="width: 10%; padding: 2mm;">REALISASI<br>Rp</th>
                <th style="width: 8%; padding: 2mm;">SISA<br>Rp</th>
                <th style="width: 8%; padding: 2mm;">%<br>CAPAIAN<br>OUTPUT</th>
                <th style="width: 5%; padding: 2mm;">KET.</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
                <th>10</th>
            </tr>
            <?php foreach($datadd->all() as $v) { ?>
            <tr>
                <td><?= $v->kd_rincian ?></td>
                <td><?= $v->uraian ?></td>
                <td><?= $v->uraian_output ?></td>
                <td><?= $v->volume_output ?></td>
                <td><?= $v->cara_pengadaan ?></td>
                <td><?= $v->anggaran ?></td>
                <td><?= $v->realisasi ?></td>
                <td><?= $v->sisa ?></td>
                <td><?= $v->capaian_output ?></td>
                <td><?= $v->keterangan ?></td>
            </tr>
            <?php } ?>

            <tr>
                <td></td>
                <td>JUMLAH (PENDAPATAN - BELANJA - <br> PEMBIAYAAN)</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2">Rp.</td>
                <td></td>
            </tr>
        </table>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 20%; padding: 2mm;">
                    <br>
                    <br>
                    BENDAHARA DESA <?= $nama_desa ?><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?= $data->Nm_Bendahara ?><br> 
                </td>
                <td style="width: 40%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    Disetujui Oleh,<br>
                    <?= ucwords($nama_desa).', '.date('d-m-Y') ?><br>
                    KEPALA DESA <?= $nama_desa ?><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?= $data->Nm_Kades ?><br>
                </td>
            </tr>
        </table>
    </div>
</page>
