<?php
use frontend\models\transaksi\TaKegiatan;
use frontend\models\DesaCount;
$datac = DesaCount::find()->where(['kd_desa' => $kd_desa])->one();
$data = TaKegiatan::find()->where(['Kd_Desa' => $kd_desa, 'Tahun' => $tahun, 'Kd_Keg'=> $kode])->one();
$nama_desa = substr($datac->desa->Nama_Desa, 16);
$nama_kecamatan = $datac->desa->kecamatan->Nama_Kecamatan;
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    .midcen {
        text-align: center;
        vertical-align: middle;
    }

    div.niveau
    {
        padding-left: 2mm;
        /*border: 0.2mm #000 solid;*/
    }
    .no_top {
        border-top: none !important;
        padding: 5px;
    }
    .no_bott {
        border-bottom: none !important;
    }
    .no_top_bott {
        border-top: none !important;
        border-bottom: none !important;
        padding: 5px;
    }
-->
</style>
<page backtop="4mm" backbottom="4mm" backleft="5mm" backright="5mm" style="font-size: 10pt;">
    <bookmark title="Penerimaan" level="0" ></bookmark>
    <div class="niveau">
        <b style="text-align: center;"><br> RENCANA ANGGARAN BIAYA<br>DESA <?= $nama_desa ?> <?= $nama_kecamatan ?><br>TAHUN ANGGARAN <?=$tahun?></b>
        <br><br>
        
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr><td>1. Bidang</td><td>: <?= substr($data->Kd_Bid,6).' - '.$data->bidang->Nama_Bidang ?></td></tr>
            <tr><td>2. Kegiatan</td><td>: <?= $data->ID_Keg.' - '.$data->Nama_Kegiatan ?></td></tr>
            <tr><td>3. Waktu Pelaksanaan</td><td>: <?= $data->Waktu ?></td></tr>
        </table>

        Rincian Pendanaan<br>

        <table border="1" style="border-collapse: collapse; width: 100%;">
        <tr class="midcen"><td>Kode Rekening</td><td>Uraian</td><td>Volume</td><td>Satuan</td><td>Harga</td><td>Jumlah</td></tr>
        <tr class="midcen"><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6 = 3 x 5</td></tr>
        
        <tr><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td></tr>
        <tr><td class="no_top_bott"><?= $data->rab->Kd_Rincian ?></td>
        <td class="no_top_bott"><b><?= $data->ww($data->Nama_Kegiatan, 40) ?></b></td>
        <td class="no_top_bott"></td><td class="no_top_bott"></td>
        <td class="no_top_bott"></td>
        <td class="no_top_bott" align="right">Rp. <?= number_format(round($data->rab->Anggaran), 0, ',','.') ?></td></tr>
        <?php $total = 0; foreach ($data->rab->rincian2 as $x) {
            echo '<tr><td class="no_top_bott"></td><td class="no_top_bott"> '.$x->No_Urut.' . '.$x->Uraian.'</td><td class="no_top_bott">'.$x->JmlSatuan.'</td><td class="no_top_bott">'.$x->Satuan.'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->HrgSatuan), 0, ',','.').'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->Anggaran), 0, ',','.').'</td></tr>';
        } 
        ?>        
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>

        <tr><td colspan="5" align="right"><b>Total</b></td><td align="right">Rp. <?= number_format(round($total), 0, ',','.') ?></td></tr>
        </table>
        <br>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;">
                    Disetujui/Mengesahkan,<br>
                    KEPALA DESA <?= $nama_desa ?><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?= $datac->desaRinci->Nm_Kades ?><br>
                </td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    Tuban, <?= date('d-F-Y') ?><br>
                    Pelaksana Kegiatan<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?= $data->Nm_PPTKD ?><br>
                </td>
            </tr>
        </table>
    </div>
</page>
