<?php
use frontend\models\transaksi\TaBidang;
use frontend\models\DesaCount;
$datac = DesaCount::find()->select(['dana_pendapatan'])->where(['kd_desa' => $kd_desa])->one();
$data = TaBidang::find()->where(['Kd_Desa' => $kd_desa]);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}
    .no_top {
        border-top: none !important;
    }
    .no_bott {
        border-bottom: none !important;
    }
    .no_top_bott {
        border-top: none !important;
        border-bottom: none !important;
    }

    div.niveau
    {
        padding-left: 2mm;
    }
-->
</style>
<page backtop="4mm" backbottom="4mm" backleft="4mm" backright="4mm" style="font-size: 10pt">
    <bookmark title="Sommaire" level="0" ></bookmark>
    <div class="niveau">
        <h2>Data Bidang</h2>
        
        <table border="0.4" style="border-collapse: collapse;">
        <tr>
            <td style="width: 14%; padding: 2mm;">Kode</td>
            <td style="width: 44%; padding: 2mm;">Uraian</td>
            <td style="width: 14%; padding: 2mm;">Nilai</td></tr>
        <tr><td class="no_bott"></td> <td class="no_bott"></td> <td class="no_bott"></td></tr>
        <?php $total = 0; foreach ($data->all() as $v) {
        	echo "<tr><td class=\"no_top_bott\">".$v->Kd_Bid."</td><td class=\"no_top_bott\"><b>".$v->Nama_Bidang.'</b></td><td class="no_top_bott"></td></tr>';
        	foreach ($v->kegiatan as $x) {
        		echo '<tr><td class="no_top_bott"> - '.$x->Kd_Keg.'</td><td class="no_top_bott"> - '.$x->Nama_Kegiatan.'</td><td class="no_top_bott" align="right">'.number_format(round($x->Pagu), 0, ',','.').'</td></tr>';
                $total += (int) $x->Pagu;
        	}
        } 
        ?>
        <tr><td class="no_top_bott"></td><td class="no_top_bott" align="right">Total</td><td class="no_top_bott" align="right"><?= number_format(round($total), 0, ',','.') ?></td></tr>
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>
        </table>
    </div>
</page>
