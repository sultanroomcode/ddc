<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
        border: 0.2mm #000 solid;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 5pt">    
    <div class="niveau" style="font-size: 7pt; height: 195mm">    
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm;">
                    LAMPIRAN III<br>
                    PERATURAN DESA {...}<br>
                    NOMOR {...} TAHUN {...}<br>
                    TENTANG<br>
                    ANGGARAN PENDAPATAN DAN BELANJA DESA<br>
                    TAHUN ANGGARAN {...}<br>
                </td>
            </tr>
        </table>

        <b style="text-align: center;"><br>
        DAFTAR PENYERTAAN MODAL DESA {...}<br>
        TAHUN ANGGARAN {...}</b><br>

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 5%; padding: 2mm;">NO</th>
                <th style="width: 11%; padding: 2mm;">Tujuan<br> Pembentukan<br> Dana Cadangan</th>
                <th style="width: 11%; padding: 2mm;">Dasar Hukum<br> Pembentukan<br> Dana Cadangan</th>
                <th style="width: 11%; padding: 2mm;">Jumlah Dana<br> Cadangan yang<br> Direncanakan (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Saldo Awal<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Transfer Dari<br> Kas Umum<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Transfer Ke<br> Kas Umum<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Saldo<br> Akhir (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Sisa Dana yang<br> Belum<br> Dicadangkan<br> (Rp)</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
            </tr>
            <?php for ($i=0; $i < 5; $i++) { ?>
            <tr>
                <td><?= $i ?></td>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>D</td>
                <td>E</td>
                <td>F</td>
                <td>G</td>
                <td>H</td>
            </tr>
            <?php } ?>
        </table>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    KEPALA DESA {...}<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    ................................................<br>
                    <b>(Nama Tanpa Gelar, Tanda Tangan dan Cap)</b><br>
                </td>
            </tr>
        </table>
    </div>
</page>
