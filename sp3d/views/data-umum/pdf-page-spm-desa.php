<?php
use frontend\models\transaksi\TaKegiatan;
use frontend\models\DesaCount;
$datac = DesaCount::find()->where(['kd_desa' => $kd_desa])->one();
$data = TaKegiatan::find()->where(['kd_desa' => $kd_desa, 'Kd_Keg' => $kode]);

$nama_desa = substr($datac->desa->Nama_Desa, 16);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
        border: 0.2mm #000 solid;
    }
-->
</style>

<page format="A4" orientation="P">
    <bookmark title="Lampiran E -  SPM Kolektif" level="0" ></bookmark>
    <div class="niveau" style="font-size: 7pt; height: 255mm"> 
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td><img src="./wh-logo.png" style="width: 10mm"></td>
                <td><b style="text-align: center;"><br>
        PEMERINTAH DESA <?= $nama_desa ?><br>
        <?= $datac->desa->kecamatan->Nama_Kecamatan ?> KABUPATEN TUBAN<br>
        Jalan <?= $datac->desaRinci->Alamat ?> Telepon {...}</b><br></td>
            </tr>
        </table>


        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr><td></td> <td style="width: 70%;"></td> <td>Tuban, <?= date('d-F-Y') ?></td></tr>
            <tr><td></td> <td></td> <td>a</td></tr>
            <tr><td>Nomor</td> <td>: 920/&nbsp;&nbsp;&nbsp;/414.<?=$kd_desa?>/<?= $tahun ?></td> <td>Kepada :</td></tr>
            <tr><td>Sifat</td> <td>: Biasa</td><td>Yth. Bendahara Umum Desa</td></tr>
            <tr><td>Lampiran</td> <td>: 1 (satu) berkas</td><td>di -</td></tr>
            <tr><td>Perihal</td> <td>: SPM Perintah Membayar</td><td>.........</td></tr>
        </table>

        Bersama ini disampaikan dengan hormat Surat Perintah Membayar (SPM) dengan rincian sebagai berikut :

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 10%; padding: 2mm;">NO.</th>
                <th style="width: 10%; padding: 2mm;">KODE REKENING</th>
                <th style="width: 40%; padding: 2mm;">URAIAN</th>
                <th style="width: 17%; padding: 2mm;">BESARNYA</th>
                <th style="width: 17%; padding: 2mm;">KET.</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
            </tr>
            <?php $i = 1; foreach ($data->all() as $v) { ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $v->ID_Keg ?></td>
                <td><?= $v->Nama_Kegiatan ?></td>
                <td align="right"><?= $v->nf($v->Pagu) ?></td>
                <td></td>
            </tr>

                <?php foreach($v->rab->rincian2 as $x): ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?= $x->No_Urut ?> - <?= $x->Uraian ?></td>
                        <td align="right"><?= $x->nf($x->Anggaran) ?></td>
                        <td></td>
                    </tr>
                <?php endforeach; ?>

            <tr>
                <td colspan="3" align="center">JUMLAH</td>
                <td>Rp.</td>
                <td></td>
            </tr>
            <?php $i++; } ?>
        </table>

        Bendahara segera menyerahkan kepada pelaksana kegiatan sesuai dengan SPP yang diajukan<br>
        Demikian untuk menjadi perhatian dan dilaksanakan dengan penuh tanggungjawab.

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;">
                    
                </td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    PEMEGANG KEKUASAAN PENGELOLAAN KEUANGAN<br>
                    DESA <?= $nama_desa ?>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?= $datac->desaRinci->Nm_Kades ?><br>
                </td>
            </tr>
        </table>
    </div>
</page>
