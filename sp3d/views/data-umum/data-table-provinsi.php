<h2 align="center">APBDesa Provinsi Jawa Timur Tahun <?= $data['tahun-anggaran'] ?></h2>
<table class="table compact" id="data-table-provinsi">
    <thead>
        <tr>            
            <th>Kabupaten</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Kabupaten</th>
            <th>Pendapatan</th>
            <th>Belanja</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach($model->all() as $v): ?>
        <tr>                
            <td><?= $v->label->description ?></td>
            <td align="right"><?= $v->nf($v->dana_pendapatan) ?></td>
            <td align="right"><?= $v->nf($v->dana_rab) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script =<<<JS
$('#data-table-provinsi').DataTable({
  "language": {
      "decimal": ",",
      "thousands": "."
  }
});
JS;

$this->registerJs($script);