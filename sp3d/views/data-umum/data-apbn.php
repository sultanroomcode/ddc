<?php
if($model != null){
?>
<style type="text/css">
	.pad {
		padding: 5px;
	}
</style>
<div class="row">
  <div class="col-md-5">
    <table border="1" style="border-collapse: collapse; padding: 5px;">
      <tr>
        <th class="pad"></th>
        <th class="pad">Jumlah Dana</th>
        <th class="pad">Jumlah Data</th>
      </tr>

      <tr>
        <td class="pad">Pendapatan</td>
        <td class="pad" align="right">Rp. <?=$model->nf($model->dana_pendapatan)?></td>
        <td class="pad"><?= $model->nfo($model->data_pendapatan) ?> data</td>
      </tr>

      <tr>
        <td class="pad">Belanja</td>
        <td class="pad" align="right">Rp. <?=$model->nf($model->dana_rab)?></td>
        <td class="pad"><?= $model->nfo($model->data_rab) ?> data</td>
      </tr>

      
      <tr>
        <td class="pad">Penerimaan Pembiayaan</td>
        <td class="pad" align="right">Rp. <?=$model->nf($model->dana_penerimaan)?></td>
        <td class="pad"><?= $model->nfo($model->data_penerimaan) ?> data</td>
      </tr>

      <tr>
        <td class="pad">Pengeluaran Pembiayaan</td>
        <td class="pad" align="right">Rp. <?=$model->nf($model->dana_pengeluaran)?></td>
        <td class="pad"><?= $model->nfo($model->data_pengeluaran) ?> data</td>
      </tr>

      <tr>
        <td class="pad">Pajak</td>
        <td class="pad" align="right">Rp. <?=$model->nf($model->dana_pajak)?></td>
        <td class="pad"><?= $model->nfo($model->data_pajak) ?> data</td>
      </tr>
    </table>
  </div>
  <div class="col-md-7">
    <div class="tabs">
      <ul class="nav nav-tabs">
          <li class="active"><a href="#pendapatan-tab-chart" data-toggle="tab"><i class="fa fa-pie-chart"></i> Chart Pendapatan</a>
          </li>
          <li class=""><a href="#belanja-tab-chart" data-toggle="tab"><i class="fa fa-bar-chart"></i> Chart Belanja</a>
          </li>
      </ul>
      <div class="tab-content">
          <div class="tab-pane active" id="pendapatan-tab-chart">
              This is tab one.
          </div>
          <div class="tab-pane" id="belanja-tab-chart">
              This is tab two.
          </div>
      </div>
      <!-- /.tab-content -->
    </div>
  </div>
</div>

   <?php
   $desa = ($kd_desa != null)?'desa':'kecamatan';
   ?>
<script type="text/javascript">
  var isDesa = '<?=$desa?>';
  if(isDesa == 'desa'){
    goLoad({elm:'#pendapatan-tab-chart', url:'data-visual/pendapatan-desa?kd_desa=<?=$kd_desa?>&tahun=<?=$tahun?>'});
    goLoad({elm:'#belanja-tab-chart', url:'data-visual/belanja-desa?kd_desa=<?=$kd_desa?>&tahun=<?=$tahun?>'});
  } else {
    goLoad({elm:'#pendapatan-tab-chart', url:'data-visual/pendapatan-kecamatan?kd_kecamatan=<?=$kd_kecamatan?>&tahun=<?=$tahun?>'});
    goLoad({elm:'#belanja-tab-chart', url:'data-visual/belanja-kecamatan?kd_kecamatan=<?=$kd_kecamatan?>&tahun=<?=$tahun?>'});
  }
  </script>
<?php
} else {
	echo 'Data tidak tersedia';
}