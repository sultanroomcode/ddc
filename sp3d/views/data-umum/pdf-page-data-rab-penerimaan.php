<?php
use frontend\models\transaksi\TaRAB;
use frontend\models\DesaCount;
$datac = DesaCount::find()->select(['dana_pendapatan'])->where(['kd_desa' => $kd_desa])->one();
$data = TaRAB::find()->where(['Kd_Desa' => $kd_desa, 'kegiatan' => 'penerimaan']);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    .midcen {
        text-align: center;
        vertical-align: middle;
    }

    div.niveau
    {
        padding-left: 2mm;
        /*border: 0.2mm #000 solid;*/
    }
    .no_top {
        border-top: none !important;
        padding: 5px;
    }
    .no_bott {
        border-bottom: none !important;
    }
    .no_top_bott {
        border-top: none !important;
        border-bottom: none !important;
        padding: 5px;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 10pt;">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 100%; text-align: left">
                    Data Rab - Penerimaan
                </td>
            </tr>
        </table>
    </page_header>

    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 100%; text-align: right">
                    Halaman [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Penerimaan" level="0" ></bookmark>
    <div class="niveau">
        <h2>Data Penerimaan Desa</h2>
        <table border="1" style="border-collapse: collapse;width: 95%">
        <tr class="midcen"><td>Kode Rekening</td><td style="width: 20px;">Uraian</td><td>Volume</td><td>Satuan</td><td>Harga</td><td>Jumlah</td></tr>
        <tr class="midcen"><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6 = 3 x 5</td></tr>
        
        <tr><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td></tr>
        
        <?php $total = 0; foreach ($data->all() as $v) {
            //echo $v->Kd_Bid.' - '.$v->Nama_Bidang.'<br>';
            echo "<tr><td class=\"no_top_bott\">".$v->rincianPembiayaan->kode."</td><td class=\"no_top_bott\"><b>".wordwrap($v->rincianPembiayaan->uraian,45,"<br>",TRUE).'</b></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott" align="right">Rp. '.number_format(round($v->Anggaran), 0, ',','.').'</td></tr>';
            foreach ($v->rincian2 as $x) {
                echo '<tr><td class="no_top_bott"></td><td class="no_top_bott"> '.$x->No_Urut.' . '.$x->Uraian.'</td><td class="no_top_bott">'.$x->JmlSatuan.'</td><td class="no_top_bott">'.$x->Satuan.'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->HrgSatuan), 0, ',','.').'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->Anggaran), 0, ',','.').'</td></tr>';
                $total += (int) $x->Anggaran;
            }
        } 
        ?>        
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>

        <tr><td colspan="5" align="right"><b>Total</b></td><td align="right">Rp. <?= number_format(round($total), 0, ',','.') ?></td></tr>
        </table>
        <br>
    </div>
</page>
