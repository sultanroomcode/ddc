<?php
use frontend\models\transaksi\TaPerangkat;
$data = TaPerangkat::find()->where(['Kd_Desa' => $kd_desa]);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}
    .no_top {
        border-top: none !important;
    }
    .no_bott {
        border-bottom: none !important;
    }
    .no_top_bott {
        border-top: none !important;
        border-bottom: none !important;
    }

    div.niveau
    {
        padding-left: 2mm;
        border: 0.2mm #000 solid;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 10pt">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 100%; text-align: left">
                    Data Perangkat Desa
                </td>
            </tr>
        </table>
    </page_header>

    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 100%; text-align: right">
                    Halaman [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Sommaire" level="0" ></bookmark>
    <div class="niveau">
        <h2>Data Perangkat Desa</h2>

        <table border="1" style="border-collapse: collapse;">
        <tr class="midcen"><td>No Urut</td><td>Nama</td><td>Alamat</td><td>No HP</td></tr>
        <tr class="midcen"><td>1</td><td>2</td><td>3</td><td>4</td></tr>
        
        <tr><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td></tr>
        
        <?php $total = 0; foreach ($data->all() as $v) {
            //echo $v->Kd_Bid.' - '.$v->Nama_Bidang.'<br>';
            echo "<tr><td class=\"no_top_bott\">".$v->No_ID."</td><td class=\"no_top_bott\"><b>".wordwrap($v->Nama_Perangkat,45,"<br>",TRUE).'</b></td><td class="no_top_bott">'.$v->Alamat_Perangkat.'</td><td class="no_top_bott">'.$v->Nomor_HP.'</td></tr>';
        } 
        ?>        
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>

        </table>
        <br>
    </div>
</page>
