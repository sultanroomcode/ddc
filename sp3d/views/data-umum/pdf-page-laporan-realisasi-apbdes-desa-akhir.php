<?php
use frontend\models\transaksi\TaDesa;
$data = TaDesa::findOne(['Kd_Desa' => $kd_desa]);
$joindata = $data->laporanAPBDes;
$nama_desa = substr($data->desa->Nama_Desa, 16);
$nama_kecamatan = $data->desa->kecamatan->Nama_Kecamatan;
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
    }
-->
</style>
<page orientation="paysage" backtop="3mm" backbottom="3mm" backleft="12mm" backright="7mm" style="font-size: 10pt">
    <bookmark title="Sommaire" level="0" ></bookmark>
    <div class="niveau">
        <b style="text-align: center;"><br>
        LAPORAN REALISASI PELAKSANAAN<br>
        ANGGARAN PENDAPATAN DAN BELANJA DESA (APBDes)<br>
        SEMESTER KEDUA<br>
        DESA <?= $nama_desa ?> <?= $nama_kecamatan ?><br>
        TAHUN ANGGARAN <?= $tahun ?><br><br></b>

        <!-- LEBIH/KURANG (Rp) (Realisasi - Anggaran) -->

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 10%; padding: 2mm;">KODE REKENING</th>
                <th style="width: 32%; padding: 2mm;">URAIAN</th>
                <th style="width: 13%; padding: 2mm;">JUMLAH ANGGARAN (Rp)</th>
                <th style="width: 13%; padding: 2mm;">REALISASI (Rp)</th>
                <th style="width: 13%; padding: 2mm;">LEBIH/KURANG (Rp)</th>
                <th style="width: 10%; padding: 2mm;">KET</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5 = 4-3</th>
                <th>6</th>
            </tr>
            
            <tr>
                <td>1.</td>
                <td>PENDAPATAN</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <?php $total = 0; foreach ($joindata['pendapatan']->all() as $v) { ?>
            <tr>
                <td><?= $v->Kd_Rincian ?></td>
                <td><?= $data->ww($v->rincianPendapatan->uraian, 45) ?></td>
                <td align="right"><?= $data->nf($v->Anggaran) ?></td>
                <td align="right"><?= $data->nf($v->AnggaranStlhPAK) ?></td>
                <td align="right"><?= $data->nf(abs($v->AnggaranStlhPAK - $v->Anggaran)) ?></td>
                <td></td>
            </tr>

                <?php /* rincian */ foreach ($v->rincian2 as $x) { ?>
                    <tr>
                        <td><?= $x->Kd_Rincian.'.'.$x->No_Urut ?></td>
                        <td><?= $data->ww($x->Uraian, 45) ?></td>
                        <td align="right"><?= $data->nf($x->Anggaran) ?></td>
                        <td align="right"><?= $data->nf($x->AnggaranStlhPAK) ?></td>
                        <td align="right"><?= $data->nf(abs($x->AnggaranStlhPAK - $x->Anggaran)) ?></td>
                        <td></td>
                    </tr>
                <?php } ?>

            <?php $total += $v->Anggaran; } ?>

            <tr>
                <td colspan="2">JUMLAH PENDAPATAN</td>
                <td align="right"><?=$data->nf($total) ?></td>
                <td align="right"></td>
                <td align="right"></td>
                <td></td>
            </tr>

            <tr>
                <td>2.</td>
                <td>BELANJA</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <?php $total_b = 0; foreach ($joindata['belanja']->all() as $v) { ?>
            <tr>
                <td><?= $v->Kd_Rincian ?></td>
                <td><?= $data->ww($v->rincianKegiatan->uraian, 45) ?></td>
                <td align="right"><?= $data->nf($v->Anggaran) ?></td>
                <td align="right"><?= $data->nf($v->AnggaranStlhPAK) ?></td>
                <td align="right"><?= $data->nf(abs($v->AnggaranStlhPAK - $v->Anggaran)) ?></td>
                <td></td>
            </tr>


                <?php /* rincian */ foreach ($v->rincian2 as $x) { ?>
                    <tr>
                        <td><?= $x->Kd_Rincian.$x->No_Urut ?></td>
                        <td><?= $data->ww($x->Uraian, 45) ?></td>
                        <td align="right"><?= $data->nf($x->Anggaran) ?></td>
                        <td align="right"><?= $data->nf($x->AnggaranStlhPAK) ?></td>
                        <td align="right"><?= $data->nf(abs($x->AnggaranStlhPAK - $x->Anggaran)) ?></td>
                        <td></td>
                    </tr>
                <?php } ?>

            <?php $total_b += $v->Anggaran; } ?>

            <tr>
                <td colspan="2">JUMLAH BELANJA</td>
                <td align="right"><?=$data->nf($total_b) ?></td>
                <td align="right"></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="2">SURPLUS / DEFISIT</td>
                <td align="right"><?= ($total_b > $total)?($data->nf($total_b-$total)):($data->nf($total-$total_b)) ?></td>
                <td align="right"></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td>3.</td>
                <td>PEMBIAYAAN</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>3.1.</td>
                <td>PENERIMAAN PEMBIAYAAN</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php $total_c = 0; foreach ($joindata['penerimaan']->all() as $v) { ?>
            <tr>
                <td><?= $v->Kd_Rincian ?></td>
                <td><?= $data->ww($v->rincianPembiayaan->uraian, 45) ?></td>
                <td align="right"><?= $data->nf($v->Anggaran) ?></td>
                <td align="right"><?= $data->nf($v->AnggaranStlhPAK) ?></td>
                <td align="right"><?= $data->nf(abs($v->AnggaranStlhPAK - $v->Anggaran)) ?></td>
                <td></td>
            </tr>

                <?php /* rincian */ foreach ($v->rincian2 as $x) { ?>
                    <tr>
                        <td><?= $x->Kd_Rincian.$x->No_Urut ?></td>
                        <td><?= $data->ww($x->Uraian, 45) ?></td>
                        <td align="right"><?= $data->nf($x->Anggaran) ?></td>
                        <td align="right"><?= $data->nf($x->AnggaranStlhPAK) ?></td>
                        <td align="right"><?= $data->nf(abs($x->AnggaranStlhPAK - $x->Anggaran)) ?></td>
                        <td></td>
                    </tr>
                <?php } ?>

            <?php $total_c += $v->Anggaran; } ?>
            <tr>
                <td>3.2.</td>
                <td>PENGELUARAN PEMBIAYAAN</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php $total_d = 0; foreach ($joindata['pengeluaran']->all() as $v) { ?>
            <tr>
                <td><?= $v->Kd_Rincian ?></td>
                <td><?= $data->ww($v->rincianPembiayaan->uraian, 45) ?></td>
                <td align="right"><?= $data->nf($v->Anggaran) ?></td>
                <td align="right"><?= $data->nf($v->AnggaranStlhPAK) ?></td>
                <td align="right"><?= $data->nf(abs($v->AnggaranStlhPAK - $v->Anggaran)) ?></td>
                <td></td>
            </tr>

                <?php /* rincian */ foreach ($v->rincian2 as $x) { ?>
                    <tr>
                        <td><?= $x->Kd_Rincian.$x->No_Urut ?></td>
                        <td><?= $data->ww($x->Uraian, 45) ?></td>
                        <td align="right"><?= $data->nf($x->Anggaran) ?></td>
                        <td align="right"><?= $data->nf($x->AnggaranStlhPAK) ?></td>
                        <td align="right"><?= $data->nf(abs($x->AnggaranStlhPAK - $x->Anggaran)) ?></td>
                        <td></td>
                    </tr>
                <?php } ?>

            <?php $total_d += $v->Anggaran; } ?>
            <tr>
                <td colspan="2">PEMBIAYAAN NETTO</td>
                <td align="right"><?= $data->nf($total_c - $total_d) ?></td>
                <td align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">SILPA tahun berjalan</td>
                <td align="right"></td>
                <td align="right"></td>
                <td></td>
                <td></td>
            </tr>
        </table>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    KEPALA DESA <?= $nama_desa ?><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?= $data->Nm_Kades ?><br>
                </td>
            </tr>
        </table>
    </div>
</page>
