<?php
use frontend\models\transaksi\TaRAB;
use frontend\models\transaksi\TaBidang;
use frontend\models\transaksi\TaDesa;
use frontend\models\DesaCount;
$datac = DesaCount::find()->select(['dana_rab', 'dana_pendapatan'])->where(['kd_desa' => $kd_desa])->one();
$data_b = TaRAB::find()->where(['Kd_Desa' => $kd_desa, 'kegiatan' => 'belanja']);
$data_pd = TaRAB::find()->where(['Kd_Desa' => $kd_desa, 'kegiatan' => 'pendapatan']);
$data_in = TaRAB::find()->where(['Kd_Desa' => $kd_desa, 'kegiatan' => 'penerimaan']);
$data_out = TaRAB::find()->where(['Kd_Desa' => $kd_desa, 'kegiatan' => 'pengeluaran']);
$data_bid = TaBidang::find()->where(['Kd_Desa' => $kd_desa]);
$data_des = TaDesa::findOne(['Kd_Desa' => $kd_desa]);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    .midcen {
        text-align: center;
        vertical-align: middle;
    }

    div.niveau
    {
        padding-left: 2mm;
        /*border: 0.2mm #000 solid;*/
    }
    .no_top {
        border-top: none !important;
        padding: 5px;
    }
    .no_bott {
        border-bottom: none !important;
    }
    .no_top_bott {
        border-top: none !important;
        border-bottom: none !important;
        padding: 5px;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 10pt;">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 100%; text-align: left">
                    Data Bagian Penganggaran
                </td>
            </tr>
        </table>
    </page_header>

    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 100%; text-align: right">
                    Halaman [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Data Desa" level="0"></bookmark>
    <div class="niveau">
        <h2>Data Desa : <?= $data_des->desa->Nama_Desa ?> - <?= $data_des->desa->kecamatan->Nama_Kecamatan ?> - Tuban</h2>
        Tahun <?= $data_des->Tahun ?><br>
        
        Nama Kades : <?= $data_des->Nm_Kades ?><br>
        Jabatan Kades : <?= $data_des->Jbt_Kades ?><br>
        <br>
        Nama : <?= $data_des->Nm_Sekdes ?><br>
        NIP : <?= $data_des->NIP_Sekdes ?><br>
        Jabatan : <?= $data_des->Jbt_Sekdes ?><br>
        <br>
        Nama : <?= $data_des->Nm_Kaur_Keu ?><br>
        Jabatan : <?= $data_des->Jbt_Kaur_Keu ?><br>
        <br>
        Nama : <?= $data_des->Nm_Bendahara ?><br>
        Jabatan : <?= $data_des->Jbt_Bendahara ?><br>
        <br>
        No. Perdes : <?= $data_des->No_Perdes ?><br>
        Tanggal Perdes : <?= $data_des->Tgl_Perdes ?><br>
        <br>
        No. Perdes PB : <?= $data_des->No_Perdes_PB ?><br>
        Tanggal Perdes PB : <?= $data_des->Tgl_Perdes_PB ?><br>
        <br>
        
        No. Perdes PJ : <?= $data_des->No_Perdes_PJ ?><br>
        Tanggal Perdes PJ : <?= $data_des->Tgl_Perdes_PJ ?><br>
        <br>
        Alamat : <?= $data_des->Alamat ?><br>
        
        Status : <?= $data_des->Status ?><br>
        NPWP : <?= $data_des->NPWP ?><br>
    </div>
</page>

<page pageset="old">
    <bookmark title="Data Bidang" level="0" ></bookmark>
    <div class="niveau" style="height: 255mm">    
        <h2>Data Bidang</h2>
        
        <table border="0.4" style="border-collapse: collapse;">
        <tr>
            <td style="width: 14%; padding: 2mm;">Kode</td>
            <td style="width: 44%; padding: 2mm;">Uraian</td>
            <td style="width: 14%; padding: 2mm;">Nilai</td></tr>
        <tr><td class="no_bott"></td> <td class="no_bott"></td> <td class="no_bott"></td></tr>
        <?php $total = 0; foreach ($data_bid->all() as $v) {
            echo "<tr><td class=\"no_top_bott\">".$v->Kd_Bid."</td><td class=\"no_top_bott\"><b>".$v->Nama_Bidang.'</b></td><td class="no_top_bott"></td></tr>';
            foreach ($v->kegiatan as $x) {
                echo '<tr><td class="no_top_bott"> - '.$x->Kd_Keg.'</td><td class="no_top_bott"> - '.$x->Nama_Kegiatan.'</td><td class="no_top_bott" align="right">'.number_format(round($x->Pagu), 0, ',','.').'</td></tr>';
                $total += (int) $x->Pagu;
            }
        } 
        ?>
        <tr><td class="no_top_bott"></td><td class="no_top_bott" align="right">Total</td><td class="no_top_bott" align="right"><?= number_format(round($total), 0, ',','.') ?></td></tr>
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>
        </table>
    </div>
</page>

<page pageset="old">
    <bookmark title="Pendapatan" level="0" ></bookmark>
    <div class="niveau" style="height: 255mm">    
        <h2>Data Pendapatan Desa</h2>
        <table border="1" style="border-collapse: collapse;">
        <tr class="midcen"><td>Kode Rekening</td><td>Uraian</td><td>Volume</td><td>Satuan</td><td>Harga</td><td>Jumlah</td></tr>
        <tr class="midcen"><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6 = 3 x 5</td></tr>
        
        <tr><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td></tr>
        
        <?php $total = 0; foreach ($data_pd->all() as $v) {
            //echo $v->Kd_Bid.' - '.$v->Nama_Bidang.'<br>';
            echo "<tr><td class=\"no_top_bott\">".$v->rincianPendapatan->kode."</td><td class=\"no_top_bott\"><b>".$v->rincianPendapatan->uraian.'</b></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott" align="right">Rp. '.number_format(round($v->Anggaran), 0, ',','.').'</td></tr>';
            foreach ($v->rincian2 as $x) {
                echo '<tr><td class="no_top_bott"></td><td class="no_top_bott"> '.$x->No_Urut.' . '.$x->Uraian.'</td><td class="no_top_bott">'.$x->JmlSatuan.'</td><td class="no_top_bott">'.$x->Satuan.'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->HrgSatuan), 0, ',','.').'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->Anggaran), 0, ',','.').'</td></tr>';
                $total += (int) $x->Anggaran;
            }
        } 
        ?>        
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>

        <tr><td colspan="5" align="right"><b>Total</b></td><td align="right">Rp. <?= number_format(round($total), 0, ',','.') ?></td></tr>
        </table>
        <br>
    </div>
</page>

<page pageset="old">
    <bookmark title="Belanja" level="0" ></bookmark>
    <div class="niveau" style="height: 255mm">    
        <h2>Data Belanja Desa</h2>
        <table border="1" style="border-collapse: collapse;">
        <tr class="midcen"><td>Kode Rekening</td><td>Uraian</td><td>Volume</td><td>Satuan</td><td>Harga</td><td>Jumlah</td></tr>
        <tr class="midcen"><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6 = 3 x 5</td></tr>
        
        <tr><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td></tr>
        
        <?php $total = 0; foreach ($data_b->all() as $v) {
            //echo $v->Kd_Bid.' - '.$v->Nama_Bidang.'<br>';
            echo "<tr><td class=\"no_top_bott\">".$v->Kd_Keg."</td><td class=\"no_top_bott\"><b>".wordwrap($v->rincianKegiatan->uraian,45,"<br>",TRUE).'</b></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott" align="right">Rp. '.number_format(round($v->Anggaran), 0, ',','.').'</td></tr>';
            foreach ($v->rincian2 as $x) {
                echo '<tr><td class="no_top_bott"></td><td class="no_top_bott"> '.$x->No_Urut.' . '.$x->Uraian.'</td><td class="no_top_bott">'.$x->JmlSatuan.'</td><td class="no_top_bott">'.$x->Satuan.'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->HrgSatuan), 0, ',','.').'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->Anggaran), 0, ',','.').'</td></tr>';
                $total += (int) $x->Anggaran;
            }
        } 
        ?>        
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>

        <tr><td colspan="5" align="right"><b>Total</b></td><td align="right">Rp. <?= number_format(round($total), 0, ',','.') ?></td></tr>
        </table>
        <br>
    </div>
</page>

<page pageset="old">
    <bookmark title="Penerimaan" level="0" ></bookmark>
    <div class="niveau" style="height: 255mm">    
        <h2>Data Pendapatan Desa</h2>
        <table border="1" style="border-collapse: collapse;width: 95%">
        <tr class="midcen"><td>Kode Rekening</td><td style="width: 20px;">Uraian</td><td>Volume</td><td>Satuan</td><td>Harga</td><td>Jumlah</td></tr>
        <tr class="midcen"><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6 = 3 x 5</td></tr>
        
        <tr><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td></tr>
        
        <?php $total = 0; foreach ($data_in->all() as $v) {
            //echo $v->Kd_Bid.' - '.$v->Nama_Bidang.'<br>';
            echo "<tr><td class=\"no_top_bott\">".$v->rincianPembiayaan->kode."</td><td class=\"no_top_bott\"><b>".wordwrap($v->rincianPembiayaan->uraian,45,"<br>",TRUE).'</b></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott" align="right">Rp. '.number_format(round($v->Anggaran), 0, ',','.').'</td></tr>';
            foreach ($v->rincian2 as $x) {
                echo '<tr><td class="no_top_bott"></td><td class="no_top_bott"> '.$x->No_Urut.' . '.$x->Uraian.'</td><td class="no_top_bott">'.$x->JmlSatuan.'</td><td class="no_top_bott">'.$x->Satuan.'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->HrgSatuan), 0, ',','.').'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->Anggaran), 0, ',','.').'</td></tr>';
                $total += (int) $x->Anggaran;
            }
        } 
        ?>        
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>

        <tr><td colspan="5" align="right"><b>Total</b></td><td align="right">Rp. <?= number_format(round($total), 0, ',','.') ?></td></tr>
        </table>
        <br>
    </div>
</page>

<page pageset="old">
    <bookmark title="Pengeluaran" level="0" ></bookmark>
    <div class="niveau" style="height: 255mm">    
        <h2>Data Pendapatan Desa</h2>
        <table border="1" style="border-collapse: collapse;">
        <tr class="midcen"><td>Kode Rekening</td><td>Uraian</td><td>Volume</td><td>Satuan</td><td>Harga</td><td>Jumlah</td></tr>
        <tr class="midcen"><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6 = 3 x 5</td></tr>
        
        <tr><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td><td class="no_bott"></td></tr>
        
        <?php $total = 0; foreach ($data_out->all() as $v) {
            //echo $v->Kd_Bid.' - '.$v->Nama_Bidang.'<br>';
            echo "<tr><td class=\"no_top_bott\">".$v->rincianPembiayaan->kode."</td><td class=\"no_top_bott\"><b>".$v->rincianPembiayaan->uraian.'</b></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott"></td><td class="no_top_bott" align="right">Rp. '.number_format(round($v->Anggaran), 0, ',','.').'</td></tr>';
            foreach ($v->rincian2 as $x) {
                echo '<tr><td class="no_top_bott"></td><td class="no_top_bott"> '.$x->No_Urut.' . '.$x->Uraian.'</td><td class="no_top_bott">'.$x->JmlSatuan.'</td><td class="no_top_bott">'.$x->Satuan.'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->HrgSatuan), 0, ',','.').'</td><td class="no_top_bott" align="right">Rp. '.number_format(round($x->Anggaran), 0, ',','.').'</td></tr>';
                $total += (int) $x->Anggaran;
            }
        } 
        ?>        
        <tr><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td><td class="no_top"></td></tr>

        <tr><td colspan="5" align="right"><b>Total</b></td><td align="right">Rp. <?= number_format(round($total), 0, ',','.') ?></td></tr>
        </table>
        <br>
    </div>
</page>