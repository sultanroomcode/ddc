<?php

//border:#000 solid 1px;
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
        border: 0.2mm #000 solid;
    }
-->
</style>

<page backtop="14mm" backbottom="14mm" orientation="paysage" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <bookmark title="Kwitansi Tanggungjawab Belanja" level="0" ></bookmark>
    <div class="niveau">

        <div style="background:url(./kwitansi.jpg);  background-size: 70% 70%; background-repeat: no-repeat; ">
            <div style="rotate: 90; position: absolute; width: 50mm; height: 7mm; left: 14mm; top: 60mm; font-weight: normal; text-align: center;">
                Setuju Dibayar,<br>Kepala Desa ...<br>PKPK Desa<br><br><br>(Nama Kepdes)
            </div>

            <div style="position: absolute; width: 140mm; height: 7mm; left: 45mm; top: 90mm; font-weight: normal; text-align: center;">
                <table border="0" style="width: 90%">
                    <tr>
                        <td style="width: 70mm">Lunas Dibayar Tgl....<br>Bendahara Desa ....<br><br><br><br>Nama</td>
                        <td>Mengetahui,<br>Pelaksana Kegiatan<br><br><br><br>Nama</td>
                    </tr>
                </table>
            </div>

            <table border="0" style="border-collapse: collapse;">
              <tr>
                <td style="width:78mm; height: 8mm;"></td>
                <td colspan="4"></td>
              </tr>
               <tr>
                <td style="height: 8mm;"></td>
                <td colspan="4">No Bukti</td>
              </tr>
              <tr>
                <td></td>
                <td style="width:8mm;"></td>
                <td colspan="3" style="width: 165mm; height: 8mm;">&nbsp;&nbsp;&nbsp;Nama Penyetor</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td colspan="3" style="height: 8.1mm;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Terbilang Rupiah</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td colspan="3" style="height: 55mm;line-height: 7mm;">Belanja ... Kegiatan ... sebesar Rp. .... Dengan Rincian :<br>
                - ...........................................<br>
                - ...........................................<br>
                - ...........................................<br>
                - ...........................................<br>
                </td>
              </tr>
              <tr>
                <td colspan="3"></td>
                <td>Tuban, <?= date('d-m-Y') ?><br></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td style="height: 22mm;"></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td style="height: 27mm; width: 55mm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah</td>
                <td>Penerima</td>
              </tr>
            </table>
        </div>
    </div>
</page>
