<?php
use yii\helpers\Url;
use frontend\models\transaksi\TaTBP;
//use frontend\models\DesaCount;
//$datac = DesaCount::find()->select(['dana_pendapatan'])->where(['kd_desa' => $kd_desa])->one();
$data = TaTBP::find()->where(['Kd_Desa' => $kd_desa, 'No_Bukti' =>  $kode])->one();
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}
    .no_top {
        border-top: none !important;
    }
    .no_bott {
        border-bottom: none !important;
    }
    .no_top_bott {
        border-top: none !important;
        border-bottom: none !important;
    }

    div.niveau
    {
        padding-left: 2mm;
        /*border: 0.2mm #000 solid;*/
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" orientation="paysage" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <bookmark title="Sommaire" level="0" ></bookmark>
    <div class="niveau">
        <div style="background:url(./kwitansi.jpg);  background-size: 70% 70%; background-repeat: no-repeat; ">
            <table border="0" style="border-collapse: collapse;">
              <tr>
                <td style="width:78mm; height: 8mm;"></td>
                <td colspan="4"></td>
              </tr>
               <tr>
                <td style="height: 8mm;"></td>
                <td colspan="4"><?= $data->No_Bukti ?></td>
              </tr>
              <tr>
                <td></td>
                <td style="width:8mm;"></td>
                <td colspan="3" style="width: 165mm; height: 8mm;">&nbsp;&nbsp;&nbsp;<?= $data->Nm_Penyetor ?></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td colspan="3" style="height: 8.1mm;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $data->terbilang($data->Jumlah, 3) ?> Rupiah</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td colspan="3" style="height: 55mm;line-height: 7mm;"><?= $data->ww($data->Uraian, 80); ?>  </td>
              </tr>
              <tr>
                <td colspan="3"></td>
                <td>Tuban, <?= date('d-m-Y', strtotime($data->Tgl_Bukti)) ?><br></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td style="height: 22mm;"></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td style="height: 27mm; width: 55mm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $data->nf($data->Jumlah) ?></td>
                <td>Penerima</td>
              </tr>
            </table>
        </div>
    </div>
</page>
