<?php
use frontend\models\transaksi\TaRPJMVisi;
use frontend\models\transaksi\TaRPJMBidang;
$data = TaRPJMVisi::find()->where(['Kd_Desa' => $kd_desa]);
$datab = TaRPJMBidang::find()->where(['Kd_Desa' => $kd_desa]);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
        border: 0.2mm #000 solid;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 10pt">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 100%; text-align: left">
                    Data RPJM Desa
                </td>
            </tr>
        </table>
    </page_header>

    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 100%; text-align: right">
                    Halaman [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Data Visi Misi RPJM" level="0" ></bookmark>
    <div class="niveau">
        <h2>Data RPJM Desa</h2>
        
        <?php $a = $b = $c = $d = 1; foreach ($data->all() as $v) {
            echo $a.'.'.$v->Uraian_Visi.'<br>';
            $b = $c = $d = 1;
            foreach ($v->misi as $x) {
                echo $a.'.'.$b.'.'.$x->Uraian_Misi.'<br>';
                $c = $d = 1;
                foreach ($x->tujuan as $y) {
                    echo $a.'.'.$b.'.'.$c.'.'.$y->Uraian_Tujuan.'<br>';
                    $d = 1;
                    foreach ($y->sasaran as $z) {
                        echo $a.'.'.$b.'.'.$c.'.'.$d.'.'.$z->Uraian_Sasaran.'<br>';
                        $d++;
                    }
                    $c++;
                }
                $b++;
            }
            $a++;
        } 
        ?>        
        
        <br>
    </div>
</page>

<page pageset="old">
    <bookmark title="Data Bidang RPJM" level="0" ></bookmark>
    <div class="niveau" style="height: 255mm">    
        <?php foreach ($datab->all() as $v) {
            echo $v->Kd_Bid. ' - '. $v->Nama_Bidang.'<br>';
            foreach ($v->kegiatan as $x) {
                echo "- ". $x->Kd_Keg .' - '. $x->Nama_Kegiatan ."<br>";
            }
        } ?>
    </div>
</page>