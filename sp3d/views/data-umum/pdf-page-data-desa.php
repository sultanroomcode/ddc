<?php
use frontend\models\transaksi\TaDesa;
$data = TaDesa::findOne(['Kd_Desa' => $kd_desa]);
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
        border: 0.2mm #000 solid;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 10pt">
    <bookmark title="Sommaire" level="0" ></bookmark>
    <div class="niveau">
        <h2>Data Desa : <?= $data->desa->Nama_Desa ?> - <?= $data->desa->kecamatan->Nama_Kecamatan ?> - Tuban</h2>
        Tahun <?= $data->Tahun ?><br>
        
        Nama Kades : <?= $data->Nm_Kades ?><br>
        Jabatan Kades : <?= $data->Jbt_Kades ?><br>
        <br>
        Nama : <?= $data->Nm_Sekdes ?><br>
        NIP : <?= $data->NIP_Sekdes ?><br>
        Jabatan : <?= $data->Jbt_Sekdes ?><br>
        <br>
        Nama : <?= $data->Nm_Kaur_Keu ?><br>
        Jabatan : <?= $data->Jbt_Kaur_Keu ?><br>
        <br>
        Nama : <?= $data->Nm_Bendahara ?><br>
        Jabatan : <?= $data->Jbt_Bendahara ?><br>
        <br>
        No. Perdes : <?= $data->No_Perdes ?><br>
        Tanggal Perdes : <?= $data->Tgl_Perdes ?><br>
        <br>
        No. Perdes PB : <?= $data->No_Perdes_PB ?><br>
        Tanggal Perdes PB : <?= $data->Tgl_Perdes_PB ?><br>
        <br>
        
        No. Perdes PJ : <?= $data->No_Perdes_PJ ?><br>
        Tanggal Perdes PJ : <?= $data->Tgl_Perdes_PJ ?><br>
        <br>
        Alamat : <?= $data->Alamat ?><br>
        
        Status : <?= $data->Status ?><br>
        NPWP : <?= $data->NPWP ?><br>
    </div>
</page>
