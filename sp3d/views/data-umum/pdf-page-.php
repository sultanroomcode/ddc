<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 2mm;
        border: 0.2mm #000 solid;
    }
-->
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 5pt">
    <bookmark title="Lampiran 1 - Anggaran Pendapatan dan Belanja" level="0" ></bookmark>
    <div class="niveau" style="font-size: 7pt; height: 255mm">    
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm;">
                    LAMPIRAN I<br>
                    PERATURAN DESA {...}<br>
                    NOMOR {...} TAHUN {...}<br>
                    TENTANG<br>
                    ANGGARAN PENDAPATAN DAN BELANJA DESA<br>
                    TAHUN ANGGARAN {...}<br>
                </td>
            </tr>
        </table>

        <b style="text-align: center;"><br>
        ANGGARAN PENDAPATAN DAN BELANJA DESA<br>
        PEMERINTAHAN DESA {...}<br>
        TAHUN ANGGARAN {...}</b><br>

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 15%; padding: 2mm;">KODE</th>
                <th style="width: 52%; padding: 2mm;">URAIAN</th>
                <th style="width: 17%; padding: 2mm;">ANGGARAN *<br>(Rp)</th>
                <th style="width: 14%; padding: 2mm;">KETERANGAN</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
            </tr>
            <?php for ($i=0; $i < 4; $i++) { ?>
            <tr>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>D</td>
            </tr>
            <?php } ?>
        </table>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    KEPALA DESA {...}<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    ................................................<br>
                    <b>(Nama Tanpa Gelar, Tanda Tangan dan Cap)</b><br>
                </td>
            </tr>
        </table>
    </div>
</page>

<page orientation="paysage">
    <bookmark title="Lampiran 2 - Daftar Dana Cadangan" level="0"></bookmark>
    <div class="niveau" style="font-size: 7pt; height: 195mm">    
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm;">
                    LAMPIRAN II<br>
                    PERATURAN DESA {...}<br>
                    NOMOR {...} TAHUN {...}<br>
                    TENTANG<br>
                    ANGGARAN PENDAPATAN DAN BELANJA DESA<br>
                    TAHUN ANGGARAN {...}<br>
                </td>
            </tr>
        </table>

        <b style="text-align: center;"><br>
        DAFTAR DANA CADANGAN DESA {...}<br>
        TAHUN ANGGARAN {...}</b><br>

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 5%; padding: 2mm;">NO</th>
                <th style="width: 11%; padding: 2mm;">Tujuan<br> Pembentukan<br> Dana Cadangan</th>
                <th style="width: 11%; padding: 2mm;">Dasar Hukum<br> Pembentukan<br> Dana Cadangan</th>
                <th style="width: 11%; padding: 2mm;">Jumlah Dana<br> Cadangan yang<br> Direncanakan (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Saldo Awal<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Transfer Dari<br> Kas Umum<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Transfer Ke<br> Kas Umum<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Saldo<br> Akhir (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Sisa Dana yang<br> Belum<br> Dicadangkan<br> (Rp)</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
            </tr>
            <?php for ($i=0; $i < 3; $i++) { ?>
            <tr>
                <td><?= $i ?></td>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>D</td>
                <td>E</td>
                <td>F</td>
                <td>G</td>
                <td>H</td>
            </tr>
            <?php } ?>
        </table>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    KEPALA DESA {...}<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    ................................................<br>
                    <b>(Nama Tanpa Gelar, Tanda Tangan dan Cap)</b><br>
                </td>
            </tr>
        </table>
    </div>
</page>

<page orientation="paysage">
    <bookmark title="Lampiran 3 - Penyertaan Modal" level="0"></bookmark>
    <div class="niveau" style="font-size: 7pt; height: 195mm">    
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm;">
                    LAMPIRAN III<br>
                    PERATURAN DESA {...}<br>
                    NOMOR {...} TAHUN {...}<br>
                    TENTANG<br>
                    ANGGARAN PENDAPATAN DAN BELANJA DESA<br>
                    TAHUN ANGGARAN {...}<br>
                </td>
            </tr>
        </table>

        <b style="text-align: center;"><br>
        DAFTAR PENYERTAAN MODAL DESA {...}<br>
        TAHUN ANGGARAN {...}</b><br>

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 5%; padding: 2mm;">NO</th>
                <th style="width: 11%; padding: 2mm;">Tujuan<br> Pembentukan<br> Dana Cadangan</th>
                <th style="width: 11%; padding: 2mm;">Dasar Hukum<br> Pembentukan<br> Dana Cadangan</th>
                <th style="width: 11%; padding: 2mm;">Jumlah Dana<br> Cadangan yang<br> Direncanakan (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Saldo Awal<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Transfer Dari<br> Kas Umum<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Transfer Ke<br> Kas Umum<br> (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Saldo<br> Akhir (Rp)</th>
                <th style="width: 11%; padding: 2mm;">Sisa Dana yang<br> Belum<br> Dicadangkan<br> (Rp)</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
            </tr>
            <?php for ($i=0; $i < 3; $i++) { ?>
            <tr>
                <td><?= $i ?></td>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>D</td>
                <td>E</td>
                <td>F</td>
                <td>G</td>
                <td>H</td>
            </tr>
            <?php } ?>
        </table>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;"></td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    KEPALA DESA {...}<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    ................................................<br>
                    <b>(Nama Tanpa Gelar, Tanda Tangan dan Cap)</b><br>
                </td>
            </tr>
        </table>
    </div>
</page>

<page format="A4" orientation="P">
    <bookmark title="Lampiran B - SPP" level="0" ></bookmark>
    <div class="niveau" style="font-size: 7pt; height: 255mm"> 
        <b style="text-align: center;"><br>
        SURAT PERMINTAAN PEMBAYARAN (SPP)<br>
        DESA {...} KECAMATAN {...}<br>
        TAHUN ANGGARAN {...}</b><br>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr><td>1. Bidang</td><td>: </td></tr>
            <tr><td>2. Kegiatan</td><td>: </td></tr>
            <tr><td>3. Waktu Pelaksanaan</td><td>: </td></tr>
        </table>

        Rincian Pendanaan<br>

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 5%; padding: 2mm;">NO.</th>
                <th style="width: 20%; padding: 2mm;">URAIAN</th>
                <th style="width: 12%; padding: 2mm;">PAGU<br>ANGGARAN<br>(Rp)</th>
                <th style="width: 10%; padding: 2mm;">PENCAIRAN S/D<br>YANG LALU<br>(Rp)</th>
                <th style="width: 10%; padding: 2mm;">PERMINTAAN<br>SEKARANG<br>(Rp)</th>
                <th style="width: 10%; padding: 2mm;">JUMLAH SAMPAI<br>SAAT INI<br>(Rp)</th>
                <th style="width: 10%; padding: 2mm;"><br>SISA DANA<br>(Rp)</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
            </tr>
            <?php for ($i=0; $i < 4; $i++) { ?>
            <tr>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>D</td>
                <td>E</td>
                <td>F</td>
                <td>G</td>
            </tr>
            <?php } ?>
        </table>

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;">
                    Telah dilakukan verifikasi,<br>
                    Sekretaris Desa,<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Nama<br>
                </td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    Tuban, <?= date('d-F-Y') ?><br>
                    Pelaksana Kegiatan<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Nama<br>
                </td>
            </tr>

            <tr>
                <td style="width: 60%; padding: 2mm;">
                    Setuju untuk dibayarkan,<br>
                    Kepala Desa,<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Nama<br>
                </td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    Telah dibayar lunas,<br>
                    Bendahara<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Nama<br>
                </td>
            </tr>
        </table>
    </div>
</page>

<page format="A4" orientation="P">
    <bookmark title="Lampiran D - SPM" level="0" ></bookmark>
    <div class="niveau" style="font-size: 7pt; height: 255mm"> 
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td><img src="./wh-logo.png" style="width: 10mm"></td>
                <td><b style="text-align: center;"><br>
        PEMERINTAH DESA {...}<br>
        KECAMATAN {...} KABUPATEN {...}<br>
        Jalan {alamat_kantor_desa} Telepon {...}</b><br></td>
            </tr>
        </table>


        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr><td></td> <td style="width: 70%;"></td> <td>Tuban, <?= date('d-F-Y') ?></td></tr>
            <tr><td></td> <td></td> <td>a</td></tr>
            <tr><td>Nomor</td> <td>: 920/{no surat keluar}/414.{kode_ds}/{tahun}</td> <td>Kepada :</td></tr>
            <tr><td>Sifat</td> <td>: Biasa</td><td>Yth. Bendahara Umum Desa</td></tr>
            <tr><td>Lampiran</td> <td>: 1 (satu) berkas</td><td>di -</td></tr>
            <tr><td>Perihal</td> <td>: SPM Kegiatan ...</td><td>.........</td></tr>
        </table>

        Bersama ini disampaikan dengan hormat Surat Perintah Membayar (SPM) dengan rincian sebagai berikut :

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 10%; padding: 2mm;">NO.</th>
                <th style="width: 10%; padding: 2mm;">KODE REKENING</th>
                <th style="width: 40%; padding: 2mm;">URAIAN</th>
                <th style="width: 17%; padding: 2mm;">BESARNYA</th>
                <th style="width: 17%; padding: 2mm;">KET.</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
            </tr>
            <?php for ($i=0; $i < 4; $i++) { ?>
            <tr>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>D</td>
                <td>E</td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="3" align="center">JUMLAH</td>
                <td>Rp.</td>
                <td></td>
            </tr>
        </table>

        Bendahara segera menyerahkan kepada pelaksana kegiatan sesuai dengan SPP yang diajukan<br>
        Demikian untuk menjadi perhatian dan dilaksanakan dengan penuh tanggungjawab.

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;">
                    
                </td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    PEMEGANG KEKUASAAN PENGELOLAAN KEUANGAN<br>
                    DESA {...}
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Nama<br>
                </td>
            </tr>
        </table>
    </div>
</page>

<page format="A4" orientation="P">
    <bookmark title="Lampiran E -  SPM Kolektif" level="0" ></bookmark>
    <div class="niveau" style="font-size: 7pt; height: 255mm"> 
        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td><img src="./wh-logo.png" style="width: 10mm"></td>
                <td><b style="text-align: center;"><br>
        PEMERINTAH DESA {...}<br>
        KECAMATAN {...} KABUPATEN {...}<br>
        Jalan {alamat_kantor_desa} Telepon {...}</b><br></td>
            </tr>
        </table>


        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr><td></td> <td style="width: 70%;"></td> <td>Tuban, <?= date('d-F-Y') ?></td></tr>
            <tr><td></td> <td></td> <td>a</td></tr>
            <tr><td>Nomor</td> <td>: 920/{no surat keluar}/414.{kode_ds}/{tahun}</td> <td>Kepada :</td></tr>
            <tr><td>Sifat</td> <td>: Biasa</td><td>Yth. Bendahara Umum Desa</td></tr>
            <tr><td>Lampiran</td> <td>: 1 (satu) berkas</td><td>di -</td></tr>
            <tr><td>Perihal</td> <td>: SPM Perintah Membayar</td><td>.........</td></tr>
        </table>

        Bersama ini disampaikan dengan hormat Surat Perintah Membayar (SPM) dengan rincian sebagai berikut :

        <table border="1" style="border-collapse: collapse; width: 95%">
            <tr align="center" valign="middle">
                <th style="width: 10%; padding: 2mm;">NO.</th>
                <th style="width: 10%; padding: 2mm;">KODE REKENING</th>
                <th style="width: 40%; padding: 2mm;">URAIAN</th>
                <th style="width: 17%; padding: 2mm;">BESARNYA</th>
                <th style="width: 17%; padding: 2mm;">KET.</th>
            </tr>
            <tr align="center">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
            </tr>
            <?php for ($i=0; $i < 2; $i++) { ?>
            <tr>
                <td></td>
                <td>Kegiatan</td>
                <td>C</td>
                <td>D</td>
                <td>E</td>
            </tr>
            
            <tr>
                <td colspan="3" align="center">JUMLAH</td>
                <td>Rp.</td>
                <td></td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="3" align="center">JUMLAH TOTAL</td>
                <td>Rp.</td>
                <td></td>
            </tr>
        </table>

        Bendahara segera menyerahkan kepada pelaksana kegiatan sesuai dengan SPP yang diajukan<br>
        Demikian untuk menjadi perhatian dan dilaksanakan dengan penuh tanggungjawab.

        <table border="0" style="border-collapse: collapse; width: 95%">
            <tr>
                <td style="width: 60%; padding: 2mm;">
                    
                </td>
                <td style="width: 40%; padding: 2mm; text-align: center;">
                    PEMEGANG KEKUASAAN PENGELOLAAN KEUANGAN<br>
                    DESA {...}
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Nama<br>
                </td>
            </tr>
        </table>
    </div>
</page>

<page backtop="14mm" backbottom="14mm" orientation="paysage" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <bookmark title="Kwitansi Tanggungjawab Belanja" level="0" ></bookmark>
    <div class="niveau">

        <div style="background:url(./kwitansi.jpg);  background-size: 70% 70%; background-repeat: no-repeat; ">
            <div style="rotate: 90; position: absolute; width: 50mm; height: 7mm; left: 14mm; top: 60mm; font-weight: normal; text-align: center;">
                Setuju Dibayar,<br>Kepala Desa ...<br>PKPK Desa<br><br><br>(Nama Kepdes)
            </div>

            <div style="position: absolute; width: 140mm; height: 7mm; left: 45mm; top: 90mm; font-weight: normal; text-align: center;">
                <table border="0" style="width: 90%">
                    <tr>
                        <td style="width: 70mm">Lunas Dibayar Tgl....<br>Bendahara Desa ....<br><br><br><br>Nama</td>
                        <td>Mengetahui,<br>Pelaksana Kegiatan<br><br><br><br>Nama</td>
                    </tr>
                </table>
            </div>

            <table border="0" style="border-collapse: collapse;">
              <tr>
                <td style="width:78mm; height: 8mm;"></td>
                <td colspan="4"></td>
              </tr>
               <tr>
                <td style="height: 8mm;"></td>
                <td colspan="4">No Bukti</td>
              </tr>
              <tr>
                <td></td>
                <td style="width:8mm;"></td>
                <td colspan="3" style="width: 165mm; height: 8mm;">&nbsp;&nbsp;&nbsp;Nama Penyetor</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td colspan="3" style="height: 8.1mm;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Terbilang Rupiah</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td colspan="3" style="height: 55mm;line-height: 7mm;">Belanja ... Kegiatan ... sebesar Rp. .... Dengan Rincian :<br>
                - ...........................................<br>
                - ...........................................<br>
                - ...........................................<br>
                - ...........................................<br>
                </td>
              </tr>
              <tr>
                <td colspan="3"></td>
                <td>Tuban, <?= date('d-m-Y') ?><br></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td style="height: 22mm;"></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td style="height: 27mm; width: 55mm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah</td>
                <td>Penerima</td>
              </tr>
            </table>
        </div>
    </div>
</page>
