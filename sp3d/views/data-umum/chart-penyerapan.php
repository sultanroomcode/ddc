<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(['action' => 'javascript:void(0)', 'id' => 'login-form', 'options' => [
          'class' => 'form-inline',
          'role' => 'form'
        ]]); ?>

        <div class="form-group">
          <?= $form->field($model, 'tahun')->dropdownList($model->showTahun(), ['class' => 'selectpickera']) ?>
        </div>

        <div class="form-group">
          <?= $form->field($model, 'refkecamatan')->dropdownList($model->showKecamatan(), ['class' => 'selectpickera', 'onchange' => 'cekDesa()']) ?>
        </div>

        <div class="form-group">
          <?= $form->field($model, 'refdesa')->dropdownList(['' => '---'], ['class' => 'selectpickerb']) ?>
        </div>

        <div class="form-group">
              <button onclick="getData();" class="btn btn-success">Data</button>
        </div>

        <?php ActiveForm::end(); ?>  
    </div>

    <div id="dataplace"></div>
  </div>
</div>

<script>
$('.selectpickera, .selectpickerb').selectpicker({
  liveSearch:true
});
function getData(){
  var tahun = $('#penyerapan-tahun').val();
  var urlid = $('#penyerapan-refkecamatan').val();
  var urlid2 = $('#penyerapan-refdesa').val();

  $('#dataplace').load(base_url+'data-umum/data-apbn?tahun='+tahun+'&kid='+urlid+'&did='+urlid2);
}
function cekDesa(){
    var urlid = $('#penyerapan-refkecamatan').val();
    $.post(base_url+'data-umum/referensi-desa2?kid='+urlid+'&form=1').done(function(res){
        $('#penyerapan-refdesa').html(res);
        $('.selectpickerb').selectpicker('refresh');
    });
}

cekDesa();
</script>
<?php
// $this->registerCssFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css')
// $this->registerJsFile('//localhost/local-cdn/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js')