<?php
return [
    'adminEmail' => 'sp3d@jatimprov.go.id',
    'urlServer' => '192.168.43.92',
    // 'urlServer' => '192.168.0.104',
    'directoryIcon' => '../css/SuperAdmin-1-0-3/Template/1-0-3/',
    'offline' => ($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '127.0.0.1')?true:false,
];
