<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

if($params['offline']) {
    $db_access = [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ddc',
            'username' => 'taroom',
            'password' => '1234',
            'charset' => 'utf8',
        ];
} else {
    $db_access = [
            // 'class' => 'yii\db\Connection',
            // 'dsn' => 'mysql:host=localhost;dbname=mediatub_sp3d',
            // 'username' => 'mediatub_cli',
            // 'password' => 'XUIJOy70sH7di5pvVG',
            // 'charset' => 'utf8',

            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ddc',
            'username' => 'taroom',
            'password' => '1234',
            'charset' => 'utf8',
        ];
}

return [
    'id' => 'sp3d',
    'language' => 'id-ID',
    'name' => 'sp3d',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'sp3d\controllers',
    'as access' => [
         'class' => '\hscstudio\mimin\components\AccessControl',
         'allowActions' => [
            // add wildcard allowed action here!, data yang bisa di akses umum
            '*',
            'mimin/*'
            /*'site/*',
            'data-umum-desa/*',
            'data-check/*',
            'data-umum/*',
            'labs/*',
            'debug/*',
            'gii/*',
            , */// only in dev mode
        ],
    ],
    'components' => [
        'db' => $db_access,
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '749163426623-bj4925k1jdd9p1h6ji9hc2tqig51uvsq.apps.googleusercontent.com',
                    'clientSecret' => 'eNu_tziehuZgQi_DzQ7Fy1Mh',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => 'facebook_client_id',
                    'clientSecret' => 'facebook_client_secret',
                ],
                // etc.
            ],
        ],
        'request' => [
            // Enable JSON Input:
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null && Yii::$app->request->get('suppress_response_code')) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data,
                    ];
                    $response->statusCode = 200;
                }
            },
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'bundles' => [
                // 'yii\web\JqueryAsset' => [
                //     'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                // ],
            ],
        ],
        'user' => [
            'identityClass' => 'sp3d\models\User',
            'authTimeout' => 3600,//detik, harus set beforeAction di tiap controller
            // 'enableAutoLogin' => true,
        ],
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => false,
            'useFileTransport' => true,
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',

                //'host' => 'smtp.gmail.com',
                //'username' => 'sultanroomcode@gmail.com',
                //'password' => 'tar00mmemang0ke',
                //'port' => '567',
                //'encryption' => 'tls'//for port 465
                 'host' => 'bulsjh201202.jogjahost.com',
                 'username' => 'lpm@unirow.ac.id',
                 'password' => '@born2beprogrammer',
                 'port' => '465',
                 'encryption' => 'ssl'//for port 465
            ],*/
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            // 'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                 [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['api/operator'],
                    'pluralize' => false
                 ]
            ]
        ],
    ],

    'modules' => [
        'api' => [
            'basePath' => '@app/modules/api',
            'class' => 'sp3d\modules\api\Api',
        ],
        'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        'transaksi' => [
            'class' => 'sp3d\modules\transaksi\Transaksi',
        ],
        'referensi' => [
            'class' => 'sp3d\modules\referensi\Referensi',
        ],
        'umum' => [
            'class' => 'sp3d\modules\umum\Umum',
        ],
        'pendamping' => [
            'class' => 'sp3d\modules\pendamping\Pendamping',
        ],
        'sp3ddashboard' => [
            'class' => 'sp3d\modules\sp3ddashboard\Sp3dDashboard',
        ],
        'posyandu' => [
            'class' => 'sp3d\modules\posyandu\Posyandu',
        ],
        'bumdes' => [
            'class' => 'sp3d\modules\bumdes\Bumdes',
        ],
        'klinikbumdes' => [
            'class' => 'sp3d\modules\klinikbumdes\KlinikBumdes',
        ],
        'kpm' => [
            'class' => 'sp3d\modules\kpm\Kpm',
        ],
        'operatora' => [//dikarenakan folder akses data operator bernama 'operator' agar tidak bentrok
            'class' => 'ddcop\modules\operator\Operator',
        ],
        'ddc' => [
            'class' => 'sp3d\modules\ddc\Ddc',
        ],
        'chart' => [
            'class' => 'sp3d\modules\chart\Chart',
        ],
        'laporan' => [
            'class' => 'sp3d\modules\laporan\Laporan',
        ],

        // 'lkmd' => [
        //     'class' => 'sp3d\modules\lkmd\Lkmd',
        // ],//lembaga ketahanan masyarakat desa
        'lpmd' => [//as LPMD
            'class' => 'sp3d\modules\lpmd\Lpmd',
        ],
        'pkk' => [
            'class' => 'sp3d\modules\pkk\Pkk',
        ],
        'kartar' => [
            'class' => 'sp3d\modules\kartar\Kartar',
        ],
        'lembagaadat' => [
            'class' => 'sp3d\modules\lembagaadat\LembagaAdat',
        ],
        'kelas' => [
            'class' => 'sp3d\modules\kelas\Kelas',
        ],
        'puem-editor' => [
            'class' => 'sp3d\modules\puem\Puem',
        ],
    ],    
    'params' => $params,
];