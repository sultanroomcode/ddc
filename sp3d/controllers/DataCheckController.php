<?php
namespace sp3d\controllers;

use Yii;
use common\models\LoginForm;
use sp3d\models\transaksi\TaRAB;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Site controller
 */
class DataCheckController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function init()
    {   }

    public function actionIndex()
    {
        return $this->renderPartial('index');
    }

    public function actionTransaksiRab($rid, $kd_desa, $tahun=2017)
    {
        $data = TaRAB::findOne(['Kd_Rincian' => $rid, 'Kd_Desa' => $kd_desa, 'tahun' => $tahun]);
        if($data != null){
            echo "<span class='label label-danger'>Rekening Rincian sudah ada / dipakai</span>";
        } else {
            echo "<span class='label label-success'>Anda Dapat Menggunakan Rekening Rincian ini</span>";
        }
    }
}
