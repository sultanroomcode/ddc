<?php
namespace sp3d\controllers;
use sp3d\models\TaDesa;

class DataUmumDesaController extends \yii\web\Controller
{
    public function actionCreateDesa()
    {
        return $this->render('create-desa');
    }

    public function actionIndex()
    {
        $data = TaDesa::find()->count();
        return $this->renderPartial('index', [
            'ta_desa' => $data
        ]);
    }

    public function actionChart()
    {
        return $this->renderPartial('chart', []);
        Yii::$app->response->format = Response::FORMAT_JSON;
        //var_dump($datar);
        return ['data' => [
            'element' => $this->renderPartial('chart-akumulasi', ['data' => $data])
            ]
        ];
    }

    public function actionUpdateDesa()
    {
        return $this->render('update-desa');
    }
}
