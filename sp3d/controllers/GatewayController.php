<?php
namespace sp3d\controllers;

use Yii;
use common\models\LoginForm;
use sp3d\models\PasswordResetRequestForm;
use sp3d\models\ResetPasswordForm;
use sp3d\models\AESServer;
use sp3d\models\Credential;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class GatewayController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionDekrip()
    {
        $enkrip = new AESServer('aku','enkripsi',128);
        $text = $enkrip->encrypt();
        echo $text;

        $dekrip = new AESServer($text, 'enkripsi', 128);
        $text2 = $dekrip->decrypt();
        echo $text2;
    }

    public function actionIndex()
    {
        //$this->enableCsrfValidation = false;
        Yii::$app->controller->enableCsrfValidation = false;
        var_dump(Yii::$app->request->post());
        echo "aaaab";
        // if(Yii::$app->request->post('submisi') == 1){            
        //     $user = htmlentities(trim($this->input->post('username')));
        //     $pass = htmlentities(trim($this->input->post('password')));

        //     $cre = new Credential();
        //     $res = $cre->login($user, $pass);

        //     if($res){
        //         // $result = $dbres->fetch_object();
        //         // move_uploaded_file($_FILES['filegen']['tmp_name'],'client_file/'.$_FILES['filegen']['name']);
        //         // //write log
        //         // $db->query("INSERT INTO log (id,cre_id,aktivitas,tanggal_aksi) VALUES ('".time().$result->id."','".$result->id."','Mengupload File',NOW())");
        //         // //$db->query("INSERT INTO file_transfer_log ('".$result->id."','Mengupload File',NOW())");
        //         $data = ['status' => 'success', 'user' => $user];
        //     } else {
        //         $data = ['status' => 'fail', 'user' => $user];
        //     }

        //     echo json_encode($data);
        //}

        // masih
        /*Bad Request (#400)
        Unable to verify your data submission.
        The above error occurred while the Web server was processing your request.
        Please contact us if you think this is a server error. Thank you.
*/
    }
}
