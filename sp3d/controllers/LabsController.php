<?php
namespace sp3d\controllers;

use Yii;

use sp3d\models\AESServer;
use sp3d\models\MsSql;
use sp3d\models\Log;
use sp3d\models\User;
use sp3d\models\filterxls\ReadDdcExcelOnly;
use sp3d\models\Credential;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use sp3d\modules\bumdes\models\KlasifikasiFinalBumdes;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class LabsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['pass'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['pass'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init(){
        parent::init();
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionPass($p)
    {
        echo Yii::$app->security->generatePasswordHash($p);
    }

    //handling csv file
    public function actionReadArr()
    {
        $a = range('H', 'Z');
        $b = ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO'];
        $c = array_merge($a, $b);
        var_dump($c);
    }

    public function actionCsv()
    {
        echo fgetcsv(handle);
    }

    public function actionMsaccess()
    {
        $model = new MsSql();
        var_dump($model);
    }

    public function actionPassChangeMass()
    {
    
        $arr = [
    		['3517120001','87398681'],
    		['3517120002','81924743'],
    		['3517120003','39122009'],
    		['3517120004','81196899'],
    		['3517120005','16690673'],
    		['3517120006','24213562'],
    		['3517120007','44466857'],
    		['3517120008','34562683'],
    		['3517120009','42286071'],
    		['3517120010','90397949'],
    		['3517120011','47386474'],
    		['3517120012','33214111'],
    		['3517120013','20192565'],
    		['3517120014','75997619']
        ];
        
        $max = 24;
        $i = 1;
        foreach($arr as $v){
        	// echo '- '.$v[1];
        	if($i > 24){ echo 'end'; break; }
        	echo "UPDATE user SET password_default = '$v[1]', password_hash = '".Yii::$app->security->generatePasswordHash($v[1])."' WHERE id = '$v[0]'; <br>";
        	$i++;

        	// $transaction = Yii::$app->db->beginTransaction();
	        // try {
	        // 	$model = User::findOne($v[0]);
	        // 	if($model != null){
	        // 		echo "<br>".$v[0];
	        // 		$model->password_hash = Yii::$app->security->generatePasswordHash($v[1]);
	        // 		$model->password_default = $v[1];
	        // 		$st = $model->save(false);
	        // 		var_dump($st);
	        // 	}

	        //     $transaction->commit();
	        // } catch(\Exception $e) {
	        //     $transaction->rollBack();
	        //     throw $e;
	        // }
        }
    }

    public function actionReklasifikasi()
    {
        $m = KlasifikasiFinalBumdes::find();
        $data = Yii::$app->request->get();
        if(isset($data['offset'])){
            $m->offset((int) $data['offset']);
        }

        if(isset($data['limit'])){
            $m->limit((int) $data['limit']);
        }
        $i = 1;
        foreach($m->all() as $v){
            Yii::$app->db->createCommand("CALL HitungKlasifikasi(:kd_desa, :id_bumdes) ",  [":kd_desa" => $v->kd_desa, ":id_bumdes" => $v->id_bumdes])->execute();
            echo $i.', <br>';
            $i++;
        }
    }

    // labs/user-mass-insert
    public function actionUserMassInsert()
    {
        /*$arr = [
            //id. username, type, passwprd, description aktif
            ['35', '35', 'provinsi', 'anon1@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('JAWATIMUR'), 'Bangkalan-Madura',Yii::$app->security->generateRandomString(), '1', '10', time(), time()],
            ['3526', '3526', 'kabupaten', 'anon2@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('BANGKALAN'), 'Bangkalan-Madura',Yii::$app->security->generateRandomString(), '2', '10', time(), time()],
            ['3526160', '3526160', 'kecamatan', 'anon3@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('20328UMI'), 'TANJUNGBUMI', Yii::$app->security->generateRandomString(), '3','10', time(), time()],
            ['3526060', '3526060', 'kecamatan', 'anon4@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('17245ANG'), 'KONANG', Yii::$app->security->generateRandomString(), '4','10', time(), time()],
            ['3526060007', '3526060007', 'desa', 'anon5@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('14497UNG'), 'BANDUNG', Yii::$app->security->generateRandomString(), '5','10', time(), time()],
            ['3526060004', '3526060004', 'desa', 'anon6@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('78857BAN'), 'BATOKABAN', Yii::$app->security->generateRandomString(), '6','10', time(), time()],
            ['3526160014', '3526160014', 'desa', 'anon7@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('41567BAR'), 'AENGTABAR', Yii::$app->security->generateRandomString(), '7','10', time(), time()],
            ['3526160003', '3526160003', 'desa', 'anon8@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('35126AYA'), 'BANDANG DAYA',Yii::$app->security->generateRandomString(), '8', '10', time(), time()]
        ];*/
        // $arr = //desa
        // [
        //     // ['3526160003','26','160','003'],
        //     // ['3526160014','26','160','014'],
        //     // ['3526060004','26','060','004'],
        //     // ['3526060007','26','060','007']
        //     // ['3504180','26','060','007']

        // ];
        //
        // Yii::$app->db->createCommand()->batchInsert('user', ['id', 'username', 'type', 'email','password_hash', 'description', 'auth_key', 'password_reset_token', 'status', 'created_at', 'updated_at'], $arr)->execute();
        // Yii::$app->db->createCommand()->batchInsert('Ta_UserDesa', ['Id', 'kd_kabupaten',  'Kd_Kec', 'Kd_Desa'], $arr)->execute();
        $tcreate = time();
        $arr = [
            //id. username, type, passwprd, description aktif
            // ['35', '35', 'provinsi', 'anon1@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('JAWATIMUR'), 'Bangkalan-Madura',Yii::$app->security->generateRandomString(), '1', '10', time(), time()],
            // ['3526', '3526', 'kabupaten', 'anon2@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('BANGKALAN'), 'Bangkalan-Madura',Yii::$app->security->generateRandomString(), '2', '10', time(), time()],
            // ['3526160', '3526160', 'kecamatan', 'anon3@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('20328UMI'), 'TANJUNGBUMI', Yii::$app->security->generateRandomString(), '3','10', time(), time()],
            // ['3526060', '3526060', 'kecamatan', 'anon4@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('17245ANG'), 'KONANG', Yii::$app->security->generateRandomString(), '4','10', time(), time()],
            // ['3526060007', '3526060007', 'desa', 'anon5@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('14497UNG'), 'BANDUNG', Yii::$app->security->generateRandomString(), '5','10', time(), time()],
            // ['3526060004', '3526060004', 'desa', 'anon6@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('78857BAN'), 'BATOKABAN', Yii::$app->security->generateRandomString(), '6','10', time(), time()],
            // ['3526160014', '3526160014', 'desa', 'anon7@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('41567BAR'), 'AENGTABAR', Yii::$app->security->generateRandomString(), '7','10', time(), time()],
            // ['3526160003', '3526160003', 'desa', 'anon8@sp3d.jatimprov.go.id', Yii::$app->security->generatePasswordHash('35126AYA'), 'BANDANG DAYA',Yii::$app->security->generateRandomString(), '8', '10', time(), time()]
        // ];
            // ['3504190', '3504190', 'kecamatan', 'anony1@sp3d.jatimprov.go.id', '11111ANG', Yii::$app->security->generatePasswordHash('11111ANG'),  'KECAMATAN SENDANG', 13, 'asaasa', '10', time(), time()],
            // ['3504180', '3504180', 'kecamatan', 'anony2@sp3d.jatimprov.go.id', '11111OJO', Yii::$app->security->generatePasswordHash('11111OJO'),  'KECAMATAN PAGERWOJO', 123, 'asaaasa', '10', time(), time()],
            // ['3504130', '3504130', 'kecamatan', 'anony4@sp3d.jatimprov.go.id', '11111EKO', Yii::$app->security->generatePasswordHash('11111EKO'),  'KECAMATAN KEDUNGWUNGU', 123, 'asaaassssa', '10', time(), time()],

            ['3513080004',  '3513080004',  'desa', 'anonyandung@sp3d.jatimprov.go.id', '28823IRU', Yii::$app->security->generatePasswordHash('28823IRU'), 'DESA ANDUNGBIRU',124,null,10,2020,2020],
            ['3513080002',  '3513080002',  'desa', 'anonyandungsari@sp3d.jatimprov.go.id', '79171ARI', Yii::$app->security->generatePasswordHash('79171ARI'), 'DESA ANDUNGSARI',124,null,10,2020,2020],
            ['3513080009',  '3513080009',  'desa', 'anonyjangkang@sp3d.jatimprov.go.id', '09208ANG', Yii::$app->security->generatePasswordHash('09208ANG'), 'DESA JANGKANG  ',124,null,10,2020,2020],
            ['3513080013',  '3513080013',  'desa', 'anonydagang@sp3d.jatimprov.go.id', '94172GAN', Yii::$app->security->generatePasswordHash('94172GAN'), 'DESA PEDAGANGAN',124,null,10,2020,2020],
            ['3513080012',  '3513080012',  'desa', 'anonysawah@sp3d.jatimprov.go.id', '78507HAN', Yii::$app->security->generatePasswordHash('78507HAN'), 'DESA PESAWAHAN ',124,null,10,2020,2020],
            ['3513080011',  '3513080011',  'desa', 'anonracek@sp3d.jatimprov.go.id', '10918CEK', Yii::$app->security->generatePasswordHash('10918CEK'), 'DESA RACEK ',124,null,10,2020,2020],
            ['3513080006',  '3513080006',  'desa', 'anonyagung@sp3d.jatimprov.go.id', '34469UNG', Yii::$app->security->generatePasswordHash('34469UNG'), 'DESA RANUAGUNG ',124,null,10,2020,2020],
            ['3513080008',  '3513080008',  'desa', 'anonygedang@sp3d.jatimprov.go.id', '26821ANG', Yii::$app->security->generatePasswordHash('26821ANG'), 'DESA RANUGEDANG',124,null,10,2020,2020],
            ['3513080014',  '3513080014',  'desa', 'anonyrejing@sp3d.jatimprov.go.id', '45244ING', Yii::$app->security->generatePasswordHash('45244ING'), 'DESA REJING',124,null,10,2020,2020],
            ['3513080007',  '3513080007',  'desa', 'anonysegara@sp3d.jatimprov.go.id', '90751RAN', Yii::$app->security->generatePasswordHash('90751RAN'), 'DESA SEGARA',124,null,10,2020,2020],
            ['3513080015',  '3513080015',  'desa', 'anonytgal@sp3d.jatimprov.go.id', '58798ATU', Yii::$app->security->generatePasswordHash('58798ATU'), 'DESA TEGALWATU ',124,null,10,2020,2020],
            ['3513080005',  '3513080005',  'desa', 'anonytiris@sp3d.jatimprov.go.id', '42341RIS', Yii::$app->security->generatePasswordHash('42341RIS'), 'DESA TIRIS ',124,null,10,2020,2020],
            ['3513080003',  '3513080003',  'desa', 'anonyargo@sp3d.jatimprov.go.id', '95411RGO', Yii::$app->security->generatePasswordHash('95411RGO'), 'DESA TLOGOARGO ',124,null,10,2020,2020],
            ['3513080001',  '3513080001',  'desa', 'anonygosari@sp3d.jatimprov.go.id', '04068ARI', Yii::$app->security->generatePasswordHash('04068ARI'), 'DESA TLOGOSARI ',124,null,10,2020,2020],
            ['3513080016',  '3513080016',  'desa', 'anonypari@sp3d.jatimprov.go.id', '01668ARI', Yii::$app->security->generatePasswordHash('01668ARI'), 'DESA TULUPARI  ',124,null,10,2020,2020],
            ['3513080010',  '3513080010',  'desa', 'anonywedus@sp3d.jatimprov.go.id', '89298SAN', Yii::$app->security->generatePasswordHash('89298SAN'), 'DESA WEDUSA',124,null,10,2020,2020]



            /*['3504020', '3504020', 'kecamatan', 'anony5@sp3d.jatimprov.go.id', '999999UNG', Yii::$app->security->generatePasswordHash('999999UNG'), 'KECAMATAN BANDUNG', 123, null, '10', $tcreate, $tcreate],
            ['3504010', '3504010', 'kecamatan', 'anony6@sp3d.jatimprov.go.id', '999999UKI', Yii::$app->security->generatePasswordHash('999999UKI'), 'KECAMATAN BESUKI ', 123, null, '10', $tcreate, $tcreate],
            ['3504110', '3504110', 'kecamatan', 'anony7@sp3d.jatimprov.go.id', '999999NGU', Yii::$app->security->generatePasswordHash('999999NGU'), 'KECAMATAN BOYOLANGU', 123, null, '10', $tcreate, $tcreate],
            ['3504040', '3504040', 'kecamatan', 'anony8@sp3d.jatimprov.go.id', '999999RAT', Yii::$app->security->generatePasswordHash('999999RAT'), 'KECAMATAN CAMPUR DARAT', 123, null, '10', $tcreate, $tcreate],
            ['3504170', '3504170', 'kecamatan', 'anony9@sp3d.jatimprov.go.id', '999999ANG', Yii::$app->security->generatePasswordHash('999999ANG'), 'KECAMATAN GONDANG', 123, null, '10', $tcreate, $tcreate],
            ['3504060', '3504060', 'kecamatan', 'anony10@sp3d.jatimprov.go.id', '999999WIR', Yii::$app->security->generatePasswordHash('999999WIR'), 'KECAMATAN KALIDAWIR', 123, null, '10', $tcreate, $tcreate],
            ['3504160', '3504160', 'kecamatan', 'anony11@sp3d.jatimprov.go.id', '999999MAN', Yii::$app->security->generatePasswordHash('999999MAN'), 'KECAMATAN KAUMAN', 123, null, '10', $tcreate, $tcreate],
            ['3504140', '3504140', 'kecamatan', 'anony12@sp3d.jatimprov.go.id', '999999TRU', Yii::$app->security->generatePasswordHash('999999TRU'), 'KECAMATAN NGANTRU', 123, null, '10', $tcreate, $tcreate],
            ['3504090', '3504090', 'kecamatan', 'anony13@sp3d.jatimprov.go.id', '999999NUT', Yii::$app->security->generatePasswordHash('999999NUT'), 'KECAMATAN NGUNUT', 123, null, '10', $tcreate, $tcreate],
            ['3504030', '3504030', 'kecamatan', 'anony14@sp3d.jatimprov.go.id', '999999KEL', Yii::$app->security->generatePasswordHash('999999KEL'), 'KECAMATAN PAKEL', 123, null, '10', $tcreate, $tcreate],
            ['3504070', '3504070', 'kecamatan', 'anony15@sp3d.jatimprov.go.id', '999999BAN', Yii::$app->security->generatePasswordHash('999999BAN'), 'KECAMATAN PUCANGLABAN', 123, null, '10', $tcreate, $tcreate],
            ['3504080', '3504080', 'kecamatan', 'anony16@sp3d.jatimprov.go.id', '999999GAN', Yii::$app->security->generatePasswordHash('999999GAN'), 'KECAMATAN REJOTANGAN', 123, null, '10', $tcreate, $tcreate],
            ['3504100', '3504100', 'kecamatan', 'anony17@sp3d.jatimprov.go.id', '999999POL', Yii::$app->security->generatePasswordHash('999999POL'), 'KECAMATAN SUMBERGEMPOL', 123, null, '10', $tcreate, $tcreate],
            ['3504050', '3504050', 'kecamatan', 'anony18@sp3d.jatimprov.go.id', '999999UNG', Yii::$app->security->generatePasswordHash('999999UNG'), 'KECAMATAN TANGGUNG GUNUNG', 123, null, '10', $tcreate, $tcreate],
            ['3504120', '3504120', 'kecamatan', 'anony19@sp3d.jatimprov.go.id', '999999TLG', Yii::$app->security->generatePasswordHash('999999TLG'), 'KECAMATAN TULUNGAGUNG', 123, null, '10', $tcreate, $tcreate],*/

            // ['3504130', '3504130', 'kecamatan', 'anony4@sp3d.jatimprov.go.id', '11111EKO', Yii::$app->security->generatePasswordHash('11111EKO'),  'KECAMATAN KEDUNGWUNGU', 123, 'asaaassssa', '10', $tcreate, $tcreate],
        ];

        Yii::$app->db->createCommand()->batchInsert('user', ['id', 'username', 'type', 'email', 'password_default','password_hash', 'description', 'auth_key', 'password_reset_token', 'status', 'created_at', 'updated_at'], $arr)->execute();
        

        $arr = //desa
        [
            // ['desa','3526160003',$tcreate],
            // ['desa','3526160014',$tcreate],
            // ['desa','3526060004',$tcreate],
            // ['desa','3526060007',$tcreate]
            // ['kecamatan','3504190',$tcreate],
            // ['kecamatan','3504130',$tcreate]
            // ['kecamatan','3504130',$tcreate]
            // ['kecamatan', '3504020', $tcreate],
            // ['kecamatan', '3504010', $tcreate],
            // ['kecamatan', '3504110', $tcreate],
            // ['kecamatan', '3504040', $tcreate],
            // ['kecamatan', '3504170', $tcreate],
            // ['kecamatan', '3504060', $tcreate],
            // ['kecamatan', '3504160', $tcreate],
            // ['kecamatan', '3504140', $tcreate],
            // ['kecamatan', '3504090', $tcreate],
            // ['kecamatan', '3504030', $tcreate],
            // ['kecamatan', '3504070', $tcreate],
            // ['kecamatan', '3504080', $tcreate],
            // ['kecamatan', '3504100', $tcreate],
            // ['kecamatan', '3504050', $tcreate],
            // ['kecamatan', '3504120', $tcreate]

            ['desa', '3513080004', 2020],
            ['desa', '3513080002', 2020],
            ['desa', '3513080009', 2020],
            ['desa', '3513080013', 2020],
            ['desa', '3513080012', 2020],
            ['desa', '3513080011', 2020],
            ['desa', '3513080006', 2020],
            ['desa', '3513080008', 2020],
            ['desa', '3513080014', 2020],
            ['desa', '3513080007', 2020],
            ['desa', '3513080015', 2020],
            ['desa', '3513080005', 2020],
            ['desa', '3513080003', 2020],
            ['desa', '3513080001', 2020],
            ['desa', '3513080016', 2020],
            ['desa', '3513080010', 2020]

        ];
        Yii::$app->db->createCommand()->batchInsert('auth_assignment', ['item_name', 'user_id', 'created_at'], $arr)->execute();
    }

    function actionGetCsvMerge(){
        $exporter = new Spreadsheet([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [
                    [
                        'id' => 1,
                        'name' => 'first',
                    ],
                    [
                        'id' => 2,
                        'name' => 'second',
                    ],
                ],
            ]),
            'columns' => [
                // [
                //     'class' => SerialColumn::class,
                // ],
                [
                    'attribute' => 'id',
                ],
                [
                    'attribute' => 'name',
                ],
            ]
        ]);//->render(); // render the document

        // override serial column header :
        $exporter->renderCell('A1', 'Overridden serial column header');

        // add custom footer :
        $exporter->renderCell('A4', 'Custom A4', [
            'font' => [
                'color' => [
                    'rgb' => '#FF0000',
                ],
                'background' => [
                    'rgb' => '#00FF00',
                ],
            ],
        ]);

        // merge footer cells :
        $exporter->mergeCells('A4:B4');
        $exporter->save('file-merge.xls');
    }

    function actionGetCsvAgain(){
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => User::find()->select(['username' , 'description', 'password_default']),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
            ]),
            'columns' => [
                ['attribute' => 'username',],
                ['attribute' => 'description',],
                ['attribute' => 'password_default',],
            ],
        ]);
        $exporter->save('file-a.xls');
        echo "<a href='../file-a.xls'>Download File</a>";
    }

    function actionGetCsv(){
        $exporter = new Spreadsheet([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [
                    [
                        'name' => 'some name',
                        'price' => '9879',
                    ],
                    [
                        'name' => 'name 2',
                        'price' => '79',
                    ],
                ],
            ]),
            'columns' => [
                [
                    'attribute' => 'name',
                    'contentOptions' => [
                        'alignment' => [
                            'horizontal' => 'center',
                            'vertical' => 'center',
                        ],
                    ],
                ],
                [
                    'attribute' => 'price',
                ],
            ],
        ]);
        $exporter->save('file.xls');
    }
}


