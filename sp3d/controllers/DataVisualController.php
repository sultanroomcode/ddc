<?php
namespace sp3d\controllers;

use Yii;
use common\models\LoginForm;
use sp3d\models\transaksi\TaRAB;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Site controller
 */
class DataVisualController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function init()
    {   }

    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    public function actionPendapatanDesa($kd_desa, $tahun=2017)
    {
        return $this->renderAjax('pendapatan-desa',[
        	'kd_desa' => $kd_desa,
        	'tahun' => $tahun
        ]);
    }

    public function actionBelanjaDesa($kd_desa, $tahun=2017)
    {
        return $this->renderAjax('belanja-desa',[
        	'kd_desa' => $kd_desa,
        	'tahun' => $tahun
        ]);
    }

    public function actionPendapatanKecamatan($kd_kecamatan, $tahun=2017)
    {
        return $this->renderAjax('pendapatan-kecamatan',[
            'kd_kecamatan' => $kd_kecamatan,
            'tahun' => $tahun
        ]);
    }

    public function actionBelanjaKecamatan($kd_kecamatan, $tahun=2017)
    {
        return $this->renderAjax('belanja-kecamatan',[
            'kd_kecamatan' => $kd_kecamatan,
            'tahun' => $tahun
        ]);
    }

    public function actionPendapatanKabupaten($tahun=2017)
    {
        return $this->renderAjax('pendapatan-kabupaten',[
            'tahun' => $tahun
        ]);
    }

    public function actionBelanjaKabupaten($tahun=2017)
    {
        return $this->renderAjax('belanja-kabupaten',[
            'tahun' => $tahun
        ]);
    }
}
