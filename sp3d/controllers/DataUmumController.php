<?php
namespace sp3d\controllers;

use Yii;
use common\models\LoginForm;
use sp3d\models\PasswordResetRequestForm;
use sp3d\models\ResetPasswordForm;
use sp3d\models\AESServer;
use sp3d\models\Penyerapan;
use sp3d\models\Credential;
use sp3d\models\DesaCount;
use sp3d\models\KecamatanCount;
use sp3d\models\KabupatenCount;
use sp3d\models\ProvinsiCount;
use sp3d\models\referensi\RefKegiatan;
use sp3d\models\referensi\RefRekeningKegiatan;
use sp3d\models\referensi\RefDesa;
use sp3d\models\transaksi\TaKegiatan;
use sp3d\models\transaksi\TaRAB;
use sp3d\models\transaksi\TaDesa;
use sp3d\models\transaksi\TaMdbUpload;
use sp3d\models\transaksi;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

/**
 * Site controller
 */
class DataUmumController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function init()
    {   }
    //bagian depan
    public function actionDataUserImport()
    {
        $data = Yii::$app->request->get();
        return $this->renderAjax('data-user-import', [
            'data' => $data
        ]);
    }

    public function actionDataTableProvinsi()
    {
        //tahun-anggaran=2018&kabupaten-input=&kecamatan-input=&desa-input=
        $data = Yii::$app->request->get();
        $type = 'kabupaten';
        if ($data['desa-input'] != '' && isset($data['desa-input'])) {
            //'desa':
            $type = 'desa';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_desa' => $data['desa-input']]);
        } else if($data['kecamatan-input'] != '' && isset($data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $model = DesaCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kecamatan' => $data['kecamatan-input']]);
        } else if($data['kabupaten-input'] != '' && isset($data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $model = KecamatanCount::find()->where(['tahun' => $data['tahun-anggaran'], 'kd_kabupaten' => $data['kabupaten-input']]); 
        } else {
            // 'provinsi'
            $type = 'provinsi';
            $model = KabupatenCount::find()->where(['tahun' => $data['tahun-anggaran']]);
        }

        $data['type'] = $type;
        
        return $this->renderAjax('data-table-'.$type, [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionDataCountProvinsi($tahun)
    {
        //querying based on year
        $pc = ProvinsiCount::findOne(['tahun' => $tahun]);
    
        $conn = Yii::$app->db;
        $model = $conn->createCommand("SELECT count(DISTINCT user) AS hitung FROM ta_mdb_upload WHERE tahun = '$tahun'");
        $resultan = $model->queryAll();

        if($pc != null){
            $foo = [
                // 'dana_desa' => $pc->dana_pendapatan.' M',
                // 'desa_input' => $resultan[0]['hitung'],
                // 'pendapatan' => $pc->dana_pendapatan.' M',
                // 'belanja' => $pc->dana_rab.' M',
                // 'pembiayaan' => $pc->dana_penerimaan.' M',

                'dana_desa' => round($pc->dana_pendapatan/1000000000,3).' M',
                'desa_input' => $resultan[0]['hitung'],
                'pendapatan' => round($pc->dana_pendapatan/1000000000,3).' M',
                'belanja' => round($pc->dana_rab/1000000000,3).' M',
                'pembiayaan' => round($pc->dana_penerimaan/1000000000,3).' M',
            ];
        } else {
            $foo = [
                'dana_desa' => 0,
                'desa_input' => $resultan[0]['hitung'],
                'pendapatan' => 0,
                'belanja' => 0,
                'pembiayaan' => 0,
            ];
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $foo;
    }

    public function actionDataOption($tahun, $type='kabupaten', $kode=null)
    {
        switch ($type) {
            case 'kabupaten':
                $model = KabupatenCount::find()->where(['tahun' => $tahun]);
            break;
            case 'kecamatan':
                $model = KecamatanCount::find()->where(['tahun' => $tahun, 'kd_kabupaten' => $kode]);
            break;
            case 'desa':
                $model = DesaCount::find()->where(['tahun' => $tahun, 'kd_kecamatan' => $kode]);
            break;
        }

        return $this->renderPartial('data-option-'.$type, [
            'model' => $model,
            'kode' => $kode,
            'tahun' => $tahun
        ]);
    }
    //apbn
    public function actionDataApbn($tahun, $kid, $did=null)
    {
        if($did != null) {
            $model = DesaCount::findOne(['tahun' => $tahun, 'kd_kecamatan' => $kid, 'kd_desa' => $did]);
        } else {
            $model = \sp3d\models\KecamatanCount::findOne(['tahun' => $tahun, 'kd_kecamatan' => $kid]);
        }

        return $this->renderPartial('data-apbn', [
            'model' => $model,
            'kd_kecamatan' => $kid,
            'kd_desa' => $did,
            'tahun' => $tahun
        ]);
    }

    public function actionIndex()
    {
        return $this->renderPartial('index');
    }

    public function actionPatch()
    {
        return $this->renderAjax('patch');
    }

    public function actionSdat()
    {
        return $this->renderAjax('sdat');
    }

    /*public function actionLaporanUtama($kd_desa, $tahun)
    {
        $foo = [
            ['id' => 'lp-1', 'text' => 'Laporan Perencanaan'.$tahun, 'children' => [
                ['id' => 'lp-1-1', 'text' => 'Laporan RPJM '.$tahun.'<i class="fa fa-file-pdf-o"></i></i>', 'a_attr' => 'href="'.Url::to(['/data-umum/pdf?bagian=data-rpjm-desa&kd_desa='.$kd_desa], true)],
                ['id' => 'lp-1-2', 'text' => 'Laporan Perangkat '.$tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
                ['id' => 'lp-1-3', 'text' => 'Laporan Bidang '.$tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
                ['id' => 'lp-1-4', 'text' => 'Laporan TPK '.$tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
            ]],//perencanaan
            ['id' => 'lp-2', 'text' => 'Laporan Penganggaran'.$tahun, 'children' => [
                ['id' => 'lp-2-1', 'text' => 'Laporan Pendapatan '.$tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
                ['id' => 'lp-2-2', 'text' => 'Laporan Belanja '.$tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
                ['id' => 'lp-2-3', 'text' => 'Laporan Penerimaan '.$tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
                ['id' => 'lp-2-4', 'text' => 'Laporan Pengeluaran '.$tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
            ]],//penganggaran
            ['id' => 'lp-3', 'text' => 'Laporan Penata Bukuan', 'children' => [
                ['id' => 'lp-3-1', 'text' => 'Laporan SPJ '. $tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
                ['id' => 'lp-3-2', 'text' => 'Laporan SPP '. $tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
                ['id' => 'lp-3-3', 'text' => 'Laporan TBP '. $tahun.' <i class="fa fa-file-pdf-o"></i></i>'],
            ]],//penata-bukuan
            ['id' => 'lp-4', 'text' => 'Laporan Pembukuan', 'children' => [
                ['id' => 'lp-4-1', 'text' => 'tragis'],
                ['id' => 'lp-4-2', 'text' => 'terkuagis'],
            ]],//pembukuan
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;
        // return $mod->all();
        return $foo;
    }*/

    public function actionCreateObject($kd_desa, $tahun, $kd)
    {
        $model = TaKegiatan::findOne(['Kd_Desa' => $kd_desa, 'Tahun' => $tahun, 'Kd_Keg' => $kd]);
        if($model != null){
            if($model->insertRab()){
                echo "Berhasil membuat objek";
            } else {
                echo "Gagal membuat objek";
            }
        } else {
            echo "Tidak ditemukan";
        }
    }

    public function actionBidangUtama($kd_desa, $tahun)
    {
        $url = '/transaksi/ta-bidang/desa-view-auto?id='.$kd_desa.'&tahun='.$tahun.'&automate=true';

        $foomix = [];
        //old
        /*$foomix[] = ['id' => 'sub'.$v->kode, 'text' => '<a href="javascript:void(0)" onclick="goLoad({elm:\'#bidang-index-dash\', url:\''.$url.'&kd=0'.substr($v->kode,2,1).'\'});goHiatus({elm:\'#ta-belanja-main-form\', url:\'/transaksi/ta-kegiatan/desa-isi-kegiatan?id='.$kd_desa.'&tahun='.$tahun.'&kd_bid='.$kd_desa.'0'.substr($v->kode,2,1).'&kd_prog='.$v->kode.'\', state:\'show\'});">'.$v->uraian.'</a>', 'parent' => 'bidang-'.substr($v->kode,0,3)];
        }*/
        foreach (RefRekeningKegiatan::find()->where(['tipe' => 2])->all() as $v) {
            $foomix[] = ['id' => 'sub'.$v->kode, 'text' => '<a href="javascript:void(0)" onclick="goHiatus({elm:\'#ta-belanja-main-form\', url:\'/transaksi/ta-kegiatan/desa-isi-kegiatan?id='.$kd_desa.'&tahun='.$tahun.'&kd_bid='.$kd_desa.'0'.substr($v->kode,2,1).'&kd_prog='.$v->kode.'\', state:\'show\'});">'.$v->uraian.'</a>', 'parent' => 'bidang-'.substr($v->kode,0,3)];
        }

      /*  ['id' => 'bidang-2.1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({elm:\'#ta-belanja-main-form\', url:\''.$url.'&kd=01\'});">Bidang Penyelenggaraan Pemerintah Desa</a>'],
            ['id' => 'bidang-2.2', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({elm:\'#ta-belanja-main-form\', url:\''.$url.'&kd=02\'}); ">Bidang Pelaksanaan Pembangunan Desa</a>'],
            ['id' => 'bidang-2.3', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({elm:\'#ta-belanja-main-form\', url:\''.$url.'&kd=03\'}); ">Bidang Pembinaan Kemasyarakatan Desa</a>'],
            ['id' => 'bidang-2.4', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({elm:\'#ta-belanja-main-form\', url:\''.$url.'&kd=04\'}); ">Bidang Pemberdayaan Masyarakat Desa</a>'],
            ['id' => 'bidang-2.5', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({elm:\'#ta-belanja-main-form\', url:\''.$url.'&kd=05\'}); ">Bidang Tidak Terduga</a>'],*/

        $foo = [
            ['id' => 'bidang-2.1', 'parent' => '#', 'text' => 'Bidang Penyelenggaraan Pemerintah Desa'],
            ['id' => 'bidang-2.2', 'parent' => '#', 'text' => 'Bidang Pelaksanaan Pembangunan Desa'],
            ['id' => 'bidang-2.3', 'parent' => '#', 'text' => 'Bidang Pembinaan Kemasyarakatan Desa'],
            ['id' => 'bidang-2.4', 'parent' => '#', 'text' => 'Bidang Pemberdayaan Masyarakat Desa'],
            ['id' => 'bidang-2.5', 'parent' => '#', 'text' => 'Bidang Tidak Terduga'],

        ];
        $foor = array_merge($foo, $foomix);
        Yii::$app->response->format = Response::FORMAT_JSON;
        // return $mod->all();
        return $foor;
    }

    public function actionMenuUtama($referensi, $type, $kd_desa, $tahun)
    {
        //menu-utama
        switch ($referensi) {
            case 'belanja':
                $mod = \sp3d\models\referensi\RefRekeningBelanja::find();
            break;
            case 'kegiatan'://untuk belanja
                $mod = \sp3d\models\referensi\RefRekeningKegiatan::find()->where(['tipe' => 1])->orderBy('kode ASC');

                $foo = [];
                $a = $b = $c = $d = 0;
                foreach ($mod->all() as $v) {
                    //level 1
                    $b = $c = $d = 0;
                    $foo[$a] = [
                        'id' => 'modul-kegiatan-1-'.$v->kode,
                        'text' => $v->kode.' | '.$v->uraian
                    ];
                    $mod2 = \sp3d\models\referensi\RefRekeningKegiatan::find()->where(['tipe' => 2])->orderBy('kode ASC');
                    $mod2->andWhere('kode LIKE :query')->addParams([':query'=> $v->kode.'%']);

                    foreach ($mod2->all() as $v2) {
                        //level 2
                        $foo[$a]['children'][$b] = [
                            'id' => 'modul-kegiatan-2-'.$v2->kode,
                            'text' => $v2->kode.' | '.$v2->uraian
                        ];
                        $mod3 = \sp3d\models\referensi\RefRekeningKegiatan::find()->where(['tipe' => 3])->orderBy('kode ASC');
                        $mod3->andWhere('kode LIKE :query')->addParams([':query'=> $v2->kode.'%']);
                        $c = $d = 0;
                        foreach ($mod3->all() as $v3) {
                            //level 3
                            $foo[$a]['children'][$b]['children'][$c] = [
                                'id' => 'modul-kegiatan-3-'.$v3->kode,
                                'text' => $v3->kode.' | '.$v3->uraian
                            ];
                            $mod4 = \sp3d\models\referensi\RefRekeningKegiatan::find()->where(['tipe' => 4])->orderBy('kode ASC');
                            $mod4->andWhere('kode LIKE :query')->addParams([':query'=> $v3->kode.'%']);
                            if($mod4->count() > 0){
                                $d = 0;
                                foreach ($mod4->all() as $v4) {
                                    //level 4
                                    $foo[$a]['children'][$b]['children'][$c]['children'][$d] = [
                                        'id' => 'modul-kegiatan-4-'.$v4->kode,
                                        'text' => '<a href="javascript:void(0)" onclick="goLoad({elm:\'#ta-belanja-main-form\', url:\'/transaksi/ta-rab/desa-create?id='.$kd_desa.'&tahun='.$tahun.'&automate=true&kode='.$v4->kode.'\'})">'.$v4->kode.' | '.$v4->uraian.'</a>'
                                    ];
                                    $d++;
                                }
                            } else {
                                $foo[$a]['children'][$b]['children'][$c]['text'] = '<a href="javascript:void(0)" onclick="goLoad({elm:\'#ta-belanja-main-form\', url:\'/transaksi/ta-rab/desa-create?id='.$kd_desa.'&tahun='.$tahun.'&automate=true&kode='.$v3->kode.'\'})">'.$v3->kode.' | '.$v3->uraian.'</a>';
                            }
                            
                            $c++;
                        }
                        $b++;
                    }
                    $a++;
                }
            break;
            case 'pembiayaan':
                $mod = \sp3d\models\referensi\RefRekeningPembiayaan::find()->where(['tipe' => 1])->orderBy('kode ASC');
                $mod->andWhere('kode LIKE :query')->addParams([':query'=> '3.'.$type.'%']);//type digunakan untuk mengetahui tipe pembiayaan, 1 untuk penerimaan dan 2 untuk pengeluaran contoh : 3.1 untuk penerimaan
                $parameter = ($type == 1)?'penerimaan':'pengeluaran';
                $foo = [];
                $a = $b = $c = $d = 0;
                foreach ($mod->all() as $v) {
                    //level 1
                    $b = $c = $d = 0;
                    $foo[$a] = [
                        'id' => 'modul-'.$parameter.'-1-'.$v->kode,
                        'text' => $v->kode.' | '.$v->uraian
                    ];
                    $mod2 = \sp3d\models\referensi\RefRekeningPembiayaan::find()->where(['tipe' => 2])->orderBy('kode ASC');
                    $mod2->andWhere('kode LIKE :query')->addParams([':query'=> $v->kode.'%']);

                    foreach ($mod2->all() as $v2) {
                        //level 2
                        $foo[$a]['children'][$b] = [
                            'id' => 'modul-'.$parameter.'-2-'.$v2->kode,
                            'text' => $v2->kode.' | '.$v2->uraian
                        ];
                        $mod3 = \sp3d\models\referensi\RefRekeningPembiayaan::find()->where(['tipe' => 3])->orderBy('kode ASC');
                        $mod3->andWhere('kode LIKE :query')->addParams([':query'=> $v2->kode.'%']);
                        $c = $d = 0;
                        foreach ($mod3->all() as $v3) {
                            //level 3
                            $foo[$a]['children'][$b]['children'][$c] = [
                                'id' => 'modul-'.$parameter.'-3-'.$v3->kode,
                                'text' => $v3->kode.' | '.$v3->uraian
                            ];
                            $mod4 = \sp3d\models\referensi\RefRekeningPembiayaan::find()->where(['tipe' => 4])->orderBy('kode ASC');
                            $mod4->andWhere('kode LIKE :query')->addParams([':query'=> $v3->kode.'%']);
                            if($mod4->count() > 0){
                                $d = 0;
                                foreach ($mod4->all() as $v4) {
                                    //level 4
                                    $foo[$a]['children'][$b]['children'][$c]['children'][$d] = [
                                        'id' => 'modul-'.$parameter.'-4-'.$v4->kode,
                                        'text' => '<a href="javascript:void(0)" onclick="goHiatus({elm:\'#ta-'.$parameter.'-main-form\', url:\'/transaksi/ta-rab/'.$parameter.'-desa-create?id='.$kd_desa.'&tahun='.$tahun.'&automate=true&kode='.$v4->kode.'\', state:\'show\'});">'.$v4->kode.' | '.$v4->uraian.'</a>'
                                    ];
                                    $d++;
                                }
                            } else {
                                $foo[$a]['children'][$b]['children'][$c]['text'] = '<a href="javascript:void(0)" onclick="goHiatus({elm:\'#ta-'.$parameter.'-main-form\', url:\'/transaksi/ta-rab/'.$parameter.'-desa-create?id='.$kd_desa.'&tahun='.$tahun.'&automate=true&kode='.$v3->kode.'\', state:\'show\'});">'.$v3->kode.' | '.$v3->uraian.'</a>';
                            }
                            
                            $c++;
                        }
                        $b++;
                    }
                    $a++;
                }               
            break;
            case 'pendapatan':
                $foo = [
                    ['id' => 'pendapatan-1.1', 'parent' => '#', 'text' => '1.1 | Pendapatan Asli Desa'],
                    ['id' => 'pendapatan-1.2', 'parent' => '#', 'text' => '1.2 | Pendapatan Transfer'],
                    ['id' => 'pendapatan-1.3', 'parent' => '#', 'text' => '1.3 | Pendapatan Lain-Lain'],
                ];
                $foomix = [];

                $mod = \sp3d\models\referensi\RefRekeningPendapatan::find()->where('tipe > :tipevr', ['tipevr' => 1])->orderBy('kode ASC');
                foreach ($mod->all() as $v) {
                    $foomix[] = [
                        'id' => 'pendapatan-'.$v->kode, 
                        'parent' => 'pendapatan-'.(($v->tipe > 2)?substr($v->kode,0, -3):substr($v->kode, 0,-2)), 
                        'text' => (($v->tipe > 2 && $v->hchild == 'N')?'<a href="javascript:void(0)" onclick="goHiatus({elm:\'#ta-pendapatan-main-form\', url:\'/transaksi/ta-rab/pendapatan-desa-create?id='.$kd_desa.'&tahun='.$tahun.'&automate=true&kode='.$v->kode.'\', state:\'show\'});">':'').$v->kode.' | '.$v->uraian.(($v->tipe > 2)?'</a>':'')
                        ];
                }

                $foo = array_merge($foo, $foomix);
            break;
            default:

            break;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        // return $mod->all();
        return $foo;
    }

    public function actionDashboardHome($type='desa', $kode=null)
    {
        return $this->renderAjax('dashboard-home-'.$type, [
            'kode' => $kode,
        ]);
    }

    public function actionLaporan($kd_desa)
    {
        return $this->renderAjax('laporan', [
            'id' => $kd_desa,
        ]);
    }

    public function actionImportFile()
    {
        return $this->renderAjax('import-file');
    }

    public function actionDataCount()
    {
        $data_desa = TaDesa::find()->count();
        $desa_c = KabupatenCount::findOne(['kd_kabupaten' => '35.23']);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data' => [
            'desa' => $data_desa, 
            'akumulasi' => 'Rp. '.number_format(round($desa_c->dana_rab), 2, ',','.')
        ]];
    }

    public function actionDesa()
    {
        $data1 = RefDesa::find()->count();
        $data2 = TaDesa::find()->count();
        //Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('chart-desa', ['all' => $data1, 'desa' => $data2]);
        /*return ['data' => [
            'element' => $this->renderPartial('chart-desa', ['all' => $data1, 'desa' => $data2]), 
            'waktu' => date('Y-m-d H:i:s'), 'nilai' => ['desa' => $data1, 'desa_selesai' => $data2]
            ]
        ];*/
    }

    public function actionPenyerapan()
    {
        // /Yii::$app->response->format = Response::FORMAT_JSON;

        $penyerapan  = new Penyerapan();
        return $this->renderAjax('chart-penyerapan', [
            'model' => $penyerapan
        ]);
        //var_dump($datar);
        /*return ['data' => [
            'element' => 
            ]
        ];*/
    }

    public function actionAkumulasi()
    {
        // $datar = TaRAB::find()->select(['Ref_Desa.Nama_Desa AS Nama_Desa', 'SUM(Ta_RAB.Anggaran) AS Anggaran'])->joinWith('desa')
        // ->groupBy(['Ref_Desa.Kd_Desa'])->all();

        //$datar = TaRAB::find()->select(['Nama_Desa', ])->where(['id_mapel' => 2])->groupBy('id_peserta')->all();
        //$datar = Yii::$app->db->createCommand('SELECT Ref_Desa.Nama_Desa AS Nama_Desa, SUM(Ta_RAB.Anggaran) AS Anggaran FROM Ta_RAB JOIN Ref_Desa ON Ref_Desa.Kd_Desa = Ta_RAB.Kd_Desa GROUP BY Ta_RAB.Kd_Desa')->queryAll();
        $data = KabupatenCount::find()->where(['kd_kabupaten' => '35.23'])->one();
        return $this->renderAjax('chart-akumulasi', ['data' => $data]);
        // Yii::$app->response->format = Response::FORMAT_JSON;
        //var_dump($datar);
        // return ['data' => [
        //     'element' => $this->renderPartial('chart-akumulasi', ['data' => $data])
        //     ]
        // ];
    }

    public function actionPdf($bagian, $kd_desa, $tahun=2017, $kode=null)
    {
        //PDF
        try {
            ob_start();
            echo $this->renderPartial('pdf-page-'.$bagian, [
                'kd_desa' => $kd_desa,
                'kode' => $kode,
                'tahun' => $tahun
            ]);
            
            $content = ob_get_clean();
            $html2pdf = new Html2Pdf('P', 'A4', 'fr');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content);
            $html2pdf->output('exemple00.pdf');
        } catch (Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }
    //ambil-transaksi-kegiatan
    public function actionAmbilTransaksiKegiatan($bid, $parsing=0, $form=0)
    {
        if($parsing == 1){
            $exbid = explode('.', $bid);
            $bid = $exbid[2];
        }

        $data = TaKegiatan::find()->where(['Kd_Bid' => $bid]);
        if($form == 1){
            if($data->count() > 0){
                foreach ($data->all() as $v) {
                    echo "<option value='".$v->Kd_Keg."'>".$v->Kd_Keg.' | '.$v->Nama_Kegiatan."</option>";
                }
            } else {
                echo "<option value=''>---</option>";
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['result' => [
                'kegiatan' => $data->all()
            ]];
        }   
    }
    //transaksi-kegiatan
    public function actionTransaksiKegiatan($kid, $kd_desa, $check=0, $form=0)
    {
        $data = TaKegiatan::findOne(['ID_Keg' => $kid, 'Kd_Desa' => $kd_desa]);
        if($data != null){
            echo "<span class='label label-danger'>Data sudah ada / dipakai</span>";
        }
    }
    //referensi-kegiatan
    public function actionReferensiKegiatan($bid, $parsing=0, $form=0, $prog=null)
    {
        if($parsing == 1){
            $exbid = explode('.', $bid);
            $bid = '2.'.substr($exbid[2], 1);//menurut buku kuning maka akan menjadi 2.2
        }

        if($prog != null){
            $bid = $prog;
        }

        $data = RefRekeningKegiatan::find()->where(['tipe' => 3]);
        $data->andWhere('kode LIKE :query')->addParams([':query'=> $bid.'%']);

        if($form == 1){
            if($data->count() > 0){
                foreach ($data->all() as $v) {
                    echo "<option value='".$v->kode."'>".$v->uraian."</option>";
                }
            } else {
                echo "<option value=''>---</option>";
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['result' => [
                'kegiatan' => $data->all()
            ]];
        }        
    }
    //referensi
    public function actionReferensi($jenis='kegiatan', $kd_kab = '35.23.', $type='1', $kode='', $form=0)// kid = Kecamatan ID
    {

        $type = (int) $type;
        $type++;
        switch ($jenis) {
            case 'belanja':
                $mod = \sp3d\models\referensi\RefRekeningBelanja::find()->where(['id_kabupaten' => $kd_kab]);
                $mod->andWhere('tipe > 1');
                $mod->andWhere('kode LIKE :query')->addParams([':query'=> $kode.'%']);
                $mod->orderBy('kode ASC');
            break;
            case 'kegiatan':
                $mod = \sp3d\models\referensi\RefRekeningKegiatan::find()->where(['id_kabupaten' => $kd_kab, 'tipe' => $type]);
                $mod->andWhere('kode LIKE :query')->addParams([':query'=> $kode.'%']);
            break;
            case 'pembiayaan':
                $mod = \sp3d\models\referensi\RefRekeningPembiayaan::find()->where(['id_kabupaten' => $kd_kab, 'tipe' => $type]);
                $mod->andWhere('kode LIKE :query')->addParams([':query'=> $kode.'%']);
            break;
            case 'pendapatan':
                $mod = \sp3d\models\referensi\RefRekeningPendapatan::find()->where(['id_kabupaten' => $kd_kab, 'tipe' => $type]);
                $mod->andWhere('kode LIKE :query')->addParams([':query'=> $kode.'%']);
            break;
        }

        if($form == 1){
            if($mod->count() > 0){
                //menggunakan variables variable
                echo "<option value=''>--Pilih--</option>";
                foreach ($mod->all() as $v) {
                    echo "<option value='".$v->kode."'>".$v->kode.' | '.$v->uraian."</option>";
                }
            } else {
                echo "<option value=''>---</option>";
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['result' => [
                'data' => $mod->all()
            ]];
        }
    }

    public function actionReferensiRpjm($type='visi', $id='01', $form=0)// kid = Kecamatan ID
    {
        $varIO = [];
        switch ($type) {
            case 'misi':
                $mod = transaksi\TaRPJMMisi::find()->where(['ID_Visi' => $id]);
                $varIO['key'] = 'ID_Misi';
                $varIO['val'] = 'Uraian_Misi';
            break;
            case 'tujuan':
                $mod = transaksi\TaRPJMTujuan::find()->where(['ID_Misi' => $id]);
                $varIO['key'] = 'ID_Tujuan';
                $varIO['val'] = 'Uraian_Tujuan';
            break;
            case 'sasaran':
                $mod = transaksi\TaRPJMSasaran::find()->where(['ID_Tujuan' => $id]);
                $varIO['key'] = 'ID_Sasaran';
                $varIO['val'] = 'Uraian_Sasaran';
            break;
            default://visi
                $mod = transaksi\TaRPJMVisi::find()->where(['ID_Visi' => $id]);
                $varIO['key'] = 'ID_Visi';
                $varIO['val'] = 'Uraian_Visi';
            break;
        }

        if($form == 1){
            if($mod->count() > 0){
                //menggunakan variables variable
                foreach ($mod->all() as $v) {
                    echo "<option value='".$v->{$varIO['key']}."'>".$v->{$varIO['val']}."</option>";
                }
            } else {
                echo "<option value=''>---</option>";
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['result' => [
                'data' => $mod->all()
            ]];
        }
    }

    public function actionReferensiDesa($kid)// kid = Kecamatan ID
    {
        $data = RefDesa::find()->where(['Kd_Kec' => $kid])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => [
            'desa' => $data
        ]];
    }

    public function actionReferensiDesa2($kid, $form=0)// kid = Kecamatan ID
    {
        $mod = RefDesa::find()->where(['Kd_Kec' => $kid]);

        if($form == 1){
            if($mod->count() > 0){
                //menggunakan variables variable
                echo "<option value=''>--Pilih--</option>";
                foreach ($mod->all() as $v) {
                    echo "<option value='".$v->Kd_Desa."'>".$v->Nama_Desa."</option>";
                }
            } else {
                echo "<option value=''>---</option>";
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['result' => [
                'data' => $mod->all()
            ]];
        }
    }

    public function actionDataCountingDisplay($tahun, $kd_kecamatan, $kd_desa=null, $datauntuk='desa', $kode=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        switch ($datauntuk) {
            case 'desa':
                $mod = DesaCount::findOne(['tahun' => $tahun, 'kd_kecamatan' => $kd_kecamatan , 'kd_desa' => $kd_desa]);
            break;
            case 'kecamatan':
                $mod = KecamatanCount::findOne(['tahun' => $tahun, 'kd_kecamatan' => $kd_kecamatan]);
            break;
            case 'kabupaten':
                $mod = KabupatenCount::findOne(['tahun' => $tahun, 'kd_kabupaten' => $kode]);
            break;
            case 'provinsi':
                $mod = ProvinsiCount::findOne(['tahun' => $tahun, 'kd_provinsi' => $kd_kecamatan]);
            break;
        }
        $model2 = transaksi\TaDesa::findOne(['tahun' => $tahun, 'kd_desa' => $kd_desa, 'kd_kecamatan' => $kd_kecamatan]);
        return ['data' => [
            'dana_anggaran' => 'Rp. '.$mod->nf($mod->dana_anggaran),
            'dana_pencairan' => 'Rp. '.$mod->nf($mod->dana_pencairan),
            'dana_kegiatan' => 'Rp. '.$mod->nf($mod->dana_kegiatan),
            'dana_mutasi' => 'Rp. '.$mod->nf($mod->dana_mutasi),
            'dana_pajak' => 'Rp. '.$mod->nf($mod->dana_pajak),
            
            'dana_pendapatan' => ($mod->dana_pendapatan),
            'dana_penerimaan' => ($mod->dana_penerimaan),
            'dana_pengeluaran' => ($mod->dana_pengeluaran),
            'dana_rab' => ($mod->dana_rab),
            
            'dana_spp' => 'Rp. '.$mod->nf($mod->dana_spp),
            'dana_spj' => 'Rp. '.$mod->nf($mod->dana_spj),
            'dana_sts' => 'Rp. '.$mod->nf($mod->dana_sts),
            'dana_tbp' => 'Rp. '.$mod->nf($mod->dana_tbp),
            
            'tabel_desa' => count($model2).' Data Desa',
            'tabel_anggaran' => $mod->nfo($mod->data_anggaran).' Data Anggaran',
            'tabel_pencairan' => $mod->nfo($mod->data_pencairan).' Data Pencairan',
            'tabel_bidang_kegiatan' => 'Bidang '.$mod->nfo($mod->data_bidang).' / Kegiatan '.$mod->nfo($mod->data_kegiatan),
            'tabel_bidang' => $mod->nfo($mod->data_bidang).' Bidang',
            'tabel_kegiatan' => $mod->nfo($mod->data_kegiatan).' Data Kegiatan',
            'tabel_perangkat_desa' => $mod->nfo(3).' Perangkat',
            'tabel_jurnal_umum' => $mod->nfo(0).' Jurnal',
            'tabel_triwulan' => $mod->nfo(3).' Data',
            'tabel_mutasi' => $mod->nfo($mod->data_mutasi).' Data Mutasi',
            'tabel_pajak' => $mod->nfo($mod->data_pajak).' Data Pajak',
            'tabel_tbp' => $mod->nfo($mod->data_tbp).' Data TBP',
            'tabel_pendapatan' => $mod->nfo($mod->data_pendapatan).' Data RAP',
            'tabel_rab' => $mod->nfo($mod->data_rab).' Data RAB',
            'tabel_penerimaan' => $mod->nfo($mod->data_penerimaan).' Data Penerimaan',            
            'tabel_pengeluaran' => $mod->nfo($mod->data_pengeluaran).' Data Pengeluaran',
            'tabel_rpjm' => $mod->nfo(3).' Data',
            'tabel_spj' => $mod->nfo($mod->data_spj).' Data SPJ',
            'tabel_spp' => $mod->nfo($mod->data_spp).' Data SPP',
            'tabel_sts' => $mod->nfo($mod->data_sts).' Data STS'
        ]];
    }
}
