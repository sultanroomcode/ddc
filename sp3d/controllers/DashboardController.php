<?php
namespace sp3d\controllers;

use Yii;

use sp3d\models\AESServer;
use sp3d\models\Log;
use sp3d\models\Credential;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use hscstudio\mimin\components\Mimin;

class DashboardController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => [''],
                'rules' => [
                    [
                        'actions' => ['index', 'i', 'menu', 'menu-utama'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init(){
        parent::init();
        Yii::$app->view->params['metatag'] =  [
            'sitename' => 'DDC Server'
        ];
        $this->layout = 'admin';
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionExecuteLog($id)
    {
        $dekrip = new AESServer($text, 'enkripsi', 128);
        $text2 = $dekrip->decrypt();
        return $text2;
    }

    public function actionIndex()
    {
        $data = Log::find()->all();
        
        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionI()
    {
        $this->layout = 'superadmin';
        $data = Log::find()->all();
        
        return $this->render('super-index', [
            'data' => $data,
        ]);
    }

    public function actionMenu()
    {
        return $this->renderAjax('_'.Yii::$app->user->identity->viewRole().'/su-aside-extends');
    }

    public function actionProvinsi()
    {
        $data = Log::find()->all();
        $cred = Credential::findOne(['id' => 1]);
        
        //var_dump($aes->decrypt());
        //$res = Yii::$app->db->createCommand($aes->decrypt())->queryAll();
        
        return $this->render('provinsi', [
            'data' => $data,
        ]);
    }

    public function actionMenuUtama()
    {
        $url = '/';

        $foomix = $fooadd = [];
        if(Yii::$app->user->identity->type == 'provinsi'){
            $fooadd = [
            	//KELEMBAGAAN DESA
            	/*['id' => 'm0', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Labs</a>'],
                ['id' => 'm0.1', 'parent' => 'm0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/server-event-lab/responder\'})">Labs Server Event</a>'],*/

                ['id' => 'm1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Kelembagaan Desa</a>'],
	            ['id' => 'm1.1', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Pemerintahan Desa</a>'],
                
                ['id' => 'm1.1.1', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data Kepala Desa</a>'],
                //KEPALA
                ['id' => 'm1.1.1.1', 'parent' => 'm1.1.1', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.1.1.1', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=lama-jabatan\'})">Berdasarkan Lama Jabatan</a>'],
                ['id' => 'm1.1.1.1.2', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.1.1.3', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.1.1.4', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.1.1.5', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                ['id' => 'm1.1.1.2', 'parent' => 'm1.1.1', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.1.2.1', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=lama-jabatan\'})">Berdasarkan Lama Jabatan</a>'],
                ['id' => 'm1.1.1.2.2', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.1.2.3', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.1.2.4', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.1.2.5', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //PERANGKAT-DESA
                ['id' => 'm1.1.2', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data Perangkat Desa</a>'],
                ['id' => 'm1.1.2.1', 'parent' => 'm1.1.2', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.2.1.1', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.2.1.2', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.2.1.3', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.2.1.4', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                ['id' => 'm1.1.2.2', 'parent' => 'm1.1.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.2.2.1', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.2.2.2', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.2.2.3', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.2.2.4', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //PERANGKAT-BPD
                ['id' => 'm1.1.3', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data BPD</a>'],
                ['id' => 'm1.1.3.1', 'parent' => 'm1.1.3', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.3.1.1', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.3.1.2', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.3.1.3', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.3.1.4', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],

                ['id' => 'm1.1.3.2', 'parent' => 'm1.1.3', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.3.2.1', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.3.2.2', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.3.2.3', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.3.2.4', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //TKD
                ['id' => 'm1.2', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)">Tanah Kas Desa</a>'],
                ['id' => 'm1.2.0', 'parent' => 'm1.2', 'text' => '<a href="javascript:void(0)">Data Tanah Kas Desa</a>'],
                ['id' => 'm1.2.0.1', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=luas-lahan\'})">Berdasarkan Luas Lahan</a>'],
                ['id' => 'm1.2.0.2', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=jenis-lahan\'})">Berdasarkan Jenis Lahan</a>'],
                ['id' => 'm1.2.0.3', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=jenis-sertifikat\'})">Berdasarkan Jenis Sertifikat</a>'],
                ['id' => 'm1.2.0.4', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=lokasi\'})">Berdasarkan Lokasi</a>'],
                ['id' => 'm1.2.1', 'parent' => 'm1.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.2.1.1', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=luas-lahan\'})">Berdasarkan Luas Lahan</a>'],
                ['id' => 'm1.2.1.2', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=jenis-lahan\'})">Berdasarkan Jenis Lahan</a>'],
                ['id' => 'm1.2.1.3', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=jenis-sertifikat\'})">Berdasarkan Jenis Sertifikat</a>'],
                ['id' => 'm1.2.1.4', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=lokasi\'})">Berdasarkan Lokasi</a>'],
                //DATA TAMBAHAN
                ['id' => 'm1.3', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)">Data Desa</a>'],
                ['id' => 'm1.3.1', 'parent' => 'm1.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/data-tambahan/div-laporan?subjek=umum\'})">RT/RW</a>'],
                ['id' => 'm1.3.2', 'parent' => 'm1.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/data-tambahan/div-chart?subjek=umum\'})">Rekapitulasi</a>'],

                //APBDESA
                ['id' => 'm2', 'parent' => '#', 'text' => 'Data APBDesa'],            
	            ['id' => 'm2.5', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm2.5.1', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/apbdesa\'})">Data APBDesa</a>'],
                ['id' => 'm2.5.2', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/grafik\'})">Grafik</a>'],
                ['id' => 'm2.5.3', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/import/provinsi-mdb-list\'})">Daftar Upload Database</a>'],
                ['id' => 'm2.5.4', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/apbdesa-sorting\'})">Rekapitulasi APBDesa</a>'],

                //LEMBAGA KEMASYARAKATAN DESA
                ['id' => 'm3', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Lembaga Kemasyarakatan Desa</a>'],

                ['id' => 'm3.1', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data PKK</a>'],
                ['id' => 'm3.1.1', 'parent' => 'm3.1', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.1.1.1', 'parent' => 'm3.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=pkk&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.1.1.2', 'parent' => 'm3.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=pkk&subjek=kepengurusan\'})">Berdasarkan Kepengurusan</a>'],
                ['id' => 'm3.1.1.3', 'parent' => 'm3.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=pkk&subjek=sumber-dana\'})">Berdasarkan Sumber Dana</a>'],
                ['id' => 'm3.1.1.4', 'parent' => 'm3.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=pkk&subjek=kegiatan\'})">Berdasarkan Kegiatan Yang dilakukan</a>'],

                ['id' => 'm3.1.2', 'parent' => 'm3.1', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.1.2.1', 'parent' => 'm3.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=pkk&subjek=pembina\'})">Berdasarkan Status Pembinaan</a>'],
                ['id' => 'm3.1.2.2', 'parent' => 'm3.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=pkk&subjek=sumber-dana\'})">Berdasarkan Sumber Dana</a>'],
                ['id' => 'm3.1.2.3', 'parent' => 'm3.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=pkk&subjek=kegiatan\'})">Berdasarkan Kegiatan Yang Dilakukan</a>'],

                ['id' => 'm3.2', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data LPMD</a>'],
                ['id' => 'm3.2.1', 'parent' => 'm3.2', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.2.1.1', 'parent' => 'm3.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=lpmd&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.2.2', 'parent' => 'm3.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.2.2.1', 'parent' => 'm3.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=lpmd&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                ['id' => 'm3.3', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data Karang Taruna</a>'],
                ['id' => 'm3.3.1', 'parent' => 'm3.3', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.3.1.1', 'parent' => 'm3.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=kartar&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.3.2', 'parent' => 'm3.3', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.3.2.1', 'parent' => 'm3.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=kartar&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                // ['id' => 'm3.4', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Lembaga Kemasyarakatan Lainnya</a>'],

                ['id' => 'm3.5', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data Posyandu</a>'],
                ['id' => 'm3.5.1', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Data</a>'],
                /*['id' => 'm3.5.1.1', 'parent' => 'm3.5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.1.2', 'parent' => 'm3.5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.2', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Grafik</a>'],
                ['id' => 'm3.5.2.1', 'parent' => 'm3.5.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.2.2', 'parent' => 'm3.5.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.3', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=posyandu&provinsi=true\'})">Daftar Operator</a>'],*/

                ['id' => 'm3.6', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data KPM</a>'],
                ['id' => 'm3.6.1', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.6.1.0', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kpm/non-operator-kpm\'})">Lihat Data</a>'],
                ['id' => 'm3.6.1.1', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm3.6.1.2', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm3.6.1.3', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm3.6.1.4', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=status\'})">Berdasarkan Status Aktif</a>'],
                ['id' => 'm3.6.2', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.6.2.1', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm3.6.2.2', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm3.6.2.3', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm3.6.2.4', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=status\'})">Berdasarkan Status Aktif</a>'],
                ['id' => 'm3.6.3', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=kpm&provinsi=true\'})">Daftar Operator</a>'],

                ['id' => 'm3.7', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/lembagaadat/default\'})">Data Lembaga Adat Desa</a>'],
                /*['id' => 'm3.7.1', 'parent' => 'm3.7', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.7.1.1', 'parent' => 'm3.7.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=lembagaadat&subjek=sekretariat\'})">Berdasarkan Status Sekretariat</a>'],
                ['id' => 'm3.7.2', 'parent' => 'm3.7', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.7.2.1', 'parent' => 'm3.7.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=lembagaadat&subjek=sekretariat\'})">Berdasarkan Status Sekretariat</a>'],*/
                //PASAR DESA
                ['id' => 'm32', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Pasar Desa</a>'],  
                ['id' => 'm32.1', 'parent' => 'm32', 'text' => '<a href="javascript:void(0)">Laporan</a>'],  
                ['id' => 'm32.1.1', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm32.1.2', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm32.1.3', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-pedagang\'})">Jumlah Pedagang</a>'],  
                ['id' => 'm32.1.4', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-kios\'})">Jumlah Kios</a>'],  
                ['id' => 'm32.1.5', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-los\'})">Jumlah Los</a>'],  
                ['id' => 'm32.1.6', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-lapak\'})">Jumlah Lapak</a>'],  
                ['id' => 'm32.1.7', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-lesehan\'})">Jumlah Lesehan</a>'],  
                ['id' => 'm32.1.8', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-ruko\'})">Jumlah Ruko</a>'],  
                
                ['id' => 'm32.2', 'parent' => 'm32', 'text' => '<a href="javascript:void(0)">Grafik</a>'],  
                ['id' => 'm32.2.1', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm32.2.2', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm32.2.3', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-pedagang\'})">Jumlah Pedagang</a>'],  
                ['id' => 'm32.2.4', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-kios\'})">Jumlah Kios</a>'],  
                ['id' => 'm32.2.5', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-los\'})">Jumlah Los</a>'],  
                ['id' => 'm32.2.6', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-lapak\'})">Jumlah Lapak</a>'],  
                ['id' => 'm32.2.7', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-lesehan\'})">Jumlah Lesehan</a>'],  
                ['id' => 'm32.2.8', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-ruko\'})">Jumlah Ruko</a>'],  
                

                ['id' => 'm33', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/puem-editor/default\'})">Garasi PUEM</a>'], 


                //BUMDES
                ['id' => 'm6', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data BumDesa</a>'],
                ['id' => 'm6.1', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm6.1.1', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=keuntungan\'})">Berdasarkan Keuntungan</a>'],  
                ['id' => 'm6.1.2', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=modal\'})">Berdasarkan Modal</a>'],  
                ['id' => 'm6.1.3', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=omset\'})">Berdasarkan Omset</a>'],  
                ['id' => 'm6.1.4', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=unit-usaha\'})">Berdasarkan Unit Usaha</a>'],  
                ['id' => 'm6.1.5', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm6.1.6', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm6.1.7', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/bumdes/rekap?subjek=rekap\'})">Rekapitulasi Dana</a>'],  
                ['id' => 'm6.1.8', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/bumdes/klasifikasi\'})">Klasifikasi BumDesa</a>'],  

                

                ['id' => 'm6.2', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm6.2.1', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=keuntungan\'})">Berdasarkan Keuntungan</a>'],  
                ['id' => 'm6.2.2', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=modal\'})">Berdasarkan Modal</a>'],  
                ['id' => 'm6.2.3', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=omset\'})">Berdasarkan Omset</a>'],  
                ['id' => 'm6.2.4', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=unit-usaha\'})">Berdasarkan Unit Usaha</a>'],  
                ['id' => 'm6.2.5', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm6.2.6', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm6.2.7', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=klasifikasi\'})">Berdasarkan Klasifikasi BumDesa</a>'],  
                //KLINIK BUMDES
                ['id' => 'm7', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Klinik Bumdes</a>'],
                // ['id' => 'm7.1', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/default\'})">Administrasi Klinik BumDesa</a>'],
                ['id' => 'm7.2', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/default/new-index\'})">Administrasi Klinik BumDesa</a>'],
                // PODES
                ['id' => 'm71', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Potensi Unggulan Desa</a>'],
                ['id' => 'm71.0', 'parent' => 'm71', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/laporan-podes\'})">Data Podes</a>'],
                ['id' => 'm71.0.1', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?objek=podes-pertanian-pangan\'})">Data Podes Pertanian Pangan</a>'],
                ['id' => 'm71.0.2', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?objek=podes-pertanian-buah\'})">Data Podes Pertanian Buah</a>'],
                ['id' => 'm71.0.3', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?objek=podes-pertanian-apotik\'})">Data Podes Apotik Hidup</a>'],
                ['id' => 'm71.0.4', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?objek=podes-perkebunan\'})">Data Podes Perkebunan</a>'],
                ['id' => 'm71.0.5', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?objek=podes-kehutanan\'})">Data Podes Kehutanan</a>'],
                ['id' => 'm71.0.6', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?objek=podes-peternakan\'})">Data Podes Peternakan</a>'],
                ['id' => 'm71.0.7', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?objek=podes-perikanan\'})">Data Podes Perikanan</a>'],

                ['id' => 'm71.1', 'parent' => 'm71', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/chart-podes\'})">Grafik</a>'],
                ['id' => 'm71.1.1', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-chart?objek=podes-pertanian-pangan\'})">Data Podes Pertanian Pangan</a>'],
                ['id' => 'm71.1.2', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-chart?objek=podes-pertanian-buah\'})">Data Podes Pertanian Buah</a>'],
                ['id' => 'm71.1.3', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-chart?objek=podes-pertanian-apotik\'})">Data Podes Apotik Hidup</a>'],
                ['id' => 'm71.1.4', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-chart?objek=podes-perkebunan\'})">Data Podes Perkebunan</a>'],
                ['id' => 'm71.1.5', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-chart?objek=podes-kehutanan\'})">Data Podes Kehutanan</a>'],
                ['id' => 'm71.1.6', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-chart?objek=podes-peternakan\'})">Data Podes Peternakan</a>'],
                ['id' => 'm71.1.7', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-chart?objek=podes-perikanan\'})">Data Podes Perikanan</a>'],

                //BUMDES MANUAL
                /*['id' => 'm21', 'parent' => '#', 'text' => 'Bumdes'],
                ['id' => 'm21.1', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/default/dashboard\'})">Tambah Bumdes</a>'],  
                ['id' => 'm21.2', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/index-provinsi\'})">Bumdes Mandiri</a>'],  
                ['id' => 'm21.3', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes-bersama/index-provinsi\'})">Bumdes Bersama</a>'],  */
                //SETTING
                ['id' => 'm8', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Setting</a>'],
                ['id' => 'm8.0', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan-tipe\'})">Master Lembaga</a>'],
                ['id' => 'm8.01', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan\'})">Master Jabatan</a>'],
                ['id' => 'm8.1', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)">Sub Master Jabatan</a>'],
                ['id' => 'm8.1.2', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=desa\'})">Perangkat Desa</a>'],
                ['id' => 'm8.1.3', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=bpd\'})">BPD</a>'],
                ['id' => 'm8.1.4', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=lpmd\'})">LPMD</a>'],
                ['id' => 'm8.1.5', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=kartar\'})">Karang Taruna</a>'],
                ['id' => 'm8.1.6', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=pkk\'})">PKK</a>'],
                ['id' => 'm8.1.7', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=pbumdes\'})">BUMDESA</a>'],
                ['id' => 'm8.1.8', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=bkad\'})">BKAD</a>'],
                ['id' => 'm8.1.9', 'parent' => 'm8.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan?tipe=lembaga-adat\'})">Lembaga Adat</a>'],

                ['id' => 'm8.2', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)">Master Jenis Pelatihan</a>'],
                ['id' => 'm8.2.1', 'parent' => 'm8.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jenis-pelatihan?tipe=desa\'})">Kepala Desa & Perangkat</a>'],
                // ['id' => 'm8.2.2', 'parent' => 'm8.2', 'text' => '<a href="javascript:void(0)">Perangkat Desa</a>'],
                ['id' => 'm8.2.3', 'parent' => 'm8.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jenis-pelatihan?tipe=bpd\'})">BPD</a>'],
                ['id' => 'm8.2.4', 'parent' => 'm8.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jenis-pelatihan?tipe=pbumdes\'})">BUMDESA</a>'],
                ['id' => 'm8.2.5', 'parent' => 'm8.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jenis-pelatihan?tipe=kpm\'})">KPM</a>'],
                ['id' => 'm8.2.6', 'parent' => 'm8.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jenis-pelatihan?tipe=pkk\'})">PKK</a>'],
                ['id' => 'm8.2.7', 'parent' => 'm8.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jenis-pelatihan?tipe=kartar\'})">Karang Taruna</a>'],

                ['id' => 'm8.3', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)">Tambah Modul User</a>'],
                ['id' => 'm8.3.1', 'parent' => 'm8.3', 'text' => '<a href="javascript:void(0)">BUMDESA</a>'],
                ['id' => 'm8.3.2', 'parent' => 'm8.3', 'text' => '<a href="javascript:void(0)">LPMD</a>'],
                ['id' => 'm8.3.3', 'parent' => 'm8.3', 'text' => '<a href="javascript:void(0)">PKK</a>'],
                ['id' => 'm8.3.4', 'parent' => 'm8.3', 'text' => '<a href="javascript:void(0)">Karang Taruna</a>'],
                ['id' => 'm8.3.5', 'parent' => 'm8.3', 'text' => '<a href="javascript:void(0)">Posyandu</a>'],
                ['id' => 'm8.3.6', 'parent' => 'm8.3', 'text' => '<a href="javascript:void(0)">KPM</a>'],

                ['id' => 'm8.4', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes-tipe-unit-usaha/index\'})">Master Unit Usaha Bumdes</a>'],

                ['id' => 'm8.5', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)">Master Satuan Podes</a>'],
                ['id' => 'm8.5.1', 'parent' => 'm8.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-satuan/index?modul=pertanian-pangan\'})">Satuan Pertanian - Tanaman Pangan</a>'],
                ['id' => 'm8.5.2', 'parent' => 'm8.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-satuan/index?modul=pertanian-buah\'})">Satuan Pertanian - Tanaman Buah</a>'],
                ['id' => 'm8.5.3', 'parent' => 'm8.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-satuan/index?modul=pertanian-apotik\'})">Satuan Pertanian - Tanaman Apotik Hidup</a>'],
                ['id' => 'm8.5.4', 'parent' => 'm8.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-satuan/index?modul=perkebunan\'})">Satuan Perkebunan</a>'],
                ['id' => 'm8.5.5', 'parent' => 'm8.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-satuan/index?modul=kehutanan\'})">Satuan Kehutanan</a>'],
                ['id' => 'm8.5.6', 'parent' => 'm8.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-satuan/index?modul=peternakan\'})">Satuan Peternakan</a>'],
                ['id' => 'm8.5.7', 'parent' => 'm8.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-satuan/index?modul=perikanan\'})">Satuan Perikanan</a>'],

                ['id' => 'm8.6', 'parent' => 'm8', 'text' => '<a href="javascript:void(0)">Master Komoditas Podes</a>'],
                ['id' => 'm8.6.1', 'parent' => 'm8.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-komoditas-podes/index?modul=pertanian-pangan\'})">Komoditas Pertanian - Tanaman Pangan</a>'],
                ['id' => 'm8.6.2', 'parent' => 'm8.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-komoditas-podes/index?modul=pertanian-buah\'})">Komoditas Pertanian - Tanaman Buah</a>'],
                ['id' => 'm8.6.3', 'parent' => 'm8.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-komoditas-podes/index?modul=pertanian-apotik\'})">Komoditas Pertanian - Tanaman Apotik Hidup</a>'],
                ['id' => 'm8.6.4', 'parent' => 'm8.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-komoditas-podes/index?modul=perkebunan\'})">Komoditas Perkebunan</a>'],
                ['id' => 'm8.6.5', 'parent' => 'm8.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-komoditas-podes/index?modul=kehutanan\'})">Komoditas Kehutanan</a>'],
                ['id' => 'm8.6.6', 'parent' => 'm8.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-komoditas-podes/index?modul=peternakan\'})">Komoditas Peternakan</a>'],
                ['id' => 'm8.6.7', 'parent' => 'm8.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-komoditas-podes/index?modul=perikanan\'})">Komoditas Perikanan</a>'],
                //UTILITAS
	            ['id' => 'm72', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Utilitas</a>'],
	            ['id' => 'm72.1', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/rekap/index\'})">Rekap</a>'],
                ['id' => 'm72.1a', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/chart/rekap-chart/entrian\'})">Rekap Entrian</a>'],
	           	 ['id' => 'm72.2', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/rekap/bumdes\'})">Rekap Bumdes</a>'],
                 ['id' => 'm72.2a', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/rekap/kpm\'})">Rekap KPM</a>'],
	            ['id' => 'm72.3', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/download\'})">Download</a>'],
	            ['id' => 'm72.4', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/dokumen\'})">Dokumen</a>'],
	            ['id' => 'm72.5', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/ganti-password\'})">Ganti Password</a>'],
                ['id' => 'm72.6', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/management\'})">User Management</a>'],
                ['id' => 'm72.7', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/counting/provinsi-normalize-page\'})">Hitung Ulang APBDesa</a>'],
                ['id' => 'm72.8', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/log\'})">Log Aktifitas</a>'],
                // ['id' => 'm72.7', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/master-jabatan-tipe\'})">Master Tipe Jabatan</a>'],
                ['id' => 'm72.9', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/message-center/view-message-all\'})">Pesan Pengguna</a>'],
                ['id' => 'm72.10', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/ping-win-server\'})">Ping Windows Server</a>'],
                ['id' => 'm72.11', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/verifikasi/operator\'})">Verifikasi Operator</a>'],
                ['id' => 'm72.12', 'parent' => 'm72', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/verifikasi/desa-operator\'})">Verifikasi Desa Operator</a>'],
            ];
        }

        if(Yii::$app->user->identity->type == 'kabupaten'){
            $fooadd = [
            	//LEMBAGA KEMASYARAKATAN DESA DAN KELURAHAN
                ['id' => 'm1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Kelembagaan Desa</a>'],
                ['id' => 'm1.1', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Pemerintahan Desa</a>'],
                
                ['id' => 'm1.1.1', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data Kepala Desa</a>'],
                //KEPALA
                ['id' => 'm1.1.1.1', 'parent' => 'm1.1.1', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.1.1.1', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=lama-jabatan\'})">Berdasarkan Lama Jabatan</a>'],
                ['id' => 'm1.1.1.1.2', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.1.1.3', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.1.1.4', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.1.1.5', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                ['id' => 'm1.1.1.2', 'parent' => 'm1.1.1', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.1.2.1', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=lama-jabatan\'})">Berdasarkan Lama Jabatan</a>'],
                ['id' => 'm1.1.1.2.2', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.1.2.3', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.1.2.4', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.1.2.5', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //PERANGKAT-DESA
                ['id' => 'm1.1.2', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data Perangkat Desa</a>'],
                ['id' => 'm1.1.2.1', 'parent' => 'm1.1.2', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.2.1.1', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.2.1.2', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.2.1.3', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.2.1.4', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                ['id' => 'm1.1.2.2', 'parent' => 'm1.1.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.2.2.1', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.2.2.2', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.2.2.3', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.2.2.4', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //PERANGKAT-BPD
                ['id' => 'm1.1.3', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data BPD</a>'],
                ['id' => 'm1.1.3.1', 'parent' => 'm1.1.3', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.3.1.1', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.3.1.2', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.3.1.3', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.3.1.4', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],

                ['id' => 'm1.1.3.2', 'parent' => 'm1.1.3', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.3.2.1', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.3.2.2', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.3.2.3', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.3.2.4', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //TKD
                ['id' => 'm1.2', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)">Tanah Kas Desa</a>'],
                ['id' => 'm1.2.0', 'parent' => 'm1.2', 'text' => '<a href="javascript:void(0)">Data Tanah Kas Desa</a>'],
                ['id' => 'm1.2.0.1', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=luas-lahan\'})">Berdasarkan Luas Lahan</a>'],
                ['id' => 'm1.2.0.2', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=jenis-sertifikat\'})">Berdasarkan Jenis Sertifikat</a>'],
                ['id' => 'm1.2.0.3', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=lokasi\'})">Berdasarkan Lokasi</a>'],
                ['id' => 'm1.2.1', 'parent' => 'm1.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.2.1.1', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=luas-lahan\'})">Berdasarkan Luas Lahan</a>'],
                ['id' => 'm1.2.1.2', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=jenis-sertifikat\'})">Berdasarkan Jenis Sertifikat</a>'],
                ['id' => 'm1.2.1.3', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=lokasi\'})">Berdasarkan Lokasi</a>'],
                //DATA TAMBAHAN
                ['id' => 'm1.3', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)">Data Desa Tambahan</a>'],
                ['id' => 'm1.3.1', 'parent' => 'm1.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/data-tambahan/div-laporan?subjek=umum\'})">RT/RW</a>'],
                ['id' => 'm1.3.2', 'parent' => 'm1.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/data-tambahan/div-chart?subjek=umum\'})">Berdasarkan Data Tambahan</a>'],

	            //APBDESA
                ['id' => 'm2', 'parent' => '#', 'text' => 'APBDesa'],            
	            ['id' => 'm2.5', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
	            ['id' => 'm2.5.1', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/apbdesa\'})">Data APBDesa</a>'],
                ['id' => 'm2.5.2', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/grafik\'})">Grafik</a>'],
                ['id' => 'm2.5.4', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/apbdesa-sorting\'})">Rekapitulasi APBDesa</a>'],
                //LEMBAGA KEMASYARAKATAN DESA
                ['id' => 'm3', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Lembaga Kemasyarakatan Desa dan Kelurahan</a>'],

                ['id' => 'm3.1', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data PKK</a>'],
                ['id' => 'm3.1.1', 'parent' => 'm3.1', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.1.1.1', 'parent' => 'm3.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=pkk&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.1.2', 'parent' => 'm3.1', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.1.2.1', 'parent' => 'm3.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=pkk&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                ['id' => 'm3.2', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data LPMD/K</a>'],
                ['id' => 'm3.2.1', 'parent' => 'm3.2', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.2.1.1', 'parent' => 'm3.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=lpmd&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.2.2', 'parent' => 'm3.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.2.2.1', 'parent' => 'm3.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=lpmd&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                ['id' => 'm3.3', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data Karang Taruna</a>'],
                ['id' => 'm3.3.1', 'parent' => 'm3.3', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.3.1.1', 'parent' => 'm3.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=kartar&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.3.2', 'parent' => 'm3.3', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.3.2.1', 'parent' => 'm3.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=kartar&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                // ['id' => 'm3.4', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Lembaga Kemasyarakatan Lainnya</a>'],

                ['id' => 'm3.5', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data Posyandu</a>'],
                ['id' => 'm3.5.1', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Data</a>'],
                /*['id' => 'm3.5.1.1', 'parent' => 'm3.5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.1.2', 'parent' => 'm3.5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.2', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Grafik</a>'],
                ['id' => 'm3.5.2.1', 'parent' => 'm3.5.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.2.2', 'parent' => 'm3.5.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.3', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=posyandu&provinsi=true\'})">Daftar Operator</a>'],*/

                ['id' => 'm3.6', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data KPM</a>'],
                ['id' => 'm3.6.1', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.6.1.0', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kpm/non-operator-kpm\'})">Lihat Data</a>'],
                ['id' => 'm3.6.1.1', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm3.6.1.2', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm3.6.1.3', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm3.6.1.4', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=status\'})">Berdasarkan Status Aktif</a>'],
                ['id' => 'm3.6.2', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.6.2.1', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm3.6.2.2', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm3.6.2.3', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm3.6.2.4', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=status\'})">Berdasarkan Status Aktif</a>'],
                ['id' => 'm3.6.3', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=kpm&provinsi=true\'})">Daftar Operator</a>'],
                ['id' => 'm3.7', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/lembagaadat/default\'})">Data Lembaga Adat Desa</a>'],
                /*['id' => 'm3.7.1', 'parent' => 'm3.7', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.7.1.1', 'parent' => 'm3.7.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=lembagaadat&subjek=sekretariat\'})">Berdasarkan Status Sekretariat</a>'],
                ['id' => 'm3.7.2', 'parent' => 'm3.7', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.7.2.1', 'parent' => 'm3.7.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=lembagaadat&subjek=sekretariat\'})">Berdasarkan Status Sekretariat</a>'],*/
                //PASAR DESA
                ['id' => 'm32', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Pasar Desa</a>'],  
                ['id' => 'm32.1', 'parent' => 'm32', 'text' => '<a href="javascript:void(0)">Laporan</a>'],  
                ['id' => 'm32.1.1', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm32.1.2', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm32.1.3', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-pedagang\'})">Jumlah Pedagang</a>'],  
                ['id' => 'm32.1.4', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-kios\'})">Jumlah Kios</a>'],  
                ['id' => 'm32.1.5', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-los\'})">Jumlah Los</a>'],  
                ['id' => 'm32.1.6', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-lapak\'})">Jumlah Lapak</a>'],  
                ['id' => 'm32.1.7', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-lesehan\'})">Jumlah Lesehan</a>'],  
                ['id' => 'm32.1.8', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-ruko\'})">Jumlah Ruko</a>'],  
                
                ['id' => 'm32.2', 'parent' => 'm32', 'text' => '<a href="javascript:void(0)">Grafik</a>'],  
                ['id' => 'm32.2.1', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm32.2.2', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm32.2.3', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-pedagang\'})">Jumlah Pedagang</a>'],  
                ['id' => 'm32.2.4', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-kios\'})">Jumlah Kios</a>'],  
                ['id' => 'm32.2.5', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-los\'})">Jumlah Los</a>'],  
                ['id' => 'm32.2.6', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-lapak\'})">Jumlah Lapak</a>'],  
                ['id' => 'm32.2.7', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-lesehan\'})">Jumlah Lesehan</a>'],  
                ['id' => 'm32.2.8', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-ruko\'})">Jumlah Ruko</a>'],  
                //BUMDES-KABUPATEN
                ['id' => 'm6', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Bumdes</a>'],
                ['id' => 'm6.1', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm6.1.1', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=keuntungan\'})">Berdasarkan Keuntungan</a>'],  
                ['id' => 'm6.1.2', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=modal\'})">Berdasarkan Modal</a>'],  
                ['id' => 'm6.1.3', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=omset\'})">Berdasarkan Omset</a>'],  
                ['id' => 'm6.1.4', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=unit-usaha\'})">Berdasarkan Unit Usaha</a>'],  
                ['id' => 'm6.1.5', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm6.1.6', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm6.1.8', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/bumdes/klasifikasi\'})">Klasifikasi BumDesa</a>'],  

                ['id' => 'm6.2', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm6.2.1', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=keuntungan\'})">Berdasarkan Keuntungan</a>'],  
                ['id' => 'm6.2.2', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=modal\'})">Berdasarkan Modal</a>'],  
                ['id' => 'm6.2.3', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=omset\'})">Berdasarkan Omset</a>'],  
                ['id' => 'm6.2.4', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=unit-usaha\'})">Berdasarkan Unit Usaha</a>'],  
                ['id' => 'm6.2.5', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm6.2.6', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=region\'})">Berdasarkan Kabupaten</a>'],
                ['id' => 'm6.2.7', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=klasifikasi\'})">Berdasarkan Klasifikasi BumDesa</a>'],

                // PODES
                ['id' => 'm71', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Potensi Unggulan Desa</a>'],
                ['id' => 'm71.0', 'parent' => 'm71', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/laporan-podes\'})">Data Podes</a>'],
                ['id' => 'm71.0.1', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-pangan\'})">Data Podes Pertanian Pangan</a>'],
                ['id' => 'm71.0.2', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-buah\'})">Data Podes Pertanian Buah</a>'],
                ['id' => 'm71.0.3', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-apotik\'})">Data Podes Apotik Hidup</a>'],
                ['id' => 'm71.0.4', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perkebunan\'})">Data Podes Perkebunan</a>'],
                ['id' => 'm71.0.5', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=kehutanan\'})">Data Podes Kehutanan</a>'],
                ['id' => 'm71.0.6', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=peternakan\'})">Data Podes Peternakan</a>'],
                ['id' => 'm71.0.7', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perikanan\'})">Data Podes Perikanan</a>'],

                ['id' => 'm71.1', 'parent' => 'm71', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/chart-podes\'})">Grafik</a>'],
                ['id' => 'm71.1.1', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-pangan\'})">Data Podes Pertanian Pangan</a>'],
                ['id' => 'm71.1.2', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-buah\'})">Data Podes Pertanian Buah</a>'],
                ['id' => 'm71.1.3', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-apotik\'})">Data Podes Apotik Hidup</a>'],
                ['id' => 'm71.1.4', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perkebunan\'})">Data Podes Perkebunan</a>'],
                ['id' => 'm71.1.5', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=kehutanan\'})">Data Podes Kehutanan</a>'],
                ['id' => 'm71.1.6', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=peternakan\'})">Data Podes Peternakan</a>'],
                ['id' => 'm71.1.7', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perikanan\'})">Data Podes Perikanan</a>'],


                //UTILITAS
	            ['id' => 'm7', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Utilitas</a>'],
	            ['id' => 'm7.1', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/download\'})">Download</a>'],
	            ['id' => 'm7.2', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/dokumen\'})">Dokumen</a>'],
	            ['id' => 'm7.3', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/ganti-password\'})">Password</a>'],
                ['id' => 'm7.4', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/management-kabupaten\'})">User Management</a>'],
                ['id' => 'm7.5', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/log\'})">Log Aktifitas</a>'],
                ['id' => 'm7.6', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/list-verifikasi\'})">Verifikasi Indikator Dana Desa</a>'],
                
            ];
        }

        if(Yii::$app->user->identity->type == 'kecamatan'){
            $fooadd = [
            	//LEMBAGA KEMASYARAKATAN DESA DAN KELURAHAN
                ['id' => 'm1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Kelembagaan Desa</a>'],
                ['id' => 'm1.1', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Pemerintahan Desa</a>'],
                
                ['id' => 'm1.1.1', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data Kepala Desa</a>'],
                //KEPALA
                ['id' => 'm1.1.1.1', 'parent' => 'm1.1.1', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.1.1.1', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=lama-jabatan\'})">Berdasarkan Lama Jabatan</a>'],
                ['id' => 'm1.1.1.1.2', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.1.1.3', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.1.1.4', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.1.1.5', 'parent' => 'm1.1.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=kepala-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                ['id' => 'm1.1.1.2', 'parent' => 'm1.1.1', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.1.2.1', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=lama-jabatan\'})">Berdasarkan Lama Jabatan</a>'],
                ['id' => 'm1.1.1.2.2', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.1.2.3', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.1.2.4', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.1.2.5', 'parent' => 'm1.1.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=kepala-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //PERANGKAT-DESA
                ['id' => 'm1.1.2', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data Perangkat Desa</a>'],
                ['id' => 'm1.1.2.1', 'parent' => 'm1.1.2', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.2.1.1', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.2.1.2', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.2.1.3', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.2.1.4', 'parent' => 'm1.1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=perangkat-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                ['id' => 'm1.1.2.2', 'parent' => 'm1.1.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.2.2.1', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.2.2.2', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.2.2.3', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.2.2.4', 'parent' => 'm1.1.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=perangkat-desa&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //PERANGKAT-BPD
                ['id' => 'm1.1.3', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)">Data BPD</a>'],
                ['id' => 'm1.1.3.1', 'parent' => 'm1.1.3', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm1.1.3.1.1', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.3.1.2', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.3.1.3', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.3.1.4', 'parent' => 'm1.1.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/list-report?objek=bpd&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],

                ['id' => 'm1.1.3.2', 'parent' => 'm1.1.3', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.1.3.2.1', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm1.1.3.2.2', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm1.1.3.2.3', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm1.1.3.2.4', 'parent' => 'm1.1.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/chart-report?objek=bpd&subjek=pelatihan\'})">Berdasarkan Pelatihan</a>'],
                //TKD
                ['id' => 'm1.2', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)">Tanah Kas Desa</a>'],
                ['id' => 'm1.2.0', 'parent' => 'm1.2', 'text' => '<a href="javascript:void(0)">Data Tanah Kas Desa</a>'],
                ['id' => 'm1.2.0.1', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=luas-lahan\'})">Berdasarkan Luas Lahan</a>'],
                ['id' => 'm1.2.0.2', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=jenis-sertifikat\'})">Berdasarkan Jenis Sertifikat</a>'],
                ['id' => 'm1.2.0.3', 'parent' => 'm1.2.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-laporan?subjek=lokasi\'})">Berdasarkan Lokasi</a>'],
                ['id' => 'm1.2.1', 'parent' => 'm1.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm1.2.1.1', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=luas-lahan\'})">Berdasarkan Luas Lahan</a>'],
                ['id' => 'm1.2.1.2', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=jenis-sertifikat\'})">Berdasarkan Jenis Sertifikat</a>'],
                ['id' => 'm1.2.1.3', 'parent' => 'm1.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/tkd/div-chart?subjek=lokasi\'})">Berdasarkan Lokasi</a>'],
                //DATA TAMBAHAN
                ['id' => 'm1.3', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)">Data Desa Tambahan</a>'],
                ['id' => 'm1.3.1', 'parent' => 'm1.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/data-tambahan/div-laporan?subjek=umum\'})">RT/RW</a>'],
                ['id' => 'm1.3.2', 'parent' => 'm1.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/data-tambahan/div-chart?subjek=umum\'})">Berdasarkan Data Tambahan</a>'],

                //APBDESA
                ['id' => 'm2', 'parent' => '#', 'text' => 'APBDesa'],            
	            ['id' => 'm2.5', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm2.5.1', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/apbdesa\'})">Data APBDesa</a>'],
                ['id' => 'm2.5.2', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/grafik\'})">Grafik</a>'],
                ['id' => 'm2.5.4', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/apbdesa-sorting\'})">Rekapitulasi APBDesa</a>'],

                //LEMBAGA KEMASYARAKATAN DESA
                ['id' => 'm3', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Lembaga Kemasyarakatan Desa dan Kelurahan</a>'],

                ['id' => 'm3.1', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data PKK</a>'],
                ['id' => 'm3.1.1', 'parent' => 'm3.1', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.1.1.1', 'parent' => 'm3.1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=pkk&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.1.2', 'parent' => 'm3.1', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.1.2.1', 'parent' => 'm3.1.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=pkk&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                ['id' => 'm3.2', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data LPMD/K</a>'],
                ['id' => 'm3.2.1', 'parent' => 'm3.2', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.2.1.1', 'parent' => 'm3.2.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=lpmd&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.2.2', 'parent' => 'm3.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.2.2.1', 'parent' => 'm3.2.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=lpmd&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                ['id' => 'm3.3', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data Karang Taruna</a>'],
                ['id' => 'm3.3.1', 'parent' => 'm3.3', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.3.1.1', 'parent' => 'm3.3.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=kartar&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],
                ['id' => 'm3.3.2', 'parent' => 'm3.3', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.3.2.1', 'parent' => 'm3.3.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=kartar&subjek=pembina\'})">Berdasarkan Status Pembina</a>'],

                // ['id' => 'm3.4', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Data Lembaga Kemasyarakatan Lainnya</a>'],

                ['id' => 'm3.5', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data Posyandu</a>'],
                ['id' => 'm3.5.1', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Data</a>'],
                /*['id' => 'm3.5.1.1', 'parent' => 'm3.5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.1.2', 'parent' => 'm3.5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.2', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Grafik</a>'],
                ['id' => 'm3.5.2.1', 'parent' => 'm3.5.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.2.2', 'parent' => 'm3.5.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Berdasarkan </a>'],
                ['id' => 'm3.5.3', 'parent' => 'm3.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=posyandu&provinsi=true\'})">Daftar Operator</a>'],*/

                ['id' => 'm3.6', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data KPM</a>'],
                ['id' => 'm3.6.1', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.6.1.0', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kpm/non-operator-kpm\'})">Lihat Data</a>'],
                ['id' => 'm3.6.1.1', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm3.6.1.2', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm3.6.1.3', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm3.6.1.4', 'parent' => 'm3.6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-laporan?subjek=status\'})">Berdasarkan Status Aktif</a>'],
                ['id' => 'm3.6.2', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.6.2.1', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=umur\'})">Berdasarkan Umur</a>'],
                ['id' => 'm3.6.2.2', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=pendidikan\'})">Berdasarkan Pendidikan</a>'],
                ['id' => 'm3.6.2.3', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=jenis-kelamin\'})">Berdasarkan Jenis Kelamin</a>'],
                ['id' => 'm3.6.2.4', 'parent' => 'm3.6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/kpm/div-chart?subjek=status\'})">Berdasarkan Status Aktif</a>'],
                ['id' => 'm3.6.3', 'parent' => 'm3.6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=kpm&provinsi=true\'})">Daftar Operator</a>'],

                ['id' => 'm3.7', 'parent' => 'm3', 'text' => '<a href="javascript:void(0)">Data Lembaga Adat Desa</a>'],
                /*['id' => 'm3.7.1', 'parent' => 'm3.7', 'text' => '<a href="javascript:void(0)">Data</a>'],
                ['id' => 'm3.7.1.1', 'parent' => 'm3.7.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-laporan?objek=lembagaadat&subjek=sekretariat\'})">Berdasarkan Status Sekretariat</a>'],
                ['id' => 'm3.7.2', 'parent' => 'm3.7', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm3.7.2.1', 'parent' => 'm3.7.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/lembaga/div-chart?objek=lembagaadat&subjek=sekretariat\'})">Berdasarkan Status Sekretariat</a>'],*/
                //PASAR DESA
                ['id' => 'm32', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Pasar Desa</a>'],  
                ['id' => 'm32.1', 'parent' => 'm32', 'text' => '<a href="javascript:void(0)">Laporan</a>'],  
                ['id' => 'm32.1.1', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm32.1.2', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm32.1.3', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-pedagang\'})">Jumlah Pedagang</a>'],  
                ['id' => 'm32.1.4', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-kios\'})">Jumlah Kios</a>'],  
                ['id' => 'm32.1.5', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-los\'})">Jumlah Los</a>'],  
                ['id' => 'm32.1.6', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-lapak\'})">Jumlah Lapak</a>'],  
                ['id' => 'm32.1.7', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-lesehan\'})">Jumlah Lesehan</a>'],  
                ['id' => 'm32.1.8', 'parent' => 'm32.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-laporan?subjek=jml-ruko\'})">Jumlah Ruko</a>'],  
                
                ['id' => 'm32.2', 'parent' => 'm32', 'text' => '<a href="javascript:void(0)">Grafik</a>'],  
                ['id' => 'm32.2.1', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm32.2.2', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                ['id' => 'm32.2.3', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-pedagang\'})">Jumlah Pedagang</a>'],  
                ['id' => 'm32.2.4', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-kios\'})">Jumlah Kios</a>'],  
                ['id' => 'm32.2.5', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-los\'})">Jumlah Los</a>'],  
                ['id' => 'm32.2.6', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-lapak\'})">Jumlah Lapak</a>'],  
                ['id' => 'm32.2.7', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-lesehan\'})">Jumlah Lesehan</a>'],  
                ['id' => 'm32.2.8', 'parent' => 'm32.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/pasar-desa/div-chart?subjek=jml-ruko\'})">Jumlah Ruko</a>'],  
                //BUMDES
                ['id' => 'm6', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Data Bumdes</a>'],
                ['id' => 'm6.1', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)">Laporan</a>'],
                ['id' => 'm6.1.1', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=keuntungan\'})">Berdasarkan Keuntungan</a>'],  
                ['id' => 'm6.1.2', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=modal\'})">Berdasarkan Modal</a>'],  
                ['id' => 'm6.1.3', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=omset\'})">Berdasarkan Omset</a>'],  
                ['id' => 'm6.1.4', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=unit-usaha\'})">Berdasarkan Unit Usaha</a>'],  
                ['id' => 'm6.1.5', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm6.1.6', 'parent' => 'm6.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/list-report?subjek=region\'})">Berdasarkan Kabupaten</a>'],  

                ['id' => 'm6.2', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)">Grafik</a>'],
                ['id' => 'm6.2.1', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=keuntungan\'})">Berdasarkan Keuntungan</a>'],  
                ['id' => 'm6.2.2', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=modal\'})">Berdasarkan Modal</a>'],  
                ['id' => 'm6.2.3', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=omset\'})">Berdasarkan Omset</a>'],  
                ['id' => 'm6.2.4', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=unit-usaha\'})">Berdasarkan Unit Usaha</a>'],  
                ['id' => 'm6.2.5', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=tahun\'})">Berdasarkan Tahun Berdiri</a>'],  
                ['id' => 'm6.2.6', 'parent' => 'm6.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/bumdes/chart-report?subjek=region\'})">Berdasarkan Kabupaten</a>'],  
                // PODES
                ['id' => 'm71', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Potensi Unggulan Desa</a>'],
                ['id' => 'm71.0', 'parent' => 'm71', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/laporan-podes\'})">Data Podes</a>'],
                ['id' => 'm71.0.1', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-pangan\'})">Data Podes Pertanian Pangan</a>'],
                ['id' => 'm71.0.2', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-buah\'})">Data Podes Pertanian Buah</a>'],
                ['id' => 'm71.0.3', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-apotik\'})">Data Podes Apotik Hidup</a>'],
                ['id' => 'm71.0.4', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perkebunan\'})">Data Podes Perkebunan</a>'],
                ['id' => 'm71.0.5', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=kehutanan\'})">Data Podes Kehutanan</a>'],
                ['id' => 'm71.0.6', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=peternakan\'})">Data Podes Peternakan</a>'],
                ['id' => 'm71.0.7', 'parent' => 'm71.0', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perikanan\'})">Data Podes Perikanan</a>'],

                ['id' => 'm71.1', 'parent' => 'm71', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/chart-podes\'})">Grafik</a>'],
                ['id' => 'm71.1.1', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-pangan\'})">Data Podes Pertanian Pangan</a>'],
                ['id' => 'm71.1.2', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-buah\'})">Data Podes Pertanian Buah</a>'],
                ['id' => 'm71.1.3', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=pertanian-apotik\'})">Data Podes Apotik Hidup</a>'],
                ['id' => 'm71.1.4', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perkebunan\'})">Data Podes Perkebunan</a>'],
                ['id' => 'm71.1.5', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=kehutanan\'})">Data Podes Kehutanan</a>'],
                ['id' => 'm71.1.6', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=peternakan\'})">Data Podes Peternakan</a>'],
                ['id' => 'm71.1.7', 'parent' => 'm71.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/laporan/podes/div-laporan?subjek=perikanan\'})">Data Podes Perikanan</a>'],


                //UTILITAS
                ['id' => 'm7', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Utilitas</a>'],
                ['id' => 'm7.1', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/download\'})">Download</a>'],
                ['id' => 'm7.2', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/dokumen\'})">Dokumen</a>'],
                ['id' => 'm7.3', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/ganti-password\'})">Password</a>']
            ];
        }

        if(Yii::$app->user->identity->type == 'desa'){
            $fooadd = [
            	//KELEMBAGAAN DESA
            	['id' => 'm1', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Kelembagaan Desa</a>'],
	            ['id' => 'm1.1', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Pemerintahan Desa</a>'],
                ['id' => 'm1.1.1', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/data-kepala-desa\'})">Kepala Desa</a>'],
                ['id' => 'm1.1.2', 'parent' => 'm1.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-perangkat-desa/data-perangkat-desa\'})">Perangkat Desa</a>'],

                ['id' => 'm1.2', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/ta-bpd/data-bpd\'})">BPD</a>'],
                ['id' => 'm1.3', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-tkd/data-tanah\'})">Tanah Kas Desa</a>'],
             //    ['id' => 'm1.6', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Laporan</a>'],
	            // ['id' => 'm1.7', 'parent' => 'm1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Grafik</a>'],
	            //LEMBAGA KEMASYARAKATAN DESA
                ['id' => 'm1a', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Lembaga Kemasyarakatan Desa dan Kelurahan</a>'],
                ['id' => 'm1a.1', 'parent' => 'm1a', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">PKK</a>'],
                
                ['id' => 'm1a.1.1', 'parent' => 'm1a.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/pkk/pkk/view\'})">Data PKK</a>'],
                ['id' => 'm1a.1.2', 'parent' => 'm1a.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=pkk&provinsi=0\'})">Daftar Operator</a>'],
                ['id' => 'm1a.1.3', 'parent' => 'm1a.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/data-print/prepare?chapter=pkk\'})">Laporan</a>'],
                // ['id' => 'm1a.1.31', 'parent' => 'm1a.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/data-print/test?chapter=pkk\'})">Laporan Test</a>'],
                ['id' => 'm1a.1.4', 'parent' => 'm1a.1', 'text' => '<a href="javascript:void(0)">Grafik</a>'],

                ['id' => 'm1a.2', 'parent' => 'm1a', 'text' => '<a href="javascript:void(0)">LPMD/K</a>'],
                ['id' => 'm1a.2.1', 'parent' => 'm1a.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/lpmd/lpmd/view\'})">Data LPMD</a>'],
                ['id' => 'm1a.2.2', 'parent' => 'm1a.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=lpmd&provinsi=0\'})">Daftar Operator</a>'],
                ['id' => 'm1a.2.3', 'parent' => 'm1a.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/data-print/prepare?chapter=lpmd\'})">Laporan</a>'],
                ['id' => 'm1a.2.4', 'parent' => 'm1a.2', 'text' => '<a href="javascript:void(0)">Grafik</a>'],

                ['id' => 'm1a.3', 'parent' => 'm1a', 'text' => '<a href="javascript:void(0)">Karang Taruna</a>'],
                ['id' => 'm1a.3.1', 'parent' => 'm1a.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kartar/kartar/view\'})">Data Karang Taruna</a>'],
                ['id' => 'm1a.3.2', 'parent' => 'm1a.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=kartar&provinsi=0\'})">Daftar Operator</a>'],
                ['id' => 'm1a.3.3', 'parent' => 'm1a.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/data-print/prepare?chapter=kartar\'})">Laporan</a>'],
                ['id' => 'm1a.3.4', 'parent' => 'm1a.3', 'text' => '<a href="javascript:void(0)">Grafik</a>'],

                // ['id' => 'm1a.4', 'parent' => 'm1a', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Lembaga Kemasyarakatan Lainnya</a>'], 
                ['id' => 'm1a.5', 'parent' => 'm1a', 'text' => '<a href="javascript:void(0)">Posyandu</a>'],
                ['id' => 'm1a.5.1', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/default\'})">Data</a>'],
                ['id' => 'm1a.5.2', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=posyandu&provinsi=0\'})">Daftar Operator</a>'],
                // ['id' => 'm1a.5.1', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/non-operator-posyandu\'})">Master Posyandu</a>'],
                // ['id' => 'm1a.5.1', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/\'})">Master Posyandu</a>'],
                // ['id' => 'm1a.5.2', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-isian\'})">Isian Posyandu</a>'],
                // ['id' => 'm1a.5.3', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-kegiatan\'})">Kegiatan Posyandu</a>'],
                // ['id' => 'm1a.5.4', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-data\'})">Data Posyandu</a>'],
                // ['id' => 'm1a.5.5', 'parent' => 'm1a.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/posyandu/posyandu-sarana\'})">Sarana Posyandu</a>'],
                
             //    ['id' => 'm1a.6', 'parent' => 'm1a', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Laporan</a>'],
	            // ['id' => 'm1a.7', 'parent' => 'm1a', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/kelembagaan\'})">Grafik</a>'],
	            //LEMBAGA ADAT DESA
                ['id' => 'm1b', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Lembaga Adat Desa</a>'], 
                ['id' => 'm1b.1', 'parent' => 'm1b', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/lembagaadat/lembaga-adat/view\'})">Data Lembaga Adat</a>'],
                ['id' => 'm1b.2', 'parent' => 'm1b', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/data-print/prepare?chapter=lembagaadat\'})">Laporan</a>'],
                ['id' => 'm1b.3', 'parent' => 'm1b', 'text' => '<a href="javascript:void(0)">Grafik</a>'],   
                //PASAR DESA
                ['id' => 'm1c', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/transaksi/pasar-desa/index\'})">Pasar Desa</a>'],	
	            //DATA POTENSI DESA - PODES
	            //---------------------------------------------------------
	            ['id' => 'm21', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Potensi Desa</a>'],
                ['id' => 'm21.1', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/index?modul=pertanian-pangan\'})">Pertanian - Tanaman Pangan</a>'],
                ['id' => 'm21.2', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/index?modul=pertanian-buah\'})">Pertanian - Tanaman Buah</a>'],
                ['id' => 'm21.3', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/index?modul=pertanian-apotik\'})">Pertanian - Tanaman Apotik Hidup</a>'],
                ['id' => 'm21.4', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/index?modul=perkebunan\'})">Perkebunan</a>'],
                ['id' => 'm21.5', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes/index?modul=kehutanan\'})">Kehutanan</a>'],
                ['id' => 'm21.6', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes-pp/index?modul=peternakan\'})">Peternakan</a>'],
                ['id' => 'm21.7', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/ddc-podes-pp/index?modul=perikanan\'})">Perikanan</a>'],
	            // ['id' => 'm21.1', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sda\'})">Sumber Daya Alam</a>'],
	            // ['id' => 'm21.1.1', 'parent' => 'm21.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sda\'})">Upload Excel Prodeskel</a>'],
	            // ['id' => 'm21.1.2', 'parent' => 'm21.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sda\'})">Laporan</a>'],
	            // ['id' => 'm21.1.3', 'parent' => 'm21.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sda\'})">Grafik</a>'],
	            //---------------------------------------------------------
	            // ['id' => 'm21.2', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sdm\'})">Sumber Daya Manusia</a>'],
	            // ['id' => 'm21.2.1', 'parent' => 'm21.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sdm\'})">Upload Excel Prodeskel</a>'],
	            // ['id' => 'm21.2.2', 'parent' => 'm21.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sdm\'})">Laporan</a>'],
	            // ['id' => 'm21.2.3', 'parent' => 'm21.2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/sdm\'})">Grafik</a>'],
	            //---------------------------------------------------------
	            // ['id' => 'm21.3', 'parent' => 'm21', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/spd\'})">Sarana dan Prasaran Desa</a>'],
	            // ['id' => 'm21.3.1', 'parent' => 'm21.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/spd\'})">Upload Excel Prodeskel</a>'],
	            // ['id' => 'm21.3.2', 'parent' => 'm21.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/spd\'})">Laporan</a>'],
	            // ['id' => 'm21.3.3', 'parent' => 'm21.3', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/spd\'})">Grafik</a>'],

	            //----------------------------------------------------
                //APBDESA
	            ['id' => 'm2', 'parent' => '#', 'text' => '<a href="javascript:void(0)">APBDesa</a>'],            
	            ['id' => 'm2.5', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)">Laporan</a>'],

                ['id' => 'm2.1', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/rpjm-desa\'})">RPJM Desa</a>'],
                ['id' => 'm2.2', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/setting\'})">Kode Desa</a>'],
                ['id' => 'm2.3', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/import/new-flow-mdb-list\'})">Upload MDE Database</a>'],
                // ['id' => 'm2.4', 'parent' => 'm2', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Indikator Dana Desa</a>'],
                // ['id' => 'm2.4.1', 'parent' => 'm2.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Publikasi Dana Desa</a>'],
                // ['id' => 'm2.4.1.1', 'parent' => 'm2.4.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/rapbdes\'})">RAPBDes</a>'],
                // ['id' => 'm2.4.1.2', 'parent' => 'm2.4.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/penggunaan-dana-desa\'})">Penggunaan Dana Desa</a>'],
                // ['id' => 'm2.4.1.3', 'parent' => 'm2.4.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/pertanggungjawaban-dana-desa\'})">Pertanggungjawaban Dana Desa</a>'],

                // ['id' => 'm2.4.2', 'parent' => 'm2.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Transparansi Prosentase Dana Desa (SISKEUDES)</a>'],
                // ['id' => 'm2.4.3', 'parent' => 'm2.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Pelibatan Masyarakat Dalam Musyawarah Desa</a>'],
                // ['id' => 'm2.4.4', 'parent' => 'm2.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Administrasi Dana Desa</a>'],
                // ['id' => 'm2.4.4.1', 'parent' => 'm2.4.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Perencanaan APBDES</a>'],
                // ['id' => 'm2.4.4.2', 'parent' => 'm2.4.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Pelaksanaan APBDES</a>'],
                // ['id' => 'm2.4.4.3', 'parent' => 'm2.4.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Penatausahaan Keuangan Desa</a>'],
                // ['id' => 'm2.4.4.4', 'parent' => 'm2.4.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Laporan Realisasi Pelaksanaan APBDES</a>'],
                // ['id' => 'm2.4.4.5', 'parent' => 'm2.4.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Laporan Pertanggungjawaban APBDES</a>'],
                
                // ['id' => 'm2.4.5', 'parent' => 'm2.4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Peraturan Penetapan APBDES</a>'],
                // ['id' => 'm2.4.5.1', 'parent' => 'm2.4.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Peraturan Kepala Daerah</a>'],
                // ['id' => 'm2.4.5.2', 'parent' => 'm2.4.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/indikator-dana-desa/index\'})">Peraturan Desa</a>'],

                ['id' => 'm2.5.1', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/apbdes-desa\'})">Data APBDesa</a>'],
                ['id' => 'm2.5.2', 'parent' => 'm2.5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/grafik-desa\'})">Grafik</a>'],     

	            //---------------------------------------------------------
	            // BUMDESA
                ['id' => 'm5', 'parent' => '#', 'text' => '<a href="javascript:void(0)">BumDesa</a>'],
                ['id' => 'm5.1', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/non-operator-bumdes\'})">Data</a>'],
                ['id' => 'm5.2', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=bumdes&provinsi=0\'})">Daftar Operator</a>'],
	            // ['id' => 'm5', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/bumdes\'})">Bum Desa</a>'],
	            // ['id' => 'm5.1', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/bumdes\'})">Bum Desa Mandiri</a>'],
	            // ['id' => 'm5.1.1', 'parent' => 'm5.1', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/default/index?bumdes=mandiri\'})">Data Bum Desa Mandiri</a>'],
	            // ['id' => 'm5.2', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/bumdes\'})">Bum Desa Bersama</a>'],
	            ['id' => 'm51', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/bumdes/default/index?bumdes=bersama\'})">BumDesMa</a>'],
	            // ['id' => 'm5.3', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/bumdes\'})">Laporan</a>'],
	            // ['id' => 'm5.4', 'parent' => 'm5', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/bumdes\'})">Grafik</a>'],
                //KLINIK BUMDES
                // ['id' => 'm51', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kelas/default/index-peserta\'})">Klinik BumDes</a>'],

	            //---------------------------------------------------------
	            //KADER PEMBERDAYAAN MASYARAKAT
	            ['id' => 'm6', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kpm/non-operator-kpm\'})">Kader Pemberdayaan Masyarakat</a>'],
	            ['id' => 'm6.1', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/kpm/non-operator-kpm\'})">Data Isian Kader</a>'],
                ['id' => 'm6.2', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/ddc/data-operator/list-operator?type=kpm&provinsi=0\'})">Daftar Operator</a>'],
	            //---------------------------------------------------------
	            /*['id' => 'm6.2', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/pendamping-desa\'})">Laporan</a>'],
	            ['id' => 'm6.3', 'parent' => 'm6', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/pendamping-desa\'})">Grafik</a>'],*/
	            //---------------------------------------------------------
                //ASET DESA
	            // ['id' => 'm4', 'parent' => '#', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/aset-desa\'})">Aset Desa</a>'],
	            // ['id' => 'm4.1', 'parent' => 'm4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/aset-desa\'})">Pendataan Aset Desa</a>'],
	            // ['id' => 'm4.2', 'parent' => 'm4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/aset-desa\'})">Laporan</a>'],
	            // ['id' => 'm4.3', 'parent' => 'm4', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/aset-desa\'})">Grafik</a>'],
	            //---------------------------------------------------------
	            //UTILITAS
	            ['id' => 'm7', 'parent' => '#', 'text' => '<a href="javascript:void(0)">Utilitas</a>'],
	            ['id' => 'm7.1', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/download\'})">Download</a>'],
	            ['id' => 'm7.2', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/default/dokumen\'})">Dokumen</a>'],
                ['id' => 'm7.3', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/user/ganti-password\'})">Password</a>'],
                ['id' => 'm7.4', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/sp3ddashboard/message-center/create\'})">Tulis Kritik dan Saran</a>'],
                ['id' => 'm7.5', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/operatora/desa-akses-operator/konfirmasi-desa-index\'})">Permintaan Operator</a>'],
	            ['id' => 'm7.6', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/notifikasi/index\'})">Pusat Notifikasi</a>'],
	            ['id' => 'm7.7', 'parent' => 'm7', 'text' => '<a href="javascript:void(0)" onclick="goLoad({url:\'/umum/utilitas/ping-win-server\'})">Ping Windows Server</a>'],
	        ];
        }
        // $foor = array_merge($foo, $foomix);
        // Yii::$app->response->format = Response::FORMAT_JSON;
        // return $mod->all();
        $foomix = array_merge($fooadd);
        sort($foomix);
        return $this->asJson($foomix);
    }
}
