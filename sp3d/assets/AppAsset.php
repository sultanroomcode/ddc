<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace sp3d\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/Sailor/css/bootstrap.min.css',//use 3.3.2
        // 'css/Sailor/plugins/flexslider/flexslider.css',
        // 'css/Sailor/css/cubeportfolio.min.css',
        // 'css/Sailor/css/style.css',
        // 'css/Sailor/skins/default.css',
        // 'css/Sailor/bodybg/bg1.css',
        // 'css/vendor/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
        // 'local-cdn/bootstrap-select/dist/css/bootstrap-select.min.css'
    ];
   
    public $js = [
        // 'css/Sailor/js/modernizr.custom.js',
        // 'css/Sailor/js/jquery.easing.1.3.js',
        'css/Sailor/js/bootstrap.min.js',
        // 'css/Sailor/plugins/flexslider/jquery.flexslider-min.js',
        // 'css/Sailor/plugins/flexslider/flexslider.config.js',
        // 'css/Sailor/js/jquery.appear.js',
        // 'css/Sailor/js/stellar.js',
        // 'css/Sailor/js/classie.js',
        // 'css/Sailor/js/uisearch.js',
        // 'css/Sailor/js/jquery.cubeportfolio.min.js',
        // 'css/Sailor/js/google-code-prettify/prettify.js',
        // 'css/Sailor/js/animate.js',
        // 'css/Sailor/js/custom.js',
       
        // 'js/chart.js/dist/Chart.min.js',
        // 'js/randomColor.js',
        // 'local-cdn/bootstrap-select/dist/js/bootstrap-select.min.js',
        // 'local-cdn/bootstrap-select/dist/js/i18n/defaults-id_ID.min.js',
        // 'local-cdn/echarts.min.js',
        // 'js/sts.js'
        'js/sts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
