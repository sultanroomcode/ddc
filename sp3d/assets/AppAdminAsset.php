<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace sp3d\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/adminlte/bootstrap/css/bootstrap.min.css',
        'css/site.css',
        'css/adminlte/dist/css/AdminLTE.min.css',
        'css/adminlte/dist/css/skins/_all-skins.min.css',
        // 'css/adminlte/plugins/iCheck/flat/blue.css',
        // 'css/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        // 'css/adminlte/plugins/morris/morris.css',
        // 'css/adminlte/plugins/daterangepicker/daterangepicker.css',
        // 'css/adminlte/plugins/iCheck/flat/blue.css',
        'css/vendor/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
        'js/datetimepicker/jquery.datetimepicker.css',
        'css/adminlte/plugins/iCheck/flat/blue.css',
        // 'js/magnific-popup.css',

    ];
    public $js = [
        //'css/adminlte/bootstrap/js/bootstrap.min.js',
        // 'css/adminlte/plugins/morris/morris.min.js',
        // 'css/adminlte/plugins/jQueryUI/jquery-ui.min.js',
        // 'css/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'css/adminlte/dist/js/app.js',
        'css/adminlte/dist/js/demo.js',
        'css/adminlte/dist/js/pages/dashboard.js',
        'js/jquery.magnific-popup.min.js',
        'js/datetimepicker/build/jquery.datetimepicker.full.min.js',
        'js/sts.js',
        //js/jFormValidator/form-validator/jquery.form-validator.min.js
        //js/mustache/mustache.min.js
        //js/siapatakut.js
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}