<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace sp3d\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Front2AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        // '//localhost/local-cdn/bower_components/textillate/assets/animate.css',
        // '//localhost/local-cdn/bower_components/tether/dist/css/tether.min.css',
        'css/bootstrap/css/bootstrap.min.css',
        'css/font-awesome/css/font-awesome.min.css',
        '//localhost/local-cdn/bower_components/datatables/media/css/dataTables.bootstrap4.min.css',
        '//localhost/labs/ddc-new-frontend/ddc/site/css/custom.css',
        '//localhost/local-cdn/bower_components/datatables/media/css/jquery.dataTables.min.css',
        // 'css/animate.css',
        // 'css/style.css',
        '//fonts.googleapis.com/css?family=Arvo:400,700%7COpen+Sans:300,300italic,400,400italic,700italic,800%7CUbuntu:500',
        '//localhost/labs/ddc-new-frontend/ddc/site/css/custom.css',
        '//localhost/labs/ddc-new-frontend/ddc/site/css/style.css',
    ];
   
    public $js = [
        // 'css/DataTables/jQuery-3.2.1/jquery-3.2.1.min.js',
        '//localhost/local-cdn/bower_components/datatables/media/js/dataTables.bootstrap4.min.js',
        // '//localhost/local-cdn/bower_components/textillate/assets/jquery.lettering.js',
        // '//localhost/local-cdn/bower_components/textillate/jquery.textillate.js',
        // '//localhost/local-cdn/bower_components/datatables/media/js/jquery.dataTables.min.js',
        '//localhost/local-cdn/bower_components/tether/dist/js/tether.min.js',
        'css/bootstrap/js/bootstrap.min.js',
        '//localhost/local-cdn/echarts.min.js',
        'js/wow.min.js',
        '//localhost/labs/ddc-new-frontend/ddc/site/js/core.min.js',
        '//localhost/labs/ddc-new-frontend/ddc/site/js/script.js',
        'js/sts2.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
