<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace sp3d\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class OnlineFront2AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/textillate/assets/animate.css',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/tether/dist/css/tether.min.css',
        'css/bootstrap/css/bootstrap.min.css',
        'css/font-awesome/css/font-awesome.min.css',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/datatables/media/css/dataTables.bootstrap4.min.css',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/datatables/media/css/jquery.dataTables.min.css',
        'css/animate.css',
        'css/style.css',
    ];
   
    public $js = [
        // 'css/DataTables/jQuery-3.2.1/jquery-3.2.1.min.js',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/datatables/media/js/dataTables.bootstrap4.min.js',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/textillate/assets/jquery.lettering.js',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/textillate/jquery.textillate.js',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/datatables/media/js/jquery.dataTables.min.js',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/bower_components/tether/dist/js/tether.min.js',
        'css/bootstrap/js/bootstrap.min.js',
        '//datadesacenter.dpmd.jatimprov.go.id/local-cdn/echarts.min.js',
        'js/wow.min.js',
        'js/sts.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
