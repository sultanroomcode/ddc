<?php
namespace sp3d\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "pasardesa_data".
 *
 * @property string $kd_desa
 * @property int $id_pasar
 * @property int $kd_isian
 * @property int $jml_pedagang
 * @property int $jml_jenis_barang
 * @property int $pras_ruko
 * @property int $pras_kios
 * @property int $pras_los
 * @property int $pras_lapak
 * @property int $pras_lesehan
 * @property string $pras_kantor
 * @property string $pras_aset
 * @property string $pras_pendukung
 * @property string $pengelola
 * @property string $keterangan
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PasardesaData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasardesa_data';
    }

    public function behaviors()
    {
         return [
             /*[
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],*/
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_pasar', 'kd_isian'], 'required'],
            [['id_pasar', 'kd_isian', 'jml_pedagang', 'jml_jenis_barang', 'pras_ruko', 'pras_kios', 'pras_los', 'pras_lapak', 'pras_lesehan'], 'integer'],
            [['kd_desa'], 'string', 'max' => 12],
            [['pras_kantor'], 'string', 'max' => 10],
            [['pras_aset', 'pras_pendukung', 'keterangan'], 'string', 'max' => 255],
            [['pengelola'], 'string', 'max' => 100],
            [['kd_desa', 'id_pasar', 'kd_isian'], 'unique', 'targetAttribute' => ['kd_desa', 'id_pasar', 'kd_isian']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kode Desa',
            'id_pasar' => 'ID Pasar',
            'kd_isian' => 'Kode Isian',
            'jml_pedagang' => 'Jumlah Pedagang',
            'jml_jenis_barang' => 'Jumlah Jenis Barang',
            'pras_ruko' => 'Prasarana Ruko',
            'pras_kios' => 'Prasarana Kios',
            'pras_los' => 'Prasarana Los',
            'pras_lapak' => 'Prasarana Lapak',
            'pras_lesehan' => 'Prasarana Lesehan',
            'pras_kantor' => 'Prasarana Kantor',
            'pras_aset' => 'Prasarana Aset',
            'pras_pendukung' => 'Prasarana Pendukung',
            'pengelola' => 'Pengelola',
            'keterangan' => 'Keterangan',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getPasar()
    {
        return $this->hasOne(Pasardesa::className(), ['kd_desa' => 'kd_desa', 'id_pasar' => 'id_pasar']);
    }
}
