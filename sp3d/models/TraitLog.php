<?php
namespace sp3d\models;

trait TraitLog {
    public function writeLog($user, $message, $kd_tipe='UPLOAD_MDB'){//wordwrap
        $model = new Log;
        $model->id = time().'-'.$user;
        $model->kd_pum = $model->cre_id = $user;
        $model->aktivitas = $message;
        $model->ip_addr = $this->getUserIP();
        $model->tanggal_aksi = date('Y-m-d H:i:s');
        $model->kd_tipe = $kd_tipe;

        return $model->save();
    }

    public function getUserIP()
    {
        //https://stackoverflow.com/questions/13646690/how-to-get-real-ip-from-visitor
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            return $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            return $forward;
        }
        else
        {
            return $remote;
        }
    }
}