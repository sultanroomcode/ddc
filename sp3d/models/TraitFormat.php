<?php
namespace sp3d\models;

trait TraitFormat {
	public function nf($val){//number format
        //return $val;
        \Yii::$app->formatter->thousandSeparator = '.';
        \Yii::$app->formatter->decimalSeparator = ',';
        return \Yii::$app->formatter->asDecimal($val,2);
        // return number_format($val, 2, ',','.');
    }

    public function nfo($val){//number format
        return number_format(round($val), 0, ',','.');
    }

    public function ww($text, $far){//wordwrap
        return wordwrap($text,$far,"<br>",TRUE);
    }
}