<?php
namespace sp3d\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "pasardesa".
 *
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property string $kd_provinsi
 * @property int $id_pasar
 * @property string $nama
 * @property string $kepemilikan_tanah
 * @property string $legalitas
 * @property string $no_legalitas
 * @property string $tahun
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Pasardesa extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasardesa';
    }

    public function behaviors()
    {
         return [
             // [
             //     'class' => BlameableBehavior::className(),
             //     'createdByAttribute' => 'created_by',
             //     'updatedByAttribute' => 'updated_by',
             // ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_desa', 'id_pasar'], 'required'],
            [['id_pasar'], 'integer'],
            [['kd_desa', 'kd_kecamatan', 'kd_kabupaten', 'kd_provinsi'], 'string', 'max' => 12],
            [['nama'], 'string', 'max' => 150],
            [['kepemilikan_tanah', 'legalitas', 'no_legalitas', 'keterangan'], 'string', 'max' => 100],
            [['tahun'], 'string', 'min' => 4],
            [['tahun'], 'string', 'max' => 4],
            [['kd_desa', 'id_pasar'], 'unique', 'targetAttribute' => ['kd_desa', 'id_pasar']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_provinsi' => 'Kode Provinsi',
            'id_pasar' => 'Id Pasar',
            'nama' => 'Nama',
            'kepemilikan_tanah' => 'Kepemilikan Tanah',
            'legalitas' => 'Legalitas',
            'no_legalitas' => 'No Legalitas',
            'tahun' => 'Tahun',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    //relation
    public function getData()
    {
        return $this->hasMany(PasardesaData::className(), ['kd_desa' => 'kd_desa', 'id_pasar' => 'id_pasar']);
    }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa])->max('id_pasar');
        if($mx == null){
            $this->id_pasar = 1;
        } else {
            $this->id_pasar = $mx +1;
        }
    }

    public function arrLahan()
    {
        return [
            'perorangan' => 'Perorangan',
            'perusahaan' => 'Perusahaan',
            'pemerintah' => 'Pemerintah'
        ];
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        // PasardesaData::deleteAll(['kd_desa' => $this->kd_desa, 'id_pasar' => $this->id_pasar]);
        // sudah ditangani foreign key
        return true;
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }
}
