<?php
namespace sp3d\models;
use Yii;
use yii\base\Model;
use sp3d\models\transaksi\Sp3dMasterJabatan;
use sp3d\models\transaksi\Sp3dMasterJabatanTipe;
use sp3d\models\transaksi\Sp3dMasterJenisPelatihan;
use sp3d\models\transaksi\Sp3dPerangkatDesa;
use sp3d\models\transaksi\Sp3dPerangkatPelatihan;

use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\bumdes\models\BumdesModal;

class DataLaporan extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $regionWhere;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        $objekparam = (isset($this->data['jabatan']) && $this->data['jabatan'] != '-')?$this->data['jabatan']:$this->data['objek'];
        switch ($this->reportType) {
            case 'kepala-desa-lama-jabatan':
            case 'kepala-desa-habis-jabatan':
            case 'kepala-desa-jenis-kelamin':
            case 'kepala-desa-pendidikan':
            case 'kepala-desa-umur':
            case 'perangkat-desa-lama-jabatan':
            case 'perangkat-desa-habis-jabatan':
            case 'perangkat-desa-jenis-kelamin':
            case 'perangkat-desa-pendidikan':
            case 'perangkat-desa-umur':
            case 'bpd-lama-jabatan':
            case 'bpd-habis-jabatan':
            case 'bpd-jenis-kelamin':
            case 'bpd-pendidikan':
            case 'bpd-umur':
                $this->model = Sp3dPerangkatDesa::find();
                $this->model->where(['status_aktif' => 'aktif']);
                $this->model->andWhere(['jabatan' => $objekparam]);
                $this->regionWhere = 'sp3d_perangkat_desa';
            break;
            case 'kepala-desa-pelatihan':
            case 'perangkat-desa-pelatihan':
            case 'bpd-pelatihan':
                $this->model = Sp3dPerangkatPelatihan::find()->joinWith(['kategoripelatihan', 'perangkat']);
                $this->model->andWhere(['sp3d_perangkat_desa.jabatan' => $objekparam]);
                $this->regionWhere = 'sp3d_perangkat_desa';
            break;
            case 'bumdes-keuntungan':
            case 'bumdes-omset':
            case 'bumdes-modal':
                $this->model = BumdesModal::find()->joinWith('bumdes');
                $this->regionWhere = 'bumdes';
            break;
            case 'bumdes-region':
            case 'bumdes-tahun':
                $this->model = Bumdes::find();
                $this->regionWhere = 'bumdes';
            break;
            case 'bumdes-unit-usaha':
                $this->model = BumdesUnitUsaha::find()->joinWith('bumdes');
                $this->regionWhere = 'bumdes';
            break;
            
            default: exit('no choice'); break;
        }

        $this->pageToOpen = 'data-'.$this->reportType;
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere($this->regionWhere.'.kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere($this->regionWhere.'.kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        // var_dump($this->data['subjek']);
        // var_dump($this->data['subjekval']);
        if($this->data['subjek'] != '-' && $this->data['subjek'] != null){
            switch($this->data['subjek']){
                case 'pelatihan':
                    
                    $objekparam = ($this->data['jabatan'] != '-')?$this->data['jabatan']:$this->data['objek'];
                    if($this->data['subjekval'] != '-'){
                        $this->model->andWhere([
                            'sp3d_perangkat_pelatihan.tipe' => $this->data['tipe'],//khusu pelatihan param tipe harus ada, bpd, desa, dsb 
                            'sp3d_master_jenis_pelatihan.id' => $this->data['subjekval']
                        ]);                        
                    }
                break;
                case 'lama-jabatan':
                    $this->model->select(['*', "(tahun_akhir - DATE_FORMAT(NOW(), '%Y')) AS dummy_var"]);
                    if($this->data['subjekval'] != '-'){
                        $this->model->andFilterHaving(['dummy_var' => $this->data['subjekval']]);
                    }
                break;
                case 'jenis-kelamin':
                    if($this->data['subjekval'] != '-'){
                        $this->model->andWhere(['jenis_kelamin' => $this->data['subjekval']]);
                    }
                break;
                case 'pendidikan':
                    if($this->data['subjekval'] != '-'){    
                        $this->model->andWhere(['pendidikan' => $this->data['subjekval']]);
                    }
                break;
                case 'umur':
                    $this->model->select(['*', "(DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) AS dummy_var"]);
                    if($this->data['subjekval'] != '-'){
                        $exp = explode('-', $this->data['subjekval']);
                        if(count($exp) > 1){
                            $this->model->andFilterHaving(['>=','dummy_var', $exp[0]]);
                            $this->model->andFilterHaving(['<=','dummy_var', $exp[1]]);
                        }
                    }
                break;
                //BUMDESA
                case 'tahun':
                    if($this->data['subjekval'] != '-'){
                        $this->model->andWhere(['tahun_berdiri' => $this->data['subjekval']]);
                    }
                break;
                case 'keuntungan':
                case 'modal':
                case 'omset':
                    $this->model->select(['*', "CASE WHEN (".$this->data['subjek'].") <= 1000000 THEN '0-1jt'
                     WHEN (bumdes_modal.".$this->data['subjek'].") <= 5000000 THEN '1-5jt'
                     WHEN (bumdes_modal.".$this->data['subjek'].") <= 10000000 THEN '5-10jt'
                     WHEN (bumdes_modal.".$this->data['subjek'].") <= 50000000 THEN '10-50jt'
                     WHEN (bumdes_modal.".$this->data['subjek'].") > 50000000 THEN '50jt-keatas' END AS subjekval"]);
                    if($this->data['subjekval'] != '-'){
                        $this->model->andFilterHaving(['subjekval' => $this->data['subjekval']]);
                    }
                break;
                case 'region':
                    if($this->data['subjekval'] != '-'){
                        $this->model->andWhere(['kd_kabupaten' => $this->data['subjekval']]);
                    }
                break;
                case 'unit-usaha':
                    // SELECT count(*) AS hit, unit_usaha AS subjekval FROM bumdes_unit_usaha GROUP BY subjekval ORDER BY hit DESC LIMIT 20
                    if($this->data['subjekval'] != '-'){
                        $this->model->andWhere(['unit_usaha' => $this->data['subjekval']]);
                    }
                break;
            }
            // ini_set('memory_limit', '512M');
            set_time_limit(60);
            ini_set('memory_limit', '8192M');
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }

    public function attributes()
    {
        $arr = [];
        switch($this->data['subjek']){
            case 'pelatihan':
                $arr = [
                    ['attribute' => 'perangkat.kabupaten.description', 'label' => 'Kabupaten',],
                    ['attribute' => 'perangkat.kecamatan.description', 'label' => 'Kecamatan',],
                    ['attribute' => 'perangkat.desa.description', 'label' => 'Desa',],
                    ['attribute' => 'nik',],
                    ['attribute' => 'perangkat.nama',],
                    ['attribute' => 'perangkat.jabatan',],
                    ['attribute' => 'nama', 'label' => 'Nama Pelatihan',],
                    ['attribute' => 'kategori', 'label' => 'Kategori Pelatihan',],
                    ['attribute' => 'tahun', 'label' => 'Tahun Pelatihan',],
                    ['attribute' => 'penyelenggara', 'label' => 'Penyelenggara Pelatihan',]
                ];
            break;
            case 'lama-jabatan':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten',],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan',],
                    ['attribute' => 'desa.description', 'label' => 'Desa',],
                    ['attribute' => 'user',],
                    ['attribute' => 'nik',],
                    ['attribute' => 'nama',],
                    ['attribute' => 'jabatan',],
                    ['attribute' => 'tempat_lahir',],
                    ['attribute' => 'tanggal_lahir', 'value' => function($m){
                        return date('d-F-Y', strtotime($m->tanggal_lahir));
                    }],
                    ['attribute' => 'dummy_var', 'value' => function($m){
                        return $m->tahun_akhir - date('Y');                        
                    },  'label' => 'Lama Jabatan'],
                ];
            break;
            case 'jenis-kelamin':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten',],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan',],
                    ['attribute' => 'desa.description', 'label' => 'Desa',],
                    ['attribute' => 'user',],
                    ['attribute' => 'nik', 'value' => function($m){
                        return (string) $m->nik;
                    }],
                    ['attribute' => 'nama',],
                    ['attribute' => 'jabatan',],
                    ['attribute' => 'tempat_lahir',],
                    ['attribute' => 'tempat_lahir',],
                    ['attribute' => 'tanggal_lahir', 'value' => function($m){
                        return date('d-F-Y', strtotime($m->tanggal_lahir));
                    }],
                ];
            break;
            case 'pendidikan':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten',],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan',],
                    ['attribute' => 'desa.description', 'label' => 'Desa',],
                    ['attribute' => 'user',],
                    ['attribute' => 'nik',],
                    ['attribute' => 'nama',],
                    ['attribute' => 'jabatan',],
                    ['attribute' => 'tempat_lahir',],
                    ['attribute' => 'tanggal_lahir', 'value' => function($m){
                        return date('d-F-Y', strtotime($m->tanggal_lahir));
                    }],
                    ['attribute' => 'pendidikan', 'label' => 'Pendidikan', 'value' => function($m){
                        return substr($m->pendidikan, 2);
                    }],
                ];
            break;
            case 'umur':
                $arr = [
                    ['attribute' => 'kabupaten.description', 'label' => 'Kabupaten',],
                    ['attribute' => 'kecamatan.description', 'label' => 'Kecamatan',],
                    ['attribute' => 'desa.description', 'label' => 'Desa',],
                    ['attribute' => 'user',],
                    ['attribute' => 'nik'],
                    ['attribute' => 'nama',],
                    ['attribute' => 'jabatan',],
                    ['attribute' => 'tempat_lahir',],
                    ['attribute' => 'tanggal_lahir', 'value' => function($m){
                        return date('d-F-Y', strtotime($m->tanggal_lahir));
                    }],
                    ['attribute' => 'dummy_var', 'label' => 'Umur'],
                ];
            break;
            //BUMDESA
            case 'tahun':
                $arr = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'bumdes.nama_bumdes',],
                    ['attribute' => 'bumdes.tahun_berdiri',],
                    ['attribute' => 'unit_usaha'],
                ];
            break;
            case 'keuntungan':
            case 'modal':
            case 'omset':
                $arr = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'bumdes.nama_bumdes',],
                    ['attribute' => 'bumdes.tahun_berdiri',],
                    ['attribute' => 'unit_usaha'],
                ];
            break;
            case 'region':
                $arr = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'bumdes.nama_bumdes',],
                    ['attribute' => 'bumdes.tahun_berdiri',],
                    ['attribute' => 'unit_usaha'],
                ];
            break;
            case 'unit-usaha':
                $arr = [
                    ['attribute' => 'kabupaten.description',],
                    ['attribute' => 'kecamatan.description',],
                    ['attribute' => 'desa.description',],
                    ['attribute' => 'bumdes.nama_bumdes',],
                    ['attribute' => 'bumdes.tahun_berdiri',],
                    ['attribute' => 'unit_usaha'],
                ];
            break;
        }

        return $arr;
    }
}
