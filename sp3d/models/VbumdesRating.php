<?php

namespace sp3d\models;

use Yii;
use sp3d\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "vbumdes_rating".
 *
 * @property string $kb_id
 * @property string $kb_nama
 * @property string $kc_id
 * @property string $kc_nama
 * @property string $ds_id
 * @property string $ds_nama
 * @property string $nama_bumdes
 * @property string $nilai_akhir
 * @property string $nilai_akhir_kategori
 */
class VbumdesRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vbumdes_rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kb_id', 'kb_nama', 'kc_id', 'kc_nama', 'ds_id', 'ds_nama'], 'required'],
            [['kb_id', 'kc_id', 'ds_id'], 'string', 'max' => 15],
            [['kb_nama', 'kc_nama', 'ds_nama'], 'string', 'max' => 255],
            [['nama_bumdes'], 'string', 'max' => 150],
            [['nilai_akhir'], 'string', 'max' => 9],
            [['nilai_akhir_kategori'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kb_id' => 'Kb ID',
            'kb_nama' => 'Kb Nama',
            'kc_id' => 'Kc ID',
            'kc_nama' => 'Kc Nama',
            'ds_id' => 'Ds ID',
            'ds_nama' => 'Ds Nama',
            'nama_bumdes' => 'Nama Bumdes',
            'nilai_akhir' => 'Nilai Akhir',
            'nilai_akhir_kategori' => 'Nilai Akhir Kategori',
        ];
    }

    public function dataKabupaten()
    {
        return ArrayHelper::map(User::find()->where(['type' => 'kabupaten'])->orderBy('description ASC')->all(), 'id', 'description');
    }
}
