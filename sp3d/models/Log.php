<?php
namespace sp3d\models;

use Yii;
use Carbon\Carbon;
use yii\db\ActiveRecord;
Carbon::setLocale('id');
/**
 * This is the model class for table "log".
 *
 * @property string $id
 * @property string $cre_id
 * @property string $aktivitas
 * @property string $tanggal_aksi
 */
class Log extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cre_id', 'aktivitas', 'tanggal_aksi'], 'required'],
            [['tanggal_aksi'], 'safe'],
            [['cre_id', 'ip_addr'], 'string', 'max' => 20],
            [['kd_tipe'], 'string', 'max' => 10],
            [['id'], 'string', 'max' => 21],
            [['aktivitas'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip_addr' => 'IP Address',
            'cre_id' => 'Cre ID',
            'aktivitas' => 'Aktivitas',
            'tanggal_aksi' => 'Tanggal Aksi',
        ];
    }

    public function cDate()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->tanggal_aksi)->diffForHumans();
    }
}
