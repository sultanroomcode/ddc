<?php
namespace sp3d\models;
use Yii;
use yii\base\Model;
use sp3d\models\transaksi\Sp3dMasterJabatan;
use sp3d\models\transaksi\Sp3dMasterJabatanTipe;
use sp3d\models\transaksi\Sp3dMasterJenisPelatihan;
use sp3d\models\transaksi\Sp3dPerangkatDesa;
use sp3d\models\transaksi\Sp3dPerangkatPelatihan;

use sp3d\modules\bumdes\models\BumdesUnitUsaha;
use sp3d\modules\bumdes\models\Bumdes;
use sp3d\modules\bumdes\models\BumdesModal;

class DataChart extends Model
{
    use \sp3d\models\TraitFormat;
    public $model;
    public $data;
    public $pageToOpen;
    public $reportType;
    /*
    you have, objek, subjek, subjekval, jabatan, kabupaten-input, kecamatan-input
    */
    public function dataSet($data){
        $this->data = $data;
        $this->reportType = $data['objek'].'-'.$data['subjek'];
        switch ($this->reportType) {
            case 'kepala-desa-lama-jabatan':
            case 'kepala-desa-habis-jabatan':
            case 'kepala-desa-jenis-kelamin':
            case 'kepala-desa-pendidikan':
            case 'kepala-desa-umur':
            case 'perangkat-desa-lama-jabatan':
            case 'perangkat-desa-habis-jabatan':
            case 'perangkat-desa-jenis-kelamin':
            case 'perangkat-desa-pendidikan':
            case 'perangkat-desa-umur':
            case 'bpd-lama-jabatan':
            case 'bpd-habis-jabatan':
            case 'bpd-jenis-kelamin':
            case 'bpd-pendidikan':
            case 'bpd-umur':
                $this->model = Sp3dPerangkatDesa::find();
                $this->model->where(['status_aktif' => 'aktif']);
                $this->model->andWhere(['jabatan' => $data['objek']]);
            break;
            case 'kepala-desa-pelatihan':
            case 'perangkat-desa-pelatihan':
            case 'bpd-pelatihan':
                $this->model = Sp3dPerangkatPelatihan::find()->joinWith(['kategoripelatihan', 'perangkat']);
            break;
            case 'bumdes-keuntungan':
            case 'bumdes-omset':
            case 'bumdes-modal':
                $this->model = BumdesModal::find();
            break;
            case 'bumdes-region':
            case 'bumdes-tahun':
                $this->model = Bumdes::find();
            break;
            case 'bumdes-unit-usaha':
                $this->model = BumdesUnitUsaha::find();
            break;
            
            default: exit('no choice'); break;
        }

        $this->pageToOpen = 'data-'.$this->reportType;
    }

    public function dataRegion()
    {
        if($this->data['kecamatan-input'] != '' && isset($this->data['kecamatan-input'])) {
            // 'kecamatan':
            $type = 'kecamatan';
            $kode = $this->data['kecamatan-input'];
            $this->model->andWhere('kd_kecamatan = :query')->addParams([':query'=> $kode]);
        } else if($this->data['kabupaten-input'] != '' && isset($this->data['kabupaten-input'])) {
           // 'kabupaten':
            $type = 'kabupaten';
            $kode = $this->data['kabupaten-input'];
            $this->model->andWhere('kd_kabupaten = :query')->addParams([':query'=> $kode]);
        } else {
            // 'provinsi'
            $kode = '-';
            $type = 'provinsi';
        }
    }

    public function dataSelect()
    {
        if($this->data['subjekval'] != '-' && $this->data['subjekval'] != null){
            switch($this->data['subjek']){
                case 'pelatihan':
                    switch ($this->data['objek']) {
                        case 'kepala-desa':
                        case 'perangkat-desa':
                            $tipe = 'desa';
                        break;
                        case 'bpd':
                            $tipe = 'bpd';
                        break;
                    }
                    $objekparam = ($this->data['jabatan'] != '-')?$this->data['jabatan']:$this->data['objek'];
                    $this->model->andWhere(['sp3d_perangkat_pelatihan.tipe' => $tipe, 'sp3d_perangkat_desa.jabatan' => $objekparam,'sp3d_master_jenis_pelatihan.id' => $this->data['subjekval']]);
                break;
                case 'lama-jabatan':
                    $this->model->select(['*', "(DATE_FORMAT(NOW(), '%Y') - tahun_awal) AS dummy_var"]);
                    $this->model->andFilterHaving(['dummy_var' => $this->data['subjekval']]);
                break;
                case 'jenis-kelamin':
                    $this->model->andWhere(['jenis_kelamin' => $this->data['subjekval']]);
                break;
                case 'pendidikan':
                    $this->model->andWhere(['pendidikan' => $this->data['subjekval']]);
                break;
                case 'umur':
                    $this->model->select(['*', "CASE WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 0 THEN '0'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 20 THEN '1-20'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 30 THEN '20-30'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 50 THEN '30-50'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) <= 70 THEN '50-70'
                     WHEN (DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(tanggal_lahir, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(tanggal_lahir, '00-%m-%d'))) > 70 THEN '70-up' END AS dummy_var"]);
                    $this->model->andFilterHaving(['dummy_var' => $this->data['subjekval']]);
                break;
                //BUMDESA
                case 'tahun':
                    $this->model->andWhere(['tahun_berdiri' => $this->data['subjekval']]);
                break;
                case 'keuntungan':
                case 'modal':
                case 'omset':
                    $this->model->select(['*', "CASE WHEN (".$this->data['subjek'].") <= 1000000 THEN '0-1jt'
                     WHEN (".$this->data['subjek'].") <= 5000000 THEN '1-5jt'
                     WHEN (".$this->data['subjek'].") <= 10000000 THEN '5-10jt'
                     WHEN (".$this->data['subjek'].") <= 50000000 THEN '10-50jt'
                     WHEN (".$this->data['subjek'].") > 50000000 THEN '50jt-keatas' END AS subjekval"]);
                    $this->model->andFilterHaving(['subjekval' => $this->data['subjekval']]);
                break;
                case 'region':
                break;
                case 'unit-usaha':
                    // SELECT count(*) AS hit, unit_usaha AS subjekval FROM bumdes_unit_usaha GROUP BY subjekval ORDER BY hit DESC LIMIT 20
                    $this->model->andWhere(['unit_usaha' => $this->data['subjekval']]);
                break;
            }
        } else {
            set_time_limit(60);
            ini_set('memory_limit', '-1');//perlu penanganan lanjut, misalkan ditampilkan secara ajax datatables
        }
    }

    public function result()
    {
        return $this->model;
    }
}
