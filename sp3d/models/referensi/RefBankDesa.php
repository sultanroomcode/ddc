<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Bank_Desa".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $Kd_Rincian
 * @property string $NoRek_Bank
 * @property string $Nama_Bank
 */
class RefBankDesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Bank_Desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa', 'Kd_Rincian'], 'required'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['Kd_Rincian'], 'string', 'max' => 12],
            [['NoRek_Bank'], 'string', 'max' => 20],
            [['Nama_Bank'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kd  Desa',
            'Kd_Rincian' => 'Kd  Rincian',
            'NoRek_Bank' => 'No Rek  Bank',
            'Nama_Bank' => 'Nama  Bank',
        ];
    }
}
