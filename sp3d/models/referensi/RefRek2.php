<?php
namespace sp3d\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Rek2".
 *
 * @property string $Akun
 * @property string $Kelompok
 * @property string $Nama_Kelompok
 */
class RefRek2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Rek2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Akun', 'Kelompok'], 'required'],
            [['Akun'], 'string', 'max' => 3],
            [['Kelompok'], 'string', 'max' => 4],
            [['Nama_Kelompok'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Akun' => 'Akun',
            'Kelompok' => 'Kelompok',
            'Nama_Kelompok' => 'Nama  Kelompok',
        ];
    }

    public function getRek3()//rekening
    {
        return $this->hasMany(RefRek3::className(), ['Kelompok' => 'Kelompok']);
    }
}
