<?php
namespace frontend\models\referensi;
use yii\db\ActiveRecord;
use frontend\models\KecamatanCount;
use Yii;

/**
 * This is the model class for table "Ref_Kecamatan".
 *
 * @property string $Kd_Kec
 * @property string $Nama_Kecamatan
 */
class RefKecamatan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $tahun;
    public static function tableName()
    {
        return 'Ref_Kecamatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Kec', 'kd_pum'], 'required'],
            [['Kd_Kec'], 'string', 'max' => 4],
            [['kd_pum'], 'string', 'max' => 12],
            [['Nama_Kecamatan'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Kec' => 'Kd  Kec',
            'Nama_Kecamatan' => 'Nama  Kecamatan',
        ];
    }

    public function getDesa()
    {
        return $this->hasMany(RefDesa::className(), ['Kd_Kec' => 'Kd_Kec']);   
    }

    public function getKecamatancount()
    {
        //please sett tahun
        return $this->hasOne(KecamatanCount::className(), ['kd_kecamatan' => 'Kd_Kec'])->onCondition(['tahun' => $this->tahun]);        
    }
}
