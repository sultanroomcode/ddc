<?php
namespace sp3d\models\referensi;

use Yii;

/**
 * This is the model class for table "ref_rekening_pembiayaan".
 *
 * @property string $id_kabupaten
 * @property string $kode
 * @property integer $tipe
 * @property string $uraian
 * @property string $keterangan
 */
class RefRekeningPembiayaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $kd_desa;
    public static function tableName()
    {
        return 'ref_rekening_pembiayaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kabupaten', 'kode'], 'required'],
            [['tipe'], 'integer'],
            [['id_kabupaten'], 'string', 'max' => 10],
            [['kode'], 'string', 'max' => 15],
            [['uraian', 'keterangan'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kabupaten' => 'Id Kabupaten',
            'kode' => 'Kode',
            'tipe' => 'Tipe',
            'uraian' => 'Uraian',
            'keterangan' => 'Keterangan',
        ];
    }

    public function getRab()
    {
        //table - id in tabel - id in this model
        return $this->hasOne(TaRAB::className(), ['Kd_Rincian' => 'kode']);
    }
}
