<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Korolari".
 *
 * @property string $Kd_Rincian
 * @property string $Kd_RekDB
 * @property string $Kd_RekKD
 * @property double $Jenis
 */
class RefKorolari extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Korolari';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Rincian'], 'required'],
            [['Jenis'], 'number'],
            [['Kd_Rincian', 'Kd_RekDB', 'Kd_RekKD'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Rincian' => 'Kd  Rincian',
            'Kd_RekDB' => 'Kd  Rek Db',
            'Kd_RekKD' => 'Kd  Rek Kd',
            'Jenis' => 'Jenis',
        ];
    }
}
