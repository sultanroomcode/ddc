<?php
namespace sp3d\models\referensi;

use Yii;

/**
 * This is the model class for table "ref_rekening_belanja".
 *
 * @property string $id_kabupaten
 * @property string $kode
 * @property integer $tipe
 * @property string $uraian
 * @property string $keterangan
 */
class RefRekeningBelanja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $kode_bidang, $kode_program;//hanya digunakan jika tipe != 1
    public static function tableName()
    {
        return 'ref_rekening_belanja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kabupaten', 'kode', 'hchild'], 'required'],
            [['tipe'], 'integer'],
            [['hchild'], 'safe'],
            [['id_kabupaten'], 'string', 'max' => 10],
            [['kode'], 'string', 'max' => 15],
            [['uraian', 'keterangan'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kabupaten' => 'Id Kabupaten',
            'kode' => 'Kode',
            'tipe' => 'Tipe',
            'uraian' => 'Uraian',
            'keterangan' => 'Keterangan',
        ];
    }
}
