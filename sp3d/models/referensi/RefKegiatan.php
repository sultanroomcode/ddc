<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Kegiatan".
 *
 * @property string $kode_bidang
 * @property string $kode
 * @property string $nama
 */
class RefKegiatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode'], 'required'],
            [['kode_bidang'], 'string', 'max' => 6],
            [['kode'], 'string', 'max' => 8],
            [['nama'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode_bidang' => 'Kode Bidang',
            'kode' => 'Kode Kegiatan',
            'nama' => 'Nama  Kegiatan',
        ];
    }


    public function getBidang()//for kecamatan up..
    {
        return $this->hasOne(RefBidang::className(), ['kode' => 'kode_bidang']);//kode is on bidang
    }
}
