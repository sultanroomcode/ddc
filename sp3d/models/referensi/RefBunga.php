<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Bunga".
 *
 * @property string $Kd_Bunga
 * @property string $Kd_Admin
 */
class RefBunga extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Bunga';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Bunga'], 'required'],
            [['Kd_Bunga', 'Kd_Admin'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Bunga' => 'Kd  Bunga',
            'Kd_Admin' => 'Kd  Admin',
        ];
    }
}
