<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Version".
 *
 * @property string $Versi
 * @property string $Tgl_Rilis
 */
class RefVersion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Version';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tgl_Rilis'], 'safe'],
            [['Versi'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Versi' => 'Versi',
            'Tgl_Rilis' => 'Tgl  Rilis',
        ];
    }
}
