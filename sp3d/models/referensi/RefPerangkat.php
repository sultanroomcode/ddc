<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Perangkat".
 *
 * @property string $Kode
 * @property string $Nama_Perangkat
 */
class RefPerangkat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Perangkat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode'], 'required'],
            [['Kode'], 'string', 'max' => 2],
            [['Nama_Perangkat'], 'string', 'max' => 35]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode' => 'Kode',
            'Nama_Perangkat' => 'Nama  Perangkat',
        ];
    }
}
