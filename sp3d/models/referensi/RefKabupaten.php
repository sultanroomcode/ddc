<?php
namespace frontend\models\referensi;
use yii\db\ActiveRecord;
use frontend\models\KabupatenCount;
use Yii;

/**
 * This is the model class for table "Ref_Kabupaten".
 *
 * @property string $Kd_Kec
 * @property string $Nama_Kecamatan
 */
class RefKabupaten extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $tahun;
    public static function tableName()
    {
        return 'Ref_Kabupaten';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kabupaten', 'kd_pum'], 'required'],
            [['kd_kabupaten'], 'string', 'max' => 4],
            [['kd_pum'], 'string', 'max' => 12],
            [['nama_kabupaten'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kd  Kec',
            'nama_kabupaten' => 'Nama  Kecamatan',
        ];
    }

    public function getDesa()
    {
        return $this->hasMany(RefDesa::className(), ['Kd_Kec' => 'Kd_Kec']);   
    }

    public function getKabupatencount()
    {
        //please sett tahun
        return $this->hasOne(KabupatenCount::className(), ['kd_kabupaten' => 'kd_kabupaten'])->onCondition(['tahun' => $this->tahun]);
    }
}
