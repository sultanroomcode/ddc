<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ta_Desa".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $Nm_Kades
 * @property string $Jbt_Kades
 * @property string $Nm_Sekdes
 * @property string $NIP_Sekdes
 * @property string $Jbt_Sekdes
 * @property string $Nm_Kaur_Keu
 * @property string $Jbt_Kaur_Keu
 * @property string $Nm_Bendahara
 * @property string $Jbt_Bendahara
 * @property string $No_Perdes
 * @property string $Tgl_Perdes
 * @property string $No_Perdes_PB
 * @property string $Tgl_Perdes_PB
 * @property string $No_Perdes_PJ
 * @property string $Tgl_Perdes_PJ
 * @property string $Alamat
 * @property string $Ibukota
 * @property string $Status
 * @property string $NPWP
 */
class TaDesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa'], 'required'],
            [['Tgl_Perdes', 'Tgl_Perdes_PB', 'Tgl_Perdes_PJ'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa', 'Status'], 'string', 'max' => 8],
            [['Nm_Kades', 'Jbt_Kades', 'Nm_Sekdes', 'Jbt_Sekdes', 'Nm_Kaur_Keu', 'Jbt_Kaur_Keu', 'Nm_Bendahara', 'Jbt_Bendahara', 'Alamat'], 'string', 'max' => 50],
            [['NIP_Sekdes'], 'string', 'max' => 18],
            [['No_Perdes', 'No_Perdes_PB', 'No_Perdes_PJ', 'Ibukota', 'NPWP'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kd  Desa',
            'Nm_Kades' => 'Nm  Kades',
            'Jbt_Kades' => 'Jbt  Kades',
            'Nm_Sekdes' => 'Nm  Sekdes',
            'NIP_Sekdes' => 'Nip  Sekdes',
            'Jbt_Sekdes' => 'Jbt  Sekdes',
            'Nm_Kaur_Keu' => 'Nm  Kaur  Keu',
            'Jbt_Kaur_Keu' => 'Jbt  Kaur  Keu',
            'Nm_Bendahara' => 'Nm  Bendahara',
            'Jbt_Bendahara' => 'Jbt  Bendahara',
            'No_Perdes' => 'No  Perdes',
            'Tgl_Perdes' => 'Tgl  Perdes',
            'No_Perdes_PB' => 'No  Perdes  Pb',
            'Tgl_Perdes_PB' => 'Tgl  Perdes  Pb',
            'No_Perdes_PJ' => 'No  Perdes  Pj',
            'Tgl_Perdes_PJ' => 'Tgl  Perdes  Pj',
            'Alamat' => 'Alamat',
            'Ibukota' => 'Ibukota',
            'Status' => 'Status',
            'NPWP' => 'Npwp',
        ];
    }
}
