<?php
namespace frontend\models\referensi;
use frontend\models\DesaCount;
use Yii;

/**
 * This is the model class for table "Ref_Desa".
 *
 * @property string $Kd_Kec
 * @property string $Kd_Desa
 * @property string $Nama_Desa
 */
class RefDesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $tahun;
    public static function tableName()
    {
        return 'Ref_Desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Desa', 'kd_pum'], 'required'],
            [['Kd_Kec'], 'string', 'max' => 4],
            [['kd_pum'], 'string', 'max' => 12],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['Nama_Desa'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kabupaten' => 'Kode Kabupaten',
            'Kd_Kec' => 'Kode Kecamatan',
            'Kd_Desa' => 'Kode Desa',
            'Nama_Desa' => 'Nama Desa',
        ];
    }

    public function getKecamatan()
    {
        return $this->hasOne(RefKecamatan::className(), ['Kd_Kec' => 'Kd_Kec']);        
    }

    public function getDesacount()
    {
        //please sett tahun
        return $this->hasOne(DesaCount::className(), ['kd_desa' => 'Kd_Desa'])->onCondition(['tahun' => $this->tahun]);        
    }
}
