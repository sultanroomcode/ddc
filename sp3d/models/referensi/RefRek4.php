<?php
namespace sp3d\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Rek4".
 *
 * @property string $Jenis
 * @property string $Obyek
 * @property string $Nama_Obyek
 * @property string $Peraturan
 */
class RefRek4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Rek4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Jenis', 'Obyek'], 'required'],
            [['Jenis'], 'string', 'max' => 7],
            [['Obyek'], 'string', 'max' => 10],
            [['Nama_Obyek'], 'string', 'max' => 100],
            [['Peraturan'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Jenis' => 'Jenis',
            'Obyek' => 'Obyek',
            'Nama_Obyek' => 'Nama  Obyek',
            'Peraturan' => 'Peraturan',
        ];
    }
}
