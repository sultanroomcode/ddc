<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_SBU".
 *
 * @property string $Kd_Rincian
 * @property string $Kode_SBU
 * @property string $NoUrut_SBU
 * @property string $Nama_SBU
 * @property double $Nilai
 * @property string $Satuan
 */
class RefSBU extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_SBU';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Rincian', 'Kode_SBU'], 'required'],
            [['Nilai'], 'number'],
            [['Kd_Rincian'], 'string', 'max' => 12],
            [['Kode_SBU'], 'string', 'max' => 20],
            [['NoUrut_SBU'], 'string', 'max' => 4],
            [['Nama_SBU'], 'string', 'max' => 50],
            [['Satuan'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Rincian' => 'Kd  Rincian',
            'Kode_SBU' => 'Kode  Sbu',
            'NoUrut_SBU' => 'No Urut  Sbu',
            'Nama_SBU' => 'Nama  Sbu',
            'Nilai' => 'Nilai',
            'Satuan' => 'Satuan',
        ];
    }
}
