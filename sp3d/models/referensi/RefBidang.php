<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Bidang".
 *
 * @property string $kode
 * @property string $nama
 */
class RefBidang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_bidang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode'], 'required'],
            [['kode'], 'string', 'max' => 6],
            [['nama'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode' => 'Kode Bidang',
            'nama' => 'Nama Bidang',
        ];
    }
    //tools
    public function setNewNumber()//how to do set new number on init
    {
        $mod = \frontend\models\referensi\RefBidang::find();
        if($mod->count() > 0){
            $counter = (int) $mod->max('kode');
            $counter++;
            $this->kode = sprintf('%02d', $counter);
        } else {
            $this->kode = sprintf('%02d', 1);  
        }        
    }

    public function getKegiatan()
    {
        return $this->hasMany(RefKegiatan::className(), ['kode_bidang' => 'kode']);
    }
}
