<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Setting".
 *
 * @property string $ID
 * @property string $Kd_Prov
 * @property string $Kd_Kab
 * @property integer $AutoSPD
 * @property integer $AutoSPP
 * @property integer $AutoSPM
 * @property integer $AutoSP2D
 * @property integer $AutoTBP
 * @property integer $AutoSTS
 * @property integer $AutoSPJ
 * @property integer $AutoBukti
 * @property integer $AutoSSP
 * @property integer $FiturSBU
 * @property string $AppVer
 */
class RefSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['AutoSPD', 'AutoSPP', 'AutoSPM', 'AutoSP2D', 'AutoTBP', 'AutoSTS', 'AutoSPJ', 'AutoBukti', 'AutoSSP', 'FiturSBU'], 'integer'],
            [['ID'], 'string', 'max' => 2],
            [['Kd_Prov', 'Kd_Kab'], 'string', 'max' => 4],
            [['AppVer'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Kd_Prov' => 'Kd  Prov',
            'Kd_Kab' => 'Kd  Kab',
            'AutoSPD' => 'Auto Spd',
            'AutoSPP' => 'Auto Spp',
            'AutoSPM' => 'Auto Spm',
            'AutoSP2D' => 'Auto Sp2 D',
            'AutoTBP' => 'Auto Tbp',
            'AutoSTS' => 'Auto Sts',
            'AutoSPJ' => 'Auto Spj',
            'AutoBukti' => 'Auto Bukti',
            'AutoSSP' => 'Auto Ssp',
            'FiturSBU' => 'Fitur Sbu',
            'AppVer' => 'App Ver',
        ];
    }
}
