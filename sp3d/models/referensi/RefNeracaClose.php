<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_NeracaClose".
 *
 * @property string $Kd_Rincian
 * @property string $Kelompok
 */
class RefNeracaClose extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_NeracaClose';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Rincian', 'Kelompok'], 'required'],
            [['Kd_Rincian'], 'string', 'max' => 12],
            [['Kelompok'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Rincian' => 'Kd  Rincian',
            'Kelompok' => 'Kelompok',
        ];
    }
}
