<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ta_RAB".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property double $Anggaran
 * @property double $AnggaranPAK
 * @property double $AnggaranStlhPAK
 */
class TaRAB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RAB';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa', 'Kd_Keg', 'Kd_Rincian'], 'required'],
            [['Anggaran', 'AnggaranPAK', 'AnggaranStlhPAK'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Rincian'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kd  Desa',
            'Kd_Keg' => 'Kd  Keg',
            'Kd_Rincian' => 'Kd  Rincian',
            'Anggaran' => 'Anggaran',
            'AnggaranPAK' => 'Anggaran Pak',
            'AnggaranStlhPAK' => 'Anggaran Stlh Pak',
        ];
    }

    public function getDesa()
    {
        //table - id in tabel - id in this model
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }
}
