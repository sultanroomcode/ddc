<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Bel_Operasional".
 *
 * @property string $ID_Keg
 */
class RefBelOperasional extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Bel_Operasional';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_Keg'], 'required'],
            [['ID_Keg'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_Keg' => 'Id  Keg',
        ];
    }
}
