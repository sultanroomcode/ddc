<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "ref_rekening".
 *
 * @property string $id_kabupaten
 * @property string $kode
 * @property integer $jenis
 * @property integer $tipe
 * @property string $uraian
 * @property string $keterangan
 */
class RefRekening extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_rekening';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kabupaten', 'kode'], 'required'],
            [['jenis', 'tipe'], 'integer'],
            [['id_kabupaten'], 'string', 'max' => 10],
            [['kode'], 'string', 'max' => 15],
            [['uraian', 'keterangan'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kabupaten' => 'Id Kabupaten',
            'kode' => 'Kode',
            'jenis' => 'Jenis',
            'tipe' => 'Tipe',
            'uraian' => 'Uraian',
            'keterangan' => 'Keterangan',
        ];
    }
}
