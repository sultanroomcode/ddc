<?php
namespace sp3d\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Rek3".
 *
 * @property string $Kelompok
 * @property string $Jenis
 * @property string $Nama_Jenis
 * @property integer $Formula
 */
class RefRek3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Rek3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kelompok', 'Jenis'], 'required'],
            [['Formula'], 'integer'],
            [['Kelompok'], 'string', 'max' => 4],
            [['Jenis'], 'string', 'max' => 7],
            [['Nama_Jenis'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kelompok' => 'Kelompok',
            'Jenis' => 'Jenis',
            'Nama_Jenis' => 'Nama  Jenis',
            'Formula' => 'Formula',
        ];
    }

    public function getRek4()//rekening
    {
        return $this->hasMany(RefRek4::className(), ['Jenis' => 'Jenis']);
    }
}
