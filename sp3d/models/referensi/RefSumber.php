<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Sumber".
 *
 * @property string $Kode
 * @property string $Nama_Sumber
 * @property integer $Urut
 */
class RefSumber extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Sumber';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode'], 'required'],
            [['Urut'], 'integer'],
            [['Kode'], 'string', 'max' => 3],
            [['Nama_Sumber'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode' => 'Kode',
            'Nama_Sumber' => 'Nama  Sumber',
            'Urut' => 'Urut',
        ];
    }
}
