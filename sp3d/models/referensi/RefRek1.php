<?php
namespace sp3d\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Rek1".
 *
 * @property string $Akun
 * @property string $Nama_Akun
 * @property string $NoLap
 */
class RefRek1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Rek1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Akun'], 'required'],
            [['Akun'], 'string', 'max' => 3],
            [['Nama_Akun'], 'string', 'max' => 100],
            [['NoLap'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Akun' => 'Akun',
            'Nama_Akun' => 'Nama  Akun',
            'NoLap' => 'No Lap',
        ];
    }

    public function getRek()//rekening
    {
        return $this->hasMany(RefRek2::className(), ['Akun' => 'Akun']);
    }
}
