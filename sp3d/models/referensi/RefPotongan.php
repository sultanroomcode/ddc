<?php

namespace frontend\models\referensi;

use Yii;

/**
 * This is the model class for table "Ref_Potongan".
 *
 * @property string $Kd_Rincian
 * @property string $Kd_Potongan
 */
class RefPotongan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Potongan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Rincian', 'Kd_Potongan'], 'required'],
            [['Kd_Rincian', 'Kd_Potongan'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Rincian' => 'Kd  Rincian',
            'Kd_Potongan' => 'Kd  Potongan',
        ];
    }
}
