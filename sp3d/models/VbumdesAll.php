<?php

namespace sp3d\models;

use Yii;

/**
 * This is the model class for table "vbumdes_all".
 *
 * @property string $ds_id
 * @property string $ds_nama
 * @property string $kc_id
 * @property string $kc_nama
 * @property string $kb_id
 * @property string $kb_nama
 * @property string $id_bumdes
 * @property string $nama_bumdes
 * @property string $tahun_berdiri
 * @property string $status_bumdes
 */
class VbumdesAll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vbumdes_all';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ds_id', 'kc_id', 'kb_id'], 'string', 'max' => 15],
            [['ds_nama', 'kc_nama', 'kb_nama'], 'string', 'max' => 255],
            [['id_bumdes'], 'string', 'max' => 11],
            [['nama_bumdes'], 'string', 'max' => 150],
            [['tahun_berdiri'], 'string', 'max' => 4],
            [['status_bumdes'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ds_id' => 'Ds ID',
            'ds_nama' => 'Ds Nama',
            'kc_id' => 'Kc ID',
            'kc_nama' => 'Kc Nama',
            'kb_id' => 'Kb ID',
            'kb_nama' => 'Kb Nama',
            'id_bumdes' => 'Id Bumdes',
            'nama_bumdes' => 'Nama Bumdes',
            'tahun_berdiri' => 'Tahun Berdiri',
            'status_bumdes' => 'Status Bumdes',
        ];
    }

    public function excellAttributes()
    {
        $arr = [];
        
        $arr = [
            ['attribute' => 'kb_id', 'label' => 'Kode Kabupaten'],
            ['attribute' => 'kb_nama', 'label' => 'Nama Kabupaten'],
            ['attribute' => 'kc_id', 'label' => 'Kode Kecamatan'],
            ['attribute' => 'kc_nama', 'label' => 'Nama Kecamatan'],
            ['attribute' => 'ds_id', 'label' => 'Kode Desa'],
            ['attribute' => 'ds_nama', 'label' => 'Nama Desa'],
            ['attribute' => 'nama_bumdes']
        ];

        return $arr;
    }
}
