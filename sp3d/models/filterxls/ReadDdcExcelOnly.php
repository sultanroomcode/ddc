<?php
namespace sp3d\models\filterxls;

class ReadDdcExcelOnly implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter {
	private $min = 1;
    private $max = 10;
	private $range = ['A', 'B', 'C'];

	public function setMinMax($min, $max) {
		$this->min = $min;
		$this->max = $max;
	}

    public function getMin()
    {
        return $this->min;
    }

    public function getMax()
    {
        return $this->max;
    }

    public function setRange($arr=null)
    {
        if($arr != null){
            $this->range = $arr;
        }        
    }

	public function readCell($column, $row, $worksheetName = '') {
        //  Read rows 1 to 7 and columns A to E only
        // $min = 5649;
        // $max = 5959;
        if ($row >= $this->min && $row <= $this->max) {
            if (in_array($column,$this->range)) {
                return true;
            }
        }
        return false;
    }
}