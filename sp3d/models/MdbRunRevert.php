<?php
namespace sp3d\models;

use sp3d\models\transaksi;
use Yii;
use yii\base\Exception;
use yii\base\Model;
/**
 * This is the model class for table "desa_count".
 *
 * @property string $tahun
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $dana_anggaran
 * @property integer $dana_pencairan
 * @property integer $dana_kegiatan
 */
class MdbRunRevert extends Model
{
    use \sp3d\models\TraitFormat;
    use \sp3d\models\TraitLog;
    
    public $user, $id_mdb, $ipremote;
    protected $ipext = '/sp3dclient/deleteFile.php';

    public function rules()
    {
        return [
            [['user', 'id_mdb'], 'required']
        ];
    }


    public function initial($user, $id_mdb, $ipremote)
    {
        $this->user = $user;
        $this->id_mdb = $id_mdb;
        $this->ipremote = $ipremote;
    }
    
    public function doRemove()
    {
        $model = transaksi\TaMdbUpload::findOne(['user' => $this->user, 'id' => $this->id_mdb]);
        $model->delete();
        $this->writeLog($model->user, 'user telah menghapus file mdb');
        return true;
    }

    public function doRevert()
    {
        $model = transaksi\TaMdbUpload::findOne(['user' => $this->user, 'id' => $this->id_mdb]);
        // var_dump($model);
        //remove inserted data
        $this->removeInsertedData($model->user, $model->tahun, $model->id);//fix1
        //then recounting
        //remove file
        $this->removeFile($model->user, $model->tahun, $model->nama_file);//fix1
        // var_dump($this->removeFile($model->user, $model->tahun, $model->nama_file));
        // $this->removeFileRemote($model->user, $model->tahun, $model->nama_file);
        //last do is remove query
        $model->delete();
        $this->writeLog($model->user, 'user telah menghapus file mdb dan seluruh data rab desa');
        return true;
    }

    protected function removeInsertedData($user, $tahun, $imp_id){
        // return collective counting;
        // disable first
        /*
        DELETE FROM Ta_Anggaran WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_AnggaranLog WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_AnggaranRinci WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Desa WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_JurnalUmum WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_JurnalUmumRinci WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Mutasi WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Pajak WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_PajakRinci WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Pemda WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Pencairan WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Perangkat WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SaldoAwal WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPJ WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPJBukti WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPJPot WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPJRinci WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPJSisa WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPP WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPPBukti WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPPPot WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_SPPRinci WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_STS WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_STSRinci WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_TBP WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_TBPRinci WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM ta_tpk WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Triwulan WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_TriwulanArsip WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        
        $runsql = "
        DELETE FROM Ta_Kegiatan WHERE user_id = '$user' AND imp_id = '$imp_id';
        DELETE FROM Ta_Bidang WHERE user_id = '$user' AND Tahun = '$tahun' AND imp_id = '$id';
        DELETE FROM Ta_Kegiatan WHERE user_id = '$user' AND imp_id = '$imp_id';
        DELETE FROM Ta_RAB WHERE user_id = '$user' AND imp_id = '$imp_id';
        DELETE FROM Ta_RABRinci WHERE user_id = '$user' AND imp_id = '$imp_id';
        ";
        */

        $runsql = "
        DELETE FROM Ta_Bidang WHERE user_id = '$user' AND imp_id = '$imp_id';
        DELETE FROM Ta_Kegiatan WHERE user_id = '$user' AND imp_id = '$imp_id';
        DELETE FROM Ta_RAB WHERE user_id = '$user' AND imp_id = '$imp_id';
        DELETE FROM Ta_RABRinci WHERE user_id = '$user' AND imp_id = '$imp_id';
        ";
        // var_dump($runsql);
        $model = Yii::$app->db->createCommand($runsql);

        $status = [];
        $status['state'] = true;
        $status['message'] = $model->execute();

        return $status;
    }

    protected function removeFile($user, $tahun, $nama_file){
        $dbmde = $user.'/db/'.$nama_file;
        $file_name_with_full_path = './userfile/'.$dbmde;

        $status = [];
        $status['state'] = false;
        $status['message'] = 'Gagal menghapus file';
        if(file_exists($file_name_with_full_path)){
            unlink($file_name_with_full_path);
            $status['state'] = true;
            $status['message'] = 'Berhasil menghapus file';
        }

        return $status;
    }

    protected function removeFileRemote($user, $tahun, $nama_file){
        $url = $this->ipremote.$this->ipext.'?user='.$user.'&tahun='.$tahun.'&namafile='.$nama_file;
        $ch = curl_init();
        // var_dump($parameter);
        // http://xx.xx.xx.xx/sp3dclient/deleteFile.php?path=... //example
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $output = curl_exec($ch);
        if($output != ''){
            $status = json_decode($output, true);//when true will return assoc arry
            // echo $status->message;
        } else {
            $status = null;
        }
        // close curl resource to free up system resources
        curl_close($ch);
        // var_dump($url);
        return $status;
    }
}
