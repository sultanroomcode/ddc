<?php
$tbl['Ta_Anggaran'] = ['KdPosting','Tahun','KURincianSD','Kd_Rincian','RincianSD','Anggaran','AnggaranPAK','AnggaranStlhPAK','Belanja','Kd_Keg','SumberDana','Kd_Desa','TglPosting'];

$tbl['Ta_AnggaranLog'] = ['KdPosting','Tahun','Kd_Desa','No_Perdes','TglPosting','UserID','Kunci'];

$tbl['Ta_AnggaranRinci'] = ['KdPosting','Tahun','Kd_Desa','Kd_Keg','Kd_Rincian','Kd_SubRinci','No_Urut','Uraian','SumberDana','JmlSatuan','HrgSatuan','Satuan','Anggaran','JmlSatuanPAK','HrgSatuanPAK','AnggaranStlhPAK','AnggaranPAK'];

$tbl['Ta_Bidang'] = ['Tahun','Kd_Desa','Kd_Bid','Nama_Bidang'];

$tbl['Ta_Desa'] = ['Tahun','Kd_Desa','Nm_Kades','Jbt_Kades','Nm_Sekdes','NIP_Sekdes','Jbt_Sekdes','Nm_Kaur_Keu','Jbt_Kaur_Keu','Nm_Bendahara', 'Jbt_Bendahara', 'No_Perdes','Tgl_Perdes','No_Perdes_PB','Tgl_Perdes_PB','No_Perdes_PJ','Tgl_Perdes_PJ','Alamat','Ibukota','Status','NPWP'];

$tbl['Ta_JurnalUmum'] = ['Tahun','KdBuku','Kd_Desa','Tanggal','JnsBukti','NoBukti','Keterangan','DK','Debet','Kredit','Jenis','Posted'];

$tbl['Ta_JurnalUmumRinci'] = ['Tahun','NoBukti','Kd_Keg','RincianSD','NoID','Kd_Desa','Akun','Kd_Rincian','Sumberdana','DK','Debet','Kredit'];

$tbl['Ta_Kegiatan'] = ['Tahun','Kd_Desa','Kd_Bid','Kd_Keg','ID_Keg','Nama_Kegiatan','Pagu','Pagu_PAK','Nm_PPTKD','NIP_PPTKD','Lokasi','Waktu','Keluaran','Sumberdana'];

$tbl['Ta_Mutasi'] = ['Tahun','Kd_Desa','No_Bukti','Tgl_Bukti','Keterangan','Kd_Bank','Kd_Rincian','Kd_Keg','Sumberdana','Kd_Mutasi','Nilai'];

$tbl['Ta_Pajak'] = ['Tahun','Kd_Desa','No_SSP','Tgl_SSP','Keterangan','Nama_WP','Alamat_WP','NPWP','Kd_MAP','Nm_Penyetor','Jn_Transaksi','Kd_Rincian','Jumlah','KdBayar'];

$tbl['Ta_PajakRinci'] = ['Tahun','Kd_Desa','No_SSP','No_Bukti','Kd_Rincian','Nilai'];


$tbl['Ta_Pemda'] = ['Tahun','Kd_Prov','Kd_Kab','Nama_Pemda','Nama_Provinsi','Ibukota','Alamat','Nm_Bupati','Jbt_Bupati','Logo','C_Kode','C_Pemda','C_Data'];

$tbl['Ta_Pencairan'] = ['Tahun','No_Cek','No_SPP','Tgl_Cek','Kd_Desa','Keterangan','Jumlah','Potongan','KdBayar'];


$tbl['Ta_Perangkat'] = ['Tahun','Kd_Desa','Kd_Jabatan','No_ID','Nama_Perangkat','Alamat_Perangkat','Nomor_HP','Rek_Bank','Nama_Bank'];

$tbl['Ta_RAB'] = ['Tahun','Kd_Desa','Kd_Keg','Kd_Rincian','Anggaran','AnggaranPAK','AnggaranStlhPAK'];

$tbl['Ta_RABRinci'] = ['Tahun','Kd_Desa','Kd_Keg','Kd_Rincian','Kd_SubRinci','No_Urut','SumberDana','Uraian','Satuan','JmlSatuan','HrgSatuan','Anggaran','JmlSatuanPAK','HrgSatuanPAK','AnggaranStlhPAK','AnggaranPAK','Kode_SBU'];

$tbl['Ta_RABSub'] = ['Tahun','Kd_Desa','Kd_Keg','Kd_Rincian','Kd_SubRinci','Nama_SubRinci','Anggaran','AnggaranPAK','AnggaranStlhPAK'];

$tbl['Ta_RPJM_Bidang'] = ['Kd_Desa','Kd_Bid','Nama_Bidang'];

$tbl['Ta_RPJM_Kegiatan'] = ['Kd_Desa','Kd_Bid','Kd_Keg','ID_Keg','Nama_Kegiatan','Lokasi','Keluaran','Kd_Sas','Sasaran','Tahun1','Tahun2','Tahun3','Tahun4','Tahun5','Tahun6','Swakelola','Kerjasama','Pihak_Ketiga','Sumberdana'];

$tbl['Ta_RPJM_Misi'] = ['ID_Misi','Kd_Desa','ID_Visi','No_Misi','Uraian_Misi'];

$tbl['Ta_RPJM_Pagu_Indikatif'] = ['Kd_Desa','Kd_Keg','Kd_Sumber','Tahun1','Tahun2','Tahun3','Tahun4','Tahun5','Tahun6','Pola'];

$tbl['Ta_RPJM_Pagu_Tahunan'] = ['Kd_Desa','Kd_Keg','Kd_Tahun','Kd_Sumber','Biaya','Volume','Satuan','Lokasi_Spesifik','Jml_Sas_Pria','Jml_Sas_Wanita','Jml_Sas_ARTM','Waktu','Mulai','Selesai','Pola_Kegiatan','Pelaksana'];

$tbl['Ta_RPJM_Sasaran'] = ['ID_Sasaran','Kd_Desa','ID_Tujuan','No_Sasaran','Uraian_Sasaran'];

$tbl['Ta_RPJM_Tujuan'] = ['ID_Tujuan','Kd_Desa','ID_Misi','No_Tujuan','Uraian_Tujuan'];

$tbl['Ta_RPJM_Visi'] = ['ID_Visi','Kd_Desa','No_Visi','Uraian_Visi','TahunA','TahunN'];

$tbl['Ta_SPJ'] = ['Tahun','No_SPJ','Tgl_SPJ','Kd_Desa','No_SPP','Keterangan','Jumlah','Potongan','Status'];

$tbl['Ta_SPJBukti'] = ['Tahun','No_SPJ','Kd_Keg','Kd_Rincian','No_Bukti','Tgl_Bukti','Sumberdana','Kd_Desa','Nm_Penerima','Alamat','Rek_Bank','Nm_Bank','NPWP','Keterangan','Nilai'];


$tbl['Ta_SPJPot'] = ['Tahun','Kd_Desa','No_SPJ','Kd_Keg','No_Bukti','Kd_Rincian','Nilai'];

$tbl['Ta_SPJRinci'] = ['Tahun','No_SPJ','Kd_Keg','Kd_Rincian','Sumberdana','Kd_Desa','No_SPP','JmlCair','Nilai','Sisa'];

$tbl['Ta_SPJSisa'] = ['Tahun','Kd_Desa','No_Bukti','Tgl_Bukti','No_SPJ','Tgl_SPJ','No_SPP','Tgl_SPP','Kd_Keg','Keterangan','Nilai'];

$tbl['Ta_SPP'] = ['Tahun','No_SPP','Tgl_SPP','Jn_SPP','Kd_Desa','Keterangan','Jumlah','Potongan','Status'];

$tbl['Ta_SPPBukti'] = ['Tahun','Kd_Desa','No_SPP','Kd_Keg','Kd_Rincian','Sumberdana','No_Bukti','Tgl_Bukti','Nm_Penerima','Alamat','Rek_Bank','Nm_Bank','NPWP','Keterangan','Nilai'];

$tbl['Ta_SPPPot'] = ['Tahun','Kd_Desa','No_SPP','Kd_Keg','No_Bukti','Kd_Rincian','Nilai'];

$tbl['Ta_SPPRinci'] = ['Tahun','Kd_Desa','No_SPP','Kd_Keg','Kd_Rincian','Sumberdana','Nilai'];

$tbl['Ta_STS'] = ['Tahun','No_Bukti','Tgl_Bukti','Kd_Desa','Uraian','NoRek_Bank','Nama_Bank','Jumlah','Nm_Bendahara','Jbt_Bendahara'];

$tbl['Ta_STSRinci'] = ['Tahun','Kd_Desa','No_Bukti','No_TBP','Uraian','Nilai'];

$tbl['Ta_SaldoAwal'] = ['Tahun','Kd_Desa','Kd_Rincian','Jenis','Anggaran','Debet','Kredit','Tgl_Bukti'];

$tbl['Ta_TBP'] = ['Tahun','No_Bukti','Tgl_Bukti','Kd_Desa','Uraian','Nm_Penyetor','Alamat_Penyetor','TTD_Penyetor','NoRek_Bank','Nama_Bank','Jumlah','Nm_Bendahara','Jbt_Bendahara','Status','KdBayar','Ref_Bayar'];

$tbl['Ta_TBPRinci'] = ['Tahun','No_Bukti','Kd_Desa','Kd_Keg','Kd_Rincian','RincianSD','SumberDana','Nilai'];
$tbl['Ta_Triwulan'] = ['KURincianSD','Tahun','Sifat','SumberDana','Kd_Desa','Kd_Keg','Kd_Rincian','Anggaran','AnggaranPAK','Tw1Rinci','Tw2Rinci','Tw3Rinci','Tw4Rinci','KunciData'];

$tbl['Ta_TriwulanArsip'] = ['KdPosting','KURincianSD','Tahun','Sifat','SumberDana','Kd_Desa','Kd_Keg','Kd_Rincian','Anggaran','AnggaranPAK','Tw1Rinci','Tw2Rinci','Tw3Rinci','Tw4Rinci','KunciData'];

$tbl['Ta_UserDesa'] = ['UserID','Kd_Kec','Kd_Desa'];

$tbl['Ta_UserLog'] = ['TglLogin','Komputer','UserID','Tahun','AplVer','DbVer'];