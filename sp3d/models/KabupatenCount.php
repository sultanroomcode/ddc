<?php
namespace sp3d\models;

use Yii;

/**
 * This is the model class for table "kabupaten_count".
 *
 * @property string $tahun
 * @property string $kd_provinsi
 * @property string $kd_kabupaten
 * @property integer $dana_anggaran
 * @property integer $dana_pencairan
 * @property integer $dana_kegiatan
 * @property integer $dana_mutasi
 * @property integer $dana_pajak
 * @property string $dana_rab
 * @property integer $dana_spj
 * @property integer $dana_spp
 * @property integer $dana_sts
 * @property integer $dana_tbp
 * @property integer $data_pencairan
 * @property integer $data_anggaran
 * @property integer $data_kegiatan
 * @property integer $data_bidang
 * @property integer $data_mutasi
 * @property integer $data_pajak
 * @property integer $data_rab
 * @property integer $data_spj
 * @property integer $data_spp
 * @property integer $data_sts
 * @property integer $data_tbp
 */
class KabupatenCount extends \yii\db\ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kabupaten_count';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'kd_provinsi', 'kd_kabupaten'], 'required'],
            [['dana_anggaran', 'dana_pencairan', 'dana_kegiatan','dana_mutasi', 'dana_pajak','dana_rab', 'dana_spj', 'dana_spp', 'dana_sts', 'dana_tbp', 'dana_pendapatan', 'dana_penerimaan', 'dana_pengeluaran', 'data_anggaran', 'data_pencairan', 'data_kegiatan','data_bidang', 'data_mutasi', 'data_pajak','data_rab', 'data_spj', 'data_spp', 'data_sts', 'data_tbp', 'data_pendapatan','data_penerimaan', 'data_pengeluaran'], 'default', 'value' => 0, 'isEmpty' => true, 'when' => function($model){ $model->isNewRecord; } ],

            [['dana_anggaran', 'dana_pencairan', 'dana_kegiatan','dana_mutasi', 'dana_pajak','dana_rab', 'dana_spj', 'dana_spp', 'dana_sts', 'dana_tbp', 'dana_pendapatan', 'dana_penerimaan', 'dana_pengeluaran'], 'integer'],

            [['data_anggaran', 'data_pencairan', 'data_kegiatan','data_bidang', 'data_mutasi', 'data_pajak','data_rab', 'data_spj', 'data_spp', 'data_sts', 'data_tbp', 'data_pendapatan','data_penerimaan', 'data_pengeluaran'], 'integer'],
            [['tahun'], 'string', 'max' => 4],
            [['kd_provinsi','kd_kabupaten'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun' => 'Tahun',
            'kd_provinsi' => 'Kd Provinsi',
            'kd_kabupaten' => 'Kd Kabupaten',
            'dana_anggaran' => 'Dana Anggaran',
            'dana_pencairan' => 'Dana Pencairan',
            'dana_kegiatan' => 'Dana Kegiatan',
            'dana_mutasi' => 'Dana Mutasi',
            'dana_pajak' => 'Dana Pajak',
            'dana_rab' => 'Dana Rab',
            'dana_spj' => 'Dana Spj',
            'dana_spp' => 'Dana Spp',
            'dana_sts' => 'Dana Sts',
            'dana_tbp' => 'Dana Tbp',
            'data_pencairan' => 'Data Pencairan',
            'data_anggaran' => 'Data Anggaran',
            'data_kegiatan' => 'Data Kegiatan',
            'data_bidang' => 'Data Bidang',
            'data_mutasi' => 'Data Mutasi',
            'data_pajak' => 'Data Pajak',
            'data_rab' => 'Data Rab',
            'data_spj' => 'Data Spj',
            'data_spp' => 'Data Spp',
            'data_sts' => 'Data Sts',
            'data_tbp' => 'Data Tbp',
        ];
    }

    public function getLabel()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
}
