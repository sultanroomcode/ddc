<?php
namespace sp3d\models\transaksi;

use Yii;

/**
 * This is the model class for table "Ta_UserID".
 *
 * @property string $UserID
 * @property string $Nama
 * @property string $Password
 * @property string $Level
 * @property string $Keterangan
 */
class TaUserID extends \yii\db\ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_UserID';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID'], 'required'],
            [['UserID'], 'string', 'max' => 20],
            [['Nama'], 'string', 'max' => 25],
            [['Password', 'Level'], 'string', 'max' => 15],
            [['Keterangan'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UserID' => 'User ID',
            'Nama' => 'Nama',
            'Password' => 'Password',
            'Level' => 'Level',
            'Keterangan' => 'Keterangan',
        ];
    }
}
