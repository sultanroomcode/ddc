<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_SPPBukti".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_SPP
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property string $Sumberdana
 * @property string $No_Bukti
 * @property string $Tgl_Bukti
 * @property string $Nm_Penerima
 * @property string $Alamat
 * @property string $Rek_Bank
 * @property string $Nm_Bank
 * @property string $NPWP
 * @property string $Keterangan
 * @property double $Nilai
 */
class TaSPPBukti extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPPBukti';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_Bukti'], 'required'],
            [['Tgl_Bukti'], 'safe'],
            [['imp_id'], 'safe'],
            [['Nilai'], 'number'],
            [['Tahun', 'Sumberdana'], 'string', 'max' => 4],
            [['Kd_Desa', 'Kd_Rincian'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_SPP', 'No_Bukti'], 'string', 'max' => 30],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Nm_Penerima', 'Nm_Bank'], 'string', 'max' => 40],
            [['Alamat'], 'string', 'max' => 50],
            [['Rek_Bank', 'NPWP'], 'string', 'max' => 20],
            [['Keterangan'], 'string', 'max' => 150],
            [['No_Bukti'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_SPP' => 'No. Spp',
            'Kd_Keg' => 'Kode Keg',
            'Kd_Rincian' => 'Kode Rincian',
            'Sumberdana' => 'Sumberdana',
            'No_Bukti' => 'No. Bukti',
            'Tgl_Bukti' => 'Tanggal Bukti',
            'Nm_Penerima' => 'Nama Penerima',
            'Alamat' => 'Alamat',
            'Rek_Bank' => 'Rekening Bank',
            'Nm_Bank' => 'Nama Bank',
            'NPWP' => 'NPWP',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
            'Keterangan' => 'Keterangan',
            'Nilai' => 'Nilai',
        ];
    }
}
