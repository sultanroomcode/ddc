<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_RPJM_Visi".
 *
 * @property string $ID_Visi
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_Visi
 * @property string $Uraian_Visi
 * @property string $TahunA
 * @property string $TahunN
 */
class TaRPJMVisi extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Visi';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_Visi', 'Kd_Desa', 'kd_kecamatan'], 'required'],
            [['ID_Visi'], 'string', 'max' => 10],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_Visi'], 'string', 'max' => 2],
            [['Uraian_Visi'], 'string', 'max' => 250],
            [['TahunA', 'TahunN'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_Visi' => 'ID Visi',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_Visi' => 'No. Visi',
            'Uraian_Visi' => 'Uraian Visi',
            'TahunA' => 'Tahun Awal',
            'TahunN' => 'Tahun Akhir',

            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function setNewNumber()
    {
        $mod = \sp3d\models\transaksi\TaRPJMVisi::find()->where(['Kd_Desa' => $this->Kd_Desa, 'kd_kecamatan' => $this->kd_kecamatan]);
        if($mod->count() > 0){
            $counter = (int) $mod->max('No_Visi');
            $counter++;
            $this->No_Visi = sprintf('%02d', $counter);
        } else {
            $this->No_Visi = sprintf('%02d', 1);  
        }        
    }

    public function getMisi()//for kecamatan up..
    {
        return $this->hasMany(TaRPJMMisi::className(), ['ID_Visi' => 'ID_Visi']);
    }
}
