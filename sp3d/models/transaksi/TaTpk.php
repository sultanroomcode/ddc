<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "ta_tpk".
 *
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property string $no_urut
 * @property string $nama_tpk
 * @property string $alamat_tpk
 * @property string $no_hp_tpk
 */
class TaTpk extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ta_tpk';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'no_urut', 'nama_tpk', 'alamat_tpk', 'no_hp_tpk', 'tipe_tpk'], 'required'],
            [['kd_kecamatan', 'kd_desa'], 'string', 'max' => 8],
            [['no_urut'], 'string', 'max' => 3],
            [['imp_id'], 'safe'],
            [['nama_tpk', 'alamat_tpk'], 'string', 'max' => 255],
            [['no_hp_tpk'], 'string', 'max' => 30],
            [['tipe_tpk'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_desa' => 'Kd Desa',
            'no_urut' => 'No Urut',
            'nama_tpk' => 'Nama Tpk',
            'alamat_tpk' => 'Alamat Tpk',
            'no_hp_tpk' => 'No Hp Tpk',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function setNewNumber()
    {
        $mod = \sp3d\models\transaksi\TaTpk::find()->where(['kd_desa' => $this->kd_desa, 'kd_kecamatan' => $this->kd_kecamatan]);
        if($mod->count() > 0){
            $counter = (int) $mod->max('no_urut');
            $counter++;
            $this->no_urut = sprintf('%03d', $counter);
        } else {
            $this->no_urut = sprintf('%03d', 1);  
        }        
    }
}
