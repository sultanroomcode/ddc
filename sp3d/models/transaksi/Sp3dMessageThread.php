<?php

namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "sp3d_message_thread".
 *
 * @property string $id
 * @property string $title_header
 * @property string $body_content
 * @property string $type
 * @property string $from
 * @property string $to
 * @property integer $status
 * @property string $created_at
 * @property string $last_inserted_at
 */
class Sp3dMessageThread extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp3d_message_thread';
    }

    public function behaviors()
    {
         return [
             // [
             //     'class' => BlameableBehavior::className(),
             //     'createdByAttribute' => 'user',
             //     'updatedByAttribute' => 'user',
             // ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'last_inserted_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,//['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from', 'to'], 'required'],
            [['body_content'], 'string'],
            [['status'], 'integer'],
            [['created_at', 'last_inserted_at'], 'safe'],
            [['id'], 'string', 'max' => 20],
            [['title_header'], 'string', 'max' => 255],
            [['type', 'from', 'to'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_header' => 'Judul',
            'body_content' => 'Konten',
            'type' => 'Type',
            'from' => 'From',
            'to' => 'To',
            'status' => 'Status',
            'created_at' => 'Created At',
            'last_inserted_at' => 'Last Inserted At',
        ];
    }
}
