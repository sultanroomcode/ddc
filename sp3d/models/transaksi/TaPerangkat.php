<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_Perangkat".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Jabatan
 * @property string $No_ID
 * @property string $Nama_Perangkat
 * @property string $Alamat_Perangkat
 * @property string $Nomor_HP
 * @property string $Rek_Bank
 * @property string $Nama_Bank
 */
class TaPerangkat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Perangkat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa', 'Kd_Jabatan', 'No_ID'], 'required'],
            [['Tahun'], 'string', 'max' => 4],
            [['imp_id'], 'safe'],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Jabatan', 'No_ID'], 'string', 'max' => 2],
            [['Nama_Perangkat', 'Nama_Bank'], 'string', 'max' => 40],
            [['Alamat_Perangkat'], 'string', 'max' => 50],
            [['Nomor_HP', 'Rek_Bank'], 'string', 'max' => 20]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Jabatan' => 'Kode Jabatan',
            'No_ID' => 'No. ID',
            'Nama_Perangkat' => 'Nama Perangkat',
            'Alamat_Perangkat' => 'Alamat Perangkat',
            'Nomor_HP' => 'Nomor HP',
            'Rek_Bank' => 'Rekening Bank',
            'Nama_Bank' => 'Nama Bank',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
