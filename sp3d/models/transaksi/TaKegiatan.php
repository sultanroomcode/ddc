<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;
/**
 * This is the model class for table "Ta_Kegiatan".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Bid
 * @property string $Kd_Keg
 * @property string $ID_Keg
 * @property string $Nama_Kegiatan
 * @property double $Pagu
 * @property double $Pagu_PAK
 */
class TaKegiatan extends ActiveRecord
{

    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public $PaguMask, $Pagu_PAKMask;
    public static function tableName()
    {
        return 'Ta_Kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Keg'], 'required'],
            [['Pagu', 'Pagu_PAK'], 'number'],
            [['imp_id'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],            
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['f_verified'], 'integer'],
            [['Kd_Bid', 'ID_Keg'], 'string', 'max' => 10],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Sumberdana'], 'string', 'max' => 100],
            [['Nama_Kegiatan'], 'string', 'max' => 200],
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Bid' => 'Kode Bidang',
            'Kd_Keg' => 'Kode Kegiatan',
            'ID_Keg' => 'ID Kegiatan',
            'Nama_Kegiatan' => 'Nama Kegiatan',
            'Pagu' => 'Pagu',
            'PaguMask' => 'Pagu',
            'Pagu_PAK' => 'Pagu PAK (Peninjauan Anggaran Kembali)',
            'Pagu_PAKMask' => 'Pagu PAK (Peninjauan Anggaran Kembali)',
            'Sumberdana' => 'Sumber Dana',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function insertRab(){
        $mo = new TaRAB();
        $mo->Tahun = $this->Tahun;
        $mo->Kd_Desa = $this->Kd_Desa;
        $mo->kd_kecamatan = $this->kd_kecamatan;
        $mo->Kd_Keg = $this->Kd_Keg;
        $mo->Kd_Rincian = $this->ID_Keg;
        $mo->kegiatan = 'belanja';
        $mo->sumberdana = '-';
        $mo->Anggaran = $mo->AnggaranPAK = $mo->AnggaranStlhPAK = 0;
        return $mo->save();
    }

    public function updatingKegiatanDesaCount()
    {
        $total = \sp3d\models\transaksi\TaKegiatan::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_kegiatan', 'dana' => 'dana_kegiatan']);
        $mod->dana_kegiatan = $total->sum('Pagu');
        $mod->data_kegiatan = $total->count();
        $mod->save(false);
    }

    public function getSub()//for kecamatan up..
    {
        return $this->hasOne(TaKegiatanSub::className(), ['Tahun' => 'Tahun', 'Kd_Desa' => 'Kd_Desa', 'Kd_Keg' => 'Kd_Keg', 'Kd_Bid' => 'Kd_Bid']);
    }

    public function getBidang()//for kecamatan up..
    {
        return $this->hasOne(TaBidang::className(), ['Kd_Bid' => 'Kd_Bid']);
    }

    public function getRab()//for kecamatan up..
    {
        return $this->hasOne(TaRAB::className(), ['Kd_Desa' => 'Kd_Desa', 'Kd_Keg' => 'Kd_Keg', 'Tahun' => 'Tahun']);
    }

    public function getBelanjaRinci()
    {
        return $this->hasMany(TaRABRinci::className(), ['Kd_Desa' => 'Kd_Desa', 'Tahun' => 'Tahun', 'Kd_Keg' => 'Kd_Keg']); 
    }

    //events
    public function afterDelete()
    {
        $this->updatingKegiatanDesaCount();
    }

    public function afterSave($ins, $old)
    {
        if($ins){
            $this->insertRab();
        } else {
            //update
        }
        $this->updatingKegiatanDesaCount();
        
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        $mo = TaRAB::findOne(['Kd_Desa' => $this->Kd_Desa, 'Kd_Keg' => $this->Kd_Keg,'Kd_Rincian' => $this->ID_Keg, 'Tahun' => $this->Tahun]);
        if($mo != null){
            $mo->delete();
        }

        foreach (TaKegiatanSub::find()->where(['Tahun' => $this->Tahun, 'Kd_Keg' => $this->Kd_Keg])->all() as $rr) {
            $rr->delete();
        }
        
        return true;
    }
}
