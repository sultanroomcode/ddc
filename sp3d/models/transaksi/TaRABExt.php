<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\referensi\RefRekeningBelanja;
use sp3d\models\referensi\RefRekeningKegiatan;
use sp3d\models\referensi\RefRekeningPendapatan;
use sp3d\models\referensi\RefRekeningPembiayaan;
use sp3d\models\DesaCount;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_RAB".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property double $Anggaran
 * @property double $AnggaranPAK
 * @property double $AnggaranStlhPAK
 */
class TaRABExt extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public $pendapatan_x, $pendapatan_xmask;
    public $kode_bidang, $kode_program, $kode_kegiatan_rinci, $kode_rincian;
    public $kode_bidang_kegiatan, $kode_program_kegiatan;
    public static function tableName()
    {
        return 'Ta_RAB_Ext';
        //tabel ini menampung kode rekening kode rekening yang read only (tidak bisa di edit secara langsung)
        //hanya bertujuan untuk laporan saja
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pendapatan_x'], 'required', 'when' => function($model){
                return $model->kegiatan !== 'belanja' && $model->isNewRecord;
            }, 'whenClient' => "function(attribute, value){ return $('#tarab-kegiatan').val() !== 'belanja'; }"],
            [['Tahun', 'Kd_Desa', 'Kd_Keg', 'Kd_Rincian','kegiatan'], 'required'],
            [['Anggaran', 'AnggaranPAK', 'AnggaranStlhPAK', 'pendapatan_x'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['imp_id'], 'safe'],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Rincian'], 'string', 'max' => 12],
            [['sumberdana'], 'string', 'max' => 12],
            [['kegiatan'], 'string', 'max' => 15]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Anggaran' => 'Anggaran',
            'AnggaranPAK' => 'Anggaran PAK',
            'AnggaranStlhPAK' => 'Anggaran Setelah PAK',
            'sumberdana' => 'Sumber Dana',
            'pendapatan_x' => 'Jumlah Pendapatan',
            'pendapatan_xmask' => 'Jumlah Pendapatan',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    //fungsi pengINSERTAN
    public function checkAndInsert()
    {
        //detect parent if exist

        //set parent which not available

        //then insert with sum of childs or value of one child (first summit)
        
    }
}


            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            