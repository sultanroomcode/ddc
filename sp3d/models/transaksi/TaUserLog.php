<?php
namespace sp3d\models\transaksi;

use Yii;

/**
 * This is the model class for table "Ta_UserLog".
 *
 * @property string $TglLogin
 * @property string $Komputer
 * @property string $UserID
 * @property string $Tahun
 * @property string $AplVer
 * @property string $DbVer
 */
class TaUserLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_UserLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TglLogin', 'Komputer', 'UserID'], 'required'],
            [['TglLogin'], 'safe'],
            [['Komputer'], 'string', 'max' => 20],
            [['UserID', 'AplVer', 'DbVer'], 'string', 'max' => 15],
            [['Tahun'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TglLogin' => 'Tgl Login',
            'Komputer' => 'Komputer',
            'UserID' => 'User ID',
            'Tahun' => 'Tahun',
            'AplVer' => 'Apl Ver',
            'DbVer' => 'Db Ver',
        ];
    }
}
