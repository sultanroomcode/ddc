<?php
namespace sp3d\models\transaksi;

use Yii;

/**
 * This is the model class for table "sp3d_master_jabatan".
 *
 * @property string $id
 * @property string $nama_jabatan
 * @property string $description
 * @property integer $status
 */
class Sp3dMasterJabatanTipe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp3d_master_jabatan_tipe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['id', 'nama_tipe_jabatan'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_tipe_jabatan' => 'Tipe Nama Jabatan',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
}
