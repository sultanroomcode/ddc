<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_TBP".
 *
 * @property string $Tahun
 * @property string $No_Bukti
 * @property string $Tgl_Bukti
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Uraian
 * @property string $Nm_Penyetor
 * @property string $Alamat_Penyetor
 * @property string $TTD_Penyetor
 * @property string $NoRek_Bank
 * @property string $Nama_Bank
 * @property double $Jumlah
 * @property string $Nm_Bendahara
 * @property string $Jbt_Bendahara
 * @property string $Status
 * @property double $KdBayar
 * @property string $Ref_Bayar
 */
class TaTBP extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_TBP';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_Bukti'], 'required'],
            [['Tgl_Bukti'], 'safe'],
            [['imp_id'], 'safe'],
            [['Jumlah', 'KdBayar'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['No_Bukti', 'Nm_Bendahara', 'Ref_Bayar'], 'string', 'max' => 30],
            [['Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Uraian'], 'string', 'max' => 250],
            [['Nm_Penyetor', 'Alamat_Penyetor'], 'string', 'max' => 50],
            [['TTD_Penyetor', 'Jbt_Bendahara'], 'string', 'max' => 35],
            [['NoRek_Bank'], 'string', 'max' => 20],
            [['Nama_Bank'], 'string', 'max' => 40],
            [['Status'], 'string', 'max' => 1],
            [['No_Bukti'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_Bukti' => 'No. Bukti',
            'Tgl_Bukti' => 'Tanggal Bukti',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Uraian' => 'Uraian',
            'Nm_Penyetor' => 'Nama Penyetor',
            'Alamat_Penyetor' => 'Alamat Penyetor',
            'TTD_Penyetor' => 'Ttd Penyetor',
            'NoRek_Bank' => 'No. Rekening Bank',
            'Nama_Bank' => 'Nama Bank',
            'Jumlah' => 'Jumlah',
            'Nm_Bendahara' => 'Nama Bendahara',
            'Jbt_Bendahara' => 'Jabatan Bendahara',
            'Status' => 'Status',
            'KdBayar' => 'Kode Bayar',
            'Ref_Bayar' => 'Ref  Bayar',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getRincian()//for kecamatan up..
    {
        return $this->hasMany(TaTBPRinci::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function afterDelete()
    {
        $this->updatingTbpDesaCount();
    }

    public function afterSave($ins, $old)
    {
        // if($ins){
            $this->updatingTbpDesaCount();
        // }
    }

    public function updatingTbpDesaCount()
    {
        $total = \sp3d\models\transaksi\TaTBP::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_tbp', 'dana' => 'dana_tbp']);
        $mod->dana_tbp = $total->sum('Jumlah');
        $mod->data_tbp = $total->count();
        $mod->save(false);
    }

    protected function kekata($x) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = $this->kekata($x - 10). " belas";
        } else if ($x <100) {
            $temp = $this->kekata($x/10)." puluh". $this->kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . $this->kekata($x - 100);
        } else if ($x <1000) {
            $temp = $this->kekata($x/100) . " ratus" . $this->kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . $this->kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = $this->kekata($x/1000) . " ribu" . $this->kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = $this->kekata($x/1000000) . " juta" . $this->kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = $this->kekata($x/1000000000) . " milyar" . $this->kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = $this->kekata($x/1000000000000) . " trilyun" . $this->kekata(fmod($x,1000000000000));
        }     
        
        return $temp;
    }

    public function nf($val){//number format
        return number_format(round($val), 0, ',','.');
    }

    public function ww($text, $far){//wordwrap
        return wordwrap($text,$far,"<br>",TRUE);
    }

    public function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }
}
