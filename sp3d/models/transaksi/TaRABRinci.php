<?php
namespace sp3d\models\transaksi;

use Yii;
use sp3d\models\DesaCount;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_RABRinci".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property string $Kd_SubRinci
 * @property string $No_Urut
 * @property string $SumberDana
 * @property string $Uraian
 * @property string $Satuan
 * @property double $JmlSatuan
 * @property double $HrgSatuan
 * @property double $Anggaran
 * @property double $JmlSatuanPAK
 * @property double $HrgSatuanPAK
 * @property double $AnggaranStlhPAK
 * @property double $AnggaranPAK
 * @property string $Kode_SBU
 */
class TaRABRinci extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public $JmlSatuanMask,$HrgSatuanMask,$AnggaranMask,$JmlSatuanPAKMask,$HrgSatuanPAKMask,$AnggaranStlhPAKMask,$AnggaranPAKMask;
    public $jenis_belanja_xtd1;
    public $oldval, $manualtrigger = false;//for belanja only
    public static function tableName()
    {
        return 'Ta_RABRinci';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa', 'Kd_Keg', 'Kd_Rincian', 'Kd_SubRinci', 'No_Urut', 'SumberDana', 'JmlSatuan', 'HrgSatuan', 'Anggaran', 'JmlSatuanPAK', 'HrgSatuanPAK', 'AnggaranStlhPAK', 'AnggaranPAK', 'kegiatan'], 'required'],
            [['No_Keg','jenis_belanja'], 'required', 'when' => function($m){ return $m->kegiatan == 'belanja'; }, 'whenClient' => "function(attribute, value){ return $('#tarabrinci-kegiatan').val() == 'belanja'; }"],
            [['JmlSatuan', 'HrgSatuan', 'Anggaran', 'JmlSatuanPAK', 'HrgSatuanPAK', 'AnggaranStlhPAK', 'AnggaranPAK', 'oldval'], 'number'],
            [['Tahun', 'SumberDana'], 'string', 'max' => 4],
            [['imp_id'], 'safe'],
            [['Kd_Desa', 'kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Rincian', 'Kode_SBU'], 'string', 'max' => 12],
            [['Kd_SubRinci', 'No_Urut'], 'string', 'max' => 2],
            [['f_verified'], 'integer'],
            [['Uraian'], 'string', 'max' => 255],
            [['Satuan', 'kegiatan', 'jenis_belanja', 'jenis_belanja_xtd1'], 'string', 'max' => 15],
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_Keg' => 'No. Kegiatan',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Kd_SubRinci' => 'Kode Sub Rinci',
            'No_Urut' => 'No. Urut',
            'SumberDana' => 'Sumber Dana',
            'Uraian' => 'Uraian',
            'Satuan' => 'Satuan',
            'JmlSatuan' => 'Jumlah Satuan',
            'HrgSatuan' => 'Harga Satuan',
            'Anggaran' => 'Anggaran',
            'JmlSatuanPAK' => 'Jumlah Satuan PAK',
            'HrgSatuanPAK' => 'Harga Satuan PAK',
            'AnggaranStlhPAK' => 'Anggaran Setelah PAK',
            'AnggaranPAK' => 'Anggaran PAK',
            
            'JmlSatuanMask' => 'Jumlah Satuan',
            'HrgSatuanMask' => 'Harga Satuan',
            'AnggaranMask' => 'Anggaran',
            'JmlSatuanPAKMask' => 'Jumlah Satuan PAK',
            'HrgSatuanPAKMask' => 'Harga Satuan PAK',
            'AnggaranStlhPAKMask' => 'Anggaran Setelah PAK',
            'AnggaranPAKMask' => 'Anggaran PAK',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
            'jenis_belanja' => 'Jenis Belanja',
            'jenis_belanja_xtd1' => 'Jenis Belanja',
            'tgl_submit' => 'Tanggal Submit',

            'Kode_SBU' => 'Kode SBU',
        ];
    }

    public function setNewNumber()
    {
        $mod = \sp3d\models\transaksi\TaRABRinci::find()->where(['Kd_Desa' => $this->Kd_Desa, 'kd_kecamatan' => $this->kd_kecamatan, 'Kd_Rincian' => $this->Kd_Rincian, 'No_Keg' => $this->No_Keg, 'Kd_Keg' => $this->Kd_Keg]);
        if($mod->count() > 0){
            $counter = (int) $mod->max('No_Urut');
            $counter++;
            $this->No_Urut = sprintf('%02d', $counter);
        } else {
            $this->No_Urut = sprintf('%02d', 1);  
        }        
    }

    public function getRab()//for kecamatan up..
    {
        return $this->hasOne(TaRAB::className(), ['Kd_Desa' => 'Kd_Desa', 'Tahun' => 'Tahun', 'Kd_Rincian' => 'Kd_Rincian']);
    }


    public function updatingPaguKegiatan($nilai)//hanya pada saat belanja
    {
        $m = TaKegiatan::findOne(['Tahun' => $this->Tahun, 'Kd_Keg' => $this->Kd_Keg]);
        $m->Pagu = $nilai;
        $m->save();
    }

    public function updatingPaguKegiatanSub($nilai)//hanya pada saat belanja
    {
        $m = TaKegiatanSub::findOne(['Tahun' => $this->Tahun, 'No_Keg' => $this->No_Keg , 'Kd_Keg' => $this->Kd_Keg]);
        $m->Pagu = $nilai;
        $m->save();
    }

    public function updatingRabDesaCount($ins, $dele=false)
    {
        // $myfile = fopen('rab-rinci'.time().".txt", "w") or die("Unable to open file!");
        // fwrite($myfile, '1 ..... :'.$ins);
        $total = \sp3d\models\transaksi\TaRABRinci::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa,'Kd_Keg' => $this->Kd_Keg, 'Kd_Rincian' => $this->Kd_Rincian]);
        $totalsub = \sp3d\models\transaksi\TaRABRinci::find()->where(['Tahun' => $this->Tahun, 'No_Keg' => $this->No_Keg, 'Kd_Desa' => $this->Kd_Desa,'Kd_Keg' => $this->Kd_Keg, 'Kd_Rincian' => $this->Kd_Rincian]);

        $model = TaRAB::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa, 'Kd_Keg' => $this->Kd_Keg, 'Kd_Rincian' => $this->Kd_Rincian]);
        $model->Anggaran = $total->sum('Anggaran');
        $model->AnggaranPAK = $total->sum('AnggaranPAK');
        $model->AnggaranStlhPAK = $total->sum('AnggaranStlhPAK');
        $model->save(false);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        //diletakkan dibaris ini dikarenakan harus melalui perubahan dulu
        $total2 = TaRAB::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa, 'kegiatan' => $this->kegiatan]);
        if($this->kegiatan == 'penerimaan'){//penerimaan
            $mod->setChange(true, ['data' => 'data_penerimaan', 'dana' => 'dana_penerimaan']);
            $mod->dana_penerimaan = $total2->sum('Anggaran');
            $mod->data_penerimaan = $total2->count();
        } else if($this->kegiatan == 'pengeluaran'){//pengeluaran
            $mod->setChange(true, ['data' => 'data_pengeluaran', 'dana' => 'dana_pengeluaran']);
            $mod->dana_pengeluaran = $total2->sum('Anggaran');
            $mod->data_pengeluaran = $total2->count();
        } else if($this->kegiatan == 'pendapatan'){//pendapatan
            $mod->setChange(true, ['data' => 'data_pendapatan', 'dana' => 'dana_pendapatan']);
            $mod->dana_pendapatan = $total2->sum('Anggaran');
            $mod->data_pendapatan = $total2->count();
        } else {//belanja
            $mod->setChange(true, ['data' => 'data_rab', 'dana' => 'dana_rab']);
            $mod->dana_rab = $total2->sum('Anggaran');
            $mod->data_rab = $total2->count();

            if($dele){
                $nm = TaRAB::findOne(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa, 'Kd_Rincian' => $this->Kd_Rincian]);
                $nm->Anggaran = $this->Anggaran;
                $nm->recalculateRabExt(true);//delete actived
            }

            $totaling = ($totalsub->sum('Anggaran') != null)?$totalsub->sum('Anggaran'):0;

            $this->updatingPaguKegiatan($model->Anggaran);//hanya diupdate saat kegiatan == belanja
            $this->updatingPaguKegiatanSub($totaling);//hanya diupdate saat kegiatan == belanja
        }
        $mod->save(false);
    }

    public function beforeSave($insert){
        $this->Satuan = strtoupper($this->Satuan);
        $this->jenis_belanja = $this->jenis_belanja_xtd1;
        if (parent::beforeSave($insert)) {
            // ...custom code here...
            return true;
        } else {
            return false;
        }
    } 

    public function afterDelete()
    {
        $this->updatingRabDesaCount(0, true);//2 parameter will send to rab
    }

    public function afterSave($ins, $old)
    {
        // if($ins){
            $this->updatingRabDesaCount($ins);
        // }
    }
}
