<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_JurnalUmumRinci".
 *
 * @property string $Tahun
 * @property string $NoBukti
 * @property string $Kd_Keg
 * @property string $RincianSD
 * @property string $NoID
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Akun
 * @property string $Kd_Rincian
 * @property string $Sumberdana
 * @property string $DK
 * @property double $Debet
 * @property double $Kredit
 */
class TaJurnalUmumRinci extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_JurnalUmumRinci';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NoBukti', 'Kd_Keg', 'RincianSD', 'NoID'], 'required'],
            [['Debet', 'Kredit'], 'number'],
            [['imp_id'], 'safe'],
            [['Tahun', 'Sumberdana'], 'string', 'max' => 4],
            [['NoBukti'], 'string', 'max' => 30],
            [['Kd_Keg'], 'string', 'max' => 20],
            [['RincianSD'], 'string', 'max' => 16],
            [['NoID'], 'string', 'max' => 3],
            [['Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Akun'], 'string', 'max' => 2],
            [['Kd_Rincian'], 'string', 'max' => 13],
            [['DK'], 'string', 'max' => 1]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'NoBukti' => 'No. Bukti',
            'Kd_Keg' => 'Kode Kegiatan',
            'RincianSD' => 'Rincian Sd',
            'NoID' => 'No. ID',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Akun' => 'Akun',
            'Kd_Rincian' => 'Kode Rincian',
            'Sumberdana' => 'Sumberdana',
            'DK' => 'DK',
            'Debet' => 'Debet',
            'Kredit' => 'Kredit',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
