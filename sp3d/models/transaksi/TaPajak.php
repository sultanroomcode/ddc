<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;

/**
 * This is the model class for table "Ta_Pajak".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_SSP
 * @property string $Tgl_SSP
 * @property string $Keterangan
 * @property string $Nama_WP
 * @property string $Alamat_WP
 * @property string $NPWP
 * @property string $Kd_MAP
 * @property string $Nm_Penyetor
 * @property string $Jn_Transaksi
 * @property string $Kd_Rincian
 * @property double $Jumlah
 * @property integer $KdBayar
 */
class TaPajak extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Pajak';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_SSP'], 'required'],
            [['Tgl_SSP'], 'safe'],
           
            [['imp_id'], 'safe'],
            [['Jumlah'], 'number'],
            [['KdBayar'], 'integer'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_SSP', 'NPWP'], 'string', 'max' => 30],
            [['Keterangan'], 'string', 'max' => 250],
            [['Nama_WP', 'Alamat_WP', 'Nm_Penyetor'], 'string', 'max' => 40],
            [['Kd_MAP'], 'string', 'max' => 20],
            [['Jn_Transaksi'], 'string', 'max' => 10],
            [['Kd_Rincian'], 'string', 'max' => 12]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_SSP' => 'No. SSP',
            'Tgl_SSP' => 'Tanggal SSP',
            'Keterangan' => 'Keterangan',
            'Nama_WP' => 'Nama Wajib Pajak',
            'Alamat_WP' => 'Alamat Wajib Pajak',
            'NPWP' => 'NPWP',
            'Kd_MAP' => 'Kode Map',
            'Nm_Penyetor' => 'Nama Penyetor',
            'Jn_Transaksi' => 'Jn Transaksi',
            'Kd_Rincian' => 'Kode Rincian',
            'Jumlah' => 'Jumlah',
            'KdBayar' => 'Kode Bayar',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getRincian()//for kecamatan up..
    {
        return $this->hasMany(TaPajakRinci::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function afterDelete()
    {
        $this->updatingPajakDesaCount();
    }

    public function afterSave($ins, $old)
    {
        // if($ins){
            $this->updatingPajakDesaCount();
        // }
    }

    public function updatingPajakDesaCount()
    {
        $total = \sp3d\models\transaksi\TaPajak::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_pajak', 'dana' => 'dana_pajak']);
        $mod->dana_pajak = $total->sum('Jumlah');
        $mod->data_pajak = $total->count();
        $mod->save(false);
    }
}
