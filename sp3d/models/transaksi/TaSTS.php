<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_STS".
 *
 * @property string $Tahun
 * @property string $No_Bukti
 * @property string $Tgl_Bukti
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Uraian
 * @property string $NoRek_Bank
 * @property string $Nama_Bank
 * @property double $Jumlah
 * @property string $Nm_Bendahara
 * @property string $Jbt_Bendahara
 */
class TaSTS extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_STS';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'No_Bukti'], 'required'],
            [['Tgl_Bukti'], 'safe'],
            [['imp_id'], 'safe'],
            [['Jumlah'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['No_Bukti', 'Nm_Bendahara'], 'string', 'max' => 30],
            [['Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Uraian'], 'string', 'max' => 250],
            [['NoRek_Bank'], 'string', 'max' => 20],
            [['Nama_Bank'], 'string', 'max' => 40],
            [['Jbt_Bendahara'], 'string', 'max' => 35],
            [['No_Bukti'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_Bukti' => 'No. Bukti',
            'Tgl_Bukti' => 'Tanggal Bukti',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Uraian' => 'Uraian',
            'NoRek_Bank' => 'No. Rekening Bank',
            'Nama_Bank' => 'Nama Bank',
            'Jumlah' => 'Jumlah',
            'Nm_Bendahara' => 'Nama Bendahara',
            'Jbt_Bendahara' => 'Jabatan Bendahara',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getRincian()//for kecamatan up..
    {
        return $this->hasMany(TaSTSRinci::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function afterDelete()
    {
        $this->updatingStsDesaCount();
    }

    public function afterSave($ins, $old)
    {
        //if($ins){
            $this->updatingStsDesaCount();
        //}
    }

    public function updatingStsDesaCount()
    {
        $total = \sp3d\models\transaksi\TaSTS::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_sts', 'dana' => 'dana_sts']);
        $mod->dana_sts = $total->sum('Jumlah');
        $mod->data_sts = $total->count();
        $mod->save(false);
    }
}
