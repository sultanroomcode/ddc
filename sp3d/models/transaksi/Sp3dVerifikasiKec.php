<?php
namespace sp3d\models\transaksi;

use sp3d\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Carbon\Carbon;
Carbon::setLocale('id');
/**
 * This is the model class for table "sp3d_verifikasi_kec".
 *
 * @property string $imp_id
 * @property string $kd_desa
 * @property string $nik
 * @property string $verified_status
 * @property string $user
 * @property string $created_at
 * @property string $updated_at
 */
class Sp3dVerifikasiKec extends ActiveRecord
{
    use \sp3d\models\TraitLog;
    public static function tableName()
    {
        return 'sp3d_verifikasi_kec';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user',
                 'updatedByAttribute' => 'user',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imp_id', 'verified_status', 'nik'], 'required'],
            [['tahun'], 'string', 'max' => 4],
            [['imp_id', 'user'], 'string', 'max' => 10],
            [['nik'], 'string', 'max' => 20],
            [['kd_desa'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'imp_id' => 'ID',
            'kd_desa' => 'Kd Desa',
            'tahun' => 'Tahun',
            'verified_status' => 'Status Verifikasi',
            'nik' => 'NIK',
            'user' => 'User',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //mencari nama orang dan desa yang di verifikasi
    public function getVerifikator()//for kecamatan up..
    {
        return $this->hasOne(Sp3dVerifikatorKec::className(), ['nik' => 'nik']);
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa']);
    }

    public function cDate($f=true)//true == created, false == update
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', ($f)?$this->created_at:$this->updated_at)->diffForHumans();
    }

    public function verifiedQuery()
    {
        //verifikasi semua data di tabel bidang, kegiatan
        Yii::$app->db->createCommand("
            UPDATE Ta_Bidang SET f_verified = 1 WHERE imp_id = '".$this->imp_id."' AND user_id = '".$this->kd_desa."';
            UPDATE Ta_Kegiatan SET f_verified = 1 WHERE imp_id = '".$this->imp_id."' AND user_id = '".$this->kd_desa."';
            UPDATE Ta_RAB SET f_verified = 1 WHERE imp_id = '".$this->imp_id."' AND user_id = '".$this->kd_desa."';
            UPDATE Ta_RABRinci SET f_verified = 1 WHERE imp_id = '".$this->imp_id."' AND user_id = '".$this->kd_desa."';
            ")
        ->execute();
        //kemudian counting
    }
    //event
    public function afterSave($ins, $old)
    {
        if($ins){
            
        } else {
            if($this->verified_status == 1){
                $this->writeLog($this->user, 'kecamatan '.$this->user.' telah memverifikasi file db '.$this->kd_desa);    
            }

            if($this->verified_status == 2){
                $this->writeLog($this->user, 'kecamatan '.$this->user.' telah menolak file db '.$this->kd_desa. ' dan memberikan revisi');    
            }            
        }
    }
}
