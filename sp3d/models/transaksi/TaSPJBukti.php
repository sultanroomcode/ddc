<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_SPJBukti".
 *
 * @property string $Tahun
 * @property string $No_SPJ
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property string $No_Bukti
 * @property string $Tgl_Bukti
 * @property string $Sumberdana
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Nm_Penerima
 * @property string $Alamat
 * @property string $Rek_Bank
 * @property string $Nm_Bank
 * @property string $NPWP
 * @property string $Keterangan
 * @property double $Nilai
 */
class TaSPJBukti extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPJBukti';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],

         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_Bukti'], 'required'],
            [['Tgl_Bukti'], 'safe'],
            [['imp_id'], 'safe'],
            [['Nilai'], 'number'],
            [['Tahun', 'Sumberdana'], 'string', 'max' => 4],
            [['No_SPJ', 'No_Bukti'], 'string', 'max' => 30],
            [['Kd_Keg', 'Rek_Bank', 'NPWP'], 'string', 'max' => 20],
            [['Kd_Rincian', 'Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Nm_Penerima', 'Nm_Bank'], 'string', 'max' => 40],
            [['Alamat'], 'string', 'max' => 50],
            [['Keterangan'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_SPJ' => 'No. Spj',
            'Kd_Keg' => 'Kode Keg',
            'Kd_Rincian' => 'Kode Rincian',
            'No_Bukti' => 'No. Bukti',
            'Tgl_Bukti' => 'Tanggal Bukti',
            'Sumberdana' => 'Sumberdana',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Nm_Penerima' => 'Nama Penerima',
            'Alamat' => 'Alamat',
            'Rek_Bank' => 'Rekening Bank',
            'Nm_Bank' => 'Nama Bank',
            'NPWP' => 'NPWP',
            'Keterangan' => 'Keterangan',
            'Nilai' => 'Nilai',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
