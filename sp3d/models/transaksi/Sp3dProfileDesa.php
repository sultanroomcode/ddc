<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
use sp3d\models\User;

class Sp3dProfileDesa extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $foto_profile_box;
    public static function tableName()
    {
        return 'sp3d_profile_desa';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user',
                 'updatedByAttribute' => 'user',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user'], 'required'],
            [['foto_profile_box'], 'file', 'extensions' => 'jpg'],
            [['email'], 'email'],
            [['jml_rt', 'jml_rw'], 'number'],
            // [['created_at', 'updated_at'], 'safe'],
            [['user'], 'string', 'max' => 10],
            [['kd_kecamatan'], 'string', 'max' => 7],
            [['kd_kabupaten'], 'string', 'max' => 4],
            [['foto_profile', 'deskripsi'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user' => 'User',
            'foto_profile' => 'Foto Profile',
            'foto_profile_box' => 'Foto Profile',
            'deskripsi' => 'Deskripsi',
            'jml_rw' => 'Jumlah RT',
            'jml_rt' => 'Jumlah RW',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // if($insert){
                $filemdb = $this->upload();
            // }
            
            return true;
        } else {
            return false;
        }
    }

    public function upload()
    {
        $this->validate();
        $this->foto_profile_box = UploadedFile::getInstance($this, 'foto_profile_box');
        if (isset($this->foto_profile_box) && $this->foto_profile_box->size != 0){
            $url = 'userfile/'.$this->user.'/';
            $this->makeDir($url);
            $newname = 'pd-'.time().'-'.$this->user.'.'.$this->foto_profile_box->extension;
            if($this->isNewRecord){
                $callback = $this->foto_profile_box->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->foto_profile) && $this->foto_profile !== '' && $this->foto_profile != null){
                    unlink($url.$this->foto_profile);
                }
                $callback = $this->foto_profile_box->saveAs($url.$newname);               
            }
            $this->foto_profile = $newname;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'user'])->onCondition(['type' => 'desa']);
    }
}
