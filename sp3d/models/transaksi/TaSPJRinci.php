<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_SPJRinci".
 *
 * @property string $Tahun
 * @property string $No_SPJ
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property string $Sumberdana
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_SPP
 * @property double $JmlCair
 * @property double $Nilai
 * @property double $Sisa
 */
class TaSPJRinci extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPJRinci';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_SPJ', 'Kd_Keg', 'Kd_Rincian', 'Sumberdana'], 'required'],
            [['JmlCair', 'Nilai', 'Sisa'], 'number'],
            [['imp_id'], 'safe'],
            [['Tahun', 'Sumberdana'], 'string', 'max' => 4],
            [['No_SPJ', 'No_SPP'], 'string', 'max' => 30],
            [['Kd_Keg'], 'string', 'max' => 20],
            [['Kd_Rincian', 'Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_SPJ' => 'No. SPJ',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Sumberdana' => 'Sumberdana',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_SPP' => 'No. SPP',
            'JmlCair' => 'Jumlah Cair',
            'Nilai' => 'Nilai',
            'Sisa' => 'Sisa',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
