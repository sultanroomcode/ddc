<?php

namespace sp3d\models\transaksi;

use Yii;

/**
 * This is the model class for table "sp3d_message_conversation".
 *
 * @property string $thr_id
 * @property string $message_content
 * @property string $submited_by
 * @property integer $status
 * @property string $submited_at
 * @property string $read_at
 */
class Sp3dMessageConversation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp3d_message_conversation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_content'], 'string'],
            [['status'], 'integer'],
            [['submited_at', 'read_at'], 'safe'],
            [['thr_id'], 'string', 'max' => 20],
            [['submited_by'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'thr_id' => 'Thr ID',
            'message_content' => 'Message Content',
            'submited_by' => 'Submited By',
            'status' => 'Status',
            'submited_at' => 'Submited At',
            'read_at' => 'Read At',
        ];
    }
}
