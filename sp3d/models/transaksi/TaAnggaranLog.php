<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_AnggaranLog".
 *
 * @property string $KdPosting
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_Perdes
 * @property string $TglPosting
 * @property string $UserID
 * @property integer $Kunci
 */
class TaAnggaranLog extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_AnggaranLog';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['KdPosting', 'Tahun', 'Kd_Desa'], 'required'],
            [['Kunci'], 'integer'],
            [['TglPosting', 'imp_id'], 'safe'],
            [['KdPosting'], 'string', 'max' => 1],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_Perdes'], 'string', 'max' => 40],
            [['UserID'], 'string', 'max' => 25]
        ];
    }

    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'KdPosting' => 'Kode Posting',//primary
            'Tahun' => 'Tahun',//primary
            'Kd_Desa' => 'Kode Desa',//primary
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_Perdes' => 'No. Perdes',
            'TglPosting' => 'Tanggal Posting',
            'UserID' => 'User ID',
            'Kunci' => 'Kunci',
            
            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
