<?php
namespace sp3d\models\transaksi;

use sp3d\models\referensi\RefDesa;
use sp3d\models\referensi\RefRekeningPembiayaan;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_Desa".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Nm_Kades
 * @property string $Jbt_Kades
 * @property string $Nm_Sekdes
 * @property string $NIP_Sekdes
 * @property string $Jbt_Sekdes
 * @property string $Nm_Kaur_Keu
 * @property string $Jbt_Kaur_Keu
 * @property string $Nm_Bendahara
 * @property string $Jbt_Bendahara
 * @property string $No_Perdes
 * @property string $Tgl_Perdes
 * @property string $No_Perdes_PB
 * @property string $Tgl_Perdes_PB
 * @property string $No_Perdes_PJ
 * @property string $Tgl_Perdes_PJ
 * @property string $Alamat
 * @property string $Ibukota
 * @property string $Status
 * @property string $NPWP
 */
class TaDesa extends ActiveRecord
{

    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa', 'kd_kecamatan', 'Nm_Kades', 'Nm_Sekdes', 'Nm_Bendahara', 'Nm_Kaur_Keu'], 'required'],
            [['Tgl_Perdes', 'Tgl_Perdes_PB', 'Tgl_Perdes_PJ'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['Status'], 'string', 'max' => 8],       
            [['imp_id'], 'safe'],
            [['kd_kecamatan', 'Kd_Desa'], 'string', 'max' => 10],
            [['Nm_Kades', 'Jbt_Kades', 'Nm_Sekdes', 'Jbt_Sekdes', 'Nm_Kaur_Keu', 'Jbt_Kaur_Keu', 'Nm_Bendahara', 'Jbt_Bendahara', 'Alamat'], 'string', 'max' => 50],
            [['NIP_Sekdes'], 'string', 'max' => 18],
            [['No_Perdes', 'No_Perdes_PB', 'No_Perdes_PJ', 'Ibukota', 'NPWP'], 'string', 'max' => 30]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Nm_Kades' => 'Nama Kepala Desa',
            'Jbt_Kades' => 'Jabatan Kepala Desa',
            'Nm_Sekdes' => 'Nama Sekretaris Desa',
            'NIP_Sekdes' => 'NIP Sekretaris Desa',
            'Jbt_Sekdes' => 'Jabatan Sekretaris Desa',
            'Nm_Kaur_Keu' => 'Nama Kaur Keuangan',
            'Jbt_Kaur_Keu' => 'Jabatan Kaur Keuangan',
            'Nm_Bendahara' => 'Nama Bendahara',
            'Jbt_Bendahara' => 'Jabatan Bendahara',
            'No_Perdes' => 'No. Perdes APBDes',
            'Tgl_Perdes' => 'Tanggal Perdes',
            'No_Perdes_PB' => 'No. Perdes Perubahan APBDES',
            'Tgl_Perdes_PB' => 'Tanggal Perdes Perubahan APBDES',
            'No_Perdes_PJ' => 'No. perkades Penjabaran APBDES',
            'Tgl_Perdes_PJ' => 'Tanggal perkades Penjabaran APBDES',
            'Alamat' => 'Alamat',
            'Ibukota' => 'Ibukota',
            'Status' => 'Status',
            'NPWP' => 'NPWP',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);        
    }

    public function getLaporanAPBDes()
    {
        // $connection = Yii::$app->getDb();
        // $command = $connection->createCommand("SELECT `Ta_RAB`.`Anggaran`, `ref_rekening_pembiayaan`.* FROM `ref_rekening_pembiayaan` LEFT JOIN `Ta_RAB` ON `ref_rekening_pembiayaan`.`kode` = `Ta_RAB`.`Kd_Rincian` WHERE `Ta_RAB`.`Kd_Desa`= :kd_desa COLLATE utf8_general_ci ", [':kd_desa' => $this->Kd_Desa]);

        $v = [
            'pendapatan' => $this->hasMany(TaRAB::className(), ['Kd_Desa' => 'Kd_Desa'])->where(['kegiatan' => 'pendapatan']),
            'belanja' => $this->hasMany(TaRAB::className(), ['Kd_Desa' => 'Kd_Desa'])->where(['kegiatan' => 'belanja']),
            'penerimaan' => $this->hasMany(TaRAB::className(), ['Kd_Desa' => 'Kd_Desa'])->where(['kegiatan' => 'penerimaan']),
            'pengeluaran' => $this->hasMany(TaRAB::className(), ['Kd_Desa' => 'Kd_Desa'])->where(['kegiatan' => 'pengeluaran']),
            // 'pembiayaan' => $command->queryAll(),
            // 'pembiayaan' => $command->queryAll(),
        ];




        return $v;
    }
}
