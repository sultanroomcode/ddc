<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;
use sp3d\models\DesaCount;
/**
 * This is the model class for table "Ta_Mutasi".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_Bukti
 * @property string $Tgl_Bukti
 * @property string $Keterangan
 * @property string $Kd_Bank
 * @property string $Kd_Rincian
 * @property string $Kd_Keg
 * @property string $Sumberdana
 * @property double $Kd_Mutasi
 * @property double $Nilai
 */
class TaMutasi extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Mutasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_Bukti'], 'required'],
            [['Tgl_Bukti'], 'safe'],
            [['imp_id'], 'safe'],
            [['Kd_Mutasi', 'Nilai'], 'number'],
            [['Tahun', 'Sumberdana'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_Bukti'], 'string', 'max' => 30],
            [['Keterangan'], 'string', 'max' => 200],
            [['Kd_Bank', 'Kd_Rincian'], 'string', 'max' => 12],
            [['Kd_Keg'], 'string', 'max' => 18]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_Bukti' => 'No Bukti',
            'Tgl_Bukti' => 'Tanggal Bukti',
            'Keterangan' => 'Keterangan',
            'Kd_Bank' => 'Kode Bank',
            'Kd_Rincian' => 'Kode Rincian',
            'Kd_Keg' => 'Kode Kegiatan',
            'Sumberdana' => 'Sumberdana',
            'Kd_Mutasi' => 'Kode Mutasi',
            'Nilai' => 'Nilai',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function afterDelete()
    {
        $this->updatingMutasiDesaCount();
    }

    public function afterSave($ins, $old)
    {
        //if($ins){
            $this->updatingMutasiDesaCount();
        //}
    }

    public function updatingMutasiDesaCount()
    {
        $total = \sp3d\models\transaksi\TaMutasi::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_mutasi', 'dana' => 'dana_mutasi']);
        $mod->dana_mutasi = $total->sum('Nilai');
        $mod->data_mutasi = $total->count();
        $mod->save(false);
    }
}
