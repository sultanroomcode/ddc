<?php
namespace sp3d\models\transaksi;
use sp3d\models\User;
use Yii;

/**
 * This is the model class for table "sp3d_pendamping_pilih_desa".
 *
 * @property string $nik
 * @property string $kd_desa
 * @property string $description
 * @property string $verified_code
 * @property integer $verified_status
 * @property string $submited_at
 * @property string $verified_by
 */
class Sp3dPendampingPilihDesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $kd_kab, $kd_kec;
    public static function tableName()
    {
        return 'sp3d_pendamping_pilih_desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nik', 'kd_desa'], 'required'],
            [['nik', 'kd_desa'], 'required', 'on' => 'verifikasi'],
            [['verified_status'], 'integer'],
            [['verified_at', 'submited_at'], 'safe'],
            [['kd_desa'], 'string', 'min' => 9],
            [['nik', 'kd_desa'], 'string', 'max' => 10],
            [['description'], 'string', 'max' => 255],
            [['verified_code'], 'string', 'max' => 6],
            [['verified_by'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nik' => 'Nik',
            'kd_kab' => 'Pilih Kabupaten',
            'kd_kec' => 'Pilih Kecamatan',
            'kd_desa' => 'Pilih Desa',
            'description' => 'Deskripsi',
            'verified_code' => 'Verified Code',
            'verified_status' => 'Verified Status',
            'verified_at' => 'Verified At',
            'verified_by' => 'Verified By',
        ];
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                $this->verified_code = rand(100000,999999);;
                $this->verified_status = 0;
                $this->verified_at = date('1000-01-01 00:00');
                $this->submited_at = date('Y-m-d H:i:s');
                $this->verified_by = '-';
            }

            if($this->scenario == 'verifikasi' && $this->verified_status == 10){
                $this->verified_at = date('Y-m-d H:i:s');
            }

            return true;
        } else {
            return false;
        }
    } 

    //relation
    public function getDesa()//rekening
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa']);
    }

    public function getPendamping()//rekening
    {
        return $this->hasOne(Sp3dPendampingDesa::className(), ['nik' => 'nik']);
    }
}
