<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_SaldoAwal".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Rincian
 * @property string $Jenis
 * @property double $Anggaran
 * @property double $Debet
 * @property double $Kredit
 * @property string $Tgl_Bukti
 */
class TaSaldoAwal extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SaldoAwal';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa', 'Kd_Rincian'], 'required'],
            [['Anggaran', 'Debet', 'Kredit'], 'number'],
            [['Tgl_Bukti'], 'safe'],
            [['imp_id'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['Kd_Rincian'], 'string', 'max' => 12],
            [['Jenis'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Jenis' => 'Jenis',
            'Anggaran' => 'Anggaran',
            'Debet' => 'Debet',
            'Kredit' => 'Kredit',
            'Tgl_Bukti' => 'Tanggal Bukti',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
