<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

use yii\web\UploadedFile;
/**
 * This is the model class for table "ta_mdb_upload".
 *
 * @property string $id
 * @property string $tahun
 * @property string $nama_file
 * @property string $f_ftp_send
 * @property string $f_ftp_back
 * @property string $date_upload
 * @property string $date_send
 * @property string $date_back
 * @property string $user
 * @property string $upload_ip
 */
class TaMdbUpload extends ActiveRecord
{
    /**
     * @inheritdoc
     */

    use \sp3d\models\TraitLog;
    use \sp3d\models\Utf8Cleaner;//traits
    public $nama_file_front, $k_desa, $k_kec, $k_kab, $k_prov;
    public static function tableName()
    {
        return 'ta_mdb_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user', 'f_aggree'], 'required'],
            [['date_upload', 'date_send', 'date_back'], 'safe'],
            [['nama_file_front'], 'file', 'extensions' => 'mde','maxSize' => 104857600, 'tooBig' => 'Maksimal 100 Mb'],
            //ukurannya dikalikan dengan 1024 bytes
            [['tahun'], 'string', 'max' => 4],
            ['tahun', 'checkExistMdb', 'message'=>'Sudah ada!'],
            [['id'], 'string', 'max' => 50],
            [['nama_file'], 'string', 'min' => 5, 'max' => 255],
            [['f_aggree', 'f_ftp_send', 'f_ftp_back'], 'string', 'max' => 1],
            [['user'], 'string', 'max' => 15],
            [['upload_ip'], 'string', 'max' => 20],
        ];
    }

    public function behaviors()
    {
     return [
         [
             'class' => BlameableBehavior::className(),
             'createdByAttribute' => 'user',
             'updatedByAttribute' => false,
         ],
         'timestamp' => [
             'class' => 'yii\behaviors\TimestampBehavior',
             'attributes' => [
                 ActiveRecord::EVENT_BEFORE_INSERT => ['date_upload'],
                 //ActiveRecord::EVENT_BEFORE_UPDATE => ['date_upload'],
             ],
             'value' => new \yii\db\Expression('NOW()'),
         ],
     ];
    }

    /**
     * @inheritdoc
     */
    public function checkExistMdb($attribute, $params)
    {
        $user = TaRAB::find()->where(['user_id'=>$this->user, 'Tahun' => $this->tahun]);
        if($user->count() > 0){
            $this->addError($attribute, 'Sudah ada!');
        }
    }

    public function checkExistData()
    {
        $user = TaRAB::find()->where(['user_id'=>$this->user, 'Tahun' => $this->tahun]);
        if($user->count() > 0){
            return true;//exist
        } else {
            return false;
        }
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tahun' => 'Tahun',
            'nama_file' => 'Nama File',
            'nama_file_front' => 'File MDE',
            'f_aggree' => 'Persetujuan',
            'f_ftp_send' => 'F Ftp Send',
            'f_ftp_back' => 'F Ftp Back',
            'date_upload' => 'Tanggal Upload',
            'date_send' => 'Tanggal Kirim',
            'date_back' => 'Tanggal Kembali',
            'user' => 'User',
            'upload_ip' => 'Upload IP',
        ];
    }

    //evemt
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($insert){
                $this->f_ftp_back = $this->f_ftp_send = 'N';
                $this->date_send = $this->date_back = '0000-01-01 00:00:01';
                $filemdb = $this->upload();
            }
            
            $this->getUserIP();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($ins, $old)
    {
        if($ins){//hanya
            //do this
            // $filemdb = $this->upload();
            // if($filemdb != false){
            //     $modelt = TaMdbUpload::findOne(['id' => $this->id, 'user' => $this->user]);
            //     $modelt->nama_file = $filemdb;
            //     $modelt->update();
            //     //$modelt->save(false);
            // }
            // $this->writeLog($this->user, 'Telah Menambahkan MDE Baru');
            // non-activekan dulu write log
        }
    }

    //other function
    public function getUserIP()
    {
        //https://stackoverflow.com/questions/13646690/how-to-get-real-ip-from-visitor
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $this->upload_ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $this->upload_ip = $forward;
        }
        else
        {
            $this->upload_ip = $remote;
        }
    }

    public function getSiskeudes()
    {
        return $this->hasOne(TaUserDesa::className(), ['Id' => 'user']);
    }

    /*public function getVerifikasi()
    {
        return $this->hasOne(Sp3dVerifikasiKec::className(), ['imp_id' => 'id', 'kd_desa' => 'user']);
    }*/

    public function upload()
    {
        $this->validate();
        $this->nama_file_front = UploadedFile::getInstance($this, 'nama_file_front');
        if (isset($this->nama_file_front) && $this->nama_file_front->size != 0){
            $url = 'userfile/'.$this->user.'/db/';
            $this->makeDir($url);
            if($this->isNewRecord){
                $newname = time().'-'.$this->user.'.'.$this->nama_file_front->extension;
                $callback = $this->nama_file_front->saveAs($url.$newname);
                // return $newname;
            }

            $this->nama_file = $newname; 
            //revalidate
            // $this->validate();
            return true;
        }
        else{
            return false;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
