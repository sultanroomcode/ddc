<?php

namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_Pemda".
 *
 * @property string $Tahun
 * @property string $Kd_Prov
 * @property string $Kd_Kab
 * @property string $Nama_Pemda
 * @property string $Nama_Provinsi
 * @property string $Ibukota
 * @property string $Alamat
 * @property string $Nm_Bupati
 * @property string $Jbt_Bupati
 * @property resource $Logo
 * @property string $C_Kode
 * @property string $C_Pemda
 * @property string $C_Data
 */
class TaPemda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Pemda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun'], 'required'],
            [['Logo'], 'string'],
            [['imp_id'], 'safe'],
            [['Tahun', 'Kd_Prov', 'Kd_Kab'], 'string', 'max' => 4],
            [['Nama_Pemda', 'Jbt_Bupati'], 'string', 'max' => 100],
            [['Nama_Provinsi', 'C_Kode', 'C_Pemda', 'C_Data'], 'string', 'max' => 200],
            [['Ibukota'], 'string', 'max' => 30],
            [['Alamat', 'Nm_Bupati'], 'string', 'max' => 50]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Prov' => 'Kode Provinsi',
            'Kd_Kab' => 'Kode Kabupaten',
            'Nama_Pemda' => 'Nama Pemda',
            'Nama_Provinsi' => 'Nama Provinsi',
            'Ibukota' => 'Ibukota',
            'Alamat' => 'Alamat',
            'Nm_Bupati' => 'Nama Bupati',
            'Jbt_Bupati' => 'Jabatan Bupati',
            'Logo' => 'Logo',
            'C_Kode' => 'C Kode',
            'C_Pemda' => 'C Pemda',
            'C_Data' => 'C Data',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
