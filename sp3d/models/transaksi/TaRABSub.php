<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_RABSub".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property string $Kd_SubRinci
 * @property string $Nama_SubRinci
 * @property double $Anggaran
 * @property double $AnggaranPAK
 * @property double $AnggaranStlhPAK
 */
class TaRABSub extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RABSub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Desa', 'Kd_Keg', 'Kd_Rincian', 'Kd_SubRinci'], 'required'],
            [['Anggaran', 'AnggaranPAK', 'AnggaranStlhPAK'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['imp_id'], 'safe'],
            [['Kd_Desa','kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Rincian'], 'string', 'max' => 12],
            [['Kd_SubRinci'], 'string', 'max' => 2],
            [['Nama_SubRinci'], 'string', 'max' => 50]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Kd_SubRinci' => 'Kode Sub Rinci',
            'Nama_SubRinci' => 'Nama Sub Rinci',
            'Anggaran' => 'Anggaran',
            'AnggaranPAK' => 'Anggaran Pak',
            'AnggaranStlhPAK' => 'Anggaran Setelah Pak',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
