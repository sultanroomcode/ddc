<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Carbon\Carbon;
Carbon::setLocale('id');

/**
 * This is the model class for table "sp3d_verifikasi_revisi_kec".
 *
 * @property string $id
 * @property string $kd_desa
 * @property string $year
 * @property string $message
 * @property string $user
 * @property string $created_at
 * @property string $updated_at
 */
class Sp3dVerifikasiRevisiKec extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitLog;
    public static function tableName()
    {
        return 'sp3d_verifikasi_revisi_kec';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user',
                 'updatedByAttribute' => null,
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kd_desa',], 'required'],
            [['message'], 'string'],
            [['id', 'user'], 'string', 'max' => 10],
            [['kd_desa'], 'string', 'max' => 12],
            [['year'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_desa' => 'Kd Desa',
            'year' => 'Year',
            'message' => 'Message',
            'user' => 'User',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function cDate($f=true)//true == created, false == update
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', ($f)?$this->created_at:$this->updated_at)->diffForHumans();
    }
}
