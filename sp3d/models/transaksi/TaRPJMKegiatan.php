<?php

namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_RPJM_Kegiatan".
 *
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Bid
 * @property string $Kd_Keg
 * @property string $ID_Keg
 * @property string $Nama_Kegiatan
 * @property string $Lokasi
 * @property string $Keluaran
 * @property string $Kd_Sas
 * @property string $Sasaran
 * @property integer $Tahun1
 * @property integer $Tahun2
 * @property integer $Tahun3
 * @property integer $Tahun4
 * @property integer $Tahun5
 * @property integer $Tahun6
 * @property integer $Swakelola
 * @property integer $Kerjasama
 * @property integer $Pihak_Ketiga
 * @property string $Sumberdana
 */
class TaRPJMKegiatan extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Keg'], 'required'],
            [['Tahun1', 'Tahun2', 'Tahun3', 'Tahun4', 'Tahun5', 'Tahun6', 'Swakelola', 'Kerjasama', 'Pihak_Ketiga'], 'integer'],
            [['TglPosting'], 'safe'],
            [['Kd_Desa', 'ID_Keg'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Bid'], 'string', 'max' => 10],
            [['Kd_Keg', 'Kd_Sas'], 'string', 'max' => 18],
            [['Nama_Kegiatan'], 'string', 'max' => 100],
            [['Lokasi', 'Keluaran', 'Sasaran'], 'string', 'max' => 50],
            [['Sumberdana'], 'string', 'max' => 30]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Bid' => 'Kode Bidang',
            'Kd_Keg' => 'Kode Kegiatan',
            'ID_Keg' => 'Id Kegiatan',
            'Nama_Kegiatan' => 'Nama Kegiatan',
            'Lokasi' => 'Lokasi',
            'Keluaran' => 'Keluaran',
            'Kd_Sas' => 'Kode Sas',
            'Sasaran' => 'Sasaran',
            'Tahun1' => 'Tahun1',
            'Tahun2' => 'Tahun2',
            'Tahun3' => 'Tahun3',
            'Tahun4' => 'Tahun4',
            'Tahun5' => 'Tahun5',
            'Tahun6' => 'Tahun6',
            'Swakelola' => 'Swakelola',
            'Kerjasama' => 'Kerjasama',
            'Pihak_Ketiga' => 'Pihak  Ketiga',
            'Sumberdana' => 'Sumberdana',

            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
