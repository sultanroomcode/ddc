<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "ta_kegiatan_sub".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Bid
 * @property string $Kd_Keg
 * @property string $No_Keg
 * @property string $ID_Keg
 * @property string $Nama_Kegiatan
 * @property string $Pagu
 * @property string $Pagu_PAK
 * @property string $Nm_PPK
 * @property string $Lokasi
 * @property string $Waktu
 * @property string $Keluaran
 * @property string $Sumberdana
 * @property integer $user_id
 * @property string $tgl_submit
 */
class TaKegiatanSub extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    use \sp3d\models\TraitFormat;
    public static function tableName()
    {
        return 'ta_kegiatan_sub';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Keg', 'No_Keg', 'Pagu', 'Pagu_PAK'], 'required'],
            [['Pagu', 'Pagu_PAK'], 'number'],
            [['imp_id'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa', 'kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Bid', 'ID_Keg'], 'string', 'max' => 10],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['No_Keg'], 'string', 'max' => 2],
            [['Nama_Kegiatan'], 'string', 'max' => 200],
            [['Nm_PPK', 'Lokasi', 'Keluaran'], 'string', 'max' => 50],
            [['Waktu'], 'string', 'max' => 30],
            [['Sumberdana'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',//pk
            'Kd_Desa' => 'Kd  Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'Kd_Bid' => 'Kd Bid',
            'Kd_Keg' => 'Kd Keg',//pk
            'No_Keg' => 'No Keg',//pk
            'ID_Keg' => 'Id Keg',
            'Nama_Kegiatan' => 'Nama Kegiatan',
            'Pagu' => 'Pagu',
            'Pagu_PAK' => 'Pagu Pak',
            'Nm_PPK' => 'PPK / TPK',
            'Lokasi' => 'Lokasi',
            'Waktu' => 'Waktu',
            'Keluaran' => 'Keluaran',
            'Sumberdana' => 'Sumberdana',

            'imp_id' => 'Kode Import',
            'user_id' => 'User ID',
            'tgl_submit' => 'Tgl Submit',
        ];
    }

    public function setNewNumber()
    {
        $mod = \sp3d\models\transaksi\TaKegiatanSub::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa, 'kd_kecamatan' => $this->kd_kecamatan, 'Kd_Keg' => $this->Kd_Keg]);
        if($mod->count() > 0){
            $counter = (int) $mod->max('No_Keg');
            $counter++;
            $this->No_Keg = sprintf('%02d', $counter);
        } else {
            $this->No_Keg = sprintf('%02d', 1);  
        }        
    }

    public function getBelanjaRinci()
    {
        return $this->hasMany(TaRABRinci::className(), ['Kd_Desa' => 'Kd_Desa', 'Tahun' => 'Tahun', 'No_Keg' => 'No_Keg', 'Kd_Keg' => 'Kd_Keg']); 
    }
    //events
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        foreach (TaRABRinci::find()->where(['Kd_Desa' => $this->Kd_Desa, 'Tahun' => $this->Tahun, 'No_Keg' => $this->No_Keg, 'Kd_Keg' => $this->Kd_Keg])->all() as $rr) {
            $rr->delete();
        }

        return true;
    }
}
