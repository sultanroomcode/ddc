<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_UserMenu".
 *
 * @property string $UserID
 * @property string $IDMenu
 * @property string $Otoritas
 */
class TaUserMenu extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_UserMenu';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID', 'IDMenu'], 'required'],
            [['tgl_submit'], 'safe'],
            [['user_id'], 'integer'],
            [['UserID'], 'string', 'max' => 20],
            [['IDMenu'], 'string', 'max' => 10],
            [['Otoritas'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UserID' => 'User ID',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
            'IDMenu' => 'Idmenu',
            'Otoritas' => 'Otoritas',
        ];
    }
}
