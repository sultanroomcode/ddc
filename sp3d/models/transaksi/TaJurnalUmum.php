<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_JurnalUmum".
 *
 * @property string $Tahun
 * @property string $KdBuku
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Tanggal
 * @property string $JnsBukti
 * @property string $NoBukti
 * @property string $Keterangan
 * @property string $DK
 * @property double $Debet
 * @property double $Kredit
 * @property string $Jenis
 * @property integer $Posted
 */
class TaJurnalUmum extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_JurnalUmum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tanggal'], 'safe'],
            [['NoBukti'], 'required'],
            [['Debet', 'Kredit'], 'number'],
            [['Posted'], 'integer'],
            [['imp_id'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['KdBuku'], 'string', 'max' => 10],
            [['Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['JnsBukti'], 'string', 'max' => 5],
            [['NoBukti'], 'string', 'max' => 30],
            [['Keterangan'], 'string', 'max' => 250],
            [['DK'], 'string', 'max' => 1],
            [['Jenis'], 'string', 'max' => 3]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'KdBuku' => 'Kode Buku',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Tanggal' => 'Tanggal',
            'JnsBukti' => 'Jenis Bukti',
            'NoBukti' => 'No. Bukti',
            'Keterangan' => 'Keterangan',
            'DK' => 'DK',
            'Debet' => 'Debet',
            'Kredit' => 'Kredit',
            'Jenis' => 'Jenis',
            'Posted' => 'Posted',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getRincian()//for kecamatan up..
    {
        return $this->hasMany(TaJurnalUmumRinci::className(), ['Kd_Desa' => 'Kd_Desa']);
    }
}
