<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "kpm_pelatihan".
 *
 * @property string $kd_desa
 * @property integer $id
 * @property integer $idp
 * @property string $nama
 * @property string $tahun
 * @property string $penyelenggara
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Sp3dPerangkatPelatihan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp3d_perangkat_pelatihan';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'created_by',
                 'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_desa', 'nik', 'idp'], 'required'],
            [['idp'], 'integer'],
            [['kategori'], 'string', 'max' => 50],
            [['tipe'], 'string', 'max' => 50],
            [['kd_desa'], 'string', 'max' => 12],
            [['nama', 'penyelenggara'], 'string', 'max' => 150],
            [['tahun'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_desa' => 'Kd Desa',
            'nik' => 'ID',
            'idp' => 'Idp',
            'nama' => 'Nama',
            'tipe' => 'Lembaga',
            'kategori' => 'Kategori',
            'tahun' => 'Tahun',
            'penyelenggara' => 'Penyelenggara',
            'created_by' => 'Dibuat Oleh',
            'updated_by' => 'Diupdate Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_at' => 'Diupdate Pada',
        ];
    }

    //tools
    public function genNum()//generate number
    {
        //look for the max of
        $mx = $this->find()->where(['kd_desa' => $this->kd_desa, 'nik' => $this->nik])->max('idp');
        if($mx == null){
            $this->idp = 1;
        } else {
            $this->idp = $mx +1;
        }
    }

    public function arrKategori()//for kecamatan up..
    {
        return ArrayHelper::map(Sp3dMasterJenisPelatihan::findAll(['tipe' => $this->tipe, 'status' => 10]), 'id','nama_pelatihan');
    }

    public function getPerangkat()//for kecamatan up..
    {
        return $this->hasOne(Sp3dPerangkatDesa::className(), ['user' => 'kd_desa', 'nik' => 'nik']);
    }

    public function getKategoripelatihan()//for kecamatan up..
    {
        //column other - colomn of this table
        return $this->hasOne(Sp3dMasterJenisPelatihan::className(), ['id' => 'kategori', 'tipe' => 'tipe']);
    }
}
