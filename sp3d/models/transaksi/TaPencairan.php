<?php

namespace sp3d\models\transaksi;

use Yii;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_Pencairan".
 *
 * @property string $Tahun
 * @property string $No_Cek
 * @property string $No_SPP
 * @property string $Tgl_Cek
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Keterangan
 * @property double $Jumlah
 * @property double $Potongan
 * @property double $KdBayar
 */
class TaPencairan extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Pencairan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_Cek', 'No_SPP'], 'required'],
            [['Tgl_Cek'], 'safe'],
            [['imp_id'], 'safe'],
            [['Jumlah', 'Potongan', 'KdBayar'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['No_Cek', 'No_SPP'], 'string', 'max' => 30],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Keterangan'], 'string', 'max' => 250],
            [['No_Cek'], 'unique']
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_Cek' => 'No. Cek',
            'No_SPP' => 'No. SPP',
            'Tgl_Cek' => 'Tanggal Cek',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Keterangan' => 'Keterangan',
            'Jumlah' => 'Jumlah',
            'Potongan' => 'Potongan',
            'KdBayar' => 'Kode Bayar',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function afterDelete()
    {
        $this->updatingPencairanDesaCount();
    }

    public function afterSave($ins, $old)
    {
        // if($ins){
            $this->updatingPencairanDesaCount();
        // }
    }

    public function getTanggal()
    {
        return date('d-F-Y', strtotime($this->Tgl_Cek));
    }

    public function updatingPencairanDesaCount()
    {
        $total = \sp3d\models\transaksi\TaPencairan::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_pencairan', 'dana' => 'dana_pencairan']);
        $mod->dana_pencairan = $total->sum('Jumlah');
        $mod->data_pencairan = $total->count();
        $mod->save(false);
    }
}
