<?php

namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
/**
 * This is the model class for table "sp3d_file_indikator".
 *
 * @property string $id
 * @property string $file_indikator
 * @property string $tipe_indikator
 * @property string $kd_desa
 * @property string $kd_kecamatan
 * @property string $kd_kabupaten
 * @property integer $verifikasi_kab
 * @property string $komen_kab
 * @property string $komen_desa
 * @property string $tanggal_upload
 * @property string $tanggal_verifikasi
 */
class Sp3dFileIndikator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file_indikator_box;
    public static function tableName()
    {
        return 'sp3d_file_indikator';
    }

    public function behaviors()
    {
         return [
             // [
             //     'class' => BlameableBehavior::className(),
             //     'createdByAttribute' => 'user',
             //     'updatedByAttribute' => 'user',
             // ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tanggal_upload'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => null,//['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kd_desa'], 'required'],
            [['verifikasi_kab'], 'integer'],
            [['komen_kab', 'komen_desa'], 'string'],
            [['tanggal_upload', 'tanggal_verifikasi'], 'safe'],
            [['file_indikator_box'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
            [['id', 'tipe_indikator'], 'string', 'max' => 50],
            [['file_indikator'], 'string', 'max' => 100],
            [['kd_desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['kd_kabupaten'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_indikator' => 'File Indikator',
            'file_indikator_box' => 'File Indikator',
            'kd_desa' => 'Kd Desa',
            'kd_kecamatan' => 'Kd Kecamatan',
            'kd_kabupaten' => 'Kd Kabupaten',
            'verifikasi_kab' => 'Verifikasi Kab',
            'komen_kab' => 'Komen Kabupaten',
            'komen_desa' => 'Komen Desa',
            'tanggal_upload' => 'Tanggal Upload',
            'tanggal_verifikasi' => 'Tanggal Verifikasi',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // if($insert){
            $this->tanggal_verifikasi = '0000-00-00 00:00:01';
            $filemdb = $this->upload();
            // }
            
            return true;
        } else {
            return false;
        }
    }

    public function upload()
    {
        $this->validate();
        $this->file_indikator_box = UploadedFile::getInstance($this, 'file_indikator_box');
        if (isset($this->file_indikator_box) && $this->file_indikator_box->size != 0){
            $url = 'userfile/'.$this->kd_desa.'/fi/';
            $this->makeDir($url);
            $newname = 'fi-'.time().'-'.$this->tipe_indikator.'-'.$this->kd_desa.'.'.$this->file_indikator_box->extension;
            if($this->isNewRecord){
                $callback = $this->file_indikator_box->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->file_indikator)){
                    unlink($url.$this->file_indikator);
                }
                $callback = $this->file_indikator_box->saveAs($url.$newname);           
            }
            $this->file_indikator = $newname;
            return true;
        }
        else{
            return false;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
