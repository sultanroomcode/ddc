<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_RPJM_Bidang".
 *
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Bid
 * @property string $Nama_Bidang
 */
class TaRPJMBidang extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Bidang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Bid'], 'required'],
            [['TglPosting'], 'safe'],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Bid'], 'string', 'max' => 10],
            [['Nama_Bidang'], 'string', 'max' => 100]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Bid' => 'Kode Bidang',
            'Nama_Bidang' => 'Nama Bidang',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getKegiatan()//for kecamatan up..
    {
        return $this->hasMany(TaRPJMKegiatan::className(), ['Kd_Bid' => 'Kd_Bid']);
    }
}
