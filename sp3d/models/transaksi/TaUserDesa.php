<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\User;
use Yii;

/**
 * This is the model class for table "Ta_UserDesa".
 *
 * @property integer $Id
 * @property string $Kd_Kec
 * @property string $Kd_Desa
 */
class TaUserDesa extends \yii\db\ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_UserDesa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id'], 'required'],
            [['Id'], 'integer'],
            [['Kd_Kec'], 'string', 'max' => 8],
            [['Kd_Desa'], 'string', 'max' => 12],
            [['siskeu_id', 'kd_kabupaten'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'siskeu_id' => 'Kode Desa Siskeudes',
            'kd_kabupaten' => 'Kode Kabupaten (PUM)',
            'Kd_Kec' => 'Kode Kecamatan (PUM)',
            'Kd_Desa' => 'Kode Desa (PUM)',
        ];
    }

    public function getAllRoot()
    {
        $kd_desa = $this->Id;
        $u = User::find()->select(['type', 'description'])->where(['IN','Id', [substr($kd_desa, 0, 7), substr($kd_desa, 0,4), substr($kd_desa, 0, 2)]]);
        return $u;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'Id']);
    }
}
