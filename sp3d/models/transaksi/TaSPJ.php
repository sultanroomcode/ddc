<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_SPJ".
 *
 * @property string $Tahun
 * @property string $No_SPJ
 * @property string $Tgl_SPJ
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_SPP
 * @property string $Keterangan
 * @property double $Jumlah
 * @property double $Potongan
 * @property string $Status
 */
class TaSPJ extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPJ';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_SPJ'], 'required'],
            [['Tgl_SPJ'], 'safe'],
            [['imp_id'], 'safe'],
            [['Jumlah', 'Potongan'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['No_SPJ', 'No_SPP'], 'string', 'max' => 30],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Keterangan'], 'string', 'max' => 250],
            [['Status'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_SPJ' => 'No. Spj',
            'Tgl_SPJ' => 'Tanggal Spj',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_SPP' => 'No. Spp',
            'Keterangan' => 'Keterangan',
            'Jumlah' => 'Jumlah',
            'Potongan' => 'Potongan',
            'Status' => 'Status',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getBukti()//for kecamatan up..
    {
        return $this->hasMany(TaSPJBukti::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getPotongan()//for kecamatan up..
    {
        return $this->hasMany(TaSPJPot::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getRincian()//for kecamatan up..
    {
        return $this->hasMany(TaSPJRinci::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getSisa()//for kecamatan up..
    {
        return $this->hasMany(TaSPJSisa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function afterDelete()
    {
        $this->updatingSpjDesaCount();
    }

    public function afterSave($ins, $old)
    {
        // if($ins){
            $this->updatingSpjDesaCount();
        // }
    }

    public function updatingSpjDesaCount()
    {
        $total = \sp3d\models\transaksi\TaSPJ::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_spj', 'dana' => 'dana_spj']);
        
        $mod->dana_spj = $total->sum('Jumlah');
        $mod->data_spj = $total->count();
        $mod->save(false);
    }
}
