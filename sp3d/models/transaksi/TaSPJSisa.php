<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_SPJSisa".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_Bukti
 * @property string $Tgl_Bukti
 * @property string $No_SPJ
 * @property string $Tgl_SPJ
 * @property string $No_SPP
 * @property string $Tgl_SPP
 * @property string $Kd_Keg
 * @property string $Keterangan
 * @property double $Nilai
 */
class TaSPJSisa extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPJSisa';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_Bukti'], 'required'],
            [['Tgl_Bukti', 'Tgl_SPJ', 'Tgl_SPP'], 'safe'],
            [['Nilai'], 'number'],
            [['imp_id'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_Bukti', 'No_SPJ', 'No_SPP'], 'string', 'max' => 30],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Keterangan'], 'string', 'max' => 250],
            [['No_Bukti'], 'unique'],
            [['No_SPJ'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_Bukti' => 'No. Bukti',
            'Tgl_Bukti' => 'Tanggal Bukti',
            'No_SPJ' => 'No. SPJ',
            'Tgl_SPJ' => 'Tanggal SPJ',
            'No_SPP' => 'No. SPP',
            'Tgl_SPP' => 'Tanggal SPP',
            'Kd_Keg' => 'Kode Kegiatan',
            'Keterangan' => 'Keterangan',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
            'Nilai' => 'Nilai',
        ];
    }
}
