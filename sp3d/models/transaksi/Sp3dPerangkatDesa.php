<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
use sp3d\models\User;
/**
 * This is the model class for table "sp3d_kepala_desa".
 *
 * @property string $nik
 * @property string $nama
 * @property string $jabatan
 * @property string $tipe
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $foto_profile
 * @property string $jenis_kelamin
 * @property string $pendidikan
 * @property string $tgl_sk_pengangkatan
 * @property string $no_sk_pengangkatan
 * @property string $tahun_awal
 * @property string $tahun_akhir
 * @property string $user
 * @property string $created_at
 * @property string $updated_at
 */
class Sp3dPerangkatDesa extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $foto_profile_box, $dummy_var;
    public static function tableName()
    {
        return 'sp3d_perangkat_desa';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user',
                 'updatedByAttribute' => 'user',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nik', 'jabatan'], 'required'],
            [['tanggal_lahir', 'tgl_sk_pengangkatan'], 'safe'],
            [['tahun_awal', 'tahun_akhir'], 'string', 'max' => 4],
            [['catatan_pelatihan'], 'safe'],
            [['tipe'], 'string', 'max' => 50],
            [['nik','no_kontak'], 'string', 'max' => 20],
            [['nama'], 'string', 'max' => 200],
            [['tempat_lahir'], 'string', 'max' => 100],
            [['foto_profile'], 'string', 'max' => 255],
            [['foto_profile_box'], 'file', 'extensions' => 'jpg'],
            [['jenis_kelamin', 'no_sk_pengangkatan', 'user', 'pendidikan'], 'string', 'max' => 10],
            [['no_sk_pengangkatan'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nik' => 'NIK',
            'nama' => 'Nama Perangkat Desa',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'pendidikan' => 'Pendidikan Terakhir',
            'foto_profile_box' => 'Foto Profile',
            'foto_profile' => 'Foto Profile',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tgl_sk_pengangkatan' => 'Tgl Sk Pengangkatan',
            'no_sk_pengangkatan' => 'No Sk Pengangkatan',
            'user' => 'User',
            'no_kontak' => 'No Handphone',
            'tahun_awal' => 'Tahun Awal Masa Jabatan',
            'tahun_akhir' => 'Tahun Akhir Masa Jabatan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // if($insert){
            $this->tempat_lahir = strtoupper($this->tempat_lahir);
            $this->kd_kecamatan = substr($this->user, 0, 7);
            $this->kd_kabupaten = substr($this->user, 0, 4);
            $this->kd_provinsi = substr($this->user, 0, 2);
            $filemdb = $this->upload();
            // }
            
            return true;
        } else {
            return false;
        }
    }

    public function getJabatanproperti()//for kecamatan up..
    {
        //column other - colomn of this table
        return $this->hasOne(Sp3dMasterJabatan::className(), ['id' => 'jabatan']);
    }

    public function getPelatihan()//for kecamatan up..
    {
        //column other - colomn of this table
        return $this->hasMany(Sp3dPerangkatPelatihan::className(), ['kd_desa' => 'user', 'nik' => 'nik']);//diversi selanjutnya akan diperbaiki relasinya menjad kd_desa -> kd_desa
    }


    public function getKabupaten()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatan()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesa()
    {
        return $this->hasOne(User::className(), ['id' => 'user'])->onCondition(['type' => 'desa']);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $url = 'userfile/'.$this->user.'/';
        if(file_exists($url.$this->foto_profile)){
            unlink($url.$this->foto_profile);
        }
    }

    public function gender(){
        return ($this->jenis_kelamin == 'P')?'Perempuan':'Laki-Laki';
    }

    public function tanggal_lahir_format()
    {
        return date('d-F-Y', strtotime($this->tanggal_lahir));
    }

    public function tanggal_sk_format()
    {
        return date('d-F-Y', strtotime($this->tgl_sk_pengangkatan));
    }

    public function show_foto()
    {
        $img = "../css/SuperAdmin-1-0-3/Template/1-0-3/img/profile-pic.png";
        if(file_exists('./userfile/'.$this->user.'/'.$this->foto_profile)){
            $img = '../userfile/'.$this->user.'/'.$this->foto_profile;
        }
        
        return $img;
    }

    public function upload()
    {
        $this->validate();
        $this->foto_profile_box = UploadedFile::getInstance($this, 'foto_profile_box');
        if (isset($this->foto_profile_box) && $this->foto_profile_box->size != 0){
            $url = 'userfile/'.$this->user.'/';
            $this->makeDir($url);
            $newname = 'kd-'.time().'-'.$this->user.'.'.$this->foto_profile_box->extension;
            if($this->isNewRecord){
                $callback = $this->foto_profile_box->saveAs($url.$newname);
            } else {
                if(file_exists($url.$this->foto_profile) && !is_dir($url.$this->foto_profile)){
                    unlink($url.$this->foto_profile);
                }
                $callback = $this->foto_profile_box->saveAs($url.$newname);           
            }
            $this->foto_profile = $newname;
        }
    }

    protected function makeDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
