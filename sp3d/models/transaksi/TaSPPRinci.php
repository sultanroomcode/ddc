<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_SPPRinci".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_SPP
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property string $Sumberdana
 * @property double $Nilai
 */
class TaSPPRinci extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPPRinci';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_SPP', 'Kd_Keg', 'Kd_Rincian', 'Sumberdana'], 'required'],
            [['Nilai'], 'number'],
            [['imp_id'], 'safe'],
            [['Tahun', 'Sumberdana'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_SPP'], 'string', 'max' => 30],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Rincian'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_SPP' => 'No. Spp',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Sumberdana' => 'Sumberdana',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
            'Nilai' => 'Nilai',
        ];
    }
}
