<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_RPJM_Pagu_Indikatif".
 *
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Sumber
 * @property double $Tahun1
 * @property double $Tahun2
 * @property double $Tahun3
 * @property double $Tahun4
 * @property double $Tahun5
 * @property double $Tahun6
 * @property string $Pola
 */
class TaRPJMPaguIndikatif extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Pagu_Indikatif';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Keg', 'Kd_Sumber'], 'required'],
            [['Tahun1', 'Tahun2', 'Tahun3', 'Tahun4', 'Tahun5', 'Tahun6'], 'number'],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Sumber'], 'string', 'max' => 4],
            [['Pola'], 'string', 'max' => 5]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Sumber' => 'Kode Sumber',
            'Tahun1' => 'Tahun1',
            'Tahun2' => 'Tahun2',
            'Tahun3' => 'Tahun3',
            'Tahun4' => 'Tahun4',
            'Tahun5' => 'Tahun5',
            'Tahun6' => 'Tahun6',
            'Pola' => 'Pola',

            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
