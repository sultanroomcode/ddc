<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\referensi\RefRek4;
use sp3d\models\referensi\RefRekeningBelanja;
use sp3d\models\referensi\RefRekeningKegiatan;
use sp3d\models\referensi\RefRekeningPendapatan;
use sp3d\models\referensi\RefRekeningPembiayaan;
use sp3d\models\DesaCount;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_RAB".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property double $Anggaran
 * @property double $AnggaranPAK
 * @property double $AnggaranStlhPAK
 */
class TaRAB extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public $pendapatan_x, $pendapatan_xmask;
    public $rab_ext, $rab_ext_ada, $jenis_belanja, $balance, $accumulate, $oldval;
    public $kode_bidang, $kode_program, $kode_kegiatan_rinci, $kode_rincian;
    public $kode_bidang_kegiatan, $kode_program_kegiatan;
    public $nama_rincian;
    public $manualtrigger = false;
    public static function tableName()
    {
        return 'Ta_RAB';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pendapatan_x'], 'required', 'when' => function($model){
                return $model->kegiatan !== 'belanja' && $model->isNewRecord;
            }, 'whenClient' => "function(attribute, value){ return $('#tarab-kegiatan').val() !== 'belanja'; }"],
            [['Tahun', 'Kd_Desa', 'Kd_Keg', 'Kd_Rincian','kegiatan'], 'required'],
            [['Anggaran', 'AnggaranPAK', 'AnggaranStlhPAK', 'pendapatan_x'], 'number'],
            [['Tahun'], 'string', 'max' => 4],
            [['f_verified'], 'integer'],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['imp_id'], 'safe'],
            [['rab_ext', 'rab_ext_ada', 'balance'], 'string', 'max' => 255],
            [['accumulate'], 'number'],//accumulate and balance digunakan pada saat update
            
            [['Kd_Rincian'], 'string', 'max' => 12],
            [['sumberdana'], 'string', 'max' => 12],
            [['kegiatan'], 'string', 'max' => 15]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Anggaran' => 'Anggaran',
            'AnggaranPAK' => 'Anggaran PAK',
            'AnggaranStlhPAK' => 'Anggaran Setelah PAK',
            'sumberdana' => 'Sumber Dana',
            'pendapatan_x' => 'Jumlah Pendapatan',
            'pendapatan_xmask' => 'Jumlah Pendapatan',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getUraian()//rekening
    {
        return $this->hasOne(RefRek4::className(), ['Obyek' => 'Kd_Rincian']);
    }

    public function getRincianBelanja()//for kecamatan up..
    {
        return $this->hasOne(RefRekeningBelanja::className(), ['kode' => 'Kd_Rincian']);
    }

    public function getRincianPembiayaan()//for kecamatan up..
    {
        return $this->hasOne(RefRekeningPembiayaan::className(), ['kode' => 'Kd_Rincian']);
    }

    public function getRincianPendapatan()//for kecamatan up..
    {
        return $this->hasOne(RefRekeningPendapatan::className(), ['kode' => 'Kd_Rincian']);
    }

    public function getRincianKegiatan()//for kecamatan up..
    {
        return $this->hasOne(RefRekeningKegiatan::className(), ['kode' => 'Kd_Rincian']);
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getDetailkegiatan()//for kecamatan up..
    {
        return $this->hasOne(TaKegiatan::className(), ['Kd_Desa' => 'Kd_Desa', 'Kd_Keg' => 'Kd_Keg', 'Tahun' => 'Tahun']);
    }

    public function getRincian()//for kecamatan up..
    {
        return $this->hasMany(TaRABRinci::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getRincian2()//for kecamatan up..
    {
        return $this->hasMany(TaRABRinci::className(), ['Kd_Desa' => 'Kd_Desa', 'Kd_Keg' => 'Kd_Keg','Kd_Rincian' => 'Kd_Rincian', 'Tahun' => 'Tahun', 'kegiatan' => 'kegiatan']);
    }

    public function getSub()//for kecamatan up..
    {
        return $this->hasMany(TaRABSub::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function recalculateRabExt($dele=false, $belanja=false)
    {
        // $myfile = fopen('rab'.time().".txt", "w") or die("Unable to open file!");

        if($dele || $belanja)
        {
            rtrim($this->Kd_Rincian,'. ');
            if($belanja){
                //set Anggaran
                $this->pendapatan_x = $this->Anggaran;
            }
            $exp = explode('.', $this->Kd_Rincian);
            $vb = '';
            $vr = [];
            foreach ($exp as $v) {
                $vb .= $v.'.';
                $vr[] = rtrim($vb,'. ');// for in https://stackoverflow.com/questions/5592994/remove-the-last-character-from-string
            }
            unset($vr[0], $vr[count($exp)-1]);
            $data = TaRABExt::find()->select(['Kd_Rincian'])->where(['in', 'Kd_Rincian', $vr])->andWhere(['Kd_Desa' => $this->Kd_Desa]);

            if($data->count() > 0){
                $txt = [];
                foreach ($data->all() as $v) {
                    $txt[] = $v->Kd_Rincian;
                }

                $this->rab_ext = implode('::',array_diff($vr,$txt));
                $this->rab_ext_ada = implode('::', $txt);
            } else {
                $this->rab_ext = implode('::',$vr);
                $this->rab_ext_ada = '';
            }
        }


        // fwrite($myfile, '1 ..... :'.$source.'-'.$dele.'-');
        $foo = explode('::', $this->rab_ext);
        if(count($foo) > 0 && $foo[0] != ''){
            $varr = [];
            $userid = Yii::$app->user->identity->id;
            $dating = date('Y-m-d H:i:s');
            foreach ($foo as $v) {
                $varr[] = [
                    $this->Tahun, $this->Kd_Desa, $this->kd_kecamatan, $this->Kd_Desa.$v, $v, $this->pendapatan_x, 0, $this->pendapatan_x, $this->sumberdana, $this->kegiatan, $userid, $dating
                ];
            }

            Yii::$app->db->createCommand()
            ->batchInsert('Ta_RAB_Ext', ['Tahun','Kd_Desa','kd_kecamatan','Kd_Keg','Kd_Rincian','Anggaran','AnggaranPAK','AnggaranStlhPAK','sumberdana','kegiatan','user_id', 'tgl_submit'],$varr)
            ->execute();
            // recalculate
            $sql = '';
            foreach ($foo as $v) {
                $sql .= "UPDATE Ta_RAB_Ext a INNER JOIN (SELECT SUM(Anggaran) AS Ang FROM Ta_RAB WHERE Tahun = '".$this->Tahun."' AND Kd_Desa = '".$this->Kd_Desa."' AND Kd_Rincian LIKE '".$v.".%') b SET a.Anggaran = b.Ang, a.AnggaranStlhPAK = b.Ang WHERE Tahun = '".$this->Tahun."' AND Kd_Desa = '".$this->Kd_Desa."' Kd_Rincian = '".$v."';";
                // fwrite($myfile, '1 ..... :'.$v.'-'.$dele.'-');
            }

            Yii::$app->db->createCommand($sql)->execute();
        }


        $foo2 = explode('::', trim($this->rab_ext_ada));
        if(count($foo2) > 0 && $foo2[0] != ''){
            $sql = '';
            foreach ($foo2 as $v) {
                $sql .= "UPDATE Ta_RAB_Ext a INNER JOIN (SELECT SUM(Anggaran) AS Ang FROM Ta_RAB WHERE Tahun = '".$this->Tahun."' AND Kd_Desa = '".$this->Kd_Desa."' AND Kd_Rincian LIKE '".$v.".%') b SET a.Anggaran = b.Ang, a.AnggaranStlhPAK = b.Ang WHERE Tahun = '".$this->Tahun."' AND Kd_Desa = '".$this->Kd_Desa."' AND Kd_Rincian = '".$v."';";
            }

            Yii::$app->db->createCommand($sql)->execute();
        }
        // fclose($myfile);
    }

    public function insertLangsungRinci()
    {
        $m = new TaRABRinci();
        $m->Tahun = $this->Tahun;
        $m->Kd_Desa = $this->Kd_Desa;
        $m->kd_kecamatan = $this->kd_kecamatan;
        $m->Kd_Keg = $this->Kd_Keg;
        $m->Kd_Rincian = $this->Kd_Rincian;
        $m->Kd_SubRinci = $m->No_Keg = $m->No_Urut = '01';
        $m->SumberDana = $this->sumberdana;
        $m->Uraian = '---'.$this->kegiatan.'---';
        $m->Satuan = 'Tahun';
        $m->JmlSatuanPAK = $m->JmlSatuan = '1';
        $m->HrgSatuanPAK = $m->Anggaran = $m->AnggaranStlhPAK = $m->HrgSatuan = $this->pendapatan_x;
        $m->AnggaranPAK = 0;
        $m->jenis_belanja = $this->jenis_belanja;//khusu belanja saja
        $m->kegiatan = $this->kegiatan;
        $m->save();
    }

    public function updateLangsungRinci()
    {
        $m = TaRABRinci::findOne(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa, 'Kd_Rincian' => $this->Kd_Rincian]);
        $m->SumberDana = $this->sumberdana;
        $m->HrgSatuanPAK = $m->Anggaran = $m->AnggaranStlhPAK = $m->HrgSatuan = $this->pendapatan_x;
        $m->save();
    }

    public function updatingRabDesaCount()
    {
        $total = \sp3d\models\transaksi\TaRAB::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa, 'kegiatan' => $this->kegiatan]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);

        if($this->kegiatan == 'pendapatan'){
            $mod->setChange(true, ['data' => 'data_pendapatan', 'dana' => 'dana_pendapatan']);
            $mod->dana_pendapatan = $total->sum('Anggaran');
            $mod->data_pendapatan = $total->count();
        } else if($this->kegiatan == 'penerimaan'){
            $mod->setChange(true, ['data' => 'data_penerimaan', 'dana' => 'dana_penerimaan']);
            $mod->dana_penerimaan = $total->sum('Anggaran');
            $mod->data_penerimaan = $total->count();
        } else if($this->kegiatan == 'pengeluaran'){
            $mod->setChange(true, ['data' => 'data_pengeluaran', 'dana' => 'dana_pengeluaran']);
            $mod->dana_pengeluaran = $total->sum('Anggaran');
            $mod->data_pengeluaran = $total->count();
        } else {//belanja
            $mod->setChange(true, ['data' => 'data_rab', 'dana' => 'dana_rab']);
            $mod->dana_rab = $total->sum('Anggaran');
            $mod->data_rab = $total->count();
        }
        $mod->save(false);
    }
    //event
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    } 

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        TaRABRinci::deleteAll(['Kd_Desa' => $this->Kd_Desa, 'Kd_Keg' => $this->Kd_Keg,'Kd_Rincian' => $this->Kd_Rincian, 'Tahun' => $this->Tahun]);    
        return true;
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $this->recalculateRabExt(true);//dele = true, in belanja dele true == belanja true
        $this->updatingRabDesaCount();
    }

    public function afterSave($ins, $old)
    {
        if($ins){
            //revisi 01.01
            if($this->kegiatan !== 'belanja'){
                $this->insertLangsungRinci();
            }
        } else {
            if($this->kegiatan !== 'belanja'){
                $this->updateLangsungRinci();
            }
        }

        if($this->kegiatan !== 'belanja'){
            $this->recalculateRabExt();
        } else {
            //belanja
            $this->recalculateRabExt(false, true);//2 parameter for assign belanja condtion
        }        
    }
}