<?php
namespace sp3d\models\transaksi;

use Yii;
use sp3d\models\DesaCount;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_Bidang".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Bid
 * @property string $Nama_Bidang
 */
class TaBidang extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Bidang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Bid'], 'required'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['imp_id'], 'safe'],
            [['f_verified'], 'integer'],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Bid'], 'string', 'max' => 10],
            [['Nama_Bidang'], 'string', 'max' => 100]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Bid' => 'Kode Bidang',
            'Nama_Bidang' => 'Nama  Bidang',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function afterDelete()
    {
        $this->updatingBidangDesaCount();
    }

    public function afterSave($ins, $old)
    {
        // if($ins){
            $this->updatingBidangDesaCount();
        // }
    }

    public function updatingBidangDesaCount()
    {
        $total = \sp3d\models\transaksi\TaBidang::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_bidang']);
        $mod->data_bidang = $total->count();
        $mod->save(false);
    }
    //relation
    public function getKegiatan()//for kecamatan up..
    {
        return $this->hasMany(TaKegiatan::className(), ['Kd_Bid' => 'Kd_Bid']);
    }
}
