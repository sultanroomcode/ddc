<?php

namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_RPJM_Misi".
 *
 * @property string $ID_Misi
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $ID_Visi
 * @property string $No_Misi
 * @property string $Uraian_Misi
 */
class TaRPJMMisi extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Misi';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_Misi'], 'required'],
            [['ID_Misi'], 'string', 'max' => 12],
            [['TglPosting'], 'safe'],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['ID_Visi'], 'string', 'max' => 10],
            [['No_Misi'], 'string', 'max' => 2],
            [['Uraian_Misi'], 'string', 'max' => 250]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_Misi' => 'ID Misi',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'ID_Visi' => 'ID Visi',
            'No_Misi' => 'No. Misi',
            'Uraian_Misi' => 'Uraian Misi',

            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function setNewNumber()
    {
        $mod = \sp3d\models\transaksi\TaRPJMMisi::find()->where(['Kd_Desa' => $this->Kd_Desa, 'kd_kecamatan' => $this->kd_kecamatan]);
        if($mod->count() > 0){
            $counter = (int) $mod->max('No_Misi');
            $counter++;
            $this->No_Misi = sprintf('%02d', $counter);
        } else {
            $this->No_Misi = sprintf('%02d', 1);  
        }        
    }

    public function getVisi()//for kecamatan up..
    {
        return $this->hasOne(TaRPJMVisi::className(), ['ID_Visi' => 'ID_Visi']);
    }

    public function getTujuan()//for kecamatan up..
    {
        return $this->hasMany(TaRPJMTujuan::className(), ['ID_Misi' => 'ID_Misi']);
    }
}
