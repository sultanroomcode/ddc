<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_SPJPot".
 *
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $No_SPJ
 * @property string $Kd_Keg
 * @property string $No_Bukti
 * @property string $Kd_Rincian
 * @property double $Nilai
 */
class TaSPJPot extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPJPot';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_SPJ', 'Kd_Keg', 'No_Bukti', 'Kd_Rincian'], 'required'],
            [['Nilai'], 'number'],
            [['imp_id'], 'safe'],
            [['Tahun'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['No_SPJ', 'No_Bukti'], 'string', 'max' => 30],
            [['Kd_Keg', 'Kd_Rincian'], 'string', 'max' => 18]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'No_SPJ' => 'No. SPJ',
            'Kd_Keg' => 'Kode Kegiatan',
            'No_Bukti' => 'No. Bukti',
            'Kd_Rincian' => 'Kode Rincian',
            'Nilai' => 'Nilai',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
