<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_SPP".
 *
 * @property string $Tahun
 * @property string $No_SPP
 * @property string $Tgl_SPP
 * @property string $Jn_SPP
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Keterangan
 * @property double $Jumlah
 * @property double $Potongan
 * @property string $Status
 */
class TaSPP extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_SPP';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['No_SPP'], 'required'],
            [['Tgl_SPP'], 'safe'],
            [['imp_id'], 'safe'],
            [['Jumlah', 'Potongan'], 'number'],
            [['Tahun', 'Jn_SPP'], 'string', 'max' => 4],
            [['No_SPP'], 'string', 'max' => 30],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Keterangan'], 'string', 'max' => 250],
            [['Status'], 'string', 'max' => 1],
            [['No_SPP'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_SPP' => 'No. Spp',
            'Tgl_SPP' => 'Tanggal Spp',
            'Jn_SPP' => 'Jn Spp',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Keterangan' => 'Keterangan',
            'Jumlah' => 'Jumlah',
            'Potongan' => 'Potongan',
            'Status' => 'Status',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getBukti()//for kecamatan up..
    {
        return $this->hasMany(TaSPPBukti::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getPotongan()//for kecamatan up..
    {
        return $this->hasMany(TaSPPPot::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getRincian()//for kecamatan up..
    {
        return $this->hasMany(TaSPPRinci::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function afterDelete()
    {
        $this->updatingSppDesaCount();
    }

    public function afterSave($ins, $old)
    {
        // if($ins){
            $this->updatingSppDesaCount();
        // }
    }

    public function updatingSppDesaCount()
    {
        $total = \sp3d\models\transaksi\TaSPP::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->setChange(true, ['data' => 'data_spp', 'dana' => 'dana_spp']);
        $mod->dana_spp = $total->sum('Jumlah');
        $mod->data_spp = $total->count();
        $mod->save(false);
    }
}
