<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_RPJM_Pagu_Tahunan".
 *
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Tahun
 * @property string $Kd_Sumber
 * @property double $Biaya
 * @property double $Volume
 * @property string $Satuan
 * @property string $Lokasi_Spesifik
 * @property double $Jml_Sas_Pria
 * @property double $Jml_Sas_Wanita
 * @property double $Jml_Sas_ARTM
 * @property string $Waktu
 * @property string $Mulai
 * @property string $Selesai
 * @property string $Pola_Kegiatan
 * @property string $Pelaksana
 */
class TaRPJMPaguTahunan extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Pagu_Tahunan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Keg', 'Kd_Tahun', 'Kd_Sumber'], 'required'],
            [['Biaya', 'Volume', 'Jml_Sas_Pria', 'Jml_Sas_Wanita', 'Jml_Sas_ARTM'], 'number'],
            [['Mulai', 'Selesai'], 'safe'],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Tahun', 'Kd_Sumber'], 'string', 'max' => 4],
            [['Satuan'], 'string', 'max' => 20],
            [['Lokasi_Spesifik', 'Pelaksana'], 'string', 'max' => 50],
            [['Waktu', 'Pola_Kegiatan'], 'string', 'max' => 30]
        ];
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Tahun' => 'Kode Tahun',
            'Kd_Sumber' => 'Kode Sumber',
            'Biaya' => 'Biaya',
            'Volume' => 'Volume',
            'Satuan' => 'Satuan',
            'Lokasi_Spesifik' => 'Lokasi  Spesifik',
            'Jml_Sas_Pria' => 'Jumlah SAS Pria',
            'Jml_Sas_Wanita' => 'Jumlah SAS Wanita',
            'Jml_Sas_ARTM' => 'Jumlah SAS Artm',
            'Waktu' => 'Waktu',
            'Mulai' => 'Mulai',
            'Selesai' => 'Selesai',
            'Pola_Kegiatan' => 'Pola Kegiatan',
            'Pelaksana' => 'Pelaksana',

            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
