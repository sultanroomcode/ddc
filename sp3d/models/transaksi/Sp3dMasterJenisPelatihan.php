<?php
namespace sp3d\models\transaksi;

use Yii;

/**
 * This is the model class for table "sp3d_master_jabatan".
 *
 * @property string $id
 * @property string $nama_jabatan
 * @property string $description
 * @property integer $status
 */
class Sp3dMasterJenisPelatihan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp3d_master_jenis_pelatihan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['description', 'tipe'], 'string'],
            [['status', 'urut'], 'integer'],
            [['id', 'nama_pelatihan'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pelatihan' => 'Nama Pelatihan',
            'tipe' => 'Tipe Pelatihan',
            'description' => 'Description',
            'status' => 'Status',
            'urut' => 'Urutan',
        ];
    }

    public function getTipejabatan()
    {
        return $this->hasOne(Sp3dMasterJabatanTipe::className(), ['id' => 'tipe']);
    }
}
