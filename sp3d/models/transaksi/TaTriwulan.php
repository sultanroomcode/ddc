<?php
namespace sp3d\models\transaksi;

use Yii;
use sp3d\models\referensi\RefDesa;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_Triwulan".
 *
 * @property string $KURincianSD
 * @property string $Tahun
 * @property string $Sifat
 * @property string $SumberDana
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property double $Anggaran
 * @property double $AnggaranPAK
 * @property double $Tw1Rinci
 * @property double $Tw2Rinci
 * @property double $Tw3Rinci
 * @property double $Tw4Rinci
 * @property double $KunciData
 */
class TaTriwulan extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Triwulan';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['KURincianSD', 'Tahun'], 'required'],
            [['Anggaran', 'AnggaranPAK', 'Tw1Rinci', 'Tw2Rinci', 'Tw3Rinci', 'Tw4Rinci', 'KunciData'], 'number'],
            [['KURincianSD'], 'string', 'max' => 50],
            [['imp_id'], 'safe'],
            [['Tahun', 'SumberDana'], 'string', 'max' => 4],
            [['Sifat'], 'string', 'max' => 6],
            [['Kd_Desa'], 'string', 'max' => 12],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 18],
            [['Kd_Rincian'], 'string', 'max' => 14],
            [['KURincianSD'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'KURincianSD' => 'Kurincian Sd',
            'Tahun' => 'Tahun',
            'Sifat' => 'Sifat',
            'SumberDana' => 'Sumber Dana',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Keg' => 'Kode Kegiatan',
            'Kd_Rincian' => 'Kode Rincian',
            'Anggaran' => 'Anggaran',
            'AnggaranPAK' => 'Anggaran Pak',
            'Tw1Rinci' => 'Tw1 Rinci',
            'Tw2Rinci' => 'Tw2 Rinci',
            'Tw3Rinci' => 'Tw3 Rinci',
            'Tw4Rinci' => 'Tw4 Rinci',
            'KunciData' => 'Kunci Data',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getArsip()//for kecamatan up..
    {
        return $this->hasMany(TaTriwulanArsip::className(), ['Kd_Desa' => 'Kd_Desa']);
    }
}
