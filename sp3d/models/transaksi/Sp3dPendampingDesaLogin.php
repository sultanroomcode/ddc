<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use sp3d\models\User;
use sp3d\models\AuthAssignment;
/**
 * This is the model class for table "sp3d_pendamping_desa_login".
 *
 * @property string $id
 * @property string $email
 * @property string $email_code
 * @property string $password
 * @property integer $status
 * @property string $created_at
 * @property string $verified_at
 */
class Sp3dPendampingDesaLogin extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $verifyCode;
    public static function tableName()
    {
        return 'sp3d_pendamping_desa_login';
    }

    public function behaviors()
     {
         return [
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['status'], 'integer'],
            [['verified_at'], 'safe'],
            [['id'], 'string', 'max' => 10],
            [['email', 'email_code'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 255],
            ['verifyCode', 'captcha', 'captchaAction' => 'pendamping/default/captcha', 'on' => 'registrasi'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
        	if($this->isNewRecord){
	        	$this->id = time();
	        	$this->status = 0;
	        	$this->password = Yii::$app->security->generateRandomString();
	        	$this->email_code = Yii::$app->security->generateRandomString();
	        	$this->verified_at = date('1000-01-01 00:00');	        	
	        }

            return true;
        } else {
            return false;
        }
    } 

    public function afterSave($ins, $old)
    {
        if($ins){
        	//only on register    
        	$this->sendEmailVerification();
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'email_code' => 'Email Code',
            'password' => 'Password',
            'status' => 'Status',
            'created_at' => 'Created At',
            'verified_at' => 'Verified At',
        ];
    }
    //localhost/yii/siapatakut_sv/sp3d/web/pendamping/default/verify-register?mail=agussutarom@gmail.com&registrycode=3Dc04586ae41ca9f64ef24be90987cdce8
    public function loadDataToUserThenLogin(){
    	$user = new User();
        $user->id = $user->description = $this->id;
        $user->type = 'pendamping';
        $user->username = $user->email = $this->email;
        $user->password_default = $this->password;
        $user->status = $this->status;
        $user->created_at = $user->updated_at = time();
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if ($user->save(false)) {
        	//membuat assigment baru
            $u_a = new AuthAssignment();
            $u_a->item_name = 'pendamping';
            $u_a->user_id = (string) $this->id;
            $u_a->created_at = time();
            if(!$u_a->save()) throw new Exception('Gagal isi assigment '. print_r($u_a->errors));
            
            Yii::$app->user->login($user, true? 3600 * 24 * 30 : 0);
            //return $user;

            //belum assignment ke auth_assign
        }
    }

    public function sendEmailVerification(){
        return Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom(['noreply@sp3d.dpmd.jatimprov.go.id' => 'SP3D System Server'])
            ->setSubject('validasi pendaftaran pendamping')
            ->setHtmlBody('<b>Pendaftaran Sukses</b><br>Anda telah mendaftar untuk pengisian biodata Pendamping Desa untuk Sistem Aplikasi SP3D Provinsi Jawa Timur, untuk memvalidasi email <a href="https://sp3d.dpmd.jatimprov.go.id/pendamping/default/verify-register?mail='.$this->email.'&registrycode='.md5($this->email_code).'" target="_blank">klik disini</a>. abaikan email ini jika anda tidak merasa mendaftarkannya')
            ->send();
    }
    //relation
    public function getUser()//rekening
    {
        return $this->hasOne(User::className(), ['id' => 'id'])->onCondition(['user.type' => 'pendamping']);
    }

    public function getDataext()//rekening
    {
        return $this->hasOne(Sp3dPendampingDesa::className(), ['user' => 'id']);
    }
}
