<?php
namespace sp3d\models\transaksi;
use sp3d\models\referensi\RefDesa;
use sp3d\models\DesaCount;
use Yii;

use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_Anggaran".
 *
 * @property string $KdPosting
 * @property string $Tahun
 * @property string $KURincianSD
 * @property string $Kd_Rincian
 * @property string $RincianSD
 * @property double $Anggaran
 * @property double $AnggaranPAK
 * @property double $AnggaranStlhPAK
 * @property string $Belanja
 * @property string $Kd_Keg
 * @property string $SumberDana
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $TglPosting
 */
class TaAnggaran extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Anggaran';
    }

    public function behaviors()
    {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
    }

    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['KdPosting', 'Tahun', 'KURincianSD', 'kd_kecamatan' , 'Kd_Desa','Kd_Keg'], 'required'],
            [['Anggaran', 'AnggaranPAK', 'AnggaranStlhPAK'], 'number'],
            [['TglPosting', 'imp_id'], 'safe'],
            [['KdPosting'], 'string', 'max' => 1],
            [['Tahun', 'SumberDana'], 'string', 'max' => 4],
            [['KURincianSD'], 'string', 'max' => 40],
            [['Kd_Rincian'], 'string', 'max' => 13],
            [['RincianSD'], 'string', 'max' => 16],
            [['Belanja'], 'string', 'max' => 5],
            [['Kd_Keg'], 'string', 'max' => 20],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Desa'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'KdPosting' => 'Kode Posting',//primary
            'Tahun' => 'Tahun',//primary
            'KURincianSD' => 'Kurincian Sd',//primary
            'Kd_Rincian' => 'Kode Rincian',
            'RincianSD' => 'Rincian Sd',
            'Anggaran' => 'Anggaran',
            'AnggaranPAK' => 'Anggaran PAK (Peninjauan Anggaran Kembali)',
            'AnggaranStlhPAK' => 'Anggaran Setelah PAK (Peninjauan Anggaran Kembali)',
            'Belanja' => 'Belanja',
            'Kd_Keg' => 'Kode  Kegiatan',
            'SumberDana' => 'Sumber Dana',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'TglPosting' => 'Tanggal Posting',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function updatingDesaCount($data)
    {
        
    }

    public function afterDelete()
    {
        $this->updatingRabAnggaranDesaCount();
    }

    public function afterSave($ins, $old)
    {
        if($ins){
            $this->updatingAnggaranDesaCount();
        }
    }

    public function updatingAnggaranDesaCount()
    {
        $total = \sp3d\models\transaksi\TaAnggaran::find()->where(['Tahun' => $this->Tahun, 'Kd_Desa' => $this->Kd_Desa]);

        $mod = DesaCount::findOne(['tahun' => $this->Tahun, 'kd_desa' => $this->Kd_Desa]);
        $mod->dana_anggaran = $total->sum('Anggaran');
        $mod->data_anggaran = $total->count();
        $mod->save(false);
    }

    public function getDesa()//for kecamatan up..
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'Kd_Desa']);
    }

    public function getKegiatan()//for kecamatan up..
    {
        return $this->hasOne(TaKegiatan::className(), ['Kd_Keg' => 'Kd_Keg']);
    }

    public function getLog()
    {
        return $this->hasMany(TaAnggaranLog::className(), ['KdPosting' => 'KdPosting']);
        //tambahkan onCondition
    }

    public function getRincian()
    {
        return $this->hasMany(TaAnggaranRincian::className(), ['KdPosting' => 'KdPosting']);   
    }
}
