<?php
namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Ta_RPJM_Sasaran".
 *
 * @property string $ID_Sasaran
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $ID_Tujuan
 * @property string $No_Sasaran
 * @property string $Uraian_Sasaran
 */
class TaRPJMSasaran extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Sasaran';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_Sasaran'], 'required'],
            [['ID_Sasaran'], 'string', 'max' => 18],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['ID_Tujuan'], 'string', 'max' => 16],
            [['No_Sasaran'], 'string', 'max' => 2],
            [['Uraian_Sasaran'], 'string', 'max' => 250]
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_Sasaran' => 'ID Sasaran',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'ID_Tujuan' => 'ID Tujuan',
            'No_Sasaran' => 'No. Sasaran',
            'Uraian_Sasaran' => 'Uraian Sasaran',

            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function setNewNumber()
    {
        $mod = \sp3d\models\transaksi\TaRPJMSasaran::find()->where(['Kd_Desa' => $this->Kd_Desa, 'kd_kecamatan' => $this->kd_kecamatan]);
        if($mod->count() > 0){
            $counter = (int) $mod->max('No_Sasaran');
            $counter++;
            $this->No_Sasaran = sprintf('%02d', $counter);
        } else {
            $this->No_Sasaran = sprintf('%02d', 1);  
        }        
    }

    public function getTujuan()//for kecamatan up..
    {
        return $this->hasOne(TaRPJMTujuan::className(), ['ID_Tujuan' => 'ID_Tujuan']);
    }
}
