<?php
namespace sp3d\models\transaksi;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "Ta_RPJM_Tujuan".
 *
 * @property string $ID_Tujuan
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $ID_Misi
 * @property string $No_Tujuan
 * @property string $Uraian_Tujuan
 */
class TaRPJMTujuan extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RPJM_Tujuan';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 'updatedByAttribute' => 'user_id',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_Tujuan'], 'required'],
            [['ID_Tujuan'], 'string', 'max' => 16],
            [['Kd_Desa'], 'string', 'max' => 8],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['ID_Misi'], 'string', 'max' => 12],
            [['No_Tujuan'], 'string', 'max' => 2],
            [['Uraian_Tujuan'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_Tujuan' => 'ID Tujuan',
            'Kd_Desa' => 'Kode Desa',
            'kd_kecamatan' => 'Kode Kecamatan',
            'ID_Misi' => 'ID Misi',
            'No_Tujuan' => 'No. Tujuan',
            'Uraian_Tujuan' => 'Uraian Tujuan',

            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }

    public function setNewNumber()
    {
        $mod = \sp3d\models\transaksi\TaRPJMTujuan::find()->where(['Kd_Desa' => $this->Kd_Desa, 'kd_kecamatan' => $this->kd_kecamatan]);
        if($mod->count() > 0){
            $counter = (int) $mod->max('No_Tujuan');
            $counter++;
            $this->No_Tujuan = sprintf('%02d', $counter);
        } else {
            $this->No_Tujuan = sprintf('%02d', 1);  
        }        
    }

    public function getMisi()//for kecamatan up..
    {
        return $this->hasOne(TaRPJMMisi::className(), ['ID_Misi' => 'ID_Misi']);
    }

    public function getSasaran()//for kecamatan up..
    {
        return $this->hasMany(TaRPJMSasaran::className(), ['ID_Tujuan' => 'ID_Tujuan']);
    }
}
