<?php

namespace sp3d\models\transaksi;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "Ta_AnggaranRinci".
 *
 * @property string $KdPosting
 * @property string $Tahun
 * @property string $Kd_Desa
 * @property string $kd_kecamatan
 * @property string $Kd_Keg
 * @property string $Kd_Rincian
 * @property string $Kd_SubRinci
 * @property string $No_Urut
 * @property string $Uraian
 * @property string $SumberDana
 * @property double $JmlSatuan
 * @property double $HrgSatuan
 * @property string $Satuan
 * @property double $Anggaran
 * @property double $JmlSatuanPAK
 * @property double $HrgSatuanPAK
 * @property double $AnggaranStlhPAK
 * @property double $AnggaranPAK
 */
class TaAnggaranRinci extends ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_AnggaranRinci';
    }

    public function behaviors()
     {
         return [
             [
                 'class' => BlameableBehavior::className(),
                 'createdByAttribute' => 'user_id',
                 //'updatedByAttribute' => 'updated_by',
             ],
             'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['tgl_submit'],
                     // ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
             ],
         ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['KdPosting', 'Tahun', 'Kd_Desa', 'Kd_Keg', 'Kd_Rincian', 'Kd_SubRinci', 'No_Urut', 'SumberDana'], 'required'],
            [['JmlSatuan', 'HrgSatuan', 'Anggaran', 'JmlSatuanPAK', 'HrgSatuanPAK', 'AnggaranStlhPAK', 'AnggaranPAK'], 'number'],            
            [['KdPosting'], 'string', 'max' => 1],
            [['imp_id'], 'safe'],
            [['Tahun', 'SumberDana'], 'string', 'max' => 4],
            [['Kd_Desa'], 'string', 'max' => 16],
            [['kd_kecamatan'], 'string', 'max' => 8],
            [['Kd_Keg'], 'string', 'max' => 20],
            [['Kd_Rincian'], 'string', 'max' => 13],
            [['Kd_SubRinci'], 'string', 'max' => 2],
            [['No_Urut'], 'string', 'max' => 3],
            [['Uraian'], 'string', 'max' => 54],
            [['Satuan'], 'string', 'max' => 15]
        ];
    }

    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'KdPosting' => 'Kode Posting',//primary
            'Tahun' => 'Tahun',//primary
            'Kd_Desa' => 'Kode  Desa',//primary
            'kd_kecamatan' => 'Kode Kecamatan',
            'Kd_Keg' => 'Kode Keg',
            'Kd_Rincian' => 'Kode Rincian',
            'Kd_SubRinci' => 'Kode Sub Rinci',
            'No_Urut' => 'No Urut',
            'Uraian' => 'Uraian',
            'SumberDana' => 'Sumber Dana',
            'JmlSatuan' => 'Jumlah Satuan',
            'HrgSatuan' => 'Harga Satuan',
            'Satuan' => 'Satuan',
            'Anggaran' => 'Anggaran',
            'JmlSatuanPAK' => 'Jumlah Satuan PAK',
            'HrgSatuanPAK' => 'Harga Satuan PAK',
            'AnggaranStlhPAK' => 'Anggaran Setelah PAK',
            'AnggaranPAK' => 'Anggaran PAK',

            'imp_id' => 'Kode Import',
            'user_id' => 'ID User',
            'tgl_submit' => 'Tanggal Submit',
        ];
    }
}
