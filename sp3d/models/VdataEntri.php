<?php
namespace sp3d\models;

use Yii;

/**
 * This is the model class for table "vdata_entri".
 *
 * @property string $fdata
 * @property int $c
 * @property string $tahun
 */
class VdataEntri extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vdata_entri';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c'], 'integer'],
            [['fdata'], 'string', 'max' => 16],
            [['tahun'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fdata' => 'Data',
            'c' => 'Hitung',
            'tahun' => 'Tahun',
        ];
    }
}
