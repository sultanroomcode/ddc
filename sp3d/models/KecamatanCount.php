<?php
namespace sp3d\models;

use Yii;

/**
 * This is the model class for table "desa_count".
 *
 * @property string $tahun
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $dana_anggaran
 * @property integer $dana_pencairan
 * @property integer $dana_kegiatan
 */
class KecamatanCount extends \yii\db\ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kecamatan_count';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['tahun', 'kd_provinsi', 'kd_kabupaten', 'kd_kecamatan'], 'required'],
            [['dana_anggaran', 'dana_pencairan', 'dana_kegiatan','dana_mutasi', 'dana_pajak','dana_rab', 'dana_spj', 'dana_spp', 'dana_sts', 'dana_tbp', 'dana_pendapatan', 'dana_penerimaan', 'dana_pengeluaran', 'data_anggaran', 'data_pencairan', 'data_kegiatan','data_bidang', 'data_mutasi', 'data_pajak','data_rab', 'data_spj', 'data_spp', 'data_sts', 'data_tbp', 'data_pendapatan','data_penerimaan', 'data_pengeluaran'], 'default', 'value' => 0, 'isEmpty' => true, 'when' => function($model){ $model->isNewRecord; } ],

            [['dana_anggaran', 'dana_pencairan', 'dana_kegiatan','dana_mutasi', 'dana_pajak','dana_rab', 'dana_spj', 'dana_spp', 'dana_sts', 'dana_tbp', 'dana_pendapatan', 'dana_penerimaan', 'dana_pengeluaran'], 'integer'],

            [['data_anggaran', 'data_pencairan', 'data_kegiatan','data_bidang', 'data_mutasi', 'data_pajak','data_rab', 'data_spj', 'data_spp', 'data_sts', 'data_tbp', 'data_pendapatan','data_penerimaan', 'data_pengeluaran'], 'integer'],
            [['tahun'], 'string', 'max' => 4],
            [['kd_provinsi', 'kd_kecamatan', 'kd_kabupaten'], 'string', 'max' => 10],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun' => 'Tahun',
            'kd_provinsi' => 'Kode Provinsi',
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_kecamatan' => 'Kode Kecamatan',
            'dana_anggaran' => 'Dana Anggaran',
            'dana_pencairan' => 'Dana Pencairan',
            'dana_kegiatan' => 'Dana Kegiatan',
            'data_anggaran' => 'Data Anggaran',
            'data_pencairan' => 'Data Pencairan',
            'data_kegiatan' => 'Data Kegiatan',
        ];
    }
    //on afterUpdate only

    public function getKabupatenLabel()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatanLabel()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }
}
