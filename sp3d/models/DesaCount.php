<?php
namespace sp3d\models;

use sp3d\models\referensi\RefDesa;
use sp3d\models\transaksi\TaDesa;
use Yii;

/**
 * This is the model class for table "desa_count".
 *
 * @property string $tahun
 * @property string $kd_kabupaten
 * @property string $kd_kecamatan
 * @property string $kd_desa
 * @property integer $dana_anggaran
 * @property integer $dana_pencairan
 * @property integer $dana_kegiatan
 */
class DesaCount extends \yii\db\ActiveRecord
{
    use \sp3d\models\TraitFormat;
    /**
     * @inheritdoc
     */
    public $yang_diubah, $perlu_diubah;
    public static function tableName()
    {
        return 'desa_count';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['tahun', 'kd_provinsi', 'kd_kabupaten', 'kd_kecamatan', 'kd_desa'], 'required'],
            
            [['dana_anggaran', 'dana_pencairan', 'dana_kegiatan','dana_mutasi', 'dana_pajak','dana_rab', 'dana_spj', 'dana_spp', 'dana_sts', 'dana_tbp', 'dana_pendapatan', 'dana_penerimaan', 'dana_pengeluaran', 'data_anggaran', 'data_pencairan', 'data_kegiatan','data_bidang', 'data_mutasi', 'data_pajak','data_rab', 'data_spj', 'data_spp', 'data_sts', 'data_tbp', 'data_pendapatan','data_penerimaan', 'data_pengeluaran'], 'default', 'value' => 0, 'isEmpty' => true, 'when' => function($model){ $model->isNewRecord; } ],

            [['dana_anggaran', 'dana_pencairan', 'dana_kegiatan','dana_mutasi', 'dana_pajak','dana_rab', 'dana_spj', 'dana_spp', 'dana_sts', 'dana_tbp', 'dana_pendapatan', 'dana_penerimaan', 'dana_pengeluaran'], 'integer'],

            [['data_anggaran', 'data_pencairan', 'data_kegiatan','data_bidang', 'data_mutasi', 'data_pajak','data_rab', 'data_spj', 'data_spp', 'data_sts', 'data_tbp', 'data_pendapatan','data_penerimaan', 'data_pengeluaran'], 'integer'],
            [['tahun'], 'string', 'max' => 4],
            [['kd_kabupaten', 'kd_desa', 'kd_kecamatan'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun' => 'Tahun',
            'kd_provinsi' => 'Kode Provinsi',
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_desa' => 'Kode Desa',
            'dana_anggaran' => 'Dana Anggaran',
            'dana_pencairan' => 'Dana Pencairan',
            'dana_kegiatan' => 'Dana Kegiatan',
            'data_anggaran' => 'Data Anggaran',
            'data_pencairan' => 'Data Pencairan',
            'data_kegiatan' => 'Data Kegiatan',
        ];
    }
    //on afterUpdate only
    public function setChange($change, $data = [])
    {
        $this->perlu_diubah = $change;//true false
        $this->yang_diubah = $data;
    }
    
    public function getDesa()
    {
        return $this->hasOne(RefDesa::className(), ['Kd_Desa' => 'kd_desa']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa']);
    }

    public function getDesaRinci()
    {
        return $this->hasOne(TaDesa::className(), ['Kd_Desa' => 'kd_desa']);
    }

    public function afterSave($ins, $old)
    {
        if(!$ins && $this->perlu_diubah){//then update
            //do this
            $this->updateKecamatan();
        }
    }

    public function updateKecamatan(){
        $total = \sp3d\models\DesaCount::find()->where(['tahun' => $this->tahun, 'kd_kecamatan' => $this->kd_kecamatan]);
        $mod = KecamatanCount::findOne(['tahun' => $this->tahun, 'kd_kecamatan' => $this->kd_kecamatan]);
        $kode_kabupaten = $mod->kd_kabupaten;
        $kode_provinsi = $mod->kd_provinsi;
        if(isset($this->yang_diubah['dana'])){ $mod->{$this->yang_diubah['dana']} = $total->sum($this->yang_diubah['dana']); }
        if(isset($this->yang_diubah['data'])){ $mod->{$this->yang_diubah['data']} = $total->sum($this->yang_diubah['data']); }
        $mod->save(false);

        $total1 = KecamatanCount::find()->where(['tahun' => $this->tahun, 'kd_kabupaten' => $kode_kabupaten]);
        $mod2 = KabupatenCount::findOne(['tahun' => $this->tahun, 'kd_kabupaten' => $kode_kabupaten]);
        if(isset($this->yang_diubah['dana'])){ $mod2->{$this->yang_diubah['dana']} = $total1->sum($this->yang_diubah['dana']); }
        if(isset($this->yang_diubah['data'])){ $mod2->{$this->yang_diubah['data']} = $total1->sum($this->yang_diubah['data']); }
        $mod2->save(false);

        $total2 = KabupatenCount::find()->where(['tahun' => $this->tahun, 'kd_provinsi' => $kode_provinsi]);
        $mod3 = ProvinsiCount::findOne(['tahun' => $this->tahun, 'kd_provinsi' => $kode_provinsi]);
        if(isset($this->yang_diubah['dana'])){ $mod3->{$this->yang_diubah['dana']} = $total2->sum($this->yang_diubah['dana']); }
        if(isset($this->yang_diubah['data'])){ $mod3->{$this->yang_diubah['data']} = $total2->sum($this->yang_diubah['data']); }
        $mod3->save(false);
        //solusinya bisa pakai cara ini search di google jquery detect form which change by user
        //pendekatan ini juga bisa dicoba https://stackoverflow.com/questions/18461503/yii-on-update-detect-if-a-specific-ar-property-has-been-changed-on-beforesave
        // gunakan  metode afterFind() untuk menyimpan data lama
    }

    public function getKabupatenLabel()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kabupaten'])->onCondition(['type' => 'kabupaten']);
    }
    
    public function getKecamatanLabel()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_kecamatan'])->onCondition(['type' => 'kecamatan']);
    }

    public function getDesaLabel()
    {
        return $this->hasOne(User::className(), ['id' => 'kd_desa'])->onCondition(['type' => 'desa']);
    }
}
