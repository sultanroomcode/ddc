<?php
namespace sp3d\models;
use sp3d\models\transaksi\TaUserDesa;
use yii\base\InvalidParamException;
use yii\base\Exception;
use yii\base\Model;
use Yii;
/**
 * Model ini digunakan admin untuk membuat pengguna baru
 */
class IsiPengguna extends Model
{
    public $username, $email, $status, $password;
    public $kd_assignment, $kd_kecamatan, $kd_desa;

    public function rules()
    {
        return [
            [['username', 'email', 'status', 'password', 'kd_assignment'], 'required'],
            [['kd_kecamatan'], 'required', 'when' => function($m){ return $m->kd_assignment == 'kecamatan'; }, 'whenClient' => "function(attribute, value){ return $('#isipengguna-kd_assigment').val() == 'kecamatan'; }"],
            [['kd_kecamatan', 'kd_desa'], 'required', 'when' => function($m){ return $m->kd_assignment == 'desa'; }, 'whenClient' => "function(attribute, value){ return $('#isipengguna-kd_assigment').val() == 'desa'; }"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tahun' => 'Tahun',
            'kd_kabupaten' => 'Kode Kabupaten',
            'kd_kecamatan' => 'Kode Kecamatan',
            'kd_desa' => 'Kode Desa',
            'dana_anggaran' => 'Dana Anggaran',
            'dana_pencairan' => 'Dana Pencairan',
            'dana_kegiatan' => 'Dana Kegiatan',
        ];
    }

    public function savePengguna()
    {
        //isi user
        $usermodel = User::findOne(['username' => $this->username, 'email' => $this->email]);
        if($usermodel == null){
            $u = new User();
            $u->username = $this->username;
            $u->email = $this->email;
            $u->status = $this->status;
            $u->setPassword($this->password);
            $u->generateAuthKey();
            $mdl = $u->save(false);
        //isi aut assignment
            $u_a = new AuthAssignment();
            $u_a->item_name = $this->kd_assignment;
            $u_a->user_id = (string) $u->id;
            $u_a->created_at = time();
            $u_a->save();
            //membuat koneksi antara User dan RefDesa
            if($this->kd_assignment == 'kecamatan'){
                $u_t = new \sp3d\models\transaksi\TaUserDesa();
                $u_t->Id = $u->id;
                $u_t->Kd_Kec = $this->kd_kecamatan;
                $u_t->Kd_Desa = '-';
                $u_t->save();
            }

            if($this->kd_assignment == 'desa'){
                $u_t = new \sp3d\models\transaksi\TaUserDesa();
                $u_t->Id = $u->id;
                $u_t->Kd_Kec = $this->kd_kecamatan;
                $u_t->Kd_Desa = $this->kd_desa;
                $u_t->save();

                $u_dc = new DesaCount();
                $u_dc->tahun = date('Y');
                $u_dc->kd_provinsi = '35';
                $u_dc->kd_kabupaten = '35.23';
                $u_dc->kd_kecamatan = $this->kd_kecamatan;
                $u_dc->kd_desa = $this->kd_desa;
                $u_dc->data_bidang = 5;
                $u_dc->dana_pencairan = $u_dc->dana_kegiatan = $u_dc->dana_anggaran = 0;
                $u_dc->save();

                //check kecamatan count
                $checker = KecamatanCount::findOne(['tahun' => $u_dc->tahun,'kd_provinsi' => $u_dc->kd_provinsi, 'kd_kabupaten' => $u_dc->kd_kabupaten, 'kd_kecamatan' => $u_dc->kd_kecamatan]);

                if($checker == null){
                    //insert
                    $ikec = new KecamatanCount();
                    $ikec->tahun = date('Y');
                    $ikec->kd_provinsi = $u_dc->kd_provinsi;
                    $ikec->kd_kabupaten = $u_dc->kd_kabupaten;
                    $ikec->kd_kecamatan = $u_dc->kd_kecamatan;
                    $ikec->save(false);
                }
                //batch insert tabidang dan tarpjm bidang
                $fields = ['Tahun','Kd_Desa','kd_kecamatan','Kd_Bid','Nama_Bidang'];
                $db = Yii::$app->db; 
                $rows = [
                    [$u_dc->tahun, $u_dc->kd_desa, $u_dc->kd_kecamatan, $u_dc->kd_desa.'01', 'Bidang Penyelenggaraan Pemerintah Desa'],
                    [$u_dc->tahun, $u_dc->kd_desa, $u_dc->kd_kecamatan, $u_dc->kd_desa.'02', 'Bidang Pelaksanaan Pembangunan Desa'],
                    [$u_dc->tahun, $u_dc->kd_desa, $u_dc->kd_kecamatan, $u_dc->kd_desa.'03', 'Bidang Pembinaan Kemasyarakatan Desa'],
                    [$u_dc->tahun, $u_dc->kd_desa, $u_dc->kd_kecamatan, $u_dc->kd_desa.'04', 'Bidang Pemberdayaan Masyarakat Desa'],
                    [$u_dc->tahun, $u_dc->kd_desa, $u_dc->kd_kecamatan, $u_dc->kd_desa.'05', 'Bidang Tidak Terduga']
                ];
                $sql = $db->queryBuilder->batchInsert('Ta_Bidang', $fields, $rows); 
                $db->createCommand($sql)->execute();
                //hapus tahun untuk digunakan di RPJM
                unset($fields[0], $rows[0][0], $rows[1][0], $rows[2][0], $rows[3][0], $rows[4][0]);
                
                $sql2 = $db->queryBuilder->batchInsert('Ta_RPJM_Bidang', $fields, $rows); 
                $db->createCommand($sql2)->execute();
            }

            return $mdl;
        }
    }
}

